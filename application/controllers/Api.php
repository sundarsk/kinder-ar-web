<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
var $info;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		
		// Call the CI_Model constructor
		parent::__construct();
		//$this->lang->load("menu_lang","english");
		$this->load->model('Api_model');
		$this->load->library('session');	
		$this->load->library('My_PHPMailer');
		$this->info=array("secret_key"=>"Ed6S@2018","default_play_time"=>$this->config->item('game_limit')); 
		//$this->lang->load("menu_lang","french");		
		
    }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	
	public function checklogin()
	{
		$username = $this->input->post('UserId');
		$password = $this->input->post('Password');
		$deviceid = $this->input->post('DeviceId');
		$Key = $this->input->post('Key');
		$timestamp = $this->input->post('ts');
		
		
		if(($username!='') && ($password!='') && ($deviceid!='') && ($Key!='') && ($timestamp!=''))
		{
			 
			$token=$this->encMethod($timestamp,$deviceid,$this->info['secret_key']); 
			 
			if($Key==$token['Hash'])
			{
				$data['query'] = $this->Api_model->checklogin($username,$password,$deviceid);
				if(isset($data['query'][0]->id))
				{
						if(($data['query'][0]->deviceid!='') && ($data['query'][0]->deviceid!=$deviceid))
						{
							$resparr=array("status"=>"Failed","UserId"=>'-',"msg"=>"User Already Login in some Devices");
							echo json_encode($resparr);
						}
						else
						{
							$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);
							$this->Api_model->update_loginDetails($data['query'][0]->id,$uniqueId);
							
							$login_count=$data['query'][0]->login_count;
							$prelogin_date=$data['query'][0]->pre_logindate;
							$todaydate=date('Y-m-d');
							$login_date=$data['query'][0]->login_date;
							$start_ts = strtotime($login_date);
							$end_ts = strtotime($todaydate);
							$diff = $end_ts - $start_ts;
							$datediff = round($diff / 86400);
							
							if((isset($login_count) && $login_count<=1) || $prelogin_date == "0000-00-00")
							{
								$greetings_content = 'Thanks for subscribing to SkillAngels. I am sure you will enjoy the learning process. Good Luck';
								
							}
							elseif((isset($login_count) && $login_count>1) && $datediff <3 )
							{
								$greetings_content ='You last logged on '.date("d-m-Y", strtotime($prelogin_date)).' Nice to see you pay so much  attention';
								
							}
							elseif((isset($login_count) && $login_count>1) && ($datediff >= 3 && $datediff < 7 ))
							{
								$greetings_content =' You last logged on '.date("d-m-Y", strtotime($prelogin_date)).'. You were missing for the last '.$datediff.' days. Everything Okay?';
							}
							elseif((isset($login_count) && $login_count>1) && ($datediff >= 7 && $datediff <= 30 ))
							{
								$greetings_content ='You last logged on '.date("d-m-Y", strtotime($prelogin_date)).'.  You were missing for the last '.$datediff.' days. Please make it a habit to use SkillAngels regularly';
							}
							elseif((isset($login_count) && $login_count>1) && ($datediff > 30))
							{
								$greetings_content ='You last logged on '.date("d-m-Y", strtotime($prelogin_date)).'. You were missing for the last '.$datediff.' days. That seems to be a long break. Please resume your training  Good Luck';
							}
							/*......... Crownies .........*/
							if($data['query'][0]->portal_type=='CLP')
							{
								$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$data['query'][0]->id,'inScenarioCode'=>'LOGIN','inTotal_Ques'=>'','inAttempt_Ques'=>'','inAnswer'=>'','inGame_Score'=>'',"inPlanid"=>$data['query'][0]->gp_id,'inGameid'=>'');
							
								$crownies_output=$this->Api_model->insertsparkies($arrofinput);
								$finalpoints=$crownies_output[0]['OUTPOINTS'];
							}
							else
							{
								$finalpoints='0';
							}
							/*......... Crownies .........*/
							$curdate=strtotime(date('Y-m-d'));
							$mydate=strtotime($data['query'][0]->enddate);
							if($curdate <= $mydate)
							{
								$isexpired=1;
							}
							else
							{	// User expired
								$finalpoints='0';$isexpired=0;
							}
							
							if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
							 $ip=$_SERVER['HTTP_CLIENT_IP'];}
							 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
							 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
							 $ip=$_SERVER['REMOTE_ADDR'];}
							 
							$this->Api_model->insert_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
							
							$data['query1'] = $this->Api_model->updateDeviceID($username,$deviceid,'');
							
							$isportal=$this->Api_model->getOrganization_PortalType($data['query'][0]->org_id);
							
							$portal_type=$data['query'][0]->portal_type;
							$portal_game_type=$isportal[0]['asaponly'];
							
							
							$resparr=array("status"=>"success","UserId"=>$data['query'][0]->username,"msg"=>"User Login successfully","Welcomemsg"=>$greetings_content,"Crownypoints"=>$finalpoints,"isexpired"=>$isexpired,"portal_type"=>$portal_type,"portal_game_type"=>$portal_game_type);
							echo json_encode($resparr);
						}
				}
				else 
				{
					$resparr=array("status"=>"Failed","UserId"=>"-","msg"=>"Username or Password Wrong","Welcomemsg"=>'',"Crownypoints"=>'',"isexpired"=>'',"portal_type"=>"-","portal_game_type"=>"-");
					echo json_encode($resparr);
				}
			}
			else
			{
				$resparr=array("status"=>"Failed","UserId"=>"-","msg"=>"Key Mismatch","Welcomemsg"=>'',"Crownypoints"=>'',"isexpired"=>'',"portal_type"=>"-","portal_game_type"=>"-");
				echo json_encode($resparr);
			}
		}
	}
	
	public function fpvalidate()
	{
		$username = $this->input->post('UserId');
		
		if(($username!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
					
					$OTP=mt_rand(100000, 999999);
					$MSG="Dear ".$data['query'][0]->fname.", OTP : ".$OTP." is to reset your password";
					$this->sendsms($data['query'][0]->mobile,$MSG);
					$this->Api_model->updatetempotp($OTP,$data['query'][0]->id);
					$this->Api_model->forgetpwdlog($data['query'][0]->id,$OTP);
					$resparr=array("status"=>"success","OTP"=>$OTP,"msg"=>"-");
					echo json_encode($resparr);
			}
			else
			{
				$resparr=array("status"=>"Failed","OTP"=>"-","msg"=>"Enter Valid Username");
				echo json_encode($resparr);
			}
		}
	}
	
	
	public function fpupdate()
	{
		$username = $this->input->post('UserId');
		$password = $this->input->post('Password');
		$OTP = $this->input->post('OTP');
 		 
		if(($username!='') && ($password!='') && ($OTP!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			
			if(isset($data['query'][0]->id))
			{		 
				if($data['query'][0]->otp==$OTP)
				{
					$shpassword = $this->salt_my_pass($password);
					$data['query1'] = $this->Api_model->updatePassword($data['query'][0]->id,$username,$password,$shpassword['Hash'],$shpassword['Salt1'],$shpassword['Salt2']);
					$resparr=array("status"=>"success","msg"=>"Password Updated Successfully");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"OTP Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}

 
 	public function flushlogin()
	{
		$username = $this->input->post('UserId');
		if(($username!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
					$OTP=mt_rand(100000, 999999);
					$MSG="Dear ".$data['query'][0]->fname.", OTP : ".$OTP." is to confirm to flush logins in other device";
					$this->sendsms($data['query'][0]->mobile,$MSG);
					$this->Api_model->updatetempotp($OTP,$data['query'][0]->id);
					$resparr=array("status"=>"success","OTP"=>$OTP,"msg"=>"-");
					echo json_encode($resparr);
				
			}
			else 
			{
				$resparr=array("status"=>"Failed","OTP"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}

	
 	public function flushloginupdate()
	{
		$username = $this->input->post('UserId');
		$deviceid = $this->input->post('DeviceId');
		$OTP = $this->input->post('OTP');
		if(($username!='') && ($deviceid!='') && ($OTP!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{ 
				if($data['query'][0]->otp==$OTP)
				{
					$data['query1'] = $this->Api_model->updateDeviceID($username,$deviceid,'');
					$this->Api_model->clearTempotp($data['query'][0]->id);
					$resparr=array("status"=>"success","UserId"=>$data['query'][0]->username,"msg"=>"Login in other device has been flushed successfully");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","UserId"=>'-',"msg"=>"OTP Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","UserId"=>'-',"msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	
 
 	public function userprofile()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					if($data['query'][0]->show_grade=='Y')
					{
						$gradename=$data['query'][0]->gradename;
					}
					else
					{
						$gradename='';
					}
					$expiry_date=date("d-m-Y", strtotime($data['query'][0]->enddate));
					$resparr=array("status"=>"success","first_name"=>$data['query'][0]->fname,"last_name"=>$data['query'][0]->lname,"mailid"=>$data['query'][0]->email,"mobile"=>$data['query'][0]->mobile,"expiry_date"=>$expiry_date,"grade"=>$gradename,"start_date"=>$data['query'][0]->startdate,"avatarimage"=>$data['query'][0]->avatarimage,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","first_name"=>"-","last_name"=>"-","mailid"=>"-","mobile"=>"-","expiry_date"=>"-", "grade"=>"-","start_date"=>"-","avatarimage"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","first_name"=>"-","last_name"=>"-","mailid"=>"-","mobile"=>"-","expiry_date"=>"-", "grade"=>"-","start_date"=>"-","avatarimage"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
 	public function gamedetails()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$check_date_time = date('Y-m-d');
					$catid=1;
					$userid=$data['query'][0]->id;
					$planid=$data['query'][0]->gp_id;
					$gradeid=$data['query'][0]->grade_id;
					$schoolid=$data['query'][0]->sid;
					$section=$data['query'][0]->section;
					$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					
					$data['assignGames']=$this->Api_model->getAssignGames($planid,$gradeid,$userid,$catid);
					
					$cur_day_skills = count($data['randomGames']);
					$assign_count = count($data['assignGames']);
									

					if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) 
					{						
						$this->fn_Rand_games($userid,$check_date_time,$cur_day_skills,$assign_count,$catid,$planid,$gradeid,$schoolid,$section);
					}
					$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					$where = "";
					foreach($data['randomGames'] as $brainData )
					{
						$brainIds[] = $brainData['gid'];
						$active_game_ids = @implode(',',$brainIds);
						$where = " and g.gid in ($active_game_ids)";
					}
						 
					$data['actualGames'] = $this->Api_model->getActualGames($planid,$gradeid,$userid,$catid,$where,$schoolid);
					
					$data['checkgame'] = $this->Api_model->checkgame($data['actualGames'][0]['gid'],$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
 					if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
					
					if($gradeid==8 || $gradeid==9 || $gradeid==10)
					{	//Grade VI,VII & VIII
						$cqvisible=$this->Api_model->getChallengeQuestionVisible($userid);
						if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
						{
							$CQvisible=1;
						}
						else if($cqvisible[0]['playedstatus']==1)
						{
							$CQvisible=2;
						}
						else
						{
							$CQvisible=0;
						}
					}
					else
					{
						$CQvisible='-1';
					}
					
					$resparr=array("status"=>"success","gamedetails"=>$data['actualGames'],"image_path"=>base_url('assets/'),"total_play_times"=>$this->info['default_play_time'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function gamedetails1()
	{
		$username = 'sundar7@gmail.com';
		
		$data['query'] = $this->Api_model->checkuserexist($username);
		if(isset($data['query'][0]->id))
		{
				
				$check_date_time = date('Y-m-d');
				$catid=1;
				$userid=$data['query'][0]->id;
				$planid=$data['query'][0]->gp_id;
				$gradeid=$data['query'][0]->grade_id;
				$schoolid=$data['query'][0]->sid;
				$section=$data['query'][0]->section;
				$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
				
				$data['assignGames']=$this->Api_model->getAssignGames($planid,$gradeid,$userid,$catid);
				
				$cur_day_skills = count($data['randomGames']);
				$assign_count = count($data['assignGames']);
								

				if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) 
				{						
					$this->fn_Rand_games($userid,$check_date_time,$cur_day_skills,$assign_count,$catid,$planid,$gradeid,$schoolid,$section);
				}
				$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
				$where = "";
				foreach($data['randomGames'] as $brainData )
				{
					$brainIds[] = $brainData['gid'];
					$active_game_ids = @implode(',',$brainIds);
					$where = " and g.gid in ($active_game_ids)";
				}
					 
				$data['actualGames'] = $this->Api_model->getActualGames($planid,$gradeid,$userid,$catid,$where,$schoolid);
				
				$data['checkgame'] = $this->Api_model->checkgame($data['actualGames'][0]['gid'],$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
				if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
				
				if($gradeid==8 || $gradeid==9 || $gradeid==10)
				{	//Grade VI,VII & VIII
					$cqvisible=$this->Api_model->getChallengeQuestionVisible($userid);
					if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
					{
						$CQvisible=1;
					}
					else if($cqvisible[0]['playedstatus']==1)
					{
						$CQvisible=2;
					}
					else
					{
						$CQvisible=0;
					}
				}
				else
				{
					$CQvisible='-1';
				}
				
				$resparr=array("status"=>"success","gamedetails"=>$data['actualGames'],"image_path"=>base_url('assets/'),"total_play_times"=>$this->info['default_play_time'],"msg"=>"-");
				//echo json_encode($resparr);
				echo "<pre>";print_r($resparr);
		}
		else 
		{
			$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Invalid User");
			echo json_encode($resparr);
		}
	}
	
	public function Ogamedetails()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$check_date_time = date('Y-m-d');
					$catid=1;
					$userid=$data['query'][0]->id;
					$planid=$data['query'][0]->gp_id;
					$gradeid=$data['query'][0]->grade_id;
					$schoolid=$data['query'][0]->sid;
					$section=$data['query'][0]->section;
					$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					
					$data['assignGames']=$this->Api_model->getAssignGames($planid,$gradeid,$userid,$catid);
					
					$cur_day_skills = count($data['randomGames']);
					$assign_count = count($data['assignGames']);
									

					if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) 
					{						
						$this->fn_Rand_games($userid,$check_date_time,$cur_day_skills,$assign_count,$catid,$planid,$gradeid,$schoolid,$section);
					}
					$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					$where = "";
					foreach($data['randomGames'] as $brainData )
					{
						$brainIds[] = $brainData['gid'];
						$active_game_ids = @implode(',',$brainIds);
						$where = " and g.gid in ($active_game_ids)";
					}
						 
					$data['actualGames'] = $this->Api_model->getActualGames($planid,$gradeid,$userid,$catid,$where,$schoolid);
					
					$data['checkgame'] = $this->Api_model->checkgame($data['actualGames'][0]['gid'],$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
 					if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
					
					if($gradeid==8 || $gradeid==9 || $gradeid==10)
					{	//Grade VI,VII & VIII
						$cqvisible=$this->Api_model->getChallengeQuestionVisible($userid);
						if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
						{
							$CQvisible=1;
						}
						else if($cqvisible[0]['playedstatus']==1)
						{
							$CQvisible=2;
						}
						else
						{
							$CQvisible=0;
						}
					}
					else
					{
						$CQvisible='-1';
					}
					$gid=$data['actualGames'][0]['id'].",".$data['actualGames'][1]['id'].",".$data['actualGames'][2]['id'];
				
					$gname=$data['actualGames'][0]['game_html'].",".$data['actualGames'][1]['game_html'].",".$data['actualGames'][2]['game_html'];
					
					$img_path=base_url('assets/')."".$data['actualGames'][0]['img_path'].",".base_url('assets/')."".$data['actualGames'][1]['img_path'].",".base_url('assets/')."".$data['actualGames'][2]['img_path'];
					
					$game_score=$data['actualGames'][0]['tot_game_score'].",".$data['actualGames'][1]['tot_game_score'].",".$data['actualGames'][2]['tot_game_score'];
					
					$tot_game_played=$data['actualGames'][0]['tot_game_played'].",".$data['actualGames'][1]['tot_game_played'].",".$data['actualGames'][2]['tot_game_played'];
					
					
					$resparr=array("status"=>"success","gid"=>$gid,"gname"=>$gname,"img_path"=>$img_path,"game_score"=>$game_score,"tot_game_played"=>$tot_game_played,"total_play_times"=>$this->info['default_play_time'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function Ogamedetails1()
	{
		$username ='sundar7@gmail.com';
		
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$check_date_time = date('Y-m-d');
				$catid=1;
				$userid=$data['query'][0]->id;
				$planid=$data['query'][0]->gp_id;
				$gradeid=$data['query'][0]->grade_id;
				$schoolid=$data['query'][0]->sid;
				$section=$data['query'][0]->section;
				$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
				
				$data['assignGames']=$this->Api_model->getAssignGames($planid,$gradeid,$userid,$catid);
				
				$cur_day_skills = count($data['randomGames']);
				$assign_count = count($data['assignGames']);
								

				if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) 
				{						
					$this->fn_Rand_games($userid,$check_date_time,$cur_day_skills,$assign_count,$catid,$planid,$gradeid,$schoolid,$section);
				}
				$data['randomGames']=$this->Api_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
				$where = "";
				foreach($data['randomGames'] as $brainData )
				{
					$brainIds[] = $brainData['gid'];
					$active_game_ids = @implode(',',$brainIds);
					$where = " and g.gid in ($active_game_ids)";
				}
					 
				$data['actualGames'] = $this->Api_model->getActualGames($planid,$gradeid,$userid,$catid,$where,$schoolid);
				
				$data['checkgame'] = $this->Api_model->checkgame($data['actualGames'][0]['gid'],$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
				if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
				
				if($gradeid==8 || $gradeid==9 || $gradeid==10)
				{	//Grade VI,VII & VIII
					$cqvisible=$this->Api_model->getChallengeQuestionVisible($userid);
					if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
					{
						$CQvisible=1;
					}
					else if($cqvisible[0]['playedstatus']==1)
					{
						$CQvisible=2;
					}
					else
					{
						$CQvisible=0;
					}
				}
				else
				{
					$CQvisible='-1';
				}
				
				$gid=$data['actualGames'][0]['id'].",".$data['actualGames'][1]['id'].",".$data['actualGames'][2]['id'];
				
				$gname=$data['actualGames'][0]['game_html'].",".$data['actualGames'][1]['game_html'].",".$data['actualGames'][2]['game_html'];
				
				$img_path=base_url('assets/')."".$data['actualGames'][0]['img_path'].",".base_url('assets/')."".$data['actualGames'][1]['img_path'].",".base_url('assets/')."".$data['actualGames'][2]['img_path'];
				
				$game_score=$data['actualGames'][0]['tot_game_score'].",".$data['actualGames'][1]['tot_game_score'].",".$data['actualGames'][2]['tot_game_score'];
				
				$tot_game_played=$data['actualGames'][0]['tot_game_played'].",".$data['actualGames'][1]['tot_game_played'].",".$data['actualGames'][2]['tot_game_played'];
				
				
				$resparr=array("status"=>"success","gid"=>$gid,"gname"=>$gname,"img_path"=>$img_path,"game_score"=>$game_score,"tot_game_played"=>$tot_game_played,"total_play_times"=>$this->info['default_play_time'],"msg"=>"-");
				echo json_encode($resparr);
				//echo "<pre>";print_r($resparr);exit;
			}
			else 
			{
				$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		
	}
	
	public function logout()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
				{
					$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
					if($Key==$token['Hash'])
					{	
						$this->Api_model->updateDeviceID($username,'','');
						$this->Api_model->update_logout_log($data['query'][0]->id,$data['query'][0]->session_id);
						$this->Api_model->updateuserloginstatus($data['query'][0]->id,$data['query'][0]->session_id);
						$resparr=array("status"=>"success","msg"=>"Device Id cleared successfully");
						echo json_encode($resparr);
					}
					else 
					{
						$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
						echo json_encode($resparr);
					}
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Invalid User");
					echo json_encode($resparr);
				}
		}
	}
 	public function loadgameold()
	{
		 
		$username = $this->input->post('UserId');
		$gameid = $this->input->post('GameId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
 
		if(($username!='')  && ($gameid!='') && ($Key!='') && ($timestamp!=''))
		{
			
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{
  
					$data['checkgame'] = $this->Api_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
									
					$gid = $data['checkgame'][0]['gameexist']; 
					if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
					 
					if($data['checkgame'][0]['game_limit']>$data['checkgame'][0]['played_time'])
					{
						if($gid>=1)
						{
							
							$this->session->set_userdata(array( 'currentgameid'=> $gameid ));
							$this->session->set_userdata(array( 'isskillkit'=> 'N' ));
							$this->session->set_userdata(array( 'isapp'=> 1 ));
							$this->session->set_userdata(array( 'app_userid'=>$username ));
							
							if(($data['query'][0]->grade_id == 1) || ($data['query'][0]->grade_id==11) || ($data['query'][0]->grade_id==2))
							{
								/* if($data['query'][0]->id=='75' || $data['query'][0]->id=='91' || $data['query'][0]->id=='92' || $data['query'][0]->id=='93')
								{	$gamepath = '1';
								}
								else
								{	$gamepath = 'kinder';
								} */
								$gamepath = '4';
							}
							else
							{
								$gamepath = 3;
							}
							
							redirect(base_url('assets/swf/'.$gamepath.'/'.$data['checkgame'][0]['gpath'].'.html'));
						}
						else
						{
							
							//echo 'IA'; exit;
						}
					}
					else
					{
						echo "<html><body style='width: 100%;text-align: center;margin: 0 auto;background: #0db1c1;'><div class='' style='left: 50%;top: 50%;transform: translate(-50%, -50%);width: 35%;height:10%;padding: 10%;background: #fff;color: #f30694;text-align: center;font-weight: bold;position: fixed;font-size: 25px;box-shadow: 5px 10px #0e545a;'><h2 style='padding: 0;margin: 0;'>Your Game Limit Expired</h2></div></body></html>";exit;
					}
	
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function result()
	{
		$data['query'] = $this->Api_model->checkuserexist($this->session->app_userid);
		if(isset($data['query'][0]->id))
		{
			$gameid=$this->session->currentgameid;
			
			if($gameid==0)
			{
				echo '-2';exit;
			}
			
			$data['checkgame'] = $this->Api_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
			if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}		
 			if($data['checkgame'][0]['game_limit']>$data['checkgame'][0]['played_time'])
			{ 

				$postdata = $this->session->flashdata('app_post_data');
				$total_ques=$postdata["tqcnt1"];
				if($postdata["aqcnt1"]>10){
					$attempt_ques=10;
				}
				else{
					$attempt_ques=$postdata["aqcnt1"];
				}

				$answer=$postdata["cqcnt1"];
				$score=$postdata["gscore1"];
				$a6=$postdata["gtime1"];
				$a7=$postdata["rtime1"];
				$a8=$postdata["crtime1"];
				$a9=$postdata["wrtime1"];	
				
				$skillkit=0;
				$userlang = 1;
				$userid = $data['query'][0]->id; 
				$lastup_date = date("Y-m-d");
				$cid = 1;
				$data['gameDetails'] = $this->Api_model->getresultGameDetails($userid,$gameid);
				$skillid =$data['gameDetails'][0]['gameskillid'] ; 
				$schedule_val = 0;
				$pid =  $data['query'][0]->gp_id; 
				
				$data['insert1'] = $this->Api_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val);
			
				$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);

/*........................... Push Notification ..........................................................*/
			$TopScore = $this->Api_model->getTopScorePosition($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
			$PuzzlesAttempted= $this->Api_model->getMaxPuzzlesAttempted($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
			$PuzzlesSolved= $this->Api_model->getMaxPuzzlesSolved($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
			
			
			if($TopScore[0]['position']!='' && $TopScore[0]['position']==1)
			{$b1position=1;}
			else{$b1position=0;}
			$arrbadgeposition= $this->Api_model->getPrevUserPosition($userid);
			if($b1position==$arrbadgeposition[0]['sbb_position'])
			{
				// Same 
			}
			else if($b1position==1 && $arrbadgeposition[0]['sbb_position']==0)
			{
				// New Badge
				$this->Api_model->InsertUserBadgeStatusLog($userid,'SBB',$TopScore[0]['score'],1,'sbb_position',$b1position);
				
				// CALL API
				$this->sendPush('You Got a Super Brain Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
			else if($b1position==0 && $arrbadgeposition[0]['sbb_position']==1)
			{
				// Loose the Badge
				$this->Api_model->InsertUserBadgeStatusLog($userid,'SBB',$TopScore[0]['score'],0,'sbb_position',$b1position);
				
				// CALL API
				//$this->sendPush('You Got a Super Brain Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
/*....................................... Push Notification SGB ..............................................*/
			if($PuzzlesAttempted[0]['position']!='' && $PuzzlesAttempted[0]['position']==1)
			{$b2position=1;}
			else{$b2position=0;}
			$arrbadgeposition= $this->Api_model->getPrevUserPosition($userid);
			if($b2position==$arrbadgeposition[0]['sgb_position'])
			{
				// Same 
			}
			else if($b2position==1 && $arrbadgeposition[0]['sgb_position']==0)
			{
				// New Badge
				$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],1,'sgb_position',$b2position);
				
				// CALL API
				$this->sendPush('You Got a Super Goer Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperGoer-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
			else if($b2position==0 && $arrbadgeposition[0]['sgb_position']==1)
			{
				// Loose the Badge
				$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],0,'sgb_position',$b2position);
				
				// CALL API
				//$this->sendPush('You Got a Super Goer Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperGoer-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
/*....................................... Push Notification SAB ..............................................*/
			if($PuzzlesSolved[0]['position']!='' && $PuzzlesSolved[0]['position']==1)
			{$b3position=1;}
			else{$b3position=0;}
			$arrbadgeposition= $this->Api_model->getPrevUserPosition($userid);
			if($b3position==$arrbadgeposition[0]['sab_position'])
			{
				
			}
			else if($b3position==1 && $arrbadgeposition[0]['sab_position']==0)
			{
				// New Badge
				$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],1,'sab_position',$b3position);
				
				// CALL API
				$this->sendPush('You Got a Super Angels Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperAngel-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
			else if($b3position==0 && $arrbadgeposition[0]['sab_position']==1)
			{
				// Loose the Badge
				$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],0,'sab_position',$b3position);
				
				// CALL API
				//$this->sendPush('You Got a Super Angels Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperAngel-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
/*....................................... Push Notification END ..............................................*/

			/*--- Sparkies ----*/
			$sparkies_output=$this->Api_model->insertsparkies($arrofinput);
			echo $sparkies_output[0]['OUTPOINTS'];exit; 
				
			}
			$this->session->unset_userdata('app_userid');
		}
	}

	public function fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid,$planid,$gradeid,$schoolid,$section) 
	{
		if($cur_day_skills!=$assign_count)
		{ // MisMatch Skill Count => Delete mismateched games for that user in todays date.
			$del_where = " and created_date = '$check_date_time'";
			$this->Api_model->deleteMisMatchRandomGames($catid,$planid,$gradeid,$uid,$schoolid,$del_where);
		}
		//$CurrentDaySkill=$this->Api_model->getCurrentDaySkill($uid);
		$arrSkills=$this->Api_model->getSkillsRandom($catid,$CurrentDaySkill[0]['currentdayskillid']);
		foreach($arrSkills as $gs_data)
		{
		  $rand_sel = $this->Api_model->assignRandomGame($catid,$planid,$gradeid,$uid,$gs_data['skill_id'],$schoolid);
			$rand_count = count($rand_sel);
			if($rand_count<=0)
			{ 
			/*--- No Unique games => delete previous all games of user( in particular skill) and insert new game for that skill (for that date only) ---*/
		
				$del_where = " ";
				$this->Api_model->deleteRandomGames($catid,$planid,$gradeid,$uid,$gs_data['skill_id'],$schoolid,$del_where);
				
				$rand_sel = $this->Api_model->assignRandomGame($catid,$planid,$gradeid,$uid,$gs_data['skill_id'],$schoolid);
				
				$this->Api_model->insertRandomGames($catid,$planid,$gradeid,$gs_data['skill_id'],$schoolid,$section,$rand_sel[0]['gid'],$check_date_time,$uid);
			}
			else
			{ //If New Games available => insert the new game for user for that date
				$this->Api_model->insertRandomGames($catid,$planid,$gradeid,$gs_data['skill_id'],$schoolid,$section,$rand_sel[0]['gid'],$check_date_time,$uid);
			}
		}
	} 
	public function sendsms($tonum,$message)
	{
		$ch = curl_init("http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=Ac79406bbeca30e25c926bad8928bcc17&to=".$tonum."&sender=SBRAIN&message=".urlencode($message));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response=curl_exec($ch);       
		curl_close($ch);
	}
	public function confirmation_email($toemailid,$subject,$message)
	{
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = "smtp.falconide.com";
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "";
		$mail->Username = "skillsangelsfal";
		$mail->Password = "SkillAngels@123";
		$mail->setFrom('angel@skillangels.com', 'Summer Champs');
		$mail->addReplyTo('angel@skillangels.com', 'Summer Champs');
		$mail->addAddress($toemailid, ''); //to mail id
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		if (!$mail->send()) {
		   //echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		   //echo "Message sent!";
		}  
	}
	public function salt_my_pass($password)
	{
		$salt1 = mt_rand(1000,9999999999);
		$salt2 = mt_rand(100,999999999);
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	} 
	public function encMethod($salt1,$password,$salt2)
	{
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	} 
/*............... Sundar ...................*/
	public function report()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$arrresult=$this->Api_model->getUserPuzzlesDetails($data['query'][0]->id);
					/*.... User Current Total Score ........*/
					$UserBspi=$this->Api_model->getUserCurrentBspi($data['query'][0]->id);
					if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!='')
					{$totalscore=$UserBspi[0]['bspi'];}else{$totalscore=0 ;}
					/*.... User Current Percentile Rank ........*/
					$arrofoverall=$this->Api_model->MyPercentileRank($data['query'][0]->id,$data['query'][0]->grade_id,'');
					$arroforg=$this->Api_model->MyPercentileRank($data['query'][0]->id,$data['query'][0]->grade_id,$data['query'][0]->org_id);
					$OverallPercentile=((($arrofoverall[0]['belowusercount']+(0.5*$arrofoverall[0]['samescoreusercount']))/$arrofoverall[0]['totusercount'])*100);
					$orgPercentile=((($arroforg[0]['belowusercount']+(0.5*$arroforg[0]['samescoreusercount']))/$arroforg[0]['totusercount'])*100);
					$OverallPercentile=Round($OverallPercentile,2);
					$OrgPercentile=Round($OrgPercentile,2);
					$PercentileRank=$OverallPercentile."/".$OrgPercentile;
					/*.... User Current Crownies Points ........*/
					$crowny_total_points=$this->Api_model->getMyCurrentSparkies($data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->id,$data['query'][0]->startdate,$data['query'][0]->enddate);
					$crownytotpoints=$crowny_total_points[0]['mysparkies'];
					
					$resparr=array("status"=>"success","MinutesTrained"=>$arrresult[0]['MinutesTrained'],"PuzzlesSolved"=>$arrresult[0]['PuzzlesSolved'],"PuzzlesAttempted"=>$arrresult[0]['PuzzlesAttempted'],"TotalSession"=>$arrresult[0]['TotalSession'],"AttendedSession"=>$arrresult[0]['AttendedSession'],"totalscore"=>$totalscore,"PercentileRank"=>$PercentileRank,"CrownyTotpoints"=>$crownytotpoints,"cqplayedcount"=>$arrresult[0]['cqplayedcount'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","MinutesTrained"=>"-","PuzzlesSolved"=>"-","PuzzlesAttempted"=>"-","TotalSession"=>"-","AttendedSession"=>"-","totalscore"=>"-","PercentileRank"=>"-","CrownyTotpoints"=>"-","cqplayedcount"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}				
			else 
			{
				$resparr=array("status"=>"Failed","MinutesTrained"=>"-","PuzzlesSolved"=>"-","PuzzlesAttempted"=>"-","TotalSession"=>"-","AttendedSession"=>"-","totalscore"=>"-","PercentileRank"=>"-","CrownyTotpoints"=>"-","cqplayedcount"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function userplayeddate()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$arrresult=$this->Api_model->getUserPlayedDates($data['query'][0]->id);
					
					$resparr=array("status"=>"success","playeddate"=>$arrresult[0]['playeddate'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","playeddate"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}				
			else 
			{
				$resparr=array("status"=>"Failed","playeddate"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function userplayeddetails()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		$curdate = $this->input->post('curdate');
		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($curdate!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$arrresult=$this->Api_model->getUserPlayedDetails($data['query'][0]->id,$curdate);
					
					$resparr=array("status"=>"success","MinutesTrained"=>$arrresult[0]['MinutesTrained'],"PuzzlesSolved"=>$arrresult[0]['PuzzlesSolved'],"PuzzlesAttempted"=>$arrresult[0]['PuzzlesAttempted'],"TotalScore"=>$arrresult[0]['TotalScore'],"Gname"=>$arrresult[0]['Gname'],"Crownies"=>$arrresult[0]['Crownies'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","MinutesTrained"=>"-","PuzzlesSolved"=>"-","PuzzlesAttempted"=>"-","TotalScore"=>"-","Gname"=>"-","Crownies"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","MinutesTrained"=>"-","PuzzlesSolved"=>"-","PuzzlesAttempted"=>"-","TotalScore"=>"-","Gname"=>"-","Crownies"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function linechart()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$datewisescore= $this->Api_model->datewisescore($data['query'][0]->id);
					$resparr=array("status"=>"success","playeddate"=>$datewisescore,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","playeddate"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","playeddate"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	/*.................. Challenge Question .............*/
	public function loadcqgame()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$gradeid=$data['query'][0]->grade_id;
					if($gradeid==8 || $gradeid==9 || $gradeid==10)
					{	//Grade VI,VII & VIII
						$cqvisible=$this->Api_model->getChallengeQuestionVisible($data['query'][0]->id);
						if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
						{
							$CQvisible=1;
						}
						else
						{
							$CQvisible=0;
						}
					}
					else
					{
						$CQvisible='-1';
					}
					if($CQvisible==1)
					{		$this->session->set_userdata(array('isappcq'=> 1 ));
							$this->session->set_userdata(array('app_cquserid'=>$username ));
							
						redirect(base_url('assets/swf/CQ/ChallengeQuestion.html'));
					}
					else
					{
						echo "NA";exit;
					}
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function clpgameend()
	{
		$username = $this->input->post('UserId');
		$gameid = $this->input->post('GameId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
 
		if(($username!='') && ($gameid!='') &&($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$crowniesforgame=$this->Api_model->getCrwoniesofGame($gameid,$data['query'][0]->id);
					$gradeid=$data['query'][0]->grade_id;
					if($gradeid==8 || $gradeid==9 || $gradeid==10)
					{	//Grade VI,VII & VIII
						$cqvisible=$this->Api_model->getChallengeQuestionVisible($data['query'][0]->id);
						if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
						{
							$CQvisible=1;
						}
						else
						{
							$CQvisible=0;
						}
					}
					else
					{
						$CQvisible='-1';
					}
					$resparr=array("status"=>"success","CQvisible"=>$CQvisible,"Crownies"=>$crowniesforgame[0]['points'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","CQvisible"=>"-","Crownies"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","CQvisible"=>"-","Crownies"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function cqgameend()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$gradeid=$data['query'][0]->grade_id;
					if($gradeid==8 || $gradeid==9 || $gradeid==10)
					{	//Grade VI,VII & VIII
						$cqvisible=$this->Api_model->getChallengeQuestionVisible($data['query'][0]->id);
						if($cqvisible[0]['playedstatus']>0)
						{
							$CQstatus='ISPLAYED';
						}
						else
						{
							$CQstatus='NP';
						}
					}
					else
					{
						$CQstatus="-1";
					}
					$resparr=array("status"=>"success","CQstatus"=>$CQstatus,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","CQstatus"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","CQstatus"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function resultcq()
	{
		$data['query'] = $this->Api_model->checkuserexist($this->session->app_cquserid);
		if(isset($data['query'][0]->id))
		{
			$postdata = $this->session->flashdata('cqapp_post_data');
			$total_ques=$postdata["tqcnt1"];
			if($postdata["aqcnt1"]>1){
			$attempt_ques=1;
			}
			else{
			$attempt_ques=$postdata["aqcnt1"];
			}
		
			$answer=$postdata["cqcnt1"];
			$score=$postdata["gscore1"];
			$a6=$postdata["gtime1"];
			$a7=$postdata["rtime1"];
			$a8=$postdata["crtime1"];
			$a9=$postdata["wrtime1"];
			$gameid=$postdata["quesNo1"];
			
			
			$userid = $data['query'][0]->id; 
			$lastup_date = date("Y-m-d");
			$cid = 1;
			$skillid='';
			$pid =$data['query'][0]->gp_id; 
			if($postdata["quesNo1"]!='')
			{
				$data['insert1'] = $this->Api_model->insertone_CQ($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
			
				$this->Api_model->InsertCQGames($userid,$gameid);
			}
			
			echo 1;exit;
		}
		$this->session->unset_userdata('app_cquserid');
	}
	
	/*......... Badges Concept ...........*/
	public function superbrainbadge()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$position = $this->Api_model->getTopScorePosition($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					if($position[0]['position']!='')
					{
						$position=$position[0]['position'];
					}
					else
					{
						$position=0;
					}
					$resparr=array("status"=>"success","Position"=>$position,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function supergoerbadge()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$position= $this->Api_model->getMaxPuzzlesAttempted($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					if($position[0]['position']!='')
					{
						$position=$position[0]['position'];
					}
					else
					{
						$position=0;
					}
					$resparr=array("status"=>"success","Position"=>$position,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function superangelbadge()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$position= $this->Api_model->getMaxPuzzlesSolved($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					if($position[0]['position']!='')
					{
						$position=$position[0]['position'];
					}
					else
					{
						$position=0;
					}
					$resparr=array("status"=>"success","Position"=>$position,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
/*......... Badges Concept End...........*/
/*.......... CQ Question Concept ...........*/
	public function getQuesList()
	{
		$data['query'] = $this->Api_model->checkuserexist($this->session->app_cquserid);
		$userid=$data['query'][0]->id;
		if(isset($userid))
		{
			$data['query']=$this->Api_model->getQuesList($userid);
			foreach($data['query'] as $rows){
				$details[] = array('questionsList'=> $rows['quesPlayed']);
			}
			$jsonString = json_encode($details);
			echo $jsonString;
		}
		else
		{
				echo "POST value not assigned";
		}
	}
	public function getQuesInfo()
	{
		$postdata = $this->session->flashdata('cqapp_que_post_data');
		if(isset($postdata['sno']))
		{
			$sno = $postdata['sno'];
			$data['query']=$this->Api_model->getQuesInfo($sno);
			foreach($data['query'] as $rows)
			{
				$details[] = array(
				'question'=> $rows['question'],
				'choice1' => $rows['choice1'],
				'choice2' => $rows['choice2'],
				'choice3' => $rows['choice3'],
				'choice4' => $rows['choice4'],
				'answer' => $rows['answer'],
				'solution' => $rows['solution'],
				'referred' => $rows['referred'],
				'ques_no' => $rows['ques_no'],
				'exam_date' => $rows['exam_date'],
				'grade' => $rows['grade']
				);
			}
			$jsonString = json_encode($details);
			echo $jsonString;
		}
		else
		{
			echo "POST value not assigned";
		}	
	}
	public function gradewiseList()
	{
		$data['query'] = $this->Api_model->checkuserexist($this->session->app_cquserid);
		$userid=$data['query'][0]->id;
		$grade=$data['query'][0]->grade_id;
		if(isset($grade))
		{
			$data['query']=$this->Api_model->gradewiseList($grade,$userid);
			foreach($data['query'] as $rows){
				$details[] = array('questionsList'=> $rows['qnos']);
			}
			$jsonString = json_encode($details);
			echo $jsonString;
		}
		else
		{
			echo "POST value not assigned";
		}
	}
/*................ CQ Question Concept End ................*/


/*...........Auto Login ..............*/
	public function autologin()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
		$timestamp = $this->input->post('ts');
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 1000);
					$this->Api_model->update_loginDetails($data['query'][0]->id,$uniqueId);
					
					$login_count=$data['query'][0]->login_count;
					$prelogin_date=$data['query'][0]->pre_logindate;
					$todaydate=date('Y-m-d');
					$login_date=$data['query'][0]->login_date;
					$start_ts = strtotime($login_date);
					$end_ts = strtotime($todaydate);
					$diff = $end_ts - $start_ts;
					$datediff = round($diff / 86400);
					
					if((isset($login_count) && $login_count<=1) || $prelogin_date == "0000-00-00")
					{
						$greetings_content = 'Thanks for subscribing to SkillAngels. I am sure you will enjoy the learning process. Good Luck';
						
					}
					elseif((isset($login_count) && $login_count>1) && $datediff <3 )
					{
						$greetings_content ='You last logged on '.date("d-m-Y", strtotime($prelogin_date)).' Nice to see you pay so much  attention';
						
					}
					elseif((isset($login_count) && $login_count>1) && ($datediff >= 3 && $datediff < 7 ))
					{
						$greetings_content =' You last logged on '.date("d-m-Y", strtotime($prelogin_date)).'. You were missing for the last '.$datediff.' days. Everything Okay?';
					}
					elseif((isset($login_count) && $login_count>1) && ($datediff >= 7 && $datediff <= 30 ))
					{
						$greetings_content ='You last logged on '.date("d-m-Y", strtotime($prelogin_date)).'.  You were missing for the last '.$datediff.' days. Please make it a habit to use SkillAngels regularly';
					}
					elseif((isset($login_count) && $login_count>1) && ($datediff > 30))
					{
						$greetings_content ='You last logged on '.date("d-m-Y", strtotime($prelogin_date)).'. You were missing for the last '.$datediff.' days. That seems to be a long break. Please resume your training  Good Luck';
					}
					
					/*......... Crownies .........*/
					if($data['query'][0]->portal_type=='CLP')
					{
						$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$data['query'][0]->id,'inScenarioCode'=>'LOGIN','inTotal_Ques'=>'','inAttempt_Ques'=>'','inAnswer'=>'','inGame_Score'=>'',"inPlanid"=>$data['query'][0]->gp_id,'inGameid'=>'');
						
						$crownies_output=$this->Api_model->insertsparkies($arrofinput);
						$finalpoints=$crownies_output[0]['OUTPOINTS'];
					}
					else
					{
						$finalpoints='0';
					}
					/*......... Crownies .........*/
					$curdate=strtotime(date('Y-m-d'));
					$mydate=strtotime($data['query'][0]->enddate);
					if($curdate <= $mydate)
					{
						$isexpired=1;
					}
					else
					{	// User expired
						$finalpoints='0';$isexpired=0;
					}
					
					if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					 $ip=$_SERVER['HTTP_CLIENT_IP'];}
					 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
					 $ip=$_SERVER['REMOTE_ADDR'];}
					 
					$this->Api_model->insert_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
					
					$data['query1'] = $this->Api_model->updateDeviceID($username,$data['query'][0]->deviceid,$data['query'][0]->mtoken);
					
					$isportal=$this->Api_model->getOrganization_PortalType($data['query'][0]->org_id);
					$portal_type=$data['query'][0]->portal_type;
					$portal_game_type=$isportal[0]['asaponly'];
					
					$resparr=array("status"=>"success","UserId"=>$data['query'][0]->username,"msg"=>"User Login successfully","Welcomemsg"=>$greetings_content,"Crownypoints"=>$finalpoints,"isexpired"=>$isexpired,"portal_type"=>$portal_type,"portal_game_type"=>$portal_game_type);
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","UserId"=>"-","msg"=>"Username or Password Wrong","Welcomemsg"=>'',"Crownypoints"=>'',"isexpired"=>'',"portal_type"=>"-","portal_game_type"=>"-");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","UserId"=>"-","msg"=>"Username or Password Wrong","Welcomemsg"=>'',"Crownypoints"=>'',"isexpired"=>'',"portal_type"=>"-","portal_game_type"=>"-");
					echo json_encode($resparr);
			}
		}
	}
	/*....... Leader Board ........*/
	public function leaderboard()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$data['minscore']=$this->Api_model->maxbspiscore($data['query'][0]->org_id);
					$minscore = $data['minscore'][0]['val'];
					$topperlist=$this->Api_model->homepagetopperslist($minscore,$data['query'][0]->org_id);
					$resparr=array("status"=>"success","topperlist"=>$topperlist,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","topperlist"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","topperlist"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	/*....... Leader Board ........*/
	
	/*....... Profile Avatar List ........*/
	public function avatarlist()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{	
					$baseurl = "assets/images/avatarimages/avatar";
					$imagename='';$visible='';
					if ($dh = opendir($baseurl))
					{	$i=1;
						while (($file = readdir($dh)) !== false)
						{ 
							if($file!='.' && $file!='..')
							{	
								$imagename.="'".$file."',";
								if($i<=5)
								{
									$visible.="'1',";
								}
								else
								{
									$visible.="'0',";
								}
								$i=$i+1;
							}
							
						}
						closedir($dh);
						
						$resparr=array("status"=>"success","avatarurl"=>$baseurl,"avatarname"=>rtrim($imagename,','),"visible"=>rtrim($visible,','),"msg"=>"-");
					}
					else
					{
						$resparr=array("status"=>"Failed","avatarurl"=>$baseurl,"avatarname"=>rtrim($imagename,','),"visible"=>"-","msg"=>"No avatar image available ");
					}
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","avatarurl"=>"-","avatarname"=>"-","visible"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","avatarurl"=>"-","avatarname"=>"-","visible"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
		
	public function avatarupdate()
	{
		$username = $this->input->post('UserId');
		$avatarimg = $this->input->post('avatarimg');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
		if(($username!='') && ($avatarimg!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$avatarimg=$avatarimg; 
					$userid=$data['query'][0]->id;
					$this->Api_model->avatarupdate($userid,$avatarimg);
					$resparr=array("status"=>"success","msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	
	
	/*........... Generating FB Sharing Image....................*/
public function fbsharing()
{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{	
					$uid=$data['query'][0]->id;
					$curdate=date('Y-m-d');
					$arrval=$this->Api_model->getFBSharingData($uid,$curdate);
					
					//$BackgroundImage= base_url().'assets/images/new/FbShare.png';
					$BackgroundImage= '/mnt/vol2/sites/schamps/dev/assets/images/new/FB_Share.png';
					
					$img = $this->LoadPNG($BackgroundImage,$arrval[0]['TotScore'],$arrval[0]['PuzzlesSolved'],$arrval[0]['Crownies'],$curdate);
					 
						$foldername="fb/";
						if (!file_exists($foldername)) {
							mkdir($foldername, 0777, true);
						}
						imagepng($img,$foldername.''.$uid.".png");
						imagedestroy($img);
						$fbimgurl=$foldername.''.$uid.".png?id".date('YmdHis');
						
						$resparr=array("status"=>"success","fbimgurl"=>$fbimgurl,"msg"=>"-");
						echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","fbimgurl"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","fbimgurl"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
}

public function LoadPNG($fBackgroundImage,$TotScore,$PuzzlesSolved,$Crownies,$curdate)
{
/* Attempt to open */
$im = @imagecreatefrompng($fBackgroundImage);
/* See if it failed */
if(!$im)
{
echo "Loading bg has issue ".$fBackgroundImage;
}
// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
$yellow = imagecolorallocate($im, 245, 237, 6);

// The text to draw
// Replace path by your own font path
//$font = APPPATH."../assets/fonts/LobsterTwo-Bold.ttf";
$font = "/mnt/vol2/sites/schamps/dev/assets/fonts/LobsterTwo-Bold.ttf";
// Add some shadow to the text
$text_color = imagecolorallocate($im, 0, 0, 0);


/* imagettftext ($im, 15, 0, 800, 155, $yellow, $font, 'Total Score'); */
//imagettftext ("img","Font SIZE", 0, "X-POs", "Y-POS","COLOR","Style Font","Value");
imagettftext ($im, 80, 0, 700, 210, $white, $font, $TotScore);

/* imagettftext ($im, 15, 0, 155, 480, $yellow, $font, 'Crownies'); */
imagettftext ($im, 60, 0, 1030, 310, $white, $font, $Crownies);


/* imagettftext ($im, 15, 0, 940, 480, $yellow, $font, wordwrap('Question Solved',8)); */
imagettftext ($im, 50, 0, 870, 510, $white, $font, $PuzzlesSolved);


imagettftext ($im, 11, 0, 1030, 20, $white, $font, date('d/m/Y H:i:s'));


return $im;
}
	
public function BadgesStatus()
{					

$this->sendPush('You Got a Super Brain Badge','fNGdPBhQOGo:APA91bGCjrdaSt-2v4u-Hv2bOV_t77hJXcy5sve-OOhzNIq4g_96nNiF6SlN3XBo3HtGtCUsS2-K0OeiwdfKJOxNLDOiHhh--FuK8Iz3jtJ3mSZ3V4iyvXiUznJDQ2fDQ1YyqQf-JIwc-c20rX_sur6LbtjiUXIE9A','https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
exit;

		$userid=77;
		$TopScore = $this->Api_model->getTopScorePosition($userid);
		$PuzzlesAttempted= $this->Api_model->getMaxPuzzlesAttempted($userid);
		$PuzzlesSolved= $this->Api_model->getMaxPuzzlesSolved($userid);
		
		//echo "<pre>";print_r($TopScore);exit;
		if($TopScore[0]['position']!='' && $TopScore[0]['position']==1)
		{$b1position=1;}
		else{$b1position=0;}
		$arrbadgeposition= $this->Api_model->getPrevUserPosition($userid);
		if($b1position==$arrbadgeposition[0]['sbb_position'])
		{
			
		}
		else if($b1position==1 && $arrbadgeposition[0]['sbb_position']==0)
		{
			// New Badge
			$this->Api_model->InsertUserBadgeStatusLog($userid,'SBB',$TopScore[0]['score'],1,'sbb_position',$b1position);
			
			// CALL API
			$this->sendPush('You Got a Super Brain Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
		}
		else if($b1position==0 && $arrbadgeposition[0]['sbb_position']==1)
		{
			// Loose the Badge
			$this->Api_model->InsertUserBadgeStatusLog($userid,'SBB',$TopScore[0]['score'],0,'sbb_position',$b1position);
			
			// CALL API
			$this->sendPush('You Got a Super Brain Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
		}
/*---------------------------------------------------------------- --------------------------------*/
		if($PuzzlesAttempted[0]['position']!='' && $PuzzlesAttempted[0]['position']==1)
		{$b2position=1;}
		else{$b2position=0;}
		$arrbadgeposition= $this->Api_model->getPrevUserPosition($userid);
		if($b2position==$arrbadgeposition[0]['sgb_position'])
		{
			
		}
		else if($b2position==1 && $arrbadgeposition[0]['sgb_position']==0)
		{
			// New Badge
			$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],1,'sgb_position',$b2position);
			
			// CALL API
			$this->sendPush('You Got a Super Goer Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
		}
		else if($b2position==0 && $arrbadgeposition[0]['sgb_position']==1)
		{
			// Loose the Badge
			$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],0,'sgb_position',$b2position);
			
			// CALL API
			$this->sendPush('You Got a Super Goer Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
		}
/*---------------------------------------------------------------- --------------------------------*/
		if($PuzzlesSolved[0]['position']!='' && $PuzzlesSolved[0]['position']==1)
		{$b3position=1;}
		else{$b3position=0;}
		$arrbadgeposition= $this->Api_model->getPrevUserPosition($userid);
		if($b3position==$arrbadgeposition[0]['sab_position'])
		{
			
		}
		else if($b3position==1 && $arrbadgeposition[0]['sab_position']==0)
		{
			// New Badge
			$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],1,'sab_position',$b3position);
			
			// CALL API
			$this->sendPush('You Got a Super Angels Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
		}
		else if($b3position==0 && $arrbadgeposition[0]['sab_position']==1)
		{
			// Loose the Badge
			$this->Api_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],0,'sab_position',$b3position);
			
			// CALL API
			$this->sendPush('You Got a Super Angels Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
		}
			
			/* Get Current position of user
			if(curposition==prevposition)
			{
				// Dont send PN
			}
			else if(prevposition==1 and curposition==0)
			{
				// Lose the Badge
			}
			else 
			{
				// Send New PN msg
			}
			*/
			
}

public function sendPush($body,$mtoken,$icon,$sound,$imgflag)
{
	#API access key from Google API's Console
	define( 'API_ACCESS_KEY', 'AIzaSyBjX69c1Hflu7hBgNTMBp_aa7T9exb5eXo' );
	//$registrationIds = 'fLoWjWYwig8:APA91bHl736oFoJrklskahkQE5rMY0Zb5FsqExflhbfcsezrGUkFz_qXZ6_fkvHSwMKjQIp7Rgs58hik_h9qiUxbzPNgdj-6fHAMYGV0ToLS6fSct38Vx1EsVOzNlegNVmDxAGEQ9I8P';
	$registrationIds=$mtoken;
	#prep the bundle
	$msg = array
	  (
	'body' 	=> $body,
	'title'	=> 'Summer Champ - Notification',
	'icon'	=>$icon,
	'sound' =>$sound,
	'tag'=>$imgflag
	);
	$fields = array
	(
		'to'		=> $registrationIds,
		'notification'	=> $msg
	);


	$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
	#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );
	#Echo Result Of FireBase Server
	echo $result;
}

/*....... M Token Update ........*/
	public function mtoken()
	{
		$username = $this->input->post('UserId');
		$mtoken = $this->input->post('mtoken');
		if(($username!='') && ($mtoken!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			
			if(isset($data['query'][0]->id))
			{
				
					$this->Api_model->updateMtoken($username,$mtoken);
					
					$resparr=array("status"=>"success","msg"=>"mtoken Updated Successfully");
					echo json_encode($resparr);
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr=array("status"=>"Failed","msg"=>"Invalid User");
			echo json_encode($resparr);
		}
	}
	/* public function badge_cron()
	{
			$arrTopScoreuser = $this->Api_model->TopScoreUser();
			$arrPuzzlesAttempteduser= $this->Api_model->TopPuzzlesAttemptedUser();
			$arrPuzzlesSolveduser= $this->Api_model->TopPuzzlesSolvedUser();
	} */
	
	/*................. ASAP Integration .......................*/
	public function asapgamedetails()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$check_date_time = date('Y-m-d');
					$catid=1;
					$userid=$data['query'][0]->id;
					$planid=$data['query'][0]->gp_id;
					$gradeid=$data['query'][0]->grade_id;
					$schoolid=$data['query'][0]->sid;
					$section=$data['query'][0]->section;
					$org_id=$data['query'][0]->org_id;
					
						$arrgame=$this->Api_model->getMultiAssesGameList($gradeid,$org_id);
						$gamescore=$this->Api_model->getGameScore($userid,$org_id);
						
						$gameid=$arrgame[0]['ME'].",".$arrgame[0]['VP'].",".$arrgame[0]['FA'].",".$arrgame[0]['PS'].",".$arrgame[0]['LI'];
						
						$gamename=$arrgame[0]['MEgname'].",".$arrgame[0]['VPgname'].",".$arrgame[0]['FAgname'].",".$arrgame[0]['PSgname'].",".$arrgame[0]['LIgname'];
						
						$imgpath=$arrgame[0]['MEimgpath'].",".$arrgame[0]['VPimgpath'].",".$arrgame[0]['FAimgpath'].",".$arrgame[0]['PSimgpath'].",".$arrgame[0]['LIimgpath'];
						
						$gamepath=$arrgame[0]['MEgamehtml'].",".$arrgame[0]['VPgamehtml'].",".$arrgame[0]['FAgamehtml'].",".$arrgame[0]['PSgamehtml'].",".$arrgame[0]['LIgamehtml'];
						
						$skillid=$arrgame[0]['s1'].",".$arrgame[0]['s2'].",".$arrgame[0]['s3'].",".$arrgame[0]['s4'].",".$arrgame[0]['s5'];
						
						$skillname="Memory,Visual Processing,Focus and Attention,Problem Solving,Linguistics";
						if($gamescore[0]['ME']!=''){$me=$gamescore[0]['ME'];}else{$me='-1';}
						if($gamescore[0]['VP']!=''){$vp=$gamescore[0]['VP'];}else{$vp='-1';}
						if($gamescore[0]['FA']!=''){$fa=$gamescore[0]['FA'];}else{$fa='-1';}
						if($gamescore[0]['PS']!=''){$ps=$gamescore[0]['PS'];}else{$ps='-1';}
						if($gamescore[0]['LI']!=''){$li=$gamescore[0]['LI'];}else{$li='-1';}
						
						$gamescore=$me.",".$vp.",".$fa.",".$ps.",".$li;
						
						$resparr=array("status"=>"success","gid"=>$gameid,"gname"=>$gamename,"skill_id"=>$skillid,"skill_name"=>$skillname,"image_path"=>$imgpath,"total_play_times"=>'',"played_times"=>'',"tot_game_score"=>$gamescore,"org_id"=>$org_id,"imgbaseurl"=>base_url('assets/'),"GameFlag"=>"1","msg"=>"-");
						echo json_encode($resparr);					
				}
				else 
				{
					$resparr=array("status"=>"Failed","gid"=>"-","gname"=>"-","skill_id"=>"-","skill_name"=>"-","image_path"=>"-","total_play_times"=>"-","played_times"=>"-","tot_game_score"=>"-","org_id"=>"-","imgbaseurl"=>"-","GameFlag"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","gid"=>"-","gname"=>"-","skill_id"=>"-","skill_name"=>"-","image_path"=>"-","total_play_times"=>"-","played_times"=>"-","tot_game_score"=>"-","org_id"=>"-","imgbaseurl"=>"-","GameFlag"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function loadgame()
	{
		$username = $this->input->post('UserId');
		$gameid = $this->input->post('GameId');
		$org_id = $this->input->post('org_id');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
 
		if(($username!='')  && ($gameid!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{
					if(isset($org_id) && $org_id!='')
					{
						$data['checkgame'] = $this->Api_model->checkagame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id,$org_id);
						if($data['checkgame'][0]['game_limit']<$data['checkgame'][0]['played_time'])
						{
							echo "<html><body style='width: 100%;text-align: center;margin: 0 auto;background: #0db1c1;'><div class='' style='left: 50%;top: 50%;transform: translate(-50%, -50%);width: 35%;height:10%;padding: 10%;background: #fff;color: #f30694;text-align: center;font-weight: bold;position: fixed;font-size: 25px;box-shadow: 5px 10px #0e545a;'><h2 style='padding: 0;margin: 0;'>Your Game Limit Expired</h2></div></body></html>";exit;
						}
					}
					else
					{
						$data['checkgame'] = $this->Api_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
					}				
					$gid = $data['checkgame'][0]['gameexist']; 
					if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
					 
					if($data['checkgame'][0]['game_limit']>$data['checkgame'][0]['played_time'])
					{
						if($gid>=1)
						{
							
							$this->session->set_userdata(array( 'currentgameid'=> $gameid ));
							$this->session->set_userdata(array( 'isskillkit'=> 'N' ));
							$this->session->set_userdata(array( 'isapp'=> 1 ));
							$this->session->set_userdata(array( 'app_userid'=>$username ));
							if(isset($org_id) && $org_id!='' && $org_id>0)
							{
								$this->session->set_userdata(array( 'currentorg_id'=>$org_id ));
								$this->session->set_userdata(array('isasap'=>1));
								$this->session->set_userdata(array( 'currentuid'=>$data['query'][0]->id));
							}
							
							if(($data['query'][0]->grade_id == 1) || ($data['query'][0]->grade_id==11) || ($data['query'][0]->grade_id==2))
							{
								$gamepath = '4';
							}
							else
							{
								$gamepath = 3;
							}
							
							if($data['query'][0]->org_id==4)
							{
								$gamepath = 'tie';
							}
							
							redirect(base_url('assets/swf/'.$gamepath.'/'.$data['checkgame'][0]['gpath'].'.html'));
						}
						else
						{
							
							//echo 'IA'; exit;
						}
					}
					else
					{
						echo "<html><body style='width: 100%;text-align: center;margin: 0 auto;background: #0db1c1;'><div class='' style='left: 50%;top: 50%;transform: translate(-50%, -50%);width: 35%;height:10%;padding: 10%;background: #fff;color: #f30694;text-align: center;font-weight: bold;position: fixed;font-size: 25px;box-shadow: 5px 10px #0e545a;'><h2 style='padding: 0;margin: 0;'>Your Game Limit Expired</h2></div></body></html>";exit;
					}
	
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	
	public function a1()
	{
		$username ='sundar6@gmail.com';
		 
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
					
					$check_date_time = date('Y-m-d');
					$catid=1;
					$userid=$data['query'][0]->id;
					$planid=$data['query'][0]->gp_id;
					$gradeid=$data['query'][0]->grade_id;
					$schoolid=$data['query'][0]->sid;
					$section=$data['query'][0]->section;
					$org_id=$data['query'][0]->org_id;
					
						$arrgame=$this->Api_model->getMultiAssesGameList($gradeid,$org_id);
						$gamescore=$this->Api_model->getGameScore($userid,$org_id);
						
						$gameid=$arrgame[0]['ME'].",".$arrgame[0]['VP'].",".$arrgame[0]['FA'].",".$arrgame[0]['PS'].",".$arrgame[0]['LI'];
						
						$gamename=$arrgame[0]['MEgname'].",".$arrgame[0]['VPgname'].",".$arrgame[0]['FAgname'].",".$arrgame[0]['PSgname'].",".$arrgame[0]['LIgname'];
						
						$imgpath=$arrgame[0]['MEimgpath'].",".$arrgame[0]['VPimgpath'].",".$arrgame[0]['FAimgpath'].",".$arrgame[0]['PSimgpath'].",".$arrgame[0]['LIimgpath'];
						
						$gamepath=$arrgame[0]['MEgamehtml'].",".$arrgame[0]['VPgamehtml'].",".$arrgame[0]['FAgamehtml'].",".$arrgame[0]['PSgamehtml'].",".$arrgame[0]['LIgamehtml'];
						
						$skillid=$arrgame[0]['s1'].",".$arrgame[0]['s2'].",".$arrgame[0]['s3'].",".$arrgame[0]['s4'].",".$arrgame[0]['s5'];
						
						$skillname="Memory,Visual Processing,Focus and Attention,Problem Solving,Linguistics";
						if($gamescore[0]['ME']!=''){$me=$gamescore[0]['ME'];}else{$me='-1';}
						if($gamescore[0]['VP']!=''){$vp=$gamescore[0]['VP'];}else{$vp='-1';}
						if($gamescore[0]['FA']!=''){$fa=$gamescore[0]['FA'];}else{$fa='-1';}
						if($gamescore[0]['PS']!=''){$ps=$gamescore[0]['PS'];}else{$ps='-1';}
						if($gamescore[0]['LI']!=''){$li=$gamescore[0]['LI'];}else{$li='-1';}
						
						$gamescore=$me.",".$vp.",".$fa.",".$ps.",".$li;
						
						$resparr=array("status"=>"success","gid"=>$gameid,"gname"=>$gamename,"skill_id"=>$skillid,"skill_name"=>$skillname,"image_path"=>$imgpath,"total_play_times"=>'',"played_times"=>'',"tot_game_score"=>$gamescore,"org_id"=>$org_id,"imgbaseurl"=>base_url('assets/'),"GameFlag"=>"1","msg"=>"-");
						echo json_encode($resparr);					
				
			}
			else 
			{
				$resparr=array("status"=>"Failed","gid"=>"-","gname"=>"-","skill_id"=>"-","skill_name"=>"-","image_path"=>"-","total_play_times"=>"-","played_times"=>"-","tot_game_score"=>"-","org_id"=>"-","imgbaseurl"=>"-","GameFlag"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		
	}
	
	public function a2()
	{
		$username ='sundar6@gmail.com';
		$gameid ='289';
		$org_id ='1';
		
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
					if(isset($org_id) && $org_id!='')
					{
						$data['checkgame'] = $this->Api_model->checkagame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id,$org_id);
						if($data['checkgame'][0]['game_limit']<$data['checkgame'][0]['played_time'])
						{
							echo "<html><body style='width: 100%;text-align: center;margin: 0 auto;background: #0db1c1;'><div class='' style='left: 50%;top: 50%;transform: translate(-50%, -50%);width: 35%;height:10%;padding: 10%;background: #fff;color: #f30694;text-align: center;font-weight: bold;position: fixed;font-size: 25px;box-shadow: 5px 10px #0e545a;'><h2 style='padding: 0;margin: 0;'>Your Game Limit Expired</h2></div></body></html>";exit;
						}
					}
					else
					{
						$data['checkgame'] = $this->Api_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
					}				
					$gid = $data['checkgame'][0]['gameexist']; 
					if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
					 
					if($data['checkgame'][0]['game_limit']>$data['checkgame'][0]['played_time'])
					{
						if($gid>=1)
						{
							
							$this->session->set_userdata(array( 'currentgameid'=> $gameid ));
							$this->session->set_userdata(array( 'isskillkit'=> 'N' ));
							$this->session->set_userdata(array( 'isapp'=> 1 ));
							$this->session->set_userdata(array( 'app_userid'=>$username ));
							if(isset($org_id) && $org_id!='' && $org_id>0)
							{
								$this->session->set_userdata(array( 'currentorg_id'=>$org_id ));
								$this->session->set_userdata(array('isasap'=>1));
								$this->session->set_userdata(array( 'currentuid'=>$data['query'][0]->id));
							}
							
							if(($data['query'][0]->grade_id == 1) || ($data['query'][0]->grade_id==11) || ($data['query'][0]->grade_id==2))
							{
								$gamepath = '4';
							}
							else
							{
								$gamepath = 3;
							}
							
							redirect(base_url('assets/swf/'.$gamepath.'/'.$data['checkgame'][0]['gpath'].'.html'));
						}
						else
						{
							
							//echo 'IA'; exit;
						}
					}
					else
					{
						echo "<html><body style='width: 100%;text-align: center;margin: 0 auto;background: #0db1c1;'><div class='' style='left: 50%;top: 50%;transform: translate(-50%, -50%);width: 35%;height:10%;padding: 10%;background: #fff;color: #f30694;text-align: center;font-weight: bold;position: fixed;font-size: 25px;box-shadow: 5px 10px #0e545a;'><h2 style='padding: 0;margin: 0;'>Your Game Limit Expired</h2></div></body></html>";exit;
					}
	
				
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		
	}
	
	
/* ....................... Registration code Begins .......................*/
	public function statelist()
	{
		$arrStateList=$this->Api_model->getStateList('113');
		$resparr=array("status"=>"success","StateList"=>$arrStateList);
		echo json_encode($resparr);
	}
	public function gradelist()
	{
		$arrGradeList=$this->Api_model->gradelist();
		$resparr=array("status"=>"success","GradeId"=>$arrGradeList[0]['grade_id'],"GradeName"=>$arrGradeList[0]['grade_name']);
		echo json_encode($resparr);
	}
	public function ismobilenounique()
	{
		$mobileno = $this->input->post('txtMobileno');	
		if($mobileno!='')
		{
			$arrGradeList=$this->Api_model->IsMobilenounique($mobileno);
			$resparr=array("status"=>"success","uniquestatus"=>$arrGradeList[0]['isexist']);
		}
		else
		{
			$resparr=array("status"=>"success","msg"=>'Invalid Access');
		}
		echo json_encode($resparr);
	}
	public function isemailunique()
	{
		
		$isusername = $this->input->post('txtEmailid');	
		if($isusername!='')
		{
			$arrGradeList=$this->Api_model->IsEmailunique($isusername);
			$resparr=array("status"=>"success","uniquestatus"=>$arrGradeList[0]['isexist']);
		}
		else
		{
			$resparr=array("status"=>"success","msg"=>'Invalid Access');
		}
		echo json_encode($resparr);
	}
	public function userregistration()
	{
		$Mobileno = $this->input->post('txtMobileno');
		$hashpass = $this->salt_my_pass($Mobileno);
		 
		$shpassword = $hashpass['Hash']; 
		$salt1 = $hashpass['Salt1']; 
		$salt2 = $hashpass['Salt2']; 
		
		$saltedpass = $salt1 . $shpassword . $salt2; 

		$firstname = $this->input->post('txtName');
		$grade = $this->input->post('ddlGrade');
		$gender = $this->input->post('rdGender');
		$mobile = $this->input->post('txtMobileno');
		$email = $this->input->post('txtEmailid');
		$org_pwd = $this->input->post('txtMobileno');
		$couponcode = $this->input->post('txtcouponcode');
		
		if($email!='' && $Mobileno!='' && $couponcode!='')
		{
			$arrplan=$this->Api_model->getPlan($grade);
			
			$isuserexit = $this->Api_model->IsEmailunique($email);
			if($isuserexit[0]['isexist']==0)
			{
				$isvalid = $this->Api_model->IsCouponCodeValid($couponcode);
				if($isvalid[0]['iscouponvalid']==1)
				{
					$created_on=date("Y-m-d");
					$data['insertreg'] = $this->Api_model->addRegistration($firstname,$grade,$gender,$mobile,$email,$shpassword,$salt1,$salt2,$arrplan[0]['id'],$org_pwd,$couponcode,$created_on);
					$RegId = $data['insertreg'];
					
					$OTP=mt_rand(100000, 999999);
					$MSG="Dear ".$firstname.", OTP : ".$OTP." is to confirm your mobile no";
					$this->sendsms($mobile,$MSG);
					$this->Api_model->updateRegOtp($OTP,$RegId);
					 
					$arrResult=array("status"=>"success","RegId"=>$RegId,"OTP"=>$OTP,"msg"=>"-");
					echo json_encode($arrResult); exit;
				}
				else
				{
					$arrResult=array("status"=>"failure","RegId"=>"-","OTP"=>"-","msg"=>"Invalid Couponcode");
					echo json_encode($arrResult);exit;
				}
			}
			else
			{
				$arrResult=array("status"=>"failure","RegId"=>"-","OTP"=>"-","msg"=>"This email id is already registred.");
				echo json_encode($arrResult);exit;
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","RegId"=>"-","OTP"=>"-","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	public function paymentresponse()
	{
		$RegId =$this->input->post('RegId');
		if($RegId!='')
		{	
			$isvaliduser= $this->Api_model->checkIsValidUSer($RegId);
			
			if($isvaliduser[0]['isuser']==1)
			{				
				$arrregdetails= $this->Api_model->getresponsedetails($RegId);
				
				$subscriberid = $arrregdetails[0]['id'];
				$salt1 = $arrregdetails[0]['salt1'];
				$password = $arrregdetails[0]['password'];
				$salt2 = $arrregdetails[0]['salt2'];
				$email = $arrregdetails[0]['email'];				
				$gradeid = $arrregdetails[0]['gradeid'];
				$firstname = $arrregdetails[0]['firstname'];				
				$gender =$arrregdetails[0]['gender'];
				$mobile = $arrregdetails[0]['mobilenumber'];				
				$gameplanid = $arrregdetails[0]['gp_id'];
				$regcouponcode= $arrregdetails[0]['couponcode'];				
				$orgpwd= $arrregdetails[0]['orgpwd'];
				
				
				$validity_details = $this->Api_model->getDefaultPlandetails();
				
				if($validity_details[0]['isasapenabled']==1)
				{
					$portal_type='ASAP';
				}
				else
				{
					$portal_type='CLP';
				}
				$profileimage="assets/images/avatar.png";
				$data['subscribeusers'] = $this->Api_model->subscribeusers($subscriberid,$salt1,$salt2,$email,$password,$gameplanid,$gradeid,$firstname,$gender,$mobile,$regcouponcode,$orgpwd,$validity_details[0]['startdate'],$validity_details[0]['enddate'],$validity_details[0]['id'],$profileimage,$portal_type);
				$this->Api_model->UpdateCouponCount($regcouponcode);
				$this->Api_model->completestatus($RegId);

				// Email MSG for => Children 
				$RegSucSub="Registration Successful - Kinder AR";
				$RegSucEMSG='<table align="center" width="800px" border="1" cellspacing="0" cellpadding="0" style="font-size:medium;margin-right:auto;margin-left:auto;border:1px solid rgb(197,197,197);font-family:Arial,Helvetica,sans-serif;background-image:initial;background-repeat:initial;box-shadow: 10px 10px 5px #35395e;"><tbody><tr style="display:block;overflow:hidden;background: #1e88e5;"><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width:33%;color: #fff;"></td><td style="float:left;border:0px;text-align: center;padding: 5px 0px;width: 33%;color: #fff;"><img alt="www.summerchamps.com" src="'.base_url().'assets/images/new/SC-logo.png" style="float: left;width:220px;"></td><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width: 33%;color: #fff;"></td></tr><tr style="padding:0px;margin:10px 42px 20px;display:block;font-size:13px;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:justify"><td colspan="2" style="border:0px">Dear '.$firstname.',<br/><br/>Thank you for registering with <strong>Summer Champs</strong>.<br/><p><strong>Now !!</strong> you can unwrap the gift to enjoy & learn thinking skills</p><br/><br/>Following are the credentials to access the service at <a href="'.base_url().'" target="_blank" >'.base_url().'</a><br/><br/>username <strong> : '.$email.'</strong><br/>password<strong> : '.$orgpwd.'</strong><br/><br/><br/>All The Very Best!!!<br/><br/>Best Regards,<br/><strong>Summer Champs Team</strong><br/></td></tr><tr style=""><td style="text-align:center;color:#ee1b5b;border:0px;background-size:100%;background-image: url('.base_url().'assets/images/emailer/footer.jpg);padding-top:20px;padding-bottom:20px;font-family: cursive;font-size: 20px;"><div style="width:100%;font-family:; float:left;text-align:center"><a href="'.base_url().'" target="_blank" style="color:#ee1b5b;text-decoration: none;" >'.base_url().'</a><br/><a href="mailto:support@summerchamps.com"  style="color:#ee1b5b;text-decoration: none;" >support@summerchamps.com</a></div></td></tr><tr style="display:block;overflow:hidden"><td style="float:left;border:0px;"></td></tr></tbody></table>';

					// SMS MSG for => User 
					$RegSucSMSG="Dear ".$firstname.", you are successfully registered with Kinder AR. Username is ".$email." and password is ".$orgpwd.". Please visit ".base_url()." ";
				
					//$this->confirmation_email($email,$RegSucSub,$RegSucEMSG);//Mail Calling => User
					//$this->sendsms($mobile,$RegSucSMSG);//SMS Calling => Children

					$resparr=array("status"=>"success","msg"=>"User Registered successfully");
					echo json_encode($resparr);
				
			}
			else
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User. Please try again");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"Failed","msg"=>"Invalid access");
			echo json_encode($resparr);
		}
	}
	
	/*....... Leader Board ........*/
	public function org_leaderboard()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$data['minscore']=$this->Api_model->maxscore_byorg($data['query'][0]->org_id,$data['query'][0]->grade_id);
					$minscore = $data['minscore'][0]['val'];
					$topperlist=$this->Api_model->Topperlist_byorg($minscore,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					$resparr=array("status"=>"success","topperlist"=>$topperlist,"show_grade"=>$data['query'][0]->show_grade,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","topperlist"=>"-","show_grade"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","topperlist"=>"-","show_grade"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function arresult()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		
		$gamename=$this->input->post('gameID');		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($gamename!=''))
		{
			$data['query'] = $this->Api_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{
					$data['gameid'] = $this->Api_model->getgameid($gamename);
					$gameid = $data['gameid'][0]['gid'];
					if($gameid!='')
					{
						$data['checkgame'] = $this->Api_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);

						$game_limit=$this->info['default_play_time'];
						
						if($game_limit>$data['checkgame'][0]['played_time'])
						{ 
							$postdata = $_POST;
							$total_ques=$postdata["total_ques"];
							if($postdata["attempt_ques"]>10){
								$attempt_ques=10;
							}
							else{
								$attempt_ques=$postdata["attempt_ques"];
							}

							$answer=$postdata["answer"];
							$score=$postdata["gamescore"];
							$a6=$postdata["gtime1"];
							$a7=$postdata["rtime1"];
							$a8=$postdata["crtime1"];
							$a9=$postdata["wrtime1"];	
							
							$skillkit=0;
							$userlang = 1;
							$userid = $data['query'][0]->id; 
							$lastup_date = date("Y-m-d");
							$cid = 1;
							$data['gameDetails'] = $this->Api_model->getresultGameDetails($userid,$gameid);
							$skillid =$data['gameDetails'][0]['gameskillid'] ; 
							$schedule_val = 0;
							$pid =  $data['query'][0]->gp_id; 
							
							$data['insert1'] = $this->Api_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val);
						
							$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);

							/*--- Sparkies ----*/
							$sparkies_output=$this->Api_model->insertsparkies($arrofinput);
							//echo $sparkies_output[0]['OUTPOINTS'];exit; 
							
							$resparr=array("status"=>"success","crownypoints"=>$sparkies_output[0]['OUTPOINTS'],"response"=>1,"msg"=>"-");
							echo json_encode($resparr);
						}
						else
						{
							$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-1","msg"=>"Game limit expired");
							echo json_encode($resparr);
						}
					}
					else
					{
						$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-2","msg"=>"Game not avialable");
						echo json_encode($resparr);
					}
				}
				else 
				{
					$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{			
				$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr= array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
	}
	
	public function gamename()
	{
		$gradeid = $this->input->post('gradeid');
		if($gradeid!='')
		{
			$arrplan=$this->Api_model->getPlan($gradeid);
			$planid=$arrplan[0]['id'];
			
			$arrgamename=$this->Api_model->getGameNameOfGrade($gradeid,$planid);
			
			$resparr= array("status"=>"success","gamename"=>$arrgamename[0]['gamename'],"msg"=>"");
			echo json_encode($resparr);
		}
		else
		{
			$resparr= array("status"=>"failure","gamename"=>'-',"msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
		
	}
	public function gamenamenew()
	{
		$gradeid = $this->input->get('gradeid');
		if($gradeid!='')
		{
			$arrplan=$this->Api_model->getPlan($gradeid);
			$planid=$arrplan[0]['id'];
			
			$arrgamename=$this->Api_model->getGameNameOfGrade1($gradeid,$planid);
			
			$resparr= array("status"=>"success","gamename"=>$arrgamename[0]['gamename'],"msg"=>"");
			//echo json_encode($resparr);
			echo "<pre>";print_r($arrgamename);exit;
		}
		else
		{
			$resparr= array("status"=>"failure","gamename"=>'-',"msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
		
	}
}