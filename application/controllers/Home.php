<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Assessment_model');
				$this->load->library('session');			
				//$this->lang->load("menu_lang","french");		
        }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	public function testview()
	{
		echo date('Y-m-d H:i:s');exit;
	}
	public function termscheck()
	{
		//print_r($_POST); 
		
		if (!empty($_POST))
		{
			$data['query'] = $this->Assessment_model->checkUser($this->input->post('email'),$this->input->post('pwd'), $this->input->post('ddlLanguage'));
			
			echo $data['query'][0]->agreetermsandservice; exit;
			
		}
	}
	
	public function userlogin()
	{

		if (!empty($_POST))
		{ 
			 
			$data['query'] = $this->Assessment_model->checkUser($this->input->post('email'),$this->input->post('pwd'), $this->input->post('ddlLanguage'));
			 //echo "<pre>";print_r($data['query']);exit;
			if(isset($data['query'][0]->id))
			{
				/*....  Creating unique login ID for Checking Multiple loging ......*/
				$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);
				$this->Assessment_model->update_parent_loginDetails($data['query'][0]->id,$uniqueId);
		
				$login_count=$data['query'][0]->login_count;
				$prelogin_date=$data['query'][0]->pre_logindate;
				$fname=$data['query'][0]->name;
				$todaydate=date('Y-m-d');
				$login_date=$data['query'][0]->login_date;
				$start_ts = strtotime($login_date);
				$end_ts = strtotime($todaydate);
				$diff = $end_ts - $start_ts;
				$datediff = round($diff / 86400);	
				
				$startdate= strtotime($data['query'][0]->startdate); // or your date as well
				$now= strtotime(date('Y-m-d'));
				$datediff = $now - $startdate;
				$useddays=round($datediff / (60 * 60 * 24));				
			
				$this->session->set_userdata(array(
					'id'       => $data['query'][0]->id,
					'parentid'       => $data['query'][0]->id,
					'pname'      => $data['query'][0]->name,
					'pusername'      => $data['query'][0]->username,
					'login_count'      => $data['query'][0]->login_count,
					'pfullname'      => $data['query'][0]->name,
					'login_session_id'=>$uniqueId, 
					'useddays'=>$useddays,
					"portal_type"=>$data['query'][0]->portal_type
				));
				//echo "<pre>";print_r($_SESSION);exit;
				
					
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				 $ip=$_SERVER['HTTP_CLIENT_IP'];}
				 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
				 $ip=$_SERVER['REMOTE_ADDR'];}		
				$this->Assessment_model->insert_parent_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);

					
				echo $result=2; exit;
			
			}
		} 
	}
	public function childlogin()
	{
		if (!empty($_POST))
		{ 
			$data['query'] = $this->Assessment_model->checkChildisExist($this->input->post('childemail'),$this->input->post('childpwd'), $this->input->post('childddlLanguage'));
			
			if(isset($data['query'][0]->id))
			{			
				$echildid=$this->my_simple_crypt($data['query'][0]->id,'Ed6S@2018');
		
				$this->session->set_userdata(array(
					'cgp_id'       => $data['query'][0]->gp_id,
					'cid'       => $data['query'][0]->id,
					'ChildId'       => $data['query'][0]->id,
					'EChildId'       => $echildid,
					'cfname'      => $data['query'][0]->fname,
					'cusername'      => $data['query'][0]->username,
					'clogin_count'      => $data['query'][0]->login_count,
					'cfullname'      => $data['query'][0]->fname.' '.$data['query'][0]->lname,
					'cgame_grade'      => $data['query'][0]->grade_id,
					'cschool_id'      => $data['query'][0]->sid,
					'csection'      => $data['query'][0]->section,
					'cstartdate'=>$data['query'][0]->startdate,
					'cenddate'=>$data['query'][0]->enddate,
					'corg'=>$data['query'][0]->org_id
				));
				echo $echildid; exit;
			}
			echo 0;exit;
		}
			echo 0;exit;		
	}
	public function childlist()
	{
		if($this->session->parentid=="" || !isset($this->session->parentid)){redirect('index.php');}
		
		$data['ChildList']=$this->Assessment_model->getChildList($this->session->parentid);
		//echo "<pre>";print_r($data['ChildList']);exit;
		$data['ChildLimit']=$this->config->item('NoOfChild');
		$this->load->view('headerinner', $data);
        $this->load->view('child/childlist', $data);
		$this->load->view('footerinner');
	}
	public function childadd()
	{
		if($this->session->parentid=="" || !isset($this->session->parentid)){redirect('index.php');}
		
		$data['StateList'] = $this->Assessment_model->getStateList();
		$data['GradeList'] = $this->Assessment_model->gradelist();
		$data['SchooLlist'] = $this->Assessment_model->getSchooLlist();
		
		$this->load->view('headerinner', $data);
        $this->load->view('child/childadd', $data);
		$this->load->view('footerinner');
	}
	public function getCityList()
	{
		$State = $this->input->post('stateid');
		$CityList= $this->Assessment_model->getCityList($State);
		$select='<option value="">Select</option>';
		foreach($CityList as $list)
		{
			
			$select.='<option value="'.$list["id"].'">'.$list["city_name"].'</option>';			
		}
		echo $select;exit;
	}
	
	public function couponajax()
	{	
		$Name = $this->input->post('txtFName');
		$Grade = $this->input->post('ddlGrade');
		$Stage = $this->input->post('ddlStage');
		$Gender = $this->input->post('rdGender');
		$Address = $this->input->post('txtAddress');
		$ParentID = $this->session->parentid;
		$State = $this->input->post('ddlState');
		$City = $this->input->post('ddlCity');
		$Pincode = $this->input->post('txtPincode');
		$Schoolname = $this->input->post('txtSchoolname');
		$Couponcode = $this->input->post('txtCouponcode');
		$SpecialChild = $this->input->post('txtSpecialChild');
		$Bookneed='N';
		
		if($SpecialChild=='Y')
		{
			$Grade=$Stage;
		}
		else
		{
			$Grade=$Grade;
		}
		
		$hashpass = $this->salt_my_pass($this->info['default_pwd']);
		$orgpwd =$this->info['default_pwd'];
		 
		$shpassword = $hashpass['Hash']; 
		$salt1 = $hashpass['Salt1']; 
		$salt2 = $hashpass['Salt2']; 
		
		$saltedpass = $salt1 . $shpassword . $salt2; 
		
		if($Grade!='' && $ParentID!='')
		{		
			$arrcoupon = $this->Assessment_model->GetCouponCodeDetails($Couponcode);
			//echo "<pre>";print_r($arrcoupon);exit;
			if($arrcoupon[0]['iscouponvalid']==1)
			{
				if($arrcoupon[0]['coupon_valid_times']==0)
				{ // User can use many time
					$isallow =1;
				}
				else
				{ // Restricted Use
					$couponcheck=$this->Assessment_model->applycoupon($Couponcode);
					if($couponcheck[0]['iscouponvalid']==1)
					{
						$isallow =1;
					}
					else
					{
						$isallow =0;
					}
				}
				if($isallow==1)
				{
					$arrplan=$this->Assessment_model->getPlan($Grade);
					$noofchild = $this->Assessment_model->ChildLimitExpired($ParentID);
					$ChildLimit=$this->config->item('NoOfChild');
					if($ChildLimit > $noofchild[0]['noofchild'])
					{									
						$planamount = $arrcoupon[0]['price'];
						$discountperc =  $arrcoupon[0]['discount_percentage'];
						$discountamt=($planamount*$discountperc/100);
						$paidamount=$planamount-$discountamt;						
						if($paidamount==0)
						{
							$created_on=date("Y-m-d");
							$data['insertreg'] = $this->Assessment_model->addChildReg($Name,$arrplan[0]['id'],$Grade,$Gender,$Address,$ParentID,$shpassword,$salt1,$salt2,$created_on,$orgpwd,$State,$City,$Pincode,$Schoolname,$Couponcode,$Bookneed,$arrcoupon[0]['validity'],$arrcoupon[0]['orgid'],$SpecialChild);
							$ChildId = $data['insertreg'];
							$arrResult=array("status"=>"1","Validity"=>$arrcoupon[0]['validity'],"DiscountPercentage"=>$arrcoupon[0]['discount_percentage'],"PlanAmount"=>$arrcoupon[0]['price'],"DiscountAmount"=>$discountamt,"PayableAmount"=>$paidamount,"msg"=>"-","url"=>md5($ChildId));
							echo json_encode($arrResult); exit;
							//redirect("index.php/home/childregsuc?hdnPaymSubscribeID=".md5($ChildId).""); 	
						}
						else
						{
							$arrResult=array("status"=>"2","Validity"=>$arrcoupon[0]['validity'],"DiscountPercentage"=>$arrcoupon[0]['discount_percentage'],"PlanAmount"=>$arrcoupon[0]['price'],"DiscountAmount"=>$discountamt,"PayableAmount"=>$paidamount,"msg"=>"-");
							echo json_encode($arrResult); exit;
						}					
						
					}
					else
					{
						$arrResult=array("status"=>"failure","msg"=>"User limit was expired");
						echo json_encode($arrResult);exit;
					}
				}
				else
				{
					$arrResult=array("status"=>"failure","msg"=>"Couponcode limit was expired");
					echo json_encode($arrResult);exit;
				}
			}
			else
			{
				$arrResult=array("status"=>"failure","msg"=>"Invalid Couponcode");
				echo json_encode($arrResult);exit;
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	public function childreg()
	{	
		$Name = $this->input->post('txtFName');
		$Grade = $this->input->post('ddlGrade');
		$Gender = $this->input->post('rdGender');
		$Address = $this->input->post('txtAddress');
		$ParentID = $this->session->parentid;
		$State = $this->input->post('ddlState');
		$City = $this->input->post('ddlCity');
		$Pincode = $this->input->post('txtPincode');
		$Schoolname = $this->input->post('txtSchoolname');
		$Couponcode = $this->input->post('txtCouponcode');
		$SpecialChild = $this->input->post('txtSpecialChild');
		$Bookneed='N';
		
		if($SpecialChild=='Y')
		{
			$Grade='11';
		}
		else
		{
			$Grade=$Grade;
		}
		
		$hashpass = $this->salt_my_pass($this->info['default_pwd']);
		$orgpwd =$this->info['default_pwd'];
		 
		$shpassword = $hashpass['Hash']; 
		$salt1 = $hashpass['Salt1']; 
		$salt2 = $hashpass['Salt2']; 
		
		$saltedpass = $salt1 . $shpassword . $salt2; 
		
		if($Grade!='' && $ParentID!='')
		{		
			$arrcoupon = $this->Assessment_model->GetCouponCodeDetails($Couponcode);
			//echo "<pre>";print_r($arrcoupon);exit;
			if($arrcoupon[0]['iscouponvalid']==1)
			{
				if($arrcoupon[0]['coupon_valid_times']==0)
				{ // User can use many time
					$isallow =1;
				}
				else
				{ // Restricted Use
					$couponcheck=$this->Assessment_model->applycoupon($Couponcode);
					if($couponcheck[0]['iscouponvalid']==1)
					{
						$isallow =1;
					}
					else
					{
						$isallow =0;
					}
				}
				if($isallow==1)
				{
					$arrplan=$this->Assessment_model->getPlan($Grade);
					$noofchild = $this->Assessment_model->ChildLimitExpired($ParentID);
					$ChildLimit=$this->config->item('NoOfChild');
					if($ChildLimit > $noofchild[0]['noofchild'])
					{
											
						$planamount = $arrcoupon[0]['price'];
						$discountperc =  $arrcoupon[0]['discount_percentage'];
						$discountamt=($planamount*$discountperc/100);
						$paidamount=$planamount-$discountamt;	
						
						$created_on=date("Y-m-d");
						$data['insertreg'] = $this->Assessment_model->addChildReg($Name,$arrplan[0]['id'],$Grade,$Gender,$Address,$ParentID,$shpassword,$salt1,$salt2,$created_on,$orgpwd,$State,$City,$Pincode,$Schoolname,$Couponcode,$Bookneed,$arrcoupon[0]['validity'],$arrcoupon[0]['orgid'],$SpecialChild);
						$ChildId = $data['insertreg'];
						
						if($paidamount==0)
						{
							redirect("index.php/home/childregsuc?hdnPaymSubscribeID=".md5($ChildId).""); 	
						}
						else
						{
							redirect("Payment/ccavenue/credit_redirect.php?hdnPaymSubscribeID=".md5($ChildId).""); 
						}
					}
					else
					{
						$arrResult=array("status"=>"failure","msg"=>"User limit was expired");
						$data['msg']=$arrResult;
						$this->load->view('headerinner');
						$this->load->view('reg', $data);
						$this->load->view('footerinner');
					}
				}
				else
				{
					$arrResult=array("status"=>"failure","msg"=>"Couponcode limit was expired");
					echo json_encode($arrResult);exit;
				}
			}
			else
			{
				$arrResult=array("status"=>"failure","msg"=>"Invalid Couponcode");
				echo json_encode($arrResult);exit;
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	public function childregsuc()
	{
		$ChildID = $_REQUEST['hdnPaymSubscribeID']	;	
		if($ChildID!='')
		{
			$data['query'] = $this->Assessment_model->getInActiveChildIDDetails($ChildID);
			if(isset($data['query'][0]->id))
			{			
				$this->Assessment_model->updateChildConfirmUser($data['query'][0]->id,$data['query'][0]->grade_id,$data['query'][0]->gp_id);				
				$this->Assessment_model->UpdateCouponCount($data['query'][0]->couponcode);	
				
				/* $this->load->view('headerinner');
				$this->load->view('paymentresponse', $data);
				$this->load->view('footerinner'); */
				redirect('index.php/home/childlist');
			}
			else 
			{
				$resparr=array("status"=>"failure","ChildID"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$this->load->view('headerinner');
			$this->load->view('paymentfailure', $data);
			$this->load->view('footerinner');
		}
	}
	public function my_simple_crypt( $string, $action = 'Ed6S@2018' )
	{
		// you may change these values to your own
		$secret_key = 'my_simple_secret_key';
		$secret_iv = 'my_simple_secret_iv';
	 
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
	 
		if( $action == 'Ed6S@2018' ) {
			$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
		}
		else if( $action == 'KAR@2018' ){
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}
	 
		return $output;
	}
	public function dashboard()
	{	
		if(!isset($this->session->parentid) && !isset($this->session->ChildId)){$isvalid=0;}else{$isvalid=1;}
		if($isvalid==0){redirect('index.php');}
		
		if(isset($this->session->EChildId))
		{
			$cid=$this->session->EChildId;
		}
		else
		{
			$cid=$this->input->get('cid');
		}
		if(isset($cid) && $cid!='')
		{
			$ChildID = $this->my_simple_crypt($cid,'KAR@2018');
			
			$ChildDetails=$this->Assessment_model->getChildDetails($ChildID);

			if(isset($ChildDetails[0]['id']) && $ChildDetails[0]['id']!='')
			{
				
				$puzzle_cycle=$ChildDetails[0]['puzzle_cycle'];
				$org_id=$ChildDetails[0]['org_id'];
				$grade_id=$ChildDetails[0]['grade_id'];
				$gp_id=$ChildDetails[0]['gp_id'];
				$fname=$ChildDetails[0]['fname'];
				$sid=$ChildDetails[0]['sid'];
				$startdate=$ChildDetails[0]['startdate'];
				$enddate=$ChildDetails[0]['enddate'];
				$data['ChildDetails']=$ChildDetails;
				
					
					if(isset($this->session->ChildId))
					{ // Only for Child Session
						if(!isset($this->session->Child_login_session_id))
						{
							/* Login Details Update */			
								$uniqueId = $ChildID."".date("YmdHis")."".round(microtime(true) * 10);
								$this->Assessment_model->update_loginDetails($ChildID,$uniqueId);
								$data['uniqueId']=$uniqueId;
							/* Login Details Update */
							$this->session->set_userdata(array('Child_login_session_id'=> $uniqueId ));
							
							if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
							 $ip=$_SERVER['HTTP_CLIENT_IP'];}
							 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
							 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
							 $ip=$_SERVER['REMOTE_ADDR'];}		
							 $this->Assessment_model->insert_login_log($ChildID,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
						}
						else
						{
							$data['uniqueId']=$this->session->Child_login_session_id;
							$uniqueId=$this->session->Child_login_session_id;
						}
					}
					else
					{ // Parent login => Child Play
						/* Login Details Update */			
								$uniqueId = $ChildID."".date("YmdHis")."".round(microtime(true) * 10);
								$this->Assessment_model->update_loginDetails($ChildID,$uniqueId);
								$data['uniqueId']=$uniqueId;
						/* Login Details Update */
						if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
						 $ip=$_SERVER['HTTP_CLIENT_IP'];}
						 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
						 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
						 $ip=$_SERVER['REMOTE_ADDR'];}		
						 $this->Assessment_model->insert_login_log($ChildID,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
					}
				
				
				$exegameexist=$this->Assessment_model->checkGameExist($ChildID,$ChildDetails[0]['grade_id']);
				
				
				if($exegameexist[0]['isgameexist']==0)
				{
			
					// INsert 2 New Games from Group Table
					$arrgames=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,2);
					//echo "<pre>";print_r($arrgames);exit;
					if(!isset($arrgames[0]['gid']))
					{
						
						$puzzle_cycle=$puzzle_cycle+1;
						$arrgames=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,2);
						$this->Assessment_model->UpdateUserCycleStatus($ChildID,$puzzle_cycle);
					}
					foreach($arrgames as $arrgamedetails)
					{
						$arrgamedetails=$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails['groupid'],$puzzle_cycle,$arrgamedetails['gid']);
					}			
				}
				else if($exegameexist[0]['isgameexist']==1)
				{
			 
					$arrgamedetails=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,1);
					if(isset($arrgamedetails[0]['gid']))
					{ 				
						$arrgamedetails=$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails[0]['groupid'],$puzzle_cycle,$arrgamedetails[0]['gid']);
					}
					else
					{ // Next Cycle
						$puzzle_cycle=$puzzle_cycle+1;
						$arrgamedetails=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,1);
						$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails[0]['groupid'],$puzzle_cycle,$arrgamedetails[0]['gid']);
						$this->Assessment_model->UpdateUserCycleStatus($ChildID,$puzzle_cycle);
					}
				}
				else if($exegameexist[0]['isgameexist']==2)
				{	
					$gameid=explode(",",$exegameexist[0]['gameid']);
					//echo "<pre>";print_r($gameid);exit;
					foreach($gameid as $gid)
					{ 
						$arrgamedetails=$this->Assessment_model->getGameScoreofGame($ChildID,$puzzle_cycle,$gid);
						
						if($arrgamedetails[0]['TotalStar']>=$this->config->item('star_limit'))
						{						
							$arrgamedetails=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,1);
							if(isset($arrgamedetails[0]['gid']))
							{ // Cycle 1
								$arrgamedetails=$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails[0]['groupid'],$puzzle_cycle,$arrgamedetails[0]['gid']);
							}										
						}
						else
						{  // Same game will be display to Child // No Action Needed
							
						}
					}		
				}
				
				$data['actualGames']=$this->Assessment_model->getActualAssignedGames($ChildID,$puzzle_cycle);


				
				$UserBspi=$this->Assessment_model->getUserCurrentBspi($ChildID);
				if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!='')
				{
					$tot =$UserBspi[0]['bspi'];
				}
				else
				{	$tot=0 ;
				}
				//echo "<pre>"; print_r($data['actualGames']); exit;
					
				
				
				
				/* Expire time Validation */
				$maxtimofplay=$this->Assessment_model->getMaxTimeofPlay($ChildID);
				$sumoftottimeused=$this->Assessment_model->getSumofUserUsedTime($ChildID,date('Y-m-d'));
				$data['maxtimeofplay']=$maxtimofplay[0]['value'];
				$data['sumoftottimeused']=$sumoftottimeused[0]['LoggedIntime'];
				$data['Remainingtime']=$maxtimofplay[0]['value']-$sumoftottimeused[0]['LoggedIntime'];
				if($sumoftottimeused[0]['LoggedIntime']>=$maxtimofplay[0]['value'])
				{
					$this->TodayTimerInsert($ChildID);
				}
				/* Expire time Validation */

				/*--- Sparkies ----*/
				$CurDate=date("Y-m-d");
				$isdataexist=$this->Assessment_model->checkLoginCrownies($ChildID,$CurDate);
				
				if($isdataexist[0]['isgotCrownies']==0)
				{
					$arrofinput=array("inSID"=>$sid,"inGID"=>$grade_id,'inUID'=>$ChildID,'inScenarioCode'=>'LOGIN','inTotal_Ques'=>'','inAttempt_Ques'=>'','inAnswer'=>'','inGame_Score'=>'',"inPlanid"=>$gp_id,'inGameid'=>'');	
					$sparkies_output=$this->Assessment_model->insertsparkies($arrofinput);
					$finalpoints=$sparkies_output[0]['OUTPOINTS'];
					if($finalpoints!=''){$isexist=1;}else{$isexist=0;}
					$data['issparkies']=$isexist;	
					$data['sparkiespoints']=$finalpoints;
				}
				else
				{
					$data['issparkies']=0;	
					$data['sparkiespoints']=0;
				}
				
				/*--- Sparkies ----*/
			
				$data['TotalScore']=$tot;
				$data['sparkies']=$this->MySparkies($sid,$grade_id,$ChildID,$startdate,$enddate);
				//echo "<pre>";print_r($data);exit;
				$this->load->view('headerinner', $data);
				$this->load->view('home/dashboard', $data);
				$this->load->view('footerinner', $data);
			}
			else
			{
				redirect('index.php/home/profile');
			}
		}
		else
		{
			redirect('index.php/home/profile');
		}
	}
	public function dashboard_ajaxnew()
	{
		if(!isset($this->session->parentid) && !isset($this->session->ChildId)){$isvalid=0;}else{$isvalid=1;}
		
		if($isvalid==0){redirect('index.php');}
		
		$ChildID = $this->input->post('cid');
		
		$ChildDetails=$this->Assessment_model->getChildDetails($ChildID);
		
		$puzzle_cycle=$ChildDetails[0]['puzzle_cycle'];
		$org_id=$ChildDetails[0]['org_id'];
		$grade_id=$ChildDetails[0]['grade_id'];
		$gp_id=$ChildDetails[0]['gp_id'];
		$fname=$ChildDetails[0]['fname'];
		$sid=$ChildDetails[0]['sid'];
		$data['ChildDetails']=$ChildDetails;
		

		//echo "<pre>";print_r($_SESSION);exit;
		$exegameexist=$this->Assessment_model->checkGameExist($ChildID,$ChildDetails[0]['grade_id']);
		//echo "<pre>";print_r($exegameexist);exit;
		
		if($exegameexist[0]['isgameexist']==0)
		{	//echo "Loop 1"."<br/>";
			// INsert 2 New Games from Group Table
			$arrgames=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,2);
			//echo "<pre>";print_r($arrgames);exit;
			if(!isset($arrgames[0]['gid']))
			{
				$puzzle_cycle=$puzzle_cycle+1;
				$arrgames=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,2);
				$this->Assessment_model->UpdateUserCycleStatus($ChildID,$puzzle_cycle);
			}
			foreach($arrgames as $arrgamedetails)
			{
				$arrgamedetails=$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails['groupid'],$puzzle_cycle,$arrgamedetails['gid']);
			}			
		}
		else if($exegameexist[0]['isgameexist']==1)
		{	//echo "Loop 2"."<br/>";
			$arrgamedetails=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,1);
			if(isset($arrgamedetails[0]['gid']))
			{ 				
				$arrgamedetails=$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails[0]['groupid'],$puzzle_cycle,$arrgamedetails[0]['gid']);
			}
			else
			{ // Next Cycle
				$puzzle_cycle=$puzzle_cycle+1;
				$arrgamedetails=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,1);
				$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails[0]['groupid'],$puzzle_cycle,$arrgamedetails[0]['gid']);
				$this->Assessment_model->UpdateUserCycleStatus($ChildID,$puzzle_cycle);
			}
		}
		else if($exegameexist[0]['isgameexist']==2)
		{	//echo "Loop 3"."<br/>";
			$gameid=explode(",",$exegameexist[0]['gameid']);
			//echo "<pre>";print_r($gameid);exit;
			foreach($gameid as $gid)
			{ 
				$arrgamedetails=$this->Assessment_model->getGameScoreofGame($ChildID,$puzzle_cycle,$gid);
				if($arrgamedetails[0]['TotalStar']>=$this->config->item('star_limit'))
				{						
					$arrgamedetails=$this->Assessment_model->checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,1);
					if(isset($arrgamedetails[0]['gid']))
					{ // Cycle 1
						$arrgamedetails=$this->Assessment_model->InsertNewGame($ChildID,$ChildDetails[0]['grade_id'],$ChildDetails[0]['gp_id'],$ChildDetails[0]['sid'],$ChildDetails[0]['section'],$arrgamedetails[0]['groupid'],$puzzle_cycle,$arrgamedetails[0]['gid']);
					}										
				}
				else
				{  // Same game will be display to Child // No Action Needed
					
				}
			}		
		}
		
		$data['actualGames']=$this->Assessment_model->getActualAssignedGames($ChildID,$puzzle_cycle);	
		
		/* Expire time Validation */
			$maxtimofplay=$this->Assessment_model->getMaxTimeofPlay($ChildID);
			$sumoftottimeused=$this->Assessment_model->getSumofUserUsedTime($ChildID,date('Y-m-d'));
			$data['maxtimeofplay']=$maxtimofplay[0]['value'];
			$data['sumoftottimeused']=$sumoftottimeused[0]['LoggedIntime'];
			$data['Remainingtime']=$maxtimofplay[0]['value']-$sumoftottimeused[0]['LoggedIntime'];
			if($sumoftottimeused[0]['LoggedIntime']>=$maxtimofplay[0]['value'])
			{
				$this->TodayTimerInsert($ChildID);
			}
		/* Expire time Validation */
		
		$data['TotalScore']=$tot;
		$data['sparkies']=$this->MySparkies($sid,$grade_id,$ChildID,$startdate,$enddate);
		
		$this->load->view('home/dashboard_ajaxnew', $data);
	}
	public function MonthWiseScore()
	{
		$ChildID = $this->input->post('cid');
		$data['MonthWiseScore'] = $this->Assessment_model->getMonthWiseScore($ChildID);
		//echo "<pre>";print_r($data['MonthWiseScore'] );exit;
		$this->load->view('home/reportsview', $data);
		//echo $data['json'] = json_encode($data['MonthWiseScore']);	exit;
	}
	public function DailyStatus()
	{
		$ChildID = $this->input->post('cid');
		$data['DailyStatus'] = $this->Assessment_model->getDailyStatus($ChildID);
		//echo "<pre>";print_r($data['DailyStatus'] );exit;
		$this->load->view('home/dailystatus', $data);
		//echo $data['json'] = json_encode($data['MonthWiseScore']);	exit;
	}
	
	public function logout()
	{
		$this->load->view('headerinner');
		$this->load->view('logout/logout');
		$this->load->view('footerinner'); 
	}
	public function mainlogout()
	{
		if($this->session->parentid=="" || !isset($this->session->parentid)){redirect('index.php');}
		
		$this->Assessment_model->update_parent_logout_log($this->session->parentid,$this->session->login_session_id);
		
		$exeout = $this->Assessment_model->update_parent_loginstatus($this->session->parentid,$this->session->login_session_id);
		
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
	public function Childlogout()
	{
		$this->load->view('headerinner');
		$this->load->view('logout/childlogout');
		$this->load->view('footerinner'); 
	}
	public function ChildMainlogout()
	{
		if($this->session->ChildId=="" || !isset($this->session->ChildId)){redirect('index.php');}
		
		$this->Assessment_model->update_logout_log($this->session->ChildId,$this->session->Child_login_session_id);
		
		$exeout = $this->Assessment_model->updateuserloginstatus($this->session->ChildId,$this->session->Child_login_session_id);
		
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
	public function gamesajax()
	{
		$gameurl =  $_POST['gameurl']; 
		$gname = substr($gameurl, strrpos($gameurl, '/') + 1);
		$gamename = str_replace('.html','', $gname); 
		$userid =$_POST['cid'];
		$ChildDetails=$this->Assessment_model->getChildDetails($userid);
		
		$puzzle_cycle=$ChildDetails[0]['puzzle_cycle'];
		$org_id=$ChildDetails[0]['org_id'];
		$grade_id=$ChildDetails[0]['grade_id'];
		$gp_id=$ChildDetails[0]['gp_id'];
		$fname=$ChildDetails[0]['fname'];
		$sid=$ChildDetails[0]['sid'];
		$curdate=date('Y-m-d');
		
			$data['gameid'] = $this->Assessment_model->getgameid($gamename);
			$gameid = $data['gameid'][0]['gid']; 
			$data['checkgame'] = $this->Assessment_model->checkgame($gameid,$grade_id,$puzzle_cycle,$userid);
			$gid = $data['checkgame'][0]['gameid']; 
			
			$game_limt=$this->config->item('game_limit');
			//echo $game_limt."==".$data['checkgame'][0]['playedgamescount'];exit;
			if($gid>=1 && ($game_limt>$data['checkgame'][0]['playedgamescount']))
			{
				$this->session->set_userdata(array( 'currentgameid'=> $gameid ));
				$this->session->set_userdata(array( 'currentchildid'=> $userid ));
				$this->session->set_userdata(array( 'isskillkit'=> $_POST['skillkit'] ));
				echo $this->session->currentgameid;exit;
			}
			else
			{
				echo 'IA'; exit;
			}
	}
	public function result()
	{	

		if(!isset($this->session->parentid) && !isset($this->session->ChildId)){$isvalid=0;}else{$isvalid=1;}
		
		if($isvalid==0){redirect('index.php');}
		
		if(!isset($_POST)){redirect('index.php'); exit;}
		if(empty($_POST)){redirect('index.php'); exit;}
		
		$total_ques=$_POST["tqcnt1"];
		if($_POST["aqcnt1"]>10){
			$attempt_ques=10;
		}
		else{
			$attempt_ques=$_POST["aqcnt1"];
		}
		
		$answer=$_POST["cqcnt1"];
		$score=$_POST["gscore1"];
		$a6=$_POST["gtime1"];
		$a7=$_POST["rtime1"];
		$a8=$_POST["crtime1"];
		$a9=$_POST["wrtime1"];	
		if($answer==0)
		{
			$star=0;
		}
		else if($answer>=1 && $answer<=4)
		{
			$star=1;
		}
		else if($answer>=5 && $answer<=7)
		{
			$star=2;
		}
		else if($answer>=8 && $answer<=10)
		{
			$star=3;
		}
		
		
		
		$gameid=$this->session->currentgameid;
		$isskillkit=$this->session->isskillkit;
		if($isskillkit=="Y"){$skillkit=1;}
		else{$skillkit=0;}
		
		if($gameid==0)
		{
			echo '-2';exit;
		}
		
		
		$userid = $this->session->currentchildid; 
		$ChildDetails=$this->Assessment_model->getChildDetails($userid,$this->session->parentid);
		
		$org_id=$ChildDetails[0]['org_id'];
		$grade_id=$ChildDetails[0]['grade_id'];
		$gp_id=$ChildDetails[0]['gp_id'];
		$fname=$ChildDetails[0]['fname'];
		$sid=$ChildDetails[0]['sid'];
		
		$lastup_date = date("Y-m-d");
		$cid = 1;
		$data['gameDetails'] = $this->Assessment_model->getresultGameDetails($userid,$gameid,$grade_id);
		$skillid =$data['gameDetails'][0]['gameskillid'] ; 
		$pid =$gp_id; 
		
			 
		$puzzle_cycle=$data['gameDetails'][0]['puzzle_cycle'] ;  
		$PrevStar=$data['gameDetails'][0]['PrevStar'];  
		$TotalStar=$star+$PrevStar;
		
		
		
		$game_limt=$this->config->item('game_limit');
		if($game_limt>$data['gameDetails'][0]['playedgamescount'])
		{
			$data['insert1'] = $this->Assessment_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$star,$puzzle_cycle);
			if($TotalStar>=$this->config->item('star_limit'))
			{
				$this->Assessment_model->UpdateGameFinishedStatus($userid,$puzzle_cycle,$grade_id,$gameid);
			}
			$arrofinput=array("inSID"=>$sid,"inGID"=>$grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);
			/*--- Sparkies ----*/
			$sparkies_output=$this->Assessment_model->insertsparkies($arrofinput);
			$_REQUEST['gameoutput']=$sparkies_output[0]['OUTPOINTS'];
			echo $sparkies_output[0]['OUTPOINTS'];exit;
		}
		else
		{
			echo '-1';exit;
		}
	}
	
	public function profile()
	{		 
		if(!isset($this->session->parentid)){$isvalid=0;}else{$isvalid=1;}
		
		if($isvalid==0){redirect('index.php');}
		
		$userid = $this->session->parentid;	
		$data['myprofile'] = $this->Assessment_model->getmyprofile($userid);
		
		$this->load->view('headerinner', $data);
        $this->load->view('myprofile/newmyprofile', $data);
		$this->load->view('footerinner');
		
	}
	public function childprofile()
	{
		if(!isset($this->session->ChildId)){$isvalid=0;}else{$isvalid=1;}
		
		if($isvalid==0){redirect('index.php');}
				

		
		$ChildId = $this->session->ChildId;	
		$data['myprofile'] = $this->Assessment_model->getChildProfile($ChildId);
		
		$this->load->view('headerinner', $data);
        $this->load->view('myprofile/childprofile', $data);
		$this->load->view('footerinner');
		
	}
	
public function salt_my_pass($password)
{
// Generate two salts (both are numerical)
$salt1 = mt_rand(1000,9999999999);
$salt2 = mt_rand(100,999999999);

// Append our salts to the password
$salted_pass = $salt1 . $password . $salt2;

// Generate a salted hash
$pwdhash = sha1($salted_pass);

// Place into an array
$hash['Salt1'] = $salt1;
$hash['Salt2'] = $salt2;
$hash['Hash'] = $pwdhash;

// Return the hash and salts to whatever called our function
return $hash;

}
	
	/* public function myprofile()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
				
if(isset($_FILES["file"]["type"]))
{
	
$validextensions = array("jpeg", "jpg", "png");
$temporary = explode(".", $_FILES["file"]["name"]);
$file_extension = end($temporary);
 $size = getimagesize($files);
$maxWidth = 150;
$maxHeight = 150;
if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
) && in_array($file_extension, $validextensions)) {

$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
$targetPath = "assets/images/avatarimages/".$_FILES['file']['name']; // Target path where file is to be stored
//$targetPath = "upload/".$_FILES['file']['name'];
$this->session->set_userdata('avatarimage', $targetPath);
move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
//echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
//echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
//echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
//echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
//echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";

}
}

		$data['updatemsG']="";
		if($_REQUEST)
		{
				$sfname =  $this->input->post('sfname');
				$gender = $this->input->post('gender');
				$dob = $this->input->post('dob');
				$fathername = $this->input->post('fathername');
				$mothename = $this->input->post('mname');
				$address = $this->input->post('saddress');
				$emailid = $this->input->post('email');
				$sphoneno = $this->input->post('sphoneno');
				$id = $this->input->post('id');
				$newpass = $this->input->post('newpass');
				$confirmpass = $this->input->post('cpass');
			
				//$password = $_POST['txtOPassword'];
				 $hashpass = $this->salt_my_pass($newpass);
				 
				$shpassword = $hashpass['Hash']; 
				$salt1 = $hashpass['Salt1']; 
				$salt2 = $hashpass['Salt2']; 
				
				$saltedpass = $salt1 . $shpassword . $salt2;
				
				$ip=$this->getRealIpAddr();
				$userid = $this->session->user_id;
				$data['updateprofile'] = $this->Assessment_model->updateprofile($sfname,$gender,$emailid,$dob,$fathername,$mothename,$address,$sphoneno,$id,$shpassword,$salt1,$salt2,$confirmpass,$targetPath,$ip);
				$data['updatemsG']="Updated Successfully !!!";
				$this->session->set_flashdata('flag', '1');
				redirect("index.php/home/profile");
		}
		
		
		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$userid = $this->session->user_id;
		//$data['query'] = $this->Assessment_model->getleftbardata($userid);
		//$data['mytrophy'] = $this->Assessment_model->gettrophy($userid);
		$data['myprofile'] = $this->Assessment_model->getmyprofile($userid);
		$planid = $data['myprofile'][0]['gp_id'];
		$schoolid = $data['myprofile'][0]['sid'];
		$gradeid = $data['myprofile'][0]['grade_id']; 
		$data['getplandetails'] = $this->Assessment_model->getplandetais($planid);
		$data['getgrade'] = $this->Assessment_model->getgradedetais($gradeid);
		$data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
		$data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
		
		$data['sparkies']=$this->MySparkies();
		
		$this->load->view('headerinner', $data);
        $this->load->view('myprofile/myprofile', $data);
		$this->load->view('footerinner');
	
	} */
	public function mybrainprofile()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 
/* $res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/$this->config->item('bspi_division'); */

	
	if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
		$data['bspi'] =$UserBspi[0]['bspi'];
	}
	else{	$data['bspi']=0 ;
	}
	
	//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $this->session->astartdate;
	$enddate = $this->session->aenddate;
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	$data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
	$data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
	$data['sparkies']=$this->MySparkies();
		$this->load->view('headerinner', $data);
        $this->load->view('mybrainprofile/mybrainprofile', $data);
		$this->load->view('footerinner');
		
	}
	public function ajaxcalendar()
	{
				 

		$yearMonthQry=$_REQUEST['yearMonth'];
		$userid = $this->session->user_id;
		
	
	//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $this->session->astartdate;
	$enddate = $this->session->aenddate;
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	$bspicalendardays= $this->Assessment_model->mybspicalendar($this->session->school_id,$userid,$yearMonthQry,$startdate,$enddate);
	$mybspiCalendar=array();	
	foreach($bspicalendardays as $days)
	{
		$mybspiCalendar[$days['playedDate']]=$days['game_score'];
	}
	 $data['mybspiCalendar']=$mybspiCalendar;
	  
	$data['yearMonthQry']=$yearMonthQry;
	
		 
        $this->load->view('mybrainprofile/ajaxcalendar', $data);
 
		
	}
	public function ajaxcalendarSkillChart()
	{
				 

		$yearMonthQry=$_REQUEST['yearMonth'];
		$userid = $this->session->user_id;
		
 
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart("",$userid,$yearMonthQry);
  
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{
		$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);
	}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
	  
	$data['yearMonthQry']=$yearMonthQry;
	
		 
        $this->load->view('mybrainprofile/ajaxcalendarSkillChart', $data);
 
		
	}
	public function mytrophies()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 
/* $res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/$this->config->item('bspi_division'); */

if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
		$data['bspi'] =$UserBspi[0]['bspi'];
	}
	else{	$data['bspi']=0 ;
	}
	//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $this->session->astartdate;
	$enddate = $this->session->aenddate;
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$mytrophies= $this->Assessment_model->myTrophiesAll($userid,$startdate,$enddate);
	$arrmyTrophies=array();
	 foreach($mytrophies as $trophies)
	 {
		 $arrmyTrophies[str_pad($trophies['month'], 2, '0', STR_PAD_LEFT)][$trophies['category']]=$trophies['totstar'];
	 }
	 $data['arrmyTrophies'] =$arrmyTrophies;
	 $data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
	 $data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
	 
$data['sparkies']=$this->MySparkies();	 
		$this->load->view('headerinner', $data);
        $this->load->view('mytrophies/mytrophies', $data);
		$this->load->view('footerinner');
		
	}
	
	public function skillkit()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		 $this->fn_SK_Rand_games($userid, $check_date_time, '',100,$catid);
		$brain_rs = $this->Assessment_model->getAssignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$userid,$this->session->school_id);
		$where = "";
		$brainIds='';
		$data['actualGames']='';
			foreach( $brain_rs as $brainData)
			{
					$brainIds[] = $brainData['gid'];
			}
			if($brainIds)
			{			
					$active_game_ids = @implode(',',$brainIds);
					$where = " and g.ID in ($active_game_ids)";

			}
	if($brainIds)
			{				
			$data['actualGames'] = $this->Assessment_model->getSK_ActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
			}
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 
/* $res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/$this->config->item('bspi_division'); */
if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
	$data['bspi'] =$UserBspi[0]['bspi'];
}
else{	$data['bspi']=0 ;
}
	$data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
	$data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
	

	$data['sparkies']=$this->MySparkies();
		$this->load->view('headerinner', $data);
        $this->load->view('home/skillkit', $data);
		$this->load->view('footerinner');
	
	//redirect('index.php/home/dashboard');
	
	}
	public function fn_SK_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid) {
//echo "fn_Rand_games";

$arrSkills=$this->Assessment_model->getSK_SkillsRandom($uid);
$arrlevelid=explode(',',$arrSkills[0]['levelid']); //echo "<pre>";print_r($arrSkills);exit;
$vv=0;
	  foreach($arrSkills as $gs_data)
	  { 	
			$rand_sel = $this->Assessment_model->assignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
		  
			$cur_day_skills = count($rand_sel);
			
			$rand_sel_limit = $this->Assessment_model->assignSK_assignGameCount($this->session->game_plan_id,$gs_data['skill_id'],$this->session->school_id,$arrlevelid[$vv]);
			$assign_count = $rand_sel_limit[0]['gameCount']; 

//echo "<pre>";print_r($assign_count);exit;
if($cur_day_skills<$assign_count){
	$rand_sel_rs=$this->Assessment_model->getSK_randomGames($this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$assign_count,$cur_day_skills,$arrlevelid[$vv]);
 
 

$rand_count = count($rand_sel_rs);	

if($rand_count <=0) {
					$del_where = "";
					if($assign_count <> $cur_day_skills && $cur_day_skills > 0)
						$del_where = " and created_date = '$check_date_time'";
					 $this->Assessment_model->deleteSK_RandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$del_where);
} 
				 
				if($rand_count > 0) {
					foreach($rand_sel_rs as $rand_data1) { 
					$rand_data = $this->Assessment_model->insertSK_RandomGames($catid,$rand_data1['plan_ID'],$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$this->session->section,$rand_data1['gid'],$check_date_time);
					}
					 
				}
	  }
			$rand_sel = $this->Assessment_model->assignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
			$cur_day_skills = count($rand_sel);	
			if($cur_day_skills == 0)
			{				  
					$this->fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count);
			}
			else if($cur_day_skills <$assign_count)
			{	  
			   $min_date_arr = $this->Assessment_model->getSK_mindatePlay($this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id ,$catid);
				$this->Assessment_model->deleteSK_OldGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$min_date_arr[0]['mindate']);
				
				$this->fn_SK_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,1);	
			}	  
	  $vv++;
}
	
	 
}

public function fetchnewsfeed()
{   
	$pageno=$this->input->post('page');
	$type=$this->input->post('type');
	//echo $pageno."==".$type;exit;
	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
	
	$data['newsfeed']=$this->Assessment_model->getNewsFeed($this->session->school_id,$this->session->game_grade,$this->session->user_id,$type,$pageno,$this->session->astartdate,$this->session->aenddate);
	$count_of_tot_record=count($data['newsfeed']);
	$htmldata='';
	if($type=='ALL'){
	$htmldata.='<input type="hidden" value="'.$count_of_tot_record.'" id="hdnnewsfeedall" />';
	}else{
	$htmldata.='<input type="hidden" value="'.$count_of_tot_record.'" id="hdnnewsfeedmine" />';
	}
	if(count($data['newsfeed'])>0){
	foreach($data['newsfeed'] as $row)
	{ if($row['avatarimage']!=''){$src=base_url()."".$row['avatarimage'];}else{$src=base_url()."assets/images/".'avatar.png';}
	  /* if (!file_get_contents(base_url().$row['avatarimage'], 0, NULL, 0, 1)) {
             $src=base_url()."assets/images/".'avatar.png';
	} */
		//echo "<pre>";print_r($row);exit;
		$htmldata.='<li style="" class="news-item">
		<table cellpadding="4">
		<tbody><tr>
		<td><img src="'.$src.'" width="40" height="40" class="img-circle"></td>
		<td>'.$row["scenario"].'</td>
		</tr>
		</tbody></table>
		</li>';
	}
	}
	else
	{	if($this->session->avatarimage!=''){$src=base_url()."".$this->session->avatarimage;}else{$src=base_url()."assets/images/".'avatar.png';}
		$htmldata.='<li style="" class="news-item">
		<table cellpadding="4">
		<tbody><tr> 
		<td><img src="'.$src.'" width="40" height="40" class="img-circle"></td>
		<td>Welcome '.$this->session->fname.', Have a great day ! Keep playing puzzles and improve your skills !!</td>
		</tr>
		</tbody></table>
		</li>';
	}
	echo $htmldata;exit;
	
}
public function fetchnewsfeedcount()
{
	$type=$this->input->post('type');
	//echo $pageno."==".$type;exit;
	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
	$newsfeedcount=$this->Assessment_model->getNewsFeedCount($this->session->school_id,$this->session->game_grade,$this->session->user_id,$type);
	echo json_encode($newsfeedcount[0]);exit;
}
public function bee_smart()
{
	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}	
	$inUID = $this->session->user_id;
	$inDateTime = date('Y-m-d');	
	$data['randomGames']=$this->Assessment_model->bee_smart($this->session->school_id,$this->session->game_grade,$inUID,$inPoints,$inType,$inScenarioID,$inDateTime);	
	
}


/* public function dashboard_ajax()
{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		$data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
		$data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
		$data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		
		$data['assignGames']=$this->Assessment_model->getAssignGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid);
	
		
		$cur_day_skills = count($data['randomGames']);
		$assign_count = count($data['assignGames']);
		if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) {
				$this->fn_Rand_games($userid, $check_date_time, $cur_day_skills, $assign_count,$catid);
			}
		$data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$where = "";
		foreach($data['randomGames'] as $brainData )
		{
			$brainIds[] = $brainData['gid'];
			$active_game_ids = @implode(',',$brainIds);
			$where = " and g.gid in ($active_game_ids)";
		}
			 
		$data['actualGames'] = $this->Assessment_model->getActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 

	$data['PlayedSkillCount']=$this->Assessment_model->getPlayedSkillCount($userid);
	
if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
$data['bspi'] =$UserBspi[0]['bspi'];
}
else{	$data['bspi']=0 ;
}
 
	$restrainCalendar= $this->Assessment_model->getTrainCalendar($userid,date('Y-m-01'),date('Y-m-d'),$catid);
	$data['arrtrainCalendar']=explode(',',$restrainCalendar[0]['updateDates']);
	$data['trophies']=array();
	$data['trophies']= $this->Assessment_model->getMyCurrentTrophies($userid);

	
	$this->load->view('home/dashboard_ajax', $data);

	
	} */
public function MyCurrenttrophies()
{
	$userid = $this->session->user_id;
	$data['trophies']=array();
	$data['trophies']= $this->Assessment_model->getMyCurrentTrophies($userid);

	$this->load->view('home/dashboard_mytrophies', $data);
}
public function MySkillPie()
{	$userid = $this->session->user_id;
	$data['PlayedSkillCount']=$this->Assessment_model->getPlayedSkillCount($userid);
	$this->load->view('home/dashboard_myskillpie', $data);
}
/* public function dashboard_ajaxnew()
{
	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		$data['PlayedSkillCount']=$this->Assessment_model->getPlayedSkillCount($userid);
		$data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$data['assignGames']=$this->Assessment_model->getAssignGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid);
		$cur_day_skills = count($data['randomGames']);
		$assign_count = count($data['assignGames']);
		if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) {
				$this->fn_Rand_games($userid, $check_date_time, $cur_day_skills, $assign_count,$catid);
			}
		$data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$where = "";
		foreach($data['randomGames'] as $brainData )
		{
			$brainIds[] = $brainData['gid'];
			$active_game_ids = @implode(',',$brainIds);
			$where = " and g.gid in ($active_game_ids)";
		}
			 
		$data['actualGames'] = $this->Assessment_model->getActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		
	
	$this->load->view('home/dashboard_ajaxnew', $data);
	
} */
public function leaderboard()
{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		$userid = $this->session->user_id;
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 
		/* $res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
		$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;

	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
		if($res_tot_memory_i==0){$res_tot_memory_i=1;}
		if($res_tot_vp_i==0){$res_tot_vp_i=1;}
		if($res_tot_fa_i==0){$res_tot_fa_i=1;}
		if($res_tot_ps_i==0){$res_tot_ps_i=1;}
		if($res_tot_lang_i==0){$res_tot_lang_i=1;}
		$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/$this->config->item('bspi_division'); */
if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
	$data['bspi'] =$UserBspi[0]['bspi'];
}
else{	$data['bspi']=0 ;
}
		
		//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
		$startdate = $this->session->astartdate;
		$enddate = $this->session->aenddate;
		$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate); 

		/* $getTopSparkiesValue=$this->Assessment_model->getTopSparkiesValue($startdate,$enddate,$this->session->school_id,$this->session->game_grade);
		 //if(34696==$userid){echo "lll"; exit;}
		$getTopPlayedGames=$this->Assessment_model->getTopPlayedGames($startdate,$enddate,$this->session->school_id,$this->session->game_grade);
		
		$getTopBSPIScore=$this->Assessment_model->getTopBSPIScore($startdate,$enddate,$this->session->school_id,$this->session->game_grade);
 
		$getSuperAngels=$this->Assessment_model->getSuperAngels($startdate,$enddate,$this->session->school_id,$this->session->game_grade);
		//echo "<pre>";print_r($data['getSuperAngels']);exit;
		
		$superbrain=array();
		foreach($getTopBSPIScore as $row)
		{
			$superbrain[$row['monthNumber']]=$row['username'];
		}
		$data['superbrain']=$superbrain;
		//echo "<pre>";print_r($superbrain);exit;
		$Sparkies=array();
		foreach($getTopSparkiesValue as $row)
		{
			$Sparkies[$row['monthNumber']]=$row['username'];
		}
		$data['Sparkies']=$Sparkies;
		
		$supergoer=array();
		foreach($getTopPlayedGames as $row)
		{
			$supergoer[$row['monthNumber']]=$row['username'];
		}
		$data['SuperGoer']=$supergoer;
		
		
		$superagels=array();
		foreach($getSuperAngels as $row)
		{
			$superagels[$row['monthNumber']]=$row['username'];
		}
		$data['getSuperAngels']=$superagels; */

/*-------------------- New Leader Board Code Modification --------------------*/
$arrCrownyBadge=$this->Assessment_model->getTopBadge($this->session->school_id,$this->session->game_grade,'CB');//Crownies-Badge
$arrSuperAngelBadge=$this->Assessment_model->getTopBadge($this->session->school_id,$this->session->game_grade,'SAB');//Super Angel-Badge
$arrSuperBrainBadge=$this->Assessment_model->getTopBadge
($this->session->school_id,$this->session->game_grade,'SBB');//Super Brain-Badge
$arrSuperGoerBadge=$this->Assessment_model->getTopBadge
($this->session->school_id,$this->session->game_grade,'SGB');//Super Goer-Badge

$CrownyBadge=array();
foreach($arrCrownyBadge as $row)
{
	$CrownyBadge[$row['monthnumber'].'-'.$row['monofyear']]=$row['username'];
	
}
$data['CrownyBadge']=$CrownyBadge;
$SuperAngelBadge=array();
foreach($arrSuperAngelBadge as $row)
{
	$SuperAngelBadge[$row['monthnumber'].'-'.$row['monofyear']]=$row['username'];
}
$data['SuperAngelBadge']=$SuperAngelBadge;
$SuperBrainBadge=array();
foreach($arrSuperBrainBadge as $row)
{
	$SuperBrainBadge[$row['monthnumber'].'-'.$row['monofyear']]=$row['username'];
}
$data['SuperBrainBadge']=$SuperBrainBadge;
$SuperGoerBadge=array();
foreach($arrSuperGoerBadge as $row)
{
	$SuperGoerBadge[$row['monthnumber'].'-'.$row['monofyear']]=$row['username'];
}
$data['SuperGoerBadge']=$SuperGoerBadge;
/*-------------------- New Leader Board Code Modification --------------------*/

	$data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
	$data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
	$data['sparkies']=$this->MySparkies();	
	$this->load->view('headerinner', $data);
	$this->load->view('leaderboard/leaderboard', $data);
	$this->load->view('footerinner');

}
	/* Hari */
	 public function mybadges()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 
/* $res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/$this->config->item('bspi_division'); */

if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
	$data['bspi'] =$UserBspi[0]['bspi'];
}
else{	$data['bspi']=0 ;
}
	
	//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $this->session->astartdate;
	$enddate = $this->session->aenddate;
	$gradeid = $this->session->game_grade;
	$schoolid = $this->session->school_id;
	
	$data['superbrainb'] = $this->Assessment_model->getbadgeone($startdate,$enddate,$gradeid,$userid,$schoolid);
	$data['supergoerb'] = $this->Assessment_model->getbadgetwo($startdate,$enddate,$gradeid,$userid,$schoolid);
	$data['superangelb'] = $this->Assessment_model->getbadgethree($startdate,$enddate,$gradeid,$userid,$schoolid);
	 
	 $data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
	 $data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
	$data['sparkies']=$this->MySparkies();	 
		$this->load->view('headerinner', $data);
        $this->load->view('mybadges/mybadges', $data);
		$this->load->view('footerinner');
		
	}
	
	 public function mythemes()
	{
	
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		//$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
		$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
		 
		/* $res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
		$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
		if($res_tot_memory_i==0){$res_tot_memory_i=1;}
		if($res_tot_vp_i==0){$res_tot_vp_i=1;}
		if($res_tot_fa_i==0){$res_tot_fa_i=1;}
		if($res_tot_ps_i==0){$res_tot_ps_i=1;}
		if($res_tot_lang_i==0){$res_tot_lang_i=1;}
		$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/$this->config->item('bspi_division'); */

if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
	$data['bspi'] =$UserBspi[0]['bspi'];
}
else{	$data['bspi']=0 ;
}
		//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
		$startdate = $this->session->astartdate;
		$enddate = $this->session->aenddate;
		$gradeid = $this->session->game_grade;

	
		$data['themeslist'] = 	$this->Assessment_model->getthemefile();
	
		$data['userthemefile'] = $this->Assessment_model->get_user_themefile($userid);
		$data['skillkitenable']=$this->Assessment_model->IsSkillkitExist($this->session->user_id,$this->session->game_plan_id);
		$data['sparkies']=$this->MySparkies();	
		$this->load->view('headerinner', $data);
        $this->load->view('mythemes/mythemes', $data);
		$this->load->view('footerinner');
		
	}	
	public function updatethemefile()
	{
		$filename = $_POST['filename'];
		$userid = $this->session->user_id;
		
		$changetheme=$this->Assessment_model->updatethemefile($filename,$userid);	
	} 
	public function getOverAllTopperList()
	{
		$type=$this->input->post('type');
		//$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
		$startdate = $this->session->astartdate;
		$enddate = $this->session->aenddate;
		//$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
		
		//echo "<pre>";print_r($getTopSparkiesValue);exit;
		if($type=='BSPI')
		{	
			$getTopBSPIScore=$this->Assessment_model->getOverallBspiToppernew($startdate,$enddate,$this->session->school_id,$this->session->game_grade);
			if(COUNT($getTopBSPIScore)>0)
			{
				foreach($getTopBSPIScore as $row1)
				{			
					$arrsch=explode(',',$row1['school_name']);
					$arruser=explode(',',$row1['username']);
					//echo "<pre>";print_r($arrsch);echo "<br/>";
					if(count($arrsch)>1)
					{
						for($i=0;$i<count($arrsch);$i++)
						{
							$data1.="<li class='news-item'><div class='main'>".$row1['classname']." - ".$arrsch[$i]." - <span style='color:green'>".ROUND($row1['bspi'],2)."</span></div><div class='sub'>".$arruser[$i]."</div></li>";
						}
					}
					else
					{
							$data1.="<li class='news-item'><div class='main'>".$row1['classname']." - ".$row1['school_name']." - <span style='color:green'>".ROUND($row1['bspi'],2)."</span></div><div class='sub'>".str_replace(',',', ',$row1['username'])."</div></li>";
					}
				}
			}
			else
			{	$data1.="<li class='news-item'><div class='norecord'>No record found</div></li>";
			}
			
		}
		else
		{
			$getTopSparkiesValue=$this->Assessment_model->getOverallSparkyToppernew($startdate,$enddate,$this->session->school_id,$this->session->game_grade);
			if(COUNT($getTopSparkiesValue)>0)
			{
				foreach($getTopSparkiesValue as $row)
				{				
					$arrsch=explode(',',$row['school_name']);
					$arruser=explode(',',$row['username']);
					//echo "<pre>";print_r($arrsch);echo "<br/>";
					if(count($arrsch)>1)
					{
						for($i=0;$i<count($arrsch);$i++)
						{ //echo $arrsch[$i];
							$data1.="<li class='news-item'><div class='main'>".$row['classname']." - ".$arrsch[$i]." - <span style='color:green'>".ROUND($row['points'],0)."</span></div><div class='sub'>".$arruser[$i]."</div></li>";
						}
					}
					else
					{
						$data1.="<li class='news-item'><div class='main'>".$row['classname']." - ".$row['school_name']." - <span style='color:green'>".ROUND($row['points'],0)."</span></div><div class='sub'>".str_replace(',',', ',$row['username'])."</div></li>";
					}
				}
			}
			else
			{	$data1.="<li class='news-item'><div class='norecord'>No record found</div></li>";
			}
			
		}
		
		
		echo $data1;exit;
	}
	public function MySparkies($sid,$grade_id,$childid,$startdate,$enddate)
	{
		$sparkies_total_points=$this->Assessment_model->getMyCurrentSparkies($sid,$grade_id,$childid,$startdate,$enddate); 
		$sparkiestotpoints=$sparkies_total_points[0]['mysparkies'];
		return $sparkiestotpoints;		
	}
	
	public function islogin()
	{ 
		if (!empty($_POST))
		{			 
			$exeislogin = $this->Assessment_model->islogin($this->input->post('email'),$this->input->post('pwd'),$this->config->item('ideal_time'));
			echo $exeislogin[0]->islogin;exit;
		}
	}
	public function ischildlogin()
	{ 
		if (!empty($_POST))
		{			 
			$exeislogin = $this->Assessment_model->ischildlogin($this->input->post('childemail'),$this->input->post('childpwd'),$this->config->item('ideal_time'));
			echo $exeislogin[0]->islogin;exit;
		}
	}
	public function checkuserisactive()
	{
		$cid=$_POST['cid'];
		$sessionid=$_POST['sessionid'];
		//$this->Assessment_model->update_logout_log($cid,$sessionid);
		
		$this->Assessment_model->update_login_log($cid,$sessionid);
		$exeislogin = $this->Assessment_model->checkuserisactive($cid,$sessionid);
		//echo $exeislogin[0]->isalive;exit;
		if($exeislogin[0]->isalive!=1)
		{ 	
			$this->session->unset_userdata();
			$this->session->sess_destroy();
			echo 1;exit;
		}
		else
		{
			echo 0;exit;
		}
	}
	
	public function checkbandwidthisexist()
	{
		$exeislogin = $this->Assessment_model->checkbandwidthisexist($this->session->school_id);
		echo $exeislogin[0]['isexist'];exit;
	}
	public function insertbandwidth()
	{	$Bps=$_POST['Bps'];
		$Kbps=$_POST['Kbps'];
		$Mbps=$_POST['Mbps'];
		$exeislogin = $this->Assessment_model->insertbandwidth($this->session->school_id,$this->session->user_id,$Bps,$Kbps,$Mbps);
	}	
	public function termsofservice()
	{
  $this->load->view('header');
  $this->load->view('footerpages/termsofservice');
  $this->load->view('footer');
	}
	
	public function privacypolicy()
	{
  $this->load->view('header');
  $this->load->view('footerpages/privacypolicy');
  $this->load->view('footer');
	}
	
	public function faq()
	{
  $this->load->view('header');
  $this->load->view('footerpages/faq');
  $this->load->view('footer');
	}
public function mycurrentbspi()
{
	$userid =$this->input->post('cid');
	$UserBspi=$this->Assessment_model->getUserCurrentBspi($userid);
	if(isset($UserBspi[0]['bspi']) && $UserBspi[0]['bspi']!=''){
		$tot =$UserBspi[0]['bspi'];
	}
	else{	$tot=0 ;
	}
	echo $tot;exit;
}

public function isUser()
{
	/*
	isUser	   =0 Is not a valid user
	isUser	   =1 valid user	
	isschedule =0 Non Schedule day 
	isschedule =1 Schedule day 
	userstatus =0 User confirmation status=No
	userstatus >0 or = 1 User confirmation status=Yes
	popup      =1 Open Popup
	popup      =0 Don't Popup.Call userlogin() function
	*/
	
	if (!empty($_POST))
	{ 
	$arrisUser= $this->Assessment_model->CheckisUser($this->input->post('email'),$this->input->post('pwd'));
	//$arrisUser= $this->Assessment_model->CheckisUser('sundar','skillangels');
	$arrisschedule= $this->Assessment_model->CheckisScheduleday($arrisUser[0]['gradename'],$arrisUser[0]['section'],$arrisUser[0]['sid']);
	
	if($arrisschedule[0]['isschedule']!=0)
	{ // Schedule Day
		$arruserstatus= $this->Assessment_model->CheckUserStatus($arrisUser[0]['id']);
		if($arruserstatus[0]['userstatus']==0)
		{
			$popup=1;
		}
		else
		{
			$popup=0;
		}
	}
	else
	{  // Non Schedule Day
		$popup=1;
	}

			$response=array(
				'isUser'=>$arrisUser[0]['isUser'],
				'Username'=>$arrisUser[0]['username'],
				'gradename'=>$arrisUser[0]['gradename'],
				'grade_id'=>$arrisUser[0]['grade_id'],
				'id'=>$arrisUser[0]['id'],
				'sid'=>$arrisUser[0]['sid'],
				'section'=>$arrisUser[0]['section'],
				'isschedule'=>$arrisschedule[0]['isschedule'],
				'portal_type'=>$arrisUser[0]['portal_type'],
				'popup'=>$popup
			);
			//echo "<pre>";print_r($response);
			echo json_encode($response);exit;
			//echo "<pre>";print_r($arrisschedule);
		
	} 
}
public function insertuserlog()
{
	/*
	isschedule =0 Non Schedule day 
	isschedule =1 Schedule day 
	*/
	$userid=$_POST['userid'];
	$isschedule=$_POST['isschedule'];
	$type=$_POST['type'];
	if($isschedule==0)
	{	$Login_type='2';//NSTL
	}
	else
	{	$Login_type='1';//FTL
	}
	if($type=='Y')
	{	$Confirmation_type='1';//YES
	}
	else
	{	$Confirmation_type='0';//NO
	}

	$arruserstatus= $this->Assessment_model->insertuserlog($userid,$Login_type,$Confirmation_type);
	echo $arruserstatus;exit;
}
public function fetchrelateduser()
{
	$userid=$_POST['userid'];
	$username=$_POST['username'];
	$grade_id=$_POST['grade_id'];
	$section=$_POST['section'];
	$sid=$_POST['sid'];
	$isschedule=$_POST['isschedule'];
	$type=$_POST['type'];
	if($isschedule==0)
	{	$Login_type='2';//NSTL
	}
	else
	{	$Login_type='1';//FTL
	}
	if($type=='Y')
	{	$Confirmation_type='1';//YES
	}
	else
	{	$Confirmation_type='0';//NO
	}

	$last_insert_id= $this->Assessment_model->insertuserlog($userid,$Login_type,$Confirmation_type);
	
	$arrgetrelatedusers=$this->Assessment_model->fetchrelateduser($userid,$username,$grade_id,$section,$sid);
	/* $html='<ul>';
	foreach($arrgetrelatedusers as $arr)
	{
		$html.="<li><label>".$arr['']."</label>";
		$html.="<a href=''></a></li>";
	} */
}

public function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
public function toppers()
{	
	/* Check whether data insterted today or not. 
	*/
		//$arrtdyentry=$this->Assessment_model->CheckTodaydataExist();
		$this->Assessment_model->ClearToppersData();
		$getTopBSPIScore=$this->Assessment_model->InsertOverallBspiToppernew();
		$getTopSparkiesValue=$this->Assessment_model->InsertOverallSparkyToppernew();
		echo "Success";exit;
}
public function schoolswiseperiodinsert($startdate = null)
{
	if($startdate==''){$startdate=date('Y-m-d');$enddate=date('Y-m-d');}else{$startdate=$startdate;$enddate=$startdate;}
	//echo $startdate."==".$enddate;exit;
	$getSuperAngels=$this->Assessment_model->Schools_Wise_Period_Insert($startdate,$enddate);
}
/*------------- Leader Board API -------------------------*/
public function leaderboardnew($sid=null,$gradeid=null,$monthno=null)
{	if($sid!='' && $gradeid!='' && $monthno!='')
	{
		$date=Date("2018-".$monthno."-d");
		$startdate=date('Y-m-01',strtotime($date));
		$enddate=date('Y-m-t',strtotime($date));
		//echo $sid."==".$gradeid."==".$startdate ."==".$enddate;exit;
		$getTopSparkiesValue=$this->Assessment_model->InsertTopSparkiesValue($startdate,$enddate,$sid,$gradeid);

		$getTopSparkiesValue=$this->Assessment_model->InsertTopPlayedGames($startdate,$enddate,$sid,$gradeid);

		$getTopSparkiesValue=$this->Assessment_model->InsertTopBSPIScore($startdate,$enddate,$sid,$gradeid);

		$getTopSparkiesValue=$this->Assessment_model->InsertSuperAngels($startdate,$enddate,$sid,$gradeid);
			echo "Success";exit;
	}
	else
	{
		echo "Please pass valid arguments";exit;
	}
}
/*-------------------30 mins Time over concept-------------------------*/
public function TodayTimerInsert($ChildID)
{
	if(isset($_POST['cid']))
	{
		$user_id=$_POST['cid'];
	}
	else
	{
		$user_id=$ChildID;
	}
	$arrofisexist=$this->Assessment_model->IsTotayTimerExist($user_id);
	//echo "<pre>";print_r($arrofisexist);exit;
	if($arrofisexist[0]['isexist']==0){
		$this->Assessment_model->TodayTimerInsert($user_id);
	}
}
/*-------------------30 mins Time over concept END-----------------------*/


public function resultcq()
{
	/*..... App Related Code .............*/
		if($this->session->isappcq=="1")
		{
			if(isset($this->session->app_cquserid))
			{
				$data = $_POST;
				$this->session->set_flashdata('cqapp_post_data',$data);
				redirect(base_url('index.php/api/resultcq'));
				exit;
			}
		exit;
		}
	/*..... App Related Code .............*/
	
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php'); exit;}
		if(!isset($_POST)){redirect('index.php'); exit;}
		if(empty($_POST)){redirect('index.php'); exit;}
		$total_ques=$_POST["tqcnt1"];
		if($_POST["aqcnt1"]>10){
		$attempt_ques=10;
		}
		else{
		$attempt_ques=$_POST["aqcnt1"];
		}

		$answer=$_POST["cqcnt1"];
		$score=$_POST["gscore1"];
		$a6=$_POST["gtime1"];
		$a7=$_POST["rtime1"];
		$a8=$_POST["crtime1"];
		$a9=$_POST["wrtime1"];
		$gameid=$_POST["quesNo1"];
		$isskillkit=$this->session->isskillkit;
		if($isskillkit=="Y"){$skillkit=1;}
		else{$skillkit=0;}
		$userlang = $this->session->userlang;
		$userid = $this->session->user_id; 
		$lastup_date = date("Y-m-d");
		$cid = 1;$skillid='';
		$pid =  $this->session->gp_id; 
		if($gameid!='')
		{
			$data['insert1'] = $this->Assessment_model->insertone_CQ($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		
			$this->Assessment_model->InsertCQGames($userid,$gameid); //Update in Rand Table.
		}
		echo 1;exit;
}
public function MyPercentileRank($type=null)
{	//echo $this->session->org;exit;
	$arrofoverall=$this->Assessment_model->MyPercentileRank($this->session->user_id,$this->session->game_grade,'');
	$arroforg=$this->Assessment_model->MyPercentileRank($this->session->user_id,$this->session->game_grade,$this->session->org);
	
	$OverallPercentile=((($arrofoverall[0]['belowusercount']+(0.5*$arrofoverall[0]['samescoreusercount']))/$arrofoverall[0]['totusercount'])*100);
	
	$orgPercentile=((($arroforg[0]['belowusercount']+(0.5*$arroforg[0]['samescoreusercount']))/$arroforg[0]['totusercount'])*100);

	$response=array(
				'overallpos'=>Round($OverallPercentile,2),
				'organizationpos'=>Round($orgPercentile,2)
	);
	if($type=='FN')
	{
		return json_encode($response);
	}
	else
	{
		echo json_encode($response);exit;
	}
}
public function MyCurrentRank($type=null)
{
	$arrval=$this->Assessment_model->MyCurrentRank($this->session->user_id,$this->session->game_grade);
	$response=array(
				'overallpos'=>$arrval[0]['overallpos'],
				'gradewisepos'=>$arrval[0]['gradewisepos']
			);
	if($type=='FN')
	{
		return json_encode($response);
	}
	else
	{
		echo json_encode($response);exit;
	}
}

public function MyChallengeQ()
{	
	if($this->session->cqvisible==1)
	{
		$cqvisible=$this->Assessment_model->getChallengeQuestionVisible($this->session->user_id);
		if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
		{
			echo 1;exit;
		}
		else
		{
			echo 0;exit;
		}
	}
	else
	{
		echo 0;exit;
	}
}
public function cqgamesajax()
{
	$gameurl =  $_POST['gameurl']; 
	$gname = substr($gameurl, strrpos($gameurl, '/') + 1);
	$gamename = str_replace('.html','', $gname); 
	
	
	if($_POST['skillkit']!='Y')
	{	$data['gameid'] = $this->Assessment_model->getgameid_CQ($gamename);
		$gameid = $data['gameid'][0]['gid']; 
		$data['checkgame'] = $this->Assessment_model->checkgame_CQ($gameid);
		
		$gid = $data['checkgame'][0]['gameid']; 
		
		if($gid>=1)
		{
			
		$this->session->set_userdata(array('currentgameid'=> $gameid ));
		$this->session->set_userdata(array('isskillkit'=> $_POST['skillkit'] ));
		echo $this->session->currentgameid;exit;
		}
		else{
			
			echo 'IA'; exit;
		}
	}
	else
	{	
		$data['gameid'] = $this->Assessment_model->getgameid_SK($gamename);
		$gameid = $data['gameid'][0]['gid'];
		$this->session->set_userdata(array( 'currentgameid'=> $gameid ));
		$this->session->set_userdata(array( 'isskillkit'=> $_POST['skillkit'] ));
		echo $this->session->currentgameid;exit;
	}


}

public function playeddates()
{

	$userid = $this->session->user_id;
	$data['playeddates']=$this->Assessment_model->getplayeddates($userid);
	$resJSON = json_encode($data['playeddates']);
	/* $res = str_replace('lastupdate','date', $resJSON);
	echo  str_replace('gc_id','badge', $res);exit; */
	echo $resJSON;exit;
	//echo json_encode(array("date"=>$data['playeddates'])); exit;
	
}
public function getUserPuzzlesDetails($type=null)
{
	$userid = $this->session->user_id;
	$arrval=$this->Assessment_model->getUserPuzzlesDetails($userid);
	$response=array(
		'MinutesTrainedper'=>floor((100*$arrval[0]['MinutesTrained'])/($this->session->useddays*15)),
		'PuzzlesSolvedper'=>floor((100*$arrval[0]['PuzzlesSolved'])/($this->session->useddays*10)),
		'PuzzlesAttemptedper'=>floor((100*$arrval[0]['PuzzlesAttempted'])/($this->session->useddays*10)),
		'UniqueGamesper'=>floor((100*$arrval[0]['UniqueGames'])/$this->session->useddays),
		'cqplayedcountper'=>floor((100*$arrval[0]['cqplayedcount'])/$this->session->useddays),
		
		'MinutesTrainedtext'=>$arrval[0]['MinutesTrained'],
		'PuzzlesSolvedtext'=>$arrval[0]['PuzzlesSolved'],
		'PuzzlesAttemptedtext'=>$arrval[0]['PuzzlesAttempted'],
		'UniqueGamestext'=>$arrval[0]['UniqueGames'],
		'cqplayedcounttext'=>$arrval[0]['cqplayedcount'],
	);
	
	if($type=='FN')
	{
		return json_encode($response);
	}
	else
	{
		echo json_encode($response);exit;
	}
}
public function getTrainingCalendarData($type=null)
{
	$userid = $this->session->user_id;
	$curdate = $this->input->post('curdate');
	$arrval=$this->Assessment_model->getTrainingCalendarData($userid,$curdate);
	
	$avg_game_score=$arrval[0]['maxscore'];
	if($avg_game_score < 20) $filled_stars = 0;
	if($avg_game_score >= 20 && $avg_game_score <= 40)	$filled_stars = 1;
	if($avg_game_score >= 41 && $avg_game_score <= 60)	$filled_stars = 2;
	if($avg_game_score >= 61 && $avg_game_score <= 80)	$filled_stars = 3;
	if($avg_game_score >= 81 && $avg_game_score <= 90)	$filled_stars = 4;
	if($avg_game_score >= 91 && $avg_game_score <= 100)	$filled_stars = 5;
	$starimg='';
	for($i=0;$i<$filled_stars;$i++)
	{
		$starimg.='<img class="" style="width:10%" src="'.base_url().'assets/images/new/Star01.png">';
	}
	for($i=0;$i<5-$filled_stars;$i++){
		$starimg.='<img class="" style="width:10%" src="'.base_url().'assets/images/new/Star00.png">';
	} 
	 
	$response=array(
	
		'MinutesTrainedper'=>floor((100*$arrval[0]['MinutesTrained'])/(15)),
		'PuzzlesSolvedper'=>floor((100*$arrval[0]['PuzzlesSolved'])/(10)),
		'PuzzlesAttemptedper'=>floor((100*$arrval[0]['PuzzlesAttempted'])/(10)),
		'TotScoreper'=>floor((100*$arrval[0]['TotScore'])/(500)),
		
		'MinutesTrained'=>$arrval[0]['MinutesTrained'],
		'PuzzlesSolved'=>$arrval[0]['PuzzlesSolved'],
		'PuzzlesAttempted'=>$arrval[0]['PuzzlesAttempted'],
		'Gname'=>$arrval[0]['Gname'],
		'TotScore'=>$arrval[0]['TotScore'],
		'Crownies'=>$arrval[0]['Crownies'],
		'Ischallengeplayed'=>$arrval[0]['Ischallengeplayed'],
		'Curday'=>date("d-m-Y", strtotime($curdate)),
		'Star'=>$filled_stars,
		'starimg'=>$starimg
	);
	
	if($type=='FN')
	{
		return json_encode($response);
	}
	else
	{
		echo json_encode($response);exit;
	}
}
/*.......... CQ Concept ...........*/
	public function getQuesList()
	{
		/*..... App Related Code .............*/
		if($this->session->isappcq=="1")
		{
			if(isset($this->session->app_cquserid))
			{
				redirect(base_url('index.php/api/getQuesList'));exit;
			}
		exit;
		}
	/*..... App Related Code .............*/
		
		$userid = $this->session->user_id;
		if(isset($userid)){
					
		$data['query']=$this->Assessment_model->getQuesList($userid);
		
		foreach($data['query'] as $rows){
			
			$details[] = array('questionsList'=> $rows['quesPlayed']);
			
		}
		$jsonString = json_encode($details);
		echo $jsonString;
		}
		else
		{
				echo "POST value not assigned";
		}
	}
	public function getQuesInfo()
	{
		/*..... App Related Code .............*/
			if($this->session->isappcq=="1")
			{
				if(isset($this->session->app_cquserid))
				{
					$data = $_POST;
					$this->session->set_flashdata('cqapp_que_post_data',$data);
					redirect(base_url('index.php/api/getQuesInfo'));exit;
				}
			exit;
			}
		/*..... App Related Code .............*/
		
		if(isset($_POST['sno'])){
		$sno = $_POST['sno'];
		$data['query']=$this->Assessment_model->getQuesInfo($sno);
  
		foreach($data['query'] as $rows){
			
			$details[] = array(
            'question'=> $rows['question'],
            'choice1' => $rows['choice1'],
            'choice2' => $rows['choice2'],
            'choice3' => $rows['choice3'],
            'choice4' => $rows['choice4'],
            'answer' => $rows['answer'],
            'solution' => $rows['solution'],
            'referred' => $rows['referred'],
            'ques_no' => $rows['ques_no'],
            'exam_date' => $rows['exam_date'],
            'grade' => $rows['grade']
		);
		}
		$jsonString = json_encode($details);
		echo $jsonString;
		}
		else{
		echo "POST value not assigned";
		}	
	}
	public function gradewiseList()
	{
		/*..... App Related Code .............*/
			if($this->session->isappcq=="1")
			{
				if(isset($this->session->app_cquserid))
				{
					redirect(base_url('index.php/api/gradewiseList'));exit;
				}
			exit;
			}
		/*..... App Related Code .............*/
		
		$grade =  $this->session->game_grade;
		$userid = $this->session->user_id;
		/* $grade = 8;
		$userid = 1; */
		//echo "<pre>";print_r($_SESSION);exit;
		if(isset($grade)){
			
		$data['query']=$this->Assessment_model->gradewiseList($grade,$userid);
	
		foreach($data['query'] as $rows){
			
			$details[] = array('questionsList'=> $rows['qnos']);
		}

		$jsonString = json_encode($details);
		echo $jsonString;
		}
		else{
		echo "POST value not assigned";
		}
	}
/*................ CQ Concept End ................*/
public function avatarupdate()
{
	$avatarimg = $_POST['avatarimg']; 
	$userid = $this->session->user_id;
	$data['avatarupdate']=$this->Assessment_model->avatarupdate($userid,$avatarimg);
	
}
/*........... Generating FB Sharing Image....................*/

public function GenerateSharingImg()
{ 	//header('Content-Type: image/png');
	$uid=$this->session->user_id;$username=$this->session->username;
	$curdate=date('Y-m-d');
	//$curdate='2018-03-30';
	
	$arrval=$this->Assessment_model->getFBSharingData($uid,$curdate);
	
	//$BackgroundImage= base_url().'assets/images/new/FB_Share.png';
	$BackgroundImage= '/mnt/vol2/sites/schamps/dev/assets/images/new/FB_Share.png';
		$img = $this->LoadPNG($BackgroundImage,$arrval[0]['TotScore'],$arrval[0]['PuzzlesSolved'],$arrval[0]['Crownies'],$curdate);
	 
		$foldername="fb/";
		if (!file_exists($foldername)) {
			mkdir($foldername, 0777, true);
		}
		imagepng($img,$foldername.''.$uid.".png");
		imagedestroy($img);
		echo $foldername.''.$uid.".png?id".date('YmdHis');
}


public function LoadPNG($fBackgroundImage,$TotScore,$PuzzlesSolved,$Crownies,$curdate)
{
/* Attempt to open */
$im = @imagecreatefrompng($fBackgroundImage);
/* See if it failed */
if(!$im)
{
echo "Loading bg has issue ".$fBackgroundImage;
}
// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
$yellow = imagecolorallocate($im, 245, 237, 6);

// The text to draw
// Replace path by your own font path
//$font = APPPATH."../assets/fonts/LobsterTwo-Bold.ttf";
$font = "/mnt/vol2/sites/schamps/dev/assets/fonts/LobsterTwo-Bold.ttf";
// Add some shadow to the text
$text_color = imagecolorallocate($im, 0, 0, 0);


/* imagettftext ($im, 15, 0, 800, 155, $yellow, $font, 'Total Score'); */
//imagettftext ("img","Font SIZE", 0, "X-POs", "Y-POS","COLOR","Style Font","Value");
imagettftext ($im, 80, 0, 700, 210, $white, $font, $TotScore);

/* imagettftext ($im, 15, 0, 155, 480, $yellow, $font, 'Crownies'); */
imagettftext ($im, 60, 0, 1030, 310, $white, $font, $Crownies);


/* imagettftext ($im, 15, 0, 940, 480, $yellow, $font, wordwrap('Question Solved',8)); */
imagettftext ($im, 50, 0, 870, 510, $white, $font, $PuzzlesSolved);


imagettftext ($im, 11, 0, 1030, 20, $white, $font, date('d/m/Y H:i:s'));


return $im;
}

public function getTopScorePosition($type=null)
{
	$position = $this->Assessment_model->getTopScorePosition($this->session->user_id);
	if($type=='FN')
	{
		return $position[0]['position'];
	}
	else
	{
		echo $position[0]['position'];exit;
	}
}
public function getMaxPuzzlesAttempted($type=null)
{
	$position= $this->Assessment_model->getMaxPuzzlesAttempted($this->session->user_id);
	if($type=='FN')
	{
		return $position[0]['position'];
	}
	else
	{
		echo $position[0]['position'];exit;
	} 
}
public function getMaxPuzzlesSolved($type=null)
{
	$position= $this->Assessment_model->getMaxPuzzlesSolved($this->session->user_id);
	if($type=='FN')
	{
		return $position[0]['position'];
	}
	else
	{
		echo $position[0]['position'];exit;
	}
}
public function daywisebadgetopper($curdate=null)
{
	if($curdate!='')
	{
		$date= date('Y-m-d',(strtotime ('-1 day' , strtotime ($curdate))));
	}
	else
	{
		$date= date('Y-m-d',(strtotime ('-1 day' , strtotime (date('Y-m-d')))));
	}
	
	$this->Assessment_model->InsertTopScorePositionUser($date);
	$this->Assessment_model->InsertMaxPuzzlesAttemptedUser($date);
	$this->Assessment_model->InsertMaxPuzzlesSolvedUser($date);
}


/* ................ Invite a Friend .................. */
public function GetReferalFriendData()
{
	$userid=$this->session->user_id;
	$userlist=$this->Assessment_model->GetReferalFriendData($userid);
	//echo "<pre>";print_r($userlist);exit;
	if($userlist[0]['mobile']!=''){$r1mobile=$userlist[0]['mobile'];}else{$r1mobile=0;}
	if($userlist[1]['mobile']!=''){$r2mobile=$userlist[1]['mobile'];}else{$r2mobile=0;}
	
	if($userlist[0]['name']!=''){$r1name=$userlist[0]['name'];}else{$r1name='-';}
	if($userlist[1]['name']!=''){$r2name=$userlist[1]['name'];}else{$r2name='-';}
	
	if($userlist[0]['pname']!=''){$r1pname=$userlist[0]['pname'];}else{$r1pname='-';}
	if($userlist[1]['pname']!=''){$r2pname=$userlist[1]['pname'];}else{$r2pname='-';}
	
	if($userlist[0]['emailid']!=''){$r1emailid=$userlist[0]['emailid'];}else{$r1emailid='-';}
	if($userlist[1]['emailid']!=''){$r2emailid=$userlist[1]['emailid'];}else{$r2emailid='-';}
	
	$userinfo=array(
		'r1name'=>$r1name,
		'r1pname'=>$r1pname,
		'r1emailid'=>$r1emailid,
		'r1mobile'=>$r1mobile,
		'r2name'=>$r2name,
		'r2pname'=>$r2pname,
		'r2emailid'=>$r2emailid,
		'r2mobile'=>$r2mobile
	);
	
	echo json_encode($userinfo);
}
public function ReferalFriend1()
{
	$name = $this->input->post('txtFName');
	$Pname = $this->input->post('txtPName');
	$mobile = $this->input->post('txtMobile');
	$emailid = $this->input->post('txtEmail');
	$userid=$this->session->user_id;
	$arrisexist=$this->Assessment_model->CheckReferalFriendIsExist($mobile,$emailid,$userid);
	if($arrisexist[0]['isexist']==0)
	{
		/* $arrisrefered=$this->Assessment_model->CheckUserIsReferred($userid);
		$isexist=$arrisrefered[0]['isexist']; */
		$lastid=$this->Assessment_model->InsertReferalFriend($name,$Pname,$mobile,$emailid,$userid);
		$msg="Dear ".$name.", Congratulations! Your friend ".$this->session->fname." has gifted you a brain magic program worth of Rs 600/-. Our support team will contact you shortly to get this card delivered to you. For any further queries please feel free to contact us at +91 9569565454";
		$this->confirmation_sms($mobile,$msg);
		echo $lastid;exit;
	}
	else
	{
		echo 0;exit;
	}
}
public function ReferalFriend2()
{
	$name = $this->input->post('txtFName1');
	$Pname = $this->input->post('txtPName1');
	$mobile = $this->input->post('txtMobile1');
	$emailid = $this->input->post('txtEmail1');
	$userid=$this->session->user_id;
	$arrisexist=$this->Assessment_model->CheckReferalFriendIsExist($mobile,$emailid,$userid);
	if($arrisexist[0]['isexist']==0)
	{
		/* $arrisrefered=$this->Assessment_model->CheckUserIsReferred($userid);
		$isexist=$arrisrefered[0]['isexist']; */
		$lastid=$this->Assessment_model->InsertReferalFriend($name,$Pname,$mobile,$emailid,$userid);
		
		$msg="Dear ".$name.", Congratulations! Your friend ".$this->session->fname." has gifted you a brain magic program worth of Rs 600/-. Our support team will contact you shortly to get this card delivered to you. For any further queries please feel free to contact us at +91 9569565454";
		
		$this->confirmation_sms($mobile,$msg);
		echo $lastid;exit;
	}
	else
	{
		echo 0;exit;
	}
}
public function confirmation_sms($tonum,$message)
{
	$baseurl="https://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
	$ch = curl_init("http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=Ac79406bbeca30e25c926bad8928bcc17&to=".$tonum."&sender=SBRAIN&message=".urlencode($message));

	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response=curl_exec($ch);       
	curl_close($ch);
}
public function checkparentisactive()
{ 
	$this->Assessment_model->update_parent_logout_log($this->session->parentid,$this->session->login_session_id);
	$exeislogin = $this->Assessment_model->checkParentIsActive($this->session->parentid,$this->session->login_session_id);
	//echo $exeislogin[0]->isalive;exit;
	if($exeislogin[0]->isalive!=1)
	{
		//$exeislogin = $this->Assessment_model->updateuserloginstatus($this->session->user_id,$this->session->login_session_id);
		$this->session->unset_userdata();
		$this->session->sess_destroy();
		echo 1;exit;
	}
	else
	{
		echo 0;exit;
	}
}
public function checkChildisactive()
{ 
		$this->Assessment_model->update_parent_logout_log($this->session->ChildId,$this->session->Child_login_session_id);
		$exeislogin = $this->Assessment_model->checkParentIsActive($this->session->ChildId,$this->session->Child_login_session_id);
	//echo $exeislogin[0]->isalive;exit;
	if($exeislogin[0]->isalive!=1)
	{
		$this->session->unset_userdata();
		$this->session->sess_destroy();
		echo 1;exit;
	}
	else
	{
		echo 0;exit;
	}
}

}
