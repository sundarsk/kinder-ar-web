<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Subscription_model');
				$this->load->library('session');
				$this->load->library('My_PHPMailer');
				$this->load->helper('captcha');
				//$this->lang->load("menu_lang","french");		
			
        }
public function indexnew()
{
	//$aaa=$this->registation_successmail('sundarcse2k11@gmail.com','Sundar');	
	//$aaa=$this->confirmation_email('sundarcse2k11@gmail.com','Sundar','sundarcse2k11@gmail.com','Sundar','sundarcse2k11@gmail.com','Sundar');
	//$aaa=$this->confirmation_email('sundarcse2k11@gmail.com','Sundar','Sundar');
	//echo "Testing";exit;
}
public function registration()
{
	
	$data['StateList'] = $this->Subscription_model->getStateList();
	$data['GradeList'] = $this->Subscription_model->gradelist();
	
  $this->load->view('header');
  $this->load->view('registration', $data);
  $this->load->view('footer');
}
public function salt_my_pass($password)
{
// Generate two salts (both are numerical)
$salt1 = mt_rand(1000,9999999999);
$salt2 = mt_rand(100,999999999);

// Append our salts to the password
$salted_pass = $salt1 . $password . $salt2;

// Generate a salted hash
$pwdhash = sha1($salted_pass);

// Place into an array
$hash['Salt1'] = $salt1;
$hash['Salt2'] = $salt2;
$hash['Hash'] = $pwdhash;

// Return the hash and salts to whatever called our function
return $hash;

} 
public function profileimage()
{
	if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
        move_uploaded_file($_FILES['file']['tmp_name'], 'upload/' . $_FILES['file']['name']);
    }
	/* basename( $_POST['imagename'] );
	$filename = str_replace( "\\", '/', $_POST['imagename'] );
	$imagename = basename( $filename ); 
	$uploaddir = 'upload/';
	$sourcePath = $_POST['imagename'];  
	echo $targetPath = $uploaddir .$imagename; 
	move_uploaded_file($sourcePath, $targetPath);  */
} 
 public function statelist()
 {
	 $countryid = $_POST['countryID']; 
	 
	 $data['statelist'] = $this->Subscription_model->statelist($countryid);
	 $this->load->view('statelist_ajax', $data);
 }
 public function checkemailexist()
 {
	 $emailid = $_POST['emailid'];
	 $data['checkemail'] = $this->Subscription_model->checkemailexist($emailid);
	 echo $data['checkemail'][0]['isexist'];
 } 
 public function IsMobilenounique()
 {
	 $Mobile = $_POST['Mobile'];
	 $data['ismobile'] = $this->Subscription_model->IsMobilenounique($Mobile);
	 echo $data['ismobile'][0]['isexist'];
 }

 public function CheckandGenerateUsername()
 {		$firstname=$_REQUEST['firstname'];
		$schoolid=$_REQUEST['schoolid'];
		$data['query1'] = $this->Subscription_model->getusercount(str_replace(' ','', strtolower($firstname)),$schoolid);
		$userexist = $data['query1'][0]['userCount']; 
		if($userexist==0)
		{
			$username=$this->Subscription_model->GenerateUsername($firstname,$schoolid,$userexist,"NEW");
		}
		else
		{
			$username=$this->Subscription_model->GenerateUsername($firstname,$schoolid,$userexist,"OLD");
		}
		echo $username[0]['username'];exit;
 }
 public function paymentfailure()
 {
	$this->load->view('header');
  $this->load->view('paymentfailure');
  $this->load->view('footer');
 }
 
  public function alreadyregistered()
 {
	$this->load->view('header');
  $this->load->view('chk_registration');
  $this->load->view('footer');
 }
 
 public function termsconditions()
	{
  $this->load->view('header');
  $this->load->view('termsconditions');
  $this->load->view('footer');
	}
	
 public function termsofservice()
	{
  $this->load->view('header');
  $this->load->view('footerpages/termsofservice');
  $this->load->view('footer');
	}
	
	public function privacypolicy()
	{
  $this->load->view('header');
  $this->load->view('footerpages/privacypolicy');
  $this->load->view('footer');
	}
	
	public function faq()
	{
  $this->load->view('header');
  $this->load->view('footerpages/faq');
  $this->load->view('footer');
	}
public function userregistration()
{
	$password = $_POST['txtOPassword'];
	$hashpass = $this->salt_my_pass($password);
	 
	$shpassword = $hashpass['Hash']; 
	$salt1 = $hashpass['Salt1']; 
	$salt2 = $hashpass['Salt2']; 
	
	$saltedpass = $salt1 . $shpassword . $salt2; 

	$firstname = $_POST['txtFName'];
	
	$mobile = $_POST['txtMobile'];
	$email = $_POST['txtEmail'];
	$password = $_POST['txtOPassword'];
	$state = $_POST['ddlState'];
	$city = $_POST['txtCity'];	 
	$pincode = $_POST['txtPincode'];
	$address = $_POST['txtAddress'];
	if($email!='' && $mobile!='')
	{
		$isuserexit = $this->Subscription_model->checkemailexist($email);
		$ismobile = $this->Subscription_model->IsMobilenounique($mobile);
		
		if(($isuserexit[0]['isexist']==0) && ($ismobile[0]['isexist']==0) )
		{
		
			$created_on=date("Y-m-d");
			$data['insertreg'] = $this->Subscription_model->addRegistration($firstname,$mobile,$email,$shpassword,$salt1,$salt2,$password,$created_on,$state,$city,$pincode,$address);
			
			$lastid = $data['insertreg'];
			
			$cdate=date('YmdHis', strtotime($created_on));
			$userlastid=$data['insertreg'];
			$RegSub="Registration Confirmation - KinderAngels";
			$RegMsg='<table align="center" width="800px" border="1" cellspacing="0" cellpadding="0" style="font-size:medium;margin-right:auto;margin-left:auto;border:1px solid rgb(197,197,197);font-family:Arial,Helvetica,sans-serif;background-image:initial;background-repeat:initial;box-shadow: 10px 10px 5px #35395e;"><tbody><tr style="display:block;overflow:hidden;background: #1e88e5;"><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width:33%;color: #fff;"></td><td style="float:left;border:0px;text-align: center;padding: 5px 0px;width: 33%;color: #fff;"><img alt="www.kinderangels.com" src="'.base_url().'assets/images/new/skinder-web.png" style="float: left;width:220px;"></td></tr><tr style="padding:0px;margin:10px 42px 20px;display:block;font-size:13px;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:justify"><td colspan="2" style="border:0px">Dear '.$firstname.',<br/><br/>Thank you for subscribing <strong>KinderAngels</strong>.<br/><br/>Click below link to confirm and proceed with registration	<br/><br/><br/>
			<a href="'.base_url().'index.php/subscription/payment?uid='.md5($userlastid).'&key='.md5($cdate).'" style="color: #fff;background: #ea0b72;padding: 13px;border-radius: 5px;text-decoration: none;font-weight: bold;margin-top: 10px;" target="_blank" >Proceed Registration</a><br/><br/><br/><br/>Best Regards,<br/><strong>KinderAngels Team</strong><br/></td></tr><tr style=""><td style="text-align:center;color:#ee1b5b;border:0px;background-size:100%;background-image: url('.base_url().'assets/images/emailer/footer.jpg);padding-top:20px;padding-bottom:20px;font-family: cursive;font-size: 20px;"><div style="width:100%;font-family:; float:left;text-align:center"><a href="'.base_url().'" target="_blank" style="color:#ee1b5b;text-decoration: none;" >'.base_url().'</a><br/><a href="mailto:support@skillangels.com"  style="color:#ee1b5b;text-decoration: none;" >support@skillangels.com</a></div></td></tr><tr style="display:block;overflow:hidden">
			<td style="float:left;border:0px;"></td></tr></tbody></table>';
			
			//echo $RegMsg;
			$this->confirmation_email($email,$RegSub,$RegMsg);
				
			$arrResult=array("response"=>"success","code"=>1,"msg"=>"Success");
			echo json_encode($arrResult);exit;
		}
		else
		{
			$arrResult=array("response"=>"failure","code"=>2,"msg"=>"Mobile No. OR Email ID Already exists");
			echo json_encode($arrResult);exit;
		}
		
	}
	else
	{
		$arrResult=array("response"=>"failure","code"=>3,"msg"=>"Please fill the mandatory fields");
		echo json_encode($arrResult);exit;
	}
}
public function confirmation_sms($tonum,$message)
{
	$baseurl="https://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
	$ch = curl_init("http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=Ac79406bbeca30e25c926bad8928bcc17&to=".$tonum."&sender=SBRAIN&message=".urlencode($message));

	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response=curl_exec($ch);       
	curl_close($ch);
}
public function confirmation_email($toemailid,$subject,$message)
{
//Create a new PHPMailer instance
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = "smtp.falconide.com";
	$mail->Port = 587;
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = "";
	$mail->Username = "skillsangelsfal";
	$mail->Password = "SkillAngels@123";
	$mail->setFrom('angel@skillangels.com', 'kinderangels');
	$mail->addReplyTo('angel@skillangels.com', 'kinderangels');
	$mail->addAddress($toemailid, ''); //to mail id
	$mail->Subject = $subject;
	$mail->msgHTML($message);
	if (!$mail->send()) {
	   //echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
	   //echo "Message sent!";
	}  
}
public function payment()
{
	$ParentID =  $_REQUEST['uid']; 
	$create =  $_REQUEST['key'];
	
	$data['validateurl'] = $this->Subscription_model->validateurl($ParentID);
	$isuserexist=$this->Subscription_model->chkprocesseduser($ParentID);
	$key = date('YmdHis', strtotime($data['validateurl'][0]['createddate']));
	
	 
	if(isset($isuserexist[0]['id']) && $isuserexist[0]['id']>0)
	{
		redirect("index.php/subscription/alreadyregistered");  
	}
	if($ParentID!=md5($data['validateurl'][0]['id']) || $create!=md5($key))
	{
		redirect("index.php/subscription/registration"); 
	}
	else
	{
		$data['query'] = $this->Subscription_model->getInActiveParentDetails($ParentID);
		if(isset($data['query'][0]->id))
		{
			$this->Subscription_model->updateParentConfirmUser($ParentID);
			
			
			$RegSucSub="Registration Successful - KinderAngels";
			$RegSucEMSG='<table align="center" width="800px" border="1" cellspacing="0" cellpadding="0" style="font-size:medium;margin-right:auto;margin-left:auto;border:1px solid rgb(197,197,197);font-family:Arial,Helvetica,sans-serif;background-image:initial;background-repeat:initial;box-shadow: 10px 10px 5px #35395e;"><tbody><tr style="display:block;overflow:hidden;background: #1e88e5;"><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width:33%;color: #fff;"></td><td style="float:left;border:0px;text-align: center;padding: 5px 0px;width: 33%;color: #fff;"><img alt="www.kinderangels.com" src="'.base_url().'assets/images/new/skinder-web.png" style="float: left;width:220px;"></td><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width: 33%;color: #fff;"></td></tr><tr style="padding:0px;margin:10px 42px 20px;display:block;font-size:13px;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:justify"><td colspan="2" style="border:0px">Dear '.$data['query'][0]->name.',<br/><br/>Thank you for registering with <strong>KinderAngels</strong>.<br/><p><strong>Now !!</strong> you can unwrap the gift to enjoy & learn thinking skills</p><br/><br/>Following are the credentials to access the service at <a href="'.base_url().'" target="_blank" >'.base_url().'</a><br/><br/>username <strong> : '.$data['query'][0]->username.'</strong><br/>password<strong> : '.$data['query'][0]->orgpwd.'</strong><br/><br/><br/>All The Very Best!!!<br/><br/>Best Regards,<br/><strong>KinderAngels Team</strong><br/></td></tr><tr style=""><td style="text-align:center;color:#ee1b5b;border:0px;background-size:100%;background-image: url('.base_url().'assets/images/emailer/footer.jpg);padding-top:20px;padding-bottom:20px;font-family: cursive;font-size: 20px;"><div style="width:100%;font-family:; float:left;text-align:center"><a href="'.base_url().'" target="_blank" style="color:#ee1b5b;text-decoration: none;" >'.base_url().'</a><br/><a href="mailto:support@skillangels.com"  style="color:#ee1b5b;text-decoration: none;" >support@skillangels.com</a></div></td></tr><tr style="display:block;overflow:hidden">
			<td style="float:left;border:0px;"></td></tr></tbody></table>';

			// SMS MSG for => Children 
			$RegSucSMSG="Dear ".$data['query'][0]->name.", you are successfully registered with KinderAngels. Username is ".$data['query'][0]->username." and password is ".$data['query'][0]->orgpwd.". Please visit ".base_url()." ";
			
			//echo $RegSucSMSG."<br/><br/>";
			//echo $RegSucEMSG."<br/><br/>"; exit;
			
			  //$this->confirmation_email($email,$RegSucSub,$RegSucEMSG);//Mail Calling => Children
			 // $this->confirmation_sms($mobile,$RegSucSMSG);//SMS Calling => Children
			$this->load->view('header');
			$this->load->view('paymentresponse', $data);
			$this->load->view('footer');
		}
		
		
	}
}
public function couponajax()
{
	if($_POST['couponcode']!='' && $_POST['userid']!='')
	{
		$couponcode = $this->input->post('couponcode');
		$userid = $this->input->post('userid'); 
		$data['plandetails'] = $this->Subscription_model->getconfigdetails($couponcode);
		
		$getCouponValidTimes=$this->Subscription_model->getCouponValidTimes($couponcode);
		$data['couponcheck'] = $this->Subscription_model->applycoupon($couponcode);
		if($getCouponValidTimes[0]['coupon_valid_times']!=0)
		{
			$planamount = $data['plandetails'][0]['price'];
			$discountperc = $data['couponcheck'][0]['discount_percentage'];
			$discountamt=($planamount*$discountperc/100);
			$paidamount=$planamount-$discountamt;
		}
		else
		{
			$planamount = $data['plandetails'][0]['price'];
			$discountperc = $getCouponValidTimes[0]['discount_percentage'];
			$discountamt=($planamount*$discountperc/100);
			$paidamount=$planamount-$discountamt;
		}
		if($data['couponcheck'][0]['discount_percentage']!='' && $data['couponcheck'][0]['discount_percentage']!=0)
		{
			$isvalid=1;
		}
		else
		{
			$isvalid=0;
		}
		$arrResult=array("discountamount"=>round($discountamt,2), "paidamount"=>round($paidamount,2),"validity"=>$data['plandetails'][0]['validity'],"price"=>$data['plandetails'][0]['price'],'isvalid'=>$isvalid);
	}
	else
	{
		$arrResult=array("discountamount"=>'', "paidamount"=>'','isvalid'=>0);
	}
		echo json_encode($arrResult);
} 
public function paymentresponse()
{
 $ChildId =  $_REQUEST['hdnPaymSubscribeID'];
 $data['getresponsedetails'] = $this->Subscription_model->getresponsedetails($ChildId);
 $subscriberid = $data['getresponsedetails'][0]['id'];
 $salt1 = $data['getresponsedetails'][0]['salt1'];
 $salt2 = $data['getresponsedetails'][0]['salt2'];
 $email = $data['getresponsedetails'][0]['email'];
 $password = $data['getresponsedetails'][0]['password'];
 $gradeid = $data['getresponsedetails'][0]['gradeid'];
 $firstname = $data['getresponsedetails'][0]['firstname'];
 $lastname = $data['getresponsedetails'][0]['lastname'];
 $gender = $data['getresponsedetails'][0]['gender'];
 $mobile = $data['getresponsedetails'][0]['mobilenumber'];
 $address = $data['getresponsedetails'][0]['address'];
 $dob = $data['getresponsedetails'][0]['dateofbirth'];
 $gameplanid = $data['getresponsedetails'][0]['gp_id'];
 $couponcode= $data['getresponsedetails'][0]['couponcode'];
 $state = $data['getresponsedetails'][0]['state'];
 $city= $data['getresponsedetails'][0]['city'];
 $orgpwd= $data['getresponsedetails'][0]['orgpwd'];
 $pincode= $data['getresponsedetails'][0]['pincode'];
	
 
	$data['subscriber_exist'] = $this->Subscription_model->subscriberexistchk($subscriberid);

	 if($data['subscriber_exist'][0]['subid']==0)
	 {	
		if($couponcode!='')
		{
			$validity_details=$this->Subscription_model->getconfigdetails($couponcode);
		}
		else
		{
			$validity_details = $this->Subscription_model->getDefaultPlandetails();
		}
		
		if($validity_details[0]['isasapenabled']==1)
		{
			$portal_type='ASAP';
		}
		else
		{
			$portal_type='CLP';
		}
		$profileimage="assets/images/avatar.png";
		$data['subscribeusers'] = $this->Subscription_model->subscribeusers($subscriberid,$salt1,$salt2,$email,$password,$gameplanid,$gradeid,$firstname,$lastname,$gender,$mobile,$address,$dob,$couponcode,$state,$city,$orgpwd,$pincode,$validity_details[0]['startdate'],$validity_details[0]['enddate'],$validity_details[0]['id'],$profileimage,$portal_type);
		$this->Subscription_model->UpdateCouponCount($couponcode);
		$this->Subscription_model->completestatus($userid);
/* $arrplan=$this->Subscription_model->getPlan($gradeid); */
// Email MSG for => Children 
$RegSucSub="Registration Successful - KinderAngels";
$RegSucEMSG='<table align="center" width="800px" border="1" cellspacing="0" cellpadding="0" style="font-size:medium;margin-right:auto;margin-left:auto;border:1px solid rgb(197,197,197);font-family:Arial,Helvetica,sans-serif;background-image:initial;background-repeat:initial;box-shadow: 10px 10px 5px #35395e;"><tbody><tr style="display:block;overflow:hidden;background: #1e88e5;"><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width:33%;color: #fff;"></td><td style="float:left;border:0px;text-align: center;padding: 5px 0px;width: 33%;color: #fff;"><img alt="www.kinderangels.com" src="'.base_url().'assets/images/new/skinder-web.png" style="float: left;width:220px;"></td><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width: 33%;color: #fff;"></td></tr><tr style="padding:0px;margin:10px 42px 20px;display:block;font-size:13px;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:justify"><td colspan="2" style="border:0px">Dear '.$firstname.',<br/><br/>Thank you for registering with <strong>KinderAngels</strong>.<br/><p><strong>Now !!</strong> you can unwrap the gift to enjoy & learn thinking skills</p><br/><br/>Following are the credentials to access the service at <a href="'.base_url().'" target="_blank" >'.base_url().'</a><br/><br/>username <strong> : '.$email.'</strong><br/>password<strong> : '.$orgpwd.'</strong><br/><br/><br/>All The Very Best!!!<br/><br/>Best Regards,<br/><strong>KinderAngels Team</strong><br/></td></tr><tr style=""><td style="text-align:center;color:#ee1b5b;border:0px;background-size:100%;background-image: url('.base_url().'assets/images/emailer/footer.jpg);padding-top:20px;padding-bottom:20px;font-family: cursive;font-size: 20px;"><div style="width:100%;font-family:; float:left;text-align:center"><a href="'.base_url().'" target="_blank" style="color:#ee1b5b;text-decoration: none;" >'.base_url().'</a><br/><a href="mailto:support@kinderangels.com"  style="color:#ee1b5b;text-decoration: none;" >support@kinderangels.com</a></div></td></tr><tr style="display:block;overflow:hidden">
<td style="float:left;border:0px;"></td></tr></tbody></table>';

// SMS MSG for => Children 
$RegSucSMSG="Dear ".$firstname.", you are successfully registered with KinderAngels. Username is ".$email." and password is ".$orgpwd.". Please visit ".base_url()." ";
/* echo $RegSucSMSG."<br/><br/>";
echo $RegSucEMSG."<br/><br/>"; */
$this->confirmation_email($email,$RegSucSub,$RegSucEMSG);//Mail Calling => Children
//$this->confirmation_sms($mobile,$RegSucSMSG);//SMS Calling => Children
		
		
		$this->load->view('header');
		$this->load->view('paymentresponse', $data);
		$this->load->view('footer');
	 }  
	 else
	 {
		 $this->load->view('header');
		$this->load->view('paymentfailure', $data);
		$this->load->view('footer');
	 }
 }
 
 /*----------------------- Forget Password ------------------ */
public function UserForgetPwdConfirmation()
{
	$username=$_POST['email'];
	$Emailexist= $this->Subscription_model->checkuseremailexist($username);
	
	if($Emailexist[0]['emailcount']==1)
	{	$randid=rand(1000000, 9999999);
		$qryinsertLog=$this->Subscription_model->forgetpwdlog($Emailexist[0]['id'],$randid);
		$subject = 'Reset Password Confirmation- KinderAngels';

		$message = '<table align="center" width="800px" border="1" cellspacing="0" cellpadding="0" style="font-size:medium;margin-right:auto;margin-left:auto;border:1px solid rgb(197,197,197);font-family:Arial,Helvetica,sans-serif;background-image:initial;background-repeat:initial;box-shadow: 10px 10px 5px #35395e;"><tbody><tr style="display:block;overflow:hidden;background: #1e88e5;"><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width:33%;color: #fff;"></td><td style="float:left;border:0px;text-align: center;padding: 5px 0px;width: 33%;color: #fff;"><img alt="www.kinderangels.com" src="'.base_url().'assets/images/new/skinder-web.png" style="float: left;width:220px;"></td><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width: 33%;color: #fff;"></td></tr>
		<tr style="padding:0px;margin:10px 42px 20px;display:block;font-size:13px;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:justify"><td colspan="2" style="border:0px">Dear '.$Emailexist[0]['fname'].',<br/><br/>Please click below link to reset your password.<br/><br/><br/><a href="'.base_url().'index.php/Subscription/change_password?rid='.md5($randid).'&uid='.md5($Emailexist[0]['id']).'" style="color: #fff;background: #ea0b72;padding: 12px;border-radius: 5px;font-weight: bold;text-decoration: none;" target="_blank" >Proceed </a><br/><br/><br/>Best Regards,<br/><strong>KinderAngels Team</strong><br/></td></tr><tr style=""><td style="text-align:center;color:#ee1b5b;border:0px;background-size:100%;background-image: url('.base_url().'assets/images/emailer/footer.jpg);padding-top:20px;padding-bottom:20px;font-family: cursive;font-size: 20px;"><div style="width:100%;font-family:; float:left;text-align:center"><a href="'.base_url().'" target="_blank" style="color:#ee1b5b;text-decoration: none;" >'.base_url().'</a><br/><a href="mailto:support@kinderangels.com"  style="color:#ee1b5b;text-decoration: none;" >support@kinderangels.com</a></div></td></tr><tr style="display:block;overflow:hidden"><td style="float:left;border:0px;"></td></tr></tbody></table>';
		/* echo $message."<br/><br/>";exit; */
		$this->confirmation_email($Emailexist[0]['email'],$subject,$message);//Mail Calling => Children
		echo 1;exit;
	}
	else
	{
		echo 0;exit;
	}
}
public function change_password()
{
	$userid=$_REQUEST['uid']; 
	$rid=$_REQUEST['rid'];
	if($userid!='' && $rid!='')
	{ 
		$isvaliduser=$this->Subscription_model->CheckIsValidUser($userid,$rid); 
		if($isvaliduser[0]['userid']!='')
		{}else{redirect("index.php");exit;}
	
		if(isset($_POST))
		{
			if(isset($_POST['txtOPassword']))
			{
			if($_POST['txtOPassword']==$_POST['txtCPassword'])
			{	
				// Generate two salts (both are numerical)
				$salt1 = mt_rand(1000,9999999999);
				$salt2 = mt_rand(100,999999999);
				// Append our salts to the password
				$salted_pass = $salt1.$_POST['txtOPassword'].$salt2;
				// Generate a salted hash
				$pwdhash = sha1($salted_pass);
				
				$arruserdetails=$this->Subscription_model->getResetpwdUserDetails($isvaliduser[0]['userid']);
				//echo "<pre>";print_r($arruserdetails);exit;
				$exepwdupdate=$this->Subscription_model->ResetUserPwd($pwdhash,$userid,$salt1,$salt2,$_REQUEST['txtOPassword'],$arruserdetails[0]['username']);
				$exelogupdate=$this->Subscription_model->ResetUserPwd_log($userid,$rid,$_REQUEST['txtOPassword']);

				$baseurl="http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
				$subject = 'Reset Password Successfully - KinderAngels';
				$message = '<table align="center" width="800px" border="1" cellspacing="0" cellpadding="0" style="font-size:medium;margin-right:auto;margin-left:auto;border:1px solid rgb(197,197,197);font-family:Arial,Helvetica,sans-serif;background-image:initial;background-repeat:initial;box-shadow: 10px 10px 5px #35395e;"><tbody><tr style="display:block;overflow:hidden;background: #1e88e5;"><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width:33%;color: #fff;"></td><td style="float:left;border:0px;text-align: center;padding: 5px 0px;width: 33%;color: #fff;"><img alt="www.kinderangels.com" src="'.base_url().'assets/images/new/skinder-web.png" style="float: left;width:220px;"></td><td style="float:left;border:0px;text-align: center;padding: 10px 0px;width: 33%;color: #fff;"></td></tr><tr style="padding:0px;margin:10px 42px 20px;display:block;font-size:13px;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:justify"><td colspan="2" style="border:0px">Dear '.$arruserdetails[0]['fname'].',<br/><br/>
				Your password has been reset successfully.<br/><br/> Now, You can login <a href="'.base_url().'" target="_blank" >'.base_url().'</a>  with the following credentials <br/><br/>Username <strong> : '.$arruserdetails[0]['email'].'</strong><br/>Password<strong> : '.$_REQUEST['txtOPassword'].'</strong><br/><br/><br/>All The Very Best!!!<br/><br/>Best Regards,<br/><strong>KinderAngels Team</strong><br/></td></tr><tr style=""><td style="text-align:center;color:#ee1b5b;border:0px;background-size:100%;background-image: url('.base_url().'assets/images/emailer/footer.jpg);padding-top:20px;padding-bottom:20px;font-family: cursive;font-size: 20px;"><div style="width:100%;font-family:; float:left;text-align:center"><a href="'.base_url().'" target="_blank" style="color:#ee1b5b;text-decoration: none;" >'.base_url().'</a><br/><a href="mailto:support@kinderangels.com"  style="color:#ee1b5b;text-decoration: none;" >support@kinderangels.com</a></div></td></tr><tr style="display:block;overflow:hidden"><td style="float:left;border:0px;"></td></tr></tbody></table>';
				
$USERMSG="Dear ".$arruserdetails[0]['fname'].", you have successfully reset the password. Username is ".$arruserdetails[0]['username']." and password is ".$_REQUEST['txtOPassword'].". Please visit ".base_url()." ";
			
			//Create a new PHPMailer instance
			$this->confirmation_email($arruserdetails[0]['email'],$subject,$message);//Mail Calling => Children
			//$this->confirmation_sms($arruserdetails[0]['mobile'],$USERMSG);
			
				$data['response']='Password Changed successfully';
			}
			else
			{
				$data['response']='Password does not match';
			}
			}
		}
		$this->load->view('header');
		$this->load->view('change_password', $data);
		$this->load->view('footer');
	}
	else
	{
		redirect("index.php");  exit;
	}
}
 
}
