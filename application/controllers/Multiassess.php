<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multiassess extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->model('Multiassess_model');
		$this->load->library('My_PHPMailer');
		$this->load->library('session');
	}
		
	public function index()
	{
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
/*------ Multi Assessment Start----------*/
	public function asap()
	{	
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
		if($this->session->isasapenabled==0)
		{
			redirect('index.php/home/dashboard');
		}
		
		$data['userid']=$this->session->user_id;
		$data['org_id']=$this->session->org;
		
		
		$this->load->view('headerinner',$data);
		$this->load->view('multiassess/gamepuzzle', $data);
		$this->load->view('footerinner');
	}
	public function masap_ajax()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
		$userid=$this->input->post('uid');
		$org_id=$this->session->org;
		
		
			$data['MultiAssesGameList'] = $this->Multiassess_model->getMultiAssesGameList($this->session->game_grade,$this->session->org);
			$data['gamescore'] = $this->Multiassess_model->getGameScore($userid,$this->session->org);
			
			$data['curorgid']=$org_id;
			$data['userid']=$userid;
			//echo "<pre>";print_r($data);exit;
			$this->load->view('multiassess/gamepuzzle_ajax', $data);
		
	}
	public function gamesajax()
	{	
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		$gameurl =  $this->input->post('gameurl'); 
		$uid =  $this->input->post('uid'); 
		$org_id=$this->session->org; 
		//echo "<pre>";print_r($_POST);exit;
		$gname = substr($gameurl, strrpos($gameurl, '/') + 1);
		$gamename = str_replace('.html','', $gname); 
		
			$data['gameid'] = $this->Multiassess_model->getgameid($gamename);
			$gameid = $data['gameid'][0]['gid']; 
			$data['checkgame'] = $this->Multiassess_model->checkgame($org_id,$gameid,$this->session->game_grade);
			
			$gid = $data['checkgame'][0]['isexist']; 
			$game_limt=$this->config->item('agame_limit');
			
			if($gid==1 && ($game_limt>$data['checkgame'][0]['playedgamescount']))
			{
				$this->session->set_userdata(array('isasap'=> 1,'currentgameid'=> $gameid,'currentuid'=>$uid,'currentorg_id'=>$org_id));
				echo $this->session->currentgameid;exit;
			}
			else
			{
				echo 'IA'; exit;
			}
	}
	public function result()
	{

		if(isset($this->session->currentuid) && $this->session->currentuid!='')
		{
			$postdata = $this->session->flashdata('MASAP_post_data');
			//echo "<pre>";print_r($postdata);exit;
			$total_ques=$postdata["tqcnt1"];
			if($postdata["aqcnt1"]>10){
				$attempt_ques=10;
			}
			else{
				$attempt_ques=$postdata["aqcnt1"];
			}

			$answer=$postdata["cqcnt1"];
			$score=$postdata["gscore1"];
			$a6=$postdata["gtime1"];
			$a7=$postdata["rtime1"];
			$a8=$postdata["crtime1"];
			$a9=$postdata["wrtime1"];
			
			$gameid=$this->session->currentgameid;
			$userid = $this->session->currentuid;
			$org_id = $this->session->currentorg_id;
			$lastup_date = date("Y-m-d");
			$cid = 1;
			$data['gameDetails'] = $this->Multiassess_model->getresultGameDetails($userid,$gameid);
			$skillid =$data['gameDetails'][0]['gameskillid'] ; 
			//$createddate = NOW();
			$pid =$data['gameDetails'][0]['gp_id'];
			
			if($gameid==0)
			{
				echo '-2';exit;
			}
			
			$data['insert1'] = $this->Multiassess_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$org_id);
			
			$arrskillid=array("59"=>"ME","60"=>"VP","61"=>"FA","62"=>"PS","63"=>"LI");
			
			$isinserorupdate=$this->Multiassess_model->checkinsertorupdate($userid,$org_id);
			if($isinserorupdate[0]['isplayed']>0)
			{
				$this->Multiassess_model->UpdateGameScore($userid,$org_id,$score,$arrskillid[$skillid]);
				$isasaponly=$this->Multiassess_model->organization_moduletype($org_id);
				if($isasaponly[0]['asaponly']=='N')
				{
					$isplayed=$this->Multiassess_model->checkUserCompletedMasap($userid,$org_id);
					if($isplayed[0]['asapcount']==1)
					{
						 $this->Multiassess_model->UpdateUserMasapStatus($userid,$org_id);
						//$MSG="Dear ".$data['gameDetails'][0]['doctorname'].", Child ".$data['gameDetails'][0]['fname']." has completed the assessment - ";
						//$this->confirmation_sms($data['gameDetails'][0]['mobilenumber'],$MSG);
					}
				}
			}
			else
			{
				$this->Multiassess_model->InsertGameScore($userid,$org_id,$score,$arrskillid[$skillid]);
			}
			$this->session->unset_userdata('currentorg_id');
			$this->session->unset_userdata('isasap');
			$this->session->unset_userdata('currentuid');
			
			echo 1;exit;
		}
	}
	public function generatereport()
	{	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
		$userid = $this->uri->segment(3);
		$masapid = $this->uri->segment(4);
		
		$userdetails = $this->Multiassess_model->getusername($userid);
		$gamescore = $this->Multiassess_model->getGameScore($userid,$masapid);
	
	//header('Content-Type: image/png');
	$username=$userdetails[0]['fname'];
	$grade_name='';$school_name='';
	$BSPIScore=$gamescore[0]['bspi'];
	/* Skill Scores */
	$fMemoryScore=$gamescore[0]['ME'];
	$fVisualProcessingScore=$gamescore[0]['VP'];
	$fFocusAndAttentionScore=$gamescore[0]['FA'];
	$fProblemSolvingScore=$gamescore[0]['PS'];
	$fLinguisticsScore=$gamescore[0]['LI'];
	/* Skill Wise Scores */
	/* Stars */	
	$filled_stars=0;
	if($fMemoryScore < 20){$filled_stars =$filled_stars+0;}
	if($fMemoryScore >= 20 && $fMemoryScore <= 40){$filled_stars =$filled_stars+1;}
	if($fMemoryScore >= 41 && $fMemoryScore <= 60){$filled_stars =$filled_stars+2;}
	if($fMemoryScore >= 61 && $fMemoryScore <= 80){$filled_stars =$filled_stars+3;}
	if($fMemoryScore >= 81 && $fMemoryScore <= 90){$filled_stars =$filled_stars+4;}
	if($fMemoryScore >= 91 && $fMemoryScore <= 100){$filled_stars =$filled_stars+5;}
	
	if($fVisualProcessingScore < 20){$filled_stars =$filled_stars+0;}
	if($fVisualProcessingScore >= 20 && $fVisualProcessingScore <= 40){$filled_stars =$filled_stars+1;}
	if($fVisualProcessingScore >= 41 && $fVisualProcessingScore <= 60){$filled_stars =$filled_stars+2;}
	if($fVisualProcessingScore >= 61 && $fVisualProcessingScore <= 80){$filled_stars =$filled_stars+3;}
	if($fVisualProcessingScore >= 81 && $fVisualProcessingScore <= 90){$filled_stars =$filled_stars+4;}
	if($fVisualProcessingScore >= 91 && $fVisualProcessingScore <= 100){$filled_stars =$filled_stars+5;}
	
	if($fFocusAndAttentionScore < 20){$filled_stars = $filled_stars+0;}
	if($fFocusAndAttentionScore >= 20 && $fFocusAndAttentionScore <= 40){$filled_stars =$filled_stars+1;}
	if($fFocusAndAttentionScore >= 41 && $fFocusAndAttentionScore <= 60){$filled_stars =$filled_stars+2;}
	if($fFocusAndAttentionScore >= 61 && $fFocusAndAttentionScore <= 80){$filled_stars =$filled_stars+3;}
	if($fFocusAndAttentionScore >= 81 && $fFocusAndAttentionScore <= 90){$filled_stars =$filled_stars+4;}
	if($fFocusAndAttentionScore >= 91 && $fFocusAndAttentionScore <= 100){$filled_stars =$filled_stars+5;}
	
	if($fProblemSolvingScore < 20){$filled_stars = $filled_stars+0;}
	if($fProblemSolvingScore >= 20 && $fProblemSolvingScore <= 40){$filled_stars =$filled_stars+1;}
	if($fProblemSolvingScore >= 41 && $fProblemSolvingScore <= 60){$filled_stars =$filled_stars+2;}
	if($fProblemSolvingScore >= 61 && $fProblemSolvingScore <= 80){$filled_stars =$filled_stars+3;}
	if($fProblemSolvingScore >= 81 && $fProblemSolvingScore <= 90){$filled_stars =$filled_stars+4;}
	if($fProblemSolvingScore >= 91 && $fProblemSolvingScore <= 100){$filled_stars =$filled_stars+5;}
	
	if($fLinguisticsScore < 20){$filled_stars = $filled_stars+0;}
	if($fLinguisticsScore >= 20 && $fLinguisticsScore <= 40){$filled_stars =$filled_stars+1;}
	if($fLinguisticsScore >= 41 && $fLinguisticsScore <= 60){$filled_stars =$filled_stars+2;}
	if($fLinguisticsScore >= 61 && $fLinguisticsScore <= 80){$filled_stars =$filled_stars+3;}
	if($fLinguisticsScore >= 81 && $fLinguisticsScore <= 90){$filled_stars =$filled_stars+4;}
	if($fLinguisticsScore >= 91 && $fLinguisticsScore <= 100){$filled_stars =$filled_stars+5;}
		
	/* Star */
	$Greeting1='Wow !!!';
	$Greeting2='Good !!!';
	$Greeting3='Nice !!!';
	$Greeting4='Super !!!';
	$Greeting5='Great !!!';
	$Greeting6='Excellent !!!';
	$Greeting7='Wonderful !!!';
	$Greeting8='Awesome !!!';
	$Greeting9='Terrific !!!';
	$Greeting10='Fantasticc !!!';
	$BackgroundImage= base_url().'assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';
	//$BackgroundImage= '/mnt/vol1/sites/nschools/assessments/assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';

	$img = $this->LoadPNG($BackgroundImage,$username,$grade_name,$school_name,$filled_stars,$BSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$Greeting1,$Greeting2,$Greeting3,$Greeting4,$Greeting5,$Greeting6,$Greeting7,$Greeting8,$Greeting9,$Greeting10);
	
	imagepng($img);
	imagedestroy($img);
}
public function LoadPNG($fBackgroundImage,$name,$grade_name,$school_name,$fTotalScore,$fBSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$fGreeting1,$fGreeting2,$fGreeting3,$fGreeting4,$fGreeting5,$fGreeting6,$fGreeting7,$fGreeting8,$fGreeting9,$fGreeting10)
{ //echo $fGreeting4;exit;
		 /* Attempt to open */
    $im = @imagecreatefrompng($fBackgroundImage);
    /* See if it failed */
    if(!$im)
    {
      echo "Loading bg has issue ".$fBackgroundImage;
    }
	 
	// Create some colors
	$white = imagecolorallocate($im, 255, 255, 255);
	$grey = imagecolorallocate($im, 128, 128, 128);
	$black = imagecolorallocate($im, 0, 0, 0);


	// The text to draw
	// Replace path by your own font path
	$font = APPPATH."../assets/fonts/OpenSansBold.ttf";
	//$font = "/mnt/vol1/sites/gtec/assess/assets/fonts/OpenSansBold.ttf";
	// Add some shadow to the text
//echo $font;exit;
	$text_color = imagecolorallocate($im, 0, 0, 0);	
	imagestring($im, 3, 90, 68, $name, $text_color);
	imagestring($im, 3, 90, 87, str_replace("Class-",'',$grade_name), $text_color);
	imagestring($im, 3, 90, 106, $school_name, $text_color);
	
	/* BSPIScore */ 
	$text_color = imagecolorallocate($im, 255, 255, 255);
	if (strlen($fBSPIScore) == 1)
    {
     imagettftext ($im, 25, 0, 195, 350, $text_color, $font, $fBSPIScore);
    }

	 elseif (strlen($fBSPIScore) > 2)
    {
     imagettftext ($im, 25, 0, 170, 350, $text_color, $font, $fBSPIScore);
    }
	
		elseif (strlen($fBSPIScore) > 3)
    {
     imagettftext ($im, 25, 0, 100, 350, $text_color, $font, $fBSPIScore);
    }
	else
    {
     imagettftext ($im, 25, 0, 180, 350, $text_color, $font, $fBSPIScore);
    }

/* TotalScore */ 
    if (strlen($fTotalScore) == 1)
    {
    imagettftext ($im, 50, 0, 570, 300, $text_color, $font, $fTotalScore);
    }
    elseif (strlen($fTotalScore) > 2)
    {
     imagettftext ($im, 50, 0, 530, 300, $text_color, $font, $fTotalScore);
    }
	else
    {
     imagettftext ($im, 50, 0, 550, 300, $text_color, $font, $fTotalScore);
    }
	
	
	
	/* MemoryScore */ 
	$text_color = imagecolorallocate($im, 255, 0, 0);
	
	if (strlen($fMemoryScore) == 1)
    {
      imagettftext ($im, 30, 0, 263, 510, $text_color, $font, $fMemoryScore);
    }
    elseif (strlen($fMemoryScore) > 2)
    {
     imagettftext ($im, 30, 0, 243, 510, $text_color, $font, $fMemoryScore);
    }
	else
    {
      imagettftext ($im,  30, 0, 253, 510, $text_color, $font, $fMemoryScore);
    }
	/* VisualProcessingScore */ 
	$text_color = imagecolorallocate($im, 255, 153, 51);
	
	if (strlen($fVisualProcessingScore) == 1)
	{
   
    imagettftext ($im, 30, 0, 373, 510, $text_color, $font, $fVisualProcessingScore);
   }
    elseif (strlen($fVisualProcessingScore) > 2)
    {
      imagettftext ($im, 30, 0, 353, 510, $text_color, $font, $fVisualProcessingScore);
    }
	else
    {
       imagettftext ($im, 30, 0, 363, 510, $text_color, $font, $fVisualProcessingScore);
    }
   /* FocusAndAttentionScore */ 
	$text_color = imagecolorallocate($im, 102, 204, 0);
	
	if (strlen($fFocusAndAttentionScore) == 1)
   {
    imagettftext ($im, 30, 0, 484, 510, $text_color, $font, $fFocusAndAttentionScore);
   }
    elseif (strlen($fFocusAndAttentionScore) > 2)
    {
      imagettftext ($im, 30, 0, 464, 510, $text_color, $font, $fFocusAndAttentionScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 474, 510, $text_color, $font, $fFocusAndAttentionScore);
    }
   /* ProblemSolvingScore */ 
	$text_color = imagecolorallocate($im, 230, 107, 25);
	
	if (strlen($fProblemSolvingScore) == 1)
  {
   imagettftext ($im, 30, 0, 594, 510, $text_color, $font, $fProblemSolvingScore);
   }
  elseif (strlen($fProblemSolvingScore) > 2)
    {
      imagettftext ($im,30, 0, 574, 510, $text_color, $font, $fProblemSolvingScore);
    }
	else
    {
       imagettftext ($im,30, 0, 584, 510, $text_color, $font, $fProblemSolvingScore);
    }
   /* LinguisticsScore */ 
	$text_color = imagecolorallocate($im, 51, 204, 255);
	
	if (strlen($fLinguisticsScore) == 1)
  {
  imagettftext ($im, 30, 0, 705, 510, $text_color, $font, $fLinguisticsScore);
  }
  elseif (strlen($fLinguisticsScore) > 2)
    {
      imagettftext ($im, 30, 0, 685, 510, $text_color, $font, $fLinguisticsScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 695, 510, $text_color, $font, $fLinguisticsScore);
    }
	/* Greetings Range */
	$text_color = imagecolorallocate($im, 0, 0, 0);
	
  if ($fBSPIScore < 11)
  {
  imagestring($im, 5, 0, 85, str_pad($fGreeting1,150," ",STR_PAD_BOTH), $text_color);
   
  }
  elseif ($fBSPIScore < 21)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting2,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	elseif ($fBSPIScore < 31)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting3,150," ",STR_PAD_BOTH), $text_color);
	  
    }
	 elseif ($fBSPIScore < 41)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting4,150," ",STR_PAD_BOTH), $text_color);
	
    }
	 elseif ($fBSPIScore < 51)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting5,150," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 61)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting6,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	 elseif ($fBSPIScore < 71)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting7,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	 elseif ($fBSPIScore < 81)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting8,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	elseif ($fBSPIScore < 91)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting9,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	else
    {
imagestring($im, 5, 0, 77, str_pad($fGreeting10,150," ",STR_PAD_BOTH), $text_color);
    }
    return $im;
	}
/*------ Multi Assessment End----------*/
public function MySparkies()
{
	$sparkies_total_points=$this->Multiassess_model->getMyCurrentSparkies($this->session->school_id,$this->session->game_grade,$this->session->user_id,$this->session->astartdate,$this->session->aenddate); 	
	if ($this->session->user_id && isset($this->session->issparkies)){ 
		if($this->session->sparkiespoints!=0){
			$sparkiestotpoints=$sparkies_total_points[0]['mysparkies']-$this->session->sparkiespoints;
		}	
		else{ 
			$sparkiestotpoints=$sparkies_total_points[0]['mysparkies'];
		}		
	}
	else{ 
			$sparkiestotpoints=$sparkies_total_points[0]['mysparkies'];
	}

	return $sparkiestotpoints;		
}

public function masap_cron()
{
	$isuserEligible=$this->Multiassess_model->getEligibleUserList();
	foreach($isuserEligible as $res)
	{	
		//echo "<pre>";print_r($isuserEligible);exit;
		if($res['takensession']>=$res['MaxSessionLimit'])
		{
			$asapid=$res['MasapPlayedCount']+1;
		}
		else
		{
			$asapid=$res['MasapPlayedCount'];
		}
		$masapid=$asapid;

		if(($res['MasapPlayedCount']<$asapid))
		{
			$ishave=$this->Multiassess_model->checkUserhavemasap($res['id'],$masapid);
			if($ishave[0]['isexist']==0)
			{
				$this->Multiassess_model->InsertMasapData($res['id'],$masapid,$res['doctorid']);
				
				$MSG="Dear ".$res['doctorname'].", Child ".$res['fname']." is ready to take assessment";
				//$this->confirmation_sms($res['mobilenumber'],$MSG);
			}
			else
			{
				echo "Data already inserted : ".$res['id'];
			}
		}
	}
}
public function confirmation_sms($tonum,$message)
{
	/* $baseurl="https://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
	$ch = curl_init("http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=Ac79406bbeca30e25c926bad8928bcc17&to=".$tonum."&sender=SBRAIN&message=".urlencode($message));

	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response=curl_exec($ch);       
	curl_close($ch); */
}

public function org_topperslist()
{
	$isasaponly=$this->Multiassess_model->organization_moduletype($this->session->org);
	if($isasaponly[0]['asaponly']=='Y')
	{
		$data['minscore']=$this->Multiassess_model->maxscore_byorg($this->session->org,$this->session->game_grade);
		$minscore = $data['minscore'][0]['val'];
		$data['query']=$this->Multiassess_model->Topperlist_byorg($minscore,$this->session->org,$this->session->game_grade);
		$this->load->view('multiassess/topperslist', $data);
	}
	else
	{
		echo 0;exit;
	}
}

	public function AsapEndPopup()
	{	
		$userid = $this->session->user_id;
		$org_id = $this->session->org;
		$isplayed=$this->Multiassess_model->checkUserCompletedMasap($userid,$org_id);
			if($isplayed[0]['asapcount']==1)
			{
				echo 1;exit;
			}
			else
			{
				echo 0;exit;
			}		
	}


}
