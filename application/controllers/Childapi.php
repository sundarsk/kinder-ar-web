<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Childapi extends CI_Controller {
var $info;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		//$this->lang->load("menu_lang","english");
		$this->load->model('Arapi_model');
		$this->load->library('session');	
		$this->load->library('My_PHPMailer');
		$this->info=array("secret_key"=>"Ed6S@2018","default_pwd"=>"skillangels","default_play_time"=>$this->config->item('game_limit')); 
    }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}
	
	public function checklogin()
	{
		$username = $this->input->post('Username');
		$password = $this->input->post('Password');
		$deviceid = $this->input->post('DeviceId');
		$Key = $this->input->post('Key');
		$timestamp = $this->input->post('ts');
		
		if(($username!='') && ($password!='') && ($deviceid!='') && ($Key!='') && ($timestamp!=''))
		{			 
			$token=$this->encMethod($timestamp,$deviceid,$this->info['secret_key']); 
			 
			if($Key==$token['Hash'])
			//if(1==1)
			{
				$data['query'] = $this->Arapi_model->checkChildLogin($username,$password,$deviceid);
				if(isset($data['query'][0]->id))
				{
						
					if(($data['query'][0]->deviceid!='') && ($data['query'][0]->deviceid!=$deviceid))
					{
						$resparr=array("status"=>"Failed","UserId"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"msg"=>"User Already Login in some Devices");
						echo json_encode($resparr);exit;
					}
					$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);
					$this->Arapi_model->update_loginDetails($data['query'][0]->id,$uniqueId);
					
					if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
						 $ip=$_SERVER['HTTP_CLIENT_IP'];}
						 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
						 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
						 $ip=$_SERVER['REMOTE_ADDR'];}
						 
					$this->Arapi_model->insert_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
					
					$this->Arapi_model->updateDeviceID($data['query'][0]->id,$deviceid,'');
					
					/*.... Child Assigned & Played Game....*/
					$arrAssignedgame= $this->Arapi_model->GetAssignedGameList($data['query'][0]->id);
					$arrPlayedGamedata= $this->Arapi_model->GetPlayedGameData($data['query'][0]->id);					
					/*.... Child Assigned Game....*/
					
					/*......... Ramaing Time Limit ...........*/
					$arrRemainingTime= $this->Arapi_model->GetRemainingTime($data['query'][0]->id);
					if($arrRemainingTime[0]['isexist']==0)
					{
						$this->Arapi_model->InsertRemainingTime($data['query'][0]->id,$data['query'][0]->time_limit);
						$RemainingTime=$data['query'][0]->time_limit;
					}
					else
					{
						$RemainingTime=$arrRemainingTime[0]['RemainingTime'];
					}
					/*......... Ramaing Time Limit ............*/
					
					$resparr=array("status"=>"success","ChildId"=>$data['query'][0]->id,"Username"=>$data['query'][0]->username,"portal_type"=>$data['query'][0]->portal_type,"startdate"=>$data['query'][0]->startdate,"enddate"=>$data['query'][0]->enddate,"ParentID"=>$data['query'][0]->parent_id,"grade_id"=>$data['query'][0]->grade_id,"AssginedGame"=>$arrAssignedgame,"PlayedGamedata"=>$arrPlayedGamedata,"SpecialChild"=>$data['query'][0]->isspecialchild,"Fname"=>$data['query'][0]->fname,"Time_Limit"=>$data['query'][0]->time_limit,"RemainingTime"=>$RemainingTime,"msg"=>"User Login successfully");
					echo json_encode($resparr);
					//echo "<pre>";print_r($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","ChildId"=>'-',"Username"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"grade_id"=>"-","AssginedGame"=>"-","PlayedGamedata"=>"-","SpecialChild"=>"-","Fname"=>"-","Time_Limit"=>"-","RemainingTime"=>"-","msg"=>"Username or Password Wrong");
					echo json_encode($resparr);
				}
			}
			else
			{
				$resparr=array("status"=>"Failed","ChildId"=>'-',"Username"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"grade_id"=>"-","AssginedGame"=>"-","PlayedGamedata"=>"-","SpecialChild"=>"-","Fname"=>"-","Time_Limit"=>"-","RemainingTime"=>"-","msg"=>"Key Mismatch");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"Failed","ChildId"=>'-',"Username"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"grade_id"=>"-","AssginedGame"=>"-","PlayedGamedata"=>"-","SpecialChild"=>"-","Fname"=>"-","Time_Limit"=>"-","RemainingTime"=>"-","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}
	
	public function insertassignedgame()
	{
		$username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		$AssignedGameList = $this->input->post('AssignedGameList');
		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($AssignedGameList!='') )
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$response= $this->Arapi_model->InsertAssignedGame($data['query'][0]->id,$AssignedGameList);
					if($response>0)
					{
						$resparr=array("status"=>"success","response"=>1,"noofrecordinserted"=>$response,"msg"=>"Data inserted successfully");
						echo json_encode($resparr);exit;
					}
					else
					{
						$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Data insertion failed");
						echo json_encode($resparr);
					}
				}
				else 
				{
					
					$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				
				$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}	
	public function updateassignedgame()
	{
		$username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		/* $GameId = $this->input->post('GameId'); */
		$Assigned_id = $this->input->post('Assigned_id');
		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($Assigned_id!='') )
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$arrstatus=$this->Arapi_model->getAssginedGameStatus($data['query'][0]->id,$Assigned_id);
				if($arrstatus[0]['Status']=='' || $arrstatus[0]['Status']==0)
					{
						$response= $this->Arapi_model->UpdateAssginedGame($data['query'][0]->id,$Assigned_id);
						if($response>0)
						{
							$resparr=array("status"=>"success","response"=>1,"noofrecordinserted"=>$response,"msg"=>"Data updated successfully");
							echo json_encode($resparr);exit;
						}
						else
						{
							$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Data insertion failed");
							echo json_encode($resparr);
						}
					}
					else
					{
						$resparr=array("status"=>"success","response"=>1,"noofrecordinserted"=>$arrstatus[0]['Status'],"msg"=>"Data updated successfully");
						echo json_encode($resparr);exit;
					}
				}
				else 
				{
					
					$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				
				$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}
	
	public function insertgamedata()
	{
		$username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		$GameData = $this->input->post('GameData');
		$RemainingTime = $this->input->post('RemainingTime');
		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($GameData!='') && ($RemainingTime!='') )
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$response= $this->Arapi_model->InsertGameData($data['query'][0]->id,$GameData);
							   $this->Arapi_model->UpdateRemainingTime($data['query'][0]->id,$RemainingTime);
					if($response>0)
					{
						$resparr=array("status"=>"success","response"=>1,"noofrecordinserted"=>$response,"msg"=>"Data inserted successfully");
						echo json_encode($resparr);exit;
					}
					else
					{
						$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Data insertion failed");
						echo json_encode($resparr);
					}
				}
				else 
				{
					
					$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				
				$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"failure","response"=>0,"noofrecordinserted"=>"-","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}	
		
	public function logout()
	{
		$username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
		$RemainingTime = $this->input->post('RemainingTime');
 		 
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($RemainingTime!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				//if(1==1)
				{
					$this->Arapi_model->updateDeviceID($username,'','');
					$this->Arapi_model->update_logout_log($data['query'][0]->id,$data['query'][0]->session_id);
					$this->Arapi_model->updateuserloginstatus($data['query'][0]->id,$data['query'][0]->session_id);
					$this->Arapi_model->UpdateRemainingTime($data['query'][0]->id,$RemainingTime);
					$resparr=array("status"=>"success","msg"=>"Device Id cleared successfully");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr=array("status"=>"Failed","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}
	public function timeover()
	{
		$username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
		$RemainingTime = $this->input->post('RemainingTime');
 		 
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($RemainingTime!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				//if(1==1)
				{
					$this->Arapi_model->UpdateRemainingTime($data['query'][0]->id,$RemainingTime);
					$resparr=array("status"=>"success","msg"=>"Remaining time updated successfully");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr=array("status"=>"Failed","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}
	
	public function salt_my_pass($password)
	{
		$salt1 = mt_rand(1000,9999999999);
		$salt2 = mt_rand(100,999999999);
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	} 
	public function encMethod($salt1,$password,$salt2)
	{
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	} 
	
	

	
public function sendPush($body,$mtoken,$icon,$sound,$imgflag)
{
	#API access key from Google API's Console
	define( 'API_ACCESS_KEY', 'AIzaSyDCUVLX4XRQrGV98uxSwl1Oq41ty042LdU' );
	//$registrationIds = 'fLoWjWYwig8:APA91bHl736oFoJrklskahkQE5rMY0Zb5FsqExflhbfcsezrGUkFz_qXZ6_fkvHSwMKjQIp7Rgs58hik_h9qiUxbzPNgdj-6fHAMYGV0ToLS6fSct38Vx1EsVOzNlegNVmDxAGEQ9I8P';
	$registrationIds=$mtoken;
	#prep the bundle
	$msg = array
	  (
	'body' 	=> $body,
	'title'	=> 'Summer Champ - Notification',
	'icon'	=>$icon,
	'sound' =>$sound,
	'tag'=>$imgflag
	);
	$fields = array
	(
		'to'		=> $registrationIds,
		'notification'	=> $msg
	);


	$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
	#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );
	#Echo Result Of FireBase Server
	//echo $result;
}

/*....... M Token Update ........*/
	public function mtoken()
	{
		$username = $this->input->post('UserId');
		$mtoken = $this->input->post('mtoken');
		if(($username!='') && ($mtoken!=''))
		{
			$isparent= $this->Arapi_model->checkParentExist($username);				
			if(isset($isparent[0]->id))
			{
				$data['query'] = $this->Arapi_model->checkParentExist($username);
			}
			else
			{
				$data['query'] = $this->Arapi_model->checkuserexist($username);
			}	
			
			if(isset($data['query'][0]->id))
			{
				if(isset($isparent[0]->id))
				{
					$this->Arapi_model->update_parent_Mtoken($username,$mtoken);
				}
				else
				{
					$this->Arapi_model->updateMtoken($username,$mtoken);
				}
				
				$resparr=array("status"=>"success","msg"=>"mtoken Updated Successfully");
				echo json_encode($resparr);
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr=array("status"=>"Failed","msg"=>"Invalid User");
			echo json_encode($resparr);
		}
	}
	
	public function arresult()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		
		$gamename=$this->input->post('gameID');		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($gamename!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{
					$data['gameid'] = $this->Arapi_model->getgameid($gamename);
					$gameid = $data['gameid'][0]['gid'];
					if($gameid!='')
					{
						$data['checkgame'] = $this->Arapi_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);

						$game_limit=$this->info['default_play_time'];
						
						if($game_limit>$data['checkgame'][0]['played_time'])
						{ 
							$postdata = $_POST;
							$total_ques=$postdata["total_ques"];
							if($postdata["attempt_ques"]>10){
								$attempt_ques=10;
							}
							else{
								$attempt_ques=$postdata["attempt_ques"];
							}

							$answer=$postdata["answer"];
							$score=$postdata["gamescore"];
							$a6=$postdata["gtime1"];
							$a7=$postdata["rtime1"];
							$a8=$postdata["crtime1"];
							$a9=$postdata["wrtime1"];	
							
							$skillkit=0;
							$userlang = 1;
							$userid = $data['query'][0]->id; 
							$lastup_date = date("Y-m-d");
							$cid = 1;
							$data['gameDetails'] = $this->Arapi_model->getresultGameDetails($userid,$gameid);
							$skillid =$data['gameDetails'][0]['gameskillid'] ; 
							$schedule_val = 0;
							$pid =  $data['query'][0]->gp_id; 
							
							$data['insert1'] = $this->Arapi_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val);
						
							$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);

							/*--- Sparkies ----*/
							$sparkies_output=$this->Arapi_model->insertsparkies($arrofinput);
							//echo $sparkies_output[0]['OUTPOINTS'];exit; 
							
							$resparr=array("status"=>"success","crownypoints"=>$sparkies_output[0]['OUTPOINTS'],"response"=>1,"msg"=>"-");
							echo json_encode($resparr);
						}
						else
						{
							$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-1","msg"=>"Game limit expired");
							echo json_encode($resparr);
						}
					}
					else
					{
						$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-2","msg"=>"Game not avialable");
						echo json_encode($resparr);
					}
				}
				else 
				{
					$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{			
				$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr= array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
	}
	
	
}