<?php //echo date("F", mktime(null, null, null, 2));
//echo "<pre>"; print_r($academicmonths);exit;?>
<link href="<?php echo base_url(); ?>assets/css/datatbl/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/datatbl/fixedColumns.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/datatbl/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatbl/dataTables.fixedColumns.min.js"></script>
<div class="clear_both"></div>
 <div class="">
 <div class="container">
 <h2>Leader Board:</h2>
 <div class="form_sec">
<div class="row">

<div id="LeaderBoardpage"> 
<table class="stripe row-border order-column" width="100%" id="LeaderBoard">
<thead>
      <tr>
        <th></th>
<?php foreach($academicmonths as $months){ 

if((date('m')==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-1 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-2 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))))
{$class="inactive";}else{$class="inactive";}?>
		<th class="<?php echo $class; ?>"><?php echo date("F", mktime(0, 0, 0, $months['monthNumber'], 10)).'-'.$months['yearNumber']; //echo date("F", mktime(null, null, null, $months['monthNumber'])); ?></th>
<?php } ?>
        </tr>
    </thead>
    <tbody>
	<tr>
	<td class="maincol"><div class="heading">Crownies</div><div class="content"><img src="<?php echo base_url() ?>assets/images/sparkies/6.gif" width="50" height="50" data-toggle="tooltip" title="Top crowny scorer!"></div></td>
	<?php    //echo "<pre>";print_r($getSuperAngels);exit;
		foreach($academicmonths as $months){ 
		if((date('m')==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-1 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-2 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))))
		{$class="inactive";}else{$class="inactive";}
	?>
		<td <?php echo $class; ?> ><?php echo str_replace(',',',<br/> ',$CrownyBadge[$months['monthNumber'].'-'.$months['yearNumber']]); ?></td>
	<?php } ?>
	</tr>
	<tr>
	<td class="maincol"><div class="heading">Super Brain-Badge</div><div class="content"><img src="<?php echo base_url() ?>assets/images/sparkies/SuperBrain-Badge.png" width="50" height="50" data-toggle="tooltip" title="Highest BSPI scorer"></div></td>
	<?php    //echo "<pre>";print_r($superbrain);exit;
		foreach($academicmonths as $months){
		if((date('m')==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-1 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-2 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))))
		{$class="active";}else{$class="inactive";}			
	?>
		<td <?php echo $class; ?> ><?php echo str_replace(',',',<br/> ',$SuperBrainBadge[$months['monthNumber'].'-'.$months['yearNumber']]); ?></td>
	<?php } ?>
	</tr>
	<tr>
	<td class="maincol"><div class="heading">Super Goer-Badge</div><div class="content"><img src="<?php echo base_url() ?>assets/images/sparkies/SuperGoer-Badge.png" width="50" height="50" data-toggle="tooltip" title="Maximum games played user"></div></td>
	<?php   
		foreach($academicmonths as $months){ 
		if((date('m')==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-1 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-2 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))))
		{$class="active";}else{$class="inactive";}			
	?>
		<td <?php echo $class; ?> ><?php echo str_replace(',',',<br/> ',$SuperGoerBadge[$months['monthNumber'].'-'.$months['yearNumber']]); ?></td>
	<?php } ?>
	</tr>
	<tr>
	<td class="maincol"><div class="heading">Super Angel-Badge</div><div class="content"><img src="<?php echo base_url() ?>assets/images/sparkies/SuperAngel-Badge.png" width="50" height="50" data-toggle="tooltip" title="Accurate played user"></div></td>
	<?php    //echo "<pre>";print_r($getSuperAngels);exit;
		foreach($academicmonths as $months){ 
		if((date('m')==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-1 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))) || (date("m",strtotime("-2 Months"))==date("m", mktime(null, null, null, $months['monthNumber']))))
		{$class="active";}else{$class="inactive";}			
	?>
		<td <?php echo $class; ?> ><?php echo str_replace(',',',<br/> ',$SuperAngelBadge[$months['monthNumber'].'-'.$months['yearNumber']]); ?></td>
	<?php } ?>
	</tr>

</tbody>
</table>
<?php //} ?>
 </div>
</div>
  </div><!--/form_sec -->
 </div><!-- container -->
 </div>
 <script>
$(document).ready(function() {
    var table = $('#LeaderBoard').DataTable( {
        scrollY:        "100%",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   {
            leftColumns: 1
        }
    } );
});
 </script>
 <script src="<?php echo base_url(); ?>assets/js/bootstraptooltip.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<style>
/* Ensure that the demo table scrolls */
#LeaderBoard th, #LeaderBoard td { white-space: nowrap; border: 1px solid#ddd;}
#LeaderBoard div.dataTables_wrapper {width: 100%;margin: 0 auto;}
.DTFC_Cloned{background:#ccc}
#LeaderBoard_filter,#LeaderBoard_info{display:none;}
#LeaderBoardpage table {border-collapse: collapse;}
#LeaderBoardpage .dataTable  .maincol{background-color:#f1f1f1;color:#ff6600;}
#LeaderBoardpage .dataTable  .heading{color:#ff6600;}
#LeaderBoardpage .dataTable  thead th{background-color:#062c65;color:#fff;border: 1px solid #ddd;}
#LeaderBoardpage .dataTable  tbody tr{background-color:#fafafa;}
#LeaderBoard_wrapper{padding-bottom:20px;}
</style>

