<footer>
<div class="container" id="footerpart">
<div class="row">
<div class="col-md-4 col-sm-5">

  
  <ul>
<li>Edsix BrainLab<sup>TM</sup> Pvt Ltd</li>
<li>Module #1, 3rd Floor, D Block,</li>
<li>Phase 2, IITM Research Park,</li>
<li>Kanagam Road, Taramani, Chennai - 600113</li>
  </ul>
  <?php //echo $this->lang->line("footeraddress"); ?> 
  </div>
<div class="col-md-4 col-sm-4">
	<ul>
		<li class="callicon"><?php echo $this->lang->line("ftphonenumber"); ?>, +91 956<span style="color:#ffd900; display:inline;">956</span>5454</li>
		<li class="msgicon"><a href="mailto:info@skillangels.com">info@skillangels.com</a></li>
	</ul>
	<div class="socialmedia">
		<span><?php echo $this->lang->line("ftjoin"); ?></span>
		<a href="https://www.facebook.com/skillangels" target="_blank"><img src="<?php echo base_url(); ?>assets/images/fb.png" width="33" height="33"></a> <a href="https://www.linkedin.com/company/edsix-brain-lab-pvt-ltd?trk=company_logo" target="_blank"><img src="<?php echo base_url(); ?>assets/images/icon_LinkedIn.png" width="33" height="33"></a>
	</div>

</div>
<div class="col-md-4 col-sm-3">
	<img src="<?php echo base_url(); ?>assets/images/banner/Edsix-Logo.png"  alt="EDSIX" class="img-responsive" width="193" height="67" >
</div>
</div>
</div>

</footer>

<div class="footerBottom"><p>&copy; <?php echo date("Y"); ?> KinderAngels. All rights reserved</p></div>

<script type="text/javascript">
  $(document).ready(function(e) { 
   $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 6000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
	/* $(".loginmodal-container .close").click(function(){
		//$("#primary-menu").trigger('click');
		//$(".loginLink").attr('style','background: #f92c8b !important;');
	}); */
});
</script>

<script>
/* ================== Parent Login =============*/	
$('#submit').click(function(){ 
	var form=$("#form-login");
	$(".loader").show();
	/* Avoid Multiple Login */	
	$.ajax({
	type:"POST",
	url:"<?php echo base_url('index.php/home/islogin') ?>",
	data:form.serialize(),
	success:function(isloginval)
	{ 
		if(isloginval==0)
		{
			userlogin(form);
		}
		else
		{
				swal({
				  title: 'Are you sure?',
				  text: "You are logging into another system.would you like to continue.",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, continue!',
				  cancelButtonText: 'No, cancel!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  allowOutsideClick: false,
				  allowEscapeKey : false,
				  buttonsStyling: false
				}).then(function () {
					
						userlogin(form);
				  
				}, function (dismiss) {
				  if (dismiss === 'cancel') {
					swal(
					  'Cancelled',
					  'You are continuing with your previous login :)',
					  'error'
					);
					$(".loader").hide();
				  }
				});
		}
	}
	});
});	

/* ================== Parent Login =============*/	
$('#Childsubmit').click(function(){ 
	var form=$("#child-form-login");
	$(".loader").show();
	/* Avoid Multiple Login */	
	$.ajax({
	type:"POST",
	url:"<?php echo base_url('index.php/home/ischildlogin') ?>",
	data:form.serialize(),
	success:function(isloginval)
	{ 
		
		if(isloginval==0)
		{
			Childuserlogin(form);
		}
		else
		{
				swal({
				  title: 'Are you sure?',
				  text: "You are logging into another system.would you like to continue.",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, continue!',
				  cancelButtonText: 'No, cancel!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  allowOutsideClick: false,
				  allowEscapeKey : false,
				  buttonsStyling: false
				}).then(function () {
					
						Childuserlogin(form);
				  
				}, function (dismiss) {
				  if (dismiss === 'cancel') {
					swal(
					  'Cancelled',
					  'You are continuing with your previous login :)',
					  'error'
					);
					$(".loader").hide();
				  }
				});
		}
	}
	});
});	

function userlogin(form)
{	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url('index.php/home/userlogin') ?>",
		data:form.serialize(),
		success:function(result)
		{
			if(result==2)
			{
				window.location.href= "<?php echo base_url();?>index.php/home/childlist";
			}
			else
			{	$(".loader").hide();
				$("#errormsg").html('Invalid Credentials');
			}
		}
	}); 
	
}
function Childuserlogin(form)
{	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url('index.php/home/childlogin') ?>",
		data:form.serialize(),
		success:function(result)
		{
			if(result!=0)
			{
				window.location.href= "<?php echo base_url();?>index.php/home/dashboard?cid="+result;
			}
			else
			{	$(".loader").hide();
				$("#childerrormsg").html('Invalid Credentials');
			}
		}
	}); 
	
}

function isUser(form)
{ 
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/isUser') ?>",
				data:form.serialize(),
				dataType: "json",
				success:function(result)
				{ 
					//alert(result); console.log(result);alert(result.portal_type); return false;
					userlogin(form);
				}
		}); 
	
}
function termscheck(form)
{
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/termscheck') ?>",
				data:form.serialize(),
				success:function(result)
				{
				//alert(result);
					if(result==0  && $.trim(result)!='')
					{	$(".loader").hide();
						$("#termschkbox").show();
						$("#terrormsg").html('Please check terms and conditions');
						
					}

					else
					{
						//userlogin(form);
						isUser(form);
					}

				}
		});
}
</script>
<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    $("html,body").animate({scrollTop:$("#header").offset().top},"100");return false;
}
</script>
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a9ea40ae7b3b317"></script>-->
<!-- Global site tag (gtag.js) - Google Analytics -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/anime/anime.css">
<script src="<?php echo base_url(); ?>assets/js/anime/anime.min.js" type="text/javascript"></script>
</body>
</html>