<div class="headerTop">

<div class="banner">
	<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>	<!-- SLIDE  -->
			
				<li data-transition="random-premium" data-slotamount="7" data-masterspeed="1500" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>assets/images/banner/KinderAngels-01A.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->
					<div class="tp-caption skewfromright customout start"
						data-x="250"
						data-y="120"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"						
						data-captionhidden="on"
						data-elementdelay="0.3"						
						style="z-index: 7;">
						<div class="a1" style="font-size:3em;">Play, Learn</div>
					</div>
					<div class="tp-caption  skewfromleftshort customout start"
						data-x="430"
						data-y="160"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="0.5"
						style="z-index: 7;">
						<div class="a3" style="font-size:1.5em;"> & </div>
					</div>
					<div class="tp-caption  skewfromleft customout start"
						data-x="420"
						data-y="200"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
						data-speed="2500"
						data-start="1500"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"						
						data-captionhidden="on"
						data-elementdelay="0.3"	
						style="z-index: 9; 
						transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 18px; border-width: 0px; margin: 0px; padding: 9px; letter-spacing: 0px; font-size: 18px; left: 151.034px; top: 173.314px; visibility: visible; opacity: 0; transform: matrix3d(0.75, 0, 0, 0, 0, 0.75, 0, 0, 0, 0, 1, -0.00166, 0, 0, 0, 1); 
						transform-origin: 50% 50% 0px;"
						>
						<div class="a4"  style="font-size:3em;">Grow together</div>
					</div>
					<div class="tp-caption  skewfromrightshort customout start"
						data-x="250"
						data-y="300"
						data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Power4.easeIn"
						data-endspeed="500"
						data-endeasing="Power4.easeInOut"
						data-captionhidden="on"
						data-elementdelay="1"
						style="z-index: 8"><div class="a2">Exciting time for both parent and child</div>
					</div>
					
				</li>
				<li data-transition="random-premium" data-slotamount="7" data-masterspeed="1500" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url(); ?>assets/images/banner/KinderAngels-06.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<!-- LAYERS -->
					<!-- LAYER NR. 13 -->
					<!--<div class="tp-caption skewfromrightshort customout start"
						data-x="150"
						data-y="120"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"						
						data-captionhidden="on"
						data-elementdelay="0.3"						
						style="z-index: 7;">
						<div class="a1" style="font-size:3em;">Potential</div>
					</div>
					<div class="tp-caption skewfromrightshort customout start"
						data-x="150"
						data-y="160"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="0.5"
						style="z-index: 7;">
						<div class="a3" style="font-size:1.5em;"> to </div>
					</div>-->
					<!--<div class="tp-caption skewfromrightshort customout start"
						data-x="750"
						data-y="200"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
						data-speed="2500"
						data-start="1500"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"						
						data-captionhidden="on"
						data-elementdelay="0.3"	
						style="z-index: 9; "
						>
						<div class="a4"  style="font-size:3em;">Enrich,Enhance</div>
					</div>-->
					<div class="tp-caption medium_bold_grey skewfromleftshort customout"
						data-x="950"
						data-y="100"
						

						
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;"
						
						
						
						>
						<div class="a4" style="font-size:1.5em; color: #f33; font-weight:100; ">Enrich</div>
						
					</div>
					
					<div class="tp-caption medium_bold_grey skewfromright customout verticalletter"
						data-x="950"
						data-y="130"
						
						
						
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="2000"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;">
						
						<div class="a4"  style="font-size:2em; font-weight:100;color: #f33;">n </div>
					</div>
					
					<div class="tp-caption medium_bold_grey skewfromright customout verticalletter"
						data-x="950"
						data-y="170"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="2000"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;"
						
						
						
						>
						<div class="a4"  style="font-size:2em;; font-weight:100;color: #fc0;">h </div>
						
					</div>
					
					<div class="tp-caption medium_bold_grey skewfromright customout verticalletter"
						data-x="950"
						data-y="200"
						
						
						
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;"
						
						
						
						>
						
						<div class="a4"  style="font-size:2em;; font-weight:100;color: #99cc00; ">a </div>
						
					</div>
					
					<div class="tp-caption medium_bold_grey skewfromright customout verticalletter"
						data-x="950"
						data-y="230"
						
						
						
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;"
						
						
						
						>
						
						<div class="a4"  style="font-size:2em;; font-weight:100; color:#ff9900;">n </div>
						
					</div>
					
					<div class="tp-caption medium_bold_grey skewfromright customout verticalletter"
						data-x="950"
						data-y="260"
						
						
						
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;"
						
						
						
						>
						
						<div class="a4"  style="font-size:2em;; font-weight:100;color:#cc0099; ">c </div>
					</div>
					
					<div class="tp-caption medium_bold_grey skewfromright customout verticalletter"
						data-x="950"
						data-y="290"
						
						
						
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="2500"
						data-start="1100"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"
						data-captionhidden="on"
						data-elementdelay="on"
						style="z-index: 7;"
						
						
						
						>
						
						<div class="a4"  style="font-size:2em;; font-weight:100;color:#00ccff; ">e </div>
					</div>
					
				
					<!--<div class="tp-caption medium_bg_org skewfromrightshort customout start"
						data-x="150"
						data-y="240"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
						data-speed="2500"
						data-start="1500"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeOut"						
						data-captionhidden="on"
						data-elementdelay="0.3"	
						style="z-index: 9;"
						>
						<div class="a4"  style="font-size:2em;;color:#fff;padding:5px">Academic Skills</div>
					</div>-->
					
					
				</li>
				<!-- SLIDE  -->
				
				
			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</div>
</div>
 
 
</div>
 
</div>

</div>

<section class="feasection" parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url(); ?>assets/images/banner/pattern01.jpg"  id="ourfeatures" style="margin:0px;">
      <div class="container pb-0">
        <div class="section-title" style="text-align:center;">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <!--<h2 class="line-bottom-center mt-0"><span class="text-theme-color-red">KinderAngels</span></h2>-->
            </div>
          </div>
        </div>
        <div class="section-content" style="text-align:center;">  
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">    
				<div class="col-xs-12 col-sm-4 col-md-4">
					<img src="<?php echo base_url(); ?>assets/images/banner/icon01.png" style="width: 200px;" alt="Cross Platform"/>
					<p style="font-size:20px;">Setting up a regular study time will help learning to stay on course.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<img src="<?php echo base_url(); ?>assets/images/banner/icon02.png" style="width: 200px;" alt="Cross Platform"/>
					<p style="font-size:20px;">Develop routine study skills,Habits,improve their thinking memory abilities.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<img src="<?php echo base_url(); ?>assets/images/banner/icon03.png" style="width: 200px;" alt="Cross Platform"/>
					<p style="font-size:20px;">Learning through images / Play way method improves visualization & thinking abilities.</p>
				</div>
			</div>     
         </div>
		 
      </div>
    </section>
	
	<!--<section class="Introcontent">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">    
			<ul class="bullet1 bpoints">
				<li>Setting up a regular study time will help learning to stay on course.</li>
				<li>Develop routine study skills,Habits,improve their thinking memory abilities.</li>
				<li>Learning through images / Play way method improves visualization & thinking abilities.</li>
			</ul>
		</div>     
	</div>
</section>-->

<div style="margin-top:0px;background-color:#dcf6f9">
<img src="<?php echo base_url(); ?>assets/images/new/f1New.png" style="width:100% !important;" />
</div>

<script>
$('#email').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
     $('#submit').click();
    return false;  
  }
}); 
$('#pwd').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('#submit').click();
    return false;  
  }
}); 
</script>
<script>
$('#childemail').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
     $('#Childsubmit').click();
    return false;  
  }
}); 
$('#childpwd').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('#Childsubmit').click();
    return false;  
  }
}); 
</script>
 <script type="text/javascript">

$(window).on("load",function() {
  $(window).scroll(function() {
    $(".fadeanimate").each(function() {
      /* Check the location of each desired element */
      var objectBottom = $(this).offset().top + $(this).outerHeight();
      var windowBottom = $(window).scrollTop() + $(window).innerHeight();
      /* If the element is completely within bounds of the window, fade it in */
       if (objectBottom < windowBottom) { //object comes into view (scrolling down)
        //if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
      } else { //object goes out of view (scrolling up)
        //if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
      }
    }); 
  }); $(window).scroll(); //invoke scroll-handler on page-load
});  
</script>

<!-- ------ Over ALL Toper LIST END------- -->
  
	<!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
 
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script src="<?php echo base_url(); ?>assets/js/banner/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/banner/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/banner/settings.css" media="screen" />
 <!-- THE SCRIPT INITIALISATION -->
	<!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
<script type="text/javascript">
var revapi;
//var $j = jQuery.noConflict();
$(document).ready(function() {
			   revapi = $('.tp-banner').revolution(
				{
					delay:90000000,
					startwidth:1170,
					startheight:550,
					hideThumbs:10,
					fullWidth:"on",
					forceFullWidth:"on"
				});
	$(".loginLink").click(function(){
		$(".glyphicon-chevron-right").trigger('click');
	});
		setInterval(function(){ 	
		$("#idRgnBtnColor").toggleClass("RgnBtnColor");
		},1000);
});	
</script>
	<!-- END REVOLUTION SLIDER -->
<!-- Full Image Fade OUT --><script src="<?php echo base_url(); ?>assets/js/imagefade/parallax.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/Celebration/randomColor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/Celebration/confettiKit.js"></script>
<script type="text/javascript">
HappyBirthday();
function HappyBirthday()
{
	new confettiKit({
                confettiCount: 70,
                angle: 90,
                startVelocity: 50,
                colors: [
					'#a864fd',
					'#29cdff',
					'#78ff44',
					'#ff718d',
					'#fdff6a',
				],
                elements: {
                    'confetti': {
                        direction: 'down',
                        rotation: true,
                    },
                    'star': {
                        count: 40,
                        direction: 'up',
                        rotation: true,
                    },
                    'ribbon': {
                        count: 15,
                        direction: 'down',
                        rotation: true,
                    },
                    'custom': [{
                        count: 8,
                        width: 30,
                        textSize: 15,
                        content: '<?php echo base_url(); ?>assets/images/new/ballon.png',
                        contentType: 'image',
                        direction: 'up',
                        rotation: false,
                    }]
                },
                position: 'bottomLeftRight',
            });
}
</script>