<?php 
$dir = "assets/images/avatarimages/avatar";
$avatar=array();
if ($dh = opendir($dir))
{
	while (($file = readdir($dh)) !== false){
		if(strpos($file,'.png')!==false)
		{
			$avatar[]=$file;
		}
	}
}
//echo "<pre>";print_r($query);exit;
$totaltoppers=count($query);
$dotposition=$totaltoppers-10;
$dotcount=10;
$Malearr=array("02.png","03.png","04.png","06.png","07.png","09.png","010.png","012.png");
$Femalearr=array("01.png","05.png","08.png","011.png");
?>
<div class="col-md-3 col-sm-2 col-lg-3 col-xs-12"></div>
<div class="col-md-6 col-sm-8 col-lg-6 col-xs-12" id="LB">
<?php 
if($totaltoppers>0)
{
	$i=0;$j=1; 
	
	$male=0;
	$female=0;
	foreach($query as $res)
	{ 
		if($i>=10){$i=0;}
		if($male>=8){$male=0;}
		if($female>=4){$female=0;}
		
	?>
		<div class="row">
			<div class="leaderboard">
				<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
					<?php 
					if($res['avatarimage']=="assets/images/avatar.png")
					{
						if($res['gender']=='Male' || $res['gender']=='M')
						{
							$img_src=base_url()."".$dir."/".$Malearr[$male];
						}
						else
						{
							$img_src=base_url()."".$dir."/".$Femalearr[$female];
						} 
						
					}
					else 
					{
						$img_src=base_url()."".$res['avatarimage'];
					}
					
					?>
					<img src="<?php echo $img_src; ?>" alt="<?php echo $res['name']; ?>" />
				</div>
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
					<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
						<h3><?php echo $res['name']; ?></h3>
					</div>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
						<h5><?php echo $res['grade']; ?></h5>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
					<h4><?php echo $res['bspi']; ?></h4>
				</div>
			</div>
		</div>
		<?php 
		if($dotposition==$j)
		{
			for($k=1;$dotcount>$k;$k++)
			{ ?>
				<div class="row dottedline">
					 <span class="dot Blinkeff<?php echo $k; ?>"></span>
				</div>
			<?php
			}
		}
	?>
<?php  $i++; $j++;
		$male++;$female++;
	
	} ?>
<?php 
}
else
{ ?>
<div class="row">
	<div class="leaderboard">
		<h4> No data found</h4>
	</div>
</div>
<?php } ?>
</div>
<div class="col-md-3 col-sm-2 col-lg-3 col-xs-12"></div>
<style>
.dot {
    height: 15px;
    width: 15px;
    background-color: #ff7701;
    border-radius: 50%;
    display: inline-block;
	margin: 5px;
}
.dottedline{border-bottom:#000;}
.Blinkeff1{ -webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff2{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff3{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff4{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff5{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff6{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff7{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff8{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff9{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
.Blinkeff10{-webkit-animation: sk-bouncedelay1 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay1 1.4s infinite ease-in-out both;}
  
.Blinkeff10{animation: Blinkdot 1.9s cubic-bezier(.5, 0, 1, 1) infinite alternate;}


@keyframes Blinkdot {  
  from { opacity: 1; transform: scale(0); }
  to { opacity: 0.3; transform: scale(1.0);}
}
@-webkit-keyframes sk-bouncedelay1 {
  0%, 80%, 100% { -webkit-transform: scale(0.5) }
  40% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bouncedelay1 {
  0%, 80%, 100% { 
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
  } 40% { 
    -webkit-transform: scale(1.0);
    transform: scale(1.0);
  }
}
</style>