<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<title>KinderAngels</title>

<link rel="icon" href="<?php echo base_url(); ?>favicon.ico"> 
<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styleinner.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/<?php if($userthemefile[0]['usertheme']==''){ echo 'default.css'; } else { echo $userthemefile[0]['usertheme']; } ?>">

 <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">

	 <!-- font CSS -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url(); ?>assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/navbar-static-top.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/fonts/Roboto.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/ie-emulation-modes-warning.js"></script>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stylenew.css">
 
<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<style>
.section_three .nav>li>a
{    padding: 22px 8px;
}


.news-item {
    margin: 0px;
    border-bottom: 1px dotted #fff;
    padding: 3px 0;
    color: #000;
}

.news-item img{margin-right:8px;}
#resultsmine,#resultsall{height: 415px;overflow-y: scroll;background-color:rgb(224, 228, 201);}

#results{background:#fafafa}
#newsfeedpart{border: 1px solid #c1bebe;}
.panel-heading{padding: 0;background:#ddd;color:#fff;padding:0;margin:0;}
.minefeeds{border-right: 1px solid;text-align: center;display: block;float: left;    cursor: pointer;}
.allfeeds{    cursor: pointer;}
.panel-heading .active{background:#ffc000}

/*   STYLE 10 */
.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100%;
    z-index: 99999;
    background: #5a5757 50% 100% no-repeat;
    opacity: 0.6;
}

.pad_v1 p{line-height:28px;}




#visiblepoints
{
	margin: auto;
    color: #fae50d;
    padding: 4px 20px;
    background: #92278f;
    position: relative;
    font-size: 20px;
    font-weight: 800;
}

</style>

</head>
<body>
<button onclick="topFunction()" id="myBtn" title="Go to top"><span class=""></span></button>
<div id="ContentPartStart">
<div class="clear_both"></div>
<div id="header">
<div class="headerarea" id="View">
<div class="headerarea">
	<div class="container">
<?php
 if($this->session->avatarimage==""){$img="assets/images/avatar.png";}else{$img=$this->session->avatarimage;}
 if(file_exists(APPPATH."../assets/".$this->session->avatarimage)=='false'){$img="assets/images/avatar.png";}else{$img=$this->session->avatarimage;}
 ?>
<div class="row">
	<div class="col-md-3 col-sm-3 col-xs-12 logo" style=""><a title="KinderAngels" href="<?php echo base_url('index.php'); ?>"><img src="<?php echo base_url(); ?>assets/images/banner/KA-Logo.png" width="200" alt="KinderAngels"></a></div>
	  
    
	
    <div class="col-md-6 col-sm-6 col-xs-12" style="float: right;">
		<nav class="navbar">
	         <div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-menu" aria-expanded="false"> 
				  <strong>Menu</strong>
				  <div class="col-xs-1 pull-right iconbar"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div></button>
			</div>
	      <div id="primary-menu" class="navbar-collapse collapse" style="float: right;">
       
          <ul class="nav navbar-nav" style="margin:0px;">
				<?php if(isset($this->session->parentid)){ ?>
				<li <?php if($this->uri->segment(2)=="profile"){echo 'class="activereport"';}?>><a href="<?php echo base_url('index.php/home/profile'); ?>" class="">Profile</a></li> 
				<?php } else {?>
				<li <?php if($this->uri->segment(2)=="childprofile"){echo 'class="activereport"';}?>><a href="<?php echo base_url('index.php/home/childprofile'); ?>" class="">Profile</a></li> 
				<?php } ?>
				<?php if(isset($this->session->parentid)){ ?>
					<li <?php if($this->uri->segment(2)=="childlist" || $this->uri->segment(2)=="childadd" || $this->uri->segment(2)=="dashboard"){echo 'class="activereport"';}?>><a class="" href="<?php echo base_url('index.php/home/childlist'); ?>">Child List</a></li> 
				<?php }  ?>
				<?php if(isset($this->session->ChildId)){ ?>
					<li <?php if($this->uri->segment(2)=="dashboard"){echo 'class="activereport"';}?>><a class="" href="<?php echo base_url('index.php/home/dashboard'); ?>">Dashboard</a></li> 
				<?php }  ?>
				<?php if(isset($this->session->parentid)){ ?>
					<li><a href="<?php echo base_url('index.php/home/logout') ?>" class="">Logout</a></li>
				<?php } else { ?>
					<li><a href="<?php echo base_url('index.php/home/Childlogout') ?>" class="">Logout</a></li>
				<?php } ?>
				
			</ul>
        </div>
      </nav>
    </div> 
	<?php if($this->uri->segment(2)=="dashboard" || $this->uri->segment(2)=="dashboard" ){ ?>
	
	<?php } ?>
	
  </div><!--/row -->
  </div><!--/container -->
</div>
</div><!--/section_two -->
</div>
<div id="mySidenav" class="sidenav">
	<div class="<?php echo $clsname; ?>">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-window-close" aria-hidden="true"></i></a>
	</div>
	
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12 starclsnew" style="background: #fafafa;">		
			<h3 style="background: #faa400;padding: 10px;color: #fff;font-size: 30px;">Month Wise Report</h3>
			<div class="clear_both"></div>	
			<div class="sectiona" id="report">
				<div class="container">
					<div class="row">	
						<div id="reportview"></div>
					</div>
				</div>
			</div>	
		</div>	
	</div>	
</div>
<div id="myDailyStatus" class="sidenav">
	<div class="<?php echo $clsname; ?>">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav1()"><i class="fa fa-window-close" aria-hidden="true"></i></a>
	</div>
	
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12 starclsnew" style="background: #fafafa;">		
			<h3 style="background: #faa400;padding: 10px;color: #fff;font-size: 30px;">Daily Status</h3>
			<div class="clear_both"></div>	
			<div class="sectionb" id="report">
				<div class="container">
					<div class="row">	
						<div id="Dailyreportview"></div>
					</div>
				</div>
			</div>	
		</div>	
	</div>	
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/menu/jquery.sticky.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery_easing.js"></script>
<!-- Sweet Alert -->
<!-- This is what you need -->
<script src="<?php echo base_url(); ?>assets/js/sweetalert/sweetalert2.all.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert/sweetalert2.min.css">



<script>
	function openNav() {
		$(".btmbar").css("z-index","99");
		$("#myBtn").css("z-index","99");
		
		document.getElementById("mySidenav").style.width = "100%";
	}

	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
		$(".btmbar").css("z-index","9999");
		$("#myBtn").css("z-index","99999");
	}

	function openNav1() {
		
		$(".btmbar").css("z-index","99");
		$("#myBtn").css("z-index","99");
		
		document.getElementById("myDailyStatus").style.width = "100%";
	}

	function closeNav1() {
		document.getElementById("myDailyStatus").style.width = "0";
		$(".btmbar").css("z-index","9999");
		$("#myBtn").css("z-index","99999");
	}

$(function () {
   $('[data-toggle="tooltip"]').tooltip();
	
});
</script>


