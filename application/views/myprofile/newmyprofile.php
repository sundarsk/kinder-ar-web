<meta name = "format-detection" content = "telephone=no">
<div id="avatarpopup" class="modal fade" role="dialog">
  <div class="modal-dialog" id="">
    <!-- Modal content-->
    <div class="modal-content" style="box-shadow: none;border: none;">
      <div class="modal-header" style="text-align:center;">
<button type="button" class="close" data-dismiss="modal" style="color: #000;opacity: 1;font-size: 30px;
">&times;</button>
			<h3 class="modal-title" id="" style="text-align: center;">Select your avatar</h3>
      </div>
      <div class="modal-body"  style="padding:0px;">
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px; background-color: #fff;">
				<div class="col-md-12 col-sm-12 col-xs-12">
				<?php 
				$dir = "assets/images/avatarimages/avatar";
  			if ($dh = opendir($dir)){
				 
    		while (($file = readdir($dh)) !== false){
				
				if($file!='.' && $file!='..'){?>
            <div class="col-md-4 col-sm-4 col-xs-12" style="padding:5px;text-align: center;">
            <a href="javascript:;" class="imgavatar" id="imgavatar" data-val="assets/images/avatarimages/avatar/<?php echo $file; ?>" ><img class="avatarprofileImg" id="avatarprofileImg" src="<?php echo base_url(); ?>assets/images/avatarimages/avatar/<?php echo $file; ?>" width="113" height="113" alt="Avatar Image"/></a>
			</div>
            
    		<?php }
			 }
    		closedir($dh);
  			} ?>
				</div>
			</div>
			
      </div>
    </div>
  </div>
</div>
<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>assets/css/newprofile.css" rel="stylesheet" media="screen">
<div class="my_profile">

<div class="container">
    <div class="row user-menu-container square">
        <div class="col-md-12 user-details">
            <div class="row coralbg white">
                <div class="col-md-12 no-pad ">
                    <div class="user-pad">
                        <h3>Welcome, <?php echo $myprofile[0]['name']; ?></h3>
                    </div>
                </div>
				
            </div>
				<div class="col-md-12 user-menu">
					<div class="user-menu-content active">
						<h3>
							Personal Info
						</h3>
						<ul class="user-menu-list">
							<?php if($myprofile[0]['name']!='') { ?>
								<li>
									<span><i class="fa fa-male coral"></i> Name -   <?php echo $myprofile[0]['name']; ?></span>
							</li> <?php } ?>
							<?php if($myprofile[0]['username']!='') { ?>
								<li>
									<span><i class="fa fa-female coral"></i> Email -    <?php echo $myprofile[0]['username']; ?></span>
							</li> <?php } ?>
							<?php if($myprofile[0]['mobileno']!='') { ?>
							<li>
									<span><i class="fa fa-mobile coral"></i> <?php echo  $myprofile[0]['mobileno']; ?></span>
							</li> <?php } ?>
						</ul>
					</div>
				</div>
        </div>
        
        
    </div>
</div>
</div>

<script>
$(document).ready(function() {
    var $btnSets = $('#responsive'),
    $btnLinks = $btnSets.find('a');
 
    $btnLinks.click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.user-menu>div.user-menu-content").removeClass("active");
        $("div.user-menu>div.user-menu-content").eq(index).addClass("active");
    });
});



	
$( document ).ready(function() {
    //$("[rel='tooltip']").tooltip();    
 
    $('.view').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});

</script>
