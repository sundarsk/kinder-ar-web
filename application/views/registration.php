<style>

</style>
<div class="row" id="regbanner">
    <div class="">
  
 </div>
</div>

<div class="registrationarea">
<h2>Registration</h2>
<div class="">
<div id="mainContDisp" class="container playGames homePlayGames"> 
<form name="frmRegister" id="frmRegister" class="container frmreg" method="post" enctype="multipart/form-data" accept-charset="utf-8" style="" >
 <div class="row">
    <div class="col-lg-12 txtclr">
	<div style="padding-top:5px;">
      <div class="">
      <h3>Please fill the details below, to register : </h3><span style="float:right;"><label><span style="color:red">*</span> Required fields
	  </label></span>
  </div></div></div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div id="contact-form" class="form-container1">
			<div class="input-container">
				<div class="col-lg-12">
					<label for="txtFName">Name<span style="color:red">*</span></label>
				   <div class="input-group">
				   <span class="input-group-addon"><i class="fa fa-pencil "></i></span>
					<input type="text" maxlength="40" class="form-control alphaOnly" name="txtFName" value=""  id="txtFName">
				  </div> 
				</div>
				<!--<div class="col-lg-12" >
					<div class="form-group" style="padding-bottom: 25px;">
					<label for="txtGender">Gender <span style="color:red">*</span></label>
					<div class="form-group"> 
					   <div class="col-sm-3"><label class="radio-inline"><input type="radio" class=""  name="rdGender" value="M" id="rdGM">Male</label></div>
					<div class="col-sm-3"><label class="radio-inline"><input type="radio" class=""  name="rdGender" value="F" id="rdGF">Female</label></div>
					</div>
					</div>
				</div>-->
				<div class="col-lg-12">
					<label for="txtMobile">Mobile<span style="color:red">*</span></label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-phone "></i></span>
						<input type="text" class="form-control numbersOnly" name="txtMobile" value="" id="txtMobile">
					</div>
					 <label id='errMobile' class="error"></label>
				</div>
				<div class="col-lg-12">
					<label for="txtEmail">Email <span style="color:red">*</span></label>
				  <div class="input-group">
					 <span class="input-group-addon"><i class="fa fa-envelope "></i></span>
					<input type="text" maxlength="100" class="form-control" name="txtEmail" value="" id="txtEmail">
				  </div>
				  <label id='errEmail' class="error"></label>
				</div>
				<!--<div class="col-lg-12">
					<label for="txtCEmail">Confirm Email <span style="color:red">*</span></label>
				  <div class="input-group">
				   <span class="input-group-addon"><i class="fa fa-envelope "></i></span>
					<input type="text" maxlength="100" class="form-control" name="txtCEmail" value="" id="txtCEmail">
				  </div>
				</div>
				<div class="col-lg-12">
					<label for="txtDOB">DOB <span style="color:red">*</span></label>
				  <div class="input-group">
					 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" readonly class="form-control" name="txtDOB" value="" id="txtDOB">
				  </div>
				</div>
				<div class="col-lg-12">
					<label for="txtparentname">Parent / Guardian Name<span style="color:red">*</span></label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-pencil "></i></span>
						<input type="text" class="form-control" name="txtparentname" value="" id="txtparentname">
					</div>
				</div>-->
				
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div id="contact-form" class="form-container2" >
			<div class="input-container">
				<div class="col-lg-12">
					<label for="txtOPassword">Password <span style="color:red">*</span></label>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock "></i></span>
					<input type="password" placeholder="Minimum 8 characters" class="form-control" maxlength="20" name="txtOPassword" value="" id="txtOPassword">
				  </div>
				</div>
				<div class="col-lg-12">
					<label for="txtCPassword">Confirm Password <span style="color:red">*</span></label>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock "></i></span>
					<input type="password" placeholder="Minimum 8 characters" class="form-control" maxlength="20" name="txtCPassword" value="" id="txtCPassword">
				  </div>
				</div>
				<!--<div class="col-lg-12">
					<label for="ddlState">State<span style="color:red">*</span></label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-pencil "></i></span>
						<select class="form-control"  name="ddlState" id="ddlState" style="margin-bottom: 0px;    border: 1px solid #ccc;">
						<option value="">Select</option>
						<?php foreach($StateList as $row){?>
							<option value="<?php echo $row['id']; ?>" rel="<?php echo $row['state_name']; ?>"><?php echo $row['state_name']; ?></option>
						<?php } ?>
						</select> 
					</div>
				</div>
				<div class="col-lg-12">
						<label for="txtCity">City <span style="color:red">*</span></label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-pencil "></i></span>
						<input class="form-control" type="text" maxlength="255" name="txtCity" id="txtCity"/>
					</div>
				</div>
				<div class="col-lg-12">
						<label for="txtPincode">Pincode <span style="color:red">*</span></label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-pencil "></i></span>
						<input type="text" class="form-control numbersOnly" maxlength="6"  name="txtPincode" value="" id="txtPincode">
					</div>
				</div>
				<div class="col-lg-12">
					<label for="txtAddress">Address <span style="color:red">*</span></label>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-address-book"></i></span>
					<textarea class="form-control"  name="txtAddress" id="txtAddress"></textarea>
				  </div>
				</div>-->
			</div>
		</div>
	</div>
	<!--<div class="col-md-4 col-sm-4 col-xs-12">
		<div id="contact-form" class="form-container3">
			<div class="input-container">
				
				
			</div>
		</div>
	</div>-->
</div> 
<div style="text-align:center;clear:both;">
   <div style="padding-bottom:5px;">   
     <label  style=""  class="" id="">By clicking register button, I agree to the <a style="color: maroon;" href="<?php echo base_url(); ?>index.php/home/termsofservice" target="_blank">terms and conditions</a> of the site.</label>
</div>
<div style="padding-bottom:5px;">   
     <label  style="color:red"  class="error" id="errcoupcode"></label>
</div>
<input type="button" id="btnRegisterSubmit" style="float:none;" class="btn btn-success" value="Register">
<input type="reset" id="frmreset" class="btn default">

</div>
</form>
</div>
<div class="rella-row_bottom_divider" style="">
	<svg fill="#ffffff" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 300">
		<path d="M 1000 299 l 2 -279 c -155 -36 -310 135 -415 164 c -102.64 28.35 -149 -32 -232 -31 c -80 1 -142 53 -229 80 c -65.54 20.34 -101 15 -126 11.61 v 54.39 z"></path>
		<path d="M 1000 286 l 2 -252 c -157 -43 -302 144 -405 178 c -101.11 33.38 -159 -47 -242 -46 c -80 1 -145.09 54.07 -229 87 c -65.21 25.59 -104.07 16.72 -126 10.61 v 22.39 z"></path>
		<path d="M 1000 300 l 1 -230.29 c -217 -12.71 -300.47 129.15 -404 156.29 c -103 27 -174 -30 -257 -29 c -80 1 -130.09 37.07 -214 70 c -61.23 24 -108 15.61 -126 10.61 v 22.39 z"></path>
	</svg>
</div>
</div>


					
<div style="display:none;" id="iddivLoading" class="loading"></div>
</div> 
 <style>
 
.form-container1{overflow:hidden;}
.form-container2{overflow:hidden;}
.form-container3{overflow:hidden;}
 .navbar {margin: 20px 0px 0px 0px;}
 #errEmail{color: red;font-size: 15px;float: inherit;margin: 0;display: block;}
 #errMobile{color: red;font-size: 15px;float: inherit;margin: 0;display: block;}
 .switch-light .btn-primary{background-color:#229a36}
 .switch-light{width: 50%;margin: 0 auto;}
 .well{background-color:rgb(239, 14, 74);color: #fff;}
.txtclr h3{color:#106790;}

.loading {
  position: fixed;
  z-index: 999;

  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: #5a5757 url(<?php echo base_url(); ?>assets/images/ajax-page-loader.gif) center center no-repeat;
  background-size: 5%;
  opacity: 0.6;
}
.green{background-color:#5cb85c;color:#fff;font-size:18px;text-align:center;padding:5px;}
.red{background-color:#da6d6d;color:#fff;font-size:18px;text-align:center;padding:5px;}
.registrationarea input[type=text], .registrationarea input[type=password], input[type=password], select {
    height: 34px;
    font-size: 16px;
    width: 100%;
    margin-bottom: 0px !important;
    -webkit-appearance: none;
	border: 1px solid #d9d9d9;
	border-top: 1px solid #c0c0c0;
    border-radius: 5px;
    padding: 0 8px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script> 
<script>
dob = $( "#txtDOB" ).datepicker({
dateFormat: 'dd-mm-yy' ,maxDate: "31-12-2015" ,minDate: "01-01-1991" ,yearRange: "1991:2015",
onSelect: function(value, ui) {
var today = new Date(),
dob = new Date(value),
age = new Date(today - dob).getFullYear() - 1970;
},
onChangeMonthYear: function (year, month, inst)
{
                var date = $("#txtDOB").val();
                if ($.trim(date) != "") {
                    var newDate = month + "/" + inst.currentDay + "/" + year;
                    $("#txtDOB").val($.datepicker.formatDate('dd-mm-yy', new Date(newDate)));
                }
},
changeMonth: true, changeYear: true
});
/* $( "#txtDOB" ).focus({
	$(this).val('');
}); */

$('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9]/g,'');
});
$('.alphaOnly').keyup(function () { 
    this.value = this.value.replace(/[^a-zA-Z ]/g,'');
});
$('#txtPassword').bind("cut copy paste",function(e) {
     e.preventDefault();
});
$('#txtCPassword').bind("cut copy paste",function(e) {
     e.preventDefault();
}); $('#txtEmail').bind("cut copy paste",function(e) {
     e.preventDefault();
});
$('#txtCEmail').bind("cut copy paste",function(e) {
     e.preventDefault();
});
 
 $("#frmRegister").validate({
        rules: {
            "txtFName": {required: true,minlength: 3},
			"txtEmail": {required: true,email: true},
			"txtCEmail": {required: true,email: true,equalTo: "#txtEmail"},			
			"txtOPassword": {required: true,minlength: 8},
			"txtCPassword": {required: true,equalTo: "#txtOPassword"},
            "txtLName": {required: true},
            "txtDOB": {required: true},            
            "txtparentname": {required: true},            
            "rdGender": {required: true},
            "txtMobile": {required: true,minlength: 10},            
            "ddlState": {required: true},
            "txtCity": {required: true},
            "txtAddress": {required: true},
            "txtPincode": {required: true,minlength:6},
            "txtCouponCode": {required: true},
            "ddlGrade": {required: true}
			
        },
        messages: {
            "txtFName": {required: "Please enter first name"},
			"txtEmail": {required: "Please enter email",email: "Please enter valid email"},
			"txtCEmail": {required: "Please confirm email",email: "Please enter valid email",equalTo: "Please enter valid email"},
            "txtOPassword": {required: "Please enter password"},
			"txtCPassword": {required: "Please confirm password",equalTo: "Please enter valid confirm password"},
			"txtLName": {required: "Please enter last name"},
			"txtDOB": {required: "Please select date of birth"},
			"txtparentname": {required: "Please enter parent/guardian name"},
			"rdGender": {required: "Please select gender"},
			"txtMobile": {required:"Please enter mobile",minlength:"Please enter at least 10 digits"},            
            "ddlState": {required: "Please select state / province"},
            "txtCity": {required: "Please enter city"},
            "txtAddress": {required: "Please enter address"},
            "txtPincode": {required: "Please enter pincode"},
            "txtCouponCode": {required: "Please enter coupon code"},
            "ddlGrade": {required: "Please select the grade"}
        },
		errorPlacement: function(error, element) {
    if (element.attr("type") === "radio") {
        error.insertAfter(element.parent().parent());
    } 
	else {
        error.insertAfter(element.parent());
    }
},
		highlight: function(input) {
            $(input).addClass('error');
        } 
    });

var isemailavail=0;
var ismobilevail=0;
var iscouponvalid=0;
$('#txtEmail').blur(function()
{
	if($('#txtEmail').val()!='')
	{
		$("#existerrormsg").empty();
		$.ajax({
		type:"POST",
		url:"<?php echo base_url('index.php/subscription/checkemailexist') ?>",
		data:{emailid:$('#txtEmail').val()},
		success:function(result)
			{
				//alert(result);
				if($.trim(result)>0)
				{
					isemailavail=1;
					$("#errEmail").html("This E-Mail ID is already registered. Please try with new id.").show();
				}
				else
				{
					isemailavail=0;
					$("#errEmail").html("").show();
				}
			}
		});
	}
});
$('#txtMobile').blur(function()
{
	if($('#txtMobile').val()!='')
	{
		$("#existerrormsg").empty();
		$.ajax({
		type:"POST",
		url:"<?php echo base_url('index.php/subscription/IsMobilenounique') ?>",
		data:{Mobile:$('#txtMobile').val()},
		success:function(result)
			{
				//alert(result);
				if($.trim(result)>0)
				{
					ismobilevail=1;
					$("#errMobile").html("This mobile number is already registered. Please try with new number.").show();
				}
				else
				{
					ismobilevail=0;
					$("#errMobile").html("").show();
				}
			}
		});
	}
});
 
$("#btnRegisterSubmit").click(function(){
	if($("#frmRegister").valid() && (isemailavail==0) && (ismobilevail==0))
	{			
		$("#errcoupcode").html('');
		$("#iddivLoading").show();
		var form=$("#frmRegister");
		$.ajax({
		type:"POST",
		dataType:"json",
		url:"<?php echo base_url('index.php/subscription/userregistration') ?>",
		data:form.serialize(),
		//data:new FormData(form),
		success:function(result)
		{	//console.log(result);alert(result);
			$("#iddivLoading").hide();
			if($.trim(result.code)=='1')
			{
				$('#frmreset').click();
				$("#regpart1").hide();
				$("#mainContDisp").html('<div class="container frmreg"><div class="col-lg-12" style="min-height:300px"><h3 style="margin:10%;color: #000; text-align:center;">"Awesome choice you are gifting the child a fun learning experience."<br/> Check your mailbox and please click proceed to complete registration.</label></span></div></div>');
			}
			else if($.trim(result.code)=='2')
			{
				$("#errcoupcode").show().html(result.msg);
			}
			else if($.trim(result.code)=='3')
			{
				$("#errcoupcode").show().html(result.msg);
			}
		}
		});
	}
	else
	{		
		$("#errEmail").show();
		$("#errMobile").show();
	}
});
</script>
