<header class="head">
</header>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
</style>
<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
<div class="col-lg-12">
<div class="text-center">
<ul class="pricing-table dark">
	<li class="primary col-lg-12">
		<h2>Schools - Training Taken</h2>
		 <div class="bg-primary text-success col-lg-4" >
	<h4>Total Number of Schools</h4>
		<h4 id="schoolscount"></h4>
    </div>
	
	  <div class="bg-warning text-success col-lg-4" >
	<h4>Total Number of Students</h4>
	<h4 id="studentscount"></h4>
     </div>
	
	 <div class="bg-info text-success col-lg-4" >
	<h4>Average BSPI</h4>
	<h4 id="trainingbspi"></h4>
     </div>		
	</li>
	<div class="clearfix"></div>
</ul>
</div>
<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
 <style>
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>

<div class="col-lg-6">
        <div class="box">
            <header>
                <h5>School wise Average BSPI</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div id="schoolbspilist"></div>
            </div>
        </div>
		</div>
		<div class="col-lg-6">
		 <div class="box">
		 <div id = "tabs-1">
         <ul>
            <li id="59"><a href = "javascript:;" onclick="skilltopperlist(59);" >Memory</a></li>
            <li id="60"><a href = "javascript:;" onclick="skilltopperlist(60);">VP</a></li>
            <li id="61"><a href = "javascript:;" onclick="skilltopperlist(61);">FA</a></li>
			<li id="62"><a href = "javascript:;" onclick="skilltopperlist(62);">PS</a></li>
			<li id="63"><a href = "javascript:;" onclick="skilltopperlist(63);">Linguistics</a></li>
         </ul>
		 </div>
		<header>
                <h5>School wise Skill Topper</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div style="display:none;" id="iddivLoading1" class="loading">Loading&#8230;</div>
            <div id="skilltopperlist"></div>   
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<script>
$(function() {
            $( "#tabs-1" ).tabs();
			
         });
var trainingskillid=59;	 
$(document).ready(function(){

	counts();
	skilltopperlist(trainingskillid);
	setInterval(function(){counts();}, 1000*10*60); 
	setInterval(function(){skilltopperlist(trainingskillid);}, 1000*10*60); 
	
});
function counts() {
	//alert('hello');
	$("#iddivLoading").show();
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/dashboard_counts",
    data: {},
    
    success: function(result){
		//alert(result.schools);
		 $("#iddivLoading").hide();
		 $('#schoolscount').html(result.schools);
		 $('#studentscount').html(result.students);
		 $('#trainingbspi').html(result.trainingbspi);
		 schoolwisebspi(result.trainingbspi);
    }
});
}

function schoolwisebspi(trainingoverallbspi)
{
	//alert(trainingoverallbspi);
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/schoolwisebspi",
    data: {trainingoverallbspi:trainingoverallbspi},
    success: function(result){
		//alert(result);
		 $('#schoolbspilist').html(result);	 
    }
});
}

function skilltopperlist(val)
{
	$("#iddivLoading1").show();
trainingskillid=val;
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/skillwisetopper",
    data: {val:val},
    success: function(result){
		 $("#iddivLoading1").hide();
		 $('#skilltopperlist').html(result);
    }
});
}

</script>