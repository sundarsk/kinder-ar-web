<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Schools Training Taken</title>
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/admin/img/metis-tile.png" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.css">   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css">   
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/metisMenu.css">    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/animate.css">
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/theme.less">
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-multiselect.css">
	

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>


</head>

 <body class="  ">
            <div class="bg-dark dk" id="wrap">
                <div id="top">
                    <!-- .navbar -->
                    <nav class="navbar navbar-inverse navbar-static-top">
                        <div class="container-fluid">
                    
                    
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <header class="navbar-header">
                    
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="javascript:;" class="navbar-brand"><img src="<?php echo base_url(); ?>assets/admin/img/logo.png" style="height:48px; width:175px;" alt=""></a>
                    
                            </header>
							
							<div class="collapse navbar-collapse navbar-ex1-collapse">
                    
                                <!-- .nav -->
                                <ul class="nav navbar-nav">
								<?php if($this->session->username == 'admin@skillangels.com') { ?>
                                    <li <?php if($this->uri->segment(2)=="dashboard"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/admin/dashboard">Dashboard</a></li><?php } ?>

								<?php if($this->session->username == 'skillangels') { ?>
									<li <?php if($this->uri->segment(2)=="schedule"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/admin/schedule">Schools Schedule</a></li><?php } ?>
									
									<?php if($this->session->username == 'admin@skillangels.com') { ?>
                                     <li class='dropdown'>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            Training Reports <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url(); ?>index.php/admin/sgb_report">School - Grade BSPI Report</a></li>
                                            <li><a href="<?php echo base_url(); ?>index.php/admin/gradewise_report">Gradewise BSPI Report</a></li>
											<li><a href="<?php echo base_url(); ?>index.php/admin/sg_skillscore">School - Grade Skillscore</a></li>
											<li><a href="<?php echo base_url(); ?>index.php/admin/gradewise_skillscore">Gradewise Skillscore</a></li>											
                                        </ul>
										
                                    </li>
									
									<!--<li class='dropdown'>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            Exception Reports <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url(); ?>index.php/admin/tsu">Top schools by more users</a></li>
                                            <li><a href="<?php echo base_url(); ?>index.php/admin/schools_usage">Top schools by more usage</a></li>
											<li><a href="<?php echo base_url(); ?>index.php/admin/schools_sparkie">Top schools by sparkie</a></li>
											<li><a href="<?php echo base_url(); ?>index.php/admin/bspi_users">Top users by BSPI</a></li>				

											<li><a href="<?php echo base_url(); ?>index.php/admin/skillscore_users">Top users by skillscore</a></li>			

											<li><a href="<?php echo base_url(); ?>index.php/admin/top_attempted_users">Top users No of days Attempted</a></li>

											<li><a href="<?php echo base_url(); ?>index.php/admin/sparkie_users">Top users by Sparkie</a></li>												
                                        </ul>
										
                                    </li>-->
									
									<li <?php if($this->uri->segment(2)=="bspi_report"){echo 'class="active"';}?>>
                                        <a href="<?php echo base_url(); ?>index.php/admin/bspi_report" >
                                            BSPI Report
                                        </a>
                                       </li>
										<li  <?php if($this->uri->segment(2)=="skillscore_report"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/admin/skillscore_report">SKIll Score Report</a></li> 
										
										<!--<li  <?php if($this->uri->segment(2)=="performance_report"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/admin/performance_report">Performance Report</a></li>-->
																				
										<!--<li  <?php if($this->uri->segment(2)=="userupload" || $this->uri->segment(2)=="userupload_list"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/admin/userupload">User List Upload</a></li>--> <?php } ?>
										
										<li class=""><a href="<?php echo base_url(); ?>index.php/admin/logout">Logout</a></li>    
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
							
							
							</div>
							</nav>
							</div>
							</div>



