<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
						<?php //echo '<pre>'; print_r($sgb); 
							  //echo '<pre>'; print_r($bspi); 
							  						  
$overallbspi=array();
foreach($bspi as $row)
{
$overallbspi[$row['id']]=$row['bspiscore'];
}
$data['overallbspi']=$overallbspi;
	//print_r($data);						  
	 ?>
                            <th>S.No.</th>
                            <th>School Name</th>
							<th>Grade Name</th>
                            <th>Avg. BSPI</th>
							<th>Total Students</th>
							
                        </tr>
                    </thead>
                    <tbody>
					<?php //print_r($bspi);
					$i =1;
					
					foreach($sgb as $b) { 
					?>
					
                        <tr class="<?php   if(round($data['overallbspi'][$b['id']], 2) >round($b['bspiscore'], 2)){echo 'danger'; $diffpercent='<span class="percent down pull-right"> <i class="fa fa-caret-down"></i> '.(round($b['bspiscore'], 2)- round($data['overallbspi'][$b['id']], 2)).' %</span>'; } else if(round($data['overallbspi'][$b['id']], 2)==round($b['bspiscore'], 2)) { $diffpercent='<span class="percent pull-right"> <i class="fa fa-caret"></i> '.(round($b['bspiscore'], 2)-round($data['overallbspi'][$b['id']], 2)).' %</span>'; } else{ echo 'success'; $diffpercent='<span class="percent up pull-right"> <i class="fa fa-caret-up"></i> '.(round($b['bspiscore'], 2)-round($data['overallbspi'][$b['id']], 2)).' %</span>';} ?>">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $b['school_name']; ?></td>
							<td><?php echo $b['gradename']; ?></td>
                            <td><?php echo round($b['bspiscore'], 2).$diffpercent ; ?></td>
							<td><?php echo $b['totstudent']; ?></td>
							
                        </tr>
						
						
			
						
						
					<?php $i++;  }  
					?>
					
					
                    </tbody>                </table>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>
					
					<style>
					span.percent{font-size:10px;}
					</style>