<header class="head">
</header>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
</style>
<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
<div class="col-lg-12">
<div class="text-center">
<ul class="pricing-table dark">
	<li class="primary col-lg-12">
		<h2>Gifted Angels - Dashboard</h2>
		 <div class="bg-primary text-success col-lg-4" >
	<h4>Total Number of Subscribers</h4>
	<h4 id="totalsubscribers"></h4>
    </div>
	
	  <div class="bg-warning text-success col-lg-4" >
	<h4>Total Number of Registered Users</h4>
	<h4 id="totalregisterusers"></h4>
     </div>
	
	 <div class="bg-info text-success col-lg-4" >
	<h4>Total Processed Amount</h4>
	<h4 id="totalprocessedamount"></h4>	
     </div>		
	</li>
	<div class="clearfix"></div>
</ul>
</div>
<?php  //foreach ($regdates as $dt) { echo $dt->format("M-Y"); }

 ?>
<div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Grade wise Registered users</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div id="container3"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" ></div>
            </div>
        </div>
		</div>
		<div class="col-lg-6">
		 <div class="box">
		<header>
                <h5>Coupon code & Usage count</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
            <div id="container4"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" ></div>   
            </div>
        </div>
    </div>
	
	<div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Daywise Registered users</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div id="container5"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" ></div>
            </div>
        </div>
		</div>
		
		<div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Daywise Subscribed users</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div id="container6"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" ></div>
            </div>
        </div>
		</div>
		
		<div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Daywise Received Amount</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div id="container7"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" ></div>
            </div>
        </div>
		</div>
		
		
		
		<div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Register user chart</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<div id="container9"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" ></div>
            </div>
        </div>
		</div>
	
</div>
</div>
</div>
</div>
</div>
<script src="http://code.highcharts.com/adapters/prototype-adapter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/highcharts-3d.js"></script>
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>

<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script>
$(document).ready(function(){
	gradewiseusers();
	counts();
	couponcodeusers();
	daywiseregisteredusers();
	daywisesubscribedusers();
	daywisepaidamount();
	registrationdates();
	setInterval(function(){counts();}, 1000*10*60); 
	setInterval(function(){gradewiseusers();}, 1000*10*60); 
	setInterval(function(){couponcodeusers();}, 1000*10*60);
	setInterval(function(){daywiseregisteredusers();}, 1000*10*60);	
	setInterval(function(){daywisesubscribedusers();}, 1000*10*60);
	setInterval(function(){daywisepaidamount();}, 1000*10*60);
	setInterval(function(){registrationdates();}, 1000*10*60);
});
function counts() {
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/counts",
    data: {},
    success: function(result){
//alert(result);
		 $('#totalsubscribers').html(result.subscibeusers);
		 $('#totalregisterusers').html(result.registerusers);
		 $('#totalprocessedamount').html(result.amount);
    }
});
}

function Gradeuserschart(result)
{
	var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container3',
			backgroundColor:'transparent',
			 
            type: 'column'
        },
		
		colors: ['#811010', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#DDDF00'],
		
        title: {
            text: ''
        },
		tooltip: {enabled: true,
		
			formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><b>Grade : '+ this.x +'</b><br/><b>Registered Users : '+ this.y +'</b></div>';
    }
		
		},exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Number of users'
            },
			max: 100 
        },xAxis: {
            categories: result.data,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
			title: {
                text: 'Grades'
            }
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
				colorByPoint: true,
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: result.values
        }]
		
		
    });
	
} 

function gradewiseusers() {
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/gradewiseusers",
    data: {},
    success: function(result){
		//alert(result);
	Gradeuserschart(result);
		 
    }
});
}

function couponcodeuserschart(result)
{
  var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container4',
			backgroundColor:'transparent',
			 
            type: 'column'
        },
		
		colors: ['#811010', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#DDDF00'],
		
        title: {
            text: ''
        },
		tooltip: {enabled: true,
		
		formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><b>code : '+ this.x +'</b><br/><b>Used times : '+ this.y +'</b></div>';
    }
	},exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Usage Count'
            },
			max: 100 
        },xAxis: {
            categories: result.data,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
			title: {
                text: 'Coupon Codes'
            }
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
				colorByPoint: true,
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: result.values
        }]		
    });
	
} 

function couponcodeusers() {
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/couponcodeusers",
    data: {},
    success: function(result){
		//alert(result);
	couponcodeuserschart(result);
		 
    }
});
}
function daywisereguserschart(result)
{
var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container5',
			backgroundColor:'transparent',
			 
            type: 'line'
        },
		
		colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#DDDF00'],
		
        title: {
            text: ''
        },
		tooltip: {enabled: true, backgroundColor: '#50B432',
    borderColor: '#50B432',
    borderRadius: 10,
    borderWidth: 3,
	
	formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><b>Date : '+ this.x +'</b><br/><b>Users : '+ this.y +'</b></div>';
    }
	},
	
	
	exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Number of users'
            },
			max: 100 
        },xAxis: {
            categories: result.data,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
			title: {
                text: 'Registered Dates'
            }
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
				colorByPoint: true,
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: result.values
        }]
		
		
    });
	
} 
function daywiseregisteredusers() {
	
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/daywiseregisteredusers",
    data: {},
    success: function(result){
		//alert(result);
	daywisereguserschart(result);
		 
    }
});
}

function daywisesubuserschart(result)
{
	
	
	 
    var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container6',
			backgroundColor:'transparent',
			 
            type: 'line'
        },
		
		colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#DDDF00'],
		
        title: {
            text: ''
        },
		tooltip: {enabled: true, backgroundColor: '#50B432',
    borderColor: '#50B432',
    borderRadius: 10,
    borderWidth: 3,
	
	formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><b>Date : '+ this.x +'</b><br/><b>Users : '+ this.y +'</b></div>';
    }
	
	
	},
	
	
	exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Number of users'
            },
			max: 100 
        },xAxis: {
            categories: result.data,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
			title: {
                text: 'Subscribed Dates'
            }
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
				colorByPoint: true,
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: result.values
        }]
		
		
    });
	
}
function daywisesubscribedusers() {
	
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/daywisesubscribedusers",
    data: {},
    success: function(result){
		//alert(result);
	daywisesubuserschart(result);
		 
    }
});
}

function daywiseamountchart(result)
{
	
	
	 
    var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container7',
			backgroundColor:'transparent',
			 
            type: 'line'
        },
		
		colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#DDDF00'],
		
        title: {
            text: ''
        },
		tooltip: {enabled: true, backgroundColor: '#50B432',
    borderColor: '#50B432',
    borderRadius: 10,
    borderWidth: 3,
	
formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><b>Date : '+ this.x +'</b><br/><b>Amount : Rs.'+ this.y +'</b></div>';
    }
	
	},
	
	exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Total Received Amount(Rs.)'
            },
			max: 1000 
        },xAxis: {
            categories: result.data,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
			title: {
                text: 'Payment Received Dates'
            }
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
				colorByPoint: true,
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: result.values
        }]
		
		
    });
	
}
function daywisepaidamount() {
	
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/daywisepaidamount",
    data: {},
    success: function(result){
		//alert(result);
	daywiseamountchart(result);
		 
    }
});
}




function userregchart1(result) {
// Create the chart
//alert(result);
//console.log(result);
var chart = new Highcharts.Chart({
    chart: {
		renderTo: 'container9',
        type: 'line'
    },
    title: {
        text: 'Registered Users By Monthwise'
    },
    subtitle: {
        text: 'Click the columns to view datewise registered users.'
    },
	exporting:false,credits: {
      enabled: false
  },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Number of users'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span> <br>',
        pointFormat: '<span style="color:{point.color}">{point.name} - {point.y} Users</span>'
    },

    series: [{
        name: 'Months',
        colorByPoint: false,
        data: result.mnthwise
		
		
    }],
    drilldown: {
        series: [
            result.datewise
        ]
		
    }
});

}

function registrationdates() {
	
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/registrationdates",
    data: {},
    success: function(result){
	 //alert(result);
	userregchart1(result);
    }
});
}
</script>