<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
<link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-multiselect.js"></script>
<link href="<?php echo base_url();?>assets/css/bootstrap-multiselect.css" rel="stylesheet">	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<div class="container">
 <h3> </h3>
<div class="row">
<div class="col-md-12">
<div class="row">
    <form class="cmxform" method="POST" name="school_monthwisw_toppers" id="school_monthwisw_toppers" accept-charset="utf-8">   
	<div class="col-md-4">
		<label> Select School :  </label>
		<select id="ddschool" multiple="multiple">
		<?php foreach($school as $row){	?>
		<option value="<?php echo $row['id']; ?>"> <?php echo $row['school_name'] ?></option>	
		<?php } ?>	
		 </select>		 
		 <input type="hidden" value="" name="hdnschool" id="hdnschool" />   
	</div>
	<div class="col-md-4">
		<label> Select Grade :  </label>
		<select id="ddgrade" multiple="multiple">
		<?php foreach($grade as $row){	?>
		<option value="<?php echo $row['id']; ?>"> <?php echo $row['classname'] ?></option>	
		<?php } ?>	
		 </select>		 
		 <input type="hidden" value="" name="hdngrade" id="hdngrade" />  
	</div>	
	<div class="col-md-4">
		<label> Select Type :  </label>
		<select id="ddtype" multiple="multiple">
			<option value="BSPI">BSPI</option>	
			<option value="SPARKIES">SPARKIES</option>	
		 </select>		 
		 <input type="hidden" value="" name="hdntype" id="hdntype" />  
	</div>	
</div><br/>
<div class="row">
	<div class="col-md-6">
			<label> Select Month : </label>
			<select id="ddmonth" multiple="multiple">
				<option value="01">Jan</option>	
				<option value="02">Feb</option>	
				<option value="03">Mar</option>	
				<option value="04">Apr</option>	
				<option value="05">May</option>	
				<option value="06">June</option>	
				<option value="07">July</option>	
				<option value="08">Aug</option>	
				<option value="09">Sep</option>	
				<option value="10">Oct</option>
				<option value="11">Nov</option>
				<option value="12">Dec</option>
			</select>		 
		 <input type="hidden" value="" name="hdnmonth" id="hdnmonth" />  
	</div>
	<div class="col-md-6">
			<input  type="submit" value="Submit" class="btn btn-default Search" id="btnSearch" name="btnSearch">
			<input  type="submit" value="Download" class="btn btn-default Search" id="btndownload" name="btndownload">
	</div>
	</form>   

</div><br/>
</div>	
</div>
<span class="counter pull-right"></span>
<table class="table table-hover table-bordered results" id="assementTable">
  <thead>
	<?php if(isset($error)){ ?>
		<tr class="warning no-result">
			<td colspan="7"><i class="fa fa-warning"></i> <?php echo $error;?></td>
		</tr>
	<?php } ?>
    <tr>     
      <th class="">Name</th>
      <th class="">School</th>
      <th class="">Grade</th>	  
	  <th class="">Section</th>
	  <th class="">Month</th>
	  <th class="">Score</th>
	  <th class="">Type</th>
    </tr>
  </thead>
  <tbody>
    <?php 
	if(isset($bspi)){
	foreach($bspi as $row){ ?>
		<tr>
			<td><?php echo $row['username']; ?></td>
			<td><?php echo $row['school_name']; ?></td>
			<td><?php echo $row['classname']; ?></td>
			<td><?php echo $row['sectionname']; ?></td>
			<td><?php echo $row['monthName']; ?></td>
			<td><?php echo $row['bspi']; ?></td>
			<td><?php echo "BSPI"; ?></td>
		</tr>
	<?php }  } ?>
	<?php 
	if(isset($sparkies)){
	foreach($sparkies as $row1){ ?>
		<tr>
			<td><?php echo $row1['username']; ?></td>
			<td><?php echo $row1['school_name']; ?></td>
			<td><?php echo $row1['classname']; ?></td>
			<td><?php echo $row1['sectionname']; ?></td>
			<td><?php echo $row1['monthName']; ?></td>
			<td><?php echo $row1['points']; ?></td>
			<td><?php echo "SPARKIES"; ?></td>
		</tr>
	<?php } } ?>
  </tbody>
</table>
 <script type="text/javascript">

$(document).ready(function(){
	$( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#txtFDate" )
        .datepicker({
          changeMonth: true, changeYear: true
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#txtTDate" ).datepicker({
        changeMonth: true,changeYear: true
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  }); 
});
$(function() {
$('#ddschool').multiselect({
includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
onChange: function(element, checked) {
var brands = $('#ddschool option:selected');
var selected = [];
$(brands).each(function(index, brand){
selected.push([$(this).val()]);
//alert(selected);
});
$("#hdnschool").val(selected);
}
});
	

$('#ddgrade').multiselect({
includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
onChange: function(element, checked) {
var brands = $('#ddgrade option:selected');
var selected = [];
$(brands).each(function(index, brand){
selected.push([$(this).val()]);
//alert(selected);
});
$("#hdngrade").val(selected);
}
});
		

$('#ddtype').multiselect({
includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
onChange: function(element, checked) {
var brands = $('#ddtype option:selected');
var selected = [];
$(brands).each(function(index, brand){
selected.push([$(this).val()]);
//alert(selected);
});
$("#hdntype").val(selected);
}
});

$('#ddmonth').multiselect({
includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
onChange: function(element, checked) {
var brands = $('#ddmonth option:selected');
var selected = [];
$(brands).each(function(index, brand){
selected.push([$(this).val()]);
//alert(selected);
});
$("#hdnmonth").val(selected);
}
});
	
});

/* $("#showresult").click(function(){
var form=$("#school_monthwisw_toppers");
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/admin/GetToppersData') ?>",
data:form.serialize(),
success:function(result)
{
}
});
}); */

$(document).ready(function() {
	$('#ddschool').multiselect();
	$('#ddgrade').multiselect();
	$('#ddtype').multiselect();
	$('#ddmonth').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdnschool; ?>";
				var dataarray=data.split(",");
				$("#ddschool").val(dataarray);
				$("#hdnschool").val(dataarray);
				$("#ddschool").multiselect("refresh");

				var data1="<?php echo $hdngrade; ?>";
				var dataarray1=data1.split(",");
				$("#ddgrade").val(dataarray1);
				$("#hdngrade").val(dataarray1);
				$("#ddgrade").multiselect("refresh");
				
				var data2="<?php echo $hdntype; ?>";
				var dataarray2=data2.split(",");
				$("#ddtype").val(dataarray2);
				$("#hdntype").val(dataarray2);
				$("#ddtype").multiselect("refresh");
				
				var data3="<?php echo $hdnmonth; ?>";
				var dataarray3=data3.split(",");
				$("#ddmonth").val(dataarray3);
				$("#hdnmonth").val(dataarray3);
				$("#ddmonth").multiselect("refresh");
				<?php } ?>
	 
    //$('#assementTable').DataTable( );
});

</script>
</div>