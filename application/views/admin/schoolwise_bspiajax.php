<table class="table table-bordered responsive-table"  id="dataTable" >
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>School Name</th>
							<th>Students</th>
                            <th>Avg. BSPI</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php //print_r($bspi);
					$i =1;
					foreach($bspi as $b) { 
					if($b['school_name']!='') {
					?>
					
                        <tr class="<?php   if($overallbspi>round($b['bspiscore'], 2)){echo 'danger'; $diffpercent='<span class="percent down pull-right"> <i class="fa fa-caret-down"></i> '.round((round($b['bspiscore'], 2)-$overallbspi), 2).' %</span>'; } else if($overallbspi==round($b['bspiscore'], 2)) { $diffpercent='<span class="percent pull-right"> <i class="fa fa-caret"></i> '.round((round($b['bspiscore'], 2)-$overallbspi), 2).' %</span>'; } else{ echo 'success'; $diffpercent='<span class="percent up pull-right"> <i class="fa fa-caret-up"></i> '.round((round($b['bspiscore'], 2)-$overallbspi), 2).' %</span>';} ?>">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $b['school_name']; ?></td>
							<td><?php echo $b['totstudent']; ?></td>
                            <td><?php echo round($b['bspiscore'], 2).$diffpercent ;?></td>
                        </tr>
						
						<?php if($i==$i+1){ ?>
						<tr>
						<table>
						  <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Grade</th>
							<th>Students</th>
                            <th>Avg. BSPI</th>
                        </tr>
                    </thead>
                    <tbody>
						   <tr>
                            <td>1</td>
                            <td>I</td>
							<td>70</td>
                            <td>20.03</td>
							</tr>
							<tr>
                            <td>1</td>
                            <td>I</td>
							<td>70</td>
                            <td>20.03</td>
							</tr>
							<tr>
                            <td>1</td>
                            <td>I</td>
							<td>70</td>
                            <td>20.03</td>
							</tr>
						</tbody>
						</table>
						</tr>
						<?php } ?>
						
					<?php $i++; } } ?>
					
					
                    </tbody>                </table>
					
										
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>
					
					<style>
					span.percent{font-size:10px;}
					</style>