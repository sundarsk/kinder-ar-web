 <header class="head">
                               
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Schoolwise - Gradewise Skillscore
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            
<div class="row">
<div class="col-lg-12">

<div class="col-lg-12">
         <div class="box">
	
	<div id = "tabs-11">
         <ul>
            <li id="59"><a href = "javascript:;" onclick="skl_grade_skillscore(59);" >Memory</a></li>
            <li id="60"><a href = "javascript:;" onclick="skl_grade_skillscore(60);">Visual Processing</a></li>
            <li id="61"><a href = "javascript:;" onclick="skl_grade_skillscore(61);">Focus and Attention</a></li>
			<li id="62"><a href = "javascript:;" onclick="skl_grade_skillscore(62);">Problem Solving</a></li>
			<li id="63"><a href = "javascript:;" onclick="skl_grade_skillscore(63);">Linguistics</a></li>
         </ul>
		 </div>
            <header>
                <h5>Schoolwise - Gradewise Skillscore</h5>
            </header>
			
            <div id="borderedTable" class="body collapse in">
			<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
                <div id="sklgradeskill"></div>
            </div>
        </div>
		
		 
    </div>
	
	
</div>



<!-- Assessment -->

</div>
 </div>
						
						
		

                    </div>
                </div>
            </div>
			
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>

 <style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
           /* background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;*/
         }
		.stats_box li{margin:0 !important}
      </style>
 <style>
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>			

<script type="text/javascript">




		 
	var skillid = 59;
	
$(document).ready(function()
{
	
	skl_grade_skillscore(skillid);
	setInterval(function(){skl_grade_skillscore(skillid);}, 1000*3*60); 
	
	
});



$(function() {
            $( "#tabs-1" ).tabs();
			$( "#tabs-7" ).tabs();
			$( "#tabs-11" ).tabs();
			
         });




function skl_grade_skillscore(val)
{
	$("#iddivLoading").show();
	skillid=val;
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/skl_gradewise_skillscore",
    data: {val:val},
    success: function(result){
		//alert(result);
		$("#iddivLoading").hide();
		 $('#sklgradeskill').html(result);	 
    }
});
}

</script>
