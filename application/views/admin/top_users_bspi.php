<header class="head">
<div class="main-bar">
<h3><i class="fa fa-dashboard"></i>&nbsp;Top Users By More BSPI</h3>
</div>
</header>
						
						<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            
<div class="row">
<div class="col-lg-12">

<div class="col-lg-12">
        <div class="box">
            <header>
                <h5>Top Users By More BSPI</h5>
            </header>
            <div id="borderedTable" class="body collapse in">
			<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>S.No.</th>
							<th>Username</th>
							<th>School name</th>
							<th>Enrollment Date</th>
                            <th>User grade</th>
							<th>BSPI</th>
                        </tr>
                    </thead>
                    <tbody>
					
                        <tr>
                            <td>1</td>
							<td>Saravanan</td>
                            <td>Edsix brain lab</td>
							<td>10-01-2014</td>
							<td>Grade I</td>
							<td>98</td>
                        </tr>
						
						<tr>
                            <td>2</td>
							<td>Cal</td>
							<td>Edsix Cognitive school</td>
                            <td>01-05-2014</td>
							<td>Grade III</td>
							<td>95.2</td>
                        </tr>
						
						<tr>
                            <td>3</td>
							<td>Nithya</td>
							<td>GKM school</td>
                            <td>01-01-2015</td>
							<td>Grade I</td>
							<td>85</td>
                        </tr>
						
						<tr>
                            <td>4</td>
							<td>Damu</td>
							<td>Jain vidyalaya</td>
                            <td>01-08-2015</td>
							<td>Grade V</td>
							<td>82</td>
                        </tr>
						
						<tr>
                            <td>5</td>
							<td>Kumaran</td>
							<td>Chandra school</td>
                            <td>01-05-2015</td>
							<td>Grade IV</td>
							<td>60</td>
                        </tr>
                    </tbody>
					</table>
                
            </div>
        </div>
		
		 
    </div>
	
	
</div>



<!-- Assessment -->

</div>
 </div>
						
						
		

                    </div>
                </div>
            </div>
			
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>

 <style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
           /* background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;*/
         }
		.stats_box li{margin:0 !important}
      </style>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>
