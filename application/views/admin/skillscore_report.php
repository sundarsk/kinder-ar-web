<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
</style>
  <style>
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>	
 <header class="head">
                               
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Skill Score Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
			
 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Skill Score Report</h5>
            </header>
			<div id = "tabs-1">
		<ul>
            <li id="59"><a href = "javascript:;" onclick="report2('','',59);" >Memory</a></li>
            <li id="60"><a href = "javascript:;" onclick="report2('','',60);">Visual Processing</a></li>
            <li id="61"><a href = "javascript:;" onclick="report2('','',61);">Focus and Attention</a></li>
			<li id="62"><a href = "javascript:;" onclick="report2('','',62);">Problem Solving</a></li>
			<li id="63"><a href = "javascript:;" onclick="report2('','',63);">Linguistics</a></li>
         </ul>
			<form id="srchreport" method="POST">
			<div class="row">
			<div class="col-sm-3">
			<label>Schools</label>
			<select  class="form-control input-sm" name="schoolid" id="schoolid">
			<option value="">Select</option>
			<?php foreach($getschools_m as $schools) {
				$schoolsid='';
				$selected='';
				//if($schoolid==$schools['id']){$selected="selected='selected'";$schoolsid=$schools['id'];}
				echo "<option id='".$schools['id']."'  value='".$schools['schoolid']."'>".trim(str_replace("",'',$schools['schoolname']))."</option>";
			}  ?>
			</select>
			</div>
			
			<div class="col-sm-3">
			<label>Grade</label>
			<select  class="form-control input-sm" name="gradeid" id="gradeid" >
			<option value="">Select</option>
			<?php foreach($grades as $gd) { ?>
			<option value="<?php echo $gd['id']; ?>"><?php echo $gd['classname']; ?></option>
			<?php } ?>
			</select>
			<!--<input type="button" name="sbmtbtn" id="sbmtbtn" Value="Get result">-->
			</div>
			
			
			<div class="col-sm-2">
			<label>Section</label>
			<select  class="form-control input-sm sectionid" name="sectionid"  id="sectionid" >
			<option value="">Select</option>
			<?php if(isset($_POST['gradeid'])){   	 ?>
			<option value="<?php echo $data; ?>" selected='selected'><?php echo $data; ?></option>
			 <?php } ?>
			</select>
			 
			</div>
			
			</div>
			</form>
			 </div>
            <div id="collapse4" class="body">
			<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
                <div id="report2"></div>
				<div class="row" id='exportbtn' style='display:none;'>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<link href = "<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(function() {
            $( "#tabs-1" ).tabs();
         });
	
var skillid_org = 59;	
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();	

$(document).ready(function(){
	
	
	$("#srchreport").validate({
		rules: {
			schoolid: {required: true},
            gradeid: {required: true},
			sectionid: {required: true}
 
		   },
		   messages: {
			schoolid: {required: "Please choose school"},
			gradeid: {required: "Please choose gradeid"},
			sectionid:{required: "Please choose sectionid"}
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	
	$('#btnExcelExport').click(function(){
		
	excel_report2(schoolid,gradeid,skillid_org);
});
	
	
});

$('#gradeid').change(function(){
	ajaxsectionload(schoolid,gradeid);
});

$('#schoolid').change(function(){
	if($("#srchreport").valid()==true)
	{
report2(schoolid,gradeid,skillid_org);
	}
});

$('#sectionid').change(function(){
	if($("#srchreport").valid()==true)
	{
report2(schoolid,gradeid,skillid_org);
	}
});

function excel_report2(schoolid,gradeid,skillid)
{
	var schoolid = 	$('#schoolid').val();
	var gradeid = $('#gradeid').val();
	var sectionid = $('#sectionid').val();

	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/skillscore_report_excel",
    data: {schoolid:schoolid,gradeid:gradeid,skillid:skillid,sectionid:sectionid},
    
    success: function(result){
		//alert(result);
		 //console.log(result);
		window.location.href= "<?php echo base_url();?>"+result;
    }
});
}

function ajaxsectionload(schoolid,gradeid)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();

$.ajax({
		 url: "<?php echo base_url(); ?>index.php/admin/getsection",
		data:{schoolid:schoolid,gradeid:gradeid},
		success: function(result)
		{
		//alert(result);
		$('#sectionid').html(result);
		}
	});
	}

function report2(schoolid,gradeid,skillid)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();
var sectionid = $('#sectionid').val();
skillid_org=skillid;
$("#iddivLoading").show();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/skillscore_reports",
    data: {schoolid:schoolid,gradeid:gradeid,skillid:skillid,sectionid:sectionid},
    
    success: function(result){
		// alert(result);	 
		$("#iddivLoading").hide();
		  if(result!='')
		 {
			 $('#exportbtn').show();
		 }
		 $('#report2').html(result);
		
    }
});
}	
		 
</script>
 