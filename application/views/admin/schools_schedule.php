<?php //echo $this->session->username; ?>
<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Schools Schedule</h5>
            </header>		
<br/>			
						<div class="container">
						<div class="row">
						<table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr class="table-info">
        <th>Period/Day</th>
        <th <?php if(strcasecmp('monday',date('l'))==0){?>class="curday"<?php } ?>>Monday</th>
        <th <?php if(strcasecmp('tuesday',date('l'))==0){?>class="curday"<?php } ?>>Tuesday</th>
        <th <?php if(strcasecmp('wednesday',date('l'))==0){?>class="curday"<?php } ?>>Wednesday</th>
        <th <?php if(strcasecmp('thursday',date('l'))==0){?>class="curday"<?php } ?>>Thursday</th>
        <th <?php if(strcasecmp('friday',date('l'))==0){?>class="curday"<?php } ?>>Friday</th>
        <th <?php if(strcasecmp('saturday',date('l'))==0){?>class="curday"<?php } ?>>Saturday</th>
		
      </tr>
    </thead>
    <tbody>
	<?php 
/*	$s = array();
foreach ($timetable as $result) { 
	$s[] = $result['monday'];
}
$mo = implode('<br/>', $s); */
	//echo '<pre>'; print_r($timetable);
	$ini=1; 
	foreach($timetable as $time) {
		
		$mon = explode(",", $time['monday']);
		$tue = explode(",", $time['tuesday']);
		$wed = explode(",", $time['wednesday']);
		$thu = explode(",", $time['thursday']);
		$fri = explode(",", $time['friday']);
		
		//$sat = explode(",",$time['saturday']);
		//$sun = explode(",",$time['sunday']);
	$monid = explode(",", $time['mon_id']);
	$tueid = explode(",", $time['tue_id']);
	$wedid = explode(",", $time['wed_id']);
	$thuid = explode(",", $time['thu_id']);
	$friid = explode(",", $time['fri_id']);
	$satid = explode(",", $time['sat_id']);
	
	$monval = explode(",", $time['mon_val']);
	$tueval = explode(",", $time['tue_val']);
	$wedval = explode(",", $time['wed_val']);
	$thuval = explode(",", $time['thu_val']);
	$frival = explode(",", $time['fri_val']);
	$satval = explode(",", $time['sat_val']);
	
	
	$schoolid = explode(",", $time['school_id']);
	
	
	?>
    
       <tr>
      <td>Period <?php echo $ini; ?></td>
     <td <?php if(strcasecmp('monday',date('l'))==0){?>class="curday"<?php } ?>>  
	 <?php foreach($mon  as $key1=>$val1){  
		if(strcasecmp('monday',date('l'))==0){
			if($monval[$key1]==0)
			{
			echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none; color: red; font-weight:500;"   id="'.$monid[$key1].'" >'.$val1.'</a><br/>'; 
			}
			else{
				echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none;"   id="'.$monid[$key1].'" >'.$val1.'</a><br/>'; 
			}
		}else{
			echo $val1.'<br/>'; 
	  } }?>
		 
		 
		 </td>
		
		
	<td <?php if(strcasecmp('tuesday',date('l'))==0){?>class="curday"<?php } ?>>	
	<?php foreach($tue  as $key1=>$val1){  
		if(strcasecmp('tuesday',date('l'))==0){
			if($tueval[$key1]==0)
			{
		 echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none; color: red; font-weight:500;" id="'.$tueid[$key1].'" >'.$val1.'</a><br/>'; 
			}
			else{
				echo '<a href="javascript:;" title="" class="tues" rel="tooltip" style="text-decoration:none;" id="'.$tueid[$key1].'" >'.$val1.'</a><br/>'; 
			}
		 }else{
			echo $val1.'<br/>';
	  } }  ?>
		 
		 </td>
		
		
	<td <?php if(strcasecmp('wednesday',date('l'))==0){?>class="curday"<?php } ?>>	
	
	<?php foreach($wed  as $key1=>$val1){  
if(strcasecmp('wednesday',date('l'))==0){
	if($wedval[$key1]==0)
			{
		echo  '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none; color: red; font-weight:500;" id="'.$wedid[$key1].'">'.$val1.'</a><br/>'; 
			}
else{
	
	echo  '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none;" id="'.$wedid[$key1].'">'.$val1.'</a><br/>'; 
	
}			} else {
			echo $val1.'<br/>'; 
	} } ?>
		</td>
		
	<td <?php if(strcasecmp('thursday',date('l'))==0){?>class="curday"<?php } ?>>	
	
	<?php foreach($thu  as $key1=>$val1){ 
		if(strcasecmp('thursday',date('l'))==0){
			
			if($thuval[$key1]==0)
			{
		echo  '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none; color: red; font-weight:500;" id="'.$thuid[$key1].'" >'.$val1.'</a><br/>';
			}
			else{
				echo  '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none;" id="'.$thuid[$key1].'" >'.$val1.'</a><br/>';
			}


		} else {
			echo $val1.'<br/>'; 
	} } ?></td>
		
	<td <?php if(strcasecmp('friday',date('l'))==0){?>class="curday"<?php } ?>>	
	
	<?php foreach($fri  as $key1=>$val1){  
if(strcasecmp('friday',date('l'))==0){
	if($frival[$key1]==0)
			{
		echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none; color: red; font-weight:500;" id="'.$friid[$key1].'" >'.$val1.'</a><br/>'; 
			}
			else{
		echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none;" id="'.$friid[$key1].'" >'.$val1.'</a><br/>'; 		
				
			}
		
		} else {
			echo $val1.'<br/>'; 
	} } ?></td>
		
	<td <?php if(strcasecmp('saturday',date('l'))==0){?>class="curday"<?php } ?>>	
	
	<?php foreach($sat  as $key1=>$val1){  
	
	if(strcasecmp('saturday',date('l'))==0){
		if($satval[$key1]==0)
			{
	echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none; color: red; font-weight:500;" id="'.$satid[$key1].'" >'.$val1.'</a><br/>'; 
			}
			else{
	echo '<a href="javascript:;" title="" rel="tooltip" style="text-decoration:none;" id="'.$satid[$key1].'" >'.$val1.'</a><br/>'; 			
			}
	
	}  else {
			echo $val1.'<br/>'; 
	} }?></td>
	
        
      </tr>
        
      
	  
	  
	 
	  
	  
	<?php $ini++;  }
//print_r (explode(" ", $time['monday']));
	?>
      
    </tbody>
  </table>
  <div>
   <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Schools Detail</h5>
            </header>
  <table class="table table-bordered responsive-table"  id="dataTable" >
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>School code</th>
							<th>School name</th>
                        </tr>
                    </thead>
                    <tbody>
				
				<?php $i =1;	foreach($schoolcodes as $code) { ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $code['school_code']; ?></td>
							<td><?php echo $code['school_name']; ?></td>
                           
                        </tr>
						
				<?php $i++; } ?>
						
					
                    </tbody>                </table></div>
						</div></div>
						
						</div></div></div>
						</div></div>
						
						
						
						
						
						
						
						</div>
						
						
						<style>
						
						.closeicon {border: 1px solid #337ab7;background-image: url(<?php echo base_url(); ?>assets/images/cross.png);background-repeat: no-repeat;background-position: right top;}
						
.curday{background-color: #02b0f9 !important;color:#fff;}

 .tooltip
{
    text-align: center;
    color: #fff;
    background: #337ab7;
    position: absolute;
    z-index: 100;
    padding: 5px; 
	min-height: 160px;
	z-index: 9999;
}
.tooltip table thead
{
	border: 1px solid #ddd;
    overflow: hidden;
}
.tooltip table thead th {
    background-color: #393c3e;
    text-align: center;
    border: 1px solid #ccc;
}
.tooltip table tbody {
    border: 1px solid #ccc;
}
.tooltip table tbody td {
    border: 1px solid #ccc;
}
.tooltip h3{background: #393c3e;}

.tooltip a{
	color: #fff;
    text-decoration: none;
    cursor: pointer;
	}
    .tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }
 
        .tooltip.top:after
        {
            border-top-color: transparent;
            border-bottom: 10px solid #111;
            top: -20px;
            bottom: auto;
        }
 
        .tooltip.left:after
        {
            left: 10px;
            margin: 0;
        }
 
        .tooltip.right:after
        {
            right: 10px;
            left: auto;
            margin: 0;
        }
 </style>
	 
	 <link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>
	 <script>
	 
	 
 $( function()
{ 
    var targets = $( '[rel~=tooltip]' ),
        target  = false,
        tooltip = false,
        title   = false;
 
    targets.bind( 'click', function()
    {  target  = $( this );tip='';
		$(".tooltip").remove();
		var grade=target.attr('data-grade');
		var section=target.attr('data-section');
		
		var data = target.attr("id");
var arr = data.split('/');
var gradeid = arr[0];
var section = arr[1];
var schoolid = arr[2];

 $.ajax({
			//dataType: "json",
			url: "<?php echo base_url(); ?>index.php/admin/schools_schedule",
			method:'POST',
			data:{schoolid:schoolid,gradeid:gradeid,section:section},
			success: function(result)
			{ target.attr( 'title','');
		tip= result;
        tooltip = $( '<div class="tooltip"></div>' );
        if( !tip || tip == '' )
            return false;
 
        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
               .html( tip )
               .appendTo( 'body' );
 
        var init_tooltip = function()
        {
            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                tooltip.css( 'max-width', $( window ).width() / 2 );
            else
                tooltip.css( 'max-width', 340 );
 
            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top  = target.offset().top - tooltip.outerHeight() - 20;
 
            if( pos_left < 0 )
            {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            }
            else
                tooltip.removeClass( 'left' );
 
            if( pos_left + tooltip.outerWidth() > $( window ).width() )
            {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            }
            else
                tooltip.removeClass( 'right' );
 
            if( pos_top < 0 )
            {
                var pos_top  = target.offset().top + target.outerHeight();
                tooltip.addClass( 'top' );
            }
            else
                tooltip.removeClass( 'top' );
 
            tooltip.css( { left: pos_left, top: pos_top } )
                   .animate( { top: '+=10', opacity: 1 }, 50 );
        };
 
        init_tooltip();
        $( window ).resize( init_tooltip );
 
        var remove_tooltip = function()
        {
            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
            {
                $( this ).remove();
            });
 
            target.attr( 'title', tip );
        };
 
		   target.bind( 'click', remove_tooltip );
       //tooltip.bind( 'click', remove_tooltip );
		   $(".closeicon").click(function(){
				$(".tooltip").remove();
			});
	  }
    });  
		
}); 

}); 

 </script>