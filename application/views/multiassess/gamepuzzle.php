<!--<a href="javascript:;" id="popup" class="feedback-button" data-toggle="tooltip" data-placement="bottom" data-html="true" title='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<ul class="guidelines bullet1">
					<li>	
						<i class="fa fa-hand-o-right" style="font-size:18px;color:#000;"></i><span>For every week you will get an assessment set till 9<sup>th</sup> week.</span>
					</li>
					<li>	
						<i class="fa fa-hand-o-right" style="font-size:18px;color:#000;"></i><span>The assessment will be available to play only for respective weeks</span>
					</li>
					<li>	
						<i class="fa fa-hand-o-right" style="font-size:18px;color:#000;"></i><span>From 10<sup>th</sup> week onwards, 1 puzzle will be given for training</span>
					</li>
					<li>	
						<i class="fa fa-hand-o-right" style="font-size:18px;color:#000;"></i><span>In 52<sup>nd</sup> week, post assessment will be given.</span>
					</li>
				</ul>
			</div>'>Guidelines</a>-->

<?php if($this->session->asaponly=='Y'){ ?>
<section class="LBPointLink">
	<a href="#leaderboard" class="lbpointr" title="Leaderboard"> Leaderboard </a>
</section>
<?php } ?>
<div class="loader" style=" display: none; "></div>
<div id="loadingimage" style="display:none;" class="loading"></div>
<div id="puzzleajax"></div>
<?php if($this->session->asaponly=='Y'){ ?>
	<section class="" id="leaderboard" data-bg-img="" style="display:none;">
		<p style="color:#000;"><img src="<?php echo base_url(); ?>assets/images/new/Leaderboardbanner.jpg" alt="Leaderboard" style="width:100%"></p>
		  <div class="container pb-0">
			<div class="col-md-12 col-sm-12 col-xs-12 section-title" style="text-align:center;">
			  <div class="row">
				<div class="col-md-2 col-sm-12 col-xs-12">
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="title-flaticon">
					<i class="flaticon-charity-alms"></i>
				  </div>
				 
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12">
				</div>
			  </div>
			</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12" id="topperslist"></div>  
        
      </div>
    </section>
<?php } ?>	
<style>
.LBPointLink
{
	float: right;
    margin: 20px 10px;
    clear: both;
    display: block;
}
.lbpointr
{
	padding: 10px;
    background: #5664ca;
    color: #fff;
    border-radius: 5px;
	text-decoration: none;
}
a.lbpointr:focus, a.lbpointr:hover
{
	 background: #0cf;
     color: #fff;
	 text-decoration: none;
}

.bold{font-weight:bold;color:#043b71}
.userinfo{font-size:26px;color:#fff;}
/* ---------- Multi ASAP Concept ------------*/
.reportChart{margin-top: 25px;text-align: center;}
.reportChart h3{color: #fff;    padding: 10px;text-align: center;background: #062c65;margin:0;font-family: 'Lobster Two';}
.skillbox{margin-bottom:15px;}
.col-xs-25,
.col-sm-25,
.col-md-25,
.col-lg-25 {
    position: relative;
    min-height: 1px;
    padding-right: 10px;
    padding-left: 10px;
}
.col-xs-25 {
    width: 20%;
    float: left;
}

@media (min-width: 768px) {
.col-sm-25 {
        width: 20%;
        float: left;
    }
}

@media (min-width: 992px) {
    .col-md-25 {
        width: 20%;
        float: left;
    }
}

@media (min-width: 1200px) {
    .col-lg-25 {
        width: 20%;
        float: left;
    }
}
.box_c1 {
    background: #FFF;
    padding: 1px 0px 0px 0px;
    -webkit-box-shadow: 1px 1px 5px 1px #ccc;
    -moz-box-shadow: 1px 1px 5px 1px #ccc;
    box-shadow: 1px 1px 5px 1px #ccc;
}
.reporttitle { color:#1abb9c; }
thead{ background-color: #1abb9c; color: #fff;}
.box_c1 h3 {
    color: #000;
    text-align: center;
    font-weight: 600px;
    margin: 13px 0px 22px 0px;
}
img {
    vertical-align: middle;
}
.tab_sec_c1 p {
    font-size: 20px;
}
p {
    font-weight: 400;
    font-size: 16px;
    line-height: 26px;
    color: #fff;
}
.starinactive {
    width: 30px;
}
.staractive {
    width: 30px;
}
.bounceIn{text-align: center;}
.btn.in_btn {
    background: #ff6600;
    padding: 4px 25px;
    border: 0px;
    color: #FFF;
    font-size: 20px;
    position: relative;
    border-radius: 0px;
    top: 10px;
    border: 2px solid #fff;
    border-radius: 7px;
}
.btnactive span:after {
    content: '\00bb';
    position: absolute;
    opacity: 0;
    top: 0;
    right: -20px;
    transition: 0.5s;
}
.gameBtnInactive {
    color: #333 !important;
    background-color: #e6e6e6 !important;
    border-color: #8c8c8c !important;
}
.reportChartContainer1 {
    padding: 20px;
    overflow: hidden;
	text-align:center;
	border: 1px solid #ccc;
}
#chart-container .fusioncharts-container
{
	border: 1px solid #ccc;
	overflow: hidden;
}
#chart-container
{	text-align:center;
	border: 1px solid #ccc;
	min-height: 270px;
	overflow: hidden;
	    padding: 5px;
}
.cb {
    clear: both;
}
.skillName {
    float: left;
    width: 34.5%;
    display: inline-block;
    padding: 10px 0 11px 0;
    border-right: 1px solid #000;
    margin-bottom: 0;    color: #000;
}
div.meter {
    float: left;
    width: 65%;
    height: 25px;
    border: 1px solid #b0b0b0;
    -webkit-box-shadow: inset 0 3px 5px 0 #d3d0d0;
    -moz-box-shadow: inset 0 3px 5px 0 #d3d0d0;
    box-shadow: inset 0 3px 5px 0 #d3d0d0;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
}
.mt10 {
    margin-top: 9px;
}
div.meter span {
    color: #000;
    top: 0;
    line-height: 0px;
    padding-left: 7px;
    font-weight: bold;
    text-align: right;
    padding-right: 5px;
    display: block;
    height: 100%;
    animation: grower 1s linear;
    -moz-animation: grower 1s linear;
    -webkit-animation: grower 1s linear;
    -o-animation: grower 1s linear;
    position: relative;
    left: -1px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: inset 0px 3px 5px 0px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: inset 0px 3px 5px 0px rgba(0, 0, 0, 0.2);
    box-shadow: inset 0px 3px 5px 0px rgba(0, 0, 0, 0.2);
    background-image: -webkit-gradient(linear, 0 0, 100% 100%, color-stop(0.25, rgba(255, 255, 255, 0.2)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.2)), color-stop(0.75, rgba(255, 255, 255, 0.2)), color-stop(0.75, transparent), to(transparent));
    background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.2) 50%, rgba(255, 255, 255, 0.2) 75%, transparent 75%, transparent);
    background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.2) 50%, rgba(255, 255, 255, 0.2) 75%, transparent 75%, transparent);
    background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, 0.2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.2) 50%, rgba(255, 255, 255, 0.2) 75%, transparent 75%, transparent);
    background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.2) 50%, rgba(255, 255, 255, 0.2) 75%, transparent 75%, transparent);
    -webkit-background-size: 45px 45px;
    -moz-background-size: 45px 45px;
    -o-background-size: 45px 45px;
    background-size: 45px 45px;
}
.redColor {
    background: #e81919;
}
.yellowColor {
    background: #ffa300;
    margin-bottom: 10px;
}
.greenColor {
    background: #8bcc46;
    margin-bottom: 10px;
}
.orangeColor {
    background: #f16202;
    margin-bottom: 10px;
}
.blueColor {
    background: #0ab7f6;
    margin-bottom: 10px;
}
div.meter span:before {
    content: '';
    display: block;
    width: 100%;
    height: 50%;
    position: relative;
    top: 50%;
    background: rgba(0, 0, 0, 0.03);
}
/* ---------- Multi ASAP Concept ------------*/
.section_new{
	overflow:hidden;
	/* margin:10px 0px; */
	padding:15px;
}
.pgtitle{padding: 10px;overflow: hidden;background: #383345;margin: 10px;    border-radius: 10px;display:none;}
.pgtype{padding: 10px;overflow: hidden;background: #fafafa;margin:10px 10px 0px 10px;text-align:center;}
.masapcls{margin: 5px;}

.Played{background: #1fc51c;border: 1px solid #1fc51c;color:#fff;}
.Play{background: #ff5809;border: 1px solid #ff5809;color:#fff;}
.Yettoplay{background: #ccc;border: 1px solid #ccc;color:#000;cursor: not-allowed;}
</style>
<script type="text/javascript">
$(document).ready(function(){
	MultiAsap();
	loadleaderboard();
}); 
function MultiAsap()
{
	$('#loadingimage').show();
	var uid='<?php echo $userid;?>';
	var org_id='<?php echo $org_id;?>';
	
	
		$.ajax({
				type: "POST",
				url: "<?php echo base_url()."index.php/multiassess/masap_ajax"; ?>",
				data: {uid:uid,org_id:org_id},
				success: function(result){
						$('#puzzleajax').html(result);
						fancyCall();
						$('#loadingimage').hide();
				}
			});
}
function loadleaderboard()
{ 
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()."index.php/multiassess/org_topperslist"; ?>",
			data: {},
			success: function(result){
				if(result!=0)
				{	
					$('#leaderboard').show();
					$('#topperslist').html(result);
				}
				else
				{
					$('#leaderboard').hide();
				}
			}
		});
}
function AsapEndPopup()
{ 
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()."index.php/multiassess/asapendpopup"; ?>",
			data: {},
			success: function(result){
				if(result==1)
				{	
					//Show PopUp
					swal("Good Job!", "You have completed assessment. The training strats from your next login.", "success")
				}
				else
				{
					//Don't show PopUp
				}
			}
		});
}
</script>