<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" media="screen">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/charts.js"></script>
<?php 
	$arrskillid=array("59"=>"ME","60"=>"VP","61"=>"FA","62"=>"PS","63"=>"LI");
	$arrskillname=array("ME"=>"Memory","VP"=>"Visual Processing
","FA"=>"Focus & Attention","PS"=>"Problem Solving","LI"=>"Linguistics");
$skillcolor=array("ME"=>"#da0404","VP"=>"#ffc000","FA"=>"#92d050","PS"=>"#ff6600","LI"=>"#00b0f0");
 ?>
	<div class="section_four1">
 <div class="container">
<div class="panel panel-default">
<div class="panel-body">
<div class="tab-content responsive">
<div class="tab-pane active" id="BrainSkillsOn">
<div class="tab_sec_c1">
 <h3> </h3>
<div class="row">
<?php
 $myskillpie=array("59"=>"#cdcdcd","60"=>"#cdcdcd","61"=>"#cdcdcd","62"=>"#cdcdcd","63"=>"#cdcdcd");
 //$myskillpie=array("59"=>"#ff6600","60"=>"#067d00","61"=>"#ffbe00","62"=>"#be0000","63"=>"#1e9ddf");
 $myskillpie_orginal=array("59"=>"#da0404","60"=>"#ffc000","61"=>"#92d050","62"=>"#ff6600","63"=>"#00b0f0");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="">
		<?php foreach($arrskillid as $skillid){ 
		
		
		$skillhint='';
	 	 
		if($skillid=='ME'){$skillhint='This skill reflects how fast your brain encodes, stores and retrieves information.';}
		else if($skillid=='VP'){$skillhint="This skill reflects how you organise and interpret information you see and give it meaning.";}
		else if($skillid=='FA'){$skillhint='This skill reflects the three components of attention: alertness, orientating and executive functions.';}
		else if($skillid=='PS'){$skillhint='This skill reflects the act of analytical and creative thinking to identify problems and evaluate alternatives.';}
		else if($skillid=='LI'){$skillhint='This skill reflects phonic and sight word recognition with images.';}
		
		
			/*echo $gamescore[0][$skillid];exit;
			echo "<pre>";print_r($gamescore);exit;*/
			 $avg_game_score=$gamescore[0][$skillid];
			if($avg_game_score < 20) $filled_stars = 0;
			if($avg_game_score >= 20 && $avg_game_score <= 40)	$filled_stars = 1;
			if($avg_game_score >= 41 && $avg_game_score <= 60)	$filled_stars = 2;
			if($avg_game_score >= 61 && $avg_game_score <= 80)	$filled_stars = 3;
			if($avg_game_score >= 81 && $avg_game_score <= 90)	$filled_stars = 4;
			if($avg_game_score >= 91 && $avg_game_score <= 100)	$filled_stars = 5; 
			?>
			<div class="col-md-25 col-sm-25 col-lg-6 col-xs-12  skillbox">
				<div class="box_c1 bounceIn " style="background-color:<?php echo $skillcolor[$skillid]; ?>">
				<h3><?php echo $arrskillname[$skillid]; ?> <a class="tooltipicon" style="" href="javascript:;" data-toggle="tooltip" data-placement="bottom" data-html="true" title='<div class=""><span style="font-size:12px;"><?php echo $skillhint; ?></span></div>'><i  style="color:black; font-size:16px;" class="fa fa-info-circle"></i></a></h3> 
				<?php if(isset($gamescore[0][$skillid]) && $gamescore[0][$skillid]!=''){ ?>
				  <a  href="javascript:;"><img src="<?php echo base_url(); ?>assets/<?php echo $MultiAssesGameList[0][$skillid.'imgpath']; ?>"/></a>
				<?php  } else {?>        
				  <a class="fancybox fancybox.iframe" id="<?php //echo $MultiAssesGameList[0][$skillid]; ?>" href="javascript:;" data-href="<?php echo base_url()."assets/swf/".$this->session->asapgameurl."/".$MultiAssesGameList[0][$skillid.'gamehtml'].".html"; ?>"><img src="<?php echo base_url(); ?>assets/<?php echo $MultiAssesGameList[0][$skillid.'imgpath']; ?>"/></a>
				<?php } ?>

				
				<p><?php //echo $MultiAssesGameList[0][$skillid.'gname']; ?></p>
				<p style="background-color: #fff;box-shadow: 2px 0px 8px 2px rgb(183, 180, 180);" id="stars<?php echo $MultiAssesGameList[0][$skillid]; ?>">
				<?php for($i=0;$i<$filled_stars;$i++){?>
					 <img class="staractive" src="<?php echo base_url(); ?>assets/images/icon_StarActive.png">
				<?php } ?>
				<?php for($i=0;$i<5-$filled_stars;$i++){  ?>
					<img class="starinactive" src="<?php echo base_url(); ?>assets/images/icon_StarInActive.png">
				<?php } ?>
				 </p>
				<?php if(isset($gamescore[0][$skillid]) && $gamescore[0][$skillid]!=''){ ?>
					<a style="" class="btn btn-default in_btn gameBtnInactive" href="javascript:;">Completed</a>
				<?php } else { ?>
					<a style="" class="btn btn-default in_btn fancybox fancybox.iframe btn-4 btnactive" id="<?php //echo $MultiAssesGameList[0][$skillid]; ?>" href="javascript:;" data-href="<?php echo base_url()."assets/swf/".$this->session->asapgameurl."/".$MultiAssesGameList[0][$skillid.'gamehtml'].".html"; ?>"><span>Play</span></a>
				<?php } ?>

				</div>
			</div>
		<?php } ?>
			</div>
		</div>
	</div>

			<div class="row reportChart">
			 <div class="clearfix"></div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					
					<div class="panel panel-default">
                        <div class="panel-body">
						<h3>Skill Scores</h3>
					<div class="reportChartContainer1">
                            <div class="cb">
                            	<p class="skillName pt0">Memory</p>
                                	
                            	<div class="meter mt10">
                                	<span class="redColor" style="width:<?php if($gamescore[0]['ME']!=''){echo $gamescore[0]['ME'];}else{echo 0;} ?>%"><?php if($gamescore[0]['ME']!=''){echo $gamescore[0]['ME'];}else{echo 0;} ?></span>
								</div>
                            </div>
                            <div class="cb">
                            	<p class="skillName">Visual Processing</p>
                            	<div class="meter mt10">
  									<span class="yellowColor" style="width:<?php if($gamescore[0]['VP']!=''){echo $gamescore[0]['VP'];}else{echo 0;} ?>%"><?php if($gamescore[0]['VP']!=''){echo $gamescore[0]['VP'];}else{echo 0;} ?></span>
								</div>
                            </div>
                            <div class="cb">
                            	<p class="skillName">Focus and Attention</p>
                            	<div class="meter mt10">
  									<span class="greenColor" style="width:<?php if($gamescore[0]['FA']!=''){echo $gamescore[0]['FA'];}else{echo 0;} ?>%"><?php if($gamescore[0]['FA']!=''){echo $gamescore[0]['FA'];}else{echo 0;} ?></span>
								</div>
                            </div>
                            <div class="cb">
                            	<p class="skillName">Problem Solving</p>
                            	<div class="meter mt10">
  									<span class="orangeColor" style="width:<?php if($gamescore[0]['PS']!=''){echo $gamescore[0]['PS'];}else{echo 0;} ?>%"><?php if($gamescore[0]['PS']!=''){echo $gamescore[0]['PS'];}else{echo 0;} ?></span>
								</div>
                            </div>
                            <div class="cb">
                            	<p class="skillName">Linguistics</p>
                            		<div class="meter mt10">
  										<span class="blueColor" style="width:<?php if($gamescore[0]['LI']!=''){echo $gamescore[0]['LI'];}else{echo 0;} ?>%"><?php if($gamescore[0]['LI']!=''){echo $gamescore[0]['LI'];}else{echo 0;} ?></span>
									</div>
                            </div>
                     </div>
				</div>
				</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
						<h3>Brain Skill Power Index: &nbsp; <b><?php if($gamescore[0]['bspi']!=''){echo $gamescore[0]['bspi']."  % "; }else{ echo '-';} ?></b></h3>
                            <div id="chart-container"></div>
                        </div>
                    </div>
				</div>
			</div>
			<!-- Row end -->
</div><!--/tab_sec_c1 -->
</div><!--/tab-pane -->
</div><!-- tab-content -->
</div><!-- panel-body -->
</div><!-- panel-default -->
 </div><!-- container -->
 </div><!-- section_four -->
<script type="text/javascript">

$(document).ready(function(){
	
	fancyCall();
}); 
function fancyCall()
{ 
$("a.fancybox").each(function() {  
var tthis = this;
$(this).fancybox({
'transitionIn'    :    'elastic',
'transitionOut'    :    'elastic',
'speedIn'     :    600,
'speedOut'     :    200,
'overlayShow'    :    false,
'width'  : 750,           // set the width
'height' : 500,           // set the height
'type'   : 'iframe',       // tell the script to create an iframe
'scrolling'   : 'no',
idleTime: false,
'href': $(this).attr('data-href'),
'afterClose': function (){
	$('#loadingimage1').show();
	var uid='<?php echo $userid;?>';
	var org_id='<?php echo $org_id;?>';
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()."index.php/multiassess/masap_ajax"; ?>",
			data: {uid:uid,org_id:org_id},
			success: function(result){
				$('#puzzleajax').html(result);
				fancyCall();
			}
		});
	loadleaderboard();
	AsapEndPopup();
},
beforeShow : function()
{ 
	$(".fancybox-inner").addClass("fancyGameClass");
	var uid='<?php echo $userid;?>';
	var org_id='<?php echo $curorgid;?>';
	
	$.ajax({
	type: "POST",
	url: "<?php echo base_url()."index.php/multiassess/gamesajax"; ?>",
	data: {gameid:tthis.id,gameurl:$(tthis).attr('data-href'),uid:uid,org_id:org_id},
	success: function(result){
	if($.trim(result)=='IA')
	{
		$.fancybox.close();
	}	 
	}
	});
}
});
});
}
</script>
<script type="text/javascript">

	FusionCharts.ready(function () {
		var csatGauge = new FusionCharts({
			"type": "angulargauge",
			"renderAt": "chart-container",
			"width": "100%",
			"height": "250",
			"dataFormat": "json",
				"dataSource": {
		"chart": {
			"caption": "",
			"lowerlimit": "0",
			"upperlimit": "100",
			"lowerlimitdisplay": "0%",
			"upperlimitdisplay": "100%",
			"palette": "1",
			"numbersuffix": "%",
			"tickvaluedistance": "10",
			"showvalue": "0",
			"gaugeinnerradius": "0",
			"bgcolor": "FFFFFF",
			"pivotfillcolor": "333333",
			"pivotradius": "8",
			"pivotfillmix": "333333, 333333",
			"pivotfilltype": "radial",
			"pivotfillratio": "0,100",
			"showtickvalues": "1",
			"showborder": "0"
		},
		"colorrange": {
			"color": [
				{
					"minvalue": "0",
					"maxvalue": "<?php echo $gamescore[0]['bspi']+.5 ?>",
					"code": "e44a00"
				},
				{
					"minvalue": "<?php echo $gamescore[0]['bspi']+1; ?>",
					"maxvalue": "100",
					"code": "#B0B0B0"
				}
			]
		},
		"dials": {
			"dial": [
				{
					"value": "<?php echo $gamescore[0]['bspi']; ?>",
					"rearextension": "15",
					"radius": "100",
					"bgcolor": "333333",
					"bordercolor": "333333",
					"basewidth": "8"
				}
			]
		}
	}
	});
		csatGauge.render();
		
	});
$(function () {
   $('[data-toggle="tooltip"]').tooltip();
});
</script>