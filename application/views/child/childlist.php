<?php
$CI =& get_instance();
//$CI->your_method($param);
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="main-timeline">
			<?php
				//foreach($ChildList as $child)
				$regchildcount=$ChildLimit-count($ChildList);
				//echo $regchildcount."==".$ChildLimit."==".count($ChildList);
				$j=1;
				for($i=0;$i<$ChildLimit;$i++)
				{ ?>
			<?php if(isset($ChildList[$i]['id'])){ ?>
                <div class="timeline">
                    <span class="icon fa fa-globe"></span>
					<div class="timeline-content childdiv<?php echo $j; ?>">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">Name <h3><?php echo $ChildList[$i]['fname']; ?></h3></div>
							<div class="col-md-6 col-sm-6 col-xs-6">Gender <h3><?php 
							if($ChildList[$i]['gender']=='M'){ echo "Male";} else{echo "Female"; } ?></h3></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">Startdate<h3><?php echo date('d-m-Y',strtotime($ChildList[$i]['startdate'])); ?></h3></div>
							<div class="col-md-6 col-sm-6 col-xs-6">Enddate <h3><?php echo date('d-m-Y',strtotime($ChildList[$i]['enddate'])); ?></h3></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">Grade <h3><?php echo str_replace("Grade","",$ChildList[$i]['gradename']); ?></h3></div>
							<div class="col-md-6 col-sm-6 col-xs-6"><h3>Time Of Play</h3>
									<select name="ddltimeofplay" id="ddltimeofplay" class="ddltimeofplay" disabled style="width:33%;color: #000;font-weight: bold;">
										<option value="30" selected>30</option>
										<option value="45">45</option>
										<option value="60">60</option>
									</select></div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6"><h3><a href="<?php echo base_url()."index.php/home/dashboard?cid=".urlencode($CI->my_simple_crypt( $ChildList[$i]['id'], 'Ed6S@2018' )); ?>" id="C<?php echo $j; ?>" class="childlistlink" data-ChildId="<?php echo $ChildList[$i]['id']; ?>" title="Play Puzzles" target="_blank">Play Puzzles</a></h3></div>
							<div class="col-md-6 col-sm-6 col-xs-6"><h3><a href="<?php echo base_url()."index.php/home/dashboard?cid=".urlencode($CI->my_simple_crypt( $ChildList[$i]['id'], 'Ed6S@2018' )); ?>" id="C<?php echo $j; ?>" class="childlistlink" data-ChildId="<?php echo $ChildList[$i]['id']; ?>" title="Play Puzzles" target="_blank">View Reports</a></h3></div>
						</div>
					</div>
				</div>
			<?php } else { ?>
				
					<div class="timeline">
						<span class="icon fa fa-globe"></span>
						<div class="timeline-content childdiv<?php echo $j; ?>">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 Newchildadd"><h3><a href="<?php echo base_url(); ?>index.php/home/childadd" id="C<?php echo $j; ?>" class="childlistlink" title="AddChild" onclick="openNav()">Add New Child</a></h3></div>
							</div>
						</div>
					</div><br/>
			<?php } ?>
			 <?php $j++; } ?>

			  </div>
        </div>
    </div>
</div>
<style>


.childlistlink{
	padding: 5px 20px;
    background: #1d86b0;
    overflow: hidden;
    font-size: 20px;
    border-radius: 5px;
}
.main-timeline{
    padding-top: 50px;
    overflow: hidden;
    position: relative;
}
.main-timeline:before{
    content: "";
    width: 7px;
    height: 100%;
    background: #084772;
    margin: 0 auto;
    position: absolute;
    top: 80px;
    left: 0;
    right: 0;
}
.main-timeline .timeline{
    width: 50%;
    float: left;
    padding: 20px 35px;
    border-top: 7px solid #084772;
    border-right: 7px solid #084772;
    border-radius: 0 30px 0 0;
    position: relative;
    right: -3.5px;
}
.main-timeline .icon{
    display: block;
    width: 50px;
    height: 50px;
    line-height:50px;
    border-radius: 50%;
    background: #e84c47;
    border: 1px solid #fff;
    text-align: center;
    font-size: 25px;
    color: #fff;
    box-shadow: 0 0 0 2px #e84c47;
    position: absolute;
    top: -30px;
    left: 0;
}
.main-timeline .timeline-content{
    display: block;
    padding: 30px 10px 10px;
    border-radius: 20px;
    background: #e84c47;
    color: #fff;
    position: relative;
	overflow: hidden;
}
.main-timeline .timeline-content:hover{
    text-decoration: none;
    color: #fff;
}
.main-timeline .timeline-content:before, .main-timeline .timeline-content:after
{
    content: "";
    display: block;
    width: 10px;
    height: 50px;
    border-radius: 10px;
    background: #e84c47;
    border: 1px solid #fff;
    position: absolute;
    top: -35px;
    left: 50px;
}
.main-timeline .timeline-content:after{
    left: auto;
    right: 50px;
}
.main-timeline .title{
    font-size: 24px;
    margin: 0;
}
.main-timeline .description{
    font-size: 15px;
    letter-spacing: 1px;
    margin: 0 0 5px 0;
}
.main-timeline .timeline:nth-child(2n){
    border-right: none;
    border-left: 7px solid #084772;
    border-radius: 30px 0 0 0;
    right: auto;
    left: -3.5px;
}
.main-timeline .timeline:nth-child(2n) .icon{
    left: auto;
    right: 0;
}
.main-timeline .timeline:nth-child(2){ /* margin-top: 130px; */ }
.main-timeline .timeline:nth-child(odd){ /* margin: -130px 0 30px 0; */ }
.main-timeline .timeline:nth-child(even){ margin-bottom: 80px; }
.main-timeline .timeline:first-child,.main-timeline .timeline:last-child:nth-child(even){ margin: 0 0 30px 0; }
.main-timeline .timeline:nth-child(2n) .timeline-content,.main-timeline .timeline:nth-child(2n) .timeline-content:before,
.main-timeline .timeline:nth-child(2n) .timeline-content:after,.main-timeline .timeline:nth-child(2n) .icon{ background: #4bd9bf;}
.main-timeline .timeline:nth-child(2n) .icon{ box-shadow: 0 0 0 2px #4bd9bf; }
.main-timeline .timeline:nth-child(3n) .timeline-content,
.main-timeline .timeline:nth-child(3n) .timeline-content:before,
.main-timeline .timeline:nth-child(3n) .timeline-content:after,
.main-timeline .timeline:nth-child(3n) .icon{ background: #ff9e09;     overflow: hidden;}
.main-timeline .timeline:nth-child(3n) .icon{ box-shadow: 0 0 0 2px #ff9e09; }
.main-timeline .timeline:nth-child(4n) .timeline-content,
.main-timeline .timeline:nth-child(4n) .timeline-content:before,
.main-timeline .timeline:nth-child(4n) .timeline-content:after,
.main-timeline .timeline:nth-child(4n) .icon{ background: #3ebae7;     overflow: hidden;}
.main-timeline .timeline:nth-child(4n) .icon{ box-shadow: 0 0 0 2px #3ebae7; }
@media only screen and (max-width: 767px){
    .main-timeline:before{
        left: 0;
        right: auto;
    }
    .main-timeline .timeline,.main-timeline .timeline:nth-child(even),.main-timeline .timeline:nth-child(odd){
        width: 100%;
        float: none;
        padding: 20px 30px;
        margin: 0 0 30px 0;
        border-right: none;
        border-left: 7px solid #084772;
        border-radius: 30px 0 0 0;
        right: auto;
        left: 0;
    }
    .main-timeline .icon{
        left: auto;
        right: 0;
    }
}
@media only screen and (max-width: 480px){
    .main-timeline .title{ font-size: 18px; }
}
.timeline h3{margin:10px 0px;}
</style>