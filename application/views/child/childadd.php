<div class="ChildReg"> 
<div id="mainContDisp" class="container playGames homePlayGames"> 
<div class="col-md-12 col-sm-12 col-xs-12 ">
			<h3> Child Registration</h3>
			<div class="col-md-12 col-sm-12 col-xs-12" id="successmsg1">
				<form name="frmchildreg" id="frmchildreg" class="frmchildreg" method="post" enctype="multipart/form-data" accept-charset="utf-8" style="padding-bottom: 20px;" novalidate="novalidate" action="<?php  echo base_url('index.php/home/childreg') ?>">
				<div class="divchildreg" id="divchildreg">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="contact-form" class="form-container1">
								<div class="col-md-6 col-sm-6 col-xs-12">
								
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtFName">Name <span style="color:red">*</span></label>
									   <div class="input-group">
									   <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
										<input type="text" maxlength="40" class="form-control alphaOnly" name="txtFName" value="" id="txtFName">
									  </div> 
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtFName">Gender</label>
										<div class="form-group"> 
										<div class="col-sm-3"><label class="radio-inline"><input type="radio" class=""  name="rdGender" value="M" id="rdGM">Male</label></div>
										<div class="col-sm-3"><label class="radio-inline"><input type="radio" class=""  name="rdGender" value="F" id="rdGF">Female</label></div>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
											<label for="txtFName">User Type</label>
										<div class="form-group"> 
											<div class="col-sm-3"><label class="checkbox-inline"><input type="checkbox" class=""  name="txtSpecialChild" value="Y" id="SpecialChild">SpecialChild</label></div>
										</div>
									</div>
								
															
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10 NC">
										<label for="ddlGrade">Grade</label>
									  <div class="input-group">
										 <span class="input-group-addon"><i class="glyphicon glyphicon-chevron-down"></i></span>
											<select class="form-control"  name="ddlGrade" id="ddlGrade" style="margin-bottom: 0px;    border: 1px solid #ccc;">
											<option value="">Select</option>
											<?php foreach($GradeList as $row){?>
											<option value="<?php echo $row['grade_id']; ?>" rel="<?php echo $row['grade_name']; ?>"><?php echo $row['grade_name']; ?></option>
											<?php } ?>
											</select> 
									  </div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10 SC" style="display:none;">
										<label for="ddlStage">Stage</label>
									  <div class="input-group">
									  <span class="input-group-addon"><i class="glyphicon glyphicon-chevron-down"></i></span>
										<select class="form-control"  name="ddlStage" id="ddlStage" style="margin-bottom: 0px;    border: 1px solid #ccc;">
											<option value="">Select</option>
											<option value="11" rel="PREKG">Stage 1</option>
											<option value="1" rel="LKG">Stage 2</option>
											<option value="2" rel="UKG">Stage 3</option>
										</select> 
									  </div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtMobile">State<span style="color:red">*</span></label>
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-chevron-down"></i></span>
											<select class="form-control"  name="ddlState" id="ddlState" style="margin-bottom: 0px;    border: 1px solid #ccc;">
											<option value="">Select</option>
											<?php foreach($StateList as $row){?>
												<option value="<?php echo $row['id']; ?>" rel="<?php echo $row['state_name']; ?>"><?php echo $row['state_name']; ?></option>
											<?php } ?>
											</select> 
										</div>
									</div>
															
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="ddlCity">City<span style="color:red">*</span></label>
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-chevron-down"></i></span>
											<select class="form-control"  name="ddlCity" id="ddlCity" style="margin-bottom: 0px;    border: 1px solid #ccc;">
											<option value="">Select</option>
											</select> 
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtPincode">Pincode<span style="color:red">*</span></label>
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
											<input type="text" class="form-control numbersOnly" maxlength="6"  name="txtPincode" value="" id="txtPincode">
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtSchoolname">School Name</label>
									  <div class="input-group">
										 <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
											<select class="form-control"  name="txtSchoolname" id="txtSchoolname" style="margin-bottom: 0px;    border: 1px solid #ccc;">
											<option value="">Select</option>
											<?php foreach($SchooLlist as $row){?>
											<option value="<?php echo $row['id']; ?>" rel="<?php echo $row['school_name']; ?>"><?php echo $row['school_name']; ?></option>
											<?php } ?>
											</select> 
									  </div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtCouponcode">Couponcode<span style="color:red">*</span></label>
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
											<input type="text" class="form-control" maxlength="8"  name="txtCouponcode" value="" id="txtCouponcode">
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 pd10">
										<label for="txtCouponcode">Address<span style="color:red">*</span></label>
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
											<textarea class="form-control"  name="txtAddress" id="txtAddress"></textarea>
										</div>
									</div>
								
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div style="text-align:center;clear:both;">
							<div style="padding-bottom:5px;">   
								 <label style="color:red;font-size: 21px;" class="error" id="errcoupcode"></label>
							</div>
							<input type="reset" id="frmreset" class="btn default">
							<input type="button" id="btnSubmit" style="float:none;" class="btn btn-success" value="Add Child">
						</div>
					</div>
					
				</div>
					
				<div id="frmpayment" class="frmpayment">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="payable-form" class="form-container1">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 pd10">
											<label for="VALIDITY">VALIDITY<span style="color:black">:</span></label>
										   <div class="input-group">
											<div id="VALIDITY"></div>
										  </div> 
										</div>
										<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 pd10">
											<label for="PLANAMOUNT">PLAN AMOUNT<span style="color:black">:</span></label>
										   <div class="input-group">
											<div id="PLANAMOUNT"></div>
										  </div> 
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 pd10">
											<label for="DISCOUNTEDAMOUNT">DISCOUNTED AMOUNT<span style="color:black">:</span></label>
										   <div class="input-group">
											<div id="DISCOUNTEDAMOUNT"></div>
										  </div> 
										</div>
										<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 pd10">
											<label for="PAYMENTAMOUNT">PAYMENT AMOUNT<span style="color:black">:</span></label>
										   <div class="input-group">
											<div id="PAYMENTAMOUNT"></div>
										  </div> 
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div style="text-align:center;clear:both;">
								<input type="submit" id="btnpayment" style="float:none;" class="btn btn-success" value="Proceed">
							</div>
						</div>
				</div>
				
				</form>
				
				
				
			</div>	
		</div>
</div>
<div class="">
	<div class="rella-row_bottom_divider" style="">
		<svg fill="#fff" preserveAspectRatio="none"   viewBox="0 0 1000 300">
			<path d="M 1000 299 l 2 -279 c -155 -36 -310 135 -415 164 c -102.64 28.35 -149 -32 -232 -31 c -80 1 -142 53 -229 80 c -65.54 20.34 -101 15 -126 11.61 v 54.39 z"></path>
			<path d="M 1000 286 l 2 -252 c -157 -43 -302 144 -405 178 c -101.11 33.38 -159 -47 -242 -46 c -80 1 -145.09 54.07 -229 87 c -65.21 25.59 -104.07 16.72 -126 10.61 v 22.39 z"></path>
			<path d="M 1000 300 l 1 -230.29 c -217 -12.71 -300.47 129.15 -404 156.29 c -103 27 -174 -30 -257 -29 c -80 1 -130.09 37.07 -214 70 c -61.23 24 -108 15.61 -126 10.61 v 22.39 z"></path>
		</svg>
	</div>
</div>
</div>
<script>
$("#frmchildreg").validate({
        rules: {
            "txtFName": {required: true},
            "rdGender": {required: true},
			"ddlGrade": {required: true},		
			"ddlStage": {required: true},		
			"ddlState": {required: true},		
			"ddlCity": {required: true},		
			"txtPincode": {required: true},
			"txtCouponcode": {required: true},
			"txtSchoolname": {required: true},
			"txtAddress": {required: true}
			
			},
        messages: {
            "txtFName": {required: "Please enter name"},
            "rdGender": {required: "Please select gender"},
            "ddlGrade": {required: "Please select grade"},
            "ddlStage": {required: "Please select stage"},
            "ddlState": {required: "Please select state"},
            "ddlCity": {required: "Please select city"},
            "txtPincode": {required: "Please enter pincode"},
            "txtCouponcode": {required: "Please enter pincode"},
            "txtSchoolname": {required: "Please select schoolname"},
			"txtAddress": {required: "Please enter address"}
			
        },
		errorPlacement: function(error, element)
		{
			if (element.attr("type") === "radio") {
				error.insertAfter(element.parent());
			} 
			if (element.attr("type") === "checkbox") {
				error.insertAfter($(".errmmsg"));
			}
			else if (element.attr("id") === "txtMobile") {
				error.insertAfter(element.parent());
			} 

			else {
				error.insertAfter(element.parent());
			}

		},
		highlight: function(input) {
           // $(input).addClass('error');
        } 
    });
	
	$('#ddlState').change(function(){
		StateChange();
	});
	$('#SpecialChild').click(function(){
		if($('#SpecialChild').is(":checked"))
		{
			$(".NC").hide();
			$(".SC").show();
			
			/*$("#ddlStage").val(1);
			$("#ddlGrade").val(11);
			 $("#ddlStage").attr("disabled",true);
			$("#ddlGrade").attr("disabled",true); */
		}
		else
		{
			$(".SC").hide();
			$(".NC").show();
			/*$("#ddlGrade").val('');
			 $("#ddlGrade").attr("disabled",false); */
		}
	});
	$("#frmpayment").hide();
	
	$('#btnSubmit').click(function(){
			if($("#frmchildreg").valid()==true)
			{
				/* $.ajax({
				url: "<?php echo base_url(); ?>index.php/home/couponajax",
				type:'POST',
				data:{couponcode:$("#txtCouponcode").val()},
				success: function(result)
				{
					if($.trim(result)=='ISVALID')
					{ */
				
							var form = $('#frmchildreg');
							$('.loading').show();
							$.ajax({
							type:"POST",
							dataType:"json",
							url:"<?php echo base_url('index.php/home/couponajax') ?>",
							data:form.serialize(),
							success:function(result)
							{	
								$('.loading').fadeOut("slow");
								if(result.status=='1')
								{
									window.location.href= "<?php echo base_url();?>index.php/home/childregsuc?hdnPaymSubscribeID="+result.url;
								}
								else if(result.status=='2')
								{
									$('#divchildreg').hide();
									//$('#frmchildreg').html('');
									$('#VALIDITY').html(result.Validity);									
									$('#PLANAMOUNT').html(result.PlanAmount);
									$('#DISCOUNTEDAMOUNT').html(result.DiscountAmount);
									$('#PAYMENTAMOUNT').html(result.PayableAmount);
									$("#frmpayment").show();
								}
								else
								{
									$("#errcoupcode").show().html(result.msg);
								}
							}
						});
					/* }
					else if($.trim(result)=='INVALID')
					{
						$("#errcoupcode").show().html('The given coupon code is not valid');
					}
					else if($.trim(result)=='EMPTY')
					{
						$("#errcoupcode").show().html('Please enter the couponcode');
					}
				}
				}); */
			}
		});
/* $('#btnpayment').click(function(){
	$('form[name=frmchildreg]').attr('action','<?php  echo base_url('index.php/home/childreg') ?>');
    $('form[name=frmchildreg]').submit();
}); */
function StateChange()
{	
	var stateid=$('#ddlState').val();
	$.ajax({
		type:"POST",
		url:"<?php echo base_url('index.php/home/getCityList') ?>",
		data:{stateid:stateid},
		success:function(result)
		{
			$('#ddlCity').html(result);
		}
	}); 
	
}	
</script>
