<style>
.demo{ background: #e6e6e8; }
.pricingTable{
text-align: center;
padding-bottom: 12px;
position: relative;
}
.pricingTable:before{
content: "";
width: 98%;
height: 85%;
border-radius: 40px;
background: #fff;
margin: 0 auto;
position: absolute;
box-shadow: 0 0 0 10px #fff inset, 0 0 0 17px #d7182a inset;
bottom: 0;
left: 0;
right: 0;
transition: all 0.3s ease 0s;
}
.pricingTable:hover:before{ box-shadow: 0 0 0 10px #fff inset, 0 0 0 17px #D7182A inset, 0 0 60px rgba(0,0,0,0.5) inset; }
.pricingTable .pricingTable-header{
width: 110%;
position: relative;
left: 50%;
transform: translateX(-50%);
}
.pricingTable .pricingTable-header svg{ width: 94%; }
.pricingTable .title{
display: inline-block;
padding: 0 0 10px 0;
margin: 0 0 20px 0;
font-size: 30px;
color: #d7182a;
text-transform: uppercase;
border-bottom: 7px dotted rgba(0,0,0,0.2);
transition: all 0.3s ease 0s;
}
.pricingTable:hover .title{ padding: 0 20px 10px 20px; }
.pricingTable .price-value{
color: #fff;
position: absolute;
top: 67px;
left: 50%;
transform: translateX(-50%);
}
.price-value .amount{
display: block;
font-size: 50px;
font-weight: 600;
}
.price-value .month{
display: block;
font-size: 16px;
font-weight: 500;
line-height: 0;
text-transform: lowercase;
}
.pricingTable .pricing-content{
padding: 0;
margin: 0 0 20px 0;
list-style: none;
position: relative;
}
.pricingTable .pricing-content li{
font-size: 18px;
color: #707070;
line-height: 40px;
}
.pricingTable .pricingTable-signup{
display: inline-block;
padding: 10px 20px;
background: #d7182a;
font-size: 22px;
font-weight: 600;
color: #fff ;
letter-spacing: 1px;
text-transform: uppercase;
border-radius: 20px 20px 0 0;
overflow: hidden;
z-index: 1;
position: relative;
transition: all 0.3s ease 0s;
}
.pricingTable .pricingTable-signup:hover{ color: #d7182a; }
.pricingTable .pricingTable-signup:before{
content: "";
width: 90%;
height: 80%;
background: #fff;
border-radius: 20px 20px 0 0;
position: absolute;
top: 150%;
left: 50%;
z-index: -1;
opacity: 0;
transform: translate(-50%, -50%);
transition: all 0.3s ease 0s;
}
.pricingTable .pricingTable-signup:hover:before{
top: 50%;
opacity: 1;
}
.pricingTable.darkblue:before{ box-shadow: 0 0 0 10px #fff inset, 0 0 0 17px #1a6bac inset; }
.pricingTable.darkblue:hover:before{ box-shadow: 0 0 0 10px #fff inset, 0 0 0 17px #1a6bac inset, 0 0 60px rgba(0,0,0,0.5) inset; }
.pricingTable.darkblue .pricingTable-header svg path{ fill: #1a6bac; }
.pricingTable.darkblue .pricingTable-signup{ background: #1a6bac; }
.pricingTable.darkblue .pricingTable-header h3,
.pricingTable.darkblue .pricingTable-signup:hover{ color: #1a6bac; }
.pricingTable.blue:before{ box-shadow: 0 0 0 10px #fff inset, 0 0 0 17px #2998c5 inset; }
.pricingTable.blue:hover:before{ box-shadow: 0 0 0 10px #fff inset, 0 0 0 17px #2998c5 inset, 0 0 60px rgba(0,0,0,0.5) inset; }
.pricingTable.blue .pricingTable-header svg path{ fill: #2998c5; }
.pricingTable.blue .pricingTable-signup{ background: #2998c5; }
.pricingTable.blue .pricingTable-header h3,
.pricingTable.blue .pricingTable-signup:hover{ color: #2998c5; }
@media only screen and (max-width: 990px){
.pricingTable{ margin-bottom: 40px; }
}
@media only screen and (max-width: 767px){
.pricingTable .pricingTable-header{ width: 102%; }
}
@media only screen and (max-width: 479px){
.pricingTable .price-value{ top: 80px; }
.price-value .amount{ font-size: 40px; }
}
</style>
</head>
<body>
<div class="demo">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricingTable-header">
                        <svg x="0px" y="0px" width="350px" height="230px" viewBox="0 5 350 200">
                            <g>
                                <path fill="#D7182A" d="M58.377,62.185c11.838-16.874,24.94-20.129,43.466-18.93C102.476,24.326,166.999,15.932,175.042,0
                c13.916,18.501,71.028,22.526,72.926,43.769c18.436-3.169,39.31,6.852,42.563,17.131c76.091-0.171,82.236,75.631,0.633,78.715
                c-9.939,13.106-23.496,19.87-43.287,17.988C244.805,176.36,190.766,183.04,174.59,200c-13.285-17.216-71.12-23.04-71.934-42.482
                c-18.795,2.228-33.888-4.284-44.732-18.502C-31.088,134.903-6.688,54.989,58.377,62.185L58.377,62.185z"></path>
                                <path fill="none" stroke="#FFFFFF" stroke-width="0" stroke-linecap="square" stroke-miterlimit="10" d="M58.377,62.185
                c11.838-16.874,24.94-20.129,43.466-18.93C102.476,24.326,166.999,15.932,175.042,0c13.916,18.501,71.028,22.526,72.926,43.769
                c18.436-3.169,39.31,6.852,42.563,17.131c76.091-0.171,82.236,75.631,0.633,78.715c-9.939,13.106-23.496,19.87-43.287,17.988
                C244.805,176.36,190.766,183.04,174.59,200c-13.285-17.216-71.12-23.04-71.934-42.482c-18.795,2.228-33.888-4.284-44.732-18.502
                C-31.088,134.903-6.688,54.989,58.377,62.185L58.377,62.185z"></path>
                                <path fill="none" stroke="#FFFFFF" stroke-width="3.54" d="M112.326,54.133c-4.519-21.67,51.78-25.096,63.167-39.401
            c12.741,16.617,67.686,18.501,61.993,39.829c18.615-4.54,45.183,6.509,46.81,17.216c68.048,0.771,75.367,54.562,0.542,57.303
            c-5.873,13.104-26.024,20.128-46.9,17.217c1.898,22.182-47.263,25.096-63.076,39.144c-12.201-13.963-66.782-18.074-63.529-39.403
            c-17.44,2.914-37.322,0.26-46.268-17.728c-78.982-0.173-61.27-62.014,1.716-55.761C70.305,53.019,100.036,52.676,112.326,54.133
            L112.326,54.133z"></path>
                            </g>
                        </svg>
                        <h3 class="title">Standard</h3>
                        <div class="price-value">
                            <span class="amount">$10.00</span>
                            <span class="month">per month</span>
                        </div>
                    </div>
                    <ul class="pricing-content">
                        <li><b>50GB</b> Disk Space</li>
                        <li><b>50</b> Email Accounts</li>
                        <li><b>50GB</b> Bandwidth</li>
                        <li><b>10</b> Subdomains</li>
                        <li><b>15</b> Domains</li>
                    </ul>
                    <a href="#" class="pricingTable-signup">Sign Up</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable darkblue">
                    <div class="pricingTable-header">
                        <svg x="0px" y="0px" width="350px" height="230px" viewBox="0 5 350 200">
                            <g>
                                <path fill="#D7182A" d="M58.377,62.185c11.838-16.874,24.94-20.129,43.466-18.93C102.476,24.326,166.999,15.932,175.042,0
                c13.916,18.501,71.028,22.526,72.926,43.769c18.436-3.169,39.31,6.852,42.563,17.131c76.091-0.171,82.236,75.631,0.633,78.715
                c-9.939,13.106-23.496,19.87-43.287,17.988C244.805,176.36,190.766,183.04,174.59,200c-13.285-17.216-71.12-23.04-71.934-42.482
                c-18.795,2.228-33.888-4.284-44.732-18.502C-31.088,134.903-6.688,54.989,58.377,62.185L58.377,62.185z"></path>
                                <path fill="none" stroke="#FFFFFF" stroke-width="0" stroke-linecap="square" stroke-miterlimit="10" d="M58.377,62.185
                c11.838-16.874,24.94-20.129,43.466-18.93C102.476,24.326,166.999,15.932,175.042,0c13.916,18.501,71.028,22.526,72.926,43.769
                c18.436-3.169,39.31,6.852,42.563,17.131c76.091-0.171,82.236,75.631,0.633,78.715c-9.939,13.106-23.496,19.87-43.287,17.988
                C244.805,176.36,190.766,183.04,174.59,200c-13.285-17.216-71.12-23.04-71.934-42.482c-18.795,2.228-33.888-4.284-44.732-18.502
                C-31.088,134.903-6.688,54.989,58.377,62.185L58.377,62.185z"></path>
                                <path fill="none" stroke="#FFFFFF" stroke-width="3.54" d="M112.326,54.133c-4.519-21.67,51.78-25.096,63.167-39.401
            c12.741,16.617,67.686,18.501,61.993,39.829c18.615-4.54,45.183,6.509,46.81,17.216c68.048,0.771,75.367,54.562,0.542,57.303
            c-5.873,13.104-26.024,20.128-46.9,17.217c1.898,22.182-47.263,25.096-63.076,39.144c-12.201-13.963-66.782-18.074-63.529-39.403
            c-17.44,2.914-37.322,0.26-46.268-17.728c-78.982-0.173-61.27-62.014,1.716-55.761C70.305,53.019,100.036,52.676,112.326,54.133
            L112.326,54.133z"></path>
                            </g>
                        </svg>
                        <h3 class="title">Business</h3>
                        <div class="price-value">
                            <span class="amount">$20.00</span>
                            <span class="month">per month</span>
                        </div>
                    </div>
                    <ul class="pricing-content">
                        <li><b>60GB</b> Disk Space</li>
                        <li><b>60</b> Email Accounts</li>
                        <li><b>60GB</b> Bandwidth</li>
                        <li><b>15</b> Subdomains</li>
                        <li><b>20</b> Domains</li>
                    </ul>
                    <a href="#" class="pricingTable-signup">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</div>
