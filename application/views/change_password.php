﻿<div id="regpart1" class="container playGames homePlayGames" style="margin-top:20px;margin-bottom:40px;">
<div class="row">
    <div class="col-lg-12">
		<h1 style="margin:0 0 10px;  border: none;text-align: center;"><span style="padding-bottom:5px;">Change Password </span> </h1>
	</div>
</div>
<div class="row" id="regbanner">
    <div class="col-lg-12">
  <img width="100%" src="<?php echo base_url(); ?>assets/images/home/banner_register.jpg">
 </div>
</div>
</div>
<div class="registrationarea">
<div id="mainContDisp" class="container playGames homePlayGames">
<?php if($response!=''){ ?>
	<h3 style="color:green;text-align: center;padding: 0px 0px 100px;"><?php echo $response;?></h3>
<?php }else{ ?> 
<form name="frmchangepwd" id="frmchangepwd" class="" method="post" enctype="multipart/form-data" accept-charset="utf-8"  >
<div class="row">
    <div class="col-lg-12 txtclr">
	<div style="padding-top:5px;">
      <div class="">
      <span style="float:right;"><label><span style="color:red">*</span> Required fields
	  </label></span>
  </div></div></div>
</div>
<div class="row">
<div class="col-lg-6">
  <div class="form-group">
    <label for="txtOPassword">Password <span style="color:red">*</span></label>
    <input type="password" placeholder="Minimum 8 characters" class="form-control" maxlength="20" name="txtOPassword" value="" id="txtOPassword">
  </div>
</div>
<div class="col-lg-6">
  <div class="form-group">
    <label for="txtCPassword">Confirm Password <span style="color:red">*</span></label>
    <input type="password" placeholder="Minimum 8 characters" class="form-control" maxlength="20" name="txtCPassword" value="" id="txtCPassword">
  </div>
</div>

</div> 
<div style="text-align:center;clear:both;">
   
<div style="padding-bottom:5px;">   
     <label  style="color:red"  class="error" id="errcoupcode"></label>
</div>
<input type="Submit" id="btnRegisterSubmit" style="float:none;" class="btn btn-success" value="Submit">
<input type="reset" id="frmreset" class="btn default">
</div>
</form>
<?php } ?>
</div>
<div style="display:none;" id="iddivLoading" class="loading"></div>
</div> 
 <style>
 .navbar {margin: 20px 0px 0px 0px;}
 #errEmail{color: red;font-size: 15px;float: inherit;margin: 0;}
 .switch-light .btn-primary{background-color:#229a36}
 .switch-light{width: 50%;margin: 0 auto;}
 .well{background-color:rgb(239, 14, 74);color: #fff;}
.txtclr h3{color:#106790;}
#errEmail{display: block;}
.loading {
  position: fixed;
  z-index: 999;
/*   height: 2em;
  width: 2em; */
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: #5a5757 url(<?php echo base_url(); ?>assets/images/ajax-page-loader.gif) center center no-repeat;
  background-size: 5%;
  opacity: 0.6;
}
.homePlayGames {
    border: 1px solid #eeeeee;
    background: #fafafa;
    box-shadow: 10px 10px 5px #888888;
    padding: 20px 15px 30px 25px;
	margin-bottom: 20px;
}
.green{background-color:#5cb85c;color:#fff;font-size:18px;text-align:center;padding:5px;}
.red{background-color:#da6d6d;color:#fff;font-size:18px;text-align:center;padding:5px;}
.registrationarea input[type=text], .registrationarea input[type=password], input[type=password], select {
    height: 34px;
    font-size: 16px;
    width: 100%;
    margin-bottom: 0px !important;
    -webkit-appearance: none;
    /* background: #fff; */
    border: 1px solid #d9d9d9;
    border-top: 1px solid #c0c0c0;
    border-radius: 5px;
    padding: 0 8px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script> 
<script>
$("#frmchangepwd").validate({
		rules: {
			"txtOPassword": {required: true,minlength: 8},
			"txtCPassword": {required: true,equalTo: "#txtOPassword"}
		},
		messages: {
			"txtOPassword": {required: "Please enter password"},
			"txtCPassword": {required: "Please confirm password",equalTo: "Please enter valid confirm password"}
		},
	errorPlacement: function(error, element) {
    if (element.attr("type") === "radio") {
        error.insertAfter(element.parent().parent());
    } 
	else {
        error.insertAfter(element);
    }
}
});
</script>
