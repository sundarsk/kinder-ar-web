<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<title>KinderAngels</title>
<link rel="icon" href="<?php echo base_url(); ?>favicon.ico">
<!-- Bootstrap -->

	<link href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stylenew.css">
<link href="<?php echo base_url(); ?>assets/fonts/Roboto.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome-animation.min.css" rel="stylesheet">
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- Custom styles for this template -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Lobster+Two" rel="stylesheet">

<script type="text/javascript">
    if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent))
        document.write('<script src="<?php echo base_url(); ?>assets/js/sweetalert/bluebird.min.js"><\/script>');
</script>

</head>
<body>
<!--<div class="top-bar" style="background: #fafafa;padding: 15px;">
	<div class="container">                 
	</div>
</div>-->
<button onclick="topFunction()" id="myBtn" title="Go to top"><span class=""></span></button>
<header id="header">
<div class="headerarea">
  <div class="container">
  <div class="col-md-12 col-sm-12 col-xs-12 puzzle-model">
     
      <div class="col-md-3 col-sm-3 col-xs-12 logo" style=""><a title="KinderAngels" href="<?php echo base_url('index.php'); ?>"><img src="<?php echo base_url(); ?>assets/images/banner/KA-Logo.png" width="200" alt="KinderAngels"></a></div>
    <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
	<nav class="navbar">
    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-menu" aria-expanded="false"> 
          <strong>Menu</strong>
          <div class="col-xs-1 pull-right iconbar"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div></button>
    </div>
		
        <div class="navbar-collapse collapse" id="primary-menu" style="float: right;">
          <ul class="nav navbar-nav">
			<?php if($this->router->fetch_method()!='index')
			{ ?>
				<li><a class="loginLink" href="<?php echo base_url('index.php') ?>">Home</a></li>
		<?php } ?>
				
			<?php if(!isset($this->session->parentid) && (!isset($this->session->ChildId)) )
			{ ?>
			<?php if($this->router->fetch_method()!='registration'){ ?>
           <!--<li><a href="#" data-toggle="modal" data-target="#login-modal" class="loginLink">Parent Login & Signup</a></li>-->
            <li><a href="#" data-toggle="modal" data-target="#Child-login-modal" class="loginLink">Login</a></li> 
			
			<?php } ?>
			<?php } else { ?>
				<?php if($this->router->fetch_method()!='registration'){ ?>
				<li><a href="<?php echo base_url('index.php/home/dashboard#View') ?>" class="loginLink" >Dashboard</a></li>
				<?php } ?>
			<?php } ?>
			</ul>
        </div>
      </nav>
	
       
    </div>
  </div>
  </div>
 
</div>
 </header>
<div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
 <div style="display:none;" id="loadingimg" class="loader"></div>
  <div class="modal-dialog">
  <div class="modal-content">
 
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="">Forgot / Change Your Password?</h1>
		  <div class="col-md-12" style="width:100%">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="">
                          <form class="form-horizontal"  role="form" name="frmFP" id="frmFP">
                          <p>If you want to update your password you can reset it here.</p>
                            <div class="panel-body"><div id="msgFP" style="font-size: 18px;"></div>
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control input-lg" placeholder="E-mail Address" name="txtemail" type="text" id="txtemail">
                                    </div>
                                    <input class="btn btn-lg btn-primary btn-block" value="Submit" type="button" id="btnpwd" name="btnpwd">
                                </fieldset>
                            </div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
      </div>
       
       
  </div>
  </div>
</div>
<script>
//testing animation with setTimeout
/* function animation(){
  var element = document.getElementById("ball"),
      time    = Date.now();
  console.log(time);
  element.style.left = (50 + Math.cos(time / 500)*25) + "px";
  element.style.top  = (50 + Math.sin(time / 500)*25) + "px";
  
  setTimeout(animation, 1000 / 30);
}

animation(); */
</script>
<!--<a href="#" data-toggle="modal" data-target="#login-modal">Login</a>-->
<style>#ball{
  width:                150px;
  height:               150px;
  position:             relative;
  left:                 15px;
  top:                  0px;
  background-image: url("https://m1.behance.net/rendition/modules/100887987/disp/c845b52e70de95a5b73b23f254ef1d29.gif");
    background-position: center center;
    background-size: 100%;
  -moz-border-radius:    50%;
  -webkit-border-radius: 50%;
  -o-border-radius:      50%;
  -ms-border-radius:     50%;
}
.reg-inc{    margin-bottom: 12px;}
</style>
<div class="modal fade"  id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div id="loginloadimage" style="display:none;" class="loader"></div>
    <div class="modal-dialog">
	<div class="modal-content">
		<div class="col-md-12 col-sm-12 col-xs-12 loginoverlay">	  	   
			<div class="nopad1 reg-container">
				<div class="reg-inc">
					<h4>New Parent to KinderAngels</h4>
					<a class="" href="<?php echo base_url('index.php/subscription/registration') ?>"><span> Sign up here</span></a>
				</div>
				<div id="ball"></div>
			</div>
			<div class="nopad1 login-container">
				<button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url();?>/assets/images/close.png" /></button>
				<div class="loginmodal-container">
					<h4>Login to Your Account</h4>
					
					<form  class="cmxform form-inline" method="POST" id="form-login" accept-charset="utf-8" > 
					<ul>
					<li>
					<div id="errormsg"></div><div id="errormsg1"></div></li>
					
					<li>
						<label for="email">Email or Username</label>
						<input required="required" type="text" name="email" value="" class="input-small logTxt unametxt" id="email">
					<!--<label for="uLogin" class="input-group-addon glyphicon glyphicon-user"></label>-->
					
					</li>
					<li>
						<label for="pwd">Password</label>
						<input required="required" type="password" name="pwd" value="" class="input-small logTxt pwdtxt" id="pwd">
					<!--<label for="uPassword" class="input-group-addon glyphicon glyphicon-lock"></label>-->
					
					</li>
					<li style="display:none">
					<select style="height:28px" name="ddlLanguage" class="input-small logTxt ddlHLanguage" id="ddlHLanguage">
								<option value="1">English</option>
								</select>
					</li>
					<li class="login-help" style="display:none;"  id="termschkbox">
					<input type="checkbox" value="1" id="termscondition" name="termscondition"><span class="spanRememberme"><a href="<?php echo base_url(); ?>index.php/home/termsofservice" style="color: #92278f; text-decoration: underline;font-weight: bold;" class="termslink" target="_blank">Please Accept Terms and Conditions</a></span>
				  </li>
					<li class="login-help" style="display:none;"><a id="ForgotPassword" href="javascript:;" href="#" data-target="#pwdModal" data-toggle="modal" class="forgotpwd">Forgot / Change Password </a></li>
					<li><input type="button" class="btn btn-primary logSubmit" id="submit" name="submit" value="Login"></li> </ul></form>
					
				 
				  <!--<div class="login-help">
					<label><input type="checkbox" id="remember" name="remember"><span class="spanRememberme">Remember me</span></label>-->
				</div>
			</div>
		</div>
		</div>
	</div>
	</div>
	
	<div class="modal fade"  id="Child-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div id="loginloadimage" style="display:none;" class="loader"></div>
	 <div class="modal-dialog">
	  <div class="modal-content">
		<div class="col-md-12 col-sm-12 col-xs-12 child-loginoverlay">	
			<div class="nopad1 login-container">
				<div class="loginmodal-container">
					<h4>Login to Your Account</h4>
					
					<form  class="cmxform form-inline" method="POST" id="child-form-login" accept-charset="utf-8" > 
					<ul>
					<li>
					<div id="childerrormsg"></div><div id="errormsg1"></div></li>
					
					<li>
						<label for="email">Username</label>
						<input required="required" type="text" name="childemail" value="" class="input-small logTxt unametxt" id="childemail">					
					</li>
					<li>
						<label for="pwd">Password</label>
						<input required="required" type="password" name="childpwd" value="" class="input-small logTxt pwdtxt" id="childpwd">					
					</li>
					<li style="display:none">
					<select style="height:28px" name="childddlLanguage" class="input-small logTxt ddlHLanguage" id="childddlLanguage">
								<option value="1">English</option>
								</select>
					</li>
					<li class="login-help" style="display:none;"  id="termschkbox">
					<input type="checkbox" value="1" id="ctermscondition" name="ctermscondition"><span class="spanRememberme"><a href="<?php echo base_url(); ?>index.php/home/termsofservice" style="color: #92278f; text-decoration: underline;font-weight: bold;" class="termslink" target="_blank">Please Accept Terms and Conditions</a></span>
				  </li>
					<li class="login-help" style="display:none;"><a id="cForgotPassword" href="javascript:;" href="#" data-target="#pwdModal" data-toggle="modal" class="forgotpwd">Forgot / Change Password </a></li>
					<li><input type="button" class="btn btn-primary logSubmit" id="Childsubmit" name="Childsubmit" value="Login"></li> </ul></form>
				</div>
			</div>
			<div class="reg-container">
				<img src="<?php echo base_url(); ?>assets/images/banner/Login-gif.gif" alt="login Popup" width="100%" />
				<div class="gifclass">
					 <button type="button" class="close" data-dismiss="modal"><i class="fa fa-window-close" aria-hidden="true"></i></button>
				</div>
			</div>
 
			
			
		</div>
		</div>
	 </div>
	</div>
</div>
<!-- Sweet Alert -->
<!-- This is what you need -->
<script src="<?php echo base_url(); ?>assets/js/sweetalert/sweetalert2.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert/sweetalert2index.min.css">
<!--.......................-->
<style>
#myModal .modal-dialog  {width:60%;}
#myModal .modal-header{background-color: #f92c8b;}
#myModal .modal-body{padding: 0 15px 15px 15px;}
#myModal h3{color:#fff;}
#myModal .close{opacity:1;color:#fff;}
</style>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#btnpwd').show();
	
$("#ForgotPassword").click(function(){
	$(".close").trigger("click");
	$('#frmFP')[0].reset();
	$("#msgFP").html("");
	$(".error").text("");
	$("#txtemail").attr('readonly', false);
	$('#btnpwd').show();
});
	$("#frmFP").validate({
	rules: {
	"txtemail": {required: true,email:true}
	},
	messages: {
	"txtemail": {required: "Please enter a email address"}
	},
	errorPlacement: function(error, element) {
	error.insertAfter(element);
	},
	highlight: function(input) {
	$(input).addClass('error');
	} 
	});
	$("#btnpwd").click(function(){
		if($("#frmFP").valid())
		{ 
			$(".loader").show();
			$.ajax({
				url: "<?php echo base_url(); ?>index.php/subscription/UserForgetPwdConfirmation", 
				type:'POST',
				data:{email:$("#txtemail").val()},
				success: function(result)
				{
					if(result==1)
					{
						$("#txtemail").attr('readonly', 'readonly');
						$("#msgFP").css("margin-bottom","10px").html("<div style='color:green;font-size: 19px;'>Reset password link has beed send to your mail id.</div>");
						$('#btnpwd').hide();
					}
					else
					{	
						$("#msgFP").css("margin-bottom","10px").html("<div style='color:red'>Enter the valid registered email id</div>");
						$('#btnpwd').show();
					}
					$(".loader").hide();
				}
			});
		}
	});
}); 
</script>
