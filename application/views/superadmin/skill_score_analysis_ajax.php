<div class="row">

<div id="tbldatapart1" class="col-lg-12">
<h3 style="text-align:center">Users Skill Score Less than 40 <?php echo '(Total Users : '.$totalusers1. ')';?></h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Name</th>
				<th>User Name</th>
				<th>Grade</th>
				<th>Section</th>
				<th>School Name</th>
				<th>Skill Score</th>				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($skillscore_report1 as $skillscore)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $skillscore['fname']; ?></td>
				<td><?php echo $skillscore['username']; ?></td>
				<td><?php echo $skillscore['gradename']; ?></td>
				<td><?php echo $skillscore['section']; ?></td>
				<td><?php echo $skillscore['schoolname']; ?></td>
				<td><?php echo round($skillscore['score'], 2); ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 <div id="" class="col-lg-12">
 <h3 style="text-align:center">Users Skill Score Between 40 To 60 <?php echo '(Total Users : '.$totalusers2. ')';?></h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Name</th>
				<th>User Name</th>
				<th>Grade</th>
				<th>Section</th>
				<th>School Name</th>
				<th>Skill Score</th>				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($skillscore_report2 as $skillscore)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $skillscore['fname']; ?></td>
				<td><?php echo $skillscore['username']; ?></td>
				<td><?php echo $skillscore['gradename']; ?></td>
				<td><?php echo $skillscore['section']; ?></td>
				<td><?php echo $skillscore['schoolname']; ?></td>
				<td><?php echo round($skillscore['score'], 2); ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
 
 <div id="" class="col-lg-12">
 <h3 style="text-align:center">Users Skill Score Between 60 To 80 <?php echo '(Total Users : '.$totalusers2. ')';?></h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Name</th>
				<th>User Name</th>
				<th>Grade</th>
				<th>Section</th>
				<th>School Name</th>
				<th>Skill Score</th>				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($skillscore_report3 as $skillscore)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $skillscore['fname']; ?></td>
				<td><?php echo $skillscore['username']; ?></td>
				<td><?php echo $skillscore['gradename']; ?></td>
				<td><?php echo $skillscore['section']; ?></td>
				<td><?php echo $skillscore['schoolname']; ?></td>
				<td><?php echo round($skillscore['score'], 2); ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
 
 <div id="" class="col-lg-12">
 <h3 style="text-align:center">Users Skill Score More than 80 <?php echo '(Total Users : '.$totalusers2. ')';?></h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Name</th>
				<th>User Name</th>
				<th>Grade</th>
				<th>Section</th>
				<th>School Name</th>
				<th>Skill Score</th>				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($skillscore_report4 as $skillscore)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $skillscore['fname']; ?></td>
				<td><?php echo $skillscore['username']; ?></td>
				<td><?php echo $skillscore['gradename']; ?></td>
				<td><?php echo $skillscore['section']; ?></td>
				<td><?php echo $skillscore['schoolname']; ?></td>
				<td><?php echo round($skillscore['score'], 2); ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
</div>

<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('.dataTable').DataTable( );
					</script>