 <header class="head">
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css">

<script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
           Schools Tracking Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
	 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Schools Tracking Report</h5>
            </header>
			<form id="frmtracking" method="POST">
			<div class="row">
			<div class="col-sm-2">
			<label>Schools</label>
			<select  class="form-control input-sm" name="schoolname" id="schoolname">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			
			<div class="col-sm-2">
			<label>Start Date</label>
			<input type="text" class="form-control" value="" name="rangefrom" id="rangefrom" />
			</div>
			<div class="col-sm-2">
			<label>End Date</label>
			<input type="text" class="form-control" value="" name="rangeto" id="rangeto" /></div>
			
			<div class="col-sm-2">
					<input type="button" class="btn sbmtbtn" name="btnreport1" id="report1" Value="Utilization Report">
				</div>
				
				<div class="col-sm-2">
					<input type="button" class="btn sbmtbtn" name="report2" id="report2" Value="Games Played">
				</div>
			
			<div class="col-sm-2">
					<input type="button" class="btn sbmtbtn" name="report3" id="report3" Value="Top Played Games">
				</div>
				
				</div>
				<div class="row">
				<div class="col-sm-2">
					<label>Grade</label>
					<select  class="form-control input-sm" name="gradename" id="ddlgrade"></select>
				</div>
				<div class="col-sm-2">
					<label>Section</label>
					<select  class="form-control input-sm" name="section" id="section"></select>
				</div>
			<div class="col-sm-2">
			<input type="reset" class="btn sbmtbtn" name="report3" id="reset" Value="Reset">
			</div>
			
			</div>
			
			
			
			
			</form>
            <div id="collapse4" class="body">
			<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
			
			<div id="report1_result"></div>

		
			
			
				
				
				
            </div>
        </div>
    </div>
	
	
	
</div>
</div>
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(document).ready(function(){
	
	
	$("#frmtracking").validate({
		rules: {
			schoolname: {required: true},
            rangefrom: {required: true},
			rangeto: {required: true}
			
 
		   },
		   messages: {
			schoolname: {required: "Please choose school"},
			rangefrom: {required: "Please select from date"},
			rangeto:{required: "Please select todate"}
			
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	$('#report1').click(function(){
		
		if($("#frmtracking").valid()==true)
		{
			tracking_report1();
		}
});

$('#report2').click(function(){
		
		if($("#frmtracking").valid()==true)
		{
	tracking_report2();
		}
});

$('#report3').click(function(){
		
		if($("#frmtracking").valid()==true)
		{
	tracking_report3();
		}
});
	
});
$('#ddlgrade').change(function(){
	var schoolid = 	$('#schoolname').val();
	var ddlgrade = $(this).val();
	ajaxsectionload(schoolid,ddlgrade);
	
});
function ajaxsectionload(schoolid,ddlgrade)
{
	//alert('hai');
var schoolid = 	$('#schoolname').val();
var ddlgrade = $('#ddlgrade').val();

$.ajax({
	 type: "POST",
		 url: "<?php echo base_url(); ?>index.php/superadmin/ajaxgetsection",
		data:{schoolid:schoolid,ddlgrade:ddlgrade},
		success: function(result)
		{
	//	alert(result);
		$('#section').html(result);
		}
	});
}



function tracking_report1()
{
	
	var schoolid = $('#schoolname').val();
	var startdate = $('#rangefrom').val();
	var enddate = $('#rangeto').val();
	var grade = $('#ddlgrade').val();
	var section = $('#section').val();

$("#iddivLoading").show();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/superadmin/tracking_report1",
    data: {schoolid:schoolid,startdate:startdate,enddate:enddate,grade:grade,section:section},
    
    success: function(result){
		//alert(result);	
$("#iddivLoading").hide();		 
		
		 $('#report1_result').html(result);
		
    }
});
}	

function tracking_report2()
{

var schoolid = $('#schoolname').val();
var startdate = $('#rangefrom').val();
var enddate = $('#rangeto').val();

$("#iddivLoading").show();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/superadmin/tracking_report2",
    data: {schoolid:schoolid,startdate:startdate,enddate:enddate},
    
    success: function(result){
		// alert(result);	
$("#iddivLoading").hide();		 
		
		 $('#report1_result').html(result);
		
    }
});
}

function tracking_report3()
{
	var schoolid = $('#schoolname').val();
	var startdate = $('#rangefrom').val();
	var enddate = $('#rangeto').val();
	var grade = $('#ddlgrade').val();
	var section = $('#section').val();
$("#iddivLoading").show();
$.ajax({
    type: "POST",
	data: {schoolid:schoolid,startdate:startdate,enddate:enddate,grade:grade,section:section},
    url: "<?php echo base_url(); ?>index.php/superadmin/tracking_report3",   
    success: function(result){
			$("#iddivLoading").hide();			
			$('#report1_result').html(result);
		
    }
});
}

$('#schoolname').change(function(){
	
	var sid = $(this).val();
	
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/superadmin/getschoolgrade",
    data: {sid:sid},
    success: function(result){
		// alert(result);	
$("#iddivLoading").hide();		 
		
		 $('#ddlgrade').html(result);
		
    }
});
	
	
	
});

$( "#rangefrom" ).datepicker(

        { 
            maxDate: '0', 
            beforeShow : function()
            {
                jQuery( this ).datepicker('option','maxDate', jQuery('#rangefrom').val() );
            },
            altFormat: "yy-mm-dd", 
            dateFormat: 'yy-mm-dd'

        }

);

$( "#rangeto" ).datepicker( 

        {
            maxDate: '0', 
            beforeShow : function()
            {
                jQuery( this ).datepicker('option','minDate', jQuery('#rangeto').val() );
            } , 
            altFormat: "yy-mm-dd", 
            dateFormat: 'yy-mm-dd'

        }

);

		 
</script>
<style>  </style>
<style>

 .sbmtbtn{padding: 10px 5px;margin: 9px auto;background: lightgreen;}
 
 #reset{padding: 8px 25px;margin: 14px auto;background: indianred;}

.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>	