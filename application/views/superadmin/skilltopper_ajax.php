<?php  $avgscore=0;$i=1; foreach($skilltopper as $skill) 
					{   
					$avgscore+=round($skill['finalscore'], 2);
					$i++; } 
					$avgscore=round($avgscore/($i-1), 2);
					?>
		<div style="text-align: center;font-weight: bold;"> Average Score : <?php echo $avgscore; ?> </div>			
 <table class="table table-bordered responsive-table" id="dataTables">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>School Name</th>
                            <th>Avg. Score</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php $i=1; foreach($skilltopper as $skill) 
					{  ?>
                        <tr class="<?php   if($avgscore>round($skill['finalscore'], 2)){echo 'danger'; $diffpercent='<span class="percent down pull-right"> <i class="fa fa-caret-down"></i> '.(round($skill['finalscore'], 2)-$avgscore).' %</span>'; } else if($avgscore==round($skill['finalscore'], 2)) { $diffpercent='<span class="percent pull-right"> <i class="fa fa-caret"></i> '.(round($skill['finalscore'], 2)-$avgscore).' %</span>'; } else{ echo 'success'; $diffpercent='<span class="percent up pull-right"> <i class="fa fa-caret-up"></i> '.(round($skill['finalscore'], 2)-$avgscore).' %</span>';} ?>">
                            <td><?php echo $i; ?></td>                   
                            <td><?php echo $skill['school_name']; ?></td>
                            <td><?php echo round($skill['finalscore'], 2).$diffpercent; ?></td>
                        </tr>
					<?php $i++; } ?>
                    </tbody>                </table>
					
					<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTables').DataTable( );
					</script>