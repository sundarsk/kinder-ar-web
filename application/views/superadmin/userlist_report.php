 <header class="head">
                               
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Userlist
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
			
 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>User List - Report</h5>
            </header>
			<form id="srchreport" method="POST">
			<div class="row">
			<div class="col-sm-2">
			<label>coupon code</label>
			<input type="text" class="form-control" value="" maxlength="12" name="couponcode" id="couponcode" />
			</div>
			
			<div class="col-sm-3">
			<label>Registration status</label>
			<select  class="form-control input-sm" name="rstatus" id="rstatus" >
			<option value="">Select</option>
			<option value="N">Registered Alone</option>
			<option value="C">Fully Registered</option>
			</select>
			</div>
			
			<div class="col-sm-2">
			<label>Registration From Date</label>
			<input type="text" class="form-control" value="" name="rfromdate" id="rfromdate" />
			</div>
			<div class="col-sm-2">
			<label>Registration To Date</label>
			<input type="text" class="form-control" value="" name="rtodate" id="rtodate" /></div>
			<div class="col-sm-2">
			<input type="button" name="srchbtn" id="srchbtn" Value="Search">
			<input type="reset" name="reset" id="reset" Value="Reset">
			</div>
			</div>
			
			</form>
			 
            <div id="collapse4" class="body">
                <div id="users"></div>
				<div class="row" id='exportbtn' style='display:none;'>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<link href = "<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>

	
$(document).ready(function(){
	
	users();
	 /*$("#srchreport").validate({
		rules: {
			couponcode: {required: true},
            rstatus: {required: true},
			rfromdate: {required: true},
            rtodate: {required: true}
 
		   },
		   messages: {
			couponcode: {required: "Please enter coupon code"},
			rstatus: {required: "Please choose any one"},
			rfromdate: {required: "Please choose from date"},
			rstatus: {required: "Please choose to date"}
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	}); */
	
	
	
	
});


	//var couponcode = $('#couponcode').val();
	//var rstatus = $('#rstatus').val();
	//var rfrmdate = $('#rfromdate').val();
	//var rtodate = $('#rtodate').val();

$('#btnExcelExport').click(function(){
		
	users_excel();
});

$('#reset').click(function(){
		
	location.reload(true);
});

$("#srchbtn").click(function(){
	
	$("#srchreport").valid()
{
	users();
}
	
});


function users()
{
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/usersreport",
    data: {couponcode:$('#couponcode').val(),rstatus:$('#rstatus').val(),rfrmdate:$('#rfromdate').val(),rtodate:$('#rtodate').val()},
    success: function(result){
		//alert(result);	 
		  if(result!='')
		 {
			 $('#exportbtn').show();
		 }
		 $('#users').html(result);
		
    }
});
}

function users_excel()
{
	
	
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/usersreport_excel",
    data: {couponcode:$('#couponcode').val(),rstatus:$('#rstatus').val(),rfrmdate:$('#rfromdate').val(),rtodate:$('#rtodate').val()},
    success: function(result){
		alert(result);	 
		  window.location.href= "<?php echo base_url();?>"+result;
		
    }
});
}

dob = $( "#rfromdate" )
        .datepicker({
			 dateFormat: 'dd-mm-yy' ,maxDate: "31-12-2017" ,minDate: "01-01-2017" ,yearRange: "2017:2017",
			 onSelect: function(value, ui) {
        var today = new Date(),
            dob = new Date(value),
            age = new Date(today - dob).getFullYear() - 1970;

       // $('#txtAge').val(age);
    },
	
          changeMonth: true, changeYear: true
        });	
		
		dob1 = $( "#rtodate" )
		 .datepicker({
			 dateFormat: 'dd-mm-yy' ,maxDate: "31-12-2017" ,minDate: "01-01-2017" ,yearRange: "2017:2017",
			 onSelect: function(value, ui) {
        var today = new Date(),
            dob1 = new Date(value),
            age = new Date(today - dob1).getFullYear() - 1970;

       // $('#txtAge').val(age);
    },
	
          changeMonth: true, changeYear: true
        });	
		 
</script>