<header class="head">
</header>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
</style>
<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
<div class="col-lg-12">
<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
 <style>
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>

 
</div>
</div>

<div class="row">

<div id="" class="col-lg-6">
<h3>Game list - Least number of times played</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>No. of times played</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($LowestPlaygames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gamelist']); ?></td>
				<td><?php echo $gamelist['gcount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
 <div id="" class="col-lg-6">
 <h3>Game list - Highest number of times played</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>No. of times played</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($HighestPlaygames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gamelist']); ?></td>
				<td><?php echo $gamelist['gcount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
 
</div>


<hr/>
<div class="row">

<div id="" class="col-lg-6">
 <h3>Game list - Avg., score less than 25</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>Grade</th>
				<th>Skill</th>
				<th>Avg. Score</th>
				<th>No. of times played</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($LowestScoreGames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gname']); ?></td>
				<td><?php echo $gamelist['grade']; ?></td>
				<td><?php echo $gamelist['skill']; ?></td>
				<td><?php echo $gamelist['gscore']; ?></td>
				<td><?php echo $gamelist['playedcount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
 <div id="" class="col-lg-6">
  <h3>Game list - Avg., score more than 75</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>Grade</th>
				<th>Skill</th>
				<th>Avg. Score</th>
				 <th>No. of times played</th>
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($HighestScoreGames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gname']); ?></td>
				<td><?php echo $gamelist['grade']; ?></td>
				<td><?php echo $gamelist['skill']; ?></td>
				<td><?php echo $gamelist['gscore']; ?></td>
				 <td><?php echo $gamelist['playedcount']; ?></td>
			</tr>
			
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 
 
</div>



</div>
</div>
</div>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('.dataTable').DataTable( );
					</script>
					