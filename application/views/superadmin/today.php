<header class="head">
</header>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>

<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
<div class="col-lg-12" style="overflow:scroll">
<div class="text-center">
<h2>Current Day Session Details : <?php echo date('d-m-Y'); ?></h2>
</div>

<?php

$compusers[$completeusers['sid']][$completeusers['grade_id']][$completeusers['section']]= $completeusers['cuser'];
		
		
		foreach($schools as $key1=>$val1) {
			
			$scklschedule[$schools[$key1]['school_id'].'-'.$schools[$key1]['gradeid'].'-'.$schools[$key1]['section'] ] =  $val1;
	
		}
		
		
		foreach($completeusers as $key2=>$val2) {
			
			$compusers[$completeusers[$key2]['sid'].'-'.$completeusers[$key2]['grade_id'].'-'.$completeusers[$key2]['section'] ] =  $val2;

		}
 ?>
 
 <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped dataTable">
                    <thead>
                    <tr>
					
						<th>S.no</th>
                        <th>Period</th>
						<th>Time</th>
						<th>Schoolname</th>
						<th>Grade</th>
						<th>Section</th>
						<th>Registered Users</th>
						<th>Logged in User</th>
						<th>Attended Users</th>
						<th>Completed Users</th>
						<th>95% Attendance</th>
						<th>90% Completion</th>
						 </tr></thead>
		<tbody>
		<?php $i=1;
		foreach($scklschedule as $key3=>$val3) {
			
			$scklschedule[$key3]['cccount'] =  $compusers[$key3]['cuser'];
			?>
			
			
			 <tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $val3['period']; ?></td>
							<td><?php if($val3['start_time']==''){ echo '-'; } else { echo date("g:i A", strtotime($val3['start_time'])).' - '.date("g:i A", strtotime($val3['end_time'])); } ?><br/><?php if($val3['remarks']!=''){ echo $remarks = '<b>['.$val3['remarks'].']</b>'; } else { echo $remarks=''; }?></td>
							<td><?php echo $val3['schoolname']; ?></td>
							<td><?php echo $val3['grade']; ?></td>
							<td><?php echo $val3['section']; ?></td>
							<td><?php echo $val3['regusers']; ?></td>
							<td><?php echo $val3['loginuser']; ?></td>
							<td><?php echo $val3['attendusers']; ?></td>
							<td><?php if($compusers[$key3]['cuser']=='') { echo 0; } else { echo $compusers[$key3]['cuser']; } ?></td>
							
							<td><?php $percent = $val3['attendusers']/$val3['regusers'];   $perc = number_format( $percent * 100, 2 );    if($perc>=95.00) { echo 'YES'; } else {echo 'NO'; }; ?></td>
							
							
							<td><?php $percent = $compusers[$key3]['cuser']/$val3['attendusers'];   $perc = number_format( $percent * 100, 2 );    if($perc>=90.00) { echo 'YES'; } else {echo 'NO'; }; ?></td>
							
				</tr>			
			
			
	<?php $i++;	} ?>
	
	</tbody>                
					</table>

</div>
</div>











</div>
</div>
</div>


<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('.dataTable').DataTable( );
					</script>
					