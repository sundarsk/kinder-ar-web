<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
					<th>s.no</th>
                        <th>Name</th>
						 <th>EmailID</th>
						 <th>Date of Register</th>
						 <th>Date of Activation</th>
						 <th>Date of Expiration</th>
						 <th>Coupon code - (%)</th>
						 <th>Original Amount</th>
						 <th>Total Paid Amount</th>
						  <th>Payment Status</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php  $validdays = $configdetails[0]['value']; $i=1; foreach($users as $us) { ?>
                            <tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $us['firstname']; ?></td>
							  <td><?php echo $us['email']; ?></td>
							   <td><?php echo date('d-m-Y', strtotime($us['createddate'])); ?></td>
							   <td><?php if ($us['completeddate'] == '0000-00-00 00:00:00'){ echo '-'; } else { echo date('d-m-Y', strtotime($us['completeddate'])); } ?></td>
							   <td><?php  if ($us['completeddate'] == '0000-00-00 00:00:00'){ echo '-'; } else { echo date('d-m-Y', strtotime($us['completeddate']. ' + '.$validdays.' days')); } ?></td>
							   <td><?php if($us['couponcode']!='') { echo $us['couponcode']; ?> - <?php echo $us['percentage']; } ?></td>
							   <td><?php echo round($us['originalamount'], 2); ?></td>
							   <td><?php echo round($us['paidamount'], 2); ?></td>
							   <td><?php echo $us['paymentstatus']; ?></td>
							   </tr>
					<?php $i++; }  ?>
								 
                    </tbody>                
					</table>
<link href="<?php echo base_url(); ?>assets/admin/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/admin/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>