 <header class="head">
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css">

<script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
           Schools Tracking Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
	 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Schools Tracking Report</h5>
            </header>
			<form id="frmtracking" method="POST" action="http://localhost/project/php/htmldevschool/live_v5/index.php/superadmin/DownloadPDF">
			<div class="row">
			<div class="col-sm-2">
			<label>Schools</label>
			<select  class="form-control input-sm" name="schoolname" id="schoolname">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			<div class="col-sm-2">
				<label>Month</label>
					<select  class="form-control input-sm" name="month" id="month">
					<option value="">Select</option>
					</select>
			</div>

			
<div class="col-sm-2"><input type="button" class="btn sbmtbtn" name="btnreport1" id="report1" Value="Utilization Report"></div>
<div style="display:none;" class="col-sm-2"><input type="button" class="btn sbmtbtn" name="btnreport2" id="report6" Value="Utilization Non-Schedule days"></div>
<div class="col-sm-2"><input type="button" class="btn sbmtbtn" name="report2" id="report2" Value="BSPI"></div>
<div class="col-sm-2"><input type="button" class="btn sbmtbtn" name="report3" id="report3" Value="Toppers List"></div>
<div class="col-sm-2"><input type="button" class="btn sbmtbtn" name="report4" id="report4" Value="Intervention"></div>

<div class="col-sm-2"><input type="button" class="btn sbmtbtn" name="report5" id="report5" Value="Intervention Lower Attempts"></div>

<div class="row">
	<div class="col-sm-6">
		<label>Report Message</label>
		<textarea id="message" name="message" style="width:100%"></textarea>
		<div class="validerr" ></div>
	</div>
	<div class="col-sm-2">
		<input type="button" class="btn sbmtbtn" value="Update" name="btnupdate" id="btnupdate" >
	</div>
</div>

<input type="hidden" value="" id="reportcode" class="" />

<!--<div class="col-sm-2"><input type="submit" class="btn sbmtbtn" name="DownloadPDF" id="DownloadPDF" Value="Download PDF"></div>-->
				</div>
			</form>
            <div id="collapse4" class="body">
				<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
				<div id="report1_result"></div>				
            </div>
        </div>
    </div>
	
	
	
</div>
</div>
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(document).ready(function(){
	
	
	$("#frmtracking").validate({
		rules: {
			schoolname: {required: true},
            month: {required: true},
		   },
		   messages: {
			schoolname: {required: "Please choose school"},
			month: {required: "Please select month"},
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	$('#report1').click(function(){
		$("#reportcode").val('UREPORT');
		if($("#frmtracking").valid()==true)
		{
			tracking_report1();GetReportMessage('UREPORT');
		}
});

$('#report2').click(function(){
		$("#reportcode").val('BSPIREPORT');
		if($("#frmtracking").valid()==true)
		{
			tracking_report2();GetReportMessage('BSPIREPORT');
		}
});

$('#report3').click(function(){
		$("#reportcode").val('TOPPERSREPORT');
		if($("#frmtracking").valid()==true)
		{
			tracking_report3();GetReportMessage('TOPPERSREPORT');
		}
});
$('#report4').click(function(){
		$("#reportcode").val('INTERVENTION');
		if($("#frmtracking").valid()==true)
		{
			tracking_report4();GetReportMessage('INTERVENTION');
		}
});
$('#report5').click(function(){
		$("#reportcode").val('ILOWERATTEMPT');
		if($("#frmtracking").valid()==true)
		{
			tracking_report5();GetReportMessage('ILOWERATTEMPT');
		}
});

$('#report6').click(function(){
		$("#reportcode").val('UREPORT');
		if($("#frmtracking").valid()==true)
		{
			tracking_report6();GetReportMessage('UREPORT');
		}
});

$('#btnupdate').click(function(){
	if($("#frmtracking").valid()==true)
	{
		if($("#reportcode").val()!='')
		{
			if($("#message").val()!='')
			{
				UpdateReport();
			}
			else
			{
				$(".validerr").html("<span class='error'>Please enter message</span>");
			}
		}
		else
		{
			$(".validerr").html("<span class='error'>Please search anyone report</span>");
		}
	}
});

$('#DownloadPDF').click(function(){
		if($("#frmtracking").valid()==true)
		{
			DownloadPDF();
		}
});
	
});
$('#ddlgrade').change(function(){
	var schoolid = 	$('#schoolname').val();
	var ddlgrade = $(this).val();
	ajaxsectionload(schoolid,ddlgrade);
	
});
function ajaxsectionload(schoolid,ddlgrade)
{
	//alert('hai');
var schoolid = 	$('#schoolname').val();
var ddlgrade = $('#ddlgrade').val();

$.ajax({
	 type: "POST",
		 url: "<?php echo base_url(); ?>index.php/superadmin/ajaxgetsection",
		data:{schoolid:schoolid,ddlgrade:ddlgrade},
		success: function(result)
		{
	//	alert(result);
		$('#section').html(result);
		}
	});
}

function UpdateReport()
{
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();
	var reportcode=$('#reportcode').val();
	var message=$('#message').val();
	$("#iddivLoading").show();
	$.ajax({
		type: "POST",
		//dataType: "json",
		url: "<?php echo base_url(); ?>index.php/superadmin/eom_reportupdate",
		data: {schoolid:schoolid,month:month,code:reportcode,message:message},
		success: function(result){//alert(result);	
				$("#iddivLoading").hide();		 
		}
	});
}
function GetReportMessage(rcode)
{
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();	
	$("#iddivLoading").show();
	$.ajax({
		type: "POST",
		//dataType: "json",
		url: "<?php echo base_url(); ?>index.php/superadmin/eom_getreportmessage",
		data: {schoolid:schoolid,month:month,code:rcode},
		success: function(result){//alert(result);	
				$("#iddivLoading").hide();		 
				$("#reportcode").val(rcode);
				$("#message").val(result);
		}
	});
}

function tracking_report6()
{
	
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();//alert(month);
	

$("#iddivLoading").show();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/superadmin/eom_utilizationreport_nonschedule",
    data: {schoolid:schoolid,month:month},
    success: function(result){//alert(result);	
			$("#iddivLoading").hide();		 
			$('#report1_result').html(result);//alert($("#month option:selected").text());
			$("#monname").html($("#month option:selected").text());
    }
});
}

function tracking_report1()
{
	
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();//alert(month);
	/* var startdate = $('#rangefrom').val();
	var enddate = $('#rangeto').val();
	var grade = $('#ddlgrade').val();
	var section = $('#section').val(); */

$("#iddivLoading").show();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/superadmin/eom_utilizationreport",
    data: {schoolid:schoolid,month:month},
    success: function(result){//alert(result);	
			$("#iddivLoading").hide();		 
			$('#report1_result').html(result);//alert($("#month option:selected").text());
			$("#monname").html($("#month option:selected").text());
    }
});
}	

function tracking_report2()
{

var schoolid = $('#schoolname').val();
var month=$('#month').val();

$("#iddivLoading").show();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/superadmin/eom_monthwisebspireport",
    data: {schoolid:schoolid,month:month},
    success: function(result){
// alert(result);	
$("#iddivLoading").hide();		 
$('#report1_result').html(result);
}
});
}

function tracking_report3()
{
var schoolid = $('#schoolname').val();
var month=$('#month').val();
$("#iddivLoading").show();
$.ajax({
    type: "POST",
	data: {schoolid:schoolid,month:month},
    url: "<?php echo base_url(); ?>index.php/superadmin/eom_topperslist",   
    success: function(result){
			$("#iddivLoading").hide();			
			$('#report1_result').html(result);
		
    }
});
}
function tracking_report4()
{
var schoolid = $('#schoolname').val();
var month=$('#month').val();
$("#iddivLoading").show();
$.ajax({
    type: "POST",
	data: {schoolid:schoolid,month:month},
    url: "<?php echo base_url(); ?>index.php/superadmin/eom_intervention",   
    success: function(result){
			$("#iddivLoading").hide();			
			$('#report1_result').html(result);
		
    }
});
}
function tracking_report5()
{
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();
	$("#iddivLoading").show();
	$.ajax({
		type: "POST",
		data: {schoolid:schoolid,month:month},
		url: "<?php echo base_url(); ?>index.php/superadmin/eom_userattendedcount",   
		success: function(result){
				$("#iddivLoading").hide();			
				$('#report1_result').html(result);
			
		}
	});
}
function DownloadPDF()
{
var schoolid = $('#schoolname').val();
var month=$('#month').val();
$("#iddivLoading").show();
$.ajax({
    type: "POST",
	data: {schoolid:schoolid,month:month},
    url: "<?php echo base_url(); ?>index.php/superadmin/DownloadPDF",   
    success: function(result){
			$("#iddivLoading").hide();			
			$('#report1_result').html(result);
		
    }
});
}

$('#schoolname').change(function(){
	var sid = $(this).val();
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/superadmin/getacademicmonth",
    data: {sid:sid},
    success: function(result){// alert(result);	
		$("#iddivLoading").hide();
		$('#month').html(result);
    }
});
	
	
	
});

$( "#rangefrom" ).datepicker(

        { 
            maxDate: '0', 
            beforeShow : function()
            {
                jQuery( this ).datepicker('option','maxDate', jQuery('#rangefrom').val() );
            },
            altFormat: "yy-mm-dd", 
            dateFormat: 'yy-mm-dd'

        }

);

$( "#rangeto" ).datepicker( 

        {
            maxDate: '0', 
            beforeShow : function()
            {
                jQuery( this ).datepicker('option','minDate', jQuery('#rangeto').val() );
            } , 
            altFormat: "yy-mm-dd", 
            dateFormat: 'yy-mm-dd'

        }

);

		 
</script>
<style>  </style>
<style>

 .sbmtbtn{padding: 10px 5px;margin: 9px auto;background: lightgreen;}
 
 #reset{padding: 8px 25px;margin: 14px auto;background: indianred;}

.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>	