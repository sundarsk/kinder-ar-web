 <header class="head">
                               
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Schoolwise - Gradewise BSPI
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            
<div class="row">
<div class="col-lg-12">

<div class="col-lg-12">
        <div class="box">
            <header>
                <h5>Schoolwise - Gradewise BSPI Average</h5>
            </header>
			<div class="body">
                <form id="srchresult" method="post" class="form-inline">
                    <div class="row form-group">
                        <div class="col-lg-4">
						<label class="control-label col-lg-6">Schools</label>
                           <select class="form-control col-lg-6" required="true" multiple="multiple" name="ddlschool" id="ddlschool">
<?php  
foreach($getschools_m as $schools) {
				$schoolsid='';
				$selected='';
				//if($schoolid==$schools['id']){$selected="selected='selected'";$schoolsid=$schools['id'];}
				echo "<option id='".$schools['id']."'  value='".$schools['schoolid']."'>".trim(str_replace("",'',$schools['schoolname']))."</option>";
			} ?>
						   </select>
<input type="hidden" value="" name="hdnschoolid" id="hdnschoolid" />
                        </div>
						 <div class="col-lg-4">
						<label class="control-label col-lg-6">Grade</label>
                            <select class="form-control col-lg-6" required="true" multiple="multiple" style="width:50%" name="ddlgrade" id="ddlgrade">
			<?php foreach($getgrades_m as $grades) {
				$gradinid='';
				$selected='';
				//if($depid==$grades['gradeid']){$selected="selected='selected'";$gradinid=$grades['gradeid'];}
				echo "<option id='".$grades['gradeid']."' value='".$grades['gradeid']."'>".trim(str_replace("",'',$grades['gradename']))."</option>";
			} ?>
			</select>
			<input type="hidden" value="" name="hdngradeID" id="hdngradeID" />
                        </div>
                        
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-6">
                          <input class="btn btn-success" id="btnSearch" name="btnSearch" value="Search" type="button">
                        </div>              
                    </div>
					
					<div class="row form-group">
                        <div class="col-lg-6">
                          <input class="btn btn-danger" id="reset" name="reset" value="Reset" type="reset">
                        </div>              
                    </div>
                </form>
            </div>
            <div id="borderedTable" class="body collapse in">
			<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
			<div id="schoolgradebspilist"></div>
            </div>
        </div>
    </div>
</div>
<!-- Assessment -->
</div>
 </div>
 </div>
</div>
</div>
			
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-multiselect.js"></script>

<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
           /* background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;*/
         }
		.stats_box li{margin:0 !important}
      </style>
	  <style>
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
			
<script src = "<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script type="text/javascript">
$('#reset').click(function()
{
	location.reload();
});
$(function() {
			            	        $('#ddlschool').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#ddlschool option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });
		$("#hdnschoolid").val(selected);
		
										}
			            	        });
									
			            	        $('#btnget').click(function() {
			            	       // alert($('#chkveg').val());
			            	        })
			            	    });
								
$(function() {
			            	        $('#ddlgrade').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#ddlgrade option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });
		$("#hdngradeID").val(selected);
		
										}
			            	        });
									
			            	        $('#btnget').click(function() {
			            	       // alert($('#chkveg').val());
			            	        })
			            	    });
								
$(document).ready(function()
{
school_grade_bspi('','');
	
	setInterval(function(){school_grade_bspi('','');}, 1000*3*60); 
	
	$('#ddlschool').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdnschoolid; ?>";
				var dataarray=data.split(",");
				$("#ddlschool").val(dataarray);
				$("#hdnschoolid").val(dataarray);
				$("#ddlschool").multiselect("refresh");
				<?php } ?>
				
				$('#ddlgrade').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdngradeID; ?>";
				var dataarray=data.split(",");
				$("#ddlgrade").val(dataarray);
				$("#hdngradeID").val(dataarray);
				$("#ddlgrade").multiselect("refresh");
				<?php } ?>
	
	
});

 $('#btnSearch').click(function(){
	var schoolid = $('#hdnschoolid').val();
	var gradeid = $('#hdngradeID').val();
	school_grade_bspi(schoolid,gradeid);
	
});	

function school_grade_bspi(schoolid,gradeid)
{	
$("#iddivLoading").show();
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/schoolwise_gradewise_bspi_avg",
    data:{schoolid:schoolid,gradeid:gradeid},
    success: function(result){
		//alert(result);
		$("#iddivLoading").hide();
		 $('#schoolgradebspilist').html(result);	 
    }
});
}
</script>
