<?php 
 foreach($eom_part2 as $key1=>$val1) {
	$query1[$eom_part2[$key1]['weekval'].'-'.$eom_part2[$key1]['gradeid'].'-'.$eom_part2[$key1]['section']] =  $val1;
}	
 foreach($eom_part_attenduser2 as $key1=>$val5) {
	$query3[$eom_part_attenduser2[$key1]['weekval'].'-'.$eom_part_attenduser2[$key1]['gradeid'].'-'.$eom_part_attenduser2[$key1]['section']] =  $val5;
}
foreach($eom_part_cuser2 as $key2=>$val2) {
	$query2[$eom_part_cuser2[$key2]['weekval'].'-'.$eom_part_cuser2[$key2]['gradeid'].'-'.$eom_part_cuser2[$key2]['section']] =  $val2;
}

foreach($weekofday as $key3=>$val3)
{ //echo $weekofday[$key3]['startdate'];exit;
	$qry[$weekofday[$key3]['weekval']]=$weekofday[$key3]['startdate']." - ".$weekofday[$key3]['enddate'];
}

foreach($eom_part_attenduser1 as $key7=>$val3)
{
	$exeattenduser[$eom_part_attenduser1[$key7]['weekval']]=$eom_part_attenduser1[$key7]['attenusers'];
}
foreach($eom_part_cuser1 as $key8=>$val3)
{
	$execompleteduser[$eom_part_cuser1[$key8]['weekval']]=$eom_part_cuser1[$key8]['completeduser'];
}

/* foreach($eom_part_timetaken as $key11=>$val1) {
	$query5[$eom_part_timetaken[$key11]['weekval'].'-'.$eom_part_timetaken[$key11]['gradeid'].'-'.$eom_part_timetaken[$key11]['section']] =  $val1;
} */

//echo "<pre>";print_r($qry);exit;

//echo "<pre>";print_r($query3);exit;
//echo "<pre>";print_r($query2);exit;
 
?>
<?php
//echo "<pre>";print_r($regattenduser);exit; 
$percent = $regattenduser[0]['attenusers']/$regattenduser[0]['totaluser'];
$perc1 = round( $percent * 100, 2 );
$percent1 = $completeduser[0]['completeduser'] /$regattenduser[0]['totaluser'];
$perc2 = round( $percent1 * 100, 2 ); 
?>
<div class="col-md-12 col-sm-12 col-xs-12">						
	<div class="col-md-4 col-sm-4 col-xs-12 countdata"><label class="text-center">Expected User Utilization </label><span class="countval"><?php echo $regattenduser[0]['totaluser']; ?></span></div>
	<div class="col-md-4 col-sm-4 col-xs-12 countdata newcolor1"><label class="text-center">Attended User <span class="low">(% of Training Utilized (Attended/Expected)*100)</span></label><span class="countval"><?php echo $regattenduser[0]['attenusers'];?> <span class="cent"><?php echo '('.$perc1.' %)'; ?></span></span></div>
	<div class="col-md-4 col-sm-4 col-xs-12 countdata newcolor2"><label class="text-center">Completed User <span class="low">(% of Training Utilized (Completed/Expected)*100)</span></label><span class="countval"><?php echo $completeduser[0]['completeduser'];?> <span class="cent"><?php echo '('.$perc2.' %)'; ?></span></span></div>
</div>
<br/>
<h2 style="text-align:center;">Summary of utilization for the month of - <span id="monname"></span></h2>
<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
						<tr>
							<th>S.No.</th>
							<th>Session</th>
							 <th>Totalusers</th>
							 <th>Attended</th>
							<th>Completed</th>						
						</tr>
                    </thead>
<tbody>
<?php $i=1;$k=0;
foreach($eom_part1 as $row) {  // echo $query2[$key3]['cuser']; 
?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $row['weekval']." (".$qry[$row['weekval']].")"; ?></td>
			<td><?php echo $row['totalusers']; ?></td>
			<td><?php if($exeattenduser[$row['weekval']]!=''){echo $exeattenduser[$row['weekval']];}else{echo "0";} ?></td>
			<td><?php if($execompleteduser[$row['weekval']]!=''){echo $execompleteduser[$row['weekval']];}else{echo "0";} ?></td>
		</tr>
<?php $i++;	$k++;} ?>	
</tbody>                
</table>

<h2 style="text-align:center;">Detailed utilization report</h2>
<table id="dataTable1" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
						<tr>
							<th>S.No.</th>
							<th>Session</th>
							<th>Grade</th>
							<th>Section</th>
							 <th>Totalusers</th>
							 <th>Total Attended</th>
							<th>Completed Users</th>
							<!--<th>Avg Time Taken</th>-->
							<th>% of Training Utilized (Attended/Total)*100</th>
							<th>% of Training Utilized (Completed/Total)*100</th>
							<th>Avg Bspi </th>
						</tr>
                    </thead>
<tbody>
<?php $j=1;
foreach($query1 as $key3=>$val3) {/*  echo "<pre>";print_r($val3);exit;  echo "<pre>";print_r($val3);exit;  */?>
		<tr>
			<td><?php echo $j; ?></td>
			<td><?php echo $val3['weekval']." (".$qry[$val3['weekval']].")"; ?></td>
			<td><?php echo $val3['gradename']; ?></td>
			<td><?php echo $val3['section']; ?></td>
			<td><?php echo $val3['totalusers']; ?></td>
			<td><?php  if($query3[$key3]['attenusers'] ==''){echo $query3[$key3]['attenusers'] =0;} else{ echo $query3[$key3]['attenusers'];} ?></td>
			<!--<td><?php //echo $val3['attenusers']; ?></td>-->
			<td><?php  if($query2[$key3]['completeduser'] ==''){echo $query2[$key3]['completeduser'] =0;} else{ echo $query2[$key3]['completeduser'];} ?></td>
			
			<!--<td><?php  if($query5[$key3]['avgtimetaken'] ==''){echo $query5[$key3]['avgtimetaken'] =0;} else{ echo $query5[$key3]['avgtimetaken'];} ?></td>-->
			
			<td><?php $percent = $query3[$key3]['attenusers']/$val3['totalusers'];   echo $perc = round( $percent * 100, 2 );  ?></td>
			<td><?php $percent1 = $query2[$key3]['completeduser'] /$val3['totalusers']; echo $perc = round( $percent1 * 100, 2 );  ?></td>
			<td><?php  if($query3[$key3]['bspi'] ==''){echo $query3[$key3]['bspi'] =0;} else{ echo $query3[$key3]['bspi'];} ?></td>
		</tr>
<?php $j++;	} ?>	
</tbody> 
</table>

<link href="<?php echo base_url(); ?>assets/admin/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/admin/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					
					$('#dataTable').dataTable();
					$('#dataTable1').dataTable();
					</script>
<style>
.countdata {
    background-color: #6f7977;
    padding-top: 10px;
    text-align: center;
    color: #fff;
    min-height: 84px;
}
.newcolor1 {
    background-color: #1abb9c;
}
.countdata label {
    font-size: 17px;
}
.countval {
    display: block;
    font-size: 17px;
    font-weight: bold;
}
.cent {
    font-size: 11px;
}
.low {
    display: block;
    color: #f0f0f0;
}
.low {
    font-size: 9px;
}
</style>