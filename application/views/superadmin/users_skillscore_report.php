<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
</style>
 <header class="head">
                               
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Skill Score Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
			
 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Skill Score Report</h5>
            </header>
			<div id = "tabs-1">
		<ul>
            <li id="59"><a href = "javascript:;" onclick="report2('','',59);" >Memory</a></li>
            <li id="60"><a href = "javascript:;" onclick="report2('','',60);">VP</a></li>
            <li id="61"><a href = "javascript:;" onclick="report2('','',61);">FA</a></li>
			<li id="62"><a href = "javascript:;" onclick="report2('','',62);">PS</a></li>
			<li id="63"><a href = "javascript:;" onclick="report2('','',63);">Linguistics</a></li>
         </ul>
			
			 </div>
            <div id="borderedTable" class="body collapse in">
			<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>S.No.</th>
							<th>Username</th>
							<th>School name</th>
							<th>Enrollment Date</th>
                            <th>User grade</th>
							<th>Skillscore</th>
                        </tr>
                    </thead>
                    <tbody>
					
                        <tr>
                            <td>1</td>
							<td>Saravanan</td>
                            <td>Edsix brain lab</td>
							<td>10-01-2014</td>
							<td>Grade I</td>
							<td>98</td>
                        </tr>
						
						<tr>
                            <td>2</td>
							<td>Cal</td>
							<td>Edsix Cognitive school</td>
                            <td>01-05-2014</td>
							<td>Grade III</td>
							<td>95.2</td>
                        </tr>
						
						<tr>
                            <td>3</td>
							<td>Nithya</td>
							<td>GKM school</td>
                            <td>01-01-2015</td>
							<td>Grade I</td>
							<td>85</td>
                        </tr>
						
						<tr>
                            <td>4</td>
							<td>Damu</td>
							<td>Jain vidyalaya</td>
                            <td>01-08-2015</td>
							<td>Grade V</td>
							<td>82</td>
                        </tr>
						
						<tr>
                            <td>5</td>
							<td>Kumaran</td>
							<td>Chandra school</td>
                            <td>01-05-2015</td>
							<td>Grade IV</td>
							<td>60</td>
                        </tr>
                    </tbody>
					</table>
                
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<link href = "<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(function() {
            $( "#tabs-1" ).tabs();
         });
	</script>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>

 