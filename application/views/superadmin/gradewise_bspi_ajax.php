<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
						<?php  //$overallbspi = round($trainningbspi[0]['overallbspi'], 2);
						//echo '<pre>'; print_r($bspi); 
						//echo '<pre>'; print_r($gradewisebspi);  exit;
						
						$overallbspi=array();
foreach($bspi as $row)
{
$overallbspi[$row['id']]=$row['bspiscore'];
}
$data['overallbspi']=$overallbspi;
						
						?>
                            <th>S.No.</th>
							<th>Grade Name</th>
                            <th>Avg. BSPI</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php //print_r($bspi);
					$i =1;
					foreach($gradewisebspi as $b) { 
					
					?>
					
                        <tr class="<?php   if(round($data['overallbspi'][$b['id']], 2)>round($b['overallbspi'], 2)){echo 'danger'; $diffpercent='<span class="percent down pull-right"> <i class="fa fa-caret-down"></i> '.(round($b['overallbspi'], 2)-round($data['overallbspi'][$b['id']], 2)).' %</span>'; } else if(round($data['overallbspi'][$b['id']], 2)==round($b['overallbspi'], 2)) { $diffpercent='<span class="percent pull-right"> <i class="fa fa-caret"></i> '.(round($b['overallbspi'], 2)-round($data['overallbspi'][$b['id']], 2)).' %</span>'; } else{ echo 'success'; $diffpercent='<span class="percent up pull-right"> <i class="fa fa-caret-up"></i> '.(round($b['overallbspi'], 2)-round($data['overallbspi'][$b['id']], 2)).' %</span>';} ?>">
                            <td><?php echo $i; ?></td>
							<td><?php echo $b['gradename']; ?></td>
                            <td><?php echo round($b['overallbspi'], 2).$diffpercent; ?></td>
                        </tr>	
					<?php $i++;  } ?>
					
					
                    </tbody>                </table>
						<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable();
					</script>
					
					<style>
					span.percent{font-size:10px;}
					</style>