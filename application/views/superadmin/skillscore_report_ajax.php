<?php 
//echo '<pre>'; print_r($report2);
//echo '<pre>'; print_r($overallsklscore);

$overallbspi=array();
foreach($overallsklscore as $row)
{
$overallbspi[$row['id']]=$row['finalscore'];
}
$data['overallskillscore']=$overallbspi;

?>
<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
					<th>s.no</th>
                        <th>Username</th>
                        <th>Grade</th>
                        <th>Skill score</th>
						 <th>AVG.Response time</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php $i=1; foreach($report2 as $r2) { ?>
                            <tr class="<?php   if(round($data['overallskillscore'][$r2['sid']], 2) >round($r2['score'], 2)){echo 'danger'; $diffpercent='<span class="percent down pull-right"> <i class="fa fa-caret-down"></i> '.round((round($r2['score'], 2)- round($data['overallskillscore'][$r2['sid']], 2))).' %</span>'; } else if(round($data['overallskillscore'][$r2['sid']], 2)==round($r2['score'], 2)) { $diffpercent='<span class="percent pull-right"> <i class="fa fa-caret"></i> '.round((round($r2['score'], 2)-round($data['overallskillscore'][$r2['sid']], 2))).' %</span>'; } else{ echo 'success'; $diffpercent='<span class="percent up pull-right"> <i class="fa fa-caret-up"></i> '.round((round($r2['score'], 2)-round($data['overallskillscore'][$r2['sid']], 2))).' %</span>';} ?>">
							<td><?php echo $i; ?></td>
							<td><?php echo $r2['fname']; ?></td>
							 <td><?php echo $r2['gradename']; ?></td>
							  <td><?php echo round($r2['score'], 2).$diffpercent; ?></td>
							   <td><?php echo round($r2['rstime'],2); ?></td>
							   </tr>
					<?php $i++; }  ?>
								 
                    </tbody>                
					</table>
<link href="<?php echo base_url(); ?>assets/admin/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/admin/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>