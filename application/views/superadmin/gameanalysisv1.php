<header class="head">
</header>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<link href = "<?php echo base_url(); ?>assets/admin/css/bootstrap-multiselect.css" rel = "stylesheet">

<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-multiselect.js"></script>
<style>
		 .sbmtbtn{padding: 10px 5px;margin: 9px auto;background: lightgreen;}
		 .chkveg1{padding: 10px 20px;margin: 9px auto;}
		 
		 .inner.bg-light.lter {
    min-height: 500px;
}
</style>
<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		


<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Game Analysis Report</h5>
            </header>
			<form id="frmtracking" action="<?php echo base_url(); ?>index.php/superadmin/gameanalysisv1" method="POST">
			<div class="row">
			<div class="col-sm-2">
			<label>Month</label><br/>
			<select id="chkveg1" multiple="multiple" name="month"  style="width:50%">
			 <?php $ini=0; foreach($academicmonths as $am){
				$ini++;
				
				echo '<option value="'.$am['monthNumber'].'">'.$am['monthName'].'</option>';
			} ?>
			
			
			            </select>
						<input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" /> 
						<?php $hdnMonthID=str_replace("multiselect-all,",'',$_POST['hdnMonthID']); 
						
						if($hdnMonthID==''){$hdnMonthID='06,07,08,09,10,11,12,01,02,03,04,05';}
						?>
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	        $('#btnget').click(function() {
			            	      //  alert($('#chkveg1').val());
			            	        })
			            	    });
			</script>
			
			</div>
			
			
			<div class="col-sm-3">
			<label>Type</label>
			<select  class="form-control input-sm" name="reporttype" id="reporttype">
			<option value="">Select</option>
			<option value="1" <?php if($_POST['reporttype']==1) { ?> selected='selected' <?php } ?>>Game list - Least number of times played</option>
			<option value="2" <?php if($_POST['reporttype']==2) { ?> selected='selected' <?php } ?>>Game list - Highest number of times played</option>
			<option value="3" <?php if($_POST['reporttype']==3) { ?> selected='selected' <?php } ?>>Game list - Avg score less than 25</option>
			<option value="4" <?php if($_POST['reporttype']==4) { ?> selected='selected' <?php } ?>>Game list - Avg score more than 75</option>
			</select>
			</div>
			
			
			<div class="col-sm-2">
					<input type="submit" class="btn sbmtbtn" name="btnreport1" id="report1" Value="Search">
				</div>
				</div>
				
			
			
			
			
			</form>
            
        </div>
    </div>
	
	
	
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
			    $(document).ready(function() {
					
					$("#frmtracking").validate({
		rules: {
			reporttype: {required: true},
            hdnMonthID: {required: true},
		   },
		   messages: {
			reporttype: {required: "Please choose report type"},
			hdnMonthID: {required: "Please select month"},
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	
		$('#report1').click(function(){
		if($("#frmtracking").valid()==true)
		{
		}
});

					
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['btnreport1'])) { ?>
				var data1="<?php echo $hdnMonthID; ?>";
				var dataarray1=data1.split(",");
				$("#chkveg1").val(dataarray1);
				$("#hdnMonthID").val(dataarray1);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
			    });
</script>

<div class="row">

<?php if($_POST['reporttype']==1) { ?>
<div id="" class="col-lg-12">
<h3>Game list - Least number of times played</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>No. of times played</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($LowestPlaygames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gamelist']); ?></td>
				<td><?php echo $gamelist['gcount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
<?php } ?>
 
 <?php if($_POST['reporttype']==2) { ?>
 <div id="" class="col-lg-12">
 <h3>Game list - Highest number of times played</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>No. of times played</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($HighestPlaygames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gamelist']); ?></td>
				<td><?php echo $gamelist['gcount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 <?php } ?>
 
 
</div>



<div class="row">
<?php if($_POST['reporttype']==3) { ?>
<div id="" class="col-lg-12">
 <h3>Game list - Avg., score less than 25</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>Grade</th>
				<th>Skill</th>
				<th>Avg. Score</th>
				<th>No. of times played</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($LowestScoreGames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gname']); ?></td>
				<td><?php echo $gamelist['grade']; ?></td>
				<td><?php echo $gamelist['skill']; ?></td>
				<td><?php echo $gamelist['gscore']; ?></td>
				<td><?php echo $gamelist['playedcount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
<?php } ?>

<?php if($_POST['reporttype']==4) { ?>
 <div id="" class="col-lg-12">
  <h3>Game list - Avg., score more than 75</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Game</th>
				<th>Grade</th>
				<th>Skill</th>
				<th>Avg. Score</th>
				 <th>No. of times played</th>
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($HighestScoreGames as $gamelist)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo str_replace(",","<br/>",$gamelist['gname']); ?></td>
				<td><?php echo $gamelist['grade']; ?></td>
				<td><?php echo $gamelist['skill']; ?></td>
				<td><?php echo $gamelist['gscore']; ?></td>
				 <td><?php echo $gamelist['playedcount']; ?></td>
			</tr>
			
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
<?php } ?>
 
</div>



</div>
</div>
</div>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('.dataTable').DataTable( );
					</script>
					