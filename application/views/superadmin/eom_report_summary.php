 <header class="head">
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css">

<script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
           Schools Tracking Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
	 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Schools Tracking Report</h5>
            </header>
			<form id="frmtracking" method="POST" action="">
			<div class="row">
			<div class="col-sm-2">
			<label>Schools</label>
			<select  class="form-control input-sm" name="schoolname" id="schoolname">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			<div class="col-sm-2">
				<label>Month</label>
					<select  class="form-control input-sm" name="month" id="month">
					<option value="">Select</option>
					</select>
			</div>
			
			<div class="col-sm-2">
					<input type="button" class="btn sbmtbtn" name="sbmtrprt" value="Submit" id="sbmtrprt" />
			</div>
</div>
	
	
	
</div>
			<br/>

<div class="row" id="editor" style="display:none;">
	<div class="col-sm-6">
		<label>Report Message</label>
		<textarea id="message" name="message" style="width:100%; display:none;"></textarea>
		<div class="validerr" ></div>
	</div>
	<div class="col-sm-2">
		<input type="button" class="btn sbmtbtn" value="Update" name="btnupdate" id="btnupdate" >
	</div>
</div>

<input type="hidden" value="" id="reportcode" class="" />

<!--<div class="col-sm-2"><input type="submit" class="btn sbmtbtn" name="DownloadPDF" id="DownloadPDF" Value="Download PDF"></div>-->
				</div>
			</form>
            <div id="collapse4" class="body">
				<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
				<div id="report1_result"></div>				
            </div>
        </div>
    
</div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/adapters/jquery.js"></script>

<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(document).ready(function(){
	
	$("#frmtracking").validate({
		rules: {
			schoolname: {required: true},
            month: {required: true},
		   },
		   messages: {
			schoolname: {required: "Please choose school"},
			month: {required: "Please select month"},
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	
	$("#sbmtrprt").click(function(){
		
		if($("#frmtracking").valid()==true)
		{
			$('#editor').show();
			GetReportMessage();
		}
		
	});
	
	$('#btnupdate').click(function(){
UpdateReport();

});
	
	
	$('textarea#message').ckeditor({
    height: "300px",
    toolbarStartupExpanded: true,
    width: "100%"
});
	

	
});

$('#month').change(function(){
	
	$('#editor').hide();
	
});
$('#ddlgrade').change(function(){

	var schoolid = 	$('#schoolname').val();
	var ddlgrade = $(this).val();
	ajaxsectionload(schoolid,ddlgrade);
	
});
function ajaxsectionload(schoolid,ddlgrade)
{
	//alert('hai');
var schoolid = 	$('#schoolname').val();
var ddlgrade = $('#ddlgrade').val();

$.ajax({
	 type: "POST",
		 url: "<?php echo base_url(); ?>index.php/superadmin/ajaxgetsection",
		data:{schoolid:schoolid,ddlgrade:ddlgrade},
		success: function(result)
		{
	//	alert(result);
		$('#section').html(result);
		}
	});
}

function UpdateReport()
{
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();
	var reportcode='SUMMARY';
	var message=$('#message').val();
	//$("#iddivLoading").show();
	$.ajax({
		type: "POST",
		//dataType: "json",
		url: "<?php echo base_url(); ?>index.php/superadmin/eom_reportupdate",
		data: {schoolid:schoolid,month:month,code:reportcode,message:message},
		success: function(result){
			$(".validerr").html("<span class='error'>Message has been Updated</span>");

			//alert(result);	
				//$("#iddivLoading").hide();		 
		}
	});
}
function GetReportMessage()
{
	var schoolid = $('#schoolname').val();
	var month=$('#month').val();	
	//$("#iddivLoading").show();
	$.ajax({
		type: "POST",
		//dataType: "json",
		url: "<?php echo base_url(); ?>index.php/superadmin/eom_getreportmessage",
		data: {schoolid:schoolid,month:month,code:"SUMMARY"},
		success: function(result){//alert(result);	
				 
			//	$("#reportcode").val(rcode);
				$("#message").val(result);
		}
	});
}


$('#schoolname').change(function(){
	$('#editor').hide();
	var sid = $(this).val();
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/superadmin/getacademicmonth",
    data: {sid:sid},
    success: function(result){// alert(result);	
		$("#iddivLoading").hide();
		$('#month').html(result);
    }
});
	
	
	
});

  

		 
</script>
<style>  </style>
<style>

 .sbmtbtn{padding: 10px 5px;margin: 9px auto;background: lightgreen;}
 
 #reset{padding: 8px 25px;margin: 14px auto;background: indianred;}

.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>	