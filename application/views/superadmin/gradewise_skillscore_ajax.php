<?php  $avgscore=0;$i=1; foreach($skilltopper as $skill) 
					{   
					$avgscore+=round($skill['finalscore'], 2);
					$i++; } 
					$avgscore=round($avgscore/($i-1), 2);
					?>
<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
						<?php //echo '<pre>'; print_r($gradewise_skillscore); exit;?>
                            <th>S.No.</th>
							<th>Skill Name</th>
							<th>Grade Name</th>
                            <th>Avg. score</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php //print_r($bspi);
					$i =1;
					foreach($gradewise_skillscore as $b) { 
					
					?>
					
                        <tr class="<?php   if($avgscore>round($b['overallskillscore'], 2)){echo 'danger'; $diffpercent='<span class="percent down pull-right"> <i class="fa fa-caret-down"></i> '.round((round($b['overallskillscore'], 2)-$avgscore)).' %</span>'; } else if($avgscore==round($b['overallskillscore'], 2)) { $diffpercent='<span class="percent pull-right"> <i class="fa fa-caret"></i> '.round((round($b['overallskillscore'], 2)-$avgscore)).' %</span>'; } else{ echo 'success'; $diffpercent='<span class="percent up pull-right"> <i class="fa fa-caret-up"></i> '.round((round($b['overallskillscore'], 2)-$avgscore)).' %</span>';} ?>">
                            <td><?php echo $i; ?></td>
							<td><?php echo $b['skillname']; ?></td>
							<td><?php echo $b['gradename']; ?></td>
                            <td><?php echo round($b['overallskillscore'], 2).$diffpercent; ?></td>
                        </tr>
						
						
			
						
						
					<?php $i++;  } ?>
					
					
                    </tbody>                </table>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>
					
					<style>
					span.percent{font-size:10px;}
					</style>