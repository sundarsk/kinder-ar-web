<header class="head">
</header>
<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-1.10.2.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
<style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:#b9cd6d;
            border: 1px solid #b9cd6d;
            color: #FFFFFF;
            font-weight: bold;
         }
</style>
<div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
<div class="col-lg-12">
<div class="text-center">
<ul class="pricing-table dark">
	<li class="primary col-lg-12">
		<h2>Schools Training - Dashboard</h2>
		 <div class="bg-primary text-success col-lg-6" >
	<h4>Total Number of Schools</h4>
		<h4 id="schoolscount"></h4>
    </div>
	
	  <div class="bg-warning text-success col-lg-6" >
	<h4>Total Number of Students</h4>
	<h4 id="studentscount"></h4>
     </div>
	
	  	
	</li>
	<div class="clearfix"></div>
</ul>
</div>
<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
 <style>
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>

 
</div>
</div>

<div class="row">

<div id="tbldatapart1" class="col-lg-12" style="overflow:scroll">
<h3>User Count by Schoolwise</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>School Name</th>
				<th>User Count</th>
				<th>Training Start Date</th>
				<th>Grade</th>
				<th>Expected User Utilization</th>
				<th>Attended User (% of Training Utilized (Attended/Expected)*100)</th>
				<th>Completed User (% of Training Utilized (Completed/Expected)*100)</th>
				<th>Completed User (% of Training Utilized (Completed/Attended)*100)</th>
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($schoolCounts as $schoolstudentcount)
		{ $i++;
		$percent = $result[$schoolstudentcount['id'].'-A']/$result[$schoolstudentcount['id'].'-R'];
		$perc1 = round( $percent * 100, 2 );
		$percent1 = $result[$schoolstudentcount['id'].'-C'] /$result[$schoolstudentcount['id'].'-A'];
		$perc2 = round( $percent1 * 100, 2 ); 
		
		$percent3 = $result[$schoolstudentcount['id'].'-C'] /$result[$schoolstudentcount['id'].'-R'];
		$perc3 = round( $percent3 * 100, 2 ); 
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $schoolstudentcount['school_name']; ?></td>
				<td><?php echo $schoolstudentcount['studentscount']; ?></td>
				<td><?php  if($schoolstudentcount['start_date']!='') { echo $date = $schoolstudentcount['start_date']; } else { echo $date=''; } ?></td>
				<td><?php echo str_replace("Grade",'',$schoolstudentcount['grade_id']); ?></td>
				<td><?php echo $result[$schoolstudentcount['id'].'-R']; ?></td>
				<td><?php echo $result[$schoolstudentcount['id'].'-A'].' ('.$perc1.'%)'; ?></td>
				<td><?php echo $result[$schoolstudentcount['id'].'-C'].' ('.$perc3.'%)'; ?></td>
				<td><?php echo $result[$schoolstudentcount['id'].'-C'].' ('.$perc2.'%)'; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
</div>
<div class="row">
 <div id="" class="col-lg-12">
 <h3>User Count by Gradewise</h3>
<table class="table table-bordered table-condensed table-hover table-striped dataTable">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Grade</th>
				<th>User Count</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($GradeStudentCount as $studentcount)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $studentcount['grade']; ?></td>
				<td><?php echo $studentcount['studentscount']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
</div>
<!--
<hr/>
<div class="row">

<div id="" class="col-lg-6">
<h3>Current Day Schedule</h3>
<table class="table table-bordered table-condensed table-hover table-striped">
		<thead>
			<tr>
				<th>S.No</th>
				<th>School Name</th>
				<th>Period</th>
				<th>Grade</th>
				<th>Section</th>
				<th>Total Registered Count</th>
				<th>Total Attended Count</th>
				<th>Total Completed Count</th>
				 
			</tr>
		</thead>  
		<tbody>
		<?php
		$i=0;
		foreach($CurrentDaySchedule as $list)
		{ $i++;
			?>
			
				<tr>
				<td><?php echo  $i; ?></td>
				<td><?php echo $list['schoolname']; ?></td>
				<td><?php echo $list['period']; ?></td>
				<td><?php echo $list['grade']; ?></td>
				<td><?php echo $list['section']; ?></td>
				<td><?php echo $list['regusers']; ?></td>
				<td><?php echo $list['attendusers']; ?></td>
				<td><?php echo $list['attendusers']; ?></td>
				 
			</tr>
			
			<?php
		}
		?>
				
						 
						 
		</tbody>
</table>
 </div>
 </div>
--><hr/>



</div>
</div>
</div>

<script>
function counts() {
	//alert('hello');
	$("#iddivLoading").show();
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/superadmin/dashboard_counts",
    data: {},
    
    success: function(result){
		//alert(result.schools);
		 $("#iddivLoading").hide();
		 $('#schoolscount').html(result.schools);
		 $('#studentscount').html(result.students);
     }
});
}

$(function() {
counts();
});
 /*
$(function() {
            $( "#tabs-1" ).tabs();
			
         });
var trainingskillid=59;	 
$(document).ready(function(){

	counts();
	skilltopperlist(trainingskillid);
	setInterval(function(){counts();}, 1000*10*60); 
	setInterval(function(){skilltopperlist(trainingskillid);}, 1000*10*60); 
	
});


function schoolwisebspi(trainingoverallbspi)
{
	//alert(trainingoverallbspi);
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/schoolwisebspi",
    data: {trainingoverallbspi:trainingoverallbspi},
    success: function(result){
		//alert(result);
		 $('#schoolbspilist').html(result);	 
    }
});
}

function skilltopperlist(val)
{
	$("#iddivLoading1").show();
trainingskillid=val;
$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/skillwisetopper",
    data: {val:val},
    success: function(result){
		 $("#iddivLoading1").hide();
		 $('#skilltopperlist').html(result);
    }
});
}
*/

</script>
<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('.dataTable').DataTable( );
					</script>
					