<div id="mainContDisp" class="container playGames homePlayGames" style="margin-top:20px;margin-bottom:70px;">
<form name="frmcoupon" id="frmcoupon" action="" method="POST" enctype="multipart/form-data" >
  <div class="row">
    <div class="col-lg-12">
	<h1 style="margin:0 0 10px;  border: none;border-bottom: 2px solid #eaeaea;padding-bottom: 7px;text-align: center;"><span style="padding-bottom:5px;">Registration</span> </h1>
	 </div></div>
	
	 <div class="row">
	  <div class="col-lg-12" style="text-align:center;">Welcome <strong><?php echo $userdetails[0]['firstname']; ?></strong>, please find your registration details below. Enter coupon code if you have, and click proceed to registration.</div>
	 
	 </div>
	 <br/>
	 
	  <div class="row">
	  <div class="col-lg-6" style="text-align:center;">Enter your coupon code to avail your discount</div>
	  <div class="col-lg-2" style="text-align:center;"><input type="text" class="form-control" value="" name="couponcode" id="couponcode" required />
	  <label id='errEmail' class="error"></label>
	  </div>
	  <div class="col-lg-2" style="text-align:center;"><input type="button" class="btn btn-success" value="Apply" id="btnapplycode" /></div>
	 
	 </div>
	
	 <br/>
	 
  <div class="row">
  <div class="col-lg-6"  style="text-align:center;border-right: 2px solid #106790;">
      <div class="form-group">
    <div  class="col-lg-6" style="text-align: right;">Name  :</div><div  class="col-lg-6" style="text-align: left;"><?php echo $userdetails[0]['firstname']; ?></div>
    </div> 
	<div class="form-group">
    <div  class="col-lg-6" style="text-align: right;">EMAIL  :</div><div  class="col-lg-6" style="text-align: left;"><?php echo $userdetails[0]['email']; ?></div>
    </div> 
	 <div class="form-group">
    <div  class="col-lg-6" style="text-align: right;">GRADE  :</div><div  class="col-lg-6" style="text-align: left;"><?php echo $userdetails[0]['gradename']; ?></div>
    </div> 
	 <!--<div class="form-group">
    <div  class="col-lg-6" style="text-align: right;">SCHOOL  :</div><div  class="col-lg-6" style="text-align: left;"><?php echo $userdetails[0]['schoolname']; ?></div>
    </div> -->
  </div>
   <div class="col-lg-6"  style="text-align:left;">
	<div class="form-group">
    <div  class="col-lg-4" style="text-align: right;">VALIDITY  :</div><div  class="col-lg-8" style="text-align: left;" id="validity"><?php echo $plandetails[0]['validity']; ?></div>
    </div> 
	 <div class="form-group">
    <div  class="col-lg-4" style="text-align: right;">PLAN AMOUNT  :</div><div  class="col-lg-8" style="text-align: left;" id="price"><?php echo $plandetails[0]['price']; ?></div>
    </div> 
	<div class="form-group">
    <div  class="col-lg-4" style="display:none;text-align: right;font-weight: bold;" id="disamnt" >DISCOUNTED AMOUNT  : </div><div  class="col-lg-8" id="discountamnt"  style="font-weight: bold;"></div>
    </div> 
	<div class="form-group">
    <div  class="col-lg-4" style="display:none;text-align: right;font-weight: bold;" id="payamnt"  >PAYMENT AMOUNT  : </div><div  class="col-lg-8" id="paidamnt" style="font-weight: bold;"></div>
    </div> 
	  </div> 
	 
   </div> 

   <div style="text-align:center;clear:both;">
    
   <div style="padding-bottom:5px;">   
     <label  style="color:red"  class="error" id="errSlot"></label>
</div>
  <!--<input type="button" id="proceed" style="float:none;" class="btn btn-success" name="proceed" value="Proceed to Pay">-->
  <input type="submit" id="proceed" style="float:none;" class="btn btn-success" name="proceed" value="Proceed to registration" >
   </div>
 </form>
</div>  
<script src="<?php echo base_url(); ?>assets/js/counter.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script> 
<script>
$("#frmcoupon").validate({
	rules: {
	//"couponcode": {required: true}
	},
	messages: {
	"couponcode": {required: "Please enter valid coupon code"}
	},
	errorPlacement: function(error, element) {
	error.insertAfter(element);
	},
	highlight: function(input) {
	$(input).addClass('error');
	} 
});
 
/* $('#proceed').click(function(){
			swal({
			  title: 'Payment',
			  text: "Please click yes,You will redirected to payment gateway",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, continue!',
			  cancelButtonText: 'No, cancel!',
			  confirmButtonClass: 'btn btn-success',
			  cancelButtonClass: 'btn btn-danger',
			  allowOutsideClick: false,
			  allowEscapeKey : false,
			  buttonsStyling: false
			}).then(function () {
					$("#frmcoupon").submit();  
			}, function (dismiss) {
			  if (dismiss === 'cancel') {
				
			  }
			});
}); */

	$('#btnapplycode').click(function(){
		//var use = '<?php $_REQUEST['uid']; ?>';
		//alert(use);
		if( $("#frmcoupon").valid() )
		{	
			$.ajax({
				 url: "<?php echo base_url(); ?>index.php/subscription/couponajax",
				 type:'POST',
				 dataType:"json",
				 data:{couponcode:$("#couponcode").val(),userid:'<?php echo $_REQUEST['uid']; ?>'},
				 success: function(result){
					 //alert(result);
					 
					 if(result.paidamount==0)
					 {
						 $('#proceed').val('Proceed to registration');
					 }
					 
					 if(result.isvalid==0)
					 {
						 $("#errEmail").html("Invalid coupon code").show();
					 }
					 else
					 {
					 
						$("#discountamnt").html(result.discountamount);
						$("#disamnt").show();
						$("#payamnt").show();
						$('#paidamnt').html(result.paidamount);
						$('#validity').html(result.validity);
						$('#price').html(result.price);
						$('#frmcoupon input').attr('readonly', 'readonly');
					 }
				}
				});
		}
	});
</script>

<style>
.error 

{   color: red;
    font-size: 13px;
}
</style>
 