<script>
$(document).ready(function(){
		
  $('.cssmenu > ul > li:has(ul)').addClass("has-sub");

  $('.cssmenu > ul > li > a').click(function() {
    var checkElement = $(this).next();
    
    $('.cssmenu li').removeClass('active');
    $(this).closest('li').addClass('active');	
    
    
    if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
      $(this).closest('li').removeClass('active');
      checkElement.slideUp('normal');
    }
    
    if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
      $('.cssmenu ul ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
    }
    
    if (checkElement.is('ul')) {
      return false;
    } else {
      return true;	
    }		
  });
  
  $(".cssmenu ul ul li:nth-child(4n+4)").addClass('fourthList');
  $(".cssmenu ul ul li:nth-child(3n+3)").addClass('threehList');
});
</script>
<div class="headerTop">
</div>
<div id="mainContDisp" class="container playGames homePlayGames" style="margin-top:20px;margin-bottom:70px;">

  
<form name="frmRegister" id="frmRegister" class="" method="post" enctype="multipart/form-data" >
  <div class="row">
    <div class="col-lg-12">
	<h1 style="margin:0 0 10px;  border: none;border-bottom: 2px solid #eaeaea;text-align: center;"><span style="padding-bottom:5px;">Frequently Asked Questions</span> </h1>
	 </div></div>
	 
	 <div class="row">
	  <div class="innerContainer">
        	<div class="tabs1">
    					<ul class="tab1-links">
        					<li class="active"><a id="firsttab" href="#tab1">Getting Started </a></li>
         					<li class=""><a href="#tab4">Playing With Skill Angels</a></li>
     					</ul>
    					<div class="tab1-content">
        					<div id="tab1" class="tab1 active" style="display: block;">
            					<div id="cssmenu" class="cssmenu" style="display: block;">
									<ul>
                                        <li class="has-sub"><a href="javascript:;"><span><h3>What skills are assessed by Skill Angels ?</h3></span></a>
                                            <ul>
                                                <p>The current package offered to schools and students caters to Cognitive &amp; Life skills.</p>
                                            </ul>
                                        </li>
                                        <li class="has-sub"><a href="javascript:;"><span><h3>For what grades are games available ?</h3></span></a>
                                            <ul>
                                                <p>Games are available from grades KG - 8 for the below skill areas</p>
                                                <p>1. Memory <br>2. Visual Processing <br>3. Problem Solving<br>4. Focus and Attention<br>5. Linguistics</p>
                                            </ul>
                                        </li>
                                        <li class="has-sub"><a href="javascript:;"><span><h3>Are games different for each grade? How many are there per grade?</h3></span></a>
                                            <ul>
                                                <p>Yes, the games are different across each grades and there are variants incorporated for each game across grades.</p>
                                            </ul>
                                        </li>
                                        <li class="has-sub"><a href="javascript:;"><span><h3>How can I get access to the games ?</h3></span></a>
                                            <ul>
                                                <p>Simple ! Games can either be purchased direct from Edsix Brain Labs or by schools in bulk for students. Schools to contact <a style="color: #e77817" href="mailto:angel@skillangel.com">angel@skillangel.com</a> for details regarding registration packages and signup</p>
                                            </ul>
                                        </li>
                                         <li class="has-sub"><a href="javascript:;"><span><h3>Once I login, what must I expect ?</h3></span></a>
                                <ul><p>Once logged in, Skill Angels will display 5 Cognitive games ( 1 game for each skill ) that can be played once.</p></ul>
                                </li>
                                
                                        
   									</ul>
								</div>
							</div>
 
        					
        					 
 
        					  
 
        					<div id="tab4" class="tab4" style="display: none;">
                            <div id="cssmenu" class="cssmenu" style="display: block;">
            					<ul>
                                <li class="has-sub"><a href="javascript:;"><span><h3>How is every game scored ?</h3></span></a>
                                <ul><p>Every game has a specific time duration for play &amp; a specific set of questions. Questions answered correctly in a shorter duration of time, will fetch higher marks than answering the questions correctly in longer duration of time. At the end of the time frame for the game or after completion of all questions, whichever is earlier, Skill angels cumulates the score for each game and presents the final score</p></ul>
                                </li>
                                
                                <li class="has-sub"><a href="javascript:;"><span><h3>How do i track my performance?</h3></span></a>
                                <ul><p>"Daily and Periodic performance can be viewed by accessing the Reports link of the Skill angels portal."</p></ul>
                                </li>
                                
                                 <li class="has-sub"><a href="javascript:;"><span><h3>What is BSPI?</h3></span></a>
                                <ul><p>BSPI stands for Brain Skill Power Index - A measure of Cognitive Brain Skills across 5 core areas : Memory, Focus &amp; Attention, Problem Solving, Visual Processing &amp; Linguistics.</p></ul>
                                </li>
                                
                                 <li class="has-sub"><a href="javascript:;"><span><h3>What inference must I draw from my BSPI ?</h3></span></a>
                                <ul><p>The BSPI is an individual score highlighting brain skill areas, well performed and areas that require further training.</p></ul>
                                </li>
                                
                                 <li class="has-sub"><a href="javascript:;"><span><h3>Is it possible for me to compare my skills with others?</h3></span></a>
                                <ul><p>Yes, it is possible to validate your BSPI score with the scores of other players in the same class.</p></ul>
                                </li>
                                
                                <li class="has-sub"><a href="javascript:;"><span><h3>What can affect my performance</h3></span></a>
                                <ul><p>Frequency of play and Focus during play are critical success factors for the program. Continous gaming enhances neuroplasticity for long lasting improvements &amp; dedication is inherent towards learning any skill.</p></ul>
                                </li>
                                
                                 
                                
                                 
                                 
                                
                                </ul>
                            </div>
        					</div>
                            
                             
    					</div>
					</div>
                    <script type="text/javascript"> 
	jQuery(document).ready(function() {
		jQuery('.cssmenu').hide();
		        jQuery('.tab1 .cssmenu').show();

    jQuery('.tabs1 .tab1-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 		// Show/Hide Tabs
        jQuery('.tabs1 ' + currentAttrValue).show().siblings().hide();
		 jQuery( currentAttrValue+' .cssmenu').show();
 		// Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 		e.preventDefault();
    });
});
</script>
        </div>
	 
	 </div>
	 <br/>
   

    
   
 </form>
</div>