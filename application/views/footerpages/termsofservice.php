<div class="headerTop">
 
 
</div>


<div id="mainContDisp" class="container playGames homePlayGames" style="margin-top:20px;margin-bottom:70px;">

  
<form name="frmRegister" id="frmRegister" class="" method="post" enctype="multipart/form-data" >
  <div class="row">
    <div class="col-lg-12">
	<h1 style="margin:0 0 10px;  border: none;border-bottom: 2px solid #eaeaea;text-align: center;"><span style="padding-bottom:5px;">Terms Of Service</span> </h1>
	 </div></div>
	 
	 <div class="row">
	  <div class="innerContainer">
        	<div class="terms">
            	<p>The following terms and conditions will be deemed to have been accepted by the User on usage of the website <a style="color: #e77817" target="_blank" href="<?php echo base_url(); ?>">KinderAngels</a>. You are requested to read them carefully before you use the services of this site.</p>
            	<ul>
                	<li><span>1</span><p>The term User shall refer to the user who is browsing the site. The term site refers to <a style="color: #e77817" target="_blank" href="<?php echo base_url(); ?>">KinderAngels</a> and other websites associated with Edsix, owned and monitored by Edsix.</p></li>
                    <li class="alterLi"><span>2</span><p>By using the site, you agree to follow and be bound by the following terms and conditions concerning your use of the site. Edsix may revise the Terms of Use at any time without notice to you. Areas of the site may have different terms of use posted. If there is a conflict between the Terms of Use and terms of use posted for a specific area of the site, the latter shall have precedence with respect to your use of that area of the site.</p></li>
                    <li><span>3</span><p>All content present on this site is the exclusive property of Edsix. The software, text, images, graphics, video and audio used on this site belong to Edsix. No material from this site may be copied, modified, reproduced, republished, uploaded, transmitted, posted or distributed in any form whatsoever without prior written permission from Edsix. All rights not expressly granted herein are reserved. Unauthorized use of the materials appearing on this site may violate copyright, trademark and other applicable laws, and could result in criminal and/or civil penalties. Edsix is a registered trademark of EdSix Brain Lab Private Ltd,. This trademark may not be used in any manner without prior written consent from EdSix Brain Lab Private Ltd,</p></li>
                    <li class="alterLi"><span>4</span><p>Edsix does not make any warranties, express or implied, including without limitation, those of merchantability and fitness for a particular purpose, with respect to any information, data, statements or products made available on the site.</p></li>
                    <li><span>5</span><p>Edsix does not accept any responsibility towards the contents and/or information practices of third party sites which have links through skill angels site. The said links to internal or external website locations are only for the purpose of facilitating your visit or clarify your query.</p></li>
                    <li class="alterLi"><span>6</span><p>The site, and all content, materials, information, software, products and services provided on the site, are provided on an "as is" and "as available" basis. Edsix expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose and non-infringement.</p></li>
                    <li><span>7</span><p>Edsix shall have no responsibility for any damage to a User's computer system or loss of data that results from the download of any content, materials, information from the site.</p></li>
                    <li class="alterLi"><span>8</span><p>In no event will Edsix be liable for damages of any kind, including without limitation, direct, incidental or consequential damages (including, but not limited to, damages for lost profits, business interruption and loss of programs or information) arising out of the use of or inability to use Edsix website, or any information provided on the website, or in the Products any claim attributable to errors, omissions or other inaccuracies in the Product or interpretations thereof. Some jurisdictions do not allow the limitation or exclusion of liability. Accordingly, some of the above limitations may not apply to the User.</p></li>
                    <li><span>9</span><p>The User agrees to assure, defend and hold Edsix harmless from and against all losses, expenses, damages and costs, including reasonable attorneys' fees, arising out of or relating to any misuse by the User of the content and services provided on the site.</p> </li>
                    <li class="alterLi"><span>10</span><p>The information contained in the site has been obtained from sources believed to be reliable. Edsix disclaims all warranties as to the accuracy, completeness or adequacy of such information.</p></li>
                    <li><span>11</span><p>Edsix makes no warranty that: (a) the site will meet your requirements; (b) the site will be available on an uninterrupted, timely, secure, or error-free basis; (c) the results that may be obtained from the use of the site or any services offered through the site will be accurate or reliable.</p></li>
                    <li class="alterLi"><span>12</span><p>The User's right to privacy is of paramount importance to Edsix. Any information provided by the User will not be shared with any third party. Edsix reserves the right to use the information to provide the User a more personalized online experience.</p></li>
                    <li><span>13</span><p>The site provides links to websites and access to content, products and services from third parties, including users, advertisers, affiliates and sponsors of the site. You agree that Edsix is not responsible for the availability of, and content provided on, third party websites. The User is requested to peruse the policies posted by other websites regarding privacy and other topics before use. Edsix is not responsible for third party content accessible through the site, including opinions, advice, statements and advertisements and User shall bear all risks associated with the use of such content. Edsix is not responsible for any loss or damage of any sort that a User may incur from dealing with any third party.</p></li>
                </ul>
                <p>These terms and conditions in the end user agreement are governed in accordance with the laws of India. Any dispute shall be subject to the exclusive jurisdiction of courts in chennai only.</p>
                <h2>General Disclaimer</h2>
                <p>The documents and graphics on this website could include technical inaccuracies or typographical errors. Changes are periodically added to the information herein. Edsix may make improvements and/or changes herein at any time. Edsix makes no representations about the accuracy of the information contained in the documents and graphics on this website for any purpose. All documents and graphics are provided "as is". Edsix hereby disclaims all warranties and conditions with regard to this information, including all implied warranties and conditions of merchantability, fitness for any particular purpose, title and non-infringement. In no event, edsix and/or its licensor/ supplier shall not be liable to any party for any direct, indirect, special or other consequential damages for any use of the sites, the information, or on any other hyperlinked website, including, without limitation, any lost profits, business interruption, loss of programs or other data on your information handling system or otherwise, even if edsix is expressly advised of the possibility of such damages.</p>
            </div>
            <script type="text/javascript">
				$(document).ready(function(e) {
                    $(".terms li:odd").addClass('alterLi');
                });
			</script>
        </div>
	 
	 </div>
 </form>
</div>