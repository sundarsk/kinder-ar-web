<footer>
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6">

  
  <ul>
<li>EdSix Brain Lab Pvt Ltd<sup>TM</sup></li>
<li># 1 H, Module no. 8,</li>
<li>IIT Madras Research Park First Floor,</li>
<li>Kanagam Road ,Taramani Chennai - 600113</li>
  </ul>
  <?php //echo $this->lang->line("footeraddress"); ?> 
  </div>
<div class="col-md-3 col-sm-6">
<ul>
<li class="callicon"><?php echo $this->lang->line("ftphonenumber"); ?></li>
<li class="msgicon"><a href="mailto:angel@skillangels.com"><?php echo $this->lang->line("ftemail"); ?></a></li>
</ul>
<div class="socialmedia">
<span><?php echo $this->lang->line("ftjoin"); ?></span>
<a href="https://www.facebook.com/skillangels" target="_blank"><img src="<?php echo base_url(); ?>assets/images/fb.png" width="33" height="33"></a> <a href="https://www.linkedin.com/company/edsix-brain-lab-pvt-ltd?trk=company_logo" target="_blank"><img src="<?php echo base_url(); ?>assets/images/icon_LinkedIn.png" width="33" height="33"></a>
</div>

</div>
<div class="col-md-3 col-sm-6">
<ul>
<li><a href=""><?php echo $this->lang->line("fthome"); ?></a></li>
<li><a href="<?php echo base_url(); ?>index.php/home/termsofservice" target="_blank"><?php echo $this->lang->line("ftterms"); ?></a></li>
<li><a href="<?php echo base_url(); ?>index.php/home/privacypolicy" target="_blank"><?php echo $this->lang->line("ftprivacy"); ?></a></li>
<li><a href="<?php echo base_url(); ?>index.php/home/faq" target="_blank"><?php echo $this->lang->line("ftfaq"); ?></a></li>
</ul>
</div>
<div class="col-md-3 col-sm-6">

  <img src="<?php echo base_url(); ?>assets/images/sklogo-web.png" class="img-responsive"  width="193" height="67">
   <br/>
<img src="<?php echo base_url(); ?>assets/images/logo_RTBI.png"  > <img src="<?php echo base_url(); ?>assets/images/logo_CJE.png"  ></div>
</div>
</div>

</footer>

<div class="footerBottom"><p>&copy; <?php echo date("Y"); ?> Skillangels. All rights reserved</p></div>

<script type="text/javascript">
  $(document).ready(function(e) { 
   $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 6000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
});
</script>

<script>
/* ****************************** User Login *********************************** */	
$('#submit').click(function(){ 
var form=$("#form-login");
/* Avoid Multiple Login */	
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/home/islogin') ?>",
data:form.serialize(),
success:function(isloginval)
{ //alert(isloginval);
	if(isloginval==0){
		if(($('#termscondition').is(':checked')) )
		{
			userlogin(form);
		}
		else
		{
			termscheck(form);
		}
	}
	else
	{
			swal({
			  title: 'Are you sure?',
			  text: "You are logging into another system.would you like to continue.",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, continue!',
			  cancelButtonText: 'No, cancel!',
			  confirmButtonClass: 'btn btn-success',
			  cancelButtonClass: 'btn btn-danger',
			  buttonsStyling: false
			}).then(function () {
					if(($('#termscondition').is(':checked')) )
					{
						userlogin(form);
					}
					else
					{
						termscheck(form);
					}
			  
			}, function (dismiss) {
			  if (dismiss === 'cancel') {
				swal(
				  'Cancelled',
				  'You are continuing with your previous login :)',
				  'error'
				)
			  }
			});
		
	}
}
});
});	

function userlogin(form)
{
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/userlogin') ?>",
				data:form.serialize(),
				success:function(result)
				{
					if(result==1)
					{
						window.location.href= "<?php echo base_url();?>index.php/home/dashboard#View";
					}
					else
					{						
						$("#errormsg").html('Invalid Credentials');
					}
				}
		}); 
	
}
function termscheck(form)
{
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/termscheck') ?>",
				data:form.serialize(),
				success:function(result)
				{
				//alert(result);
					if(result==0  && $.trim(result)!='')
					{
						$("#termschkbox").show();
						$("#terrormsg").html('Please check terms and conditions');
						
					}

					else
					{
						userlogin(form);
					}

				}
		});
}
</script>
<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    $("html,body").animate({scrollTop:$("#header").offset().top},"100");return false;
}
</script>
</body>
</html>