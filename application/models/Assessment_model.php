<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assessment_model extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
				 $this->load->library('Multipledb');
        }
		public function getacademicyearbyschoolid($sid)
		 {
			 
			$query = $this->db->query("select startdate,enddate,id from academic_year where id=(select academic_id from schools where id='".$sid."')order by id desc limit 1");
			return $query->result_array();
		 }

        public function checkUser($username,$password,$langid)
        {
			$query = $this->db->query('select a.* FROM school_parent_master as a WHERE a.username="'.$username.'" AND a.password=SHA1(CONCAT(salt1,"'.$password.'",salt2)) AND a.status=1');
			
			//echo $this->db->last_query(); exit;
			return $query->result();
        }
		public function checkChildisExist($username,$password,$langid)
        {
			$query = $this->db->query('select a.* FROM users as a WHERE a.username="'.$username.'" AND a.password=SHA1(CONCAT(salt1,"'.$password.'",salt2)) AND a.status=1');			
			return $query->result();
        }
		public function islogin($username,$password,$idealtime)
        {	//echo 'select count(id) as islogin FROM users a WHERE username='.$username.' AND password=md5('.$password.') AND status=1 AND (SELECT school_id FROM school_admin WHERE school_id=a.sid AND active=1) AND  TIMESTAMPDIFF(MINUTE,last_active_datetime,NOW())<='.$idealtime.'';
			$query = $this->db->query('CALL IsLogin("'.$username.'","'.$password.'","'.$idealtime.'","")');
			mysqli_next_result($this->db->conn_id);
			//echo $this->db->last_query(); exit;
			return $query->result();
        }
		public function ischildlogin($username,$password,$idealtime)
        {	
			$query = $this->db->query('CALL IsChildLogin("'.$username.'","'.$password.'","'.$idealtime.'","")');
			mysqli_next_result($this->db->conn_id);
			//echo $this->db->last_query(); exit;
			return $query->result();
        }
		public function checkuserisactive($userid,$login_session_id)
        {
			$query = $this->db->query('CALL checkuserisactive("'.$userid.'","'.$login_session_id.'")');
			mysqli_next_result($this->db->conn_id);
			//echo $this->db->last_query(); exit;
			return $query->result();
        }
		public function updateuserloginstatus($userid,$login_session_id)
        {	//echo 'CALL updateuserloginstatus("'.$userid.'","'.$login_session_id.'")';exit;
			$query = $this->db->query('CALL updateuserloginstatus("'.$userid.'","'.$login_session_id.'")');
			mysqli_next_result($this->db->conn_id);
			
        }	
		
		/* public function getdates($userid)
		 {
			 
		$query = $this->db->query("select AY.startdate,AY.enddate from users UG,academic_year AY where AY.id=UG.academicyear and UG.id='".$userid."' limit 0,1");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }	 */
		 
		  public function getbspireport($userid)
		 {
			 
		$query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth,year(lastupdate) as monofyear  FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."' and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."'    group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 
		 
		 
		 public function languagechange($email)
		 {
		$query = $this->db->query("select * from language_master where status='Y' and ID in (select languageID from schools_language where schoolID=(select sid from users where username='".$email."' and status=1) and status='Y')");
		//echo $this->db->last_query(); exit;
			return $query->result();
		 }
		 public function update_loginDetails($userid,$session_id)
		 { 
		$query = $this->db->query("update users set pre_logindate = login_date,login_date = CURDATE(),login_count=login_count+1,session_id=".$session_id.",islogin=1,last_active_datetime=NOW() WHERE id =".$userid);
		//echo $this->db->last_query(); exit;
		 }	

		public function getRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id)
		 {
			 
			$query = $this->db->query("SELECT gid FROM rand_selection WHERE DATE(created_date) = '".$check_date_time."' AND gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and user_id='".$this->session->user_id."' GROUP BY school_id, gs_id ORDER BY gs_id ASC");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function deleteSPLRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id)
		 {
			$query = $this->db->query("delete FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."'");
		 }
		 public function getAssignGames($game_plan_id,$game_grade,$uid,$catid)
		 {
			 
		$query = $this->db->query("SELECT a.id, a.grade_id, d.skill_id FROM users AS a JOIN g_plans AS b ON a.gp_id = b.id AND b.id = '".$game_plan_id."' JOIN class_plan_game AS c ON b.id = c.plan_id AND b.grade_id = c.class_id JOIN class_skill_game AS d ON c.class_id = d.class_id AND c.game_id = d.game_id AND d.class_id = '".$game_grade."' JOIN category_skills AS e ON e.id = d.skill_id WHERE a.id = '".$uid."' AND e.category_id = '".$catid."' GROUP BY d.skill_id limit 1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getSkillsRandom($catid,$curdayskillid)
		 {
		$query = $this->db->query("SELECT a.id AS category_id, b.id AS skill_id FROM g_category AS a JOIN category_skills AS b ON a.id = b.category_id WHERE a.id = '".$catid."' ORDER BY rand() limit 3 ");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getSK_SkillsRandom($uid)
		 {
			 
		//$query = $this->db->query("SELECT b.id AS skill_id  from category_skills AS b WHERE FIND_IN_SET ( b.id , ( select weakSkills from sk_user_game_list where 	userID='".$uid."' and status=0))");
		$query = $this->db->query("SELECT b.id AS skill_id,levelid from category_skills AS b 
join sk_user_game_list as suk on FIND_IN_SET (b.id ,suk.weakSkills) where suk.userID='".$uid."' and suk.status=0");
		
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function assignRandomGame($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id)
		 {
			 
		$query = $this->db->query("SELECT j.id, e.skill_id, j.name AS skill_name, g.gid, g.gname FROM users AS a
				JOIN g_plans AS b ON a.gp_id = b.id AND b.id = '".$game_plan_id."' JOIN class AS c ON a.grade_id = c.id AND c.id = '".$game_grade."' JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id JOIN category_skills AS j ON e.skill_id = j.id JOIN skl_class_plan AS f ON a.sid = f.school_id JOIN games AS g ON d.game_id = g.gid JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$school_id."' WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' and j.id = '".$skill_id."' and g.gid not in (SELECT gid FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and user_id='".$this->session->user_id."' and gs_id = '".$skill_id."') and g.gid not in(243,283,23,65,100,146,140,179,186,226,266,307,233) GROUP BY g.gid ORDER BY RAND() LIMIT 1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function assignSK_RandomGame($check_date_time,$game_plan_id,$game_grade,$uid,$skill_id,$school_id)
		 {
			 
		$query = $this->db->query("SELECT gid FROM sk_rand_selection WHERE DATE(created_date) = '".$check_date_time."'  AND userID = '".$uid."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' AND gs_id='".$skill_id."' GROUP BY school_id, gs_id,gid order by gs_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getAssignSK_RandomGame($check_date_time,$game_plan_id,$game_grade,$uid,$school_id)
		 {
			 
		$query = $this->db->query("SELECT gid FROM sk_rand_selection WHERE DATE(created_date) = '".$check_date_time."' AND userID = '".$uid."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' GROUP BY school_id, gs_id,gid order by gs_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function assignSK_assignGameCount($game_plan_id,$skill_id,$school_id,$levelid)
		 {
			 
		$query = $this->db->query("select * from sk_personalized_game where sk_planSkillCountID in (select ID from sk_plan_skillcount where school_ID='".$school_id."' and plan_ID='".$game_plan_id."') and skillID = '".$skill_id."' and level='".$levelid."' ");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getSK_randomGames($game_plan_id,$game_grade,$uid,$skill_id,$school_id,$assign_count,$cur_day_skills,$levelid)
		 {
			 
		$query = $this->db->query("SELECT g.skill_ID as skill_id,(select name from category_skills where id=g.skill_ID) as skill_name,g.name as gname,g.ID as gid,gp.plan_ID FROM sk_games g join sk_games_plan gp on g.ID=gp.sk_game_ID  join sk_personalized_game pg on gp.plan_ID=from_GradeID and level='".$levelid."' WHERE g.skill_ID='".$skill_id."' and gp.school_ID='".$school_id."' and  pg.skillID=g.skill_ID and pg.sk_planSkillCountID in (select ID from sk_plan_skillcount where school_ID='".$school_id."' and plan_ID='".$game_plan_id."' ) and g.ID not in (SELECT gid FROM sk_rand_selection  WHERE userID = '".$uid."' AND grade_id = '".$game_grade."'  AND school_id = '".$school_id."' and gs_id = '".$skill_id."') and g.game_masterID not in(243,283,23,65,100,146,140,179,186,226,266,307,233) order by rand() limit ".($assign_count-$cur_day_skills)." ;");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 } 
		 public function getSK_mindatePlay($game_plan_id,$game_grade,$uid,$skill_id,$school_id,$catid)
		 {
			 
		$query = $this->db->query("select min(created_date) as mindate from sk_rand_selection where gc_id = '".$catid."' AND userID = '".$uid."'  and gs_id = '".$skill_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function deleteSK_OldGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$mindate)
		 {
			 
		$query = $this->db->query("delete from sk_rand_selection where gc_id = '".$catid."' AND userID = '".$uid."'  and gs_id = '".$skill_id."' and  grade_id = '".$game_grade."'  and school_id = '".$school_id."'  and created_date='".$mindate."'");
		//echo $this->db->last_query(); exit;
			 
		 }
		 public function deleteRandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$del_where)
		 {
			 
		$query = $this->db->query("delete from rand_selection where gc_id = '".$catid."' and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' and user_id='".$this->session->user_id."' ".$del_where);
		//echo $this->db->last_query(); exit;
			 
		 }
		 public function deleteSK_RandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$del_where)
		 {
			 
		$query = $this->db->query("delete from sk_rand_selection where gc_id = '".$catid."' AND userID = '".$uid."'  and gs_id = '".$skill_id."' and  grade_id = '".$game_grade."'  and school_id = '".$school_id."' ".$del_where);
		//echo $this->db->last_query(); exit;
			 
		 }
		 public function insertRandomGames($catid,$game_plan_id,$game_grade,$skill_id,$school_id,$section,$gameid,$check_date_time)
		 { 
			$query = $this->db->query("INSERT INTO rand_selection SET gc_id = '".$catid."', gs_id = '".$skill_id."', gid = '".$gameid."', gp_id = '".$game_plan_id."', grade_id = '".$game_grade."', section = '".$section."', school_id = '".$school_id."',user_id='".$this->session->user_id."',created_date = '".$check_date_time."'"); 
		 } 
		 public function insertSK_RandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$section,$gameid,$check_date_time)
		 {

		$query = $this->db->query("INSERT INTO sk_rand_selection SET userID='".$uid."', gc_id = '".$catid."', gs_id = '".$skill_id."', gid = ".$gameid.", gp_id = '".$game_plan_id."', grade_id = '".$game_grade."', section = '".$section."', school_id = '".$school_id."', created_date = '".$check_date_time."';");
		
			 
		 }
		 public function getActualGames($game_plan_id,$game_grade,$uid,$catid,$where)
		 {
		$query = $this->db->query("SELECT j.id, (select count(*) as tot_game_played from game_reports where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id AND lastupdate = '".date('Y-m-d')."') as tot_game_played ,(select MAX(game_score)  from game_reports where gu_id =  '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id AND lastupdate = '".date('Y-m-d')."') as tot_game_score , e.skill_id, j.name AS skill_name, g.gid, g.gname, g.img_path,g.game_html, j.icon 
		FROM users AS a
		JOIN g_plans AS b ON a.gp_id = b.id AND b.id = '".$game_plan_id."'
		JOIN class AS c ON a.grade_id = c.id AND c.id = '".$game_grade."'
		JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id
		JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id
		JOIN category_skills AS j ON e.skill_id = j.id 
		JOIN skl_class_plan AS f ON a.sid = f.school_id
		JOIN games AS g ON d.game_id = g.gid
		JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$_SESSION['school_id']."'
		WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' $where
		GROUP BY skill_id,g.gid");
	//	echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	 
		 public function getSK_ActualGames($game_plan_id,$game_grade,$uid,$catid,$where)
		 {
		$query = $this->db->query("SELECT  g.skill_ID as skill_id,game_html,(select count(*) as tot_game_played from sk_game_reports where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND g_id = g.ID AND gp_id = '".$game_plan_id."' AND lastupdate = '".date('Y-m-d')."') as tot_game_played, (select times_count from sk_games_plan where school_ID='".$_SESSION['school_id']."'  and sk_game_ID=g.ID) as playcount, (select name from category_skills where id=g.skill_ID) AS skill_name, g.ID as gid, g.name as gname, g.image_path as img_path FROM sk_games AS g where 1 $where order by skill_id ");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 public function getTrainCalendar($userid,$startdate,$enddate,$catid)
		 {
		$query = $this->db->query("select group_concat(distinct(lastupdate)) as updateDates  from game_reports WHERE gu_id = '".$userid."' and (lastupdate between '".$startdate."' and '".$enddate."')  and gc_id = '".$catid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getBSPI($userid)
		 {
		$query = $this->db->query("SELECT (AVG(`game_score`)) as score ,gs_id , lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."' and (lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."')   group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 
		 public function getleftbardata($userid)
		 {
			 
		$todaydate=date('Y-m-d');
		$query = $this->db->query("select avatarimage,fname,DATEDIFF(CURDATE(), login_date) as noofdays,pre_logindate from users where status=1 and id='".$userid."'");
		//echo $this->db->last_query(); exit;
			return $query->result();
		 }			 
		 
		 public function getMyCurrentTrophies($userid)
		 {
		$query = $this->db->query("select cs.id as catid,cs.name as name ,(select sum(diamond) from popuptrophys pt where gu_id=".$userid." and pt.catid=cs.id) as diamond ,(select sum(gold) from popuptrophys pt where gu_id=".$userid." and pt.catid=cs.id) as gold ,(select sum(silver) from popuptrophys pt where gu_id=".$userid." and pt.catid=cs.id) as silver from category_skills cs where category_id=1 ");
		//echo $this->db->last_query(); //exit;
		//echo "<pre>";
		//print_r($query->result_array());
		//exit;
			return $query->result_array();
		 }		

		public function getmyprofile($userid)
		 {
			 
		$query = $this->db->query("SELECT * FROM school_parent_master where id='".$userid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 

		public function getplandetais($planid)
		 {
			 
		$query = $this->db->query("select * FROM g_plans where id='".$planid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	

		public function getgradedetais($gradeid)
		 {
			 
		$query = $this->db->query("select distinct(classname),id from class where id='".$gradeid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	

		public function updateprofile($sfname,$gender,$emailid,$dob,$fathername,$mothename,$address,$sphoneno,$id,$shpassword,$salt1,$salt2,$confirmpass,$targetPath,$ip)
		
		 {
			  if($targetPath==""){$avatarimage="avatarimage";}
			 else{$avatarimage="'".$targetPath."'";}
			 
			 if($shpassword !='' && $confirmpass!='')
{
	 //$newpassword=md5($newpass); 		 
			 $qry=$this->db->query("INSERT INTO password_history(userid, changed_on, newpwd, ip) VALUES('".$id."',NOW(),'".$confirmpass."','".$ip."') ");			 
		
		$query = $this->db->query("update users set fname = '".$sfname."',password='".$shpassword."',salt1='".$salt1."',salt2='".$salt2."',father='".$fathername."',mother='".$mothename."',gender='".$gender."',email='".$emailid."',dob='".$dob."',mobile='".$sphoneno."',address='".$address."', avatarimage=$avatarimage where id = '".$id."'");
}
else{
	
	$query = $this->db->query("update users set fname = '".$sfname."',father='".$fathername."',mother='".$mothename."',gender='".$gender."',email='".$emailid."',dob='".$dob."',mobile='".$sphoneno."',address='".$address."', avatarimage=$avatarimage where id = '".$id."'");
	
}
		//echo $this->db->last_query(); exit;
			//return $query->result_array();
		 }


	   public function getgameplanid($userid)
		 {
			 
		$query = $this->db->query("select gp_id from users where id = '".$userid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }

		public function getgamenames($userid,$pid)
		 {
			 
		$query = $this->db->query("SELECT a.gid,a.gname FROM  games a, game_reports b where a.gid=b.g_id and b.gu_id='".$userid."' and b.gp_id = '".$pid."'  and a.gc_id = 1 and (lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."') group by a.gid");
		//echo $this->db->last_query(); exit;
			return $query->result();
		 }

		public function getgamedetails($userid,$gameid,$pid)
		 {
			 
		$query = $this->db->query("select g_id, avg(game_score) as game_score,lastupdate from game_reports where gp_id = '".$pid."' and gu_id='".$userid."' and g_id='".$gameid."' AND (lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."') group by lastupdate  ORDER BY lastupdate DESC LIMIT 10");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }		 
		 public function insertone($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$star,$puzzle_cycle)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into gamedata (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate,star,puzzle_cycle,site_type) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."','".$star."','".$puzzle_cycle."','WEB')");
		//echo $this->db->last_query(); exit;
			 
		 }	 
		 public function insertone_SK($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into sk_gamedata (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."')");
		//echo $this->db->last_query(); exit;
			 
		 }	
		 
		 public function insertlang($gameid,$userid,$userlang,$skillkit)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("INSERT INTO game_language_track( gameID, userID, languageID, skillkit, createddatetime) VALUES ('".$gameid."','".$userid."','".$userlang."','".$skillkit."',NOW())");
		//echo $this->db->last_query(); exit;
			 
		 }	
		 
		public function getresultGameDetails($userid,$gameid,$gradeid)
		{			 
			$query = $this->db->query("select (select gs_id from games where gid='".$gameid."') as gameskillid,puzzle_cycle,(select count(gu_id) from game_reports as gd where gu_id=".$userid." and gd.puzzle_cycle=puzzle_cycle and g_id=".$gameid.") as playedgamescount,(select COALESCE(SUM(star),0) from game_reports as gd where gu_id=".$userid." and gd.puzzle_cycle=rs.puzzle_cycle and g_id=".$gameid.") as PrevStar from rand_selection as rs where gid=".$gameid." and grade_id='".$gradeid."' and user_id=".$userid." and played='N' ");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		}	
		 
		  public function insertthree($userid,$gameid,$acid,$lastup_date,$st)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into user_games (gu_id,played_game,last_update,date,status,academicyear) values('".$userid."','".$gameid."','".$lastup_date."','".$lastup_date."','".$st."','".$acid."')");
		 
		 }	
		 /* public function getacademicyear()
		 {
			 
		$query = $this->db->query("select startdate,enddate from academic_year where id=(select academic_id from schools where id=".$this->session->school_id.") order by id desc limit 1");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }	 */	 
		 
		public function getacademicmonths($startdate,$enddate)
		 { //echo ;exit;
			 
		$query = $this->db->query("select DATE_FORMAT(m1, '%m') as monthNumber,DATE_FORMAT(m1, '%Y') as yearNumber,DATE_FORMAT(m1, '%b') as monthName from (select ('".$startdate."' - INTERVAL DAYOFMONTH('".$startdate."')-1 DAY) +INTERVAL m MONTH as m1 from (select @rownum:=@rownum+1 as m from(select 1 union select 2 union select 3 union select 4) t1,(select 1 union select 2 union select 3 union select 4) t2,(select 1 union select 2 union select 3 union select 4) t3,(select 1 union select 2 union select 3 union select 4) t4,(select @rownum:=-1) t0) d1) d2 where m1<='".$enddate."' order by m1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 
		 public function getskills()
		 {
			 
		$query = $this->db->query("select name,id from category_skills where category_id = 1 order by id");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }
		 
		 public function getcalendar($uid,$start_date,$last_date)
		 {
	
		$query = $this->db->query("SELECT id, date_format(lastupdate, '%d/%m/%Y') as created_date FROM game_reports WHERE gu_id = '".$uid."' and lastupdate between '".$start_date."' and '".$last_date."'  and gc_id = 1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 
		 public function getskpreport($userid,$skillsid,$month)
		 {
	
		$query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth FROM `game_reports` WHERE gs_id in (".$skillsid.") and gu_id='".$userid."' and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and DATE_FORMAT(lastupdate, '%Y-%m')=\"".$month."\"   group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function mybspicalendar($school_id,$uid,$dateQry,$startdate,$enddate)
		 {
			 $query = $this->db->query('select (sum(a.game_score)) as game_score, lastupdate,playedDate from
								(
									SELECT avg(gr.game_score) as game_score,count(*) as cnt, lastupdate,DATE_FORMAT(`lastupdate`,"%d") as playedDate FROM game_reports gr join category_skills sk join users u WHERE gr.gu_id = u.id and u.sid = '.$school_id.' and gr.gu_id='.$uid.' and sk.id = gr.gs_id and gr.gs_id in (SELECT id FROM category_skills where category_id=1) and  lastupdate between "'.$startdate.'" and "'.$enddate.'" AND DATE_FORMAT(lastupdate, "%Y-%m")=\''.$dateQry.'\' group by lastupdate, gr.gs_id, gr.gu_id order by gr.gs_id
								) a group by lastupdate');
								//echo $this->db->last_query(); exit;
								return $query->result_array();
								
		 }

		 public function mybspicalendarSkillChart($skillsid,$uid,$dateQry)
		 {
			  $query = $this->db->query("select AVG(gamescore) as gamescore,gs_id,playedMonth from (SELECT (AVG(game_score)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(lastupdate,'%m') as playedMonth  FROM game_reports WHERE gs_id in (59,60,61,62,63) and gu_id='".$uid."' and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and DATE_FORMAT(lastupdate, '%Y-%m')=\"".$dateQry."\"   group by gs_id,lastupdate) a1 group by gs_id");
								//echo $this->db->last_query(); exit;
								return $query->result_array();
								
		 }
		   public function myTrophiesAll($userid,$startdate,$enddate)
		 {
			 $query = $this->db->query("select trophystar.gu_id AS gu_id,extract(month from trophystar.lastupdate) AS month,sum(trophystar.ct) as totstar,trophystar.id as category from trophystar where (trophystar.lastupdate>='".$startdate."' and trophystar.lastupdate<='".$enddate."') and  trophystar.gu_id='".$userid."' group by month,trophystar.id ");
								//echo $this->db->last_query(); exit;
								return $query->result_array();
								
		 }
		  
		 function getPlaysCountPrior($r)
		 {	
			 $query = $this->db->query("select playedGamesCount as max_playedGamesCount from (SELECT  (select count(distinct(lastupdate)) from game_reports gr where gr.gs_id=g.gs_id and gr.gu_id='".$r['id']."' and gr.g_id=cpg.game_id and lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."') as playedGamesCount FROM class_plan_game cpg join games g on g.gid=cpg.game_id WHERE cpg.class_id='".$r['grade_id']."' and cpg.plan_id='".$r['gp_id']."' and g.gs_id in (59,60,61,62,63) and cpg.game_id not in(243,283,23,65,100,146,140,179,186,226,266,307,233)) as a1 order by playedGamesCount ASC limit 1");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }		 
		 	function getPlayCounts($r,$max_playedGamesCount)
		 {
			 $query = $this->db->query("select (select IFNULL(max(SessionID),0) from sk_user_game_list where userID='".$r['id']."' and planID='".$r['gp_id']."') as maxSession, (select skill_count from sk_plan_skillcount where school_ID='".$r['sid']."' and plan_ID='".$r['gp_id']."') as skillCount, (select count(*) FROM class_plan_game cpg join games g on g.gid=cpg.game_id WHERE cpg.class_id='".$r['grade_id']."' and cpg.plan_id='".$r['gp_id']."' and g.gs_id in (59,60,61,62,63) and g.gid not in(243,283,23,65,100,146,140,179,186,226,266,307,233) ) as acualGamesCount,count(*) as playedGamesCount  from (SELECT game_id,gs_id,(select count(distinct(lastupdate)) from game_reports gr where gr.gs_id=g.gs_id and gr.gu_id='".$r['id']."' and gr.g_id=cpg.game_id and lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' ) as playedGamesCount FROM class_plan_game cpg join games g on g.gid=cpg.game_id WHERE cpg.class_id='".$r['grade_id']."' and cpg.plan_id='".$r['gp_id']."' and g.gs_id in (59,60,61,62,63) and cpg.game_id not in(243,283,23,65,100,146,140,179,186,226,266,307,233)) as a1 where a1.playedGamesCount!=0 and a1.playedGamesCount>=".$max_playedGamesCount."");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }		 
		 	function getSKBspi($r)
		 {
			 $query = $this->db->query("select ROUND(avg(gamescore),2) as gamescore,gs_id from (select avg(gamescore) as gamescore ,playedMonth,gs_id from ( SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth  FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id='".$r['id']."' and  lastupdate between '".$this->session->astartdate."'and '".$this->session->aenddate."' group by gs_id , lastupdate) as a1 group by gs_id,playedMonth) as a2 group by gs_id order by gamescore asc");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		  public function updateSKGameList($r)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("update sk_user_game_list set status=1 where userID='".$r['id']."' and planID='".$r['gp_id']."'");
		//echo $this->db->last_query(); exit;
			 
		 }	
		  public function insertSKGameList($r,$maxsession,$month_array_skill,$levelid)
		 {
			 //echo 'hello'; exit;

		$query = $this->db->query("insert into sk_user_game_list(userID,planID,SessionID,weakSkills,levelid,status,created_date) values ('".$r['id']."','".$r['gp_id']."','".($maxsession)."','".implode (",", $month_array_skill)."','".implode (",", $levelid)."',0,curdate())");
		//echo $this->db->last_query(); exit;
			 
		 }	
		 	 
		 
		 public function getbspireport1($userid,$mnths)
		 {
	
		$query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore,gs_id , lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."' and DATE_FORMAT(lastupdate,'%b-%Y') in (".$mnths.") and  date(lastupdate) between '".$this->session->astartdate."' and '".$this->session->aenddate."' group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
	  
		public function getstudentplay($userid,$txtFDate,$txtTDate)
		 {
	
		$query = $this->db->query("SELECT count(*) as PalyCount,avg(game_score) as bspi,lastupdate,gs_id ,(select name from  category_skills where id=gs_id) as skillname,(select concat(fname,' ',lname) from users u where u.id=gu_id) as Name,(select username from users u where u.id=gu_id) as Username,(select concat((select classname from class c where c.id=u.grade_id),' - ',section) from users u where u.id=gu_id) as Class from game_reports where gu_id='".$userid ."' and  date(lastupdate) between '".$this->session->astartdate."' and '".$this->session->aenddate."' and  date(lastupdate) between '".$txtFDate."' and '".$txtTDate."' group by date(lastupdate),gs_id order by date(lastupdate)");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }

	public function insertsparkies($arrofinput)	 
	{  //echo "<pre>";print_r($arrofinput);exit;
		/* $stored_procedure = "CALL insertsparkies(?,?,?,?,?,?,?,?,?) ";
		$result = $this->db->query($sp,$arrofinput); 	 */
		$inDatetime=date("Y-m-d H:i:s");
	
		$query = $this->db->query("CALL insertsparkiesnew(".$arrofinput['inSID'].",".$arrofinput['inGID'].",".$arrofinput['inUID'].",'".$arrofinput['inScenarioCode']."','".$arrofinput['inTotal_Ques']."','".$arrofinput['inAttempt_Ques']."','".$arrofinput['inAnswer']."','".$arrofinput['inGame_Score']."','".$arrofinput['inPlanid']."','".$arrofinput['inGameid']."','".$inDatetime."')");
		mysqli_next_result($this->db->conn_id);
		return $query->result_array();
	}
	public function insertnewsfeeddata($arrofinput)	 
	{
		$inDatetime=date("Y-m-d H:i:s");
		
		$query = $this->db->query("CALL insertnewsfeeddata('".$arrofinput['inSID']."','".$arrofinput['inGID']."','".$arrofinput['inUID']."','".$arrofinput['inScenarioCode']."','".$arrofinput['inTotal_Ques']."','".$arrofinput['inAttempt_Ques']."','".$arrofinput['inAnswer']."','".$arrofinput['inGame_Score']."','".$arrofinput['inPlanid']."','".$arrofinput['inGameid']."','".$inDatetime."')");
		
		//return $query->result_array();
	}
	
	public function getMyCurrentSparkies($school_id,$grade_id,$userid,$startdate,$enddate)
    {
		
		$query = $this->db->query("CALL getMyCurrentSparkies(".$school_id.",".$grade_id.",".$userid.",'".$startdate."','".$enddate."')");
		return $query->result_array();

	}	
	public function getNewsFeed($school_id,$grade_id,$userid,$type,$page,$startdate,$enddate)
	{	//echo "CALL getNewsFeed(".$school_id.",".$grade_id.",".$userid.",'".$type."')";exit;
		$query = $this->db->query("CALL getNewsFeed(".$school_id.",".$grade_id.",".$userid.",'".$type."','".$startdate."','".$enddate."')");
		mysqli_next_result($this->db->conn_id);
		return $query->result_array();
	}
	public function getNewsFeedCount($school_id,$grade_id,$userid,$type,$page,$startdate,$enddate)
	{		//echo "CALL getNewsFeedCount(".$school_id.",".$grade_id.",".$userid.",'".$type."')";exit;
			$query = $this->db->query("CALL getNewsFeedCount(".$school_id.",".$grade_id.",".$userid.",'".$type."','".$startdate."','".$enddate."')");
			mysqli_next_result($this->db->conn_id);
			return $query->result_array();
	}	
	
	public function getTopSparkiesValue($startdate,$enddate,$school_ID,$grad_ID)
	{ 	
		
		$query = $this->db->query("select U_ID,points,monthName,monthNumber,monofyear,S_ID,G_ID,group_concat(studentname) as username from (select U_ID,points,monthName,monthNumber,monofyear,S_ID,G_ID,(select username from `users` where id = U_ID) as username,(select CONCAT(fname,' ',lname) from `users` where id = U_ID) as studentname from (select `a2`.`U_ID` AS `U_ID`,sum(`a2`.`Points`) AS `points`,date_format(`a2`.`Datetime`,'%b') AS `monthName`,date_format(`a2`.`Datetime`,'%m') AS `monthNumber`,year(`a2`.`Datetime`) as monofyear,a2.S_ID,a2.G_ID from user_sparkies_history `a2` where (date_format(`a2`.`Datetime`,'%Y-%m-%d') between '".$startdate."' and '".$enddate."')   group by date_format(`a2`.`Datetime`,'%m'),`a2`.`U_ID`) a1 where a1.U_ID in (select id from users where status=1 and visible=1) and a1.G_ID=".$grad_ID." and a1.S_ID=".$school_ID." and  a1.points=(select points from vv2 where vv2.monthNumber =a1.monthNumber  and CONCAT(vv2.monthNumber,'-',vv2.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and vv2.monofyear=a1.monofyear and vv2.G_ID=".$grad_ID." and vv2.S_ID=".$school_ID." ) ) as a5 group by CONCAT(monthNumber,'-',monofyear)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getTopPlayedGames($startdate,$enddate,$school_ID,$grad_ID)
	{
		
		
		
		$query = $this->db->query("select countofplayed,gu_id,monthName,monthNumber,monofyear,grad_ID,gs_ID,group_concat(studentname) as username  from (select countofplayed,gu_id,monthName,monthNumber,monofyear,grad_ID,gs_ID,(select username from `users` where id = gu_id) as username,(select CONCAT(fname,' ',lname) from `users` where id = gu_id) as studentname from (select count(`gu_id`) AS `countofplayed`,`gu_id` AS `gu_id`,date_format(`lastupdate`,'%b') AS `monthName`,date_format(`lastupdate`,'%m') AS `monthNumber`,year(lastupdate) as monofyear,(select `sid` from `users` where (`id` = `gu_id`)) AS `gs_ID`,(select `grade_id` from `users` where (`id` = `gu_id`)) AS `grad_ID` from `game_reports` where (convert(date_format(`lastupdate`,'%Y-%m-%d') using latin1) between '".$this->session->astartdate."' and '".$this->session->aenddate."' ) group by date_format(`lastupdate`,'%m'),`gu_id`) a1 where a1.gu_id in (select id from users where status=1 and visible=1) and a1.grad_ID=".$grad_ID." and a1.gs_ID=".$school_ID." and a1.countofplayed in (select countofval from vi_gameplayed v where v.monthNumber=a1.monthNumber and CONCAT(v.monthNumber,'-',v.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and v.monofyear=a1.monofyear and  v.school_id=".$school_ID." and v.grad_id=".$grad_ID.") ) as a5 group by CONCAT(monthNumber,'-',monofyear)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getTopBSPIScore($startdate,$enddate,$school_ID,$grad_ID)
	{ 	
		$query = $this->db->query("select bspi,monthName,monthNumber,monofyear,sid,gu_id,grade_id,(select GROUP_CONCAT(CONCAT(fname,' ',lname)) from users where id = gu_id) as username,classname,school_name from(select bspi,monthName,monthNumber,monofyear,sid,gu_id,grade_id,(select CONCAT(fname,' ',lname) from users where id = gu_id) as username,(select classname from class where id = grade_id)as classname,(select school_name from schools where id = sid)as school_name from(select finalscore as bspi,gu_id,monthNumber,monthName,sid,grade_id,monofyear from vii_avguserbspiscorebymon ) as a1 where a1.gu_id in (select id from users where status=1 and visible=1) and a1.grade_id=".$grad_ID." and a1.sid=".$school_ID."  and ROUND(a1.bspi,2) in (select bspi from vii_topbspiscore as vv3 where vv3.monthNumber =a1.monthNumber and  CONCAT(vv3.monthNumber,'-',vv3.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and vv3.monofyear =a1.monofyear and vv3.grade_id=".$grad_ID." and vv3.sid=".$school_ID.")) as a5 group by CONCAT(monthNumber,'-',monofyear)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getSuperAngels($startdate,$enddate,$school_ID,$grad_ID)
	{
		$query = $this->db->query("select ans,gu_id,monthName,monthNumber,monofyear,grad_ID,gs_ID,group_concat(studentname) as username from (select ans,gu_id,monthName,monthNumber,monofyear,grad_ID,gs_ID,(select username from `users` where id = gu_id) as username,(select CONCAT(fname,' ',lname) from `users` where id = gu_id) as studentname from (select sum(answer) as ans,`game_reports`.`gu_id` AS `gu_id`,date_format(`game_reports`.`lastupdate`,'%b') AS `monthName`,date_format(`game_reports`.`lastupdate`,'%m') AS `monthNumber`,year(lastupdate) as monofyear,(select `users`.`sid` from `users` where (`users`.`id` = `game_reports`.`gu_id`)) AS `gs_ID`,(select `users`.`grade_id` from `users` where (`users`.`id` = `game_reports`.`gu_id`)) AS `grad_ID` from `game_reports` where (convert(date_format(`game_reports`.`lastupdate`,'%Y-%m-%d') using latin1) between '".$this->session->astartdate."' and '".$this->session->aenddate."' ) group by date_format(`game_reports`.`lastupdate`,'%m'),`game_reports`.`gu_id`) a1 where a1.gu_id in (select id from users where status=1 and visible=1) and a1.grad_ID=".$grad_ID." and a1.gs_ID=".$school_ID." and a1.ans in (select ans from vii_topsuperangels v where v.monthNumber=a1.monthNumber and  CONCAT(v.monthNumber,'-',v.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and v.monofyear=a1.monofyear and v.gs_ID=".$school_ID." and v.grad_ID=".$grad_ID.")) as a5 group by CONCAT(monthNumber,'-',monofyear)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	
	/* Hari */
	
	public function getbadgeone($startdate,$enddate,$grad_ID,$userid,$school_ID)	 
	{
		$currentmonth = date('F');
		
		/* $query = $this->db->query("select gu_id,score,month,count(gu_id) as total from (select score, month, gu_id from (select (sum(score1)/5) as score,DATE_FORMAT(lastupdate, '%M') AS month,gu_id from (select AVG(score) as score1,lastupdate,gs_id,gu_id from (SELECT avg(game_score) as score,gu_id, gs_id,lastupdate from game_reports WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users where grade_id ='".$gradeid."' and sid='".$schoolid."') and lastupdate between '".$startdate."' AND '".$enddate."' GROUP by gu_id,gs_id,lastupdate) a1 GROUP by gu_id,gs_id,MONTH(lastupdate)) a2 group by gu_id,MONTH(lastupdate) order by score DESC) x1 group by month order by score DESC)x2 where gu_id='".$userid."' AND month!='".$currentmonth ."'"); */
		$query = $this->db->query("select bspi,monthName,monthNumber,monofyear,sid,count(gu_id) as total,grade_id from(select finalscore as bspi,gu_id,monthNumber,monthName,sid,grade_id,monofyear from vii_avguserbspiscorebymon) as a1 where a1.grade_id=".$grad_ID." and a1.sid=".$school_ID."  and a1.gu_id in(".$userid.") and ROUND(a1.bspi,2)=(select bspi from vii_topbspiscore  as vv3 where vv3.monthNumber =a1.monthNumber and  CONCAT(vv3.monthNumber,'-',vv3.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and  vv3.monofyear =a1.monofyear and vv3.grade_id=".$grad_ID." and vv3.sid=".$school_ID.")");
		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function getbadgetwo($startdate,$enddate,$grad_ID,$userid,$school_ID)	 
	{
		$currentmonth = date('F');
		/* $query = $this->db->query("select gu_id,gameplayed,month,count(gu_id) as total from (SELECT count(gu_id) as gameplayed,DATE_FORMAT(lastupdate, '%M') AS month,gu_id from game_reports WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users where grade_id ='".$gradeid."' and sid='".$schoolid."') and lastupdate between '".$startdate."' AND '".$enddate."' GROUP by gu_id, MONTH(lastupdate) order by month,gameplayed DESC) x2 where gu_id='".$userid."'  AND month!='".$currentmonth ."'");  */
		$query = $this->db->query("select countofplayed,count(gu_id) as total,monthName,monthNumber,monofyear,grad_ID,gs_ID from (select count(`gu_id`) AS `countofplayed`,`gu_id` AS `gu_id`,date_format(`lastupdate`,'%b') AS `monthName`,date_format(`lastupdate`,'%m') AS `monthNumber`,year(lastupdate) as monofyear,(select `sid` from `users` where (`id` = `gu_id`)) AS `gs_ID`,(select `grade_id` from `users` where (`id` = `gu_id`)) AS `grad_ID` from `game_reports` where (convert(date_format(`lastupdate`,'%Y-%m-%d') using latin1) between '".$this->session->astartdate."' and '".$this->session->aenddate."' ) group by date_format(`lastupdate`,'%m'),`gu_id`) a1 where a1.grad_ID=".$grad_ID." and a1.gs_ID=".$school_ID." and a1.gu_id in(".$userid.") and a1.countofplayed in (select countofval from vi_gameplayed v where v.monthNumber=a1.monthNumber and CONCAT(v.monthNumber,'-',v.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and v.monofyear=a1.monofyear  and  v.school_id=".$school_ID." and v.grad_id=".$grad_ID.")");
		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function getbadgethree($startdate,$enddate,$gradeid,$userid,$schoolid)	 
	{
		$currentmonth = date('F');
		/* $query = $this->db->query("select  gu_id,ans,month,count(gu_id) as total from (select ans,month,gu_id from (SELECT SUM(answer) as ans,DATE_FORMAT(lastupdate, '%M') AS month,gu_id from game_reports WHERE gs_id IN (59,60,61,62,63) and gu_id IN (select id from users where grade_id ='".$gradeid."' and sid='".$schoolid."') and lastupdate between '".$startdate."' AND '".$enddate."' GROUP by gu_id, MONTH(lastupdate) order by month, ans DESC) x1 group by month order by ans DESC) x2 where gu_id='".$userid."'  AND month!='".$currentmonth ."' ") ; */
		$query = $this->db->query("select ans,count(gu_id) as total,monthName,monthNumber,monofyear,grad_ID,gs_ID from (select sum(answer) as ans,`game_reports`.`gu_id` AS `gu_id`,date_format(`game_reports`.`lastupdate`,'%b') AS `monthName`,date_format(`game_reports`.`lastupdate`,'%m') AS `monthNumber`,year(lastupdate) as monofyear,(select `users`.`sid` from `users` where (`users`.`id` = `game_reports`.`gu_id`)) AS `gs_ID`,(select `users`.`grade_id` from `users` where (`users`.`id` = `game_reports`.`gu_id`)) AS `grad_ID` from `game_reports` where (convert(date_format(`game_reports`.`lastupdate`,'%Y-%m-%d') using latin1) between '".$this->session->astartdate."' and '".$this->session->aenddate."' ) group by date_format(`game_reports`.`lastupdate`,'%m'),`game_reports`.`gu_id`) a1 where a1.grad_ID=".$gradeid." and a1.gs_ID=".$schoolid." and a1.gu_id in(".$userid.") and a1.ans in (select ans from vii_topsuperangels v where v.monthNumber=a1.monthNumber and  CONCAT(v.monthNumber,'-',v.monofyear)!=CONCAT(".date('m').",'-',".date('Y').") and v.monofyear=a1.monofyear and v.gs_ID=".$schoolid." and v.grad_ID=".$gradeid.")");
		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	
	public function getthemefile()
    {
		$query = $this->db->query("select * from thememaster where status = 1 ");
		return $query->result_array();
	}
	
	public function get_user_themefile($userid)
    {
		$query = $this->db->query("select usertheme from users where id = '".$userid."' ");
		return $query->result_array();
		 
	}
	
	public function updatethemefile($filename,$userid)
    {
	$query = $this->db->query("UPDATE users SET usertheme='".$filename."' where id = '".$userid."' ");
 
	}
	public function getOverallSparkyTopper($startdate,$enddate,$school_ID,$grad_ID)
	{ 	
		$query = $this->db->query("select U_ID,points,S_ID,G_ID,(select GROUP_CONCAT(studentname) from users where id = U_ID) as username,classname,GROUP_CONCAT(school_name) as school_name from 

(select U_ID,points,monthName,monthNumber,S_ID,G_ID,(select username from users where id = U_ID) as username,(select CONCAT(fname,' ',lname) from users where id = U_ID) as studentname,(select classname from class where id = G_ID)as classname,(select school_name from schools where id = S_ID)as school_name from 

(select a2.U_ID AS U_ID,sum(a2.Points) AS points,date_format(a2.Datetime,'%b') AS monthName,date_format(a2.Datetime,'%m') AS monthNumber,a2.S_ID,a2.G_ID from user_sparkies_history a2 where a2.S_ID in (select schools.id from schools where visible = 1) and a2.U_ID in (select id from users where status=1 and visible=1) group by a2.U_ID) a1 

where a1.points in (select points from vi_overallcrownytoppers as vv3 where a1.G_ID=vv3.G_ID )) as a5 group by classname");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getOverallBspiTopper($startdate,$enddate,$school_ID,$grad_ID)
	{ 	
		$query = $this->db->query("select bspi,sid,gu_id,grade_id,(select GROUP_CONCAT(studentname) from users where id = gu_id) as username,classname,GROUP_CONCAT(school_name) as school_name from

(select bspi,sid,gu_id,grade_id,(select username from users where id = gu_id) as username,(select CONCAT(fname,' ',lname) from users where id = gu_id) as studentname,(select classname from class where id = grade_id)as classname,(select school_name from schools where id = sid)as school_name from

(select finalscore as bspi,v1.gu_id,v1.sid,v1.grade_id from vii_avguserbspiscore as v1
join users u on u.id = v1.gu_id where u.sid in (select id from schools where visible = 1) and u.status=1 and visible=1) as a1

 where a1.bspi in (select bspi from vii_overallbspitoppers as vv3 where a1.grade_id=vv3.grade_id )) as a5 group by classname");
	
		return $query->result_array();
	}
	 public function getClassPerformace_data($schoolid,$gradeid,$section,$tablename)
	{
 
		$query = $this->db->query("select rowNumber,id, name,lname,avatarimage,bspi from (select  (@cnt := @cnt + 1) AS rowNumber,id, fname as name,lname,avatarimage, IF(avgbspiset1 IS NULL,0,avgbspiset1) as bspi from (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM ".$tablename." WHERE gs_id in (59,60,61,62,63) and lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$section."' and status=1 and visible=1  ORDER BY avgbspiset1 DESC ) as a5 CROSS JOIN (SELECT @cnt := 0) AS dummy) as b1 order by bspi asc");
		//echo $this->db->last_query(); 
		//exit;
			return $query->result_array();
	}
	public function GetBadgeData($school_id,$grade_id,$startdate,$enddate)
	{
		$query = $this->db->query("CALL GetBadgeData('".$school_id."','".$grade_id."','".$startdate."','".$enddate."')");
		//return $query->result_array();
	}
	
	public function termsandcondition($terms,$username)
	{
		$query = $this->db->query("UPDATE users SET agreetermsandservice = '".$terms."' WHERE username='".$username."' ");
	}
	
	public function getMemoryRange()
	{
		$query = $this->db->query("select max(totalCount) as memory,CONCAT(startRange,'-',endRange) as rangeval from (select r.startRange,r.endRange,count(a5.gamescore) totalCount
from(select gamescore,gs_id,lastupdate,playedMonth,gu_id from (SELECT (AVG(game_score)) as gamescore,gs_id,lastupdate,DATE_FORMAT(lastupdate,'%m') as playedMonth,gu_id  FROM game_reports join users as u on u.id=gu_id WHERE gs_id in(59) and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and sid='".$this->session->school_id."' and grade_id='".$this->session->game_grade."' group by gu_id,gs_id,lastupdate) as a1 where gamescore!=0 group by  gs_id,lastupdate order by gamescore ASC)a5 inner join range_values r on a5.gamescore >startrange  and a5.gamescore <=endrange group by r.startRange, r.endRange order by startRange asc )a2 group by totalCount order by totalCount desc limit 1");
		return $query->result_array();
	}
	public function getVisualProcessingRange()
	{
		$query = $this->db->query("select max(totalCount) as memory,CONCAT(startRange,'-',endRange) as rangeval from (select r.startRange,r.endRange,count(a5.gamescore) totalCount
from(select gamescore,gs_id,lastupdate,playedMonth,gu_id from (SELECT (AVG(game_score)) as gamescore,gs_id,lastupdate,DATE_FORMAT(lastupdate,'%m') as playedMonth,gu_id  FROM game_reports  join users as u on u.id=gu_id WHERE gs_id in(60) and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and sid='".$this->session->school_id."' and grade_id='".$this->session->game_grade."' group by gu_id,gs_id,lastupdate) as a1 where gamescore!=0 group by  gs_id,lastupdate order by gamescore ASC)a5 inner join range_values r on a5.gamescore >startrange  and a5.gamescore <=endrange group by r.startRange, r.endRange order by startRange asc )a2 group by totalCount order by totalCount desc limit 1");
		return $query->result_array();
	}
	public function getFocusAttentionRange()
	{
		$query = $this->db->query("select max(totalCount) as memory,CONCAT(startRange,'-',endRange) as rangeval from (select r.startRange,r.endRange,count(a5.gamescore) totalCount
from(select gamescore,gs_id,lastupdate,playedMonth,gu_id from (SELECT (AVG(game_score)) as gamescore,gs_id,lastupdate,DATE_FORMAT(lastupdate,'%m') as playedMonth,gu_id  FROM game_reports  join users as u on u.id=gu_id WHERE gs_id in(61) and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and sid='".$this->session->school_id."' and grade_id='".$this->session->game_grade."' group by gu_id,gs_id,lastupdate) as a1 where gamescore!=0 group by  gs_id,lastupdate order by gamescore ASC)a5 inner join range_values r on a5.gamescore >startrange  and a5.gamescore <=endrange group by r.startRange, r.endRange order by startRange asc )a2 group by totalCount order by totalCount desc limit 1");
		return $query->result_array();
	}
	public function getProblemSolvingRange()
	{
		$query = $this->db->query("select max(totalCount) as memory,CONCAT(startRange,'-',endRange) as rangeval from (select r.startRange,r.endRange,count(a5.gamescore) totalCount
from(select gamescore,gs_id,lastupdate,playedMonth,gu_id from (SELECT (AVG(game_score)) as gamescore,gs_id,lastupdate,DATE_FORMAT(lastupdate,'%m') as playedMonth,gu_id  FROM game_reports  join users as u on u.id=gu_id WHERE gs_id in(62) and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and sid='".$this->session->school_id."' and grade_id='".$this->session->game_grade."' group by gu_id,gs_id,lastupdate) as a1 where gamescore!=0 group by  gs_id,lastupdate order by gamescore ASC)a5 inner join range_values r on a5.gamescore >startrange  and a5.gamescore <=endrange group by r.startRange, r.endRange order by startRange asc )a2 group by totalCount order by totalCount desc limit 1");
		return $query->result_array();
	}
	public function getLinguisticsRange()
	{
		$query = $this->db->query("select max(totalCount) as memory,CONCAT(startRange,'-',endRange) as rangeval from (select r.startRange,r.endRange,count(a5.gamescore) totalCount
from(select gamescore,gs_id,lastupdate,playedMonth,gu_id from (SELECT (AVG(game_score)) as gamescore,gs_id,lastupdate,DATE_FORMAT(lastupdate,'%m') as playedMonth,gu_id  FROM game_reports join users as u on u.id=gu_id WHERE gs_id in(63) and  lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' and sid='".$this->session->school_id."' and grade_id='".$this->session->game_grade."' group by gu_id,gs_id,lastupdate) as a1 where gamescore!=0 group by  gs_id,lastupdate order by gamescore ASC)a5 inner join range_values r on a5.gamescore >startrange  and a5.gamescore <=endrange group by r.startRange, r.endRange order by startRange asc )a2 group by totalCount order by totalCount desc limit 1");
		return $query->result_array();
	}
	public function checkbandwidthisexist($schoolID)
	{
		$query = $this->db->query("CALL checkbandwidthisexist(".$schoolID.")");
		mysqli_next_result($this->db->conn_id);
		return $query->result_array();
	}
	public function insertbandwidth($schoolID,$user_id,$Bps,$Kbps,$Mbps)
	{
		$query = $this->db->query("CALL insertbandwidth(".$schoolID.",".$user_id.",'".$Bps."','".$Kbps."','".$Mbps."')");
		mysqli_next_result($this->db->conn_id);
	}
	public function getPlayedSkillCount($user_id)
	{
		$query = $this->db->query("SELECT COUNT(id),gs_id FROM `gamedata` WHERE gu_id =".$user_id." and lastupdate=CURDATE() group by gs_id ");
		return $query->result_array();
	}
	public function IsSkillkitExist($userid,$plan_id)
	{
		$query = $this->db->query("select count(ID) as isenable from sk_user_game_list where userID=".$userid." and planID=".$plan_id." ");
		return $query->result_array();
	}
	public function checkscheduledays($gradeid,$section,$schoolid)
	{
	$curdate=date('Y-m-d');
	$curday = date('l', strtotime($curdate)); 
	$gradecolumn = strtolower($curday).'_'."grade";
	$sectioncolumn = strtolower($curday).'_'."section";

	$query = $this->db->query("select case when count(*)>0 THEN 1 else 0 END as scheduleday from schools_period_schedule where academic_id=20 and school_id='".$schoolid."' and ".$gradecolumn." = (select REPLACE(classname,'Grade ','') from class where id ='".$gradeid."') and ".$sectioncolumn." = '".$section."' and status = 'Y' and school_id not in(select school_id from schools_leave_list where leave_date='".$curdate."' and school_id='".$schoolid."' )");
	//	 echo $this->db->last_query(); exit;
	return $query->result_array();
	} 

	 public function getgameid($gamename)
	{
		$query = $this->db->query("select gid as gid from games where game_html='".$gamename."'  order by gid ASC LIMIT 1");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function checkgame($gameid,$gradeid,$puzzle_cycle,$userid)
	{
		/*$curdate=date('Y-m-d');
		 $schoolid = $this->session->school_id;
		$gradeid = $this->session->game_grade;
		$section = $this->session->section;
		$userid = $this->session->user_id; */
		
		/* $query = $this->db->query("select count(distinct gid) as gameid,(select count(gu_id) from game_reports where gu_id=".$userid." and lastupdate='".$curdate."' and gs_id IN(59,60,61,62,63)) as playedgamescount from rand_selection where gid IN (".$gameid.") and created_date='".$curdate."' and grade_id='".$gradeid."'  and school_id='".$schoolid."'  "); */
		
		$query = $this->db->query("select gid as gameid,puzzle_cycle,(select count(gu_id) from game_reports as gd where gu_id=".$userid." and gd.puzzle_cycle=puzzle_cycle and g_id=".$gameid.") as playedgamescount from rand_selection where gid=".$gameid." and grade_id='".$gradeid."' and user_id=".$userid." and played='N' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
		
	public function insert_login_log($userid,$sessionid,$ip,$country,$region,$city,$isp,$browser,$status)
		{
			$query = $this->db->query('INSERT INTO user_login_log(userid,sessionid,created_date,lastupdate,logout_date,ip,country,region,city,browser,isp,status)VALUES("'.$userid.'","'.$sessionid.'",now(),now(),now(), "'.$ip.'","'.$country.'","'.$region.'","'.$city.'","'.$browser.'","'.$isp.'","'.$status.'")');
			return $query;
			
		}
 public function update_login_log($userid,$sessionid)
		{
			$query = $this->db->query('update user_login_log set lastupdate=now() where userid="'.$userid.'" and sessionid="'.$sessionid.'"');
			return $query;
			
		}
		public function update_logout_log($userid,$sessionid)
		{	
			$query = $this->db->query('update user_login_log set lastupdate=now(),logout_date=now() where userid="'.$userid.'" and sessionid="'.$sessionid.'"');
			return $query;
			
		}
		public function CheckSkillkitexist($userid)
		{
			$query = $this->db->query("select count(ID) as isavailable from sk_user_game_list where userID=".$userid." and DATE(created_date)=CURDATE()");
			return $query->result_array();
		}
		public function getUserPlayedDayscount($userid)
		{
			$query = $this->db->query("select count(distinct(lastupdate)) as playedDate from game_reports gr where gr.gu_id='".$userid."' and lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."' ");
			return $query->result_array();
		}
	public function getConfigNoofDaysPlay()
	{
		$query = $this->db->query("select value from config_master where code='SKILLKIT_NODAYSPLAY' and status='Y'");
		return $query->result_array();
	}
	
	/*  PopUp Code Start */
	public function CheckisUser($username,$password)
	{
		$query = $this->db->query("select count(id) as isUser,id,sid,grade_id,(select classname from class where id=grade_id) as gradename,section,username,portal_type FROM users WHERE username='".$username."' AND password=SHA1(CONCAT(salt1,'".$password."',salt2)) AND status=1 and visible=1");
		
		return $query->result_array();
	}	
	public function CheckUserStatus($userid)
	{
		$query = $this->db->query("select count(id) as userstatus FROM isuser_log  WHERE User_id='".$userid."' and Confirmation_type='1' order by id DESC");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function CheckisScheduleday($gradename,$section,$sid)
	{	$curtime=date('H:i:s');
		$currentdaygrade=strtolower(date ('l')."_grade");$currentdaysection=strtolower(date ('l')."_section");
		$query = $this->db->query("SELECT count(schedule_id) as isschedule FROM schools_period_schedule WHERE ".$currentdaygrade."='".ltrim($gradename,'Grade ')."' and ".$currentdaysection."='".$section."' and school_id='".$sid."' and academic_id=20 and '".$curtime."' between start_time and end_time");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function insertuserlog($userid,$Login_type,$Confirmation_type)
	{	$curdateandtime=date('Y-m-d h:i:s');
		$this->db->query("INSERT into isuser_log(User_id,Login_type,Confirmation_type,Logged_datetime,Org_userid)values('".$userid."','".$Login_type."','".$Confirmation_type."','".$curdateandtime."','')");
		return $this->db->insert_id();
		//echo $this->db->last_query(); exit;
	}
	public function fetchrelateduser($userid,$username,$grade_id,$section,$sid)
	{	
		$query =$this->db->query("SELECT username,id FROM users WHERE username like '%".$username."%' and grade_id='".$grade_id."' and section='".$section."' and sid='".$sid."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	/*  PopUp Code end */
	public function getgameid_SK($gamename)
	{
		$query = $this->db->query("select ID as gid from sk_games where game_html='".$gamename."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	/* -------------------------------------------------------------------------------
		New Overall Topper Concept 
	*/
	public function InsertOverallSparkyToppernew()
	{
		$query = $this->db->query("INSERT into overalltoppers(userid,gradeid,sid,value,type,duedate,created_on)
		select U_ID,G_ID,S_ID,points,'CT',CURDATE(),NOW() from (select U_ID,points,monthName,monthNumber,S_ID,G_ID from 
(select a2.U_ID AS U_ID,sum(a2.Points) AS points,date_format(a2.Datetime,'%b') AS monthName,date_format(a2.Datetime,'%m') AS monthNumber,a2.S_ID,a2.G_ID from user_sparkies_history a2 where a2.S_ID in (select schools.id from schools where visible = 1) and a2.U_ID in (select id from users where status=1 and visible=1) group by a2.U_ID) a1
where a1.points in (select points from vi_overallcrownytoppers as vv3 where a1.G_ID=vv3.G_ID )) as a5 group by G_ID");
		//echo $this->db->last_query(); exit;
		
	}
	public function InsertOverallBspiToppernew()
	{
		$query = $this->db->query("INSERT into overalltoppers(userid,gradeid,sid,value,type,duedate,created_on) 
		select gu_id,grade_id,sid,bspi,'BT',CURDATE(),NOW() from (select bspi,sid,gu_id,grade_id from
(select finalscore as bspi,v1.gu_id,v1.sid,v1.grade_id from vii_avguserbspiscore as v1
join users u on u.id = v1.gu_id where u.sid in (select id from schools where visible = 1) and u.status=1 and visible=1) as a1
 where a1.bspi in (select bspi from vii_overallbspitoppers as vv3 where a1.grade_id=vv3.grade_id )) as a5 group by grade_id");
	
		
	}
	public function CheckTodaydataExist()
	{
		$query = $this->db->query("select count(id) as isexist from overalltoppers where duedate=CURDATE()");
		return $query->result_array();
	}
	public function ClearToppersData()
	{
		$query = $this->db->query("TRUNCATE TABLE overalltoppers");
		//return $query->result_array();
	}
	public function getOverallSparkyToppernew($startdate,$enddate,$school_ID,$grad_ID)
	{
		$query = $this->db->query("select GROUP_CONCAT(username) as username,GROUP_CONCAT(studentname) as studentname,classname,GROUP_CONCAT(school_name) as school_name,points from(Select gradeid,(select username from users where id=userid) as username,(select CONCAT(fname,' ',lname) from users where id=userid) as studentname,(select classname from class where id =gradeid)as classname,(select school_name from schools where id = sid)as school_name,value as points from overalltoppers where type='CT')c1 group by gradeid");
		return $query->result_array();
	}
	public function getOverallBspiToppernew($startdate,$enddate,$school_ID,$grad_ID)
	{
		$query = $this->db->query("select GROUP_CONCAT(username) as username,GROUP_CONCAT(studentname) as studentname,classname,GROUP_CONCAT(school_name) as school_name,bspi from(Select gradeid,(select username from users where id=userid) as username,(select CONCAT(fname,' ',lname) from users where id=userid) as studentname,(select classname from class where id =gradeid)as classname,(select school_name from schools where id = sid)as school_name,value as bspi from overalltoppers where type='BT')b1 group by gradeid");
		return $query->result_array();
	}
	public function Schools_Wise_Period_Insert($startdate,$enddate)
	{
		$query = $this->db->query("CALL Schools_Wise_Period_Insert('".$startdate."','".$enddate."')");
		mysqli_next_result($this->db->conn_id);
	}
/* ---------------------- Leader Board API Insert Start -----------------------*/
	public function CheckTodayLeaderboardDataExist($sid,$gid,$monthno)
	{
		$query = $this->db->query("select count(id) as isexist from leaderboard where year=YEAR(CURDATE()) and monthnumber in(".$monthno.") and sid=".$sid." and gradeid=".$gid." ");
		return $query->result_array();
	}
	public function GetSchoolDetails()
	{
		$query = $this->db->query("select id,school_name,(select group_concat(class_id) from skl_class_plan where school_id=s.id) as grade from schools as s where status=1 and active=1 and visible=1");
		return $query->result_array();
	}
	public function InsertTopSparkiesValue($startdate,$enddate,$school_ID,$grad_ID)
	{
		$query = $this->db->query("INSERT into leaderboard(sid,gradeid,year,monthname,monthnumber,userid,value,type,Created_on)select S_ID,G_ID,Year,monthName,monthNumber,GROUP_CONCAT(U_ID) as U_ID,points,'CB',NOW() from (select U_ID,points,monthName,monthNumber,Year,S_ID,G_ID,(select username from users where id = U_ID) as username,(select CONCAT(fname,' ',lname) from users where id = U_ID) as studentname from (select a2.U_ID AS U_ID,sum(a2.Points) AS points,date_format(a2.Datetime,'%b') AS monthName,date_format(Datetime,'%Y') AS Year,date_format(a2.Datetime,'%m') AS monthNumber,a2.S_ID,a2.G_ID from user_sparkies_history a2 where (date_format(a2.Datetime,'%Y-%m-%d') between '".$startdate."' and '".$enddate."')   group by date_format(a2.Datetime,'%m'),a2.U_ID) a1 where a1.U_ID in (select id from users where status=1 and visible=1) and a1.G_ID=".$grad_ID." and a1.S_ID=".$school_ID." and  a1.points >= (select value from config_master where code='CB' and status='Y') ) as a5 group by monthNumber");
		//echo $this->db->last_query(); exit;
		
	}
	public function InsertTopPlayedGames($startdate,$enddate,$school_ID,$grad_ID)
	{
		$query = $this->db->query("INSERT into leaderboard(sid,gradeid,year,monthname,monthnumber,userid,value,type,Created_on)select gs_ID,grad_ID,Year,monthName,monthNumber,group_concat(gu_id),countofplayed,'SGB',NOW()  from (select countofplayed,gu_id,monthName,monthNumber,Year,grad_ID,gs_ID,(select username from users where id = gu_id) as username,(select CONCAT(fname,' ',lname) from users where id = gu_id) as studentname from (select count(gu_id) AS countofplayed,gu_id AS gu_id,date_format(lastupdate,'%b') AS monthName,date_format(lastupdate,'%m') AS monthNumber,date_format(lastupdate,'%Y') AS Year,(select sid from users where (id = gu_id)) AS gs_ID,(select grade_id from users where (id = gu_id)) AS grad_ID from game_reports where (convert(date_format(lastupdate,'%Y-%m-%d') using latin1) between '".$startdate."' and '".$enddate."' ) group by date_format(lastupdate,'%m'),gu_id) a1 where a1.gu_id in (select id from users where status=1 and visible=1) and a1.grad_ID=".$grad_ID." and a1.gs_ID=".$school_ID." and a1.countofplayed >= (select value from config_master where code='SGB' and status='Y') ) as a5 group by monthNumber");
		//echo $this->db->last_query(); exit;
	}
	public function InsertTopBSPIScore($startdate,$enddate,$school_ID,$grad_ID)
	{ 	
		$query = $this->db->query("INSERT into leaderboard(sid,gradeid,year,monthname,monthnumber,userid,value,type,Created_on)select sid,grade_id,date_format('".$startdate."','%Y') AS Year,DATE_FORMAT(STR_TO_DATE(monthnumber, '%m'), '%b') as monthName,monthNumber,GROUP_CONCAT(gu_id),bspi,'SBB',NOW() from
(select bspi,monthName,monthNumber,sid,gu_id,grade_id,(select CONCAT(fname,' ',lname) from users where id = gu_id) as username,(select classname from class where id = grade_id)as classname,(select school_name from schools where id = sid)as school_name from(select finalscore as bspi,gu_id,monthNumber,monthName,sid,grade_id from vii_avguserbspiscorebymon where monthNumber=DATE_FORMAT('".$startdate."', '%m')) as a1 where a1.gu_id in (select id from users where status=1 and visible=1) and a1.grade_id=".$grad_ID." and a1.sid=".$school_ID."  and ROUND(a1.bspi,2) >= (select value from config_master where code='SBB' and status='Y')) as a5 group by monthNumber");
		//echo $this->db->last_query(); exit;
	}
	public function InsertSuperAngels($startdate,$enddate,$school_ID,$grad_ID)
	{
		$query = $this->db->query("INSERT into leaderboard(sid,gradeid,year,monthname,monthnumber,userid,value,type,Created_on)select gs_ID,grad_ID,Year,monthName,monthNumber,group_concat(gu_id),ans,'SAB',NOW() as username from (select ans,gu_id,monthName,monthNumber,Year,grad_ID,gs_ID,(select username from users where id = gu_id) as username,(select CONCAT(fname,' ',lname) from users where id = gu_id) as studentname from 
(select sum(answer) as ans,game_reports.gu_id AS gu_id,date_format(game_reports.lastupdate,'%b') AS monthName,date_format(game_reports.lastupdate,'%m') AS monthNumber,date_format(game_reports.lastupdate,'%Y') AS Year,(select users.sid from users where (users.id = game_reports.gu_id)) AS gs_ID,(select users.grade_id from users where (users.id = game_reports.gu_id)) AS grad_ID from game_reports where (convert(date_format(game_reports.lastupdate,'%Y-%m-%d') using latin1) between '".$startdate."' and '".$enddate."' ) group by date_format(game_reports.lastupdate,'%m'),game_reports.gu_id) a1 where a1.gu_id in (select id from users where status=1 and visible=1) and a1.grad_ID=".$grad_ID." and a1.gs_ID=".$school_ID." and a1.ans>=(select value from config_master where code='SAB' and status='Y')) as a5 group by monthNumber");
		//echo $this->db->last_query(); exit;
	}
/*------------------- New Leader Board Concept Qry -------------------------*/
public function getTopBadge($schoolid,$gradid,$type)
{
	$query = $this->db->query("SELECT monthnumber,year as monofyear,gradeid,group_concat(concat(fname,'',lname)) as username,type FROM leaderboard as l join users as u on find_in_set(u.id,userid) where l.sid='".$schoolid."' and l.gradeid='".$gradid."' and type='".$type."' group by l.id ");
	return $query->result_array();
}

public function getUserBadgeCount($sid,$gradeid,$userid)
{
	$query = $this->db->query("SELECT SUM(CASE WHEN type='SAB' THEN 1 else 0 END ) as sabbadge, SUM(CASE WHEN type='SBB' THEN 1 else 0 END ) as sbbbadge, SUM(CASE WHEN type='SGB' THEN 1 else 0 END ) as sgbbadge from leaderboard where gradeid=".$gradeid." and sid=".$sid." and userid=".$userid." ");
	return $query->result_array();
}
/*-------------------30 mins Time over concept-------------------------*/

	public function getSumofUserUsedTime($userid,$Todaydate)
	{
		$query =$this->db->query("SELECT SUM(TimeLoggedIn) as LoggedIntime from(SELECT TIMESTAMPDIFF(SECOND,created_date,lastupdate) AS TimeLoggedIn FROM user_login_log WHERE userid=".$userid." and date(created_date)='".$Todaydate."') as a2 ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getMaxTimeofPlay($ChildID)
	{
		$query = $this->db->query("select time_limit as value from users where id=".$ChildID." ");
		
		return $query->result_array();
	}
	public function TodayTimerInsert($userid)
	{
		$query = $this->db->query("Insert into userplaytime(userid,expiredon,expireddatetime)values(".$userid.",CURDATE(),NOW())");
	}
	public function IsTotayTimerExist($userid)
	{
		$query = $this->db->query("Select count(id) as isexist from userplaytime where userid=".$userid." and expiredon=CURDATE()");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
/*-------------------30 mins Time over concept End------------------------*/
	
	public function getCurrentDaySkill($userid)
	{
		$query = $this->db->query("select CASE 
WHEN playedcount%5=0 THEN 59
WHEN playedcount%5=1 THEN 60
WHEN playedcount%5=2 THEN 61
WHEN playedcount%5=3 THEN 62
WHEN playedcount%5=4 THEN 63 
END as currentdayskillid from(select count(DISTINCT lastupdate) as playedcount from game_reports where gu_id=".$userid.")a2");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getUserCurrentBspi($userid)
	{
		$query = $this->db->query("Select sum(game_score) as bspi from game_reports as gr join users u on gr.gu_id=u.id	WHERE u.id=".$userid." and gr.lastupdate between u.startdate and u.enddate");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function datewisescore($userid)
	{
		$query = $this->db->query("SELECT sum(gr.game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=".$userid.") as TotScore,DATE_FORMAT(gr.lastupdate, '%d-%m-%Y')  as Playeddate FROM game_reports gr join users u on gr.gu_id=u.id	WHERE u.id=".$userid." and gr.lastupdate between (select startdate from users where id=".$userid.") and (select enddate from users where id=".$userid.") group by lastupdate");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
/*-------------------------------------- Challenge Question --------------------------- */
	public function checkGameisAvailable($plan_id,$game_grade,$userid)
	{
		$query = $this->db->query("select count(gid) as isavailable from rand_selection_cq where gp_id='".$plan_id."' and grade_id='".$game_grade."' and user_id='".$userid."' and date(created_date)=CURDATE() ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getChallengeQuestion($plan_id,$game_grade,$userid)
	{
		$query = $this->db->query("Select gid from games_cq where grade_id='".$game_grade."' and gp_id='".$plan_id."' and gstatus=1 and gid NOT IN(select gid from rand_selection_cq where gp_id='".$plan_id."' and grade_id='".$game_grade."' and user_id='".$userid."' and playedstatus=1) GROUP BY  gid ORDER BY RAND() LIMIT 1");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getTodayChallengeQuestion($plan_id,$game_grade,$userid)
	{
		$query = $this->db->query("Select gid,gname,path,img_path,game_html from games_cq where gid IN(select gid from rand_selection_cq where gp_id='".$plan_id."' and grade_id='".$game_grade."' and user_id='".$userid."' and date(created_date)=CURDATE() and playedstatus=0)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function insertRandomGames_CQ($gameid,$game_plan_id,$game_grade,$school_id,$user_id)
	{ 
		$query = $this->db->query("INSERT INTO rand_selection_cq SET gid = '".$gameid."', gp_id = '".$game_plan_id."', grade_id = '".$game_grade."',school_id = '".$school_id."',user_id='".$user_id."',created_date =NOW() "); 
	}
	public function getChallengeQuestionVisible($userid)
	{
		$query = $this->db->query("Select (select max(cast(game_score as unsigned)) from gamedata where gu_id='".$userid."' and lastupdate=CURDATE() group by lastupdate) as topscore,(select count(id) from gamedata where gu_id='".$userid."' and lastupdate=CURDATE() group by lastupdate) as playedcount,(select count(id) as playedstatus from gamedata_cq where gu_id='".$userid."' and date(lastupdate)=CURDATE()) as playedstatus ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function insertone_CQ($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val)
	{
		$query = $this->db->query("insert into gamedata_cq(gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."')");
	}
	public function UpdateCQGames($userid,$gameid)
	{
		$query = $this->db->query("UPDATE rand_selection_cq SET playedstatus =1 WHERE user_id='".$userid."' and gid='".$gameid."' and date(created_date)=CURDATE() "); 
	}
	public function InsertCQGames($userid,$gameid)
	{
		$query = $this->db->query("Insert into user_challenge_table(userid,challengeid)values('".$userid."','".$gameid."') "); 
	}
	public function MyCurrentRank($userid,$grade)
	{
		$query = $this->db->query("select (select position from (select *,@rownum := @rownum + 1 AS position from sc_usertotgamescore JOIN (SELECT @rownum := 0) r order by score DESC )as a1 where gu_id='".$userid."') as overallpos,
		(select position from (select *,@rownum := @rownum + 1 AS position from sc_usertotgamescore JOIN (SELECT @rownum := 0) r where grade_id='".$grade."' order by score DESC )as a1 where gu_id='".$userid."') as gradewisepos  ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getCQisplayed($userid)
	{
		$query = $this->db->query("select playedstatus from rand_selection_cq where user_id='".$userid."' and date(created_date)=CURDATE()");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function getgameid_CQ($gamename)
	{
		$query = $this->db->query("select group_concat(gid) as gid from games_cq where game_html='".$gamename."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function checkgame_CQ($gameid)
	{
		$curdate=date('Y-m-d');
		$schoolid = $this->session->school_id;
		$gradeid = $this->session->game_grade;
		$section = $this->session->section;
		
		$query = $this->db->query("select count(distinct gid) as gameid from rand_selection_cq where gid IN (".$gameid.") and date(created_date)='".$curdate."' and grade_id='".$gradeid."'  and school_id='".$schoolid."'  ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getplayeddates($userid)
	{
		$query = $this->db->query("Select lastupdate as `date`,1 as badge from gamedata where gu_id='".$userid."' group by lastupdate");
	//	echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getUserPuzzlesDetails($userid)
	{
		$query = $this->db->query("SELECT ROUND((COALESCE(SUM(gtime)/60,0)),0) as MinutesTrained  , COALESCE(SUM(answer),0) as PuzzlesSolved, COALESCE(SUM(attempt_question),0) as PuzzlesAttempted,count(distinct lastupdate) as UniqueGames,(select count(gu_id) from gamedata_cq where gu_id='".$userid."') as cqplayedcount  FROM game_reports gr join users u on gr.gu_id=u.id	WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getTrainingCalendarData($userid,$curdate)
	{
		$query = $this->db->query("SELECT ROUND((SUM(gtime)/60),0) as MinutesTrained  , SUM(answer) as PuzzlesSolved, SUM(attempt_question) as PuzzlesAttempted,(select gname from games where gid=gr.g_id) as Gname,count(gu_id) as playedcount, SUM(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id='".$userid."' and lastupdate='".$curdate."') as TotScore,(select count(gu_id) from gamedata_cq where gu_id='".$userid."' and lastupdate='".$curdate."') as Ischallengeplayed,(select SUM(Points) from user_sparkies_history where U_ID='".$userid."' and date(Datetime)='".$curdate."') as Crownies,MAX(game_score) as maxscore FROM game_reports gr join users u on gr.gu_id=u.id WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate and lastupdate='".$curdate."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	/*............Challenge Question Functions Start............*/
	
	public function getQuesList($userid)
	{
		$query = $this->db->query("SELECT * FROM playedQues WHERE userid = $userid");
		return $query->result_array();
	}
	
	public function getQuesInfo($sno)
	{
		$query = $this->db->query("SELECT * FROM challenge WHERE sno = $sno");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function gradewiseList($grade,$userid)
	{
		$query = $this->db->query("SELECT group_concat(sno) as qnos FROM challenge  WHERE grade <= $grade and sno NOT IN(SELECT challengeid FROM user_challenge_table WHERE userid = $userid)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	/*............Challenge Question Functions End............*/
	/*AVATAR IMAGE UPDATE*/
	public function avatarupdate($userid,$avatarimg)
	{
		$query = $this->db->query("update users set avatarimage='".$avatarimg."'  WHERE id ='".$userid."'");
		//echo $this->db->last_query(); exit;
	}
	
	public function maxbspiscore()
	{
		$query = $this->db->query("select min(DISTINCT(bspi)) as val from (Select u.id, sum(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=u.id) as bspi, CONCAT(fname,'',lname) as name, (select classname from class where id=u.grade_id) as grade from game_reports gr 
		JOIN users u ON gr.gu_id=u.id where status=1 and visible=1 and isdemo=0 group by u.id order by bspi DESC) z1");
		//echo $this->db->last_query();  exit;
		return $query->result_array();
	}
	
	public function homepagetopperslist($minscore)
	{
		$query = $this->db->query("select * from (Select sum(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=u.id) as bspi,CONCAT(fname,' ',lname) as name,avatarimage,(select classname from class where id=u.grade_id) as grade,gender from game_reports gr 
		JOIN users u ON gr.gu_id=u.id where status=1 and visible=1 and isdemo=0 group by u.id order by bspi DESC )z1 where bspi>='".$minscore."'");
		//echo $this->db->last_query();  exit;
		return $query->result_array();
	}
	
	public function MyPercentileRank($userid,$gradeid,$org)
	{
		if($org!=''){$gwhere="org_id=".$org;}else{$gwhere="1=1";}
		
		$query = $this->db->query("select 
(select count(gu_id) from  sc_usertotgamescore where grade_id=".$gradeid." and ".$gwhere.") as totusercount,
(select score from  sc_usertotgamescore where gu_id=".$userid." and grade_id=".$gradeid." and ".$gwhere." ) as Mytotscore,
(select count(gu_id) from  sc_usertotgamescore where score < Mytotscore and gu_id!=".$userid." and grade_id=".$gradeid." and ".$gwhere.") as belowusercount,
(select count(gu_id) from  sc_usertotgamescore where score=Mytotscore and grade_id=".$gradeid." and ".$gwhere.") as samescoreusercount");
//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function deleteMisMatchRandomGames($catid,$game_plan_id,$game_grade,$uid,$school_id,$del_where)
	{ 
		$query = $this->db->query("delete from rand_selection where gc_id = '".$catid."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' and user_id='".$uid."' ".$del_where);
	}
	/*..... Badges Concept ........*/
	public function getTopScorePosition($userid)
	{	$date=date('Y-m-d');
		$org_id=$this->session->org;
		$grade_id=$this->session->game_grade;
		/* $query = $this->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,score,FIND_IN_SET(score,(SELECT GROUP_CONCAT( score ORDER BY score DESC )FROM sc_TopScorer )) AS position FROM sc_TopScorer where gu_id='".$userid."' "); */
		
		$query = $this->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,score,position from (select gu_id,grade_id,gp_id,sid,section,username,fname,score,lastupdate,FIND_IN_SET(score,(SELECT GROUP_CONCAT( score ORDER BY score DESC )FROM sc_topscorer where org_id='".$org_id."' and grade_id='".$grade_id."' )) AS position FROM sc_topscorer where org_id='".$org_id."' and grade_id='".$grade_id."') as a1 where gu_id='".$userid."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getMaxPuzzlesAttempted($userid)
	{	$date=date('Y-m-d');
		$org_id=$this->session->org;
		$grade_id=$this->session->game_grade;
		/* $query = $this->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,PuzzlesAttempted,FIND_IN_SET(PuzzlesAttempted,(SELECT GROUP_CONCAT( PuzzlesAttempted ORDER BY PuzzlesAttempted DESC )FROM sc_MaxPuzzlesAttempted )) AS position  FROM sc_MaxPuzzlesAttempted where gu_id='".$userid."' "); */
		$query = $this->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,PuzzlesAttempted,position from (select gu_id,grade_id,gp_id,sid,section,username,fname,lastupdate,PuzzlesAttempted,FIND_IN_SET(PuzzlesAttempted,(SELECT GROUP_CONCAT( PuzzlesAttempted ORDER BY PuzzlesAttempted DESC )FROM sc_maxpuzzlesattempted where org_id='".$org_id."' and grade_id='".$grade_id."')) AS position  FROM sc_maxpuzzlesattempted where org_id='".$org_id."' and grade_id='".$grade_id."')as a1 where gu_id='".$userid."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getMaxPuzzlesSolved($userid)
	{	$date=date('Y-m-d');
		$org_id=$this->session->org;
		$grade_id=$this->session->game_grade;
		/* $query = $this->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,PuzzlesSolved,FIND_IN_SET(PuzzlesSolved,(SELECT GROUP_CONCAT( PuzzlesSolved ORDER BY PuzzlesSolved DESC )FROM sc_MaxPuzzlesSolved )) AS position FROM sc_MaxPuzzlesSolved where gu_id='".$userid."' "); */
		$query = $this->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,PuzzlesSolved,position from (select gu_id,grade_id,gp_id,sid,section,username,fname,lastupdate,PuzzlesSolved,FIND_IN_SET(PuzzlesSolved,(SELECT GROUP_CONCAT( PuzzlesSolved ORDER BY PuzzlesSolved DESC )FROM sc_maxpuzzlessolved where org_id='".$org_id."' and grade_id='".$grade_id."')) AS position FROM sc_maxpuzzlessolved where  org_id='".$org_id."' and grade_id='".$grade_id."') as a1 where gu_id='".$userid."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}	
	/*..... Cron Job Badges Concept ........*/
	public function InsertTopScorePositionUser($date)
	{
		$query = $this->db->query("Insert into daywisebadgetopper(userid,org_id,grade_id,type,value,dateontopper,created_on)
		select group_concat(gu_id) as userid,org_id,grade_id,'SBB' as type,score as value,'".$date."' as dateontopper,NOW() from (select gu_id,grade_id,gp_id,sid,section,username,fname,org_id,score,lastupdate,FIND_IN_SET(score,(SELECT GROUP_CONCAT( score ORDER BY score DESC )FROM sc_topscorer  )) AS position FROM sc_topscorer ) as a1 where position=1 group by org_id,grade_id");
	}
	public function InsertMaxPuzzlesAttemptedUser($date)
	{
		$query = $this->db->query("Insert into daywisebadgetopper(userid,org_id,grade_id,type,value,dateontopper,created_on)
select group_concat(gu_id) as userid,org_id,grade_id,'SGB' as type,PuzzlesAttempted as value,'".$date."' as dateontopper,NOW() from (select gu_id,grade_id,gp_id,sid,section,username,fname,org_id,lastupdate,PuzzlesAttempted,FIND_IN_SET(PuzzlesAttempted,(SELECT GROUP_CONCAT( PuzzlesAttempted ORDER BY PuzzlesAttempted DESC )FROM sc_maxpuzzlesattempted  )) AS position  FROM sc_maxpuzzlesattempted  )as a1 where position=1 group by org_id,grade_id");
	}
	public function InsertMaxPuzzlesSolvedUser($date)
	{
		$query = $this->db->query("Insert into daywisebadgetopper(userid,org_id,grade_id,type,value,dateontopper,created_on)
select group_concat(gu_id) as userid,org_id,grade_id,'SAB' as type,PuzzlesSolved as value,'".$date."' as dateontopper,NOW() from 
(select gu_id,grade_id,gp_id,sid,section,username,fname,org_id,lastupdate,PuzzlesSolved,FIND_IN_SET(PuzzlesSolved,(SELECT GROUP_CONCAT( PuzzlesSolved ORDER BY PuzzlesSolved DESC )FROM sc_maxpuzzlessolved )) AS position FROM sc_maxpuzzlessolved ) as a1 where position=1 group by org_id,grade_id");
	}
	/*.............. FB Sharing ..................*/
	public function getFBSharingData($userid,$curdate)
	{
		$query = $this->db->query("SELECT SUM(answer) as PuzzlesSolved,SUM(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id='".$userid."' and lastupdate='".$curdate."') as TotScore,(select SUM(Points) from user_sparkies_history where U_ID='".$userid."' and date(Datetime)='".$curdate."') as Crownies FROM game_reports gr join users u on gr.gu_id=u.id WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate and lastupdate='".$curdate."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	/*.............. FB Sharing ..................*/
	
	
/* ................ Invite a Friend .................. */

	public function GetReferalFriendData($userid)
	{
		$query = $this->db->query("SELECT * from user_referral where userID='".$userid."'");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function CheckUserIsReferred($userid)
	{
		$query = $this->db->query("SELECT count(rid) as isexist from user_referral where userID='".$userid."'");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function CheckReferalFriendIsExist($mobile,$email,$userid)
	{
		$query = $this->db->query("SELECT count(rid) as isexist from user_referral where userID='".$userid."' and mobile='".$mobile."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function InsertReferalFriend($name,$pname,$mobile,$emailid,$userid)
	{
			$query = $this->db->query("INSERT INTO user_referral(userID,name,pname,emailid,mobile,referred_on)
			VALUES('".$userid."','".$name."','".$pname."','".$emailid."','".$mobile."',NOW()) ");
			return $this->db->insert_id();
	}
	
	
	public function update_parent_loginDetails($userid,$session_id)
	{ 
	
		$query = $this->multipledb->db->query("update school_parent_master set pre_logindate = login_date,login_date = CURDATE(),login_count=login_count+1,session_id=".$session_id.",islogin=1,last_active_datetime=NOW() WHERE id =".$userid);
	}
	public function insert_parent_login_log($userid,$sessionid,$ip,$country,$region,$city,$isp,$browser,$status)
	{
		$query = $this->multipledb->db->query('INSERT INTO user_parent_login_log(userid,sessionid,created_date,lastupdate,logout_date,ip,country,region,city,browser,isp,status)VALUES("'.$userid.'","'.$sessionid.'",now(),now(),now(), "'.$ip.'","'.$country.'","'.$region.'","'.$city.'","'.$browser.'","'.$isp.'","'.$status.'")');
		return $query;
	}
	public function updateParentDeviceID($username,$deviceid,$mtoken)
	{	
		$query = $this->multipledb->db->query('UPDATE school_parent_master SET deviceid="'.$deviceid.'",mtoken="'.$mtoken.'" where  username="'.$username.'"'); 
	}
	public function update_parent_login_log($Parentid,$sessionid)
	{
		$query = $this->db->query('update school_parent_master set lastupdate=now() where userid="'.$Parentid.'" and sessionid="'.$sessionid.'"');
		
	}
	public function getChildList($Parentid)
	{
		$query = $this->db->query('select *,(select classname from class where id=grade_id) as gradename from users where parent_id="'.$Parentid.'" and status=1 and visible=1 ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();	
	}
	
	public function getStateList()
	{
		$query = $this->db->query('select id,state_name from  state where status="1" order by state_name asc');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getCityList($stateid)
	{
		$query = $this->db->query('select id,city_name from city where stateid="'.$stateid.'" and status=1 order by city_name asc');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getSchooLlist()
	{
		$query = $this->db->query('select id,school_name,school_code from  school_master where status="1" order by school_name asc');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function gradelist()
	{
		$query = $this->db->query('select id as grade_id,classname as grade_name from  class where id in(11,1,2) order by sortby ASC');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getPlan($grade)
	{
		$query = $this->db->query('select id from g_plans where grade_id="'.$grade.'" ');
		return $query->result_array();
	}
	public function IscouponcodeValid($couponcode)
	{
		$query = $this->db->query("select count(c.id) as iscouponvalid,org.id as org_id,discount_percentage,coupon_valid_times,CURDATE() as startdate,DATE_ADD(CURDATE(), INTERVAL validity-1 DAY) as enddate from couponmaster as c join organization_master as org on org.id=c.organization_id where c.couponcode='".$couponcode."' and (CURDATE() between valid_from and valid_to) and coupon_valid_times>coupon_used_times and c.status=1 and org.status=1 ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function GetCouponCodeDetails($couponcode)
	{
		$query = $this->db->query("select count(c.id) as iscouponvalid,coupon_valid_times,discount_percentage,coupon_used_times,validity,price,org.id as orgid from couponmaster as c join organization_master as org on org.id=organization_id where couponcode='".$couponcode."' and c.status=1 and org.status=1 ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function applycoupon($couponcode)
	{
		$query = $this->db->query("select count(id) as iscouponvalid from couponmaster where couponcode='".$couponcode."' and (CURDATE() between valid_from and valid_to) and coupon_valid_times>coupon_used_times and status=1");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}	
	public function updateCouponStatus($RegId,$couponcode,$planamount,$paidamount)
	{
		$query = $this->db->query('UPDATE registration SET status="P", couponcode="'.$couponcode.'",originalamount="'.$planamount.'",paidamount="'.$paidamount.'", processdate="'.date("Y-m-d H:i:s").'" where id= "'.$RegId.'" ');
		//echo $this->multipledb->db->last_query(); exit;
		
	}
	public function ChildLimitExpired($parentid)
	{
		$query = $this->db->query("select count(id) as noofchild from users where parent_id='".$parentid."' and status=1 and visible=1 and portal_type='B2C' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function addChildReg($Name,$planid,$Grade,$Gender,$Address,$ParentID,$password,$salt1,$salt2,$created_on,$orgpwd,$State,$City,$Pincode,$Schoolname,$couponcode,$Bookneed,$validity,$orgid,$SpecialChild)
	{
		$query =$this->db->query('INSERT INTO users(salt1,password,salt2,fname,gender,status,visible,gp_id,grade_id,sid, section,academicyear,creation_date,org_pwd,portal_type,org_id,avatarimage,agreetermsandservice,state,city,pincode,parent_id,time_limit,school_name,couponcode,isbookneed,startdate,enddate,isspecialchild,site_type,puzzle_cycle)
		VALUES("'.$salt1.'","'.$password.'","'.$salt2.'","'.$Name.'","'.$Gender.'",0,0,"'.$planid.'","'.$Grade.'",2,"A",20,"'.date("Y-m-d").'","'.$orgpwd.'","B2C","'.$orgid.'","'.$profileimage.'",1,"'.$State.'","'.$City.'","'.$Pincode.'","'.$ParentID.'","1800","'.$Schoolname.'","'.$couponcode.'","'.$Bookneed.'",CURDATE(),DATE_ADD(CURDATE(), INTERVAL '.$validity.'  DAY),"'.$SpecialChild.'","WEB",1)');
		
		$clplastid=$this->db->insert_id();
		
		$this->db->query("Update users SET email='".$clplastid."',username='".$clplastid."' where id='".$clplastid."' ");
		
		return $clplastid;
		
	}
	public function getInActiveChildIDDetails($ChildID)
	{
		$query = $this->db->query('select * FROM users a WHERE md5(a.id)="'.$ChildID.'"  AND a.status=0 and a.visible=0');
		return $query->result();
	}
	public function updateChildConfirmUser($childId,$grade_id,$gp_id)
	{
		$query = $this->db->query('UPDATE users SET status=1,visible=1,paymentstatus="P" where id="'.$childId.'"');
		
		$this->db->query("INSERT INTO user_academic_mapping(id, grade_id, gp_id, sid, section, academicid, status, visible)VALUES(".$childId.",'".$grade_id."','".$gp_id."',2,'A',20,1,1)");
	}
	public function updateChildFailureUser($childId,$paymentstatus)
	{
		$query = $this->db->query('UPDATE users SET paymentstatus="'.$paymentstatus.'" where id="'.$childId.'"');
	}
	public function UpdateCouponCount($couponcode)
	{
		$query = $this->db->query("UPDATE couponmaster SET coupon_used_times=coupon_used_times+1 WHERE couponcode='".$couponcode."' ");
	}
	public function getChildDetails($ChildID,$ParentID)
	{
		$query = $this->db->query('select * FROM users  as a WHERE id="'.$ChildID.'"  AND a.status=1 and a.visible=1  ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();	
	}	
	public function checkGameExist($ChildID,$grade_id)
	{
		$query = $this->db->query('select count(id) as isgameexist,group_concat(gid) as gameid from(SELECT * from rand_selection where user_id="'.$ChildID.'" and grade_id="'.$grade_id.'" and played="N" order by id DESC limit 2) as a1');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getGameScoreofGame($ChildID,$puzzle_cycle,$gameid)
	{
		//$query = $this->db->query('select COALESCE(SUM(star),0) as TotalStar from (SELECT CASE WHEN answer=0 THEN 0 WHEN answer BETWEEN 1 and 4 THEN 1 WHEN answer BETWEEN 5 and 7 THEN 2 WHEN answer BETWEEN 8 and 10 THEN 3 end as star from gamedata where gu_id="'.$ChildID.'" and g_id="'.$gameid.'" and puzzle_cycle="'.$puzzle_cycle.'") as a1 ');
		
		$query = $this->db->query('select COALESCE(SUM(star),0) as TotalStar from (SELECT star from gamedata where gu_id="'.$ChildID.'" and g_id="'.$gameid.'" and puzzle_cycle="'.$puzzle_cycle.'") as a1 ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function checkNewGameAvialbility($ChildID,$org_id,$grade_id,$puzzle_cycle,$limit)
	{
		$query = $this->db->query('select gid,groupid from game_group_mapping where org_id="'.$org_id.'" and grade_id="'.$grade_id.'" and status=1 and gid not in(select gid from rand_selection where user_id="'.$ChildID.'" and (puzzle_cycle="'.$puzzle_cycle.'" OR played="N") ) order by id ASC limit '.$limit.' ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function InsertNewGame($ChildID,$grade_id,$gp_id,$sid,$section,$groupid,$puzzle_cycle,$gid)
	{
		$this->db->query("INSERT INTO rand_selection (gc_id, gs_id, gid, gp_id, grade_id, section, school_id, user_id, created_date, puzzle_cycle)VALUES(1,".$groupid.",".$gid.",".$gp_id.",'".$grade_id."','".$section."',".$sid.",".$ChildID.",NOW(),".$puzzle_cycle.")");
	}
	public function UpdateUserCycleStatus($ChildID,$puzzle_cycle)
	{
		$query = $this->db->query("UPDATE users SET puzzle_cycle=".$puzzle_cycle." WHERE id='".$ChildID."' ");
	}
	public function getActualAssignedGames($ChildID,$puzzle_cycle)
	{
		$query = $this->db->query('SELECT g.gname,rs.gid,g.img_path,g.game_html,rs.puzzle_cycle,(select count(*) as tot_game_played from game_reports where gu_id = "'.$ChildID.'" and puzzle_cycle="'.$puzzle_cycle.'" and site_type="WEB" and g_id=rs.gid) as tot_game_played,(select count(DISTINCT g_id) from game_reports where gu_id = "'.$ChildID.'" and site_type="WEB" and lastupdate=CURDATE()) as played_game_day,(select MAX(game_score) from game_reports where gu_id =  "'.$ChildID.'" and puzzle_cycle=rs.puzzle_cycle and site_type="WEB" and g_id=rs.gid) as tot_game_score,(select COALESCE(SUM(star),0) from game_reports where gu_id =  "'.$ChildID.'" and puzzle_cycle=rs.puzzle_cycle and site_type="WEB"  and g_id=rs.gid) as TotalStar from rand_selection  as rs
		JOIN games AS g ON rs.gid = g.gid
		where user_id="'.$ChildID.'" and played="N" order by id DESC limit 2');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
		
		
	}
	public function UpdateGameFinishedStatus($ChildID,$puzzle_cycle,$grade_id,$gameid)
	{	
		$query = $this->db->query("UPDATE rand_selection SET played='Y' WHERE user_id='".$ChildID."' and puzzle_cycle='".$puzzle_cycle."' and grade_id='".$grade_id."' and gid='".$gameid."' ");
	}
	
	
	public function update_parent_logout_log($userid,$sessionid)
	{
		$query = $this->db->query('update user_parent_login_log set lastupdate=now(),logout_date=now() where userid="'.$userid.'" and sessionid="'.$sessionid.'"');
		return $query;
	}
	
	public function update_parent_loginstatus($userid,$login_session_id)
	{
		$query = $this->db->query('Update school_parent_master set islogin=0 WHERE id="'.$userid.'" AND status=1');
	}
	public function update_Child_loginstatus($userid,$login_session_id)
	{	
		$query = $this->db->query('Update users set islogin=0 WHERE id="'.$userid.'" AND status=1');
	}
	public function checkParentIsActive($userid,$login_session_id)
	{ 
		$query = $this->db->query('CALL  checkparentisactive("'.$userid.'","'.$login_session_id.'")');
		mysqli_next_result($this->db->conn_id);
		//echo $this->db->last_query(); exit;
		return $query->result();
	}
	public function getMonthWiseScore($ChildID,$startdate,$enddate)
	{
		$query = $this->db->query("SELECT sum(gr.game_score) as TotScore,SUM(star) as TotalStar,DATE_FORMAT(gr.lastupdate, '%d-%m-%Y') as monthNumber,DATE_FORMAT(gr.lastupdate, '%b') as monthName,YEAR(gr.lastupdate) as Year FROM game_reports gr join users u on gr.gu_id=u.id	WHERE gr.gu_id=".$ChildID." and gr.lastupdate between startdate and enddate group by DATE_FORMAT(gr.lastupdate,'%m') DESC,YEAR(gr.lastupdate) DESC ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function checkLoginCrownies($ChildID,$CurDate)
	{
		$query = $this->db->query("select count(id) as isgotCrownies from user_sparkies_history where U_ID='".$ChildID."' and date(Datetime)='".$CurDate."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getChildProfile($userid)
	{
		$query = $this->db->query("SELECT *,(select classname from class where id=grade_id) as gradename FROM users where id='".$userid."'");
		return $query->result_array();
	}
	public function getDailyStatus($ChildID)
	{
		$query = $this->db->query("SELECT game_html,sum(TotalScore) as TotScore,SUM(star) as TotalStar from(SELECT gid,game_html,game_score as TotalScore,answer,CASE WHEN answer=0 THEN 0 WHEN answer BETWEEN 1 and 4 THEN 1 WHEN answer BETWEEN 5 and 7 THEN 2 WHEN answer BETWEEN 8 and 10 THEN 3 end as star FROM `gamedata` join games as g on g.gid=g_id WHERE gu_id='".$ChildID."' and lastupdate=CURDATE()) as a1 group by gid");
		return $query->result_array();
	}
	
}



