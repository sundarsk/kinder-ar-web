<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multiassess_model extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
				 $this->load->library('Multipledb');
        }
/*............. Multi Assessment Concept Start .............*/
	public function getusername($userid)
	{
				 
		$query = $this->db->query("SELECT id,fname,lname,email,mobile,username,gp_id,grade_id,startdate,enddate,sid,section,parentname,doctorid FROM users where id='".$userid."'");		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getGameScore($userid,$org_id)
	{
		$query = $this->db->query('select ME,VP,FA,PS,LI,ROUND(((ME+VP+FA+PS+LI)/5),2) as bspi from asap_gamescore where gu_id="'.$userid.'" and org_id="'.$org_id.'" ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getMultiAssesGameList($grade_id,$org_id)
	{
		$query = $this->db->query('SELECT  name, sid, grade_id, ME, VP, FA, PS, LI,mg1.gs_id as s1,mg2.gs_id  as s2,mg3.gs_id as s3,mg4.gs_id as s4,mg5.gs_id as s5,
mg1.gname as MEgname,mg1.path as MEpath,mg1.img_path as MEimgpath,mg1.game_html as MEgamehtml,
mg2.gname as VPgname,mg2.path as VPpath,mg2.img_path as VPimgpath,mg2.game_html as VPgamehtml,
mg3.gname as FAgname,mg3.path as FApath,mg3.img_path as FAimgpath,mg3.game_html as FAgamehtml,
mg4.gname as PSgname,mg4.path as PSpath,mg4.img_path as PSimgpath,mg4.game_html as PSgamehtml,
mg5.gname as LIgname,mg5.path as LIpath,mg5.img_path as LIimgpath,mg5.game_html as LIgamehtml
FROM asap_game_mapping as m
join games as mg1 on mg1.gid=m.ME
join games as mg2 on mg2.gid=m.VP
join games as mg3 on mg3.gid=m.FA
join games as mg4 on mg4.gid=m.PS
join games as mg5 on mg5.gid=m.LI
WHERE org_id='.$org_id.' and grade_id='.$grade_id.' and status=1 ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getgameid($gamename)
	{	
		$query = $this->db->query("select gid from games where game_html='".$gamename."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function checkgame($org_id,$gameid,$grade_id)
	{	
		$userid = $this->session->user_id;
		$query = $this->db->query("SELECT count(*) as isexist,(select count(id) from asap_gamedata where g_id='".$gameid."' and gu_id='".$userid."' and org_id=".$org_id.") as playedgamescount  FROM asap_game_mapping WHERE org_id=".$org_id." and grade_id=".$grade_id."  and ".$gameid." in (ME,VP,FA,PS,LI)");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getresultGameDetails($userid,$gameid)
	{
		$query = $this->db->query("SELECT u.id,fname,lname,u.email,mobile,u.username,gp_id,grade_id,startdate,enddate,sid,section,(select gs_id from games where gid='".$gameid."') as gameskillid FROM users as u where u.id='".$userid."' limit 0,1");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function insertone($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$org_id)
	{
		$query = $this->db->query("insert into asap_gamedata (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate,org_id) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."','".$org_id."')");
	}
	public function checkinsertorupdate($userid,$org_id)
	{
		$query = $this->db->query("Select count(id) as isplayed from asap_gamescore where gu_id='".$userid."' and org_id='".$org_id."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function organization_moduletype($org_id)
	{
		$query = $this->db->query("select asaponly from organization_master where id='".$org_id."'");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function InsertGameScore($userid,$org_id,$score,$colname)
	{
		$query = $this->db->query("INSERT INTO asap_gamescore(gu_id,org_id,".$colname.",created_on)values('".$userid."','".$org_id."','".$score."',NOW()) ");
	}
	public function UpdateGameScore($userid,$org_id,$score,$colname)
	{
		$query = $this->db->query("Update asap_gamescore SET gu_id='".$userid."',org_id='".$org_id."',".$colname."='".$score."',created_on=NOW() where gu_id='".$userid."' and org_id='".$org_id."' ");
	}
	public function checkUserBelongtoDoctor($userid,$doctorid)
	{
		$query = $this->db->query("Select count(id) as isbelong from users where id='".$userid."' and doctorid='".$doctorid."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function checkUserisEligible($userid)
	{
		$query = $this->db->query("SELECT (select value from config_master where code='ASAP_LIMIT' and status='Y') as Asap_limit,(select value from config_master where code='ASAP_NEXT_SESSION_LIMIT' and status='Y') as Next_Session_Limit,(Select id as asapid from asap_user_session where userid='".$userid."' ORDER BY assigned_date DESC LIMIT 1) as asapid FROM users u where id='".$userid."' order by status desc");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function checkUserCompletedMasap($userid,$org_id)
	{
		$query = $this->db->query("SELECT count(gu_id) as asapcount FROM asap_gamescore WHERE gu_id='".$userid."'  and (ME!='' and VP!='' and FA!=''and PS!=''and LI!='') and org_id='".$org_id."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function UpdateUserMasapStatus($userid,$org_id)
	{
		$this->db->query("Update users SET portal_type='CLP' where id='".$userid."' ");
	} 
	
	
	
/* -------------- Multi Assessment Concept Start---------------------- */
	public function getMyCurrentSparkies($school_id,$grade_id,$userid,$startdate,$enddate)
    {
		$query = $this->db->query("CALL getMyCurrentSparkies(".$school_id.",".$grade_id.",".$userid.",'".$startdate."','".$enddate."')");
		return $query->result_array();

	}
	public function getBSPI($userid)
	{
		$query = $this->db->query("SELECT (AVG(`game_score`)) as score ,gs_id , lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."' and (lastupdate between '".$this->session->astartdate."' and '".$this->session->aenddate."')   group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getEligibleUserList()
	{
		$query = $this->db->query("Select * from 
(SELECT u.id,u.fname,u.username,u.email,u.mobile,u.doctorid,d.doctorname,d.mobilenumber,
(select value from config_master where code='asap_TYPE' and status='Y') as MaxMasaplimit,
(select value from config_master where code='asap_SESSION_LIMIT' and status='Y') as MaxSessionLimit,
(SELECT count(gu_id) as asapcount FROM asap_gamescore WHERE gu_id=u.id and (ME!='' and VP!='' and FA!=''and PS!=''and LI!='') order by date(u.created_on) desc limit 1) as MasapPlayedCount,
(CASE WHEN (SELECT date(created_on) as lastplayedon FROM asap_gamescore WHERE gu_id=u.id and (ME!='' and VP!='' and FA!=''and PS!=''and LI!='') order by date(created_on) desc limit 1) IS Not Null THEN (SELECT date(created_on) as lastplayedon FROM asap_gamescore WHERE gu_id=u.id and (ME!='' and VP!='' and FA!=''and PS!=''and LI!='') order by date(created_on) desc limit 1) ELSE date(u.creation_date) END)as  asaptakendate,
(select count( DISTINCT lastupdate) from game_reports where gu_id=u.id and lastupdate>=asaptakendate) as takensession  FROM users u join doctormaster as d on u.doctorid=d.id where u.status=1 and u.visible=1 and ismasapapproved=0 order by creation_date desc) as a1 where takensession>=MaxSessionLimit");
		return $query->result_array();
	}
	public function checkUserhavemasap($userid,$masapid)
	{
		$query = $this->db->query("Select count(id) as isexist from asap_user_approval where userid='".$userid."' and masapid='".$masapid."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function InsertMasapData($userid,$masapid,$doctorid)
	{
		$query = $this->db->query("INSERT INTO asap_user_approval(doctorid,userid,masapid,status,created_on)values('".$doctorid."','".$userid."','".$masapid."','W',NOW())");
	}
	
/* ................. Intelekt ASAP .............. */	
	
	public function getAsapType($sid,$gradeid)
	{
		$query = $this->db->query("Select id,name from asap_game_mapping where sid='".$sid."' and grade_id='".$gradeid."' ");
		
		return $query->result_array();
	}
	public function checkAsapSessionAssgined($userid,$asapid)
	{
		$query = $this->db->query("Select count(id) as isassgined from asap_user_session where userid='".$userid."' and asapid='".$asapid."' ");
		
		return $query->result_array();
	}
	public function getCurrentAsapSessionid($userid)
	{
		$query = $this->db->query("select (CASE WHEN (Select id as asapid from asap_user_session where userid='".$userid."' ORDER BY assigned_date DESC LIMIT 1) IS NOT NULL THEN (Select id as asapid from asap_user_session where userid='".$userid."' ORDER BY assigned_date DESC LIMIT 1) ELSE 1 END) as asapid");
		return $query->result_array();
	}
	public function InserAsapSession($userid,$grade_id,$asapid)
	{
		$query = $this->db->query("INSERT INTO asap_user_session(asapid,userid,grade_id,assigned_date,created_on,played_status)values('".$asapid."','".$userid."','".$grade_id."',CURDATE(),NOW(),'N')");
		return $this->db->insert_id();
	}
	
/* ...... Oranization based Leader Board ............. */
	public function maxscore_byorg($org_id,$grade_id)
	{
		$query = $this->db->query('select min(DISTINCT(bspi)) as val from (select ROUND(((ME+VP+FA+PS+LI)/5),2) as bspi from asap_gamescore as ag JOIN users u ON ag.gu_id=u.id where status=1 and visible=1 and isdemo=0 and ag.org_id="'.$org_id.'" and grade_id="'.$grade_id.'" group by u.id order by bspi DESC) as z1');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function Topperlist_byorg($minscore,$org_id,$grade_id)
	{
		$query = $this->db->query('select * from (select ROUND(((ME+VP+FA+PS+LI)/5),2) as bspi,username,CONCAT(fname," ",lname) as name,avatarimage,(select classname from class where id=u.grade_id) as grade from asap_gamescore as ag JOIN users u ON ag.gu_id=u.id where status=1 and visible=1 and isdemo=0 and ag.org_id="'.$org_id.'" and grade_id="'.$grade_id.'" group by u.id order by bspi DESC limit 50) as z1 where bspi>="'.$minscore.'" ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}

}

