<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription_model extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
				 $this->load->library('Multipledb');
        }

        public function countrylist()
        {
					 
			$query = $this->db->query('select id,countryname,code,phonecode from  country_master where status="Y" order by countryname asc');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
        }
		
		public function getStateList($countryid)
        {
					 
			$query = $this->db->query('select id,state_name from  state where countryid="113" order by state_name asc');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
        }
		
		public function gradelist()
        {
					 
			$query = $this->db->query('select id,classname from  class order by sortby ASC');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
        }
		
		public function adduser($firstname,$lastname,$gender,$dob,$parentname,$mobile,$email,$shpassword,$salt1,$salt2,$state,$city,$pincode,$address,$CouponCode,$password,$hospitalid,$doctorid,$gradeid,$planid)
        {
//CLP DB
			$query = $this->db->query('INSERT INTO users(email, salt1, password, salt2, fname, lname, gender, mobile, dob, status, visible,gp_id,grade_id,address,username,sid, section,academicyear,creation_date,org_pwd,stateid,city,pincode, parentname,hospitalid, doctorid, couponcode,portal_type,startdate,enddate)
			VALUES("'.$email.'","'.$salt1.'","'.$shpassword.'","'.$salt2.'","'.$firstname.'","'.$lastname.'","'.$gender.'","'.$mobile.'","'.$dob.'",1,1,"'.$planid.'","'.$gradeid.'","'.$address.'","'.$email.'",2,"A",20,"'.date("Y-m-d").'","'.$password.'","'.$state.'","'.$city.'","'.$pincode.'","'.$parentname.'","'.$hospitalid.'","'.$doctorid.'","'.$CouponCode.'","CLP",CURDATE(),DATE_ADD(CURDATE(), INTERVAL (select value from config_master where code="VALIDDAYS" and status="Y") DAY))');
			
			$clplastid=$this->db->insert_id();
			$query = $this->db->query("INSERT INTO user_academic_mapping(id, grade_id, gp_id, sid, section, academicid, status, visible)
			VALUES(".$clplastid.",'".$gradeid."','".$planid."',2,'A',20,1,1)");
			//echo $this->db->last_query(); exit;
			
			return $this->db->insert_id();
		}
		public function UserSubscriber($firstname,$lastname,$gender,$dob,$shpassword,$salt1,$salt2,$grade,$school,$couponcode,$username)
        {
			$query = $this->db->query('INSERT INTO registration(firstname,lastname,gender,dateofbirth,salt1,password,salt2,gradeid,schoolname,couponcode,paidamount,paymentstatus,mailstatus,status,createddate,email,processdate)VALUES("'.$firstname.'","'.$lastname.'","'.$gender.'","'.$dob.'","'.$salt1.'","'.$shpassword.'","'.$salt2.'","'.$grade.'","'.$school.'","'.$couponcode.'","0","Y","N","P","'.date("Y-m-d H:i:s").'","'.$username.'","'.date("Y-m-d H:i:s").'")');		
			//echo $this->db->last_query(); exit;
			return $this->db->insert_id();
		}
		
		public function getuserdetail($lastid)
        {
			$query = $this->db->query('select id,createddate from  registration where id="'.$lastid.'"');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		}
		
		public function userresponse($userid)
        {	
			$query = $this->db->query('select id,firstname,email,(select classname from class where id=r.gradeid) as gradename,createddate from  registration r where md5(id)="'.$userid.'"');		
			//echo $this->db->last_query();
			return $query->result_array();
		}
		
		public function getconfigdetails($couponcode)
        {
			$query = $this->db->query('select id,validity,price,CURDATE() as startdate,DATE_ADD(CURDATE(), INTERVAL validity-1 DAY) as enddate,isasapenabled from organization_master where id IN(select organization_id from couponmaster where couponcode="'.$couponcode.'" and status=1)');
			//echo $this->db->last_query(); 
			return $query->result_array();
		}
		public function getDefaultPlandetails()
        {
			$query = $this->db->query('select id,validity,price,CURDATE() as startdate,DATE_ADD(CURDATE(), INTERVAL validity DAY) as enddate,isasapenabled from organization_master where id=1');
			//echo $this->db->last_query(); 
			return $query->result_array();
		}
		
		public function getresponsedetails($userid)
        {
			$query = $this->db->query('select * from  registration r where md5(id)="'.$userid.'"');		
			//echo $this->db->last_query();
			return $query->result_array();
		}
		
		public function subscribeusers($subscriberid,$salt1,$salt2,$email,$password,$planid,$gradeid,$firstname,$lastname,$gender,$mobile,$address,$dob,$couponcode,$state,$city,$orgpwd,$pincode,$startdate,$enddate,$orgid,$profileimage,$portal_type)
        {
			$query = $this->db->query('INSERT INTO users(subscriberid,email, salt1, password, salt2, fname, lname, gender, mobile, dob, status, visible,gp_id,grade_id,address,username,sid, section,academicyear,creation_date,org_pwd,stateid,city,pincode,couponcode,portal_type,startdate,enddate,org_id,avatarimage,agreetermsandservice)
			VALUES("'.$subscriberid.'","'.$email.'","'.$salt1.'","'.$password.'","'.$salt2.'","'.$firstname.'","'.$lastname.'","'.$gender.'","'.$mobile.'","'.$dob.'",1,1,"'.$planid.'","'.$gradeid.'","'.$address.'","'.$email.'",2,"A",20,"'.date("Y-m-d").'","'.$orgpwd.'","'.$state.'","'.$city.'","'.$pincode.'","'.$couponcode.'","'.$portal_type.'","'.$startdate.'","'.$enddate.'","'.$orgid.'","'.$profileimage.'",1)');
			
			$clplastid=$this->db->insert_id();
			
			$query = $this->db->query("INSERT INTO user_academic_mapping(id, grade_id, gp_id, sid, section, academicid, status, visible)
			VALUES(".$clplastid.",'".$gradeid."','".$planid."',2,'A',20,1,1)");

			/* $query = $this->db->query('INSERT INTO users(subscriberid,salt1,salt2,email,sid,password,grade_id,section,academicyear,	login_count,agreetermsandservice,fname,lname,gender,mobile,address,username,dob,status,gp_id,creation_date,school_master_id,validitystartdate)VALUES("'.$subscriberid.'","'.$salt1.'","'.$salt2.'","'.$email.'",2,"'.$password.'","'.$gradeid.'","A",20,0,0,"'.$firstname.'","'.$lastname.'","'.$gender.'","'.$mobile.'","'.$address.'","'.$email.'","'.$dob.'",1,"'.$gameplanid.'","'.date("Y-m-d H:i:s").'",1,"'.date("Y-m-d").'")'); */
			return $clplastid;
		}
		
		public function subscriberexistchk($subscriberid)
        {
			$query = $this->db->query('select count(subscriberid) as subid, fname from users where subscriberid = "'.$subscriberid.'"');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		}
		
		public function updateprocess($userid,$couponcode,$planamount,$paidamount)
        {
			$query = $this->db->query('UPDATE registration SET status="P", couponcode="'.$couponcode.'",originalamount="'.$planamount.'",paidamount="'.$paidamount.'", processdate="'.date("Y-m-d H:i:s").'" where md5(id)= "'.$userid.'" ');
			//echo $this->db->last_query(); exit;
			
		}
		
		public function completestatus($userid)
        {
			$query = $this->db->query('UPDATE registration SET status="C",paymentstatus="Y",completeddate="'.date("Y-m-d H:i:s").'" where md5(id)= "'.$userid.'" ');		
			//echo $this->db->last_query(); exit;
		}
		
	public function applycoupon($couponcode)
	{
		//echo "CALL SP_Applycouponcode('".$couponcode."')";exit;
		$query = $this->db->query("select count(id) as iscouponvalid,discount_percentage,coupon_valid_times from couponmaster where couponcode='".$couponcode."' and (CURDATE() between valid_from and valid_to) and coupon_valid_times>coupon_used_times and status=1");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}	
	
	public function UpdateCouponCount($couponcode)
	{
		$query = $this->db->query("UPDATE couponmaster SET coupon_used_times=coupon_used_times+1 WHERE couponcode='".$couponcode."' ");
	}
		
		public function checkemailexist($username)
        {	 
			$query = $this->multipledb->db->query("SELECT count(id) as isexist from school_parent_master where username='".$username."' and status=1");
			return $query->result_array();
        }
		public function IsMobilenounique($mobileno)
		{
			$query = $this->multipledb->db->query("SELECT count(id) as isexist from school_parent_master where mobileno='".$mobileno."' and status=1");
			return $query->result_array();
		}
		
		public function chkprocesseduser($userid)
        {
			$query = $this->db->query('select * from school_parent_master where md5(id) = "'.$userid.'" AND status="1" ');
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		}
		
		public function validateurl($userid)
        {
			$query = $this->db->query('select id,date(createddate) as createddate from school_parent_master where md5(id) = "'.$userid.'" and status=0 ');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		}
		public function getSchoolList()
		{
			$query = $this->db->query('select id,school_name from schools where school_master_id!=1 and status=1 and active=1 and visible=1');		
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		}
		
		/*---------------------Offline User -----------------*/
	public function checkIsCouponValid($couponcode)
	{
		$query = $this->db->query('select count(id) as iscouponvalid from couponmaster where couponcode="'.$couponcode.'" and usedstatus="N" and status=1  and couponcode not in(select couponcode from users)');		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getusercount($data0,$schoolid)
	{
		$query = $this->db->query("select count(*) as userCount from users where username LIKE (select concat('".str_replace('.','', str_replace(' ','', strtolower($data0)))."_%')) and sid='".$schoolid."'");
		 //echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function GenerateUsername($firstname,$schoolid,$userexist,$type)
	{
		if($type=='NEW')
		{
			$query = $this->db->query("select concat('".str_replace('.','', str_replace(' ','', strtolower($firstname)))."','.',(select school_code from schools where id='$schoolid')) as username");
		}
		else
		{
			$query = $this->db->query("select concat('".str_replace('.','', str_replace(' ','', strtolower($firstname)))."','_',($userexist+1),'.',(select school_code from schools where id='$schoolid'))username");
		}
		return $query->result_array();		
	}
	Public function UpdateCouponUsedStatus($couponcode)
	{
		$qry=$this->db->query("UPDATE couponmaster set usedstatus=1 where couponcode='".$couponcode."' ");
	}
	public function getCouponcodeDetails($couponcode)
	{
		$query=$this->db->query("SELECT doctorid,(select hospitalid from doctormaster where id=doctorid) as hospitalid FROM couponmaster WHERE couponcode='".$couponcode."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	
	/*-------------------------------------------------*/
	public function getPlan($grade)
	{
		$query = $this->db->query('select id,CURDATE() as startdate,DATE_ADD(CURDATE(), INTERVAL (select value from config_master where code="VALIDDAYS" and status="Y") DAY) as enddate,(select value from config_master where code="VALIDDAYS" and status="Y") as validity from g_plans where grade_id="'.$grade.'" ');
		return $query->result_array();
	}
	
	/* public function addRegistration($firstname,$lastname,$gender,$dob,$mobilenumber,$email,$shpassword,$salt1,$salt2,$state,$city,$pincode,$address,$password,$gradeid,$planid,$created_on)
	{
		$query = $this->db->query('INSERT INTO registration(firstname,lastname,gender,dateofbirth,email,salt1,password,salt2,gradeid,gp_id,paymentstatus,country,state,city,mobilenumber,pincode,address,orgpwd,status,createddate)VALUES("'.$firstname.'","'.$lastname.'","'.$gender.'","'.$dob.'","'.$email.'","'.$salt1.'","'.$shpassword.'","'.$salt2.'","'.$gradeid.'","'.$planid.'","N","1","'.$state.'","'.$city.'","'.$mobilenumber.'","'.$pincode.'","'.$address.'","'.$password.'","N","'.$created_on.'")');
		//echo $this->db->last_query(); exit;
		return $this->db->insert_id();
	} */
	public function getCouponValidTimes($couponcode)
	{
		$query = $this->db->query("select coupon_valid_times,discount_percentage from couponmaster where couponcode='".$couponcode."' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function checkUserEmailidisregistered($userid)
	{
		$query = $this->db->query("select count(id) isexist from registration where email in (select email from registration  where md5(id)='".$userid."') and status='C' ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	/*----------- Forget Password ------------- */
	public function checkuseremailexist($emailid)
	{
		$query = $this->db->query('select id,count(email) as emailcount,fname,lname,email,username,mobile from users where email ="'.$emailid.'" and status=1 and visible=1');		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function forgetpwdlog($userid,$randid)
	{	
		$qry=$this->db->query("insert into change_password_history(userid,randid,requestdate,status)values('".$userid."','".$randid."',NOW(),0)");
	}
	function CheckIsValidUser($userid,$randid)
	{
		$query=$this->db->query("select userid,randid,requestdate from change_password_history where status=0 and md5(userid)='".$userid."' and md5(randid)='".$randid."'");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function getResetpwdUserDetails($userid)
	{	
		$query = $this->db->query('select id,fname,lname,email,username,mobile from users where id="'.$userid.'" and status=1 and visible=1');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	function ResetUserPwd($pwd,$userid,$salt1,$salt2,$orgpwd,$username)
	{
		$qry=$this->db->query("update users set password='".$pwd."',salt1='".$salt1."',salt2='".$salt2."',org_pwd='".$orgpwd."' where md5(id)='".$userid."'");
	}
	function ResetUserPwd_log($userid,$randid,$orgpwd)
	{
		$qry=$this->db->query("update change_password_history set updatedate=now(),status=1,newpwd='".$orgpwd."' where md5(userid)='".$userid."' and md5(randid)='".$randid."'");
	}
	/*------------- Forget Password --------------------*/
	
	
	public function addRegistration($firstname,$mobile,$email,$shpassword,$salt1,$salt2,$org_pwd,$created_on,$State,$City,$Pincode,$Address)
	{
		$query = $this->multipledb->db->query('INSERT INTO school_parent_master(name,username,salt1,password,salt2,mobileno,state,city,pincode,address,portal_type,orgpwd,status,createddate,site_type)VALUES("'.$firstname.'","'.$email.'","'.$salt1.'","'.$shpassword.'","'.$salt2.'","'.$mobile.'","'.$State.'","'.$City.'","'.$Pincode.'","'.$Address.'","B2C","'.$org_pwd.'","0",NOW(),"WEB")');
		//echo $this->multipledb->db->last_query(); exit;
		return $this->multipledb->db->insert_id();
	}
	public function getInActiveParentDetails($ParentID)
	{
		$query = $this->multipledb->db->query('select * FROM school_parent_master a WHERE md5(id) = "'.$ParentID.'"  AND a.status=0');
		return $query->result();
	}
	public function updateParentConfirmUser($ParentID)
	{	
		$query = $this->multipledb->db->query('UPDATE school_parent_master SET status="1" where md5(id)="'.$ParentID.'"');
	}
}
