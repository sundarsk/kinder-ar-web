<?php  include_once('../../Config/config.inc.php');?>
<?php  include_once('../../Model/Plan.class.php');?>
<?php  include_once('../../Config/classes/Common.class.php');?>
<html>
<head>
	<title>Confirming payment for The SuperBrain challenge...</title>
</head>
<body>
	<p>Please wait...</p>
<?php
error_reporting(1);
$ChecksumRec = $_REQUEST['CHECKSUM'];
$merchant_id = $_REQUEST['MCODE'];
$paymentmode = $_REQUEST['TXNTYPE'];
$order_id = $_REQUEST['REFNO'];
$narration = $_REQUEST['NARRATION'];
$amount = $_REQUEST['AMT'];
$status = $_REQUEST['STATUS'];
$mwrefno = $_REQUEST['MWREFNO'];
$reason = $_REQUEST['REASON'];
$working_key = 'FXUCRP684B';
exec("java -jar GenerateChecksum.jar 9 ".$merchant_id." ".$paymentmode." ".$order_id." ".$narration." ".$amount." ".$status." ".$mwrefno." ".$reason." ".$working_key, $ChecksumCalc, $output);
$ChecksumCalc = str_replace("Checksum:","",$ChecksumCalc[0]);
$orderid=Common::ORDER_PREFIX_SB.$order_id;

$arrPayment =$_POST;
$AuthDesc = $_REQUEST['STATUS'];
if($AuthDesc == "S"){
	$arrPayment['Payment_status']='S';
	$arrPayment['Order_Id']=$orderid;
	$arrSubPay = array ("Payment_id" =>  $orderid ,"PaymentStatus"=>'Y');	
}
if($AuthDesc == "E"){
	$arrPayment['Payment_status']='F' ;
	$arrPayment['Order_Id']=$orderid;
}
if($ChecksumCalc === $ChecksumRec && $AuthDesc==="S")
{
	echo "<br>Thank you for shopping with us. Your transaction is successful. We will be shipping your order to you soon.";
	$pageurl= Common::PAYMENT_SUCCESS_PAGE_SB;
	$pStatus = "success";          
}
else if( $ChecksumCalc === $ChecksumRec && $AuthDesc==="E")
{
	//echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
	echo "<br>".$_REQUEST['REASON'].".";
	$pageurl= Common::PAYMENT_FAILURE_PAGE_SB;	
	$pStatus = "failure";
}
else
{
	echo "<br>Security Error. Illegal access detected";
	$pageurl= Common::PAYMENT_FAILURE_PAGE_SB;
	$pStatus = "failure";
}
$arrPayment['bank_name'] = 'MRUPEE'; // Source identifier for response handler
echo "<br><br>";
?>
<form id="frmSucsses" action="<?php echo $pageurl; ?>" method="post">
	<?php foreach ($arrPayment as $key => $value): ?>
	<input type="hidden" name="data[<?php echo $key; ?>]" value="<?php echo $value; ?>"/>
<?php endforeach ?>
</form>
<script language='javascript'>document.getElementById("frmSucsses").submit();</script>
</body>
</html>