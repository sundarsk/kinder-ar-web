(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween21a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("AhDAAIABABIgBABIAAgCgABCABIABgCIAAACg");
	this.shape.setTransform(-0.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFF00","#FFCC00","#FF6600"],[0,0.298,1],-4,-11.6,-4,18.4).s().p("AgBA3IhBAeIAAAAIAKg+IglgrIAAAAIA/gIIAeg8IAhA8IA9AXIgrAgIACA+g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-8.9,18.9,17.9);


(lib.Tween20a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("AhDAAIABABIgBABIAAgCgABCABIABgCIAAACg");
	this.shape.setTransform(-0.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFF00","#FFCC00","#FF6600"],[0,0.298,1],-4,-11.6,-4,18.4).s().p("AgBA3IhBAeIAAAAIAKg+IglgrIAAAAIA/gIIAeg8IAhA8IA9AXIgrAgIACA+g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-8.9,18.9,17.9);


(lib.Tween19a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("AhDAAIABABIgBABIAAgCgABCABIABgCIAAACg");
	this.shape.setTransform(-0.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFF00","#FFCC00","#FF6600"],[0,0.298,1],-4,-11.6,-4,18.4).s().p("AgBA3IhBAeIAAAAIAKg+IglgrIAAAAIA/gIIAeg8IAhA8IA9AXIgrAgIACA+g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-8.9,18.9,17.9);


(lib.Tween18a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("AhDAAIABABIgBABIAAgCgABCABIABgCIAAACg");
	this.shape.setTransform(-0.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFF00","#FFCC00","#FF6600"],[0,0.298,1],-4,-11.6,-4,18.4).s().p("AgBA3IhBAeIAAAAIAKg+IglgrIAAAAIA/gIIAeg8IAhA8IA9AXIgrAgIACA+g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-8.9,18.9,17.9);


(lib.Tween16a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("AhDAAIABABIgBABIAAgCgABCABIABgCIAAACg");
	this.shape.setTransform(-0.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFF00","#FFCC00","#FF6600"],[0,0.298,1],-4,-11.6,-4,18.4).s().p("AgBA3IhBAeIAAAAIAKg+IglgrIAAAAIA/gIIAeg8IAhA8IA9AXIgrAgIACA+g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-8.9,18.9,17.9);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC3333").ss(3,1,1).p("AiAAKQAJAWAlAJQAqAKA0gMQA2gMAhgaQAegWAAgY");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.4,-6,28.8,12.1);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgJAPQgGgEgBgHQgCgHAEgGQAEgGAHgBQAGgCAGAEIACABQAFAEABAGQACAGgEAGQgDAEgEACIgEACIgEABQgEAAgFgDg");
	this.shape.setTransform(-0.5,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgSAdQgMgIgDgOQgEgNAIgMQAIgMAOgDQANgEANAIIACACQAKAIADAMQADANgIANQgFAIgIAEQgEACgFABIgHABQgJAAgJgGg");
	this.shape_1.setTransform(3.2,-4.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AgfAxQgUgNgFgXQgGgXAOgUQANgVAXgFQAXgFAUANIAFADQAQANAFAVQAFAWgNAVQgIANgNAHQgIADgIACQgGACgGAAQgQAAgPgKg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3366FF").s().p("Ag+BiQgpgagKgvQgLgvAbgoQAagpAvgKQAugLApAaIAJAGQAhAaAKAqQAKAugbApQgQAbgaANQgPAHgQAEQgOADgMAAQggAAgdgTgAgMg4QgXAFgNAUQgOAUAGAXQAFAYAUANQAVANAWgFQAIgCAIgEQANgGAIgOQANgUgFgXQgFgVgQgNIgFgDQgOgJgQAAQgGAAgHACg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.7,-11.7,23.5,23.4);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(1,1,1).p("ACsh3Qg2A1gYBHQgXBFggBBQgmBPhWgYQhDgSgPg4QgBgDAAgCQgDgPAAgRQABhFBBgEQAjgCAUAfQAXAigSAjIgUAMQAJgmgZgcQgLgMgOAJQglAYAJAhQADALAIALQADAHAIADQAkANAkgOQASgHAHgRQAag7AEg4QABgTgBgSQgFhMAZg0QAIgQASgSQAiANBNBDg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AhVDCQhCgSgQg4IgBgFQgDgPAAgRQABhFBBgEQAjgCAUAfQAXAigSAjIgUAMQAJgmgZgcQgLgMgOAJQglAYAJAhQADALAIALQADAHAIADQAkANAkgOQASgHAIgRQAZg7ADg4IABglQgFhMAZg0QAHgQASgSQAjANBNBDQg3A1gXBHQgXBFggBBQgeA9g5AAQgRAAgUgGg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.2,-21,36.4,42);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(1,1,1).p("ABKiFQAAgNAFgNQALgeAdgOQAegOAeALQAeALAOAdQAOAegLAeQgLAegeAOQgeAOgegLQgYgJgOgUQgDgFgDgGQgIgSABgQgABXhYQhdAPhGBKQhGBMguCHIgmgNQBAiCBJhRQBOhYBZgh");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAaQgIgDgFgHIgCgFQgDgFAAgGQAAgEACgFQAEgKAKgFQALgFAJAEQALAEAEALQAGAKgEAJQgEALgLAFQgGADgFAAQgEAAgFgCg");
	this.shape_1.setTransform(15.2,-13.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3300").s().p("AgSA0QgRgGgJgPQgDgDgBgFQgGgMAAgLQAAgIADgJQAIgVAVgKQAVgKAUAHQAVAIAJAVQAKAVgHAUQgIAVgVAKQgLAFgMAAQgIAAgKgDgAgLgYQgKAFgEAKQgCAFAAAEQAAAGADAFIACAEQAFAIAIADQAKAEAKgGQALgEAEgLQAEgJgGgLQgEgKgLgEQgEgCgFAAQgFAAgGADg");
	this.shape_2.setTransform(15.2,-13.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#990000").s().p("AjmDHQBAiCBJhRQBOhYBZghIAAACQAAAPAHARIAGALQhdAPhGBKQhGBMguCHgAB9g7QgYgJgOgUIgGgLQgHgRAAgPIAAgCQAAgNAFgNQALgeAdgOQAegOAeALQAeALAOAdQAOAegLAeQgLAegeAOQgQAIgRAAQgNAAgOgFgACBi2QgVAKgIAVQgDAJAAAJQAAALAGAMQABAFADADQAJAPARAGQAVAIAVgKQAVgKAIgVQAHgVgKgVQgJgVgVgIQgJgDgJAAQgMAAgMAGgABXhYIAAAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24,-22.2,48.2,44.5);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AFMCgIidgbQhXgSgzBOIhUCPIgWicQgThphGgHIitgnICihPQA5gdAChIIgSi4IB9B/QAtAvBIgVICohJIhMCSQgmBBAoBAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("AgXCQIAAgBQgWh3hVgFIgYgFIAcgOQBDgfADhWIAAgEIgDgjIAUAVQA0A3BTgYIAEgBIAggOIgJARQgqBJAuBHIADAFIAZAcIgOgDIACAAQhigVg3BYIgCABIgKARg");
	this.shape_1.setTransform(1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF9900").s().p("AgdCzIAAgCQgbiThpgGIgdgHIAjgRQBTgnAChpIAAgGIgDgrIAZAbQBABEBmgeIAGgCIAmgRIgKAVQg0BaA4BYIAFAGIAeAjIgRgDIACAAQh4gahFBrIgCACIgMAVgAg3h5IAAAFQgDBVhDAfIgcAOIAYAFQBVAFAVB4IAAABIACAMIAKgRIACgBQA3hXBiAVIgCAAIAOACIgZgcIgEgFQgthHAqhJIAJgRIggAOIgEABQhTAZg0g3IgVgWg");
	this.shape_2.setTransform(1,-0.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC00").s().p("AhFC0QgShphHgHIisgnICihPQA4gdAChIIgSi4IB9B/QAtAvBHgVICphJIhNCSQglBCAoA/IB7CLIicgaQhXgTgzBPIhUCPgAg8icIAAAHQgDBohTAoIgjAQIAeAHQBpAGAaCUIAAABIACAPIANgVIABgBQBFhsB4AaIgBAAIARADIgfgjIgEgFQg4hZAzhaIALgVIgnARIgFACQhnAehAhEIgZgbg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.1,-34.5,68.3,69.1);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC99").s().p("AgUB7QgRgLgTgBQgcgBAAhOQAAgJAzhJQAkg2AKgdQBBBQAGAuQAFAfgNAoQABAlgPAPIgSARIgJABQgnAAgQgLg");
	this.shape.setTransform(20.7,26.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EDAF71").s().p("AgCCGIgBAAIgWgDQgVgEgbgoQgcgpAig9IAJgIIBmiAIAMAOQgKAdgmA2QgyBJAAAJQAABOAdABQASABAQALQAQALApAAIAJgBIgHAGQggARgVAAQgUAAgJgSg");
	this.shape_1.setTransform(16.4,26.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FE2A8C").s().p("AkMkxIgEAKIAPgmQGrBFBnGSIg2BDIhnCBQgtn7lTiEg");
	this.shape_2.setTransform(-1.1,-8.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D00E68").s().p("ACAFNQAAoglFh/IADgGIAEgKIAEgJQFTCEAtH6IgIAIIg/BRIABgfg");
	this.shape_3.setTransform(-9.3,-3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.1,-42.2,58.4,84.4);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#00CCFF").ss(1,1,1).p("AFRilQAtBaAKBZQAEAkgBAkQgCAtgKAtQgnCriLBFQiLBFiehJQAAgBgBAAQhDgfg2gxQg0gvgog/QgQgagOgdQgshagLhZQgKhSAThRQAahzBIhEQAjghAtgXQCMhFCdBJQCfBKBVCsg");
	this.shape.setTransform(-13.6,-3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D3F0F9").s().p("AAzAoQhCgfg2gwQBCAeBJAGIgSArIgBAAg");
	this.shape_1.setTransform(-28.1,33.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D8C6F3").s().p("ABWFwQhKgGhCgeQg0gvgnhAQgQgZgOgdQgthbgKhYQgKhSAShRQAbhzBIhFQA1gNA/AEQAzAFAwAQQhTAAhCAyQhlBJgHCMQgBAjAEAiQANBkBEBeQBaB7CHAkQAvAMAsAAQg1AUg/AAIghgCg");
	this.shape_2.setTransform(-29.6,-3.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#ECF8FC").s().p("AAsEaQBehjgTiZQgTiXh5h1QhRhOhfggQApAAAsAMQCGAjBbB8QARAYAOAYQAoBHAKBLQAFAigCAjQgHCLhlBKQhAAwhPACQA3gWArgtg");
	this.shape_3.setTransform(-0.6,-3.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F0E6FF").s().p("Ah+GcIASgsQBTAGBBgZQgrABgvgNQiHgkhbh7QhEhdgNhlQgEgiACgjQAGiLBmhKQBDgyBSAAQBfAgBQBOQB7B1ASCXQATCZheBjQgrAtg4AWQBQgCBAgwQBmhKAGiLQACgjgEgiQgLhLgohHQgOgYgQgYQhch8iGgjQgsgMgpAAQgvgQg1gEQg/gFg1AOQAjghAtgXQCLhFCeBJQCfBKBVCsQAsBaALBZQAEAkgBAkQgCAtgLAtQgmCriLBFQhEAhhIAAQhMAAhRglg");
	this.shape_4.setTransform(-10.1,-3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54.1,-49.6,81.1,91.9);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#00CCFF").ss(1,1,1).p("AlQilQgtBagKBZQgEAkABAkQACAtAKAtQAnCrCLBFQCLBFCehJQAAgBABAAQBDgfA2gxQBJhCAxhjQBVirgnirQgahzhIhEQgjghgtgXQiIhDiZBEQgEABgEACQiFA+hRCCQgQAagOAcg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#ECF8FC").s().p("AhZErQhKg2gXhZQgJghgBglQgCgjAEgiQANhlBEhdQBQhsBxgpQAQgGARgEQAsgMAoAAQheAghRBOQh6B1gSCXQgUCZBfBjQArAtA3AWQhPgChBgwg");
	this.shape_1.setTransform(-12.9,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F0E6FF").s().p("AiqGgQiLhFgnirQgKgtgCgtQgBgkAEgkQAKhZAthaQAOgcAQgaQBRiCCFg+IAIgDQCZhECIBDQAtAXAjAhQg1gOhAAFQg0AEgwAQQgoAAgsAMQgRAEgPAGQhyAphQBsQhEBdgNBlQgEAiACAjQABAlAJAhQAXBZBKA2QBBAwBQACQg4gWgrgtQhfhjAUiZQASiXB7h1QBQhOBeggQBTAABDAyQBlBKAHCLQAHCLhbB8QhaB7iHAkQgwANgqgBQBBAZBTgGQBKgHBCgeQg2AxhDAfIgBABQhSAlhLAAQhIAAhEghg");
	this.shape_2.setTransform(-3.4,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D8C6F3").s().p("AjqFdQAsABAvgMQCHgkBah8QBbh8gHiKQgHiMhlhKQhCgxhTAAQAwgQAzgFQA/gEA1ANQBIBFAbBzQAmCqhVCsQgwBjhJBCQhCAehKAGIghACQg/AAg1gVg");
	this.shape_3.setTransform(16,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.5,-45.9,81.1,91.9);


(lib.Tween1a = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFF66","#FFF000"],[0,0.541],10,-10.2,-11.5,7.9).s().p("Ag8B4QgEgDACgGIAOhOIg5g1QgFgEABgEQACgEAHgBIBOgKIAghHQADgGAEAAQAEAAADAGIAiBHIA0AHQhHATgpAnQgsAoAAAxIAAAGIgFACIgGACIgDgBg");
	this.shape.setTransform(-1.7,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#FFF000","#FF9900"],[0,1],-9,11.7,0,-9,11.7,30.7).s().p("ABRCIIhRgtIhQAtQgFADgDgCQgDgDABgGIAShaIhEg+QgEgFABgDQABgDAHgBIBbgMIAnhTQACgFADAAQAEAAADAFIAnBTIBbAMQAGABABADQABADgEAFIhDA+IARBaQABAGgDADIgDAAIgFgBgAgGhwIghBHIhOAKQgHABgCADQgBAEAFAFIA5A1IgOBNQgCAHAEACQADADAGgDIAFgDIAAgFQAAgxAsgpQApgmBHgUIg0gGIgihHQgDgGgEAAQgDAAgDAGg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF6600","#FF9900"],[0,1],-18,4.7,18.3,-3).s().p("AhaCMQgEgDAAgGIABgEIAQhYIhBg8QgFgFAAgFIAAgCQACgGAJgBIBZgLIAmhRQACgEADgCQACgCACAAQADAAADACQACACACAEIAmBRIBZALQAJABACAGIAAACQAAAFgFAFIhBA8IARBYIAAAEQAAAGgDADQgFADgIgEIhPgrIhOArQgEACgDAAQgDAAgCgBgABRCIQAFADADgCQADgDgBgGIgRhaIBDg+QAEgFgBgDQgBgDgGgBIhbgMIgnhTQgDgFgEAAQgDAAgCAFIgnBTIhbAMQgHABgBADQgBADAEAFIBEA+IgSBaQgBAGADADQADACAFgDIBQgtg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.8,-14.1,29.7,28.3);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC3333").ss(1,1,1).p("AhBAHQANAEAOAAQAQAAATgLQAagRAKggAhDAMQAMAXASAJQANAGAUAAQASAAAPgGQARgHAWgS");
	this.shape.setTransform(47.5,-5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#CC3333").ss(1,1,1).p("ABEAMQgMAXgSAJQgNAGgUAAQgSAAgPgGQgRgHgWgSABCAHQgNAEgOAAQgQAAgTgLQgagRgKgg");
	this.shape_1.setTransform(-47.6,5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.3,-11,110.7,22.1);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#990000").s().p("Ao/JAQjvjvAAlRQAAlRDvjtQDvjwFQAAQFRAADvDwQDvDtgBFRQABFRjvDvQjvDvlRgBQlQABjvjvg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("ApXJXQj4j4AAlfQAAlfD4j3QD5j5FeAAQFfAAD4D5QD5D3AAFfQAAFfj5D4Qj4D5lfAAQleAAj5j5gAo/o+QjvDtAAFRQAAFRDvDvQDvDvFQgBQFRABDvjvQDvjvgBlRQABlRjvjtQjvjwlRAAQlQAAjvDwg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF7900").s().p("AqnKnQkZkZAAmOQAAmNEZkZQEakaGNAAQGOAAEZEaQEaEZAAGNQAAGOkaEZQkZEamOAAQmNAAkakagApXpXQj4D4AAFfQAAFfD4D4QD4D5FfAAQFfAAD4j5QD5j4AAlfQAAlfj5j4Qj4j4lfAAQlfAAj4D4g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC00").s().p("ArtLuQk2k3AAm3QAAm2E2k3QE3k2G2AAQG3AAE3E2QE2E3AAG2QAAG3k2E3Qk3E2m3AAQm2AAk3k2gAqnqmQkZEZABGNQgBGOEZEZQEaEaGOAAQGNAAEZkaQEakZAAmOQAAmNkakZQkZkZmNAAQmOAAkaEZg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-106,-106,212,212), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D66B01").s().p("EgyzAcvQhBhCgthKUAxLgBvA37ABqQguBNhEBEQhABAhJAsQk0gKkygJQhqhdAJiHQgUCMgnBUUgsDgBKgpEABoQhPguhFhFgEg1+AV1QgCgaAAgbQAUisANitMBrNAAAQAICsALCtQAAAcgCAbUhDjgCIgoaACGgEg1KAKTQAIjNgCjMUA1bgC/A1EACxQABDXAFDYQ5zA55vAAQ7nAA7ihBgEg1lgHTUA/ugDpArfADHQgIDGgDDIUgzpgBdg3BABmQgIi7gQi6gEg0FgNRQBVg1B/gaQkWgzg5hsIAAhWUA2rgBoAzoABcIj7BiIFpB7IgJCKUhJTgCnggqACQgEg1ggZuQAriLBohyUA3SgBmAu9ABaQBxB3AuCSg");
	this.shape.setTransform(-4.9,16.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("EAqgAj5IjIhoQh3hBAfhCQheBKgoCBIhRAgMhP3AAAQlcAAj2j2IghgjQhsh3g1iOQg0iKABigMAAAghbIBXiHICLgsQhygShwjBIAAmAQAAhLALhHQAViBA7hxQA7hvBfhfQBZhZBmg5QC1hkDeAAMBacAAAQDfAAC0BkQBmA5BZBZQBbBaA5BpQBBB2AWCHQAMBHAABLIAADtQg0CWhlAPICFBWIAUAwMAAAAlJQAACcgyCGQgzCNhoB3QgUAWgWAWQj2D2lcAAgEAqgAjNIB3AAQFWAADxjxIAIgJQBzh2A6iMQA4iJAEifIAAgUMAAAgktIh7h7QBogjAThqIAAj0QAAhagRhUQgciKhKh2QgyhRhIhIQh9h9iXg8QiOg4ilAAMhYuAAAQilAAiOA4QiXA8h9B9QhNBNg0BYQhEBxgbCDQgQBUAABaIAAGBIDSCnIjSDCMAAAAg/IAAAXQAECiA6CKQA8COB2B1IAAABQDyDxFVAAMBPAAAAIA8hDQBMgoAmhZIAdghg");
	this.shape_1.setTransform(-4.8,11.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F7A70D").s().p("EAmAAgJQgfBBB3BBIgVgKQhWgjhzB1QAoiABehKgEAqgAjIIksjlIgdAhQAOghAIgnQBRB6DZBiQAEACAFAAIAlAAQE9AADmjSIAggeQBohoA7h6QBCiIAKifQACgdgBgeMAAAgkEQAAgFgCgEQgehAhbgnQAbgGAVgMQA0gbAQg6QAHgWAAgaIAAgBIAAjKQABhxgbhmQgliNhXh4Qgng2gzgzQi8i8j6goQhFgLhIAAMhWJAAAQhJAAhEALQj6Aoi9C8Qg4A4gqA9QhRBzgjCGQgaBmAABxIAAFYQAAAGAEAFQBNByCOAxQhpAeg4AvQg2AtgIA+IAAADMAAAAgVQAAAeACAeQAKCiBECKQA7B3BlBlQAUAUAUASQDkDKE4AAMBNtAAAIAGgBQAdgIAZgNIg8BEMhPAAAAQlVAAjyjyIAAAAQh2h2g8iOQg6iKgEihIAAgXMAAAgg/IDSjCIjSinIAAmCQAAhaAQhTQAbiDBEhyQA0hYBNhNQB9h8CXg8QCOg5ClAAMBYuAAAQClAACOA5QCXA8B9B8QBIBIAyBRQBKB3AcCKQARBTAABaIAAD0QgTBqhoAjIB7B7MAAAAkuIAAAUQgECfg4CIQg6CNhzB1IgIAJQjxDylWAAgEg41gNFQhBhZgfh6IAAgeQBwDBByASIiLAtgEA3+gQjQBlgQA0iVIAAAiIgjCwIAPAog");
	this.shape_2.setTransform(-4.8,12.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A45301").s().p("EAqXAieQjZhihRh6QgIAngOAgQglBZhNAoQgZANgdAIIgFABMhNuAAAQk3AAjkjKQgVgTgUgTQhlhlg6h3QhEiKgLiiQgCgeAAgeMAAAggWIAAgDQAIg9A2gtQA4gvBpgeQiNgxhOhzQgEgFAAgGIAAlYQAAhxAbhmQAiiFBRhzQAqg9A4g4QC9i8D6gpQBEgLBJAAMBWJAAAQBJAABEALQD6ApC9C8QAyAyAoA3QBXB4AkCMQAbBmAABxIAADLIAAAAQgBAbgGAWQgRA6g0AbQgUAMgcAFQBbAoAeBAQADAEAAAFMAAAAkEQAAAegCAdQgKCfhCCHQg7B7hnBoIghAeQjmDSk9AAIglAAIAAAAQgFAAgEgCgEAlsAdzQAHADACAGQAMAcATAbQBNBsDDBZIAhAAQEYAADQisQAhgbAegeQBchcA3hrQBFiHALifQACgdAAgdMAAAgj/QgxhVi1ghQgIgBgFgHQgFgGABgIQACgIAGgFQAHgFAIABIABAAIABAAQBvAQA5geQAfgRAOghQAIgVABgbIAAjLQAAhxgchmQgoiOheh3QgfgngkglQjkjklDAAMhWJAAAQlDAAjlDkQgpAqgiAtQhXBzgmCHQgcBmAABxIAAFSQBaB+CwAlQAHABAFAGQAEAFAAAHQAAAHgEAGQgFAFgHACQhgATg9AeQhfAugOBIMAAAAgUQAAAdACAcQALCkBICJQA2BoBZBZQAjAjAlAdQDOClESAAMBNrAAAQCAgnAhigQAEgXADgZQABgGAEgFQAFgFAHgBIADAAQAFAAAEADg");
	this.shape_3.setTransform(-4.9,11.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C06203").s().p("EAqkAh4QjDhZhNhsQgTgbgMgcQgCgGgGgDQgGgEgHABQgHABgFAFQgEAFgBAGQgDAZgEAXQghCgiAAnMhNrAAAQkSAAjOilQglgdgjgjQhZhZg2hoQhIiJgLikQgCgcAAgdMAAAggUQAOhIBfguQA9geBggTQAHgCAFgFQAEgGAAgHQAAgHgEgFQgFgGgHgBQiwglhah+IAAlSQAAhxAchmQAmiHBXhzQAigtApgqQDljkFDAAMBWJAAAQFDAADkDkQAkAlAfAnQBeB3AoCOQAcBmAABxIAADLQgBAbgIAVQgOAhgfARQg5AehvgQIgBAAIgBAAQgIgBgHAFQgGAFgCAIQgBAIAFAGQAFAHAIABQC1AhAxBVMAAAAj/QAAAdgCAdQgLCfhFCHQg3BrhcBcQgeAeghAbQjQCskYAAgEAnEAe0QBOBFCOAvIAlAAQDHAACehhQBJgsBAhBQBEhDAuhNQBOiGAMihQACgbAAgcQgLitgIisQgIimgFimQgFjYgBjXQgBiqADipQADjIAIjGQAGijAKiiIAJiKIlph7ID7hjIBugqIAAjLQAAhygghlQguiShxh3IgPgPQjMjNkiAAUgm4AB1gvRgB1QkiAAjNDNIgaAbQhoBygrCLQggBlAAByIAAEBIAABXQA5BsEWAzQh/AahVA1QhPAygsBJQAPCCAMCBQAQC6AIC6QAICsABCtQACDMgIDNQgHCqgOCqQgNCtgUCsQAAAbACAaQALCmBSCIQAtBKBBBBQBFBGBPAuQCbBaDAAAUArygB7Ah8AB7QBOgQAwhoQAnhUAUiMQgJCHBqBdg");
	this.shape_4.setTransform(-4.9,11.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC6600").s().p("EAqgAgoQiOgvhOhFQEyAJE0AKQieBhjHAAgEgrEAgoQjAAAibhaUApEgBpAsDABLQgwBohOAQUgh8gB7gryAB7gEg1+AWhUAoagCGBDjACIQgMChhOCGUg37gBqgxLABvQhSiIgLimgEg1fAQTQAOiqAHiqUA1KAB+A1hgB2QAFCmAICmgEg1NgAzUA3BgBlAzpABdQgDCpABCqUg1EgCxg1bAC/QgBitgIisgEg2AgKqQAshJBPgyUAgqgCRBJTACoQgKCigGCjUgrfgDHg/uADpQgMiBgPiCgEg2AgVrQAAhyAghlMBrBAAAQAgBlAAByIAADLIhuAqUgzogBbg2rABngEgyzgdaQDNjNEiAAUAvRAB1Am4gB1QEiAADMDNIAPAPUgu9gBag3SABmIAagbg");
	this.shape_5.setTransform(-4.9,11.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-378.3,-217.8,746.9,459.5), null);


(lib.circle_glow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC00").s().p("Ah5B5QgxgyAAhHQAAhGAxgyQAzgzBGAAQBHAAAyAzQAzAyAABGQAABHgzAyQgyAyhHAAQhGAAgzgygAhshtQguAuABA/QgBBAAuAsQAtAuA/AAQBAAAAtguQAugsgBhAQABg/guguQgtgshAgBQg/ABgtAsg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.circle_glow, new cjs.Rectangle(-17.1,-17.1,34.3,34.3), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween8("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-23.1,21.2,1,1,0,0,0,-23.1,21.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:21.3,rotation:-6,y:21.4},9).to({regY:21.2,rotation:0,y:21.2},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24,-22.2,48.2,44.5);


(lib.star = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween6("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.2,scaleY:1.2},4).to({scaleX:1,scaleY:1},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.1,-34.5,68.3,69.1);


(lib.glow_fx = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Tween1a("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(3.8,14.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.8,scaleY:0.8},6).to({scaleX:1,scaleY:1},3).wait(1));

	// Layer 6
	this.instance_1 = new lib.Tween16a("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(3.4,15.5);
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance_1.cache(-11,-11,23,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.58,scaleY:0.58,x:28.7,y:20.8},6).to({scaleX:0.51,scaleY:0.51,x:34.2,y:16.1,alpha:0},3).wait(1));

	// Layer 4
	this.instance_2 = new lib.Tween18a("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(3.4,15.5);
	this.instance_2.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance_2.cache(-11,-11,23,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:0.48,scaleY:0.48,x:-20.4,y:20.1},6).to({scaleX:0.4,scaleY:0.4,x:-23.9,y:20.8,alpha:0},3).wait(1));

	// Layer 3
	this.instance_3 = new lib.Tween19a("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(3.4,15.5);
	this.instance_3.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance_3.cache(-11,-11,23,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:0.55,scaleY:0.55,x:10.9,y:-4.7},6).to({scaleX:0.48,scaleY:0.48,x:12.1,y:-7.7,alpha:0},3).wait(1));

	// Layer 2
	this.instance_4 = new lib.Tween20a("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(3.4,15.5);
	this.instance_4.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance_4.cache(-11,-11,23,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleX:0.62,scaleY:0.62,x:3.1,y:43.1},6).to({scaleX:0.56,scaleY:0.56,x:3,y:47.3,alpha:0},3).wait(1));

	// Layer 1
	this.instance_5 = new lib.Tween21a("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(3.4,15.5);
	this.instance_5.filters = [new cjs.ColorFilter(0, 0, 0, 1, 255, 255, 255, 0)];
	this.instance_5.cache(-11,-11,23,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({scaleX:0.27,scaleY:0.27,x:9.9,y:23.5},6).to({scaleX:0.16,scaleY:0.16,x:10.9,y:24.7,alpha:0},3).wait(1));

	// Layer 9
	this.instance_6 = new lib.circle_glow();
	this.instance_6.parent = this;
	this.instance_6.setTransform(2.3,17.3,0.2,0.2);
	this.instance_6.alpha = 0;
	this.instance_6.shadow = new cjs.Shadow("#FFFFFF",0,0,2);
	this.instance_6.filters = [new cjs.BlurFilter(2, 2, 1)];
	this.instance_6.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({scaleX:1,scaleY:1,alpha:0.309},6).to({scaleX:1.25,scaleY:1.25,alpha:0},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11,0.6,30.8,36.2);


(lib.eys = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Tween10("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-26.2,5.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({startPosition:0},7).to({regY:0.1,scaleY:0.59,y:5.8},5).to({regY:0,scaleY:1,y:5.7},5).to({startPosition:0},2).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhXCIQg4gkgPhBQgOhBAkg5QAlg4BBgPQBBgOA5AkIAMAJQAuAjANA6QAOBBgkA5QgYAlgkASQgUAKgXAFQgTAFgRAAQgsAAgpgbg");
	this.shape.setTransform(-26.3,5.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F5E8DE").s().p("AhpClQhEgsgRhQIgBgCQgQhNArhDIABgDQAshDBOgRQBPgRBFAtQBEAsARBPQARBPgrBEQgsBFhQARQgXAFgVAAQg2AAgxgggAgiieQhBAPglA4QgkA5AOBBQAPBBA4AkQA5AlBAgPQAXgFAUgKQAkgSAYglQAkg5gOhBQgNg6gugjIgMgJQgpgaguAAQgRAAgSAEg");
	this.shape_1.setTransform(-26.3,5.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-46,-14,39.4,39.5);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.star("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-40.8,-55.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("Ak8qNIATgJIJmUlIgTAJg");
	this.shape.setTransform(-3.9,22.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("AlAqMIAbgMIJmUlIgbANg");
	this.shape_1.setTransform(-1.6,21.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EDAF71").s().p("AAUA4IgphPQgNgogHgRQgLgeglgNIgIAAQAQgOATgKQAFAYAoAJQAfAHAVgEIAyCmIANAqQgHAdghAJQgHAIgHAFQgCgtgWgvg");
	this.shape_2.setTransform(6.6,50.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AghCeQhbguAMhhQALhgBLghIAIAAQAkANALAeQAHARANAoIAqBPQAWAvACAtIgeAZQgJAHgQAAQggAAg9gfgAA1hpQgngJgGgYIADgCQACgKAEgHQANgVAmgKIAMAAIAaBWQgHACgJAAQgQAAgVgFg");
	this.shape_3.setTransform(-0.5,49.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D00E68").s().p("AkZDrIgMgqQE5lBEIiPIAKAOQjTCPlhGCg");
	this.shape_4.setTransform(44.5,37);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FE2A8C").s().p("AkvBYIgahXIgFgRQFji8E6gxIAAA5IgKgNQkICPk5FBg");
	this.shape_5.setTransform(40.5,30.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-74.9,-89.6,148.9,178.3);


(lib.mascot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#990000").ss(1,1,1).p("AKOmoQAEALgIAOQgEAEgDAFQgRASgcAIQgcAJgYgGQgYgEgFgRQgFgQAQgSQANgMATgJQAGgDAIgCQAcgIAXAFQAYAFAFAQgAg8lcIDzF7IEdk9IABgCIBAhGQjCFLBkE0QAEAKAEALQgfgDgegBQk3gRjwCXQgMAIgMAIQgkkmkhi/IBlAjIADABIFcB2IAAnPQgFABgCABQgdAJgXgFQgYgGgFgPQgCgIADgIQACgIAJgKQARgRAcgJQAcgIAXAEQAZAFAEAQQABAEAAADQABANgOAOQgNAPgWAHQgCACAAAAIgBgEgAoEhGQAFAQgRASQgRARgcAJQgHACgHACQgUADgSgDQgXgGgEgQQgGgQARgRQAQgSAcgJQALgDAKgBQAQgCAPADQAXAFAGAQg");
	this.shape.setTransform(-28.3,-99.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("AkRAOIBsA5IAHADQAhALAhgLIACgBIABAAQAfgKAVgbIACgCQASgaABgiIAAgIIgVikIBCCKIACADQASAfAiANQAeAMAggHIABAAQAigHAXgbIAEgEIBChjQgXCFAMBoQj0gfjQCJQgXhbg8hdg");
	this.shape_1.setTransform(-22.2,-85.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF9900").s().p("AohgqIBlAjQDpDfAPDTIgYAQQgkkmkhi/gAFjElQk3gRjwCXQD2jLExBFgAFjElQg5jZCMlxIBAhGQjCFLBkE0Ig1ARIAAAAgAhwCmIlJisIFcB2IAAnPIA5HOQAAATgKAPQgMAPgSAFQgJAEgJAAQgJAAgJgDgACCBPQgRgHgJgQIjFmXIACgCIgDgCIABAEIgHACQgdAJgXgFQgYgGgFgPQgCgIADgIQAbAIAngEQArgEAPgJIANgLQABANgOAOQgNAPgWAHIDzF7IEdk9IjyFjQgMAOgSADQgHACgGAAQgLAAgLgEgAppgMQAQgXgDgiIgDgcQAQgCAPADQAXAFAGAQQAFAQgRASQgRARgcAJIgOAEIABgBgAIDluQgYgEgFgRQgFgQAQgSQANgMATgJQAKAdAgAHQAOAEAigCIgHAJQgRASgcAIQgRAGgPAAQgLAAgJgDg");
	this.shape_2.setTransform(-25.2,-99.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC00").s().p("AmdAFIADABIgDgBIgBgCIAEADIFJCtQASAGASgHQASgFAMgPQAKgPAAgTIg5nOIDFGXQAJAQARAHQARAGASgEQASgDAMgOIDyljIABgCQiMFxA5DZQkxhFj2DLQgPjTjpjggAiBFfQDQiKD0AfQgMhpAXiFIhCBjIgEAEQgXAbgiAHIgBAAQggAHgfgMQghgNgSgfIgCgDIhDiIIAVCiIAAAIQgBAjgSAaIgCACQgVAcgeAJIgBAAIgCABQghALghgLIgHgDIhsg5QA8BdAXBcgAmdAFgApxABQgXgFgEgQQgGgQARgRQAQgSAcgJQALgDAKgBIADAcQADAigQAWIgBABIgSABQgKAAgKgBgAHVkYIAAAAIgBACgAiVlxQACgIAJgKQARgRAcgJQAcgIAXAEQAZAFAEAQIABAHIgNALQgPAJgrAEIgYABQgYAAgSgFgAJamJQgggHgKgdQAGgDAIgCQAcgIAXAFQAYAFAFAQQAEALgIAOIgVABQgSAAgJgDg");
	this.shape_3.setTransform(-28.3,-100.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(20));

	// Layer 14
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-10.7,-14.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({startPosition:0},7).to({x:-9.9,y:-13.4},5).to({x:-10.7,y:-14.9},5).wait(3));

	// Layer 9
	this.instance_1 = new lib.eys("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(15.6,-11.1,1,1,-5,0,0,-26.3,5.6);

	this.instance_2 = new lib.eys("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-35.4,-3,1,1,-5,0,0,-26.4,5.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1}]}).wait(20));

	// Layer 13
	this.instance_3 = new lib.Tween9("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(8.3,-59,1,1,0,0,0,17.2,-20);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regY:-20.1,rotation:-6.7,x:8.4},14).to({regY:-20,rotation:0,x:8.3},5).wait(1));

	// Layer 16
	this.instance_4 = new lib.Tween11("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(-0.3,26.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleX:0.8},13).to({scaleX:1},6).wait(1));

	// Layer 3
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F7C1BB").s().p("Ak+BzQgYgPgGgdQgDgMABgLQACgPAJgPQAQgXAcgGQAdgGAYAPQAZAPAGAcQADAPgDAOQgCANgIALQgPAZgdAGQgHACgIAAQgUAAgSgMgAD+gKQgVgOgGgZIAAgBQgGgZAOgWQAOgWAagGQAVgEATAJIAHAEQAWAOAGAZQAFAagOAWQgOAWgZAFQgIACgGAAQgSAAgQgKg");
	this.shape_4.setTransform(1.2,29);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF9900").s().p("ApsF5Qg5kFCQjfQCPjhEEg5QECg4DfCRQDgCRA5EEIAFAdQg+jujZhuQjZhukCA8QkDA7iMC5QiMC2ArD0IgHgdg");
	this.shape_5.setTransform(-8,-39.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF3300").s().p("AooGeQgwhPgZhfQgrjzCMi3QCMi5EDg7QECg8DZBuQDZBuA+DvQAOBMgEBJQgDAogHAnQAAgjgGgkQgDgXgGgXQgRhQgkhEQg1hnhfhKQgcgVgegUIgDgBQjPiBjyA1QjzA1iHDLIgBACQgUAfgQAeQg4BqgGB0QgFBMASBSIAMAtQAHAXAJAWIAKAbQgNgTgMgUg");
	this.shape_6.setTransform(-6.8,-22.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("An/G1IgZgdIgNgTIgLgaQgJgXgHgXIgMgtQgRhRAFhNQAGhzA3hqQAQgfAUgeIACgCQCHjMDzg1QDxg0DQCBIACABQAfAUAbAVQBfBJA2BnQAjBEASBRQAFAXADAWQAGAlABAjIgGAaQgFAWgIAWQAFg1gIg4QgCgVgFgVQgQhJgig9QgyhdhahDQgagTgegRIgCgBQjGhzjpAyQjqAziFC8IgBACQgTAbgQAcQg3BhgHBqQgGBGAQBKIALApQAHAVAIAVQAMAeAPAcIACACIgGgGg");
	this.shape_7.setTransform(-4.6,-16.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F7DBC6").s().p("AhVHNQhNgGhagiQhZgjg9gqIhQg5IgagUIgOgMQgQgcgMgeQgIgVgHgVQgZi8B0inIADgFQARgYASgVQB/iXDOgtQDNgsCyBUIACABQAYALAYAOIAGAEQCRBWA+CMQANAdAJAfQAIA4gFA0QgLAjgPAjIgKAQQgwBLg8A7QgpAnhOA+QhNA9h2AiQhlAchGAAIgYgBgAj9EBQgcAGgRAYQgIAOgCAQQgBALACAMQAGAdAZAPQAZAQAcgGQAcgGAQgZQAIgLACgNQADgOgDgPQgHgcgZgQQgRgLgUAAQgHAAgIACgAE8CPQgZAGgOAWQgOAWAFAZIABABQAFAZAWAOQAWAOAZgGQAagFAOgWQAOgWgGgaQgFgZgWgOIgIgEQgMgGgOAAQgGAAgIABg");
	this.shape_8.setTransform(-3,2.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FAB47F").s().p("Al7GUQhLgxg4g9IgBgCIAOAMIAaAUIBQA5QA9AqBaAjQBaAiBNAGQBMAGB2giQB2ghBOg+QBNg9ApgnQA8g7AwhLIALgRQgUAtgcArQiPDgkDA4QhJAQhFAAQi0AAihhogAo0CXQgQhKAFhGQAIhpA3hiQAPgcAUgbIABgCQCEi8DqgzQDpgyDHBzIACABQAdARAaATQBbBDAyBdQAhA9AQBKQAFAVADAVQgJgfgNgdQg+iMiShWIgFgEQgYgOgYgLIgCgBQizhUjMAsQjOAth/CXQgTAVgQAYIgEAFQh0CmAaC9IgLgpg");
	this.shape_9.setTransform(-4.1,-1.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]}).wait(20));

	// Layer 10
	this.instance_5 = new lib.Symbol4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-75.4,-63,1,1,0,0,180);

	this.instance_6 = new lib.Symbol4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(45.5,-83.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6},{t:this.instance_5}]}).wait(20));

	// Layer 12
	this.instance_7 = new lib.Tween5("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(17.1,33.6,1,1,0,0,0,-29.2,-42.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({regY:-42.1,rotation:1.7,y:33.7},9).to({regY:-42.2,rotation:0,y:33.6},10).wait(1));

	// Layer 7
	this.instance_8 = new lib.Tween7("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(-8.2,42.7,1,1,-25.9,0,0,72,13.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({regY:13.5,rotation:-3.6,y:42.8},9).to({regY:13.4,rotation:-25.9,y:42.7},10).wait(1));

	// Layer 15
	this.instance_9 = new lib.glow_fx("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(-129.9,41.4,1.749,1.749,-20,0,0,5.5,25.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({scaleX:1.75,scaleY:1.75,rotation:-19.8,x:-114.6,y:-7.7,startPosition:9},9).to({scaleX:1.75,scaleY:1.75,rotation:-20,x:-129.9,y:41.4},10).wait(1));

	// Layer 4
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CBFAFA").s().p("AhyHDQh5pPFbmWIAiAAQkuGwBoKVQgjhUgbgMg");
	this.shape_10.setTransform(-17.4,88.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#00CCFF").s().p("AmjIkQiOqtE5m9IBSAAQgGAtAHAwQghFrKPHgQBHFDpSigQgjBChyAAQhSAAh6gjg");
	this.shape_11.setTransform(31.2,92.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#66FFFF").s().p("AoSIQQhpqVEwmwIBaAAQk5G9COKtQhOhZgoA0gAHQFzQqPngAhlrQAyFFKVGsQAUBihCAAQgSAAgZgIg");
	this.shape_12.setTransform(30.5,90.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0066CC").s().p("Al3lKQgIgvAGgtIBNAAQhTFEL7HTQgWAcgWAaQqWmsgxlFg");
	this.shape_13.setTransform(52.2,76.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CBFAFA").s().p("AhpHIQiCpSFWmYIAjAAQkxGyB2KTQgkhSgYgJg");
	this.shape_14.setTransform(-16.9,89);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#66FFFF").s().p("AoNISQh2qTEymzIBagBQk8HBCcKqQhNhWgpAygAHTFrQqVnQAfl1QA7FSKTGYQARBhhBAAQgRAAgXgGg");
	this.shape_15.setTransform(31.1,90.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#00CCFF").s().p("AmdInQicqqE7nBIBSgBQgFAtAIAvQgfF1KUHQQA+E4pGiQQgjBCh2AAQhRAAh3gfg");
	this.shape_16.setTransform(31.7,92.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0066CC").s().p("Al6lGQgIgvAFgtIBNgBQhHFUL3G9QgQAdgcAZQqUmZg6lRg");
	this.shape_17.setTransform(52.6,76);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CBFAFA").s().p("AhgHOQiLpVFSmcIAjAAQkzG2CDKRQglhTgVgDg");
	this.shape_18.setTransform(-16.5,89.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#66FFFF").s().p("AoIIUQiDqRE0m3IBagCQk+HGCqKnQhNhTgqAwgAHWFjQqanAAcl+QBEFeKSGEQANBhhBAAQgQAAgUgFg");
	this.shape_19.setTransform(31.7,90.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#00CCFF").s().p("AmYIqQiqqnE+nGIBSgBQgFAtAJAvQgdF+KaHAQA1Eto7iAQgiBCh7AAQhPAAh1gbg");
	this.shape_20.setTransform(32.3,92.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#0066CC").s().p("Al9lBQgJgwAFgtIBNgCQg7FlLyGlQgKAeghAZQqSmGhDlcg");
	this.shape_21.setTransform(53,75.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CBFAFA").s().p("AhXHUQiUpYFPmfIAigBQk1G6CQKPQglhSgTABg");
	this.shape_22.setTransform(-16.1,89.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#66FFFF").s().p("AoDIWQiQqPE2m6IBagEQlBHKC4KlQhMhQgrAugAHZFbQqgmxAamHQBMFrKRFwQALBhhBAAQgPAAgSgEg");
	this.shape_23.setTransform(32.3,90.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#00CCFF").s().p("AmSItQi4qlFBnJIBSgDQgFAuAJAvQgaGHKgGwQArEjowhxQghBDiBAAQhNAAhxgYg");
	this.shape_24.setTransform(32.9,92);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#0066CC").s().p("AmAk+QgJguAEguIBNgCQguF1LtGNQgEAfgnAYQqQlxhMlqg");
	this.shape_25.setTransform(53.4,75);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#CBFAFA").s().p("AhOHaQidpbFLmjIAigBQk3G+CdKNQgmhSgQAGg");
	this.shape_26.setTransform(-15.7,89.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#66FFFF").s().p("An9IYQidqOE3m9IBagEQlDHODGKhQhLhLgsArgAHdFTQqmmgAYmRQBVF3KOFcQAIBhhBAAQgNAAgPgDg");
	this.shape_27.setTransform(32.8,91);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#00CCFF").s().p("AmMIvQjGqhFDnOIBSgDQgEAtAJAvQgYGQKmGgQAiEYolhgQgfBDiHAAQhMAAhtgVg");
	this.shape_28.setTransform(33.5,91.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#0066CC").s().p("AmDk6QgKgvAEgtIBNgCQgiGELpF2QABAhgsAXQqOlehVl2g");
	this.shape_29.setTransform(53.8,74.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#CBFAFA").s().p("AhFHgQimpeFGmlIAjgDQk6HBCrKMQgnhSgNALg");
	this.shape_30.setTransform(-15.3,89.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#66FFFF").s().p("An4IZQirqME7nBIBZgDQlFHRDUKeQhLhHgtAogAHgFLQqsmSAVmYQBfGDKNFIQAEBhhBAAQgLAAgNgCg");
	this.shape_31.setTransform(33.4,91.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#00CCFF").s().p("AmGIyQjUqfFFnRIBSgFQgEAuAKAvQgVGZKsGRQAYENoZhRQgfBEiPAAQhJAAhogSg");
	this.shape_32.setTransform(34,91.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#0066CC").s().p("AmHk1QgKgvAEguIBMgDQgVGVLkFfQAHAigwAWQqNlKhfmCg");
	this.shape_33.setTransform(54.2,74);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CBFAFA").s().p("Ag8HlQiupgFCmpIAigCQk7HEC4KJQgohRgLAPg");
	this.shape_34.setTransform(-14.9,89.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#66FFFF").s().p("AnyIbQi4qKE8nEIBagFQlIHWDiKbQhLhEgtAmgAHjFDQqxmBATmjQBnGQKLE0QABBihAAAQgKAAgLgCg");
	this.shape_35.setTransform(34,91.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#00CCFF").s().p("AmAI0QjiqbFInWIBSgFQgEAtALAvQgTGiKxGBQAQECoPhAQgeBEiVAAQhIAAhjgPg");
	this.shape_36.setTransform(34.6,91.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#0066CC").s().p("AmLkxQgKgvADgtIBMgFQgJGlLgFIQANAjg2AVQqLk1homPg");
	this.shape_37.setTransform(54.7,73.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#CBFAFA").s().p("AgzHrQi3pjE+mtIAigCQk9HIDFKHQgphRgIAUg");
	this.shape_38.setTransform(-14.5,90);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#66FFFF").s().p("AntIcQjFqHE+nIIBagGQlKHaDvKZQhKhBguAjgAHnE6Qq4lxARmsQBwGdKJEgQgBBihAAAIgRgCg");
	this.shape_39.setTransform(34.6,91.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#00CCFF").s().p("Al7I2QjvqYFKnaIBSgGQgDAtALAvQgRGsK4FxQAGD3oEgwQgdBFifAAQhFAAhdgNg");
	this.shape_40.setTransform(35.2,91.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#0066CC").s().p("AmPktQgLguADguIBMgGQAEG2LbEwQASAlg7ATQqKkghwmcg");
	this.shape_41.setTransform(55.2,73);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#CBFAFA").s().p("AgpHxQjApmE6mwIAigDQlAHMDSKFQgphRgFAZg");
	this.shape_42.setTransform(-14.2,90.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#66FFFF").s().p("AnoIeQjSqFFAnLIBagHQlNHeD+KVQhKg9gvAhgAHqEyQq9lhAOm1QB5GpKIEMQgGBig/AAIgNgBg");
	this.shape_43.setTransform(35.2,91.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#00CCFF").s().p("Al1I4Qj+qVFNneIBSgHQgDAtAMAvQgOG1K9FhQgEDtn4ggQgbBGiqAAQhBAAhXgLg");
	this.shape_44.setTransform(35.8,91.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#0066CC").s().p("AmTkpQgLgvACgtIBMgGQAQHFLWEZQAZAmhBATQqIkNh5mog");
	this.shape_45.setTransform(55.7,72.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#CBFAFA").s().p("AggH2QjIpoE1m0IAigCQlBHODeKEQgphQgDAcg");
	this.shape_46.setTransform(-13.8,90.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#00CCFF").s().p("AlvI7QkMqTFPniIBSgIQgCAuAMAuQgLG+LCFSQgMDintgRQgbBIi3AAQg9AAhOgIg");
	this.shape_47.setTransform(36.3,91.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#66FFFF").s().p("AniIgQjgqDFDnPIBZgIQlOHjELKSQhJg5gwAegAHtEqQrClSALm9QCCG1KGD4QgJBjg+AAIgKgBg");
	this.shape_48.setTransform(35.7,91.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#0066CC").s().p("AmXklQgMguACguIBMgHQAcHWLSEBQAeAohGASQqGj5iCm1g");
	this.shape_49.setTransform(56.2,72);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#CBFAFA").s().p("AgoHxQjBpmE6mwIAigDQlAHMDTKFQgphQgFAYg");
	this.shape_50.setTransform(-14.2,90.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#66FFFF").s().p("AnnIfQjTqFFAnMIBagHQlNHeD/KWQhKg+gvAigAHrExQq+lgAOm1QB6GqKHEKQgGBig+AAIgNgBg");
	this.shape_51.setTransform(35.2,91.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#00CCFF").s().p("Al1I5Qj/qVFNnfIBSgHQgCAuALAvQgNG1K9FgQgEDsn3gfQgcBGisAAQhBAAhVgKg");
	this.shape_52.setTransform(35.8,91.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#0066CC").s().p("AmTkoQgMgvADguIBMgGQARHHLVEXQAaAmhCATQqHkLh6mpg");
	this.shape_53.setTransform(55.8,72.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#CBFAFA").s().p("AgxHsQi4pjE9muIAigCQk+HJDIKGQgphQgIAUg");
	this.shape_54.setTransform(-14.5,90.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#66FFFF").s().p("AnsIdQjIqHE/nIIBagHQlLHbDzKYQhKhAgvAjgAHnE5Qq4luAQmuQByGfKJEcQgDBig/AAIgRgBg");
	this.shape_55.setTransform(34.7,91.2);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#00CCFF").s().p("Al5I3QjzqYFLnbIBSgGQgDAuALAuQgQGuK4FuQAFD1oCgtQgcBFiiAAQhEAAhbgMg");
	this.shape_56.setTransform(35.3,91.5);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#0066CC").s().p("AmPksQgMgvADgtIBMgGQAGG4LaEsQAUAlg8AUQqKkdhxmeg");
	this.shape_57.setTransform(55.3,72.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#CBFAFA").s().p("Ag5HnQixphFBmrIAigCQk8HGC8KJQgohRgKAQg");
	this.shape_58.setTransform(-14.8,89.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#66FFFF").s().p("AnxIbQi8qIE9nGIBagFQlJHXDmKaQhKhDguAlgAHkFAQqzl8ASmlQBqGTKLEuQAABihAAAQgJAAgLgCg");
	this.shape_59.setTransform(34.2,91.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#00CCFF").s().p("Al/I1QjmqbFJnXIBSgFQgDAtAKAvQgSGlKzF8QAND/oLg7QgeBEiZAAQhHAAhhgOg");
	this.shape_60.setTransform(34.8,91.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#0066CC").s().p("AmMkvQgKgwADgtIBMgFQgFGqLeFBQAOAkg3AUQqLkvhqmSg");
	this.shape_61.setTransform(54.9,73.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#CBFAFA").s().p("AhCHiQippeFFmoIAigCQk6HDCwKKQgnhRgNAMg");
	this.shape_62.setTransform(-15.1,89.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#66FFFF").s().p("An2IZQiwqKE7nCIBagFQlGHUDZKdQhLhHgtAngAHhFHQqumKAUmdQBiGIKMFAQAEBihBAAQgLAAgMgDg");
	this.shape_63.setTransform(33.7,91.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#00CCFF").s().p("AmEIyQjZqdFGnTIBSgFQgEAtAKAvQgUGdKuGLQAVEIoVhKQgfBFiRAAQhJAAhmgSg");
	this.shape_64.setTransform(34.3,91.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#0066CC").s().p("AmIk0QgKgvADgtIBMgEQgQGbLiFWQAKAjgzAVQqMlBhimIg");
	this.shape_65.setTransform(54.4,73.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#CBFAFA").s().p("AhKHdQihpcFImlIAjgBQk4G/CkKMQgnhSgPAJg");
	this.shape_66.setTransform(-15.5,89.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#66FFFF").s().p("An7IYQikqME5m/IBagEQlEHPDNKgQhMhJgsApgAHeFOQqpmYAXmVQBZF9KOFSQAHBihBAAQgMAAgPgEg");
	this.shape_67.setTransform(33.2,91);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#00CCFF").s().p("AmJIwQjNqgFEnPIBSgEQgEAtAKAvQgXGVKpGYQAdETofhYQgfBDiKAAQhLAAhrgUg");
	this.shape_68.setTransform(33.8,91.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#0066CC").s().p("AmFk3QgKgvAEgtIBNgEQgcGNLnFrQAEAhguAWQqOlThal8g");
	this.shape_69.setTransform(54,74.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#CBFAFA").s().p("AhSHYQiZpaFMmiIAigBQk2G9CYKNQgmhSgRAFg");
	this.shape_70.setTransform(-15.9,89.5);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#66FFFF").s().p("AoAIXQiYqOE3m8IBagEQlCHMDBKjQhMhNgsAsgAHbFWQqkmnAZmMQBSFyKPFkQAJBhhAAAQgOAAgRgEg");
	this.shape_71.setTransform(32.6,90.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#00CCFF").s().p("AmOIuQjBqjFCnMIBSgDQgEAuAJAvQgZGMKkGnQAmEcoqhmQggBDiEAAQhNAAhugXg");
	this.shape_72.setTransform(33.2,91.9);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#0066CC").s().p("AmCk7QgKgvAFguIBMgCQgnF/LrF/QgBAggpAXQqQllhRlxg");
	this.shape_73.setTransform(53.7,74.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#CBFAFA").s().p("AhaHSQiRpWFQmfIAigBQk0G5CMKQQglhTgUAAg");
	this.shape_74.setTransform(-16.2,89.3);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#66FFFF").s().p("AoEIVQiMqQE1m5IBagCQlAHIC0KlQhMhQgrAugAHYFdQqem1AbmEQBJFnKRF2QAMBhhBAAQgPAAgTgFg");
	this.shape_75.setTransform(32.1,90.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#00CCFF").s().p("AmUIsQizqlE/nJIBSgCQgFAtAJAvQgbGFKfG1QAuEmo0h1QggBCh/AAQhOAAhzgZg");
	this.shape_76.setTransform(32.7,92);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#0066CC").s().p("Al/k/QgJgvAFgtIBMgCQgyFwLvGUQgGAfglAYQqRl3hJlmg");
	this.shape_77.setTransform(53.3,75.2);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#CBFAFA").s().p("AhiHNQiJpUFTmbIAjgBQkyG2CAKRQglhTgWgEg");
	this.shape_78.setTransform(-16.6,89.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#66FFFF").s().p("AoJITQiAqREzm2IBagCQk9HFCnKoQhNhUgqAwgAHWFkQqanDAdl8QBCFcKSGIQAOBhhBAAQgQAAgUgGg");
	this.shape_79.setTransform(31.5,90.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#00CCFF").s().p("AmZIpQinqoE9nEIBSgCQgFAtAIAwQgdF8KaHDQA2Evo9iDQgiBDh6AAQhQAAh1gdg");
	this.shape_80.setTransform(32.2,92.2);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#0066CC").s().p("Al9lCQgIgwAFgtIBNgBQg9FhLzGqQgMAeggAYQqSmJhClag");
	this.shape_81.setTransform(52.9,75.6);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#CBFAFA").s().p("AhqHIQiBpSFXmYIAiAAQkwGyB0KTQgjhTgZgIg");
	this.shape_82.setTransform(-17,89);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#66FFFF").s().p("AoOISQh0qTExmzIBagBQk7HBCbKqQhNhWgqAygAHSFsQqUnRAgl1QA5FRKUGaQARBihCAAQgRAAgXgHg");
	this.shape_83.setTransform(31,90.8);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#00CCFF").s().p("AmeInQiaqqE7nBIBSgBQgGAtAIAvQgfF0KUHSQA/E5pIiSQgjBCh1AAQhRAAh4gfg");
	this.shape_84.setTransform(31.7,92.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#0066CC").s().p("Al6lGQgIgvAGgtIBMgBQhIFTL4G+QgRAdgbAZQqUmbg6lPg");
	this.shape_85.setTransform(52.6,76);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14}]},1).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]},1).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22}]},1).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26}]},1).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30}]},1).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34}]},1).to({state:[{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38}]},1).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42}]},1).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46}]},1).to({state:[{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50}]},1).to({state:[{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54}]},1).to({state:[{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58}]},1).to({state:[{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62}]},1).to({state:[{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66}]},1).to({state:[{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70}]},1).to({state:[{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74}]},1).to({state:[{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78}]},1).to({state:[{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82}]},1).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]},1).wait(1));

	// Layer 4
	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFF00").s().p("AmjIkQiOqtE5m9IBSAAQgGAtAHAwQghFrKPHgQBHFDpSigQgjBChyAAQhSAAh6gjg");
	this.shape_86.setTransform(31.2,92.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FF9900").s().p("Al3lKQgIgvAGgtIBNAAQhTFEL7HTQgWAcgWAaQqWmsgxlFg");
	this.shape_87.setTransform(52.2,76.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFCC00").s().p("AoSIQQhpqVEwmwIBaAAQk5G9COKtQhOhZgoA0gAHQFzQqPngAhlrQAyFFKVGsQAUBihCAAQgSAAgZgIg");
	this.shape_88.setTransform(30.5,90.7);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFCC").s().p("AhyHDQh5pPFbmWIAiAAQkuGwBoKVQgjhUgbgMg");
	this.shape_89.setTransform(-17.4,88.8);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFCC").s().p("AhpHIQiCpSFWmYIAjAAQkxGyB2KTQgkhSgYgJg");
	this.shape_90.setTransform(-16.9,89);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFCC00").s().p("AoNISQh2qTEymzIBagBQk8HBCcKqQhNhWgpAygAHTFrQqVnQAfl1QA7FSKTGYQARBhhBAAQgRAAgXgGg");
	this.shape_91.setTransform(31.1,90.8);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFF00").s().p("AmdInQicqqE7nBIBSgBQgFAtAIAvQgfF1KUHQQA+E4pGiQQgjBCh2AAQhRAAh3gfg");
	this.shape_92.setTransform(31.7,92.3);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FF9900").s().p("Al6lGQgIgvAFgtIBNgBQhHFUL3G9QgQAdgcAZQqUmZg6lRg");
	this.shape_93.setTransform(52.6,76);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFCC").s().p("AhgHOQiLpVFSmcIAjAAQkzG2CDKRQglhTgVgDg");
	this.shape_94.setTransform(-16.5,89.2);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFCC00").s().p("AoIIUQiDqRE0m3IBagCQk+HGCqKnQhNhTgqAwgAHWFjQqanAAcl+QBEFeKSGEQANBhhBAAQgQAAgUgFg");
	this.shape_95.setTransform(31.7,90.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFF00").s().p("AmYIqQiqqnE+nGIBSgBQgFAtAJAvQgdF+KaHAQA1Eto7iAQgiBCh7AAQhPAAh1gbg");
	this.shape_96.setTransform(32.3,92.1);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FF9900").s().p("Al9lBQgJgwAFgtIBNgCQg7FlLyGlQgKAeghAZQqSmGhDlcg");
	this.shape_97.setTransform(53,75.5);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFCC").s().p("AhXHUQiUpYFPmfIAigBQk1G6CQKPQglhSgTABg");
	this.shape_98.setTransform(-16.1,89.4);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFCC00").s().p("AoDIWQiQqPE2m6IBagEQlBHKC4KlQhMhQgrAugAHZFbQqgmxAamHQBMFrKRFwQALBhhBAAQgPAAgSgEg");
	this.shape_99.setTransform(32.3,90.9);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFF00").s().p("AmSItQi4qlFBnJIBSgDQgFAuAJAvQgaGHKgGwQArEjowhxQghBDiBAAQhNAAhxgYg");
	this.shape_100.setTransform(32.9,92);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FF9900").s().p("AmAk+QgJguAEguIBNgCQguF1LtGNQgEAfgnAYQqQlxhMlqg");
	this.shape_101.setTransform(53.4,75);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFCC").s().p("AhOHaQidpbFLmjIAigBQk3G+CdKNQgmhSgQAGg");
	this.shape_102.setTransform(-15.7,89.5);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFCC00").s().p("An9IYQidqOE3m9IBagEQlDHODGKhQhLhLgsArgAHdFTQqmmgAYmRQBVF3KOFcQAIBhhBAAQgNAAgPgDg");
	this.shape_103.setTransform(32.8,91);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFF00").s().p("AmMIvQjGqhFDnOIBSgDQgEAtAJAvQgYGQKmGgQAiEYolhgQgfBDiHAAQhMAAhtgVg");
	this.shape_104.setTransform(33.5,91.9);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FF9900").s().p("AmDk6QgKgvAEgtIBNgCQgiGELpF2QABAhgsAXQqOlehVl2g");
	this.shape_105.setTransform(53.8,74.5);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFCC").s().p("AhFHgQimpeFGmlIAjgDQk6HBCrKMQgnhSgNALg");
	this.shape_106.setTransform(-15.3,89.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFCC00").s().p("An4IZQirqME7nBIBZgDQlFHRDUKeQhLhHgtAogAHgFLQqsmSAVmYQBfGDKNFIQAEBhhBAAQgLAAgNgCg");
	this.shape_107.setTransform(33.4,91.1);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFF00").s().p("AmGIyQjUqfFFnRIBSgFQgEAuAKAvQgVGZKsGRQAYENoZhRQgfBEiPAAQhJAAhogSg");
	this.shape_108.setTransform(34,91.7);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FF9900").s().p("AmHk1QgKgvAEguIBMgDQgVGVLkFfQAHAigwAWQqNlKhfmCg");
	this.shape_109.setTransform(54.2,74);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFCC").s().p("Ag8HlQiupgFCmpIAigCQk7HEC4KJQgohRgLAPg");
	this.shape_110.setTransform(-14.9,89.8);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFCC00").s().p("AnyIbQi4qKE8nEIBagFQlIHWDiKbQhLhEgtAmgAHjFDQqxmBATmjQBnGQKLE0QABBihAAAQgKAAgLgCg");
	this.shape_111.setTransform(34,91.1);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFFF00").s().p("AmAI0QjiqbFInWIBSgFQgEAtALAvQgTGiKxGBQAQECoPhAQgeBEiVAAQhIAAhjgPg");
	this.shape_112.setTransform(34.6,91.6);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FF9900").s().p("AmLkxQgKgvADgtIBMgFQgJGlLgFIQANAjg2AVQqLk1homPg");
	this.shape_113.setTransform(54.7,73.5);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFFFCC").s().p("AgzHrQi3pjE+mtIAigCQk9HIDFKHQgphRgIAUg");
	this.shape_114.setTransform(-14.5,90);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFCC00").s().p("AntIcQjFqHE+nIIBagGQlKHaDvKZQhKhBguAjgAHnE6Qq4lxARmsQBwGdKJEgQgBBihAAAIgRgCg");
	this.shape_115.setTransform(34.6,91.2);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFF00").s().p("Al7I2QjvqYFKnaIBSgGQgDAtALAvQgRGsK4FxQAGD3oEgwQgdBFifAAQhFAAhdgNg");
	this.shape_116.setTransform(35.2,91.5);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FF9900").s().p("AmPktQgLguADguIBMgGQAEG2LbEwQASAlg7ATQqKkghwmcg");
	this.shape_117.setTransform(55.2,73);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFFFCC").s().p("AgpHxQjApmE6mwIAigDQlAHMDSKFQgphRgFAZg");
	this.shape_118.setTransform(-14.2,90.2);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFCC00").s().p("AnoIeQjSqFFAnLIBagHQlNHeD+KVQhKg9gvAhgAHqEyQq9lhAOm1QB5GpKIEMQgGBig/AAIgNgBg");
	this.shape_119.setTransform(35.2,91.2);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFFF00").s().p("Al1I4Qj+qVFNneIBSgHQgDAtAMAvQgOG1K9FhQgEDtn4ggQgbBGiqAAQhBAAhXgLg");
	this.shape_120.setTransform(35.8,91.4);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FF9900").s().p("AmTkpQgLgvACgtIBMgGQAQHFLWEZQAZAmhBATQqIkNh5mog");
	this.shape_121.setTransform(55.7,72.5);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFFFCC").s().p("AggH2QjIpoE1m0IAigCQlBHODeKEQgphQgDAcg");
	this.shape_122.setTransform(-13.8,90.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFCC00").s().p("AniIgQjgqDFDnPIBZgIQlOHjELKSQhJg5gwAegAHtEqQrClSALm9QCCG1KGD4QgJBjg+AAIgKgBg");
	this.shape_123.setTransform(35.7,91.3);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFFF00").s().p("AlvI7QkMqTFPniIBSgIQgCAuAMAuQgLG+LCFSQgMDintgRQgbBIi3AAQg9AAhOgIg");
	this.shape_124.setTransform(36.3,91.3);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FF9900").s().p("AmXklQgMguACguIBMgHQAcHWLSEBQAeAohGASQqGj5iCm1g");
	this.shape_125.setTransform(56.2,72);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFFFCC").s().p("AgoHxQjBpmE6mwIAigDQlAHMDTKFQgphQgFAYg");
	this.shape_126.setTransform(-14.2,90.2);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFCC00").s().p("AnnIfQjTqFFAnMIBagHQlNHeD/KWQhKg+gvAigAHrExQq+lgAOm1QB6GqKHEKQgGBig+AAIgNgBg");
	this.shape_127.setTransform(35.2,91.3);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFFF00").s().p("Al1I5Qj/qVFNnfIBSgHQgCAuALAvQgNG1K9FgQgEDsn3gfQgcBGisAAQhBAAhVgKg");
	this.shape_128.setTransform(35.8,91.4);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FF9900").s().p("AmTkoQgMgvADguIBMgGQARHHLVEXQAaAmhCATQqHkLh6mpg");
	this.shape_129.setTransform(55.8,72.5);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFCC").s().p("AgxHsQi4pjE9muIAigCQk+HJDIKGQgphQgIAUg");
	this.shape_130.setTransform(-14.5,90.1);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFCC00").s().p("AnsIdQjIqHE/nIIBagHQlLHbDzKYQhKhAgvAjgAHnE5Qq4luAQmuQByGfKJEcQgDBig/AAIgRgBg");
	this.shape_131.setTransform(34.7,91.2);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFF00").s().p("Al5I3QjzqYFLnbIBSgGQgDAuALAuQgQGuK4FuQAFD1oCgtQgcBFiiAAQhEAAhbgMg");
	this.shape_132.setTransform(35.3,91.5);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FF9900").s().p("AmPksQgMgvADgtIBMgGQAGG4LaEsQAUAlg8AUQqKkdhxmeg");
	this.shape_133.setTransform(55.3,72.9);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFCC").s().p("Ag5HnQixphFBmrIAigCQk8HGC8KJQgohRgKAQg");
	this.shape_134.setTransform(-14.8,89.9);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFCC00").s().p("AnxIbQi8qIE9nGIBagFQlJHXDmKaQhKhDguAlgAHkFAQqzl8ASmlQBqGTKLEuQAABihAAAQgJAAgLgCg");
	this.shape_135.setTransform(34.2,91.1);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFFF00").s().p("Al/I1QjmqbFJnXIBSgFQgDAtAKAvQgSGlKzF8QAND/oLg7QgeBEiZAAQhHAAhhgOg");
	this.shape_136.setTransform(34.8,91.6);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FF9900").s().p("AmMkvQgKgwADgtIBMgFQgFGqLeFBQAOAkg3AUQqLkvhqmSg");
	this.shape_137.setTransform(54.9,73.4);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFFFCC").s().p("AhCHiQippeFFmoIAigCQk6HDCwKKQgnhRgNAMg");
	this.shape_138.setTransform(-15.1,89.7);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFCC00").s().p("An2IZQiwqKE7nCIBagFQlGHUDZKdQhLhHgtAngAHhFHQqumKAUmdQBiGIKMFAQAEBihBAAQgLAAgMgDg");
	this.shape_139.setTransform(33.7,91.1);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFFF00").s().p("AmEIyQjZqdFGnTIBSgFQgEAtAKAvQgUGdKuGLQAVEIoVhKQgfBFiRAAQhJAAhmgSg");
	this.shape_140.setTransform(34.3,91.7);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FF9900").s().p("AmIk0QgKgvADgtIBMgEQgQGbLiFWQAKAjgzAVQqMlBhimIg");
	this.shape_141.setTransform(54.4,73.8);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFFFCC").s().p("AhKHdQihpcFImlIAjgBQk4G/CkKMQgnhSgPAJg");
	this.shape_142.setTransform(-15.5,89.6);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFCC00").s().p("An7IYQikqME5m/IBagEQlEHPDNKgQhMhJgsApgAHeFOQqpmYAXmVQBZF9KOFSQAHBihBAAQgMAAgPgEg");
	this.shape_143.setTransform(33.2,91);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFFF00").s().p("AmJIwQjNqgFEnPIBSgEQgEAtAKAvQgXGVKpGYQAdETofhYQgfBDiKAAQhLAAhrgUg");
	this.shape_144.setTransform(33.8,91.8);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FF9900").s().p("AmFk3QgKgvAEgtIBNgEQgcGNLnFrQAEAhguAWQqOlThal8g");
	this.shape_145.setTransform(54,74.3);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFFFCC").s().p("AhSHYQiZpaFMmiIAigBQk2G9CYKNQgmhSgRAFg");
	this.shape_146.setTransform(-15.9,89.5);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFCC00").s().p("AoAIXQiYqOE3m8IBagEQlCHMDBKjQhMhNgsAsgAHbFWQqkmnAZmMQBSFyKPFkQAJBhhAAAQgOAAgRgEg");
	this.shape_147.setTransform(32.6,90.9);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFFF00").s().p("AmOIuQjBqjFCnMIBSgDQgEAuAJAvQgZGMKkGnQAmEcoqhmQggBDiEAAQhNAAhugXg");
	this.shape_148.setTransform(33.2,91.9);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FF9900").s().p("AmCk7QgKgvAFguIBMgCQgnF/LrF/QgBAggpAXQqQllhRlxg");
	this.shape_149.setTransform(53.7,74.7);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFFFCC").s().p("AhaHSQiRpWFQmfIAigBQk0G5CMKQQglhTgUAAg");
	this.shape_150.setTransform(-16.2,89.3);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFCC00").s().p("AoEIVQiMqQE1m5IBagCQlAHIC0KlQhMhQgrAugAHYFdQqem1AbmEQBJFnKRF2QAMBhhBAAQgPAAgTgFg");
	this.shape_151.setTransform(32.1,90.9);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFFF00").s().p("AmUIsQizqlE/nJIBSgCQgFAtAJAvQgbGFKfG1QAuEmo0h1QggBCh/AAQhOAAhzgZg");
	this.shape_152.setTransform(32.7,92);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FF9900").s().p("Al/k/QgJgvAFgtIBMgCQgyFwLvGUQgGAfglAYQqRl3hJlmg");
	this.shape_153.setTransform(53.3,75.2);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFFFCC").s().p("AhiHNQiJpUFTmbIAjgBQkyG2CAKRQglhTgWgEg");
	this.shape_154.setTransform(-16.6,89.1);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFCC00").s().p("AoJITQiAqREzm2IBagCQk9HFCnKoQhNhUgqAwgAHWFkQqanDAdl8QBCFcKSGIQAOBhhBAAQgQAAgUgGg");
	this.shape_155.setTransform(31.5,90.8);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFFF00").s().p("AmZIpQinqoE9nEIBSgCQgFAtAIAwQgdF8KaHDQA2Evo9iDQgiBDh6AAQhQAAh1gdg");
	this.shape_156.setTransform(32.2,92.2);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FF9900").s().p("Al9lCQgIgwAFgtIBNgBQg9FhLzGqQgMAeggAYQqSmJhClag");
	this.shape_157.setTransform(52.9,75.6);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFFFCC").s().p("AhqHIQiBpSFXmYIAiAAQkwGyB0KTQgjhTgZgIg");
	this.shape_158.setTransform(-17,89);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFCC00").s().p("AoOISQh0qTExmzIBagBQk7HBCbKqQhNhWgqAygAHSFsQqUnRAgl1QA5FRKUGaQARBihCAAQgRAAgXgHg");
	this.shape_159.setTransform(31,90.8);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFFF00").s().p("AmeInQiaqqE7nBIBSgBQgGAtAIAvQgfF0KUHSQA/E5pIiSQgjBCh1AAQhRAAh4gfg");
	this.shape_160.setTransform(31.7,92.3);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FF9900").s().p("Al6lGQgIgvAGgtIBMgBQhIFTL4G+QgRAdgbAZQqUmbg6lPg");
	this.shape_161.setTransform(52.6,76);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86}]}).to({state:[{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90}]},1).to({state:[{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94}]},1).to({state:[{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98}]},1).to({state:[{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102}]},1).to({state:[{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106}]},1).to({state:[{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110}]},1).to({state:[{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114}]},1).to({state:[{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118}]},1).to({state:[{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122}]},1).to({state:[{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126}]},1).to({state:[{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130}]},1).to({state:[{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134}]},1).to({state:[{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138}]},1).to({state:[{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142}]},1).to({state:[{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146}]},1).to({state:[{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150}]},1).to({state:[{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154}]},1).to({state:[{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158}]},1).to({state:[{t:this.shape_86},{t:this.shape_87},{t:this.shape_88},{t:this.shape_89}]},1).wait(1));

	// Layer 11
	this.instance_10 = new lib.Tween4("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(26.7,27.9,1,1,0,0,0,-28.3,-31.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({scaleX:1,scaleY:1,rotation:-26.9,y:28,alpha:0.801},4).to({scaleX:1,scaleY:1,rotation:-8.7,y:27.9,alpha:1},5).to({scaleX:1,scaleY:1,rotation:-26.9,y:28,alpha:0.801},5).to({scaleX:1,scaleY:1,rotation:0,y:27.9,alpha:1},5).wait(1));

	// Layer 5
	this.instance_11 = new lib.Tween3("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(9.5,14.9,0.8,0.8,0,0,0,39.5,-44.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({regX:39.4,regY:-44.8,rotation:11.7,alpha:0.801},4).to({regX:39.5,regY:-44.9,rotation:0,alpha:1},5).to({regX:39.4,regY:-44.8,rotation:11.7,alpha:0.801},5).to({regX:39.5,regY:-44.9,rotation:0,alpha:1},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-169.1,-145.5,259.4,296.2);


// stage content:
(lib.Greetings = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text
	this.GreetingsText = new cjs.Text(msg, "45px 'Phenomena-Regular'", "#fff");
	/* this.GreetingsText = new cjs.Text("Hey Edsix.. Is all Okay?", "45px 'Phenomena-Regular'", "#ffcc00");
	this.GreetingsText1 = new cjs.Text("Please don’t leave me like this. \n Let’s meet every day ! \n You last played on 18/02/2018 ", "30px 'Phenomena-Regular'", "#fff");
	this.GreetingsText.name = "GreetingsText";
	this.GreetingsText1.name = "GreetingsText1";
	this.GreetingsText.textAlign = "center";
	this.GreetingsText1.textAlign = "center";
	this.GreetingsText.lineHeight = 38;
	this.GreetingsText1.lineHeight = 38;
	this.GreetingsText.lineWidth = 635;
	this.GreetingsText1.lineWidth = 635;
	this.GreetingsText.parent = this;
	this.GreetingsText1.parent = this;
	this.GreetingsText.setTransform(385.6,239.8);
	this.GreetingsText1.setTransform(385.6,239.8);
	this.GreetingsText.y=this.GreetingsText.y+15;
	this.GreetingsText1.y=this.GreetingsText1.y+75;
	this.timeline.addTween(cjs.Tween.get(this.GreetingsText).wait(20));
	this.timeline.addTween(cjs.Tween.get(this.GreetingsText1).wait(20)); */
	this.GreetingsText.lineHeight = 38;
	this.GreetingsText.lineWidth = 635;
	this.GreetingsText.textAlign = "center";
	this.GreetingsText.setTransform(385.6,239.8);
	this.timeline.addTween(cjs.Tween.get(this.GreetingsText).wait(20));
	// angel
	this.instance = new lib.mascot("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(372.8,125.6,0.586,0.586,27.2,0,0,0.4,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20));

	// circle
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(362.8,114.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

	// Bg
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(381.5,300.2);
	this.instance_2.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(-47, 16, 13, -3))];
	this.instance_2.cache(-380,-220,751,464);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(379.2,283.3,751,537.1);
// library properties:
lib.properties = {
	id: '3E5EE9899C98464BB5D75A2EE3451710',
	width: 752,
	height: 550,
	fps: 30,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['3E5EE9899C98464BB5D75A2EE3451710'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;