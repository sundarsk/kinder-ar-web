///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 200, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 5, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var quesCount = 3;
var currentX, currentY
var choiceCnt = 4
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var arr = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90];
var quesArr = []
var qno2 = []
var posArr = [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3]
var qno = [];
// var ansMc = []
var choiceArr = []
var tweenArr = []

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////

function init() {
    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);


    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "AddOn-Level2/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(

            { id: "choice1", src: gameAssetsPath + "ChoiceImages1.png" },
            { id: "question", src: gameAssetsPath + "question.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}
//=================================================================DONE LOADING=================================================================//
function doneLoading1(event) {
    var event = assets[i];
    var id = event.item.id;

    if (id == "questionText") {
        var questionTextSprisheet = new createjs.SpriteSheet({
            framerate: 60,
            "images": [preload.getResult("questionText")],
            "frames": { "regX": 50, "height": 100, "count": 64, "regY": 50, "width": 869 }
        });

        questionText = new createjs.Sprite(questionTextSprisheet);
        container.parent.addChild(questionText);
        questionText.visible = false;
    }

    if (id == "question") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question")],
            "frames": { "regX": 50, "height": 415, "count": 0, "regY": 50, "width": 412 },

        });
        question = new createjs.Sprite(spriteSheet1);
        question.visible = false;
        container.parent.addChild(question);
    };

    if (id == "choice1") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 304, "count": 0, "regY": 50, "width": 302 },

        });
        choice1 = new createjs.Sprite(spriteSheet1);
        choice1.visible = false;
        container.parent.addChild(choice1);
    };

    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("D044BEAA30B3F146B78DA97BB48504C8");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }
}

function tick(e) {
    stage.update();
}
/////////////////////////////////////////////////////////////////=======HANDLE CLICK========///////////////////////////////////////////////////////////////////
function handleClick(e) {
    posArr.sort(randomSort)
    toggleFullScreen()
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Remember the images and select the image that is newly added";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 20;

    bg.visible = true;

    container.parent.addChild(questionText);
    questionText.visible = true;
    questionText.x = 270; questionText.y = 140;
    //

    for (i = 0; i < quesCount; i++) {
        quesArr[i] = question.clone();
        quesArr[i].visible = false;
        container.parent.addChild(quesArr[i])
        quesArr[i].x = 80 + i * 390;
        quesArr[i].y = 350;
    }

    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i] = choice1.clone();
        choiceArr[i].visible = true;
        container.parent.addChild(choiceArr[i])
        choiceArr[i].x = 70 + i * 310;
        choiceArr[i].y = 360;

        // ansMc[i] = new createjs.MovieClip();
        // container.parent.addChild(ansMc[i]);
        // ansMc[i].timeline.addTween(createjs.Tween.get(choiceArr[i]).to({ scaleX: .95, scaleY: .95 }, 19).to({ scaleX: 1, scaleY: 1 }, 20).wait(1));
        // ansMc[i].stop()
    }


    pickques();
}
//==============================================================HELP ENABLE/DISABLE===================================================================//
function helpDisable() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = true;
    }
}
//==================================================================PICKQUES==========================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    qno2 = []
    correctImageMc.visible = false
    wrongImageMc.visible = false
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    boardMc.boardMc.openMc.mouseEnabled = false;
    boardMc.boardMc.openMc.alpha = .5;
    //=================================================================================================================================//
    qno = between(0, 34);
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].visible = false;
    }

    for (i = 0; i < quesCount; i++) {
        quesArr[i].visible = true;
        quesArr[i].gotoAndStop(qno[i]);
        quesArr[i].alpha = 1;
        console.log(qno[i])
    }

    ans = qno[3]
    questionText.gotoAndStop(0);

    questionInterval = setInterval(questionDisplay, 3000)

    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
}

function questionDisplay() {
    clearInterval(questionInterval)
    helpMc.helpMc.visible = false;
    boardMc.boardMc.openMc.mouseEnabled = true;
    boardMc.boardMc.openMc.alpha = 1;

    for (i = 0; i < quesCount; i++) {
        quesArr[i].visible = false;
    }

    questionText.gotoAndStop(1);

    pos = posArr[cnt]
    if (pos == 0) {
        qno2.push(3, 1, 2, 0)
    } else if (pos == 1) {
        qno2.push(1, 3, 0, 2)
    } else if (pos == 2) {
        qno2.push(0, 2, 3, 1)
    } else {
        qno2.push(2, 1, 0, 3)
    }

    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].visible = true;
        choiceArr[i].gotoAndStop(qno[qno2[i]]);
        choiceArr[i].name = qno[qno2[i]]

        if (qno2[i] == 3) {
            ansPos = i
        }
    }
    enablechoices()
    rst = 0;
    gameResponseTimerStart();
}
//====================================================================CHOICE ENABLE/DISABLE==============================================================//
function enablechoices() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].alpha = 1
        choiceArr[i].mouseEnabled = true;
        choiceArr[i].cursor = "pointer";
        choiceArr[i].addEventListener("click", answerSelected)
    }
}

function disablechoices() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].name = "ch" + i;
        choiceArr[i].mouseEnabled = false;
        choiceArr[i].cursor = "default";
        choiceArr[i].removeEventListener("click", answerSelected)
        // ansMc[i].stop()
    }
    boardMc.boardMc.openMc.mouseEnabled = false;
}
//===================================================================MOUSE ROLL OVER/ROLL OUT==============================================================//
function onRoll_over(e) {
    e.currentTarget.alpha = .5;
    stage.update();
}

function onRoll_out(e) {
    e.currentTarget.alpha = 1;
    stage.update();
}
//=================================================================ANSWER SELECTION=======================================================================//
function answerSelected(e) {
    e.preventDefault();
    container.parent.addChild(correctImageMc);
    container.parent.addChild(wrongImageMc);
    uans = e.currentTarget.name;
    console.log(ans + " =correct= " + uans)
    gameResponseTimerStop();
    //pauseTimer();

    if (ans == uans) {
        currentX = e.currentTarget.x + 45
        currentY = e.currentTarget.y + 45
        disableMouse()
        starAnimation()
        setTimeout(correct, 500)

    } else {
        wrongImageMc.visible = true
        getValidation("wrong");
        disablechoices();
    }
}

function correct() {
    getValidation("correct");
    disablechoices();
}


function disableMouse() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = false
    }
}

function enableMouse() {

}