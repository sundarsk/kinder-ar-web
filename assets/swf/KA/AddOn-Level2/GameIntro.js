(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000099").s().p("Ag4CGQgZgJgSgTQgRgSgJgaQgKgaAAggQAAgeALgbQALgbASgVQATgTAZgMQAZgMAcAAQAaAAAXALQAXAKASASQASASALAYQAMAZADAbIjYAgQABASAHAOQAHAOALAKQALAJAPAGQAOAFAQAAQANAAANgEQANgEALgIQALgIAJgMQAIgLADgQIAxAJQgHAYgNASQgNASgRANQgRAOgUAIQgVAGgWABQggAAgagLgAgUhgQgNADgLAJQgMAIgKAQQgJAOgEAWICWgSIgBgGQgKgYgRgOQgQgPgZAAQgKAAgMAFg");
	this.shape.setTransform(252.6,6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000099").s().p("Ag2DRQgMgDgMgFQgLgGgLgIQgMgIgIgLQgKgMgGgPIAwgVQAFAKAHAIQAHAIAHAFQAIAFAIAEIARAEQARAEAUgCQARgEANgHQANgHAKgJQAJgJAFgKIAJgTIAEgQIABgLIAAghQgOAOgQAIQgRAHgOAEQgQAEgOABQgdAAgZgKQgZgLgTgTQgSgTgLgaQgLgbAAgiQAAgiAMgaQALgbAUgSQATgTAYgJQAYgKAZAAQAQACARAGQAPAEARAJQARAJAOAPIABgyIAxACIgBEfQAAAQgFAQQgEAQgHAPQgJAPgLAOQgMANgOALQgQAKgTAHQgSAGgXACIgGABQgaAAgYgHgAgliYQgPAHgMANQgKANgHASQgFASgBAUQABAVAFARQAHASALALQALAMAQAHQAPAHATAAQAPAAAPgFQAQgGANgKQAMgKAKgOQAIgPADgRIAAghQgCgSgJgOQgKgPgNgKQgNgLgQgGQgQgGgPAAQgSAAgPAIg");
	this.shape_1.setTransform(219.3,12.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000099").s().p("ABNCPIAEgjQgeATgeAJQgdAJgZAAQgSgBgRgEQgRgFgMgJQgOgKgHgPQgIgPAAgUQAAgVAFgPQAHgQAKgNQAKgLAPgJQAOgJAQgFQARgFASgDQARgDASAAQARAAAOACIAZACQgDgMgFgMQgEgMgIgKQgIgJgLgHQgKgFgPAAQgKAAgLACQgNADgPAHQgPAIgSANQgSANgUAVIgeglQAYgXAWgOQAWgOATgIQAUgIAQgDQAQgDAOAAQAWAAASAIQASAIAPANQANAOAJATQAJASAHAVQAFAVAEAXQACAVAAAXQAAAYgDAcQgDAcgFAggAgMgBQgUAEgQAKQgPAKgIAPQgHAPAEASQADAPAKAGQALAGAOAAQAOAAARgFQAQgFARgGIAfgQIAZgPIABgYIgBgaIgagFQgNgCgOAAQgXAAgUAFg");
	this.shape_2.setTransform(186.6,6.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000099").s().p("AB5A8IgBg2IgBgoQgBgVgFgKQgHgKgJAAQgGAAgGAEQgHAEgHAGIgMANIgMAQIgKAQIgJANIABAXIABAeIABAmIAAAvIgwADIgBhOIgCg2IgCgoQgBgVgGgKQgGgKgKAAQgGAAgGAEQgIAFgGAGIgNAQIgNARIgLARIgKANIACCEIgxADIgHkNIA0gGIABBJIARgUQAJgLALgJQAKgJAMgFQAMgGAPAAQAKAAAKAEQAKADAHAHQAIAHAFALQAGALACAPIAQgUQAJgKAKgJQAKgIALgFQAMgGAOAAQAMAAALAEQAKAEAIAIQAIAHAFANQAFAMABARIADAuIACA8IABBYIg0ADIAAhOg");
	this.shape_3.setTransform(150.5,5.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000099").s().p("AgfC8IABhAIAChPIABhrIAzgCIgCBBIgBA3IgBAsIAAAiIgBA2gAgNh3QgHgDgEgFQgFgEgDgHQgDgGABgIQgBgHADgGQADgHAFgFQAEgFAHgCQAGgEAHAAQAHAAAGAEQAHACAFAFQAFAFADAHQACAGABAHQgBAIgCAGQgDAHgFAEQgFAFgHADQgGADgHAAQgHAAgGgDg");
	this.shape_4.setTransform(123.6,0.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000099").s().p("Ag4CGQgZgJgSgTQgRgSgJgaQgKgaAAggQAAgeALgbQALgbASgVQATgTAZgMQAZgMAcAAQAaAAAXALQAXAKASASQASASALAYQAMAZADAbIjYAgQABASAHAOQAHAOALAKQALAJAPAGQAOAFAQAAQANAAANgEQANgEALgIQALgIAJgMQAIgLADgQIAxAJQgHAYgNASQgNASgRANQgRAOgUAIQgVAGgWABQggAAgagLgAgUhgQgNADgLAJQgMAIgKAQQgJAOgEAWICWgSIgBgGQgKgYgRgOQgQgPgZAAQgKAAgMAFg");
	this.shape_5.setTransform(88.4,6.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000099").s().p("ABDC/QAIgZADgWIAEgpIAAgoQgBgbgIgTQgHgSgMgLQgMgMgOgGQgOgFgOAAQgNABgPAHQgMAGgOANQgOALgNAXIgCCnIgwABIgFmCIA6gCIgCCYQAOgPAQgIQAPgJAOgEQAPgFAOgCQAdAAAXAKQAXAKAQATQARATAJAbQAKAZABAiIAAAlIgCAkIgDAiQgDAQgEANg");
	this.shape_6.setTransform(56.5,0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000099").s().p("AgegcIhXACIABguIBXgDIAChuIAxgDIgCBwIBhgDIgDAvIhfACIgCDaIg0ABg");
	this.shape_7.setTransform(27.3,1.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000099").s().p("AhxA+IgCg+IgBgwIgBglIgCg1IA5gBIABAfIAAAeIAAAeQAHgMALgNQAKgOANgNQANgMAPgJQAPgJASgCQAVgBAQAJQAHAEAHAHQAHAGAGALQAFAKAEAPQAEAOACAUIgxASIgCgVQgCgJgDgHQgDgGgEgEIgHgHQgJgHgLABQgHABgIAFQgHAEgIAIIgQARIgRAUIgPAUIgMARIAAAmIABAhIABAdIACAUIg2AHIgDhOg");
	this.shape_8.setTransform(-11,6.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000099").s().p("Ag4CGQgZgJgSgTQgRgSgJgaQgKgaAAggQAAgeALgbQALgbASgVQATgTAZgMQAZgMAcAAQAaAAAXALQAXAKASASQASASALAYQAMAZADAbIjYAgQABASAHAOQAHAOALAKQALAJAPAGQAOAFAQAAQANAAANgEQANgEALgIQALgIAJgMQAIgLADgQIAxAJQgHAYgNASQgNASgRANQgRAOgUAIQgVAGgWABQggAAgagLgAgUhgQgNADgLAJQgMAIgKAQQgJAOgEAWICWgSIgBgGQgKgYgRgOQgQgPgZAAQgKAAgMAFg");
	this.shape_9.setTransform(-41.5,6.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000099").s().p("AgXDAQgOgEgQgIQgRgIgRgOIAAAhIguABIgEmBIA2gCIgBCTQAOgOAQgHQAQgIANgDQAQgFANAAQARAAARAFQARAFAPAJQAPAIANANQAMANAKAPQAJAPAEARQAFASAAATQgBAWgGATQgGATgJAQQgJAQgNANQgMAMgOAJQgPAJgPAFQgPAEgPAAQgOAAgRgEgAAAglQgPAAgOAFQgOAFgKAJQgLAIgJAKQgIAMgEAMIgBA8QAFAQAIAMQAJAMALAIQAKAIANAEQANAEANAAQARABAQgHQAQgHAMgMQAMgNAHgRQAHgQABgUQABgTgGgRQgGgRgLgNQgLgNgRgIQgPgHgRAAIgCAAg");
	this.shape_10.setTransform(-72.7,0.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000099").s().p("AB4A8IAAg2IgBgoQAAgVgHgKQgFgKgKAAQgGAAgHAEQgGAEgGAGIgNANIgMAQIgKAQIgJANIABAXIABAeIAAAmIAAAvIgvADIgBhOIgCg2IgDgoQAAgVgGgKQgGgKgJAAQgHAAgHAEQgGAFgHAGIgOAQIgMARIgLARIgKANIADCEIgzADIgGkNIA0gGIABBJIARgUQAJgLALgJQAKgJAMgFQAMgGAPAAQAKAAAKAEQAKADAHAHQAIAHAGALQAFALACAPIAQgUQAJgKAKgJQAKgIAMgFQAMgGAOAAQALAAAKAEQALAEAIAIQAIAHAGANQAEAMABARIADAuIACA8IABBYIg1ADIAAhOg");
	this.shape_11.setTransform(-110.8,5.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000099").s().p("Ag4CGQgZgJgSgTQgRgSgJgaQgKgaAAggQAAgeALgbQALgbASgVQATgTAZgMQAZgMAcAAQAaAAAXALQAXAKASASQASASALAYQAMAZADAbIjYAgQABASAHAOQAHAOALAKQALAJAPAGQAOAFAQAAQANAAANgEQANgEALgIQALgIAJgMQAIgLADgQIAxAJQgHAYgNASQgNASgRANQgRAOgUAIQgVAGgWABQggAAgagLgAgUhgQgNADgLAJQgMAIgKAQQgJAOgEAWICWgSIgBgGQgKgYgRgOQgQgPgZAAQgKAAgMAFg");
	this.shape_12.setTransform(-146.7,6.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000099").s().p("AB5A8IgBg2IgBgoQgBgVgFgKQgHgKgJAAQgHAAgFAEQgHAEgHAGIgMANIgLAQIgLAQIgJANIABAXIABAeIABAmIAAAvIgwADIgBhOIgCg2IgCgoQgBgVgGgKQgGgKgKAAQgGAAgGAEQgIAFgGAGIgNAQIgNARIgMARIgJANIACCEIgxADIgHkNIA0gGIABBJIARgUQAJgLAKgJQALgJAMgFQAMgGAPAAQALAAAJAEQAKADAHAHQAIAHAFALQAGALABAPIARgUQAJgKAKgJQAKgIALgFQAMgGAOAAQAMAAALAEQAKAEAIAIQAIAHAFANQAGAMAAARIADAuIACA8IABBYIg0ADIAAhOg");
	this.shape_13.setTransform(-182.9,5.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000099").s().p("Ag4CGQgZgJgSgTQgRgSgJgaQgKgaAAggQAAgeALgbQALgbASgVQATgTAZgMQAZgMAcAAQAaAAAXALQAXAKASASQASASALAYQAMAZADAbIjYAgQABASAHAOQAHAOALAKQALAJAPAGQAOAFAQAAQANAAANgEQANgEALgIQALgIAJgMQAIgLADgQIAxAJQgHAYgNASQgNASgRANQgRAOgUAIQgVAGgWABQggAAgagLgAgUhgQgNADgLAJQgMAIgKAQQgJAOgEAWICWgSIgBgGQgKgYgRgOQgQgPgZAAQgKAAgMAFg");
	this.shape_14.setTransform(-218.8,6.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000099").s().p("AAOBJIgYAEQgNABgPAAIgbgBQgPgBgRgDIABBqIg1ACIAAlhQAWgHAVgDIAogFIAogCQAQAAATACQAUACAUAGQATAGATAKQATAKAOAQQAOAQAIAXQAIAWgBAeQgBAUgHAQQgHAPgLANQgMANgPAKQgPAKgSAHIBJBzIg0ARgAgyiKIgXACIgZAEIABCfQAMACAMABIAXABQAeAAAagHQAbgHAUgMQAUgLAMgQQAMgRAAgTQAAgTgJgPQgJgPgRgKQgQgLgXgFQgXgGgaAAIgYABg");
	this.shape_15.setTransform(-251.3,0.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(2,1,1).p("Egq4gGZMBVxAAAQBrAABMBMQBNBMAABsIAAErQAABshNBMQhMBMhrAAMhVxAAAQhrAAhNhMQhMhMAAhsIAAkrQAAhsBMhMQBNhMBrAAg");
	this.shape_16.setTransform(0,1.1,0.951,0.951);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("Egq4AGaQhrAAhNhMQhMhMAAhsIAAkrQAAhsBMhMQBNhMBrAAMBVxAAAQBrAABMBMQBNBMAABsIAAErQAABshNBMQhMBMhrAAg");
	this.shape_17.setTransform(0,1.1,0.951,0.951);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-286.7,-41,573.5,82.2);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgvCVQgVgIgPgQQgQgQgJgWQgIgXAAgcQAAgdAJgVQAKgWAQgPQAQgPAUgIQAUgIAVAAQANAAAOADQAMADAPAHQAOAHAMAMIAChqIAnADIgBEyIgrAAIABgaIgNAKIgNAIIgNAFIgMAEQgNADgMAAQgYAAgVgIgAgggkQgNAHgJALQgJALgFAOQgGAPAAARQAAASAGAOQAFAPAKALQAJAKANAGQANAGAQAAQAMAAAMgEQANgEAKgIQAKgHAIgLQAIgLAEgMIAAgtQgEgNgIgKQgIgMgKgIQgLgHgMgFQgNgEgMAAQgQAAgNAGg");
	this.shape.setTransform(121.6,1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgvBwQgUgIgQgQQgOgPgHgVQgIgWAAgaQAAgaAJgWQAJgXAPgQQAPgRAWgKQAUgJAYAAQAVAAATAIQAUAJAPAPQAOAOAKAVQAJAUADAXIi0AaQACAPAGAMQAGAMAIAIQAKAIAMAEQAMAFANAAQAKAAALgEQALgDAJgHQAKgGAGgKQAHgKADgNIApAIQgHATgKAQQgKAPgPALQgOALgRAGQgRAGgSAAQgbAAgWgIgAgRhQQgKADgKAHQgKAHgIANQgHAMgEASIB9gPIgBgEQgIgVgOgMQgOgLgUAAQgIAAgLADg");
	this.shape_1.setTransform(95.9,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgvCVQgVgIgPgQQgQgQgJgWQgIgXAAgcQAAgdAJgVQAKgWAQgPQAQgPAUgIQAUgIAVAAQANAAAOADQAMADAPAHQAOAHAMAMIAChqIAnADIgBEyIgrAAIABgaIgNAKIgNAIIgNAFIgMAEQgNADgMAAQgYAAgVgIgAgggkQgNAHgJALQgJALgFAOQgGAPAAARQAAASAGAOQAFAPAKALQAJAKANAGQANAGAQAAQAMAAAMgEQANgEAKgIQAKgHAIgLQAIgLAEgMIAAgtQgEgNgIgKQgIgMgKgIQgLgHgMgFQgNgEgMAAQgQAAgNAGg");
	this.shape_2.setTransform(68.1,1.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgvCVQgVgIgPgQQgQgQgJgWQgIgXAAgcQAAgdAJgVQAKgWAQgPQAQgPAUgIQAUgIAVAAQANAAAOADQAMADAPAHQAOAHAMAMIAChqIAnADIgBEyIgrAAIABgaIgNAKIgNAIIgNAFIgMAEQgNADgMAAQgYAAgVgIgAgggkQgNAHgJALQgJALgFAOQgGAPAAARQAAASAGAOQAFAPAKALQAJAKANAGQANAGAQAAQAMAAAMgEQANgEAKgIQAKgHAIgLQAIgLAEgMIAAgtQgEgNgIgKQgIgMgKgIQgLgHgMgFQgNgEgMAAQgQAAgNAGg");
	this.shape_3.setTransform(39.9,1.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("ABBB3IACgeQgZARgYAHQgYAHgVAAQgPAAgOgDQgOgEgLgIQgKgIgHgNQgHgMAAgRQAAgRAFgNQAFgOAJgKQAIgJAMgIQANgHANgEQAOgFAPgCQAOgCAPAAIAZABIAWACIgIgUQgEgKgFgIQgHgIgJgFQgIgFgNAAQgIAAgJACQgLACgNAHQgMAGgPALQgOALgRARIgagfQAUgSASgMQATgMAQgHQAQgHAOgCQANgDALAAQATAAAPAHQAPAGAMAMQALALAIAQQAIAPAEARQAFASADATQACASAAATQAAAUgDAXQgBAXgGAbgAgJAAQgSADgNAIQgNAJgFAMQgHAMAEAPQADANAIAFQAIAFAMAAQAMAAAOgEQANgEAOgGIAagNIAVgMIABgVQAAgKgBgLQgKgCgMgCQgLgCgLAAQgSAAgRAFg");
	this.shape_4.setTransform(12.7,5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AhwCHQANABAMgCQALgDAKgFQAJgFAJgIIAPgOIALgRIAKgQIhYjpIAwgGIA8C3IAMgjIAOguIAQgzIAQg0IAuAAIgTA8IgRAwIgNAnIgLAdIgPAtIgKAbQgFANgJAOQgIAOgKANQgLANgOAKQgOAKgSAGQgSAHgYABg");
	this.shape_5.setTransform(-23.7,11.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AANC1QgLgEgIgIQgJgIgGgJQgGgKgEgKQgKgXgDgfIAGkIIAqAAIgBBFIgCA4IgBAtIAAAjIgBA5QAAATAEAPQACAGAEAHQADAGAFAFQAFAFAHADQAHADAJAAIgGAqQgPAAgLgGg");
	this.shape_6.setTransform(-39.5,-2.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AAHgqIgyCRIg+AFIgpjXIArgDIAgCuIA+iuIAnADIAqCwIAdiyIAuACIgqDYIg+ACg");
	this.shape_7.setTransform(-62.8,5.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AguBwQgWgIgOgQQgPgPgIgVQgHgWAAgaQAAgaAJgWQAJgXAPgQQAQgRAUgKQAVgJAXAAQAWAAAUAIQASAJAQAPQAOAOAJAVQALAUACAXIi0AaQACAPAFAMQAGAMAJAIQAJAIAMAEQAMAFANAAQAMAAALgEQAKgDAKgHQAJgGAHgKQAGgKADgNIAoAIQgFATgLAQQgLAPgOALQgOALgRAGQgRAGgTAAQgaAAgVgIgAgRhQQgKADgKAHQgKAHgHANQgJAMgCASIB8gPIgCgEQgHgVgOgMQgOgLgVAAQgHAAgLADg");
	this.shape_8.setTransform(-91.3,5.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AhhA0IgCg0IgBgoIgBgeIgBgsIAvgBIABA8QAHgMAJgKQAJgLAKgIQAKgIALgFQALgFANgBQAQgBAMAEQALADAJAHQAIAHAGAJQAFAJADAKIAFATIABARQACAjgBAlIgCBLIgqgCIADhDQABghgCgiIAAgJIgCgNIgFgNQgDgHgFgFQgFgFgIgCQgHgDgJACQgRACgQAWQgRAWgUAoIACA2IABAeIAAAQIgsAGIgDhBg");
	this.shape_9.setTransform(-117.5,5.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(3,0,0,6.9).p("Ay1krMAlqAAAQBsAABMBMQBMBMAABsIAABPQAABshMBMQhMBMhsAAMglqAAAQhrAAhNhMQhMhMAAhsIAAhPQAAhsBMhMQBNhMBrAAg");
	this.shape_10.setTransform(0.1,1.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ay1EsQhrAAhMhMQhNhMAAhsIAAhPQAAhsBNhMQBMhMBrAAMAlqAAAQBsAABMBMQBNBMgBBsIAABPQABBshNBMQhMBMhsAAg");
	this.shape_11.setTransform(0.1,1.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148,-35.1,296.1,70.3);


(lib.questiontext_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgaBTQgLgFgJgIQgIgKgFgMQgFgMAAgQQAAgQAFgLQAGgMAJgJQAIgIAMgFQALgEALAAQAHAAAIABIAPAGQAIAEAGAHIABg8IAWACIAACrIgYAAIAAgPIgHAFIgHAEIgHADIgHADQgHACgHAAQgNgBgMgEgAgRgTQgHADgFAGQgGAHgDAGQgCAJAAAKQAAAKADAHQACAJAGAFQAFAGAHAEQAIADAIAAQAHAAAGgCQAHgDAGgEQAGgEAEgGQAEgGADgHIAAgZQgCgHgFgFQgEgGgGgFQgGgEgHgCQgHgDgGAAQgJAAgHAEg");
	this.shape.setTransform(214,-2.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZA+QgMgFgIgIQgIgJgFgMQgEgLAAgPQAAgOAFgMQAFgMAJgKQAIgJAMgGQALgFANAAQAMAAAKAFQALAFAIAIQAJAIAFAMQAFAKACANIhkAPQABAIADAHQAEAGAFAFQAFAEAGADQAHACAHAAQAGAAAGgCQAGgCAFgEQAFgDAEgGQAEgFABgHIAXAEQgDALgGAIQgGAJgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGABgFAEQgGAFgEAHQgEAGgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_1.setTransform(199.7,0.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgaBTQgLgFgJgIQgIgKgFgMQgFgMAAgQQAAgQAFgLQAGgMAJgJQAIgIAMgFQALgEALAAQAHAAAIABIAPAGQAIAEAGAHIABg8IAWACIAACrIgYAAIAAgPIgHAFIgHAEIgHADIgHADQgHACgHAAQgNgBgMgEgAgRgTQgHADgFAGQgGAHgDAGQgCAJAAAKQAAAKADAHQACAJAGAFQAFAGAHAEQAIADAIAAQAHAAAGgCQAHgDAGgEQAGgEAEgGQAEgGADgHIAAgZQgCgHgFgFQgEgGgGgFQgGgEgHgCQgHgDgGAAQgJAAgHAEg");
	this.shape_2.setTransform(184.2,-2.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaBTQgLgFgJgIQgIgKgFgMQgFgMAAgQQAAgQAFgLQAGgMAJgJQAIgIAMgFQALgEALAAQAHAAAIABIAPAGQAIAEAGAHIABg8IAWACIAACrIgYAAIAAgPIgHAFIgHAEIgHADIgHADQgHACgHAAQgNgBgMgEgAgRgTQgHADgFAGQgGAHgDAGQgCAJAAAKQAAAKADAHQACAJAGAFQAFAGAHAEQAIADAIAAQAHAAAGgCQAHgDAGgEQAGgEAEgGQAEgGADgHIAAgZQgCgHgFgFQgEgGgGgFQgGgEgHgCQgHgDgGAAQgJAAgHAEg");
	this.shape_3.setTransform(168.5,-2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAkBCIACgQQgPAJgNAEQgNAEgMAAQgIAAgIgCQgIgCgGgFQgGgEgDgHQgEgHAAgKQAAgJADgHQADgIAEgFQAFgFAHgEQAHgEAHgDIAQgEIAQgBIAOABIAMABIgEgLQgCgGgEgEQgDgFgFgCQgFgDgHAAIgJABQgGABgHAEQgHADgIAHQgJAGgJAJIgOgRQALgKAKgHQALgHAJgDQAJgEAHgCIANgBQALAAAIAEQAJADAGAHQAGAGAFAJIAHASIAEAUIABAUIgBAYIgEAcgAgFAAQgKACgHAEQgHAFgDAHQgEAGACAJQACAHAFADQAEACAHAAQAHAAAIgCIAOgFIAPgHIALgHIAAgMIAAgLIgMgCIgNgBQgKAAgJACg");
	this.shape_4.setTransform(153.4,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag9BLQAHABAGgCIAMgEIAKgHIAIgIIAHgKIAFgIIgxiBIAbgEIAgBlIAHgTIAIgZIAJgcIAJgdIAaAAIgLAhIgJAbIgIAVIgGAQIgJAZIgFAPIgHAPIgKAPQgGAHgIAGQgHAFgLAEQgKAEgNAAg");
	this.shape_5.setTransform(133.2,3.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAHBlIgKgHQgFgEgDgFIgGgLQgFgOgCgRIADiSIAYAAIgBAmIgBAgIgBAZIAAATIAAAfQAAALACAIIADAHIAFAHIAGAEQAEACAFAAIgEAWQgHABgHgDg");
	this.shape_6.setTransform(124.4,-3.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AADgWIgbBQIgiACIgWh3IAXgCIASBhIAihhIAWACIAXBhIAQhiIAaABIgYB4IgiABg");
	this.shape_7.setTransform(111.5,0.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZA+QgMgFgIgIQgIgJgFgMQgEgLAAgPQAAgOAFgMQAFgMAJgKQAIgJAMgGQALgFANAAQAMAAAKAFQALAFAIAIQAJAIAFAMQAFAKACANIhkAPQABAIADAHQAEAGAFAFQAFAEAGADQAHACAHAAQAGAAAGgCQAGgCAFgEQAFgDAEgGQAEgFABgHIAXAEQgDALgGAIQgGAJgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGABgFAEQgGAFgEAHQgEAGgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_8.setTransform(95.6,0.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag1AdIgBgdIgBgWIgBgRIAAgYIAagBIABAiQADgHAFgGIALgKQAFgFAGgCQAGgDAHgBQAJAAAHACQAGACAFAEQAFADADAGIAEAKIADALIABAJQABATgBAVIgBApIgXAAIABgmQABgSgBgSIAAgGIgBgHIgDgHQgCgEgDgDQgCgCgEgCQgEgBgGABQgJABgIAMQgKAMgLAWIABAeIABARIAAAJIgZADIgBgkg");
	this.shape_9.setTransform(81.1,0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgQBCIgKgDIgKgDIgLgFIgKgHIALgSIANAHIAOAFIAOAEQAGACAIAAQAGAAAFgCQAEgBADgCIAFgEIACgEIAAgEIgBgFIgDgEIgGgEIgJgDQgGgCgHAAIgTgCQgKgCgHgDQgIgEgFgFQgFgGgBgIQgBgJACgHQACgHAEgFQAEgGAGgEQAFgEAHgCQAHgDAHgBIAOgCIALABIANABQAHACAGADQAHACAGAEIgIAWIgOgHIgNgDIgLgCQgSgBgKAFQgKAFAAAJQAAAHAEAEQAEADAGABIAQACIASACQALACAIADQAIADAEAEQAFAFACAGQACAFAAAGQAAALgEAIQgFAHgHAFQgIAFgJACIgUADQgKAAgLgCg");
	this.shape_10.setTransform(60.8,0.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgOBXIABgdIAAglIABgxIAXgBIAAAeIgBAZIAAAVIAAAPIgBAZgAgFg2IgGgEQgCgCgBgDQgCgDABgDQgBgEACgDQABgDACgCIAGgEQADgBACAAQADAAADABIAGAEIADAFIACAHIgCAGIgDAFIgGAEQgDABgDAAQgCAAgDgBg");
	this.shape_11.setTransform(51.5,-2.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgNgMIgoABIAAgWIAogBIABgzIAXgCIgBA0IAtgBIgDAVIgrACIAABkIgYABg");
	this.shape_12.setTransform(36.6,-2.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAkBCIACgQQgPAJgNAEQgNAEgMAAQgIAAgIgCQgIgCgGgFQgGgEgDgHQgEgHAAgKQAAgJADgHQADgIAEgFQAFgFAHgEQAHgEAHgDIAQgEIAQgBIAOABIAMABIgEgLQgCgGgEgEQgDgFgFgCQgFgDgHAAIgJABQgGABgHAEQgHADgIAHQgJAGgJAJIgOgRQALgKAKgHQALgHAJgDQAJgEAHgCIANgBQALAAAIAEQAJADAGAHQAGAGAFAJIAHASIAEAUIABAUIgBAYIgEAcgAgFAAQgKACgHAEQgHAFgDAHQgEAGACAJQACAHAFADQAEACAHAAQAHAAAIgCIAOgFIAPgHIALgHIAAgMIAAgLIgMgCIgNgBQgKAAgJACg");
	this.shape_13.setTransform(23.2,0);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAfBYIAFgVIACgTIAAgTQAAgMgEgJQgEgIgFgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgGAGQgHAFgGAKIAABNIgXABIgCiyIAbgBIgBBGQAGgHAHgDQAIgFAGgCIANgDQANAAALAFQAKAFAIAIQAIAJAEAMQAFAMAAAQIAAAQIgBARIgBAQIgDANg");
	this.shape_14.setTransform(8.4,-2.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgNgMIgpABIABgWIApgBIAAgzIAWgCIgBA0IAtgBIgCAVIgrACIgBBkIgXABg");
	this.shape_15.setTransform(-5.1,-2.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgZA+QgMgFgIgIQgIgJgFgMQgEgLAAgPQAAgOAFgMQAFgMAJgKQAIgJAMgGQALgFANAAQAMAAAKAFQALAFAIAIQAJAIAFAMQAFAKACANIhkAPQABAIADAHQAEAGAFAFQAFAEAGADQAHACAHAAQAGAAAGgCQAGgCAFgEQAFgDAEgGQAEgFABgHIAXAEQgDALgGAIQgGAJgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGABgFAEQgGAFgEAHQgEAGgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_16.setTransform(-24,0.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgYBhIgLgEIgLgGIgJgJQgFgGgCgHIAWgKQACAFADAEQADAEAEACIAIAEIAHACQAIACAJgBQAIgCAGgDQAGgEAEgEQAEgEADgEIAEgJIACgIIAAgFIAAgPQgHAGgHAEIgOAFQgHACgHAAQgNAAgMgEQgLgFgJgJQgIgJgFgLQgFgNAAgPQAAgQAFgMQAGgNAJgIQAIgJAMgEQALgFALAAIAPAEIAPAGQAIAEAGAHIABgXIAWABIAACEQAAAIgCAHQgCAIgEAHQgDAHgGAGQgFAGgHAFQgHAFgJADQgJADgKABQgNAAgMgDgAgRhGQgHAEgFAGQgFAGgDAIQgDAIAAAKQAAAJADAIQADAIAGAFQAFAGAHADQAHADAIAAQAHAAAHgDQAHgCAGgFQAGgEAEgHQAEgGACgIIAAgPQgBgJgFgGQgEgHgGgFQgGgFgHgDQgIgCgGAAQgJAAgHADg");
	this.shape_17.setTransform(-39.4,2.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAkBCIACgQQgPAJgNAEQgNAEgMAAQgIAAgIgCQgIgCgGgFQgGgEgDgHQgEgHAAgKQAAgJADgHQADgIAEgFQAFgFAHgEQAHgEAHgDIAQgEIAQgBIAOABIAMABIgEgLQgCgGgEgEQgDgFgFgCQgFgDgHAAIgJABQgGABgHAEQgHADgIAHQgJAGgJAJIgOgRQALgKAKgHQALgHAJgDQAJgEAHgCIANgBQALAAAIAEQAJADAGAHQAGAGAFAJIAHASIAEAUIABAUIgBAYIgEAcgAgFAAQgKACgHAEQgHAFgDAHQgEAGACAJQACAHAFADQAEACAHAAQAHAAAIgCIAOgFIAPgHIALgHIAAgMIAAgLIgMgCIgNgBQgKAAgJACg");
	this.shape_18.setTransform(-54.5,0);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AA3AcIAAgZIAAgSQgBgKgCgFQgCgEgFAAQgDAAgDACIgGAEIgFAGIgGAIIgFAHIgEAGIAAAKIABAOIAAASIAAAVIgVACIAAgkIgBgZIgCgSQAAgKgDgFQgDgEgEAAQgDAAgDACIgHAFIgGAHIgFAIIgGAIIgEAFIABA9IgXACIgDh8IAYgDIABAiIAIgKIAJgJQAEgEAGgCQAGgDAGAAQAFAAAEACQAFABADAEQAEADACAFQACAFABAHIAIgJIAJgJQAEgEAGgCQAFgDAHAAIAKACQAEACAEADQAEAEACAGQADAFAAAIIACAWIABAbIAAAoIgYACIgBgkg");
	this.shape_19.setTransform(-71.2,-0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgNBXIAAgdIAAglIABgxIAYgBIgBAeIgBAZIAAAVIAAAPIgBAZgAgFg2IgGgEQgCgCgBgDQgBgDAAgDQAAgEABgDQABgDACgCIAGgEQADgBACAAQADAAADABIAGAEIADAFIABAHIgBAGIgDAFIgGAEQgDABgDAAQgCAAgDgBg");
	this.shape_20.setTransform(-83.7,-2.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgZA+QgMgFgIgIQgIgJgFgMQgEgLAAgPQAAgOAFgMQAFgMAJgKQAIgJAMgGQALgFANAAQAMAAAKAFQALAFAIAIQAJAIAFAMQAFAKACANIhkAPQABAIADAHQAEAGAFAFQAFAEAGADQAHACAHAAQAGAAAGgCQAGgCAFgEQAFgDAEgGQAEgFABgHIAXAEQgDALgGAIQgGAJgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGABgFAEQgGAFgEAHQgEAGgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_21.setTransform(-100,0.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAfBYIAFgVIACgTIAAgTQAAgMgEgJQgEgIgFgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgGAGQgHAFgGAKIAABNIgXABIgCiyIAbgBIgBBGQAGgHAHgDQAIgFAGgCIANgDQANAAALAFQAKAFAIAIQAIAJAEAMQAFAMAAAQIAAAQIgBARIgBAQIgDANg");
	this.shape_22.setTransform(-114.8,-2.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgNgMIgoABIAAgWIAogBIABgzIAWgCIgBA0IAtgBIgBAVIgsACIAABkIgYABg");
	this.shape_23.setTransform(-128.3,-2.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgNgMIgpABIABgWIAogBIABgzIAXgCIgBA0IAtgBIgDAVIgrACIgBBkIgXABg");
	this.shape_24.setTransform(-145.8,-2.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgUA/QgNgGgKgJQgJgIgFgNQgHgNAAgOQABgHACgJQADgJADgHQAFgHAGgHQAHgGAHgFQAIgFAJgCQAIgDAJAAQAKAAAJADQAJADAIAEQAHAFAHAGQAGAHAEAIIAAAAIgTAMIAAAAQgDgHgFgEQgEgFgFgDQgGgDgGgCQgFgCgHAAQgIAAgJADQgIAEgHAGQgFAHgFAJQgDAHAAAJQAAAKADAIQAFAJAFAFQAHAHAIAEQAJADAIAAQAGAAAFgBIALgFQAGgDAEgFQAEgEADgFIABgBIAUAMIAAABQgEAHgHAGQgHAGgHAEQgIAFgJACQgJACgJAAQgMAAgMgEg");
	this.shape_25.setTransform(-158.6,0.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgZA+QgMgFgIgIQgIgJgFgMQgEgLAAgPQAAgOAFgMQAFgMAJgKQAIgJAMgGQALgFANAAQAMAAAKAFQALAFAIAIQAJAIAFAMQAFAKACANIhkAPQABAIADAHQAEAGAFAFQAFAEAGADQAHACAHAAQAGAAAGgCQAGgCAFgEQAFgDAEgGQAEgFABgHIAXAEQgDALgGAIQgGAJgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGABgFAEQgGAFgEAHQgEAGgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_26.setTransform(-172.8,0.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAHBlIgKgHQgFgEgDgFIgGgLQgGgOgBgRIAEiSIAWAAIgBAmIgBAgIAAAZIAAATIgBAfQABALACAIIADAHIAEAHIAHAEQAEACAFAAIgDAWQgIABgHgDg");
	this.shape_27.setTransform(-182.5,-3.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgZA+QgMgFgIgIQgIgJgFgMQgEgLAAgPQAAgOAFgMQAFgMAJgKQAIgJAMgGQALgFANAAQAMAAAKAFQALAFAIAIQAJAIAFAMQAFAKACANIhkAPQABAIADAHQAEAGAFAFQAFAEAGADQAHACAHAAQAGAAAGgCQAGgCAFgEQAFgDAEgGQAEgFABgHIAXAEQgDALgGAIQgGAJgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGABgFAEQgGAFgEAHQgEAGgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_28.setTransform(-193.5,0.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgPBdIgRgFIgQgIIgQgKIARgUQAJAHAIAFIAQAHIAPADQAIABAIgCQAHgBAFgEQAGgDADgEQADgFABgGQAAgEgCgDIgFgGIgJgGIgKgEIgLgDIgLgDIgOgCIgOgGQgHgDgGgEQgHgEgEgHQgFgGgCgIQgDgKABgLQABgJADgHQADgIAFgGQAFgFAHgFIAOgHIAPgEIAPgBQALABALADIAKACIAKAFIALAFIAJAIIgNAVQgDgEgFgDIgIgGIgIgEIgIgDQgJgCgJAAQgJAAgJADQgIADgFAFQgGAFgDAHQgDAFAAAHQAAAFAEAGQADAFAGAFQAHAFAIADQAJADAIACIARACQAJACAHADQAIADAGAEQAHAFAEAGQAFAGACAHQACAIgCAJQgBAIgDAHQgEAGgFAFQgFAEgGAEIgNAEIgOAEIgOAAQgIAAgJgCg");
	this.shape_29.setTransform(-208.3,-2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#990000").ss(3,1,1).p("EAkHAAoIAAhTQgBhHhDgyQhFgyhfAAMhA/AADQhgAAhEAyQhCAyAABIIAABTQAABHBFAyQBDAyBhAAMBA/gADQBgAABCgyQBDgyAAhIg");
	this.shape_30.setTransform(3.7,-2.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF3333").s().p("EgjBAClQhFgyAAhHIAAhTQAAhIBCgyQBEgyBgAAMBA/gADQBfAABFAyQBDAyABBHIAABTQAABIhDAyQhCAyhgAAMhA/AADQhhAAhDgyg");
	this.shape_31.setTransform(3.7,-2.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy2, new cjs.Rectangle(-228.9,-25.4,465.2,46), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgZA+QgMgEgIgJQgIgIgFgMQgEgMAAgPQAAgOAFgMQAFgNAJgJQAIgJAMgFQALgGANAAQAMAAAKAFQALAEAIAJQAJAIAFALQAFAMACANIhkAOQABAIADAGQAEAHAFAEQAFAFAGACQAHADAHAAQAGAAAGgCQAGgCAFgDQAFgEAEgGQAEgFABgIIAXAFQgDAKgGAKQgGAIgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGACgFADQgGAEgEAHQgEAHgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape.setTransform(120,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYBhIgLgEIgLgGIgJgJQgFgGgCgHIAWgKQACAFADAEQADAEAEACIAIAEIAHACQAIACAJgBQAIgCAGgDQAGgEAEgEQAEgEADgEIAEgJIACgIIAAgFIAAgPQgHAGgHAEIgOAFQgHACgHAAQgNAAgMgEQgLgFgJgJQgIgJgFgLQgFgNAAgPQAAgQAFgMQAGgNAJgIQAIgJAMgEQALgFALAAIAPAEIAPAGQAIAEAGAHIABgXIAWABIAACEQAAAIgCAHQgCAIgEAHQgDAHgGAGQgFAGgHAFQgHAFgJADQgJADgKABQgNAAgMgDgAgRhGQgHAEgFAGQgFAGgDAIQgDAIAAAKQAAAJADAIQADAIAGAFQAFAGAHADQAHADAIAAQAHAAAHgDQAHgCAGgFQAGgEAEgHQAEgGACgIIAAgPQgBgJgFgGQgEgHgGgFQgGgFgHgDQgIgCgGAAQgJAAgHADg");
	this.shape_1.setTransform(104.6,2.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAkBCIACgQQgPAJgNAEQgNAEgMAAQgIAAgIgCQgIgCgGgFQgGgEgDgHQgEgHAAgKQAAgJADgHQADgIAEgFQAFgFAHgEQAHgEAHgDIAQgEIAQgBIAOABIAMABIgEgLQgCgGgEgEQgDgFgFgCQgFgDgHAAIgJABQgGABgHAEQgHADgIAHQgJAGgJAJIgOgRQALgKAKgHQALgHAJgDQAJgEAHgCIANgBQALAAAIAEQAJADAGAHQAGAGAFAJIAHASIAEAUIABAUIgBAYIgEAcgAgFAAQgKACgHAEQgHAFgDAHQgEAGACAJQACAHAFADQAEACAHAAQAHAAAIgCIAOgFIAPgHIALgHIAAgMIAAgLIgMgCIgNgBQgKAAgJACg");
	this.shape_2.setTransform(89.5,-0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AA3AcIAAgZIAAgSQgBgKgCgFQgCgEgFAAQgDAAgDACIgGAEIgFAGIgGAIIgFAHIgEAGIAAAKIABAOIAAASIAAAVIgVACIAAgkIgBgZIgCgSQAAgKgDgFQgCgEgFAAQgDAAgDACIgHAFIgGAHIgFAIIgGAIIgEAFIABA9IgXACIgDh8IAYgDIABAiIAIgKIAJgJQAEgEAGgCQAGgDAGAAQAFAAAEACQAFABADAEQAEADACAFQACAFABAHIAIgJIAJgJQAEgEAGgCQAFgDAHAAIAKACQAEACAEADQAEAEACAGQADAFAAAIIACAWIABAbIAAAoIgYACIgBgkg");
	this.shape_3.setTransform(72.8,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgNBXIAAgdIABglIAAgxIAYgBIgBAeIgBAZIAAAVIAAAPIgBAZgAgFg2IgGgEQgCgCgBgDQgBgDAAgDQAAgEABgDQABgDACgCIAGgEQADgBACAAQADAAADABIAGAEIADAFIABAHIgBAGIgDAFIgGAEQgDABgDAAQgCAAgDgBg");
	this.shape_4.setTransform(60.3,-3.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgZA+QgMgEgIgJQgIgIgFgMQgEgMAAgPQAAgOAFgMQAFgNAJgJQAIgJAMgFQALgGANAAQAMAAAKAFQALAEAIAJQAJAIAFALQAFAMACANIhkAOQABAIADAGQAEAHAFAEQAFAFAGACQAHADAHAAQAGAAAGgCQAGgCAFgDQAFgEAEgGQAEgFABgIIAXAFQgDAKgGAKQgGAIgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGACgFADQgGAEgEAHQgEAHgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_5.setTransform(44,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAfBYIAFgVIACgTIAAgTQAAgMgEgJQgEgIgFgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgGAGQgHAFgGAKIAABNIgXABIgCiyIAbgBIgBBGQAGgHAHgDQAIgFAGgCIANgDQANAAALAFQAKAFAIAIQAIAJAEAMQAFAMAAAQIAAAQIgBARIgBAQIgDANg");
	this.shape_6.setTransform(29.2,-3.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgNgNIgpABIABgVIApgBIAAgzIAXgCIgCA1IAtgCIgCAVIgrABIgBBlIgXABg");
	this.shape_7.setTransform(15.7,-2.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag0AdIgBgdIgBgWIAAgRIgBgYIAbgBIAAAPIAAANIAAAOIAJgLIAKgMQAHgGAGgEQAHgFAIgBQAJAAAIAEIAHAFIAGAIIAEAMQACAGAAAJIgWAJIgBgKIgDgHIgDgFIgDgDQgEgDgFAAQgDAAgEADIgHAFIgHAIIgIAKIgGAJIgGAHIAAASIAAAPIABANIAAAKIgZADIgBgkg");
	this.shape_8.setTransform(-2.1,-0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgZA+QgMgEgIgJQgIgIgFgMQgEgMAAgPQAAgOAFgMQAFgNAJgJQAIgJAMgFQALgGANAAQAMAAAKAFQALAEAIAJQAJAIAFALQAFAMACANIhkAOQABAIADAGQAEAHAFAEQAFAFAGACQAHADAHAAQAGAAAGgCQAGgCAFgDQAFgEAEgGQAEgFABgIIAXAFQgDAKgGAKQgGAIgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGACgFADQgGAEgEAHQgEAHgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_9.setTransform(-16.1,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKBZQgHgCgHgEQgIgDgHgHIAAAQIgWAAIgCiyIAagBIgBBEQAHgGAHgEQAHgDAGgBIANgDQAIAAAIADQAHACAIAEQAHAEAFAGIAKAMQAFAIACAHQACAJAAAJQAAAJgDAJQgDAJgEAIQgFAHgFAFQgGAGgGAEQgHAEgIADQgHACgGAAIgOgCgAAAgRQgHABgFACQgHACgFAEIgJAIQgDAGgCAFIgBAcQACAHAEAGQAEAGAFADQAFAEAGACQAFACAHAAQAHAAAIgEQAHgDAFgFQAGgGADgIQAEgHAAgJQABgJgDgIQgDgIgFgGQgGgGgHgDQgHgEgIAAIgBAAg");
	this.shape_10.setTransform(-30.5,-3.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AA3AcIAAgZIAAgSQgBgKgCgFQgCgEgFAAQgDAAgDACIgGAEIgGAGIgFAIIgFAHIgEAGIAAAKIABAOIAAASIAAAVIgWACIAAgkIAAgZIgCgSQAAgKgDgFQgDgEgEAAQgDAAgDACIgHAFIgFAHIgGAIIgGAIIgEAFIABA9IgXACIgDh8IAYgDIABAiIAIgKIAJgJQAEgEAGgCQAGgDAGAAQAFAAAEACQAFABADAEQADADADAFQADAFAAAHIAIgJIAIgJQAFgEAGgCQAFgDAHAAIAKACQAEACAFADQADAEACAGQADAFABAIIABAWIABAbIAAAoIgYACIgBgkg");
	this.shape_11.setTransform(-48.2,-0.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgZA+QgMgEgIgJQgIgIgFgMQgEgMAAgPQAAgOAFgMQAFgNAJgJQAIgJAMgFQALgGANAAQAMAAAKAFQALAEAIAJQAJAIAFALQAFAMACANIhkAOQABAIADAGQAEAHAFAEQAFAFAGACQAHADAHAAQAGAAAGgCQAGgCAFgDQAFgEAEgGQAEgFABgIIAXAFQgDAKgGAKQgGAIgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGACgFADQgGAEgEAHQgEAHgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_12.setTransform(-64.7,-0.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA3AcIAAgZIAAgSQgBgKgCgFQgCgEgFAAQgDAAgDACIgGAEIgGAGIgFAIIgFAHIgEAGIAAAKIABAOIAAASIAAAVIgWACIAAgkIAAgZIgCgSQAAgKgDgFQgDgEgEAAQgDAAgDACIgHAFIgFAHIgGAIIgGAIIgEAFIABA9IgXACIgDh8IAYgDIABAiIAIgKIAJgJQAEgEAGgCQAGgDAGAAQAFAAAEACQAFABADAEQADADADAFQADAFAAAHIAIgJIAIgJQAFgEAGgCQAFgDAHAAIAKACQAEACAFADQADAEACAGQADAFABAIIABAWIABAbIAAAoIgYACIgBgkg");
	this.shape_13.setTransform(-81.5,-0.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgZA+QgMgEgIgJQgIgIgFgMQgEgMAAgPQAAgOAFgMQAFgNAJgJQAIgJAMgFQALgGANAAQAMAAAKAFQALAEAIAJQAJAIAFALQAFAMACANIhkAOQABAIADAGQAEAHAFAEQAFAFAGACQAHADAHAAQAGAAAGgCQAGgCAFgDQAFgEAEgGQAEgFABgIIAXAFQgDAKgGAKQgGAIgIAGQgIAGgJAEQgKADgKAAQgPAAgLgFgAgJgsQgGACgFADQgGAEgEAHQgEAHgCAKIBFgIIgBgDQgEgLgIgHQgIgGgLAAQgEAAgGACg");
	this.shape_14.setTransform(-98.1,-0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAHAiIgLACIgNAAIgNAAIgOgCIAAAxIgYABIAAijIAUgEIASgDIASgBIAQABIATAEQAJACAIAGQAJAEAHAIQAGAHAEALQAEAKgBAOQAAAIgEAIQgDAGgFAHQgFAFgIAFQgHAFgIADIAiA2IgYAHgAgXg/IgKABIgMACIABBIIALACIAKABQAOgBAMgDQAMgDAKgGQAJgEAFgHQAGgIAAgJQAAgJgEgHQgEgHgIgEQgIgFgKgDQgLgDgMABIgLAAg");
	this.shape_15.setTransform(-113.1,-3.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#003399").ss(3,1,1).p("A7YAsQABBHA0AyQAzAyBJAAMAxSgAEQBIAAAygyQA0gxAAhIIAAhTQgBhHgzgyQg0gyhIAAMgxSAADQhJAAgzAyQgzAyABBIg");
	this.shape_16.setTransform(3.7,-2.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0066FF").s().p("A6jClQg0gygBhHIABhTQgBhIAzgyQAzgyBJAAMAxSgADQBIAAA0AyQAzAyABBHIAABTQAABIg0AxQgyAyhIAAMgxSAAEQhJAAgzgyg");
	this.shape_17.setTransform(3.7,-2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-173,-25.4,353.6,46), null);


(lib.Objects_mccopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AgzA0QgWgVAAgfQAAgeAWgVQAWgWAdAAQAfAAAWAWQAVAVAAAeQAAAfgVAVQgWAWgfAAQgdAAgWgWg");
	this.shape.setTransform(-0.1,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C3634").s().p("AAAE5QgTABgOgOQgOgPAAgTIAAoSQAAgVAOgOQAOgNATAAIAAAAQAUAAAOANQAOAOAAAVIAAISQAAATgOAPQgOAOgUgBg");
	this.shape_1.setTransform(-18.3,-1.1,0.48,0.48,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C3634").s().p("AAAHlQgKAAgIgIQgIgHAAgLIAAuVQAAgLAIgIQAIgHAKAAIAAAAQALAAAIAHQAIAIAAALIAAOVQAAALgIAHQgIAIgLAAg");
	this.shape_2.setTransform(0,-27.5,0.487,0.487);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#33CC00").s().p("AoSITQjdjcABk3QgBk2DdjcQDcjcE2AAQE3AADcDcQDcDcABE2QgBE3jcDcQjcDck3ABQk2gBjcjcgAnnnnQjKDKAAEdQAAEeDKDKQDKDKEdgBQEdABDKjKQDLjKAAkeQAAkdjLjKQjKjKkdAAQkdAAjKDKg");
	this.shape_3.setTransform(0.1,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AnnHoQjKjKAAkeQAAkdDKjKQDKjKEdAAQEdAADKDKQDLDKAAEdQAAEejLDKQjKDKkdAAQkdAAjKjKgAmimhQitCtAAD0QAAD0CtCuQCtCtD1AAQD0AACtitQCuiuAAj0QAAj0iuitQitiuj0AAQj1AAitCug");
	this.shape_4.setTransform(0.1,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EEECEC").s().p("AmhGiQitiuAAj0QAAjzCtiuQCtiuD0AAQD1AACtCuQCtCuAADzQAAD0itCuQitCuj1AAQj0AAitiugAg0g0QgWAWAAAeQAAAeAWAWQAWAVAeAAQAeAAAWgVQAVgWAAgeQAAgegVgWQgWgVgeAAQgeAAgWAVg");
	this.shape_5.setTransform(0,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#003366").ss(5,1,1).p("APvAAQAAGhknEnQg3A3g7AtQkCDDlTAAQl8AAkWj1QgcgYgagaQknknAAmhQAAmgEnknQAagaAcgYQEWj1F8AAQFTAAECDEQA7AsA3A3QEnEnAAGgg");

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AqRL6QgcgYgagaQkmknAAmhQAAmgEmknQAagaAcgXQEWj1F8AAQFTgBECDEQA7AsA3A3QEmEnAAGgQAAGhkmEnQg3A2g7AuQkCDClTAAQl8AAkWj0g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AwjQrMAAAghVMAhHAAAMAAAAhVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy5, new cjs.Rectangle(-106,-106.7,212.1,213.5), null);


(lib.Objects_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AgzA0QgWgVAAgfQAAgeAWgVQAWgWAdAAQAfAAAWAWQAVAVAAAeQAAAfgVAVQgWAWgfAAQgdAAgWgWg");
	this.shape.setTransform(-0.2,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C3634").s().p("AAAE5QgTABgOgOQgOgPAAgTIAAoSQAAgVAOgOQAOgNATAAIAAAAQAUAAAOANQAOAOAAAVIAAISQAAATgOAPQgOAOgUgBg");
	this.shape_1.setTransform(-18.3,-0.1,0.48,0.48,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C3634").s().p("AAAHlQgKAAgIgIQgIgHAAgLIAAuVQAAgLAIgIQAIgHAKAAIAAAAQALAAAIAHQAIAIAAALIAAOVQAAALgIAHQgIAIgLAAg");
	this.shape_2.setTransform(-0.1,-26.5,0.487,0.487);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D56636").s().p("AoSITQjcjcgBk3QABk2DcjcQDcjcE2gBQE3ABDdDcQDbDcABE2QgBE3jbDcQjdDck3ABQk2gBjcjcgAnnnnQjJDKAAEdQAAEeDJDKQDKDKEdgBQEdABDLjKQDKjKgBkeQABkdjKjKQjLjKkdAAQkdAAjKDKg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A03309").s().p("AnnHoQjJjKAAkeQAAkdDJjKQDKjKEdAAQEdAADLDKQDKDKgBEdQABEejKDKQjLDKkdAAQkdAAjKjKgAmimhQitCtAAD0QAAD0CtCuQCuCtD0AAQD0AACtitQCtiuAAj0QAAj0ititQitiuj0AAQj0AAiuCug");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EEECEC").s().p("AmhGiQititAAj1QAAj0CtitQCtiuD0AAQD1AACtCuQCtCtAAD0QAAD1itCtQitCuj1AAQj0AAitiugAg0g0QgWAVAAAfQAAAeAWAWQAWAVAeAAQAeAAAWgVQAVgWAAgeQAAgfgVgVQgWgVgeAAQgeAAgWAVg");
	this.shape_5.setTransform(-0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#003366").ss(5,1,1).p("APvAAQAAGhknEnQg3A3g7AtQkCDDlTAAQl8AAkWj1QgcgYgagaQknknAAmhQAAmgEnknQAagaAcgYQEWj1F8AAQFTAAECDEQA7AsA3A3QEnEnAAGgg");

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AqRL6QgcgYgagaQkmknAAmhQAAmgEmknQAagaAcgXQEWj1F8AAQFTgBECDEQA7AsA3A3QEmEnAAGgQAAGhkmEnQg3A2g7AuQkCDClTAAQl8AAkWj0g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AwjQrMAAAghVMAhHAAAMAAAAhVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy4, new cjs.Rectangle(-106,-106.7,212.1,213.5), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.questiontext_mccopy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AggBOQgPgFgKgLQgLgLgFgPQgGgPAAgSQAAgSAHgQQAGgPALgNQALgLAPgHQAOgHAQAAQAPABAOAFQANAHAKAKQALALAHAOQAGAOACAQIh+ASQABAKAFAJQAEAIAGAGQAHAFAIAEQAIACAJABQAIgBAHgCQAIgCAHgFQAGgFAFgGQAFgHACgJIAcAGQgEAMgHALQgIALgKAIQgKAIgMAEQgMAEgNAAQgTABgOgHgAgLg4QgIACgHAFQgGAFgGAJQgGAJgCAMIBYgKIgBgDQgGgPgKgIQgKgJgOAAQgFABgHACg");
	this.shape_18.setTransform(151,-0.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgfB6IgOgFIgNgIQgHgFgGgGQgEgHgEgJIAcgMQADAGAEAFQAEAEAEADIAJAFIAKADQAKACAMgBQAJgCAJgEQAHgFAFgFQAGgFACgGIAGgLIADgKIAAgGIAAgTQgIAIgKAEQgJAFgJACQgJACgIABQgRAAgPgGQgPgGgKgLQgLgLgGgPQgHgQAAgUQAAgUAIgPQAGgQAMgLQALgKAOgGQAOgGAOAAIATAFIATAIQAKAFAIAJIAAgdIAdAAIgBCoQABAJgDAKQgDAJgEAJQgFAJgGAIQgHAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgOgEgAgVhZQgKAFgFAHQgHAIgDAKQgEALgBAMQABAMAEAKQADAKAHAGQAGAHAJAEQAJAFALAAQAIAAAJgEQAKgDAHgGQAIgFAEgJQAGgIACgKIAAgTQgCgLgGgIQgEgJgIgGQgIgGgJgEQgJgDgJAAQgKAAgJAEg");
	this.shape_19.setTransform(131.6,3.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_20.setTransform(112.4,-0.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("ABGAjIAAggIAAgXQgBgMgDgGQgEgFgFAAQgEAAgEACIgHAGIgHAHIgHAJIgGAKIgFAHIAAAOIABASIAAAVIAAAcIgbACIgBguIgBggIgBgXQgBgMgDgGQgEgFgFAAQgEAAgEACIgIAHIgIAJIgHAKIgHAKIgFAHIABBNIgdACIgEidIAfgDIAAAqIAKgMIALgLQAGgGAIgCQAHgEAIAAQAGAAAGACQAGACADAEQAFAEADAHQAEAGAAAIIAKgLIALgLQAGgEAHgEQAHgDAIAAQAHAAAGACQAGACAFAFQAEAEADAIQAEAHAAAKIABAbIACAjIAAAzIgeACIgBgug");
	this.shape_21.setTransform(91.3,-0.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgSBuIABglIABguIABg+IAdgBIgBAlIAAAgIgBAaIAAATIAAAggAgHhFIgGgEQgDgDgCgEQgCgEAAgEQAAgEACgEQACgEADgDQACgDAEgBQADgCAEAAQAEAAAEACQAEABADADIAFAHIABAIQAAAEgBAEIgFAHIgHAEIgIACQgEAAgDgCg");
	this.shape_22.setTransform(75.6,-3.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AggBOQgPgFgLgLQgKgLgFgPQgGgPABgSQAAgSAGgQQAGgPALgNQALgLAOgHQAPgHAQAAQAPABAOAFQANAHALAKQAKALAHAOQAGAOACAQIh+ASQACAKAEAJQADAIAHAGQAHAFAIAEQAIACAJABQAIgBAHgCQAIgCAGgFQAHgFAFgGQAEgHACgJIAdAGQgEAMgHALQgIALgKAIQgKAIgMAEQgMAEgNAAQgSABgPgHgAgMg4QgGACgHAFQgIAFgFAJQgFAJgDAMIBXgKIgBgDQgFgPgKgIQgJgJgPAAQgFABgIACg");
	this.shape_23.setTransform(55,-0.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_24.setTransform(36.3,-3.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygBIAChAIAcgCIgBBBIA5gCIgDAcIg3ABIgBB/IgeAAg");
	this.shape_25.setTransform(19.2,-3.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_26.setTransform(-3.1,-0.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AggBOQgPgFgLgLQgKgLgFgPQgFgPAAgSQAAgSAGgQQAGgPALgNQALgLAOgHQAPgHAQAAQAPABAOAFQANAHALAKQAKALAHAOQAGAOADAQIh/ASQACAKADAJQAEAIAHAGQAHAFAIAEQAIACAJABQAIgBAIgCQAHgCAGgFQAHgFAFgGQAEgHACgJIAdAGQgEAMgIALQgHALgKAIQgKAIgMAEQgMAEgNAAQgSABgPgHgAgMg4QgHACgGAFQgIAFgFAJQgGAJgBAMIBWgKIgBgDQgFgPgJgIQgLgJgOAAQgFABgIACg");
	this.shape_27.setTransform(-20.9,-0.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgNBwQgIgCgKgFQgKgEgJgJIAAAUIgbAAIgDjhIAggBIgBBXQAJgJAJgEQAJgFAIgCQAJgCAIAAQAKAAAJADQAKACAJAGQAJAFAHAHQAIAHAFAJQAFAJADAKQADAKAAAMQgBAMgDALQgEALgFAKQgGAJgHAIQgHAHgIAFQgJAFgJADQgJADgIAAQgIgBgKgCgAAAgVQgIAAgIADQgIADgHAFQgGAFgFAFQgFAHgCAHIAAAjQACAJAFAHQAFAHAGAFQAHAFAHACQAHADAIAAQAKAAAJgEQAJgEAHgHQAHgIAEgJQAFgKAAgMQABgLgEgKQgDgKgHgHQgGgHgKgFQgIgEgKAAIgCAAg");
	this.shape_28.setTransform(-39.2,-3.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("ABGAjIAAggIAAgXQgBgMgDgGQgEgFgFAAQgEAAgEACIgHAGIgHAHIgHAJIgGAKIgFAHIAAAOIABASIAAAVIAAAcIgbACIgBguIgBggIgBgXQgBgMgDgGQgEgFgFAAQgEAAgEACIgIAHIgIAJIgHAKIgHAKIgFAHIABBNIgdACIgEidIAfgDIAAAqIAKgMIALgLQAGgGAIgCQAHgEAIAAQAGAAAGACQAGACADAEQAFAEADAHQAEAGAAAIIAKgLIALgLQAGgEAHgEQAHgDAIAAQAHAAAGACQAGACAFAFQAEAEADAIQAEAHAAAKIABAbIACAjIAAAzIgeACIgBgug");
	this.shape_29.setTransform(-61.5,-0.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AggBOQgPgFgKgLQgLgLgFgPQgGgPABgSQgBgSAHgQQAGgPALgNQALgLAOgHQAPgHAQAAQAPABAOAFQANAHAKAKQALALAHAOQAGAOACAQIh+ASQABAKAFAJQAEAIAGAGQAGAFAJAEQAIACAJABQAIgBAHgCQAIgCAHgFQAGgFAFgGQAFgHACgJIAcAGQgEAMgHALQgIALgKAIQgKAIgMAEQgMAEgNAAQgSABgPgHgAgMg4QgHACgHAFQgGAFgGAJQgGAJgCAMIBXgKIAAgDQgGgPgKgIQgKgJgOAAQgFABgIACg");
	this.shape_30.setTransform(-82.4,-0.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("ABGAjIAAggIAAgXQgBgMgDgGQgEgFgFAAQgEAAgEACIgHAGIgHAHIgHAJIgGAKIgFAHIAAAOIABASIAAAVIAAAcIgbACIgBguIgBggIgBgXQgBgMgDgGQgEgFgFAAQgEAAgEACIgIAHIgIAJIgHAKIgHAKIgFAHIABBNIgdACIgEidIAfgDIAAAqIAKgMIALgLQAGgGAIgCQAHgEAIAAQAGAAAGACQAGACADAEQAFAEADAHQAEAGAAAIIAKgLIALgLQAGgEAHgEQAHgDAIAAQAHAAAGACQAGACAFAFQAEAEADAIQAEAHAAAKIABAbIACAjIAAAzIgeACIgBgug");
	this.shape_31.setTransform(-103.6,-0.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AggBOQgPgFgLgLQgKgLgFgPQgGgPABgSQAAgSAGgQQAGgPALgNQALgLAOgHQAPgHAQAAQAPABAOAFQANAHALAKQAKALAHAOQAGAOACAQIh+ASQACAKAEAJQADAIAHAGQAHAFAIAEQAIACAJABQAIgBAHgCQAIgCAGgFQAHgFAFgGQAEgHACgJIAdAGQgEAMgHALQgIALgKAIQgKAIgMAEQgMAEgNAAQgSABgPgHgAgMg4QgGACgHAFQgIAFgFAJQgFAJgDAMIBXgKIgBgDQgFgPgKgIQgJgJgPAAQgFABgIACg");
	this.shape_32.setTransform(-124.5,-0.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AAIArIgNACIgRABIgQgBIgSgCIAAA+IgfABIAAjOIAagGIAXgDIAXgBIAUABIAXAFQAMADALAGQALAGAIAKQAIAJAFANQAFANgBASQAAALgEAKQgFAIgGAIQgHAHgJAGQgJAGgKAEIArBDIgfAKgAgdhQIgOABIgOADIABBcIANACIAOAAQARAAAPgEQAQgEAMgHQAMgGAHgJQAHgKAAgLQAAgLgFgJQgGgJgKgGQgJgGgOgDQgNgEgPAAIgOABg");
	this.shape_33.setTransform(-143.6,-3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]}).wait(1));

	// Layer 2
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#003399").ss(3,1,1).p("A7YAsQABBHA0AyQAzAyBJAAMAxSgAEQBIAAAygyQA0gxAAhIIAAhTQgBhHgzgyQg0gyhIAAMgxSAADQhJAAgzAyQgzAyABBIg");
	this.shape_34.setTransform(3.7,-2.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#0066FF").s().p("A6jClQg0gygBhHIABhTQgBhIAzgyQAzgyBJAAMAxSgADQBIAAA0AyQAzAyABBHIAABTQAABIg0AxQgyAyhIAAMgxSAAEQhJAAgzgyg");
	this.shape_35.setTransform(3.7,-2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35},{t:this.shape_34}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy_1, new cjs.Rectangle(-173,-28.1,353.6,48.7), null);


(lib.Objects_mccopy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AgzA0QgWgVAAgfQAAgeAWgVQAWgWAdAAQAfAAAWAWQAVAVAAAeQAAAfgVAVQgWAWgfAAQgdAAgWgWg");
	this.shape.setTransform(-0.2,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C3634").s().p("AAAE5QgTABgOgOQgOgPAAgTIAAoSQAAgVAOgOQAOgNATAAIAAAAQAUAAAOANQAOAOAAAVIAAISQAAATgOAPQgOAOgUgBg");
	this.shape_1.setTransform(-18.3,-0.1,0.48,0.48,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C3634").s().p("AAAHlQgKAAgIgIQgIgHAAgLIAAuVQAAgLAIgIQAIgHAKAAIAAAAQALAAAIAHQAIAIAAALIAAOVQAAALgIAHQgIAIgLAAg");
	this.shape_2.setTransform(-0.1,-26.5,0.487,0.487);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#33CC00").s().p("AoSITQjcjcgBk3QABk2DcjcQDcjcE2gBQE3ABDdDcQDbDcABE2QgBE3jbDcQjdDck3ABQk2gBjcjcgAnnnnQjJDKAAEdQAAEeDJDKQDKDKEdgBQEdABDLjKQDKjKgBkeQABkdjKjKQjLjKkdAAQkdAAjKDKg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AnnHoQjJjKAAkeQAAkdDJjKQDKjKEdAAQEdAADLDKQDKDKgBEdQABEejKDKQjLDKkdAAQkdAAjKjKgAmimhQitCtAAD0QAAD0CtCuQCuCtD0AAQD0AACtitQCtiuAAj0QAAj0ititQitiuj0AAQj0AAiuCug");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EEECEC").s().p("AmhGiQititAAj1QAAj0CtitQCtiuD0AAQD1AACtCuQCtCtAAD0QAAD1itCtQitCuj1AAQj0AAitiugAg0g0QgWAVAAAfQAAAeAWAWQAWAVAeAAQAeAAAWgVQAVgWAAgeQAAgfgVgVQgWgVgeAAQgeAAgWAVg");
	this.shape_5.setTransform(-0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#003366").ss(5,1,1).p("APvAAQAAGhknEnQg3A3g7AtQkCDDlTAAQl8AAkWj1QgcgYgagaQknknAAmhQAAmgEnknQAagaAcgYQEWj1F8AAQFTAAECDEQA7AsA3A3QEnEnAAGgg");

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AqRL6QgcgYgagaQkmknAAmhQAAmgEmknQAagaAcgXQEWj1F8AAQFTgBECDEQA7AsA3A3QEmEnAAGgQAAGhkmEnQg3A2g7AuQkCDClTAAQl8AAkWj0g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AwjQrMAAAghVMAhHAAAMAAAAhVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy10, new cjs.Rectangle(-106,-106.7,212.1,213.5), null);


(lib.Objects_mccopy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AgzA0QgWgVAAgfQAAgeAWgVQAWgWAdAAQAfAAAWAWQAVAVAAAeQAAAfgVAVQgWAWgfAAQgdAAgWgWg");
	this.shape.setTransform(-0.2,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C3634").s().p("AAAE5QgTABgOgOQgOgPAAgTIAAoSQAAgVAOgOQAOgNATAAIAAAAQAUAAAOANQAOAOAAAVIAAISQAAATgOAPQgOAOgUgBg");
	this.shape_1.setTransform(-18.3,-0.1,0.48,0.48,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C3634").s().p("AAAHlQgKAAgIgIQgIgHAAgLIAAuVQAAgLAIgIQAIgHAKAAIAAAAQALAAAIAHQAIAIAAALIAAOVQAAALgIAHQgIAIgLAAg");
	this.shape_2.setTransform(-0.1,-26.5,0.487,0.487);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0099FF").s().p("AoSITQjcjcgBk3QABk2DcjcQDcjcE2gBQE3ABDdDcQDbDcABE2QgBE3jbDcQjdDck3ABQk2gBjcjcgAnnnnQjJDKAAEdQAAEeDJDKQDKDKEdgBQEdABDLjKQDKjKgBkeQABkdjKjKQjLjKkdAAQkdAAjKDKg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000033").s().p("AnnHoQjJjKAAkeQAAkdDJjKQDKjKEdAAQEdAADLDKQDKDKgBEdQABEejKDKQjLDKkdAAQkdAAjKjKgAmimhQitCtAAD0QAAD0CtCuQCuCtD0AAQD0AACtitQCtiuAAj0QAAj0ititQitiuj0AAQj0AAiuCug");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EEECEC").s().p("AmhGiQititAAj1QAAj0CtitQCtiuD0AAQD1AACtCuQCtCtAAD0QAAD1itCtQitCuj1AAQj0AAitiugAg0g0QgWAVAAAfQAAAeAWAWQAWAVAeAAQAeAAAWgVQAVgWAAgeQAAgfgVgVQgWgVgeAAQgeAAgWAVg");
	this.shape_5.setTransform(-0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#003366").ss(5,1,1).p("APvAAQAAGhknEnQg3A3g7AtQkCDDlTAAQl8AAkWj1QgcgYgagaQknknAAmhQAAmgEnknQAagaAcgYQEWj1F8AAQFTAAECDEQA7AsA3A3QEnEnAAGgg");

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AqRL6QgcgYgagaQkmknAAmhQAAmgEmknQAagaAcgXQEWj1F8AAQFTgBECDEQA7AsA3A3QEmEnAAGgQAAGhkmEnQg3A2g7AuQkCDClTAAQl8AAkWj0g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AwjQrMAAAghVMAhHAAAMAAAAhVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy9, new cjs.Rectangle(-106,-106.7,212.1,213.5), null);


(lib.Objects_mccopy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AgzA0QgWgVAAgfQAAgeAWgVQAWgWAdAAQAfAAAWAWQAVAVAAAeQAAAfgVAVQgWAWgfAAQgdAAgWgWg");
	this.shape.setTransform(-0.2,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C3634").s().p("AAAE5QgTABgOgOQgOgPAAgTIAAoSQAAgVAOgOQAOgNATAAIAAAAQAUAAAOANQAOAOAAAVIAAISQAAATgOAPQgOAOgUgBg");
	this.shape_1.setTransform(-18.3,-0.1,0.48,0.48,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C3634").s().p("AAAHlQgKAAgIgIQgIgHAAgLIAAuVQAAgLAIgIQAIgHAKAAIAAAAQALAAAIAHQAIAIAAALIAAOVQAAALgIAHQgIAIgLAAg");
	this.shape_2.setTransform(-0.1,-26.5,0.487,0.487);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D56636").s().p("AoSITQjcjcgBk3QABk2DcjcQDcjcE2gBQE3ABDdDcQDbDcABE2QgBE3jbDcQjdDck3ABQk2gBjcjcgAnnnnQjJDKAAEdQAAEeDJDKQDKDKEdgBQEdABDLjKQDKjKgBkeQABkdjKjKQjLjKkdAAQkdAAjKDKg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A03309").s().p("AnnHoQjJjKAAkeQAAkdDJjKQDKjKEdAAQEdAADLDKQDKDKgBEdQABEejKDKQjLDKkdAAQkdAAjKjKgAmimhQitCtAAD0QAAD0CtCuQCuCtD0AAQD0AACtitQCtiuAAj0QAAj0ititQitiuj0AAQj0AAiuCug");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EEECEC").s().p("AmhGiQititAAj1QAAj0CtitQCtiuD0AAQD1AACtCuQCtCtAAD0QAAD1itCtQitCuj1AAQj0AAitiugAg0g0QgWAVAAAfQAAAeAWAWQAWAVAeAAQAeAAAWgVQAVgWAAgeQAAgfgVgVQgWgVgeAAQgeAAgWAVg");
	this.shape_5.setTransform(-0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#003366").ss(5,1,1).p("APvAAQAAGhknEnQg3A3g7AtQkCDDlTAAQl8AAkWj1QgcgYgagaQknknAAmhQAAmgEnknQAagaAcgYQEWj1F8AAQFTAAECDEQA7AsA3A3QEnEnAAGgg");

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AqRL6QgcgYgagaQkmknAAmhQAAmgEmknQAagaAcgXQEWj1F8AAQFTgBECDEQA7AsA3A3QEmEnAAGgQAAGhkmEnQg3A2g7AuQkCDClTAAQl8AAkWj0g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AwjQrMAAAghVMAhHAAAMAAAAhVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy8, new cjs.Rectangle(-106,-106.7,212.1,213.5), null);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween11("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},11).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148,-35.1,296.1,70.3);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.ques1 = new lib.Objects_mccopy10();
	this.ques1.name = "ques1";
	this.ques1.parent = this;
	this.ques1.setTransform(330,56.6,1.408,1.408,0,0,0,0.1,0.2);

	this.ques1_1 = new lib.Objects_mccopy9();
	this.ques1_1.name = "ques1_1";
	this.ques1_1.parent = this;
	this.ques1_1.setTransform(0.2,56.6,1.408,1.408,0,0,0,0.1,0.2);

	this.ques1_2 = new lib.Objects_mccopy8();
	this.ques1_2.name = "ques1_2";
	this.ques1_2.parent = this;
	this.ques1_2.setTransform(-329.6,56.6,1.408,1.408,0,0,0,0.1,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ques1_2},{t:this.ques1_1},{t:this.ques1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-479.1,-93.9,958.3,300.6);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween23("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02,y:-2.4},9).to({scaleX:1,scaleY:1,y:-2.5},10).to({scaleX:1.02,scaleY:1.02,y:-2.4},10).to({scaleX:1,scaleY:1,y:-2.5},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-286.7,-43.5,573.5,82.2);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Objects_mccopy11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_6
	this.ques1 = new lib.Objects_mccopy4();
	this.ques1.name = "ques1";
	this.ques1.parent = this;
	this.ques1.setTransform(80,-6.7,0.695,0.695);

	this.ques1_1 = new lib.Objects_mccopy5();
	this.ques1_1.name = "ques1_1";
	this.ques1_1.parent = this;
	this.ques1_1.setTransform(-79.9,-6.7,0.695,0.695);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ques1_1},{t:this.ques1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy11, new cjs.Rectangle(-153.6,-80.9,307.4,148.5), null);


(lib.Objects_mccopy4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_6
	this.ques1 = new lib.Objects_mccopy4();
	this.ques1.name = "ques1";
	this.ques1.parent = this;
	this.ques1.setTransform(80,-6.7,0.695,0.695);

	this.ques1_1 = new lib.Objects_mccopy5();
	this.ques1_1.name = "ques1_1";
	this.ques1_1.parent = this;
	this.ques1_1.setTransform(-79.9,-6.7,0.695,0.695);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ques1_1},{t:this.ques1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Objects_mccopy4_1, new cjs.Rectangle(-153.6,-80.9,307.4,148.5), null);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ques1 = new lib.Objects_mccopy11();
	this.ques1.name = "ques1";
	this.ques1.parent = this;
	this.ques1.setTransform(-416.6,10.8,0.779,0.779,0,0,0,0.2,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(2,1,1).p("AUPAAQAAEol7DSQl8DRoYAAQoYAAl7jRQl8jSAAkoQAAkoF8jRQDVh1EFg0QDOgpDrAAQDsAADNApQEGA0DVB1QF7DRAAEog");
	this.shape.setTransform(-413.4,1.7,1.331,1.331);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AuTH6Ql7jSAAkoQAAknF7jSQDVh1EGg0QDNgpDrAAQDsAADNApQEGA0DVB1QF7DSABEnQgBEol7DSQl8DRoYAAQoXAAl8jRg");
	this.shape_1.setTransform(-413.4,1.7,1.331,1.331);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.ques1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-586.8,-94.5,346.8,192.5);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(638.8,495.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(310).to({_off:false},0).wait(34).to({startPosition:14},0).to({alpha:0,startPosition:3},9).wait(1));

	// Layer_9
	this.instance_1 = new lib.Symbol1copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(755.7,602.7,1,1,0,0,0,190.5,64.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(254).to({_off:false},0).to({_off:true},34).wait(66));

	// Layer_6
	this.instance_2 = new lib.Symbol1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(645.6,278.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(220).to({_off:false},0).to({_off:true},34).wait(100));

	// Layer_10
	this.ques1 = new lib.Objects_mccopy9();
	this.ques1.name = "ques1";
	this.ques1.parent = this;
	this.ques1.setTransform(640.1,496.6,1.408,1.408,0,0,0,0.1,0.2);
	this.ques1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.ques1).wait(288).to({_off:false},0).to({regY:0.3,scaleX:1.7,scaleY:1.7,x:640,y:496.7},22).wait(34).to({alpha:0},9).wait(1));

	// Layer_5
	this.instance_3 = new lib.Tween5("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(1074.3,244.8);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(154).to({_off:false},0).to({alpha:1},7).wait(44).to({startPosition:0},0).to({alpha:0.02},5).to({_off:true},1).wait(143));

	// Layer_7
	this.instance_4 = new lib.Tween13("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(902.7,104.4,0.95,0.95);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(127).to({_off:false},0).to({_off:true},93).wait(124).to({_off:false,startPosition:8},0).to({alpha:0.02,startPosition:12},9).wait(1));

	// Layer_3
	this.instance_5 = new lib.Tween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(638,490.1,1,1,0,0,0,-2,50.1);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(111).to({_off:false},0).to({alpha:1},7).to({startPosition:0},226).to({alpha:0},9).wait(1));

	// Layer_4
	this.questiontxt = new lib.questiontext_mccopy2();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(630.9,105.2,1.799,1.799,0,0,0,-1.4,-2.4);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(111).to({_off:false},0).to({alpha:1},7).wait(226).to({alpha:0.02},9).wait(1));

	// Layer_9
	this.instance_6 = new lib.Symbol8("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(639.4,107.7,1,1,0,0,0,0,0.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(54).to({_off:false},0).wait(51).to({startPosition:0},0).to({alpha:0,startPosition:6},5).to({_off:true},1).wait(243));

	// Layer_1
	this.ques1_1 = new lib.Objects_mccopy4_1();
	this.ques1_1.name = "ques1_1";
	this.ques1_1.parent = this;
	this.ques1_1.setTransform(640.3,440.1,2.484,2.484,0,0,0,0.1,0.1);
	this.ques1_1.alpha = 0;
	this.ques1_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.ques1_1).wait(39).to({_off:false},0).to({alpha:1},6).wait(9).to({scaleX:2.73,scaleY:2.73,x:640.2,y:440},10).to({regY:0.2,scaleX:2.48,scaleY:2.48,y:440.3},10).to({scaleX:2.73,scaleY:2.73,x:640.1,y:440.2},10).to({scaleX:2.48,scaleY:2.48},11).to({regY:0.1,x:640.3,y:440.1},10).to({alpha:0.02},5).to({_off:true},1).wait(243));

	// Layer_2
	this.questiontxt_1 = new lib.questiontext_mccopy_1();
	this.questiontxt_1.name = "questiontxt_1";
	this.questiontxt_1.parent = this;
	this.questiontxt_1.setTransform(630.9,105.2,1.799,1.799,0,0,0,-1.4,-2.4);
	this.questiontxt_1.alpha = 0;
	this.questiontxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt_1).wait(20).to({_off:false},0).to({alpha:1},6).wait(79).to({alpha:0},5).to({_off:true},1).wait(243));

	// Layer_2
	this.questiontxt_2 = new lib.questiontext_mccopy();
	this.questiontxt_2.name = "questiontxt_2";
	this.questiontxt_2.parent = this;
	this.questiontxt_2.setTransform(630.9,105.2,1.799,1.799,0,0,0,-1.4,-2.4);
	this.questiontxt_2.alpha = 0;
	this.questiontxt_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt_2).wait(20).to({_off:false},0).to({alpha:1},6).wait(79).to({alpha:0.02},5).to({_off:true},1).wait(243));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(296.7,156.7,1954,1128);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;