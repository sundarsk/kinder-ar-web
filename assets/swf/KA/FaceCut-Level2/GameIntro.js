(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9FEF7").s().p("AgoA8QgUgCgNgHQgfgQgDgdQgBgcAhgTQAQgMATgDIASgDIAVAAQAIABAKAEQAfAJAeAlQAmAtgLAFQgNAFgVABQgTAAgjAGQgJAAgIADQgMADgKAAIgSAAgAgogtQgSAAgKANQgPANAAATQAAAUAPANQAKAMASACIAEAAIAOAAQAKgFAKgJQAMgNAAgUQAAgTgMgNQgKgJgKgEIgOgBIgEABg");
	this.shape.setTransform(35.2,-23.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F9FEF5").s().p("AgKALQgFgEgBgHQABgFAFgFQADgFAHgBQAHABAGAFQAEAFgBAFQABAHgEAEQgGAGgHgBQgHABgDgGg");
	this.shape_1.setTransform(29.9,-25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#090A0B").s().p("AAAAvIgDAAQgSgCgKgMQgPgNAAgUQAAgTAPgNQAKgNASAAIADgBIAOABQAKAEAKAJQANANAAATQAAAUgNANQgKAJgKAFIgOAAgAgbgbQgFAFAAAGQAAAIAFAEQAEAEAHAAQAHAAAGgEQADgEAAgIQAAgGgDgFQgGgFgHAAQgHAAgEAFg");
	this.shape_2.setTransform(31.5,-23.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABfAzQgFgBgFAAQhOgLhNgyQgGABgJgEQgLgGACgMIADgKQAJgNAOAHQAQAGgEAPQgBADgBADQBGAvBJALQAFAAAFAB");
	this.shape_3.setTransform(51.2,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AgwAGIgxAaQASgTADgDQA6g+BVAfQAPAIAQAJQhAgdhSAng");
	this.shape_4.setTransform(30.8,-31.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AAXhEQglhXgQhgQgPhaADhfQAChtAWhcQAeiJBMhhQAdgnAngjQAGgFAHgIQCXiADlgQQAFgBAGAAQAVgCAVAAAJanBQgUhZANhgQAEgPADgOAAXhEQAGhyAQhcQAJgrALgmQAFgNADgNQAMgpAQgmQApgDAyAJQASADAbAGQAHguAKgsQAKgqAKgoQAThAAig5QAAAXAAAaQABB2AjBtQACAHACAGQBTAECEAAQAKAAAKAAAJaFjQgOgBgNgBQgRgBgRgDQgCAAgDAAQgxgGgrgPQiqg0iAiTQhOhcgthpAG8GjIBOAhIhLA/IhlBVIhbBMAG8GjIAAhbAG8GjIjjhbIAAAEIAACkIAADTIARgOIAVgRAJaOQQiQgIhohXQhKg/gZhOAG/IDICbEmAgNufQgCgCgEgEQgMgLgSgVQgXgbgQgMQgOgKgPgIQgdgRgkgFQg4gJhEAJQgpAHgkAMQgKADgMAFQgJADgIAGQgzAZgxA0QgyA2gdBNQBzhJBRgeQAXgOAWAEQgZAMgUAKQg2AegbAoQgeAugDBIQgDBOAjBaQATAuApBHQgGgwARgpQAEgKAHgNQAMgSAUgbQALgQAHgOQAOgWAGgUQACgKADgFQAJgVANAAQAJAAAHAIQACABABADQAFAGADAMQAFAUAEAYQABAMABAOQAHgMAHgOQAIgPAKgNQAUgZASgFQAVgHAdANQAQAHARACQARADASAEQgNgWgFgNQgSgsAEgjQAJgrA3hAQAKgQANgLQAIgKAIgHQADgDAEgCIADgCIgDgBQgEgFgDgCQAAgCgCAAgAAxRSQi/hbgTgcIAAlWQAAiVCWAAIDkAA");
	this.shape_5.setTransform(-0.5,-4.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC3399").s().p("AgaB7QgNgWgFgNQgRgsAEgjQAIgqA2hAQAKgQANgLQAAALAEAMQABAHAOAaQAJAUADAOQAAAQgIATIgTAiQgZAogRAyIgQgCg");
	this.shape_6.setTransform(-6.1,-81.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF9900").s().p("AiqEDQi/hcgUgcIAAlVQAAiUCXAAIDkAAIAADSIAQgOIAVgQQAZBNBKA/QBpBWCPAIIgFEfQlkgBi/hbg");
	this.shape_7.setTransform(21.5,80.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#BAC1C6").s().p("AB5CyIgUgPQgNgLgIgEQgNgGgNAEQgPAFAAAMQgFhUgchJQAAAGgHAWQAEhQgVhJQgfBEgTBHQgbg8ggg3IBlhVICaEkIAABFIgHgDg");
	this.shape_8.setTransform(46.9,65);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#9DACB6").s().p("AhJBJQhLg/gYhNIBahMQAgA4AbA9QAThIAehFQAWBKgEBQQAHgVAAgGQAcBIAGBUQAAgMAOgGQANgEANAGQAIAFANALIAUAPIAHADIAAAiQiPgIhnhXg");
	this.shape_9.setTransform(42.4,69.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C5D7E2").s().p("AhAgWIAAilQAEALAHAGQAOAYAUAOIAMAMQAHAHACAFQABAIgCALQgBADArAbQAtAZgwAXQgwAWAJAOQAJANARADQgqA+ggBGIgRAPg");
	this.shape_10.setTransform(27.7,47.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#DBE3E8").s().p("Ag8AyQgRgDgJgNQgKgOAxgWQAvgXgsgZQgrgbABgEQACgKgBgIQgCgFgIgHIgMgMQgUgOgOgYQgHgGgEgLIAAgDIDiBbIBPAhIhMA+IhkBVIhaBMIgWAQQAghGArg+g");
	this.shape_11.setTransform(36.5,46.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#772326").s().p("ABVAyQhOgLhNgyQgGABgJgEQgLgGACgMIADgKQAJgNAOAHQAQAGgEAPIgCAGQBGAvBJALIAKABIAAANIgKgBg");
	this.shape_12.setTransform(51.2,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#622F0D").s().p("AhMAKQA6g+BVAgIAfARQhAgdhSAmIgxAaIAVgWg");
	this.shape_13.setTransform(30.8,-31.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F3CFA2").s().p("ACGFiIBMhAIhOggIAAhcQArAPAwAGQgwgGgrgPQipg0iAiRQhPhcgthqQAGhzARhbQA2gUAQgYQAdgnA0gaQA2gcAegkQAQgUAFgSQAIAbAWAZQAnApA4ANQAyAKA7gJQAZgDAogLIAAIoQhIgLhIgwIACgGQAEgPgQgGQgOgHgIANIgEAKQgCAMALAGQAKAEAFgBQBOAyBOALIAAB5QgjAVgeAhIgBABIAGAAIA8AVIAAG3gAhLmiIgWAWIAygaQBRgnBBAdIgfgRQgdgLgaAAQgyAAgmAqg");
	this.shape_14.setTransform(30.8,11.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BB2E8C").s().p("Ag4CKIgVgEQATgxAZgpIASghQAJgUAAgPQgDgOgKgVQgNgagBgHQgFgMAAgLQAJgKAIgGIAHgGIACgCQAUAQAbANQALAFAgAMQhNBggdCJQgOAAgOgCg");
	this.shape_15.setTransform(0.6,-82.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#ECC391").s().p("ADaIVIgGAAIABgBQAeghAjgVIAAA+IAAAOIg8gVgAD8IaIAaACIgagCQgSgBgQgEQAQAEASABgAkCiqQAFgOADgMQANgqAQglQApgEAyAKIAsAJQAIguAKgsIAThSQAThBAhg5IAAAyQABB1AkBuIAEANQBSAECEAAIAUAAIAAA2QgoAKgZADQg8AJgxgKQg4gMgngqQgWgZgIgbQgGATgQATQgdAlg2AbQg0AagdAnQgQAYg2AUQAIgsALglgAEQnBIAGgdIAADWQgUhZAOhgg");
	this.shape_16.setTransform(31.9,-23.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#583222").s().p("AHtFJQiEAAhSgEIgFgOQgjhtgBh2IAAgxQgiA5gTBBIgUBSIACgYQAFgWgHgKQgHgJgMgEQgMgBgLAGQgNAFgTAXQAlhBAIhNQgJAUgRAJIAahSQgZARgRAXQAfgsAFgxIhCBPQAahAAQhAQgVAOhCAnQAyhGAfhOQgJAEgLACQCXiADlgRIALADIAqAKIAABAIgIAQIAIADIAAD3Ig6AeQAdAYAdALIAAAfIgHAcQgNBhAUBZIAAAEIgUAAgAloClQgFgZgEgTQgEgMgFgGQgBgDgCgCQgHgIgJAAQgNAAgJAWIg1ARQBSg0BQhKIhfACIBRg6IgkgFQAVguAigiQhXgPhBgYQA2gRAsgcQg1gHgigGIAogaQghAJghABQAkgLApgHQBEgKA4AKQAkAFAeAQIAcASQAQANAXAbQASAUANAMIAFAGQABAAAAAAQABAAAAAAQAAABAAAAQABAAAAABIAGAGIADABIgDACIgGAGQgJAGgIAKQgNAMgKAPQg3BBgJApQgDAkARArQAFAOANAWIgigHQgSgDgQgGQgcgNgWAHQgSAFgUAYQgJANgJAQIgNAZIgCgZgAmnB/QgHATgOAXQgoACgcAAg");
	this.shape_17.setTransform(8.4,-82.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#462517").s().p("AgeFPQgPhZADhfQAChtAWhcQAeiIBMhhQAdgnAngkIANgMQALgCAJgEQgfBOgyBGQBCgnAUgOQgPBAgbBBIBDhQQgFAygfAsQARgXAYgRIgaBSQASgJAIgUQgHBNglBAQASgWAOgFQALgGAMABQAMAEAHAJQAGAJgEAWIgCAYQgKAsgHAuIgtgJQgygKgpAEQgQAlgMAqQgDAMgFAOQgLAlgJAsQgQBbgGBzQglhYgQhggAnnA0QgjhaADhNQADhIAeguQAbgoA2gfIAtgVQgWgFgXAOQhRAfhzBJQAdhNAyg2QAxg0AzgaQAIgFAJgDQAMgGAKgDQAhgBAhgJIgoAaQAhAGA1AHQgrAcg2ARQBBAYBWAPQghAigWAuIAlAFIhRA7IBfgCQhQBKhSA0IA1gRQgDAFgCAKIhYAsQAcAAAogCQgHAOgLAOIggAtQgHANgEALQgRAoAGAwQgphGgTgugAIgiNIA6geIAABBQgegLgcgYgAJRmmIAJgQIAAATIgJgDgAIwoAIgLgDIALgBQAVgCAVAAIAAAQIgqgKg");
	this.shape_18.setTransform(-0.5,-63.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-61.6,-116.2,123.4,231.5);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9FEF5").s().p("AgKALQgFgEAAgHQAAgFAFgFQAEgFAGAAQAHAAAFAFQAEAFAAAFQAAAHgEAEQgFAFgHAAQgGAAgEgFg");
	this.shape.setTransform(29.2,-24.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#090A0B").s().p("AAAAuIgEAAQgQgCgLgLQgOgOAAgTQAAgSAOgNQALgNAQAAIAEgBIANABQAKADAKAKQANANABASQgBATgNAOQgKAJgKAEIgNAAgAgagaQgGAFAAAGQAAAHAGAEQADAEAIAAQAGAAAFgEQAEgEAAgHQAAgGgEgFQgFgFgGAAQgIAAgDAFg");
	this.shape_1.setTransform(30.8,-23);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F9FEF7").s().p("AgoA7QgTgCgNgHQgegPgDgdQgBgbAggUQAQgLASgDIASgDIAVAAQAIABAJAEQAfAJAeAkQAlAtgLAFQgNAEgVABQgSAAgjAGQgIAAgIADQgMADgKAAIgSAAgAgogsQgRAAgKANQgPANAAASQAAATAPAOQAKALARACIAEAAIAOAAQAKgEAKgJQAMgOAAgTQAAgSgMgNQgKgKgKgDIgOgBIgEABg");
	this.shape_2.setTransform(34.4,-23);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABdAyQgFgBgFAAQhMgLhLgxQgGABgJgDQgLgHACgMIADgJQAIgNAPAHQAPAGgEAOQgBADgBADQBFAuBHAMQAFAAAFAA");
	this.shape_3.setTransform(50.2,7.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AgvAGIgwAZQARgSADgDQA5g9BTAfQAPAIAQAJQg/gdhQAmg");
	this.shape_4.setTransform(30.2,-30.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AAXhCQgkhWgQheQgPhYADhdQAChrAWhbQAdiFBLhfQAcgnAmgiQAGgFAHgIQCUh9DggQQAFgBAGAAQAUgCAVAAAJOm4QgUhXAOhfQADgOADgOAAXhCQAFhxARhZQAIgrALglQAFgNADgMQAMgpAPglQApgDAxAJQARADAaAGQAIgtAJgrQAKgqAKgnQASg/Ahg4QAAAXAAAZQABB0AjBrQACAHACAGQBRAECBAAQALAAAJAAAJOFcQgOgBgMgBQgRgBgQgDQgCAAgEAAQgvgGgrgPQimgzh9iPQhNhbgshmAGzGbIAAhaAGzGbIBNAgIhKA+IhjBTIhZBLAGzGbIjehaIAAAEIAAChIAADPIAQgOIAVgQAJON+QiNgIhmhWQhJg9gYhMAG2H5ICYEgAgMuMQgCgCgEgEQgMgLgSgVQgWgagQgMQgOgKgOgIQgdgQgjgFQg3gJhDAJQgoAHgjALQgKADgLAFQgKADgIAGQgyAZgwAzQgxA1gcBLQBxhHBQgeQAWgOAVAEQgYAMgTAJQg1AegbAoQgdAtgEBGQgDBMAjBZQASAtApBFQgGgvAQgoQAEgKAIgNQALgRATgbQALgPAIgOQANgWAGgTQACgKADgFQAIgVANAAQAJAAAHAIQACACABADQAFAFADAMQAFATAEAYQABAMABANQAGgLAHgOQAIgPAKgNQAUgYARgFQAVgGAcAMQAQAHARACQAQADASAEQgNgWgFgNQgRgqAEgjQAIgqA2g/QAKgPANgLQAIgKAIgHQADgDAEgCIACgCIgCgBQgEgEgDgCQAAgCgCAAgAAxQ8Qi8hagTgbIAAlPQAAiSCUAAIDfAA");
	this.shape_5.setTransform(-0.5,-4.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC3399").s().p("AgZB5QgNgWgFgNQgRgrAEgjQAIgoA1hAQAKgPANgLQAAALADAMQABAGAOAaQAJAUADAOQAAAPgIATIgTAhQgYAogRAxIgPgCg");
	this.shape_6.setTransform(-6,-79.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#BAC1C6").s().p("AB2CvIgTgPQgNgLgIgEQgNgGgMAEQgOAFAAAMQgGhSgbhIQAAAGgGAVQADhOgVhIQgeBDgTBGQgag7gfg2IBjhUICWEfIAABEIgHgDg");
	this.shape_7.setTransform(46,63.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#9DACB6").s().p("AhIBHQhJg+gYhLIBZhKQAfA3AaA7QAUhGAdhEQAVBJgEBOQAHgVAAgGQAcBHAFBSQAAgMAOgFQANgEANAGQAIAEAMALIAUAPIAHADIAAAiQiNgJhlhVg");
	this.shape_8.setTransform(41.5,68.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("AimD+Qi8hagTgbIAAlPQAAiRCTAAIDgAAIAADOIAQgOIAVgQQAYBMBJA+QBmBUCMAIIgEEZQldgBi7hZg");
	this.shape_9.setTransform(21.1,78.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C5D7E2").s().p("Ag/gWIAAihQAEAKAHAHQAOAXATAOIAMALQAHAHACAFQABAIgCAKQgBAEAqAaQAsAYgvAXQgvAWAJANQAJAOAQACQgpA9gfBFIgRAOg");
	this.shape_10.setTransform(27.2,46.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#DBE3E8").s().p("Ag7AxQgQgCgJgOQgKgNAwgWQAugXgrgYQgqgaABgEQACgKgBgIQgCgFgIgHIgMgLQgTgOgOgXQgHgHgEgKIAAgEIDeBaIBNAgIhKA9IhiBTIhZBLIgVAQQAfhFAqg9g");
	this.shape_11.setTransform(35.8,45.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#772326").s().p("ABTAxQhMgLhLgxQgGABgJgDQgLgHACgMIADgJQAIgNAPAHQAPAGgEAOIgCAGQBFAuBHAMIAKAAIAAANIgKgBg");
	this.shape_12.setTransform(50.2,7.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#622F0D").s().p("AhLAKQA5g9BTAfIAfARQg/gdhQAmIgwAZIAUgVg");
	this.shape_13.setTransform(30.2,-30.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F3CFA2").s().p("ACEFbIBKg+IhNghIAAhZQAqAPAwAFQgwgFgqgPQilgzh+iOQhMhbgthoQAGhwAQhZQA1gUAQgXQAcgnA0gZQA0gbAdgkQAQgTAFgSQAIAaAWAYQAnApA2AMQAwALA7gKQAYgCAogKIAAIdQhIgMhGgvIACgGQAEgOgPgHQgPgGgHANIgEAJQgCAMALAGQAJAEAGgBQBMAxBNALIAAB2QgjAVgdAgIgBABIAFAAIA8AVIAAGvgAhKmaIgUAWIAvgaQBRgmA/AdIgfgSQgcgKgZAAQgxAAgmApg");
	this.shape_14.setTransform(30.2,11.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BB2E8C").s().p("Ag3CHIgVgDQATgxAYgoIATghQAHgTABgPQgDgOgKgUQgNgZgBgGQgEgMAAgLQAJgKAHgHIAHgFIADgCQASAQAbAMQAMAGAeAMQhKBegeCFQgNAAgOgCg");
	this.shape_15.setTransform(0.6,-80.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#ECC391").s().p("ADVILIgFAAIABgBQAdghAjgUIAAA9IAAANIg8gUgAD3IPIAaADIgagDQgRAAgRgEQARAEARAAgAj8inQAEgNADgMQAMgoAQglQApgEAwAJIAsAJQAHgtAKgrIAThQQATg/Agg5IAAAxQABBzAjBrIAEANQBRAECBAAIAUAAIAAA1QgoAKgYADQg7AKgwgLQg2gMgngpQgWgZgIgaQgFASgQATQgdAkg0AbQg0AagcAmQgQAXg1AUQAJgrALglgAEKm4IAHgcIAADRQgUhXANheg");
	this.shape_16.setTransform(31.3,-22.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#583222").s().p("AHjFCQiBAAhRgEIgEgNQgjhrgBh0IAAgwQghA4gSA/IgUBRIACgYQAEgVgGgKQgGgJgNgEQgLgBgLAGQgNAFgTAXQAlhAAHhMQgIAUgRAJIAZhRQgYARgRAXQAfgrAFgwIhCBNQAag/AQg+QgUANhBAnQAxhFAehNQgJAEgKACQCUh9DggQIALADIApAJIAAA/IgIAQIAIADIAADyIg4AdQAbAXAdALIAAAeIgGAcQgOBfAUBXIAAAEIgUAAgAlhChQgEgYgFgTQgDgMgFgFQgBgDgCgCQgHgIgJAAQgNAAgIAVIg0ARQBRgzBOhIIheACIBQg5IgkgFQAVgtAhgiQhUgOhBgXQA1gRArgbQg0gHgggGIAmgaQggAJggABQAjgLAogHQBDgJA3AJQAjAFAdAQQAOAIAOAKQAQAMAWAaQASAVAMALIAGAGQAAAAABAAQAAAAAAAAQAAABABAAQAAAAAAABIAHAGIACABIgCACIgHAFQgIAHgIAKQgNALgKAPQg2A/gIApQgEAjARAqQAFANANAWIgigHQgRgCgQgHQgcgMgVAGQgRAFgUAYQgKANgIAPIgNAZIgCgZgAmfB8QgGATgNAWQgoADgcAAg");
	this.shape_17.setTransform(8.2,-80.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#462517").s().p("AgdFIQgPhYADhcQAChrAWhaQAdiFBLhfQAcgnAmgjIANgMQAKgCAJgEQgeBMgxBGQBBgnAUgNQgQA+gaBAIBChPQgFAygfAqQARgWAYgRIgZBRQARgJAIgUQgHBMglA/QATgWANgFQALgGALABQANAEAGAIQAGAKgEAVIgCAXQgJArgIAuIgrgJQgxgJgpADQgPAlgMApQgDAMgFANQgLAlgIArQgRBZgFBwQgkhVgQhfgAndAyQgjhXADhMQAEhHAdgtQAbgnA1geIArgVQgVgEgWANQhQAehxBIQAchMAxg1QAwgzAygYQAIgGAKgDQALgFAKgDQAggBAggJIgmAZQAgAGA0AIQgrAbg1ARQBBAWBUAPQghAhgVAuIAkAEIhQA7IBegDQhOBJhRAzIA0gRQgDAFgCAKIhXAsQAcgBAogCQgIANgLAPIgeAsQgIANgEAKQgQAoAGAvQgphGgSgtgAIWiKIA4gdIAAA/QgdgLgbgXgAJGmeIAIgPIAAATIgIgEgAIln1IgLgEIALgBQAUgCAVAAIAAAQIgpgJg");
	this.shape_18.setTransform(-0.5,-62.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.4,-113.9,121,226.9);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9FEF5").s().p("AgLAMQgGgEAAgIQAAgGAGgFQAEgGAHAAQAIAAAFAGQAFAFAAAGQAAAIgFAEQgFAGgIAAQgHAAgEgGg");
	this.shape.setTransform(31.7,-24.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#090A0B").s().p("AAAAyIgEAAQgTgCgLgMQgQgPAAgVQAAgUAQgPQALgNATAAQABAAAAAAQAAAAABAAQAAgBABAAQABAAAAAAIAOABQAMADALAKQAOAPAAAUQAAAVgOAPQgLAJgMAFIgOAAgAgdgdQgGAFAAAHQAAAIAGAEQAEAFAIAAQAIAAAFgFQAEgEAAgIQAAgHgEgFQgFgGgIAAQgIAAgEAGg");
	this.shape_1.setTransform(33.5,-23);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F9FEF7").s().p("AgrBBQgVgCgPgJQgggQgDgfQgCgfAjgVQASgMAUgEIATgDIAXAAQAJABAKAFQAiAKAgAnQApAxgMAFQgPAFgWABQgUAAgmAHQgJAAgJADQgNAEgLAAIgTAAgAgrgwQgTAAgLANQgQAPAAAUQAAAVAQAPQALAMATACIAEAAIAPAAQALgFALgJQANgPAAgVQAAgUgNgPQgLgKgLgDIgPgBQgBAAAAAAQgBAAgBABQAAAAgBAAQAAAAAAAAg");
	this.shape_2.setTransform(37.4,-23);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABmA3QgFgBgGAAQhUgMhSg2QgGABgKgEQgMgHACgNIAEgKQAJgNAPAGQARAHgFAQQgBADgBADQBMAzBOAMQAGAAAFAB");
	this.shape_3.setTransform(54.5,10.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("Ag0AGIg0AcQATgUADgDQA/hDBbAiQAQAIARAKQhFgfhYApg");
	this.shape_4.setTransform(32.7,-31.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AARAFQgohdgShnQgQhgADhmQADh0AXhkQAhiSBShnQAegqAqgmQAGgGAIgIQCeiFEyADQAIAAAIAAAJ9mSQgWhgAPhnQADgQAEgPAARAFQAGh6AShiQAJgvAMgoQAFgOADgOQANgsASgpQArgDA2AKQATADAdAHQAIgyALguQAKguALgrQAUhFAkg9QAAAaAAAbQABB+AmB1QADAIACAHQBYAECNAAQAMAAAKAAAHTIQIBVAiIhRBEIhsBbIhhBSAJ9HLQgPgBgNgBQgTgCgSgDQgCAAgEAAQg0gHgvgPQi1g4iJidQhUhjgwhxAHTIQIAAhiAHTIQIjzhiIAAAEIAACwIAADhIATgPIAXgRAJ9QfQiagJhvhdQhQhDgahTAHXJ2ICmE7AgWuSQgCgDgFgEQgNgMgTgWQgYgdgSgOQgPgKgPgJQgggSgmgFQg9gKhIAKQgsAIgmAMQgLADgNAGQgKADgJAGQg2Abg1A4Qg1A6gfBTQB7hPBXghQAZgPAXAFQgbANgVALQg6AggdArQggAxgEBNQgDBUAmBhQAUAxAsBLQgGgzASgsQAEgLAIgOQANgTAUgdQANgRAHgOQAPgZAHgVQACgKADgGQAJgXAOAAQAKAAAIAJQACABABADQAFAHAEANQAFAVAFAaQABANABAOQAGgMAIgPQAJgQAKgPQAXgaASgFQAXgIAfAOQARAHATADQARADATAFQgNgYgGgOQgTgvAFgmQAJguA7hFQALgRANgMQAJgLAJgGQADgEAFgCIACgDIgCgBQgFgEgDgCQAAgCgCAAgAiTQ3QgYAFgKg8IAAj/QAAifChAAID0AA");
	this.shape_5.setTransform(0,-10.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC3399").s().p("AgcCEQgOgYgFgOQgTgvAEgmQAJgsA6hFQALgRAOgMQAAAMAEANQABAHAPAcQAKAWADAPQAAAQgJAVQgDAFgRAfQgbAsgSA1IgRgCg");
	this.shape_6.setTransform(-6.8,-85.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#BAC1C6").s().p("ACBC/QgHgEgPgMQgMgMgKgFQgNgGgOAEQgQAGAAANQgGhagehOQABAGgIAXQAEhVgWhPQgiBKgVBMQgchBgig7IBshbIClE5IAABKQgEgBgEgCg");
	this.shape_7.setTransform(50,71.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#9DACB6").s().p("AhOBOQhQhEgbhSIBihRQAhA7AdBCQAUhNAihKQAWBPgEBWQAIgXAAgGQAeBNAGBaQAAgNAPgGQAOgEAOAGQAJAFANAMQAPAMAGAEQAEACAEABIAAAlQiZgJhuhdg");
	this.shape_8.setTransform(45.1,76.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("Al2B/QgYAFgKg8IAAj+QAAigChABID1AAIAADhIARgPIAXgSQAaBTBQBDQBvBdCaAJIgFDwQnJgWlBjCg");
	this.shape_9.setTransform(22.7,84.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C5D7E2").s().p("AhFgYIAAiwQAFALAHAIQAPAZAVAPIAOAMQAHAIACAFQABAJgCALQgBAEAuAdQAwAagzAZQg0AYAKAPQAKAOARADQgsBCgiBMIgTAPg");
	this.shape_10.setTransform(29.4,52.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#DBE3E8").s().p("AhBA1QgRgCgKgPQgLgOA1gZQAygYgvgbQgugcABgEQACgLgBgJQgCgGgIgIIgOgLQgVgQgPgZQgHgIgFgKIAAgEIDyBiIBVAiIhRBDIhrBbIhhBSIgXARQAihLAthDg");
	this.shape_11.setTransform(38.8,51.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#772326").s().p("ABbA2QhUgMhSg2QgGABgKgEQgMgHACgNIAEgKQAJgNAPAGQARAHgFAQIgCAGQBMAzBOAMIALABIAAAOIgLgBg");
	this.shape_12.setTransform(54.5,10.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#622F0D").s().p("AhSALQA/hDBbAiIAhASQhFgfhYApIg0AcIAWgXg");
	this.shape_13.setTransform(32.7,-31.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F3CFA2").s().p("ACQF7IBRhEIhUgjIAAhiQAuAQA1AHQg1gHgugQQi1g4iJicQhUhigwhyQAHh6ARhiQA6gWASgZQAegqA3gcQA7geAfgnQARgVAGgTQAJAcAYAbQAqAtA8ANQA0ALBBgKQAbgDAqgLIAAJPQhPgMhMg0IACgGQAFgQgRgHQgQgGgIANIgEAKQgCANAMAHQAKAEAGgBQBTA2BUAMIAACCQgmAWgfAkIgBABIAGAAIBAAWIAAHWgAhRnAIgWAYIA0gcQBYgrBFAgIgigSQgegMgcAAQg2AAgpAtg");
	this.shape_14.setTransform(32.7,14.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BB2E8C").s().p("Ag8CUIgWgEQATg1AbgsQAQgfAEgFQAJgUAAgRQgEgPgKgWQgOgcgBgHQgEgNgBgMQAKgLAIgHQADgDAFgCIACgDQAVARAdAPIAuASQhRBnghCSQgOAAgPgCg");
	this.shape_15.setTransform(0.3,-86.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#ECC391").s().p("ADqI8IgGAAIABgBQAfgkAmgWIAABBIAAAQIhAgWgAEOJBIAcABIgcgBQgTgCgRgDQARADATACgAkUi3IAJgbQAMgtASgoQArgDA2AKQATADAdAHQAIgyALgvQAKgtALgrQAUhFAkg9IAAA1QABB+AlB1IAFAOQBZAFCMAAIAWAAIAAA5QgqALgbAEQhBAJg1gKQg7gOgqgtQgYgagJgdQgGAUgRAUQgfAng7AeQg3AdgeApQgSAZg6AWQAJgvAMgpgAEkniIAGgeIAADlQgVhfAPhog");
	this.shape_16.setTransform(33.9,-22.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#583222").s().p("AIQFUQiNAAhZgEIgEgPQgmh1gBh+IAAg1QgkA9gVBFQgKArgKAuIABgaQAFgYgGgLQgJgJgMgEQgNgBgMAGQgPAFgUAaQAohGAIhTQgJAVgTAKIAchXQgZARgUAYQAigtAGg2IhIBVQAchFARhEQgWAPhHAqQA2hMAhhTQgKAEgLACQCeiFExADIAAA6IgJARIAJADIAAEJIg9AgQAeAZAfAMIAAAhIgGAfQgPBnAVBgIAAAEIgVAAgAmCCkQgFgagFgVQgEgNgFgHQgBgDgCgBQgHgJgKAAQgOAAgKAXIg4ATQBYg4BVhOIhmACIBXhAIgngFQAXgxAkglQhdgQhFgZQA5gSAvgeQg5gIgkgGIArgcQgjAKgjABQAmgMAsgIQBJgKA8AKQAnAFAfASIAeATQASAOAYAdQAUAWAMAMIAHAHQABAAAAAAQABAAAAAAQAAABAAAAQAAAAAAABIAIAGIACABIgCADQgFACgDAEQgIAGgKALQgNAMgLARQg7BFgJAtQgFAmAUAvQAFAOAOAYIglgIQgTgDgRgHQgfgOgWAIQgTAFgXAaQgKAPgJAQIgOAbIgCgbgAnGB7QgGAVgQAZQgrACgeAAg");
	this.shape_17.setTransform(8.7,-84.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#462517").s().p("AggFCQgQhgADhmQACh0AYhjQAgiSBShoQAegpAqgnIAOgNQAMgCAKgEQgiBTg2BMQBHgrAWgPQgQBFgdBGIBIhWQgGA1ghAvQATgYAagSIgcBYQASgKAKgVQgJBTgnBGQAUgaAOgGQAMgFANABQANADAIAKQAGALgEAXIgCAZQgLAvgIAyQgdgHgTgDQg2gKgrADQgSAogNAtIgIAbQgMApgJAvQgSBigGB7QgohegRhngAoJASQgmhfADhUQADhNAhgxQAcgrA6ggIAwgYQgXgFgZAPQhWAhh8BOQAfhSA2g6QA0g4A3gbQAIgGAKgDQANgGALgDQAjgCAjgKIgqAcQAjAHA5AIQgvAeg5ASQBGAZBcAQQgkAlgXAwIAnAGIhXA/IBngBQhWBPhYA4IA5gTQgDAGgDAKIheAwQAeAAArgCQgIAOgMAQIgiAvIgMAaQgRAsAFAzQgrhLgUgygAJHi8IA+ghIAABGQgggMgegZgAJ8nqIAJgQIAAAUIgJgEg");
	this.shape_18.setTransform(-0.8,-62.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-66.3,-119.7,132.6,238.4);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9FEF5").s().p("AgKALQgEgEAAgHQAAgEAEgGQAEgEAGAAQAHAAAFAEQADAGAAAEQAAAHgDAEQgFAFgHgBQgGABgEgFg");
	this.shape.setTransform(28,-22);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#090A0B").s().p("AAAAsIgDAAQgRgCgKgKQgOgNAAgTQAAgRAOgNQAKgMARAAIADgBIANABQAKADAJAJQAMANAAARQAAATgMANQgJAIgKAEIgNAAgAgagaQgEAGAAAFQAAAHAEAEQAEAEAHAAQAHAAAFgEQADgEAAgHQAAgFgDgGQgFgEgHAAQgHAAgEAEg");
	this.shape_1.setTransform(29.6,-20.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F9FEF7").s().p("AgmA5QgTgCgMgHQgdgPgDgbQgBgaAfgTQAPgMASgCIARgDIAUAAQAIABAJADQAdAKAdAiQAkArgLAFQgMAEgUABQgSAAghAGQgIAAgIADQgLADgKAAIgRAAgAgmgqQgRAAgJAMQgOANAAARQAAATAOANQAJAKARACIAEAAIANAAQAKgEAJgIQAMgNAAgTQAAgRgMgNQgJgJgKgDIgNgBIgEABg");
	this.shape_2.setTransform(33.1,-20.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABaAxQgFgBgFAAQhKgLhIgvQgGABgIgEQgLgGACgMIADgIQAIgMAOAFQAOAHgEANQAAADgBADQBCAsBFALQAFAAAFAB");
	this.shape_3.setTransform(48.1,9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AgtAGIgvAYQARgRADgDQA3g7BQAeQAPAHAPAJQg9gchNAlg");
	this.shape_4.setTransform(28.9,-27.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AAPAEQgjhSgQhaQgOhVADhaQAChnAVhYQAciABIhcQAbglAlghQAGgFAHgHQCKh2EOADQAHAAAIAAAIxljQgThUAOhbQACgOADgNAAPAEQAFhrAQhXQAIgpAKgkQAFgMADgMQALgnAQgkQAmgDAvAJQARADAaAGQAHgsAJgpQAJgpAKglQARg9Ahg2QAAAXAAAYQABBvAhBnQACAHACAGQBOAEB8AAQALAAAIAAAIxGUQgNgBgLAAQgRgCgQgCQgCAAgDAAQgugHgpgNQiggyh5iKQhKhXgqhkAGcHRIAAhWAGcHRIBKAfIhHA8IhfBQIhWBIAGcHRIjWhWIAAADIAACbIAADHIAQgNIAUgPAIxOiQiHgIhihSQhHg7gXhJAGfIsICSEVAgTsmQgCgDgEgEQgLgKgSgUQgVgZgQgMQgNgJgNgIQgcgQgigFQg1gIhAAIQgnAHgiALQgJADgMAFQgJADgHAFQgwAYguAyQgwAygbBJQBthFBMgdQAWgNAVAEQgYALgTAKQgzAdgZAmQgdArgDBEQgCBJAhBWQASArAmBDQgFguAQgmQAEgKAGgNQAMgQASgaQALgOAGgNQAOgWAFgSQADgKACgEQAIgVANAAQAIAAAHAIQACABABADQAFAGADALQAEASAEAYQABALABANQAGgLAHgNQAHgPAKgNQAUgWAQgFQAUgHAbAMQAPAHARACQAPADARAEQgMgVgFgNQgQgpADgiQAIgoA0g9QAKgPAMgKQAIgKAIgGQADgDAEgCIACgCIgCgBQgEgEgDgCQAAgBgCAAgAiCO4QgUAEgKg1IAAjhQAAiNCPAAIDXAA");
	this.shape_5.setTransform(0,-9.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BAC1C6").s().p("AByCoIgTgOQgMgLgIgEQgMgFgMAEQgOAFAAALQgFhPgahFQAAAFgGAVQADhLgUhGQgeBBgSBDQgZg5geg0IBfhQICSEUIAABBIgHgDg");
	this.shape_6.setTransform(44.1,63.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#9DACB6").s().p("AhFBEQhHg7gWhIIBVhIQAeA1AZA6QAShEAdhCQAVBGgFBMQAIgVgBgFQAbBEAFBQQAAgMANgFQANgEAMAFQAIAFALALIATANIAIAEIAAAfQiIgHhhhTg");
	this.shape_7.setTransform(39.8,67.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C5D7E2").s().p("Ag8gVIAAibQAEAJAGAIQANAVATAOQAJAJADABQAGAHACAFQABAIgCAJQgBAEApAZQAqAYgtAVQguAWAJAMQAJAOAPACQgnA6geBDIgQANg");
	this.shape_8.setTransform(25.9,46.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DBE3E8").s().p("Ag5AvQgPgCgJgNQgKgNAvgVQAsgWgpgXQgpgZABgEQACgKgBgIQgCgFgHgHQgDgBgJgJQgTgNgNgWQgGgHgEgKIAAgDIDVBWIBKAfIhHA7IheBQIhWBIIgUAPQAehCAog7g");
	this.shape_9.setTransform(34.2,45.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#772326").s().p("ABQAwQhKgLhIgvQgGABgIgEQgLgGACgMIADgIQAIgMAOAFQAOAHgEANIgBAGQBCAsBFALIAKABIAAANIgKgBg");
	this.shape_10.setTransform(48.1,9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF9900").s().p("AlKBwQgVAEgJg0IAAjgQAAiNCOAAIDYAAIAADHIAQgNIAUgQQAXBJBGA7QBjBSCHAIIgEDTQmTgTkcirg");
	this.shape_11.setTransform(20.1,74.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CC3399").s().p("AgYB0QgMgUgFgNQgRgqAEghQAIgnAzg9QAKgPAMgLQAAALAEAMQAAAGANAYQAJAUADANQAAAPgIASIgRAgQgYAmgQAwIgPgDg");
	this.shape_12.setTransform(-6,-75.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#622F0D").s().p("AhIAKQA3g7BQAeQAPAHAPAJQg9gchNAlIgvAYIAUgUg");
	this.shape_13.setTransform(28.9,-27.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F3CFA2").s().p("AB/FOIBHg8IhKgeIAAhXQApAOAuAGQgugGgpgOQifgxh5iKQhKhWgqhlQAFhsAQhWQAzgTAPgXQAbgkAxgZQAzgaAcgjQAQgSAEgRQAIAZAVAYQAlAnA1AMQAuAJA5gIQAYgDAlgKIAAIJQhFgLhDgtIABgGQAEgNgOgHQgOgFgIAMIgDAIQgCAMALAGQAIAEAGgBQBJAvBKALIAAByQghATgcAgIgBABIAFAAIA5ATIAAGfgAhHmLIgUAVIAugZQBOglA8AcQgPgJgOgHQgcgLgYAAQgvAAgkAog");
	this.shape_14.setTransform(28.8,12.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BB2E8C").s().p("Ag0CDIgUgDQAQgwAYgmIASggQAIgSAAgPQgDgNgJgTQgNgZgBgGQgDgMAAgKQAHgKAIgGIAHgFIACgCQASAPAaANIApAQQhJBbgcCAIgZgBg");
	this.shape_15.setTransform(0.3,-76.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#ECC391").s().p("ADOH5IAgAEIAZABIgZgBIgggEIgFAAIABgBQAbggAigTIAAA5IAAAOIg5gTgAjzihIAHgYQAMgnAPgkQAngDAvAJQAQADAaAGQAHgsAKgpIAShOQASg9Afg2IAAAvQABBvAiBnIADANQBPAEB8AAIATAAIAAAyQgmAKgXADQg5AJgvgKQg1gMglgnQgUgYgIgZQgFARgPATQgcAigzAaQgxAZgbAlQgQAWgzATQAIgpALgkgAEBmoIAGgbIAADKQgThUANhbg");
	this.shape_16.setTransform(29.9,-20.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#462517").s().p("AgdEcQgOhVADhaQADhmAUhXQAciBBJhbQAbglAkgiQAGgEAHgIQAKgBAJgEQgeBJgwBDQA/glATgNQgOA8gZA+IA/hMQgFAvgdApQARgVAWgQIgYBOQAQgJAIgTQgHBJgjA+QASgXAMgEQAKgGAMABQAMAEAGAIQAHAKgEAVIgDAVQgJApgHAtQgagHgQgCQgwgJgmADQgPAjgMAoIgHAYQgLAjgIAqQgQBWgFBsQgjhSgQhbgAnLAQQgihUAChKQAEhEAdgrQAZgmAygcIArgVQgUgFgXAOQhMAdhsBFQAbhJAvgzQAugxAwgZQAHgEAJgDQAMgGAJgCQAfgBAfgJIglAZIBRAMQgpAagzARQA+AWBSAOQggAggVArIAjAFIhNA4IBagCQhLBGhOAxIAygQQgCAFgDAJIhTAqQAbAAAlgCQgGANgLAPIgeApIgKAWQgQAnAGAtQgnhCgRgsgAIDimIA2gcIAAA9QgcgLgagWgAIwmvIAJgPIAAASIgJgDg");
	this.shape_17.setTransform(-0.7,-54.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#583222").s().p("AHSEsQh8AAhPgEIgDgNQgihngBhvIAAgvQghA2gRA9IgTBOIADgXQAEgVgHgJQgGgJgMgDQgMgBgKAFQgMAFgSAWQAjg9AHhKQgIATgQAJIAYhMQgWAPgRAVQAdgoAFgvIg/BKQAZg8AOg9QgTANg/AlQAwhCAehKQgJAEgKACQCKh2EPADIAAAzIgJAPIAJADIAADpIg2AdQAaAWAcALIAAAdIgGAbQgOBbAUBUIAAAEIgTAAgAlVCRIgIgqQgDgLgFgGIgDgEQgHgIgIAAQgMAAgJAVIgyAQQBOgyBLhEIhaACIBNg4IgjgFQAVgrAgghQhSgOg+gWQAzgQApgaIhRgNIAlgZQgfAJgfABQAigLAngHQBAgIA2AIQAhAFAcAQIAaARQARAMAUAZIAdAeIAGAHQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIAHAGIACABIgCACIgHAFQgIAGgHAKQgMAKgLAPQg0A9gHAnQgEAiAQApQAFANAMAVIgggHQgQgCgPgHQgcgMgUAHQgQAFgUAWQgKANgHAPIgNAYIgCgYgAmRBtQgFASgOAWQglACgbAAg");
	this.shape_18.setTransform(7.7,-74.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-58.5,-105.7,117.2,210.4);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ag5BaIABg1IAAgnIAAgOIgugBIAAgnIAxAAQACgVAGgUQAHgUALgPQAMgPARgKQASgJAYAAQAOAAAPAFQAPAEAPANIgVAjQgLgIgKgDQgKgEgJAAQgJgBgIABQgJACgGAFQgGAEgEAIQgFAIgDAMQgDAMgCASIBWAAIgFAoIhTgBIAAARIAAA3IAAAaIAAAbIAAAdIgBAcIgqABIABhNg");
	this.shape.setTransform(212.6,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AANCxQgMgEgGgIQgJgHgGgKQgGgJgEgKQgKgXgDgeIAGkBIAqAAIgCBCIgCA3IgBAtIAAAhIgBA3QAAATAFAPIAFANQADAGAFAFQAFAFAGADQAHACAIABIgEAoQgPAAgLgFg");
	this.shape_1.setTransform(196.9,-2.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AA/B0IADgdQgZAQgYAIQgXAHgVAAQgOAAgOgEQgOgEgKgHQgLgJgGgLQgHgNAAgRQAAgQAFgNQAFgNAJgKQAIgJAMgHQAMgHANgFQANgEAPgCQAOgDAOAAIAZABIAVADIgHgUQgEgKgFgHQgHgJgIgFQgJgEgMAAQgIAAgKACQgKADgMAGQgMAFgPAMQgOAKgRARIgYgeQATgTASgLQASgMAQgHQAQgGANgDQANgCALAAQASAAAPAGQAPAHALALQALALAIAPQAHAPAFASQAFAQACATQACASAAASQAAATgCAXIgHAxgAgJgBQgRAEgNAIQgMAIgGAMQgHAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVIgVgEQgLgCgMABQgSAAgQADg");
	this.shape_2.setTransform(176.1,4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AA3CcQAGgUACgSIAEgiIAAghQgBgVgGgQQgHgOgJgKQgKgJgLgEQgLgFgMAAQgKABgMAFQgKAFgMALQgLAJgKATIgCCHIgoABIgDk6IAvgBIgBB8QALgNAMgGQANgIALgDQANgEALgCQAXAAATAJQATAHANAQQAOAPAHAWQAIAVABAcIAAAdIgBAdIgDAdQgCANgEAJg");
	this.shape_3.setTransform(150.1,-0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgsCqIgTgGQgKgFgJgGQgJgHgHgJQgIgKgFgMIAogRQAEAIAFAHQAGAGAGAEQAGAFAHACIANAEQAPADAPgCQAPgCAKgGQALgGAHgHQAHgIAFgIIAHgPIAEgOIABgJIAAgbQgNAMgNAGQgNAGgLADQgNAEgMAAQgYAAgUgIQgUgIgPgQQgQgPgIgVQgJgWAAgcQAAgbAKgWQAJgWAQgPQAQgPATgHQAUgIAUAAQANABAOAFQAMAEAOAHQAOAHALAMIABgpIAoACIgBDpQgBANgDAOQgDANgGAMQgHAMgJALQgKALgMAJQgNAIgPAGQgPAFgSABIgGABQgVAAgTgGgAgeh7QgNAGgIAKQgJALgFAOQgFAPAAARQAAAQAFAOQAFAOAJAKQAJAKANAFQAMAGAPAAQANAAAMgFQANgEAKgJQALgHAHgMQAHgMACgOIAAgbQgCgOgHgMQgHgMgLgIQgLgJgNgFQgNgEgMAAQgPAAgMAGg");
	this.shape_4.setTransform(110.7,9.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AhfAyIgBgyIgCgnIgBgeIgBgrIAvgBIABA7QAGgMAJgKQAIgKAKgIQAKgIALgFQALgFAMgBQAQgBAMAEQALAEAIAGQAIAHAGAJQAFAIADAKQADAJABAKIACARQABAiAAAjIgCBKIgqgBIAEhCQABghgCggIAAgKIgDgMIgEgNQgDgHgFgEQgFgFgHgDQgHgCgKABQgQADgPAVQgRAVgTAoIABA0IABAdIABAQIgsAGIgDhAg");
	this.shape_5.setTransform(84.7,4.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgZCZIABg0IABhAIABhXIAqgBIgCA0IgBAtIAAAkIgBAbIAAAsgAgKhhQgGgBgDgFQgFgDgCgGQgCgFAAgGQAAgGACgFQACgFAFgEQADgEAGgDQAFgDAFABQAGgBAFADQAFADAFAEIAGAJQACAFAAAGQAAAGgCAFIgGAJQgFAFgFABQgFADgGAAQgFAAgFgDg");
	this.shape_6.setTransform(67,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA3CcQAGgUACgSIAEgiIAAghQgBgVgGgQQgHgOgJgKQgKgJgLgEQgLgFgMAAQgKABgMAFQgKAFgMALQgLAJgKATIgCCHIgoABIgDk6IAvgBIgBB8QALgNAMgGQANgIALgDQANgEALgCQAXAAATAJQATAHANAQQAOAPAHAWQAIAVABAcIAAAdIgBAdIgDAdQgCANgEAJg");
	this.shape_7.setTransform(48,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AglBuQgVgJgRgQQgRgQgKgVQgKgWAAgaQAAgOAEgPQAEgOAIgOQAIgNALgLQAKgLAOgIQANgJAPgEQAQgFAQAAQASAAAQAFQAQAEANAIQAOAIALAMQALALAHAPIAAABIgiAUIAAgBQgFgKgIgIQgHgJgKgGQgJgFgLgDQgLgEgLAAQgQAAgOAHQgPAGgLALQgLALgGAPQgHAPAAAPQAAARAHAPQAGAOALALQALALAPAHQAOAGAQAAQALAAAKgDQAKgDAJgFQAJgFAIgHQAHgIAGgJIAAgBIAkAUIgBABQgIANgLALQgLALgOAHQgOAIgPAEQgQAEgQAAQgWAAgWgJg");
	this.shape_8.setTransform(21.5,4.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIABhZIAogDIgBBcIBPgDIgDAmIhNACIgBCxIgqABg");
	this.shape_9.setTransform(-1.1,0.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AA/B0IADgdQgZAQgYAIQgXAHgVAAQgOAAgOgEQgOgEgKgHQgLgJgGgLQgHgNAAgRQAAgQAFgNQAFgNAJgKQAIgJAMgHQAMgHANgFQANgEAPgCQAOgDAOAAIAZABIAVADIgHgUQgEgKgFgHQgHgJgIgFQgJgEgMAAQgIAAgKACQgKADgMAGQgMAFgPAMQgOAKgRARIgYgeQATgTASgLQASgMAQgHQAQgGANgDQANgCALAAQASAAAPAGQAPAHALALQALALAIAPQAHAPAFASQAFAQACATQACASAAASQAAATgCAXIgHAxgAgJgBQgRAEgNAIQgMAIgGAMQgHAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVIgVgEQgLgCgMABQgSAAgQADg");
	this.shape_10.setTransform(-24.7,4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("ABiAxIgBgsIAAghQgBgQgEgJQgFgHgIgBQgFAAgFAEQgGACgFAGIgKALIgJAMIgJANIgHALIABATIAAAYIABAfIAAAmIgnACIAAg/IgCgsIgCghQgBgQgEgJQgFgHgIgBQgFAAgGAEQgFADgGAGIgLAMIgKAPIgJANIgIAKIACBsIgoACIgGjaIArgFIAAA7IAOgQQAHgJAJgHQAIgHAKgFQAKgEAMAAQAJAAAIACQAIADAFAGQAHAFAEAKQAFAIABAMIANgQQAHgIAJgGQAIgIAJgEQAKgEAMAAQAJAAAIACQAJAEAHAGQAGAHAEAKQAFAKAAAOIACAlIACAwIABBIIgrACIAAg/g");
	this.shape_11.setTransform(-54.1,3.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIABhZIAogDIgBBcIBPgDIgDAmIhNACIgBCxIgqABg");
	this.shape_12.setTransform(-91.8,0.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AglBuQgVgJgRgQQgRgQgKgVQgKgWAAgaQAAgOAEgPQAEgOAIgOQAIgNALgLQAKgLAOgIQANgJAPgEQAQgFAQAAQASAAAQAFQAQAEANAIQAOAIALAMQALALAHAPIAAABIgiAUIAAgBQgFgKgIgIQgHgJgKgGQgJgFgLgDQgLgEgLAAQgQAAgOAHQgPAGgLALQgLALgGAPQgHAPAAAPQAAARAHAPQAGAOALALQALALAPAHQAOAGAQAAQALAAAKgDQAKgDAJgFQAJgFAIgHQAHgIAGgJIAAgBIAkAUIgBABQgIANgLALQgLALgOAHQgOAIgPAEQgQAEgQAAQgWAAgWgJg");
	this.shape_13.setTransform(-114.3,4.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AA/B0IADgdQgZAQgYAIQgXAHgVAAQgOAAgOgEQgOgEgKgHQgLgJgGgLQgHgNAAgRQAAgQAFgNQAFgNAJgKQAIgJAMgHQAMgHANgFQANgEAPgCQAOgDAOAAIAZABIAVADIgHgUQgEgKgFgHQgHgJgIgFQgJgEgMAAQgIAAgKACQgKADgMAGQgMAFgPAMQgOAKgRARIgYgeQATgTASgLQASgMAQgHQAQgGANgDQANgCALAAQASAAAPAGQAPAHALALQALALAIAPQAHAPAFASQAFAQACATQACASAAASQAAATgCAXIgHAxgAgJgBQgRAEgNAIQgMAIgGAMQgHAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVIgVgEQgLgCgMABQgSAAgQADg");
	this.shape_14.setTransform(-140.7,4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AACAfIg9BPIgrgLIBOhjIhQhjIArgKIA+BOIA+hPIAmAPIhKBfIBOBgIgmAPg");
	this.shape_15.setTransform(-164.5,4.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AgtBtQgVgIgOgOQgPgQgHgUQgIgWABgaQAAgYAJgWQAIgXAPgPQAPgRAUgKQAVgJAWAAQAWAAASAJQATAIAOAOQAPAPAKAUQAJATACAXIivAaQABAPAGALQAFALAKAJQAJAIALADQAMAFAMAAQAMAAAKgEQALgDAJgHQAIgGAHgJQAGgKAEgNIAnAIQgFATgLAPQgKAPgOALQgOALgQAFQgSAHgSgBQgZABgVgJgAgQhPQgLADgJAHQgJAIgIALQgIAMgDASIB6gPIgCgDQgHgVgNgLQgPgLgUAAQgHAAgKACg");
	this.shape_16.setTransform(-187.8,4.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(3,0,0,6.9).p("A+PleMA8eAAAQBsAABMBMQBMBNAABrIAAC1QAABrhMBNQhMBMhsAAMg8eAAAQhrAAhNhMQhMhNAAhrIAAi1QAAhrBMhNQBNhMBrAAg");
	this.shape_17.setTransform(10.8,0.2,0.999,0.999);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("A+PFfQhrAAhMhMQhNhMABhsIAAi1QgBhrBNhNQBMhMBrAAMA8fAAAQBrAABMBMQBMBNAABrIAAC1QAABshMBMQhMBMhrAAg");
	this.shape_18.setTransform(10.8,0.2,0.999,0.999);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-210.2,-36.4,442,73.2);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnhHiQjIjHAAkbQAAkaDIjHQDHjIEaAAQEbAADHDIQDIDHAAEaQAAEbjIDHQjHDIkbAAQkaAAjHjIg");
	mask.setTransform(44.3,42.5);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AA5AhQgQAMgWgJQgKgFgFgCQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANg");
	this.shape.setTransform(45.8,53.5,0.325,0.325);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0BD7E").s().p("AATAkIgPgHQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANQgJAHgLAAQgIAAgKgEg");
	this.shape_1.setTransform(45.8,53.5,0.325,0.325);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AILAMIBIAmQgbgbgFgFQhVhdiAAvQgWAMgZANQBhgrB7A6gAoKAGIhIAmQAbgbAFgEQBVhdCAAvQAWALAZAOQhhgrh7A5g");
	this.shape_2.setTransform(45.3,40.1,0.325,0.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#622F0D").s().p("AILAMQh7g6hhArIAvgZQCAgvBVBdIAgAggAoyANQBVhdCAAvIAvAZQhhgrh7A5IhIAmIAggfg");
	this.shape_3.setTransform(45.3,40.1,0.325,0.325);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABigSIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQhoBDhohDQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIQAAABgBACQBiA/BihAQgBgCABgDg");
	this.shape_4.setTransform(46.8,58.8,0.928,0.928);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#772326").s().p("AhogGQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIIgBADQBiA/BihAIAAgFIAAgBIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQg0Ahg0AAQg0AAg0ghg");
	this.shape_5.setTransform(46.8,58.8,0.928,0.928);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F9FEF5").s().p("AgFAGQgCgCgBgEQABgCACgCQACgDADAAQADAAACADQADACAAACQAAAEgDACQgCACgDAAQgDAAgCgCg");
	this.shape_6.setTransform(61.4,43);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#090A0B").s().p("AAAAWIgFgBQgFgBgFgEQgGgGAAgKQAAgIAGgHQAFgEAFgCIAFgBIACABQAHAAAHAGQAGAHAAAIQAAAKgGAGQgHAFgHABIgCAAgAACgNQgCACAAAEQAAAEACABQADACACAAQAEAAACgCQAEgBAAgEQAAgEgEgCQgCgCgEAAQgCAAgDACg");
	this.shape_7.setTransform(60.5,43.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F9FEF7").s().p("AAMAdIgMgBIgHgCQgRgDgJAAQgKAAgGgDQgGgCASgWQAPgRAPgFIAIgCIALAAIAIACQAJABAHAFQARAKgCANQgCAOgNAHQgHAEgJABIgIAAgAAMgVQgFACgFAEQgFAHAAAIQAAAKAFAGQAFAEAFABIAGABIACAAQAHgBAHgFQAGgGAAgKQAAgIgGgHQgHgGgHAAIgCgBIgGABg");
	this.shape_8.setTransform(58.7,43.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F9FEF5").s().p("AgFAFQgCgBAAgEQAAgCACgDQADgCACAAQAEAAACACQACADAAACQAAAEgCABQgCADgEAAQgCAAgDgDg");
	this.shape_9.setTransform(30.3,43.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#090A0B").s().p("AAAAWIgBAAQgJgBgFgFQgHgGAAgKQAAgIAHgHQAFgGAJAAIABgBIAGABQAFACAFAEQAHAHAAAIQAAAKgHAGQgFAEgFACIgGAAgAgNgNQgCADAAADQAAAEACABQACACAEAAQADAAADgCQABgBAAgEQAAgDgBgDQgDgCgDAAQgEAAgCACg");
	this.shape_10.setTransform(31.1,43.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F9FEF7").s().p("AgTAdQgJgBgHgEQgOgHgCgOQAAgNAPgKQAIgFAJgBIAJgCIAKAAIAIADQAPAEAPARQASAWgFACQgHADgKAAQgJAAgRADIgIACIgKABIgJAAgAgTgVQgIAAgFAGQgHAHAAAIQAAAKAHAGQAFAFAIABIACAAIAHAAQAFgCAFgEQAFgGAAgKQAAgIgFgHQgFgEgFgCIgHgBIgCABg");
	this.shape_11.setTransform(32.8,43.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AkUioQgSgqgIguQgHgrABguQABg1ALgsQAPhCAkguQAOgTATgRQADgDADgDQBJg+BugIQAagCAcABQATABASACQB+AMBOBGQANALALANQAKANAJAOQASAdAKAkQADAKACALQAKAvgCA5QgCBkglBOQgYAyglApQhABIhVAXQgqAMguAAQgQAAgNgBQgJgBgIgBQAAAAgCAAQgYgDgVgHAGrmzQAGACAFADQAFACAFACQAIADAHAEQAKAGAPALQAHAFAIAIQABABACABQAFAEAEAFQAQAWgKAmQAlgzAKgpQAIgggHgbQgBAAAAAAQgRg9g8ggQArAIAgAhQACAAAAABQAFAFAFAFQgBgSgGgXQgCgFgBgEQgHgSgKgPQgPgTgRgLQgQgNgbgCQgPgCgcACQgDAAgCAAQgZACgOAEQgVAGgNAMQgHAGgMARQgGAKgHAGQACABACABQAhAXATAwQAJAWAEAgQAOAFAVAIgAkUioQADg3AIgsQAEgVAFgSQACgHACgGQAGgUAIgSQATgBAYAEQAJACANADQADgXAFgVQAFgUAFgTQAJgfAQgcQAAAMAAAMQABA5ARA0QABAEABADQAnACBAAAQAFAAAEAAQgJgsAGgwQABgHABgGQACgIABgGQAEgRAHgQQABANADAOQAHAwAZBMQATAAAUAAQBAgBBRgDQgCgLgDgKQgRg7gbgtQAJAKAKAKQAoAwAVA4QABAAABAAQAIABAGAEQA3AbgUCgAkmpFQgBgBgCgCQgGgGgIgKQgLgNgIgGQgHgEgHgEQgOgIgRgDQgbgEghAEQgUAEgRAFQgFACgFACQgFACgEACQgYANgYAZQgYAagOAlQA4gjAngPQALgHAKACQgMAGgJAFQgaAOgNAUQgPAWgBAiQgCAmARArQAJAWAUAiQgDgXAIgTQACgFAEgHQAFgIAKgNQAFgIAEgGQAGgLADgKQABgEACgDQAEgKAGAAQAFAAADAEQABAAAAACQADADABAFQADAKACAMQAAAFABAHQADgGADgGQAEgIAFgGQAKgMAIgCQAKgEAOAHQAIADAIABQAIABAJACQgGgKgDgHQgIgVACgRQAEgUAagfQAFgIAGgFQAEgFAEgDQACgCACgBIABgBIgBAAQgCgCgCgBQAAgBgBAAgAhKBCIhtgsIAAACIAABPIAABlIAIgHIALgIAhKBCIAAgsQhRgYg+hHQgmgsgVgzADVBnIB1AAQBHAAAABHIAACtIjtC3IgkAhIgnARQjwEBjWnqIAAitQAAhHBIAAIBuAAAhIBwIgxApIgrAkADGCwQgKAugqAjQg1AuhOAAQhNAAg3guQgkgegLgmADVBnIAAhRIgfAQIhLApIgQAIIAPANIANAKIAaAWIA1AsABrAXIAAAvIAAAJAhIBwIBRCcIBhioADVBnIAAACIAABTIgPgMAhKBCIAmAPIgkAf");
	this.shape_12.setTransform(44.5,66.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E78A00").s().p("AhdgDQAzgZAqgmIgigFQAXgNAIgZIgvgDQAPgJAKgPIgYgCIAVgmQgJgIgMABIAGgaIgagBIAAgCIB0AAQBHAAAABHIAACsIjrC2QAKjSAOgGg");
	this.shape_13.setTransform(72.8,98.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#9DACB6").s().p("AiFAnQgkgegLgkIArglQAPAbANAdQAKgiAOghQALAjgCAnIAEgNQANAiACAoQABgGAGgCQAHgCAGADQAEACAGAFIAKAHQAFADAHgBQAFAAACgEQgBAAAAABQAAAAABABQAAAAAAABQAAAAAAABQACADAEAAQAEACAIgEQAWgJAPgSQAQgTAEgXIAGAfIANgwQAJAAAIgHQAGgIAAgJQADAGACAFQgBgcAGgbIA1ArQgKAtgrAjQg1AuhMAAQhOAAg3gug");
	this.shape_14.setTransform(46.1,88.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BAC1C6").s().p("AAMBfQgDgBgCgDQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgCAEgEABQgHABgGgDIgKgHQgFgGgFgCQgGgDgGACQgHADAAAGQgCgpgOgjIgDANQACglgLgkQgPAhgKAiQgMgdgPgaIAwgpIBSCbIBginIANAKIAbAWQgHAcACAcQgCgGgEgFQABAJgGAIQgIAGgJAAIgOAxIgFggQgEAXgQATQgQASgVAKQgFACgEAAIgEAAg");
	this.shape_15.setTransform(45.7,86);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF9900").s().p("AlDglIAAitQAAhIBJAAIBuAAIAABlIAIgGIAKgIQALAlAkAeQA3AuBNAAQBNAAA2guQAqgjAKguIAPAMIAAhSIAaABIgGAaQAMgBAJAIIgVAlIAYADQgKAPgPAJIAvADQgIAZgXANIAiAFQgqAlgzAaQgOAHgKDRIglAhIgnARQhTBZhOAAQiZAAiMlAg");
	this.shape_16.setTransform(40.3,105);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#C5D7E2").s().p("AjFgJIAAhPIAFAIQAHALAJAHIAGAFIAFAGIgBAJQAAACAVANQAVAMgXALQgXAKAEAHQAFAGAIABQgVAegPAiIgIAHgAC3A/Ig0gsIgbgVIAFgCQAJgDANAAIAVgCIAHAAIAGgDQADgEAAgFIgBgKQgEgVAEgWIAfgQIAABRIAAACIAABSg");
	this.shape_17.setTransform(45.9,77.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#DBE3E8").s().p("AiKAYQgIgBgFgHQgEgGAXgLQAXgLgVgMQgVgMAAgCIABgJIgFgGIgGgFQgJgHgHgMIgFgIIAAgCIBtAsIAmAQIglAdIgwApIgsAlIgKAIQAPgiAVgegABqgJIgPgMIAQgJIBLgoQgEAVAEAVIABALQAAAFgDADIgGADIgHABIgVABQgNABgJACIgFABg");
	this.shape_18.setTransform(44.4,77.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#BB2E8C").s().p("AgaBCIgKgBQAIgYAMgUIAJgQQAEgJABgHQgCgHgEgKQgHgMgBgDQgBgGAAgGIAIgIIADgCIABgBQAIAHAOAHIAUAIQgkAugOBBIgNgBg");
	this.shape_19.setTransform(16.2,15.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F3CFA2").s().p("AhHCnIAkgfIgmgPIAAgsQhRgZg+hGQgmgsgVgzQADg3AIgsQAagKAIgLQANgTAagNQAZgNAOgRQAJgKACgJQAEANALAMQATAUAbAGQAYAFAcgEQATgDAhgKIABgvQACAJAFAJQALAQARAJQAPAIAVACQALACAaAAIBOABIAVgBQAMgCAGgGQAJgHgCgLQATAbAAArQAAAngRA7QgRA8gSAfQgaAvgoARQgWAJgwACQgxACgWAKQgZALgUAWIAAABIACAAQBCAXBEAPIAAAJIgQAIIAPANIhhCogAgcBXQgXgDgWgHQAWAHAXADgAEBklQgFgIgIgHQALABACAMIAAACIAAAAg");
	this.shape_20.setTransform(44.4,61);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#ECC391").s().p("Ag3D4IARACQAOABAQABQAtAAAqgNIAAAvQhFgOhBgYgAgmD6IgRgCIgCAAIAAgBQAUgWAagLQAVgKAxgCQAwgCAWgJQAogRAagvQASggARg7QARg7AAgmQAAgsgTgbIAAgBQgCgNgLgBQAIAHAFAIQABALgIAHQgGAGgMADIgVAAIhOAAQgagBgLgBQgVgDgPgIQgSgJgKgPQgEgKgCgJIgCAvQghALgTACQgdAEgXgFQgbgGgTgTQgLgMgEgNQgDAIgIAKQgOASgaAMQgZAOgNASQgIAMgaAJQAEgVAFgSIAEgNQAGgUAIgSQATgBAYAEIAWAFQADgWAFgWIAKgnQAJgfAQgcIAAAYQABA5ARA1IACAGQAnACBAAAIAKAAQgKgsAGgvIADgOIADgOQAEgQAHgRQABANADAOQAHAwAYBNIAnAAICRgFIgGgVQgRg7gagtIATAVQAoAvAVA4IABAAQAIABAHAEQA3AbgUCgQgYAxglAqQhBBHhUAYQgqANgtAAQgQgBgOgBgAg3D4IAAAAg");
	this.shape_21.setTransform(47.3,44.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC3399").s().p("AlJA7IgJgRQgIgVACgRQAEgTAagfQAFgIAGgFQAAAFACAGQABADAGANQAFAKABAGQAAAIgEAJIgJAQQgMATgJAYIgHgBgAFVAnQgLgkgRgcQgJgOgLgNIAMgHQABAFACAFQADAEAGAEIAKAIQALAJADARQAEALAAAUIAAAQIgEgBg");
	this.shape_22.setTransform(44.7,15.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#BD268B").s().p("AAQA2IgRgHIgBgRQAAgUgDgKQgEgRgKgJIgLgIQgGgFgCgEQgDgEAAgFIAJgGQAEgBACgEIAEADQAgAXATAvQAJAWAEAgIgagKg");
	this.shape_23.setTransform(79.4,15.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#583222").s().p("AgVCeQhAAAgngCIgCgGQgRg1gBg5IAAgXQgQAbgJAfIgKAoIABgMQACgKgDgFQgDgFgGgBQgGgBgFADQgHACgJAMQASggAEglQgEAKgJAEIANgnQgMAIgIALQAPgVACgXIggAlQANgeAHgfIgpAaQAYgiAPgmIgKADQBJg9BugIIAyAMQgKAFgHAJQgHAIgFAKIAQAGQAIAEAFAFQAHAHAAAJQABAJgGAGIgIAFQgFADgCAEQgCAGAFAGQAEAGAIADIAOAEQAIAEADAFQg0AdgbAOQAOAMAQAFIgDAOIgDAOQgGAvAKAsIgKAAgABWCJIgTgDQAMgMAEgOQAEgQgKgIQgFgEgRgFQgOgEgDgHQgDgGADgHQACgHAGgFIAJgLQAGgFACgGQACgHgEgHQgEgGgHABIAdgaQgEgFgFgCQgGgCgFABQAFgFAEgIQAEgIABgIIgdgLIAJgJQAEgFAAgGQACgGgFgFQgFgGgFADQAWgPANgXQgNgDgMgFQB+ANBOBFQAMALALANQALANAJAOQgBANADAPQABAMAFAVIgcggIANA0IgVgOQADAggBAZQgJgRgOgNQABAIAFAMIAIAWQAIAZgGASIAAABQgHgDgIgBIgBAAQgVg5gogvIgTgVQAaAtARA7IAGAWIiRAEIAKgVgAHsB5IgDgDQAJABAIACIgGAIIgIgIgAHBBZIgQgIIA2AAQgJAMgFANQgOgMgKgFgAmwBPIgFgVQgBgGgDgDIgBgCQgDgEgFAAQgGAAgEALIgaAIQAogZAmgkIguABIAngbIgRgDQAKgWAQgQQgpgHgggMQAagIAVgNIgpgHIATgMQgQAEgQABQARgGAUgDQAhgFAbAFQARACAOAIIAOAJQAIAGALANIAOAPIADADIABABIAEADIABABIgBABIgEACIgIAIQgGAGgFAHQgaAfgEAUQgCARAIAVIAJARIgRgEQgIgBgIgDQgOgGgKADQgIADgKALIgJAOIgGAMIgBgMgAnPA9QgDAJgGALIghABgAGdBJIgjgNQgFgggIgWQgUgvgggXIgEgDQAGgGAHgKQAMgRAHgGQANgLAVgGQAOgEAZgCIAEAAIgLAGQAmAQApgGQgTALgOARQgPARgIAUIATAKIghApQAVAIAUgDIg9ArQAcAHAegDIgeAWIgLgEg");
	this.shape_24.setTransform(45.9,15.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#462517").s().p("AEaBBIABgCQAGgRgJgaIgIgUQgEgMgCgJQAPANAIAQQABgYgDgfIAVAOIgNg0IAcAgQgFgWgBgLQgCgQAAgNQARAdALAkIAFAVQAKAugCA5QgCBkglBOQAUigg3gbgAkuCfQgHgrABguQABg1ALgrQAPhCAkguQAPgTASgRIAGgGIAKgDQgPAlgYAiIAqgZQgIAegMAgIAfgnQgCAYgPAVQAIgLAMgIIgNAoQAJgFAEgJQgEAlgSAeQAKgKAGgDQAGgCAFAAQAGACADAEQADAEgCALIgBALQgEAVgEAXIgVgFQgYgEgUABQgIASgGAUIgDANQgGASgEAVQgIAsgDA3QgSgqgIgugAIDAlIAFgIQgIgDgJAAIgPgNQAEgNAJgKIg2AAIgJgEIAegWQgeACgbgHIA9grQgVADgVgHIAigrIgTgJQAHgVAPgQQAPgRATgLQgpAFgngPIALgHQAdgCAPACQAaACARANQARALAPATQAKAPAHASIADAJQAGAXABASIgKgKIgCgBQggghgrgIQA8AgARA9IABAAQAHAbgIAfQgKApgmAzQAMgmgRgWgAoKAWQgQgqABgmQABgiAPgWQANgUAagOIAVgLQgKgCgLAHQgnAPg4AjQAPglAXgaQAYgZAZgNIAIgEIALgEQAPAAAQgFIgTANIAqAGQgWAOgZAIQAfALAqAHQgQARgLAWIASACIgoAdIAvgBQgnAjgnAZIAZgIIgDAHIgqAWIAhgBIgJANIgPAVIgGAMQgHATACAXQgTgigKgWgAAzBBQgZhLgHgwQgDgOgBgNQgGAQgFARQgPgGgOgMQAbgOA0gdQgCgGgJgDIgOgFQgHgCgFgGQgGgHADgGQACgDAGgEIAHgFQAGgGgBgJQAAgJgHgGQgFgGgIgEIgQgFQAEgKAHgJQAJgIAJgFIgygNQAZgCAdABQATABASACQAMAFANADQgNAXgWAOQAFgCAFAFQAFAFgCAHQABAFgFAGIgIAJIAcALQAAAIgEAHQgFAIgFAFQAFAAAGACQAFACAFAEIgdAbQAGgCAFAIQADAGgBAHQgCAGgHAGIgJAKQgGAGgCAGQgDAHADAGQADAIAOAEQARAEAFAEQALAIgFAQQgEAOgMALIATADIgKAVIgnAAg");
	this.shape_25.setTransform(44.5,24.9);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFCC00").ss(5,1,1).p("AH0H0QDPjPAAklQAAkkjPjPQjPjPklAAQkkAAjPDPQjPDPAAEkQAAElDPDPQDPDPEkAAQElAADPjPg");
	this.shape_26.setTransform(44.3,42.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAALDQkkAAjPjPQjPjPAAklQAAkkDPjPQDPjPEkAAQElAADPDPQDPDPAAEkQAAEljPDPQjPDPklAAIAAAAg");
	this.shape_27.setTransform(44.3,42.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26}]}).wait(1));

	// Layer_5
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(255,255,255,0.02)").s().p("ArvLzIAA3lIXfAAIAAXlg");
	this.shape_28.setTransform(44.5,41.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_28).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy13, new cjs.Rectangle(-30.6,-34.1,150.4,151.1), null);


(lib.Symbol1copy11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("AhEAQQA8AMA7gnQgBgCABgCIAAgBIABgEQAEgGAHADQAIADgCAHQgBAEgCACQgFADgEgCQg+Apg/gN");
	this.shape.setTransform(50.8,58.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("ABQg9IAYAMQgJgJgCgBQgbgfgqAPQgHAEgIAFQAfgOAoATgAhnA8QACgCAEgCQAGgCAJABQAIABAEACQAGADACAGQACAHgFAEQgFAEgHgDQgEgBgBgBQgDgBgCAAQgFABgDAAQgEABgDgDQAAgBgBAA");
	this.shape_1.setTransform(54.3,46.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AkjpkQASgBATABQATABASACQB/AMBNBGQANALAKANQAKANAJAOQASAdAKAkQADAKADALQAJAvgCA5QgCBkglBOQgYAygkApQhABHhVAYQgpAMgvAAQgNAAgMgBACNl8QAGACAGADQAEACAFACQAIADAIAEQAJAGAPALQAHAFAIAIQABABACABQAFAEAEAFQAQAWgKAmQAlgzAKgpQAIgggHgbQgBAAAAAAQgRg9g8ggQArAIAhAhQABAAAAABQAFAFAFAFQgBgSgGgXQgCgFgBgEQgGgSgLgPQgPgTgRgLQgQgNgbgCQgPgCgcACQgDAAgCAAQgZACgOAEQgVAGgNAMQgGAGgNARQgGAKgHAGQACABACABQAhAXATAwQAJAWAEAgQAOAFAVAIgAkjkmQAFAAADAAQgKgsAGgwQACgHACgGQABgIABgGQAEgRAHgQQABANADAOQAHAwAaBMQASAAAVAAQA/gBBRgDQgCgLgDgKQgRg7gbgtQAKAKAKAKQAoAwAUA4QABAAABAAQAIABAGAEQA2AbgUCgAhICeIAAhRIgfAQIhLApIgQAIIAPANIANAKIAbAWIA0AsQgKAugqAjQg1AuhOAAQgKAAgLgBAiyBOIAAAvIAAAJAkjEnIAPAcIBhioAhICeIAAACIAABTIgPgMAhICeIB0AAQBHAAAABHIAACpIAAAEIjrC3IggAd");
	this.shape_2.setTransform(73.1,61);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E78A00").s().p("AhfAlQAzgaAqgkIgjgFQAYgNAIgZIgvgDQAPgJAJgPIgXgDIAVglQgJgIgMABIAFgaIgZgBIAAgDIBzAAQBIAAAABIIAACoQhqBLh9AeQAIiCAMgGg");
	this.shape_3.setTransform(73,94);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF9900").s().p("AiPC1IAAilIAVAAQBNAAA1gsQAqgkAKgtIAPALIAAhSIAZABIgFAaQAMgBAJAIIgVAmIAXACQgJAPgPAJIAvADQgIAZgYANIAjAFQgqAlgzAZQgMAGgICCQhQAThaAAIgEAAg");
	this.shape_4.setTransform(58.3,95.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#9DACB6").s().p("AhlBVIAAgUIAEACQAGADAGgBQAGAAACgEQAAAAAAABQAAAAAAABQAAAAAAABQAAAAABABQACADADAAQAFACAIgEQAVgJAPgSQAQgTAFgXIAEAfIAOgwQAJAAAHgHQAHgIgBgJQAEAGABAFQgBgcAGgbIA1ArQgKAtgqAjQg1AuhNAAIgVAAg");
	this.shape_5.setTransform(54.1,88.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BAC1C6").s().p("AgtBfQgDgBgCgDQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgCAEgFABQgHABgGgDIgDgDIAAgqIAOAcIBginIANAKIAbAWQgHAcABAcQgBgGgEgFQABAJgGAIQgIAGgJAAIgOAxIgFggQgEAXgQATQgPASgVAKQgFACgEAAIgEAAg");
	this.shape_6.setTransform(51.5,86);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#DBE3E8").s().p("AgeAaIgPgNIAQgIIBKgoQgEAWAEAUIABAKQAAAFgDAEIgGADIgHAAIgVACQgMAAgJADIgFACg");
	this.shape_7.setTransform(58.2,73.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F0BD7E").s().p("AAFAMIgFgDIgEgBIgIACQgEAAgDgDIgBgBIAAgNQADgCADgBQAGgDAIABQAIACAEACQAGADACAFQACAHgFAEQgDACgDAAIgGgBg");
	this.shape_8.setTransform(46,53.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#772326").s().p("AhEAWIAAgGQA8AMA7gnQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAIAAgBIABgEQAEgGAHADQAIADgCAHQgBAEgCACQgFADgEgCQgxAfgvAAQgPAAgOgDg");
	this.shape_9.setTransform(50.8,58.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C5D7E2").s().p("AAgBHIgzgsIgbgWIAFgCQAJgDANAAIAUgBIAHAAIAGgDQADgEAAgFIgBgKQgEgVAEgWIAfgQIAABRIAAABIAABTg");
	this.shape_10.setTransform(61.1,77);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F9FEF5").s().p("AgFAGQgCgCgBgEQABgCACgCQACgDADAAQADAAACADQADACAAACQAAAEgDACQgCACgDAAQgDAAgCgCg");
	this.shape_11.setTransform(61.4,43);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#622F0D").s().p("AAYADQgngSgfAOIAPgIQApgPAbAdIAKAKg");
	this.shape_12.setTransform(59.9,40.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#090A0B").s().p("AAAAWIgFgBQgFgBgFgEQgGgGAAgKQAAgIAGgHQAFgEAFgCIAFgBIACABQAHAAAHAGQAGAHAAAIQAAAKgGAGQgHAFgHABIgCAAgAACgNQgCACAAAEQAAAEACABQADACACAAQAEAAACgCQAEgBAAgEQAAgEgEgCQgCgCgEAAQgCAAgDACg");
	this.shape_13.setTransform(60.5,43.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F9FEF7").s().p("AAMAdIgMgBIgHgCQgRgDgJAAQgKAAgGgDQgGgCASgWQAPgRAPgFIAIgCIALAAIAIACQAJABAHAFQARAKgCANQgCAOgNAHQgHAEgJABIgIAAgAAMgVQgFACgFAEQgFAHAAAIQAAAKAFAGQAFAEAFABIAGABIACAAQAHgBAHgFQAGgGAAgKQAAgIgGgHQgHgGgHAAIgCgBIgGABg");
	this.shape_14.setTransform(58.7,43.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AiLEnIAAjJQA3ATA5AMIAAAJIgQAIIAPANIhgCogAiLABQA/AMA/goQAEACAFgEQACgCABgEQABgHgHgDQgHgDgEAGIgBAEIAAABQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQg8Amg8gLIAAg/IABABQADADAEAAIAHgCIAFABIAFADQAHADAGgEQAEgEgCgIQgCgFgGgDQgDgCgIgCQgJgBgGADQgEABgCACIAAi5IAcgIIABgvQACAJAFAJQALAQARAJQAPAIAVACQAMACAaAAIBMABIAWgBQALgCAHgGQAIgHgCgLQATAbAAArQAAAngRA7QgRA8gRAfQgbAvgoARQgWAJgvACQgwACgXAKQgLAFgKAHgAAAjFQgOAFgPARQgSAXAGACQAGACAKABQAJAAAQADIAHABIANACIAIAAQAJgBAHgEQANgHACgOQACgOgRgKQgHgFgJgCIgIgBIgLAAIgJACgABDjAIgKgKQgcgegoAPIgQAIQAfgOAoATIAXAMIAAAAgAB5klQgFgIgHgHQAKABACAMIAAACIAAAAg");
	this.shape_15.setTransform(57.9,61);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#ECC391").s().p("AijD/IAAgEIAAgZQAKgHAMgFQAWgKAxgCQAwgCAWgJQAngRAagvQASggARg7QARg7AAgmQAAgsgTgbIAAgBQgCgNgLgBQAIAHAFAIQABALgIAHQgGAGgMADIgVAAIhNAAQgagBgLgBQgVgDgPgIQgSgJgKgPQgFgKgCgJIgCAvIgcAIIAAgbIAIAAQgKgsAGgvIADgOIADgOQAEgQAHgRQABANADAOQAHAwAZBNIAnAAICQgFIgGgVQgRg7gagtIATAVQAoAvAVA4IABAAQAIABAHAEQA3AbgUCgQgYAxglAqQhBBHhTAYQgqANguAAIgZgBIAZABQAuAAAqgNIAAAvQg6gMg3gTg");
	this.shape_16.setTransform(60.3,44.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CC3399").s().p("AAWAwQgLgjgQgdQgJgOgLgNIAMgGQABAFACAFQADAEAFAEIAKAIQAKAJAEAQQADALAAAVIABAQIgEgCg");
	this.shape_17.setTransform(76.6,14.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#BD268B").s().p("AAQA2IgRgHIgBgRQAAgUgDgKQgEgRgKgJIgLgIQgGgFgCgEQgDgEAAgFIAJgGQAEgBACgEIAEADQAgAXATAvQAJAWAEAgIgagKg");
	this.shape_18.setTransform(79.4,15.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#583222").s().p("AkGCdIAAh6IAJADIgCAPIgDANQgHAvAKAsIgHAAgAicCIIgTgDQALgMAFgNQAEgRgLgIQgEgEgRgFQgPgDgCgIQgDgGADgHQACgGAGgGIAJgLQAGgFACgGQACgHgEgHQgFgGgGACIAdgbQgEgEgGgCQgGgCgFAAQAGgFAEgIQAEgIABgHIgdgMIAIgJQAFgFAAgGQABgGgFgFQgEgFgGACQAWgOANgYQgMgCgNgGQB/ANBMBGQANAKALANQALANAJAOQgBANACAPQACAMAFAVIgcgfIAMAzIgUgNQADAfgCAZQgIgRgPgNQACAJAEAMIAIAVQAJAZgGASIgBABQgGgDgIgBIgCAAQgTg5gogvIgTgVQAaAtARA8IAFAVIiQAEIAKgVgAD4B4IgCgDQAIABAJACIgGAIIgJgIgADOBYIgQgHIA2AAQgJALgFANQgPgLgJgGgACpBIIgjgNQgEgggJgWQgTgvghgXIgDgDQAGgGAHgJQAMgSAGgGQAOgLAUgGQAPgEAYgCIAFAAIgLAGQAmAQApgGQgTAMgOAQQgQARgHAUIATAKIghApQAUAIAVgDIg9ArQAcAHAegDIgeAWIgMgEgAkGiZIAhAIQgKAEgIAJQgIAIgEAKIAQAGQAJAEAFAGQAHAGAAAJQABAJgHAGIgHAFQgGAEgBADQgDAGAGAHQAEAFAHADIAPAEQAIAEADAFIg8Agg");
	this.shape_19.setTransform(70.2,15.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#462517").s().p("AgDBBIABgCQAFgRgIgaIgIgUQgEgMgCgJQAPANAHAQQABgYgDgfIAVAOIgNg0IAcAgQgFgWgBgLQgCgQAAgNQARAdALAkIAFAVQAKAugCA5QgCBkglBOQAUigg2gbgADlAlIAFgIQgIgDgJAAIgPgNQAEgNAJgKIg2AAIgJgEIAegWQgeACgbgHIA9grQgVADgVgHIAigrIgTgJQAHgVAPgQQAPgRATgLQgpAFgngPIALgHQAdgCAPACQAaACARANQARALAPATQAKAPAHASIADAJQAGAXABASIgKgKIgCgBQggghgrgIQA8AgARA9IABAAQAHAbgIAfQgKApgmAzQAMgmgRgWgAjqBBQgZhLgHgwQgDgOgBgNQgGAQgFARIgJgEIAAgYIA7ghQgCgGgJgDIgOgFQgHgCgFgGQgGgHADgGQACgDAGgEIAHgFQAGgGgBgJQAAgJgHgGQgFgGgIgEIgRgFQAFgKAHgJQAJgIAJgFIgggIIAAgGQARgBATABQATABASACQAMAFANADQgNAXgWAOQAFgCAFAFQAFAFgCAHQABAFgFAGIgIAJIAcALQAAAIgEAHQgFAIgFAFQAFAAAGACQAFACAFAEIgdAbQAGgCAFAIQADAGgBAHQgCAGgHAGIgJAKQgGAGgCAGQgDAHADAGQADAIAOAEQARAEAFAEQALAIgFAQQgEAOgMALIATADIgKAVIgnAAg");
	this.shape_20.setTransform(73.1,24.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy11, new cjs.Rectangle(42.9,-1.3,60.3,124.7), null);


(lib.Symbol1copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnhHiQjIjHAAkbQAAkaDIjHQDHjIEaAAQEbAADHDIQDIDHAAEaQAAEbjIDHQjHDIkbAAQkaAAjHjIg");
	mask.setTransform(44.3,42.5);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AA5AhQgQAMgWgJQgKgFgFgCQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANg");
	this.shape.setTransform(45.8,53.5,0.325,0.325);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0BD7E").s().p("AATAkIgPgHQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANQgJAHgLAAQgIAAgKgEg");
	this.shape_1.setTransform(45.8,53.5,0.325,0.325);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AILAMIBIAmQgbgbgFgFQhVhdiAAvQgWAMgZANQBhgrB7A6gAoKAGIhIAmQAbgbAFgEQBVhdCAAvQAWALAZAOQhhgrh7A5g");
	this.shape_2.setTransform(45.3,40.1,0.325,0.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#622F0D").s().p("AILAMQh7g6hhArIAvgZQCAgvBVBdIAgAggAoyANQBVhdCAAvIAvAZQhhgrh7A5IhIAmIAggfg");
	this.shape_3.setTransform(45.3,40.1,0.325,0.325);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABigSIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQhoBDhohDQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIQAAABgBACQBiA/BihAQgBgCABgDg");
	this.shape_4.setTransform(46.8,58.8,0.928,0.928);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#772326").s().p("AhogGQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIIgBADQBiA/BihAIAAgFIAAgBIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQg0Ahg0AAQg0AAg0ghg");
	this.shape_5.setTransform(46.8,58.8,0.928,0.928);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F9FEF5").s().p("AgFAGQgCgCgBgEQABgCACgCQACgDADAAQADAAACADQADACAAACQAAAEgDACQgCACgDAAQgDAAgCgCg");
	this.shape_6.setTransform(61.4,43);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#090A0B").s().p("AAAAWIgFgBQgFgBgFgEQgGgGAAgKQAAgIAGgHQAFgEAFgCIAFgBIACABQAHAAAHAGQAGAHAAAIQAAAKgGAGQgHAFgHABIgCAAgAACgNQgCACAAAEQAAAEACABQADACACAAQAEAAACgCQAEgBAAgEQAAgEgEgCQgCgCgEAAQgCAAgDACg");
	this.shape_7.setTransform(60.5,43.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F9FEF7").s().p("AAMAdIgMgBIgHgCQgRgDgJAAQgKAAgGgDQgGgCASgWQAPgRAPgFIAIgCIALAAIAIACQAJABAHAFQARAKgCANQgCAOgNAHQgHAEgJABIgIAAgAAMgVQgFACgFAEQgFAHAAAIQAAAKAFAGQAFAEAFABIAGABIACAAQAHgBAHgFQAGgGAAgKQAAgIgGgHQgHgGgHAAIgCgBIgGABg");
	this.shape_8.setTransform(58.7,43.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F9FEF5").s().p("AgFAFQgCgBAAgEQAAgCACgDQADgCACAAQAEAAACACQACADAAACQAAAEgCABQgCADgEAAQgCAAgDgDg");
	this.shape_9.setTransform(30.3,43.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#090A0B").s().p("AAAAWIgBAAQgJgBgFgFQgHgGAAgKQAAgIAHgHQAFgGAJAAIABgBIAGABQAFACAFAEQAHAHAAAIQAAAKgHAGQgFAEgFACIgGAAgAgNgNQgCADAAADQAAAEACABQACACAEAAQADAAADgCQABgBAAgEQAAgDgBgDQgDgCgDAAQgEAAgCACg");
	this.shape_10.setTransform(31.1,43.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F9FEF7").s().p("AgTAdQgJgBgHgEQgOgHgCgOQAAgNAPgKQAIgFAJgBIAJgCIAKAAIAIADQAPAEAPARQASAWgFACQgHADgKAAQgJAAgRADIgIACIgKABIgJAAgAgTgVQgIAAgFAGQgHAHAAAIQAAAKAHAGQAFAFAIABIACAAIAHAAQAFgCAFgEQAFgGAAgKQAAgIgFgHQgFgEgFgCIgHgBIgCABg");
	this.shape_11.setTransform(32.8,43.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AkUioQgSgqgIguQgHgrABguQABg1ALgsQAPhCAkguQAOgTATgRQADgDADgDQBJg+BugIQAagCAcABQATABASACQB+AMBOBGQANALALANQAKANAJAOQASAdAKAkQADAKACALQAKAvgCA5QgCBkglBOQgYAyglApQhABIhVAXQgqAMguAAQgQAAgNgBQgJgBgIgBQAAAAgCAAQgYgDgVgHAGrmzQAGACAFADQAFACAFACQAIADAHAEQAKAGAPALQAHAFAIAIQABABACABQAFAEAEAFQAQAWgKAmQAlgzAKgpQAIgggHgbQgBAAAAAAQgRg9g8ggQArAIAgAhQACAAAAABQAFAFAFAFQgBgSgGgXQgCgFgBgEQgHgSgKgPQgPgTgRgLQgQgNgbgCQgPgCgcACQgDAAgCAAQgZACgOAEQgVAGgNAMQgHAGgMARQgGAKgHAGQACABACABQAhAXATAwQAJAWAEAgQAOAFAVAIgAkUioQADg3AIgsQAEgVAFgSQACgHACgGQAGgUAIgSQATgBAYAEQAJACANADQADgXAFgVQAFgUAFgTQAJgfAQgcQAAAMAAAMQABA5ARA0QABAEABADQAnACBAAAQAFAAAEAAQgJgsAGgwQABgHABgGQACgIABgGQAEgRAHgQQABANADAOQAHAwAZBMQATAAAUAAQBAgBBRgDQgCgLgDgKQgRg7gbgtQAJAKAKAKQAoAwAVA4QABAAABAAQAIABAGAEQA3AbgUCgAkmpFQgBgBgCgCQgGgGgIgKQgLgNgIgGQgHgEgHgEQgOgIgRgDQgbgEghAEQgUAEgRAFQgFACgFACQgFACgEACQgYANgYAZQgYAagOAlQA4gjAngPQALgHAKACQgMAGgJAFQgaAOgNAUQgPAWgBAiQgCAmARArQAJAWAUAiQgDgXAIgTQACgFAEgHQAFgIAKgNQAFgIAEgGQAGgLADgKQABgEACgDQAEgKAGAAQAFAAADAEQABAAAAACQADADABAFQADAKACAMQAAAFABAHQADgGADgGQAEgIAFgGQAKgMAIgCQAKgEAOAHQAIADAIABQAIABAJACQgGgKgDgHQgIgVACgRQAEgUAagfQAFgIAGgFQAEgFAEgDQACgCACgBIABgBIgBAAQgCgCgCgBQAAgBgBAAgAhKBCIhtgsIAAACIAABPIAABlIAIgHIALgIAhKBCIAAgsQhRgYg+hHQgmgsgVgzADVBnIB1AAQBHAAAABHIAACtIjtC3IgkAhIgnARQjwEBjWnqIAAitQAAhHBIAAIBuAAAhIBwIgxApIgrAkADGCwQgKAugqAjQg1AuhOAAQhNAAg3guQgkgegLgmADVBnIAAhRIgfAQIhLApIgQAIIAPANIANAKIAaAWIA1AsABrAXIAAAvIAAAJAhIBwIBRCcIBhioADVBnIAAACIAABTIgPgMAhKBCIAmAPIgkAf");
	this.shape_12.setTransform(44.5,66.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E78A00").s().p("AhdgDQAzgZAqgmIgigFQAXgNAIgZIgvgDQAPgJAKgPIgYgCIAVgmQgJgIgMABIAGgaIgagBIAAgCIB0AAQBHAAAABHIAACsIjrC2QAKjSAOgGg");
	this.shape_13.setTransform(72.8,98.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#9DACB6").s().p("AiFAnQgkgegLgkIArglQAPAbANAdQAKgiAOghQALAjgCAnIAEgNQANAiACAoQABgGAGgCQAHgCAGADQAEACAGAFIAKAHQAFADAHgBQAFAAACgEQgBAAAAABQAAAAABABQAAAAAAABQAAAAAAABQACADAEAAQAEACAIgEQAWgJAPgSQAQgTAEgXIAGAfIANgwQAJAAAIgHQAGgIAAgJQADAGACAFQgBgcAGgbIA1ArQgKAtgrAjQg1AuhMAAQhOAAg3gug");
	this.shape_14.setTransform(46.1,88.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BAC1C6").s().p("AAMBfQgDgBgCgDQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgCAEgEABQgHABgGgDIgKgHQgFgGgFgCQgGgDgGACQgHADAAAGQgCgpgOgjIgDANQACglgLgkQgPAhgKAiQgMgdgPgaIAwgpIBSCbIBginIANAKIAbAWQgHAcACAcQgCgGgEgFQABAJgGAIQgIAGgJAAIgOAxIgFggQgEAXgQATQgQASgVAKQgFACgEAAIgEAAg");
	this.shape_15.setTransform(45.7,86);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF9900").s().p("AlDglIAAitQAAhIBJAAIBuAAIAABlIAIgGIAKgIQALAlAkAeQA3AuBNAAQBNAAA2guQAqgjAKguIAPAMIAAhSIAaABIgGAaQAMgBAJAIIgVAlIAYADQgKAPgPAJIAvADQgIAZgXANIAiAFQgqAlgzAaQgOAHgKDRIglAhIgnARQhTBZhOAAQiZAAiMlAg");
	this.shape_16.setTransform(40.3,105);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#C5D7E2").s().p("AjFgJIAAhPIAFAIQAHALAJAHIAGAFIAFAGIgBAJQAAACAVANQAVAMgXALQgXAKAEAHQAFAGAIABQgVAegPAiIgIAHgAC3A/Ig0gsIgbgVIAFgCQAJgDANAAIAVgCIAHAAIAGgDQADgEAAgFIgBgKQgEgVAEgWIAfgQIAABRIAAACIAABSg");
	this.shape_17.setTransform(45.9,77.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#DBE3E8").s().p("AiKAYQgIgBgFgHQgEgGAXgLQAXgLgVgMQgVgMAAgCIABgJIgFgGIgGgFQgJgHgHgMIgFgIIAAgCIBtAsIAmAQIglAdIgwApIgsAlIgKAIQAPgiAVgegABqgJIgPgMIAQgJIBLgoQgEAVAEAVIABALQAAAFgDADIgGADIgHABIgVABQgNABgJACIgFABg");
	this.shape_18.setTransform(44.4,77.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#BB2E8C").s().p("AgaBCIgKgBQAIgYAMgUIAJgQQAEgJABgHQgCgHgEgKQgHgMgBgDQgBgGAAgGIAIgIIADgCIABgBQAIAHAOAHIAUAIQgkAugOBBIgNgBg");
	this.shape_19.setTransform(16.2,15.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F3CFA2").s().p("AhHCnIAkgfIgmgPIAAgsQhRgZg+hGQgmgsgVgzQADg3AIgsQAagKAIgLQANgTAagNQAZgNAOgRQAJgKACgJQAEANALAMQATAUAbAGQAYAFAcgEQATgDAhgKIABgvQACAJAFAJQALAQARAJQAPAIAVACQALACAaAAIBOABIAVgBQAMgCAGgGQAJgHgCgLQATAbAAArQAAAngRA7QgRA8gSAfQgaAvgoARQgWAJgwACQgxACgWAKQgZALgUAWIAAABIACAAQBCAXBEAPIAAAJIgQAIIAPANIhhCogAgcBXQgXgDgWgHQAWAHAXADgAEBklQgFgIgIgHQALABACAMIAAACIAAAAg");
	this.shape_20.setTransform(44.4,61);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#ECC391").s().p("Ag3D4IARACQAOABAQABQAtAAAqgNIAAAvQhFgOhBgYgAgmD6IgRgCIgCAAIAAgBQAUgWAagLQAVgKAxgCQAwgCAWgJQAogRAagvQASggARg7QARg7AAgmQAAgsgTgbIAAgBQgCgNgLgBQAIAHAFAIQABALgIAHQgGAGgMADIgVAAIhOAAQgagBgLgBQgVgDgPgIQgSgJgKgPQgEgKgCgJIgCAvQghALgTACQgdAEgXgFQgbgGgTgTQgLgMgEgNQgDAIgIAKQgOASgaAMQgZAOgNASQgIAMgaAJQAEgVAFgSIAEgNQAGgUAIgSQATgBAYAEIAWAFQADgWAFgWIAKgnQAJgfAQgcIAAAYQABA5ARA1IACAGQAnACBAAAIAKAAQgKgsAGgvIADgOIADgOQAEgQAHgRQABANADAOQAHAwAYBNIAnAAICRgFIgGgVQgRg7gagtIATAVQAoAvAVA4IABAAQAIABAHAEQA3AbgUCgQgYAxglAqQhBBHhUAYQgqANgtAAQgQgBgOgBgAg3D4IAAAAg");
	this.shape_21.setTransform(47.3,44.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC3399").s().p("AlJA7IgJgRQgIgVACgRQAEgTAagfQAFgIAGgFQAAAFACAGQABADAGANQAFAKABAGQAAAIgEAJIgJAQQgMATgJAYIgHgBgAFVAnQgLgkgRgcQgJgOgLgNIAMgHQABAFACAFQADAEAGAEIAKAIQALAJADARQAEALAAAUIAAAQIgEgBg");
	this.shape_22.setTransform(44.7,15.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#BD268B").s().p("AAQA2IgRgHIgBgRQAAgUgDgKQgEgRgKgJIgLgIQgGgFgCgEQgDgEAAgFIAJgGQAEgBACgEIAEADQAgAXATAvQAJAWAEAgIgagKg");
	this.shape_23.setTransform(79.4,15.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#583222").s().p("AgVCeQhAAAgngCIgCgGQgRg1gBg5IAAgXQgQAbgJAfIgKAoIABgMQACgKgDgFQgDgFgGgBQgGgBgFADQgHACgJAMQASggAEglQgEAKgJAEIANgnQgMAIgIALQAPgVACgXIggAlQANgeAHgfIgpAaQAYgiAPgmIgKADQBJg9BugIIAyAMQgKAFgHAJQgHAIgFAKIAQAGQAIAEAFAFQAHAHAAAJQABAJgGAGIgIAFQgFADgCAEQgCAGAFAGQAEAGAIADIAOAEQAIAEADAFQg0AdgbAOQAOAMAQAFIgDAOIgDAOQgGAvAKAsIgKAAgABWCJIgTgDQAMgMAEgOQAEgQgKgIQgFgEgRgFQgOgEgDgHQgDgGADgHQACgHAGgFIAJgLQAGgFACgGQACgHgEgHQgEgGgHABIAdgaQgEgFgFgCQgGgCgFABQAFgFAEgIQAEgIABgIIgdgLIAJgJQAEgFAAgGQACgGgFgFQgFgGgFADQAWgPANgXQgNgDgMgFQB+ANBOBFQAMALALANQALANAJAOQgBANADAPQABAMAFAVIgcggIANA0IgVgOQADAggBAZQgJgRgOgNQABAIAFAMIAIAWQAIAZgGASIAAABQgHgDgIgBIgBAAQgVg5gogvIgTgVQAaAtARA7IAGAWIiRAEIAKgVgAHsB5IgDgDQAJABAIACIgGAIIgIgIgAHBBZIgQgIIA2AAQgJAMgFANQgOgMgKgFgAmwBPIgFgVQgBgGgDgDIgBgCQgDgEgFAAQgGAAgEALIgaAIQAogZAmgkIguABIAngbIgRgDQAKgWAQgQQgpgHgggMQAagIAVgNIgpgHIATgMQgQAEgQABQARgGAUgDQAhgFAbAFQARACAOAIIAOAJQAIAGALANIAOAPIADADIABABIAEADIABABIgBABIgEACIgIAIQgGAGgFAHQgaAfgEAUQgCARAIAVIAJARIgRgEQgIgBgIgDQgOgGgKADQgIADgKALIgJAOIgGAMIgBgMgAnPA9QgDAJgGALIghABgAGdBJIgjgNQgFgggIgWQgUgvgggXIgEgDQAGgGAHgKQAMgRAHgGQANgLAVgGQAOgEAZgCIAEAAIgLAGQAmAQApgGQgTALgOARQgPARgIAUIATAKIghApQAVAIAUgDIg9ArQAcAHAegDIgeAWIgLgEg");
	this.shape_24.setTransform(45.9,15.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#462517").s().p("AEaBBIABgCQAGgRgJgaIgIgUQgEgMgCgJQAPANAIAQQABgYgDgfIAVAOIgNg0IAcAgQgFgWgBgLQgCgQAAgNQARAdALAkIAFAVQAKAugCA5QgCBkglBOQAUigg3gbgAkuCfQgHgrABguQABg1ALgrQAPhCAkguQAPgTASgRIAGgGIAKgDQgPAlgYAiIAqgZQgIAegMAgIAfgnQgCAYgPAVQAIgLAMgIIgNAoQAJgFAEgJQgEAlgSAeQAKgKAGgDQAGgCAFAAQAGACADAEQADAEgCALIgBALQgEAVgEAXIgVgFQgYgEgUABQgIASgGAUIgDANQgGASgEAVQgIAsgDA3QgSgqgIgugAIDAlIAFgIQgIgDgJAAIgPgNQAEgNAJgKIg2AAIgJgEIAegWQgeACgbgHIA9grQgVADgVgHIAigrIgTgJQAHgVAPgQQAPgRATgLQgpAFgngPIALgHQAdgCAPACQAaACARANQARALAPATQAKAPAHASIADAJQAGAXABASIgKgKIgCgBQggghgrgIQA8AgARA9IABAAQAHAbgIAfQgKApgmAzQAMgmgRgWgAoKAWQgQgqABgmQABgiAPgWQANgUAagOIAVgLQgKgCgLAHQgnAPg4AjQAPglAXgaQAYgZAZgNIAIgEIALgEQAPAAAQgFIgTANIAqAGQgWAOgZAIQAfALAqAHQgQARgLAWIASACIgoAdIAvgBQgnAjgnAZIAZgIIgDAHIgqAWIAhgBIgJANIgPAVIgGAMQgHATACAXQgTgigKgWgAAzBBQgZhLgHgwQgDgOgBgNQgGAQgFARQgPgGgOgMQAbgOA0gdQgCgGgJgDIgOgFQgHgCgFgGQgGgHADgGQACgDAGgEIAHgFQAGgGgBgJQAAgJgHgGQgFgGgIgEIgQgFQAEgKAHgJQAJgIAJgFIgygNQAZgCAdABQATABASACQAMAFANADQgNAXgWAOQAFgCAFAFQAFAFgCAHQABAFgFAGIgIAJIAcALQAAAIgEAHQgFAIgFAFQAFAAAGACQAFACAFAEIgdAbQAGgCAFAIQADAGgBAHQgCAGgHAGIgJAKQgGAGgCAGQgDAHADAGQADAIAOAEQARAEAFAEQALAIgFAQQgEAOgMALIATADIgKAVIgnAAg");
	this.shape_25.setTransform(44.5,24.9);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFCC00").ss(5,1,1).p("AH0H0QDPjPAAklQAAkkjPjPQjPjPklAAQkkAAjPDPQjPDPAAEkQAAElDPDPQDPDPEkAAQElAADPjPg");
	this.shape_26.setTransform(44.3,42.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAALDQkkAAjPjPQjPjPAAklQAAkkDPjPQDPjPEkAAQElAADPDPQDPDPAAEkQAAEljPDPQjPDPklAAIAAAAg");
	this.shape_27.setTransform(44.3,42.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26}]}).wait(1));

	// Layer_5
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(255,255,255,0.02)").s().p("ArvLzIAA3lIXfAAIAAXlg");
	this.shape_28.setTransform(44.5,41.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_28).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy5, new cjs.Rectangle(-30.6,-34.1,150.4,151.1), null);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnhHiQjIjHAAkbQAAkaDIjHQDHjIEaAAQEbAADHDIQDIDHAAEaQAAEbjIDHQjHDIkbAAQkaAAjHjIg");
	mask.setTransform(44.3,42.5);

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhLDIAA2FQEmAADODPQDPDPAAEkQAAEljPDPQjODPkmAAIAAAAg");
	this.shape.setTransform(8.9,42.5,1,1,180);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AINAJIBJAmQgcgbgEgEQhVhdiAAvQgXALgYAOQBhgrB6A5gAoMAJIhJAmQAcgbAEgEQBVhdCAAvQAXALAYAOQhhgrh6A5g");
	this.shape_1.setTransform(41.8,30.6,0.345,0.345);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000B01").s().p("AIOAJQh7g5hhArIAvgZQCAgvBVBdIAgAfgAo1AQQBVhdCAAvIAvAZQhggrh7A5IhJAmIAggfg");
	this.shape_2.setTransform(41.8,30.6,0.345,0.345);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AA5AhQgQAMgWgJQgKgFgFgCQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANg");
	this.shape_3.setTransform(41.9,46.7,0.345,0.345);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F0BD7E").s().p("AATAkIgPgHQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANQgJAHgLAAQgIAAgKgEg");
	this.shape_4.setTransform(41.9,46.7,0.345,0.345);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B0364E").s().p("Ag9gDIAhgIQANAEANAAQARAAAPgIQAOACASAGIgFAEQgYATgiAAQghAAgbgTg");
	this.shape_5.setTransform(41.9,57.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F2DDE3").s().p("AgZAQQgLgFgKgKQgIgIgEgKQBBgGA0AHQgFAKgIAHQgHAHgGAEQgPAIgSAAQgMAAgNgEg");
	this.shape_6.setTransform(41.6,54.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#7B0E24").s().p("AhTgRQAMAAAMgDQAEAKAJAJQAJAJAMAFIgiAIQgUgRgEgVgAAgAJQAGgEAIgGQAIgIAEgKIAaAFQgEAQgPAPQgTgGgOgCg");
	this.shape_7.setTransform(41.6,55.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#2C2221").s().p("ACAAGQAPgPAXgDQAWgEARAPQALAHAFALIgJgDQgUgIgeAEQgbACgOAIQADgHAEgHgAjVAAQAPgQAWgDQAWgDASAOQALAIAGAKQgFgBgFgDQgTgGggADQgaACgOAIQACgHAFgGg");
	this.shape_8.setTransform(42.1,37.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AjqAIQgUgPgTgTQgjglgXgqQgshQAAhkQAAhSAdhEQAVgwAjgqQAIgKAJgKQAFgEAEgFQBlhhCLAAQBHAAA+AbQA+AaAzA1QAKAKAIAKQBVBmAACKQAAA+gRA2QgXBJg3A9QgDAEgFAFQgXAYgZASQgdAVgfAPQgSAIgTAFQgSAGgTADQgfAGghAAQgdAAgagEQgGAAgFgCQgMgCgMgCQhCgSg2gsIgRgiQAbgDANgTQgaACgTgYQgRgVgEgdQgCgSgCgfQAAgCAAgDQAAgGAAgHQAAgWABgRQAAgDAAgCQABgYAGgcQAFgaANgSQASgZAygNQADgBACAAQAEgXAHgUQAQgeAZgMQgDAJgDAJQgIAagDAhQAWgEAVgCQAFgNAJgNQAUgfAcgTQgVAigNAnQBVgIBSAPQgPgegRgZQAgANAdAbQANALALAOQAkAKAPAPQAWAUAGAUQAFASAEBDQAAAOABAPQAAAJAAAJQgBAogPAjQgPAjgfAfQANAJAPgGQgPAUgKAVAhyBGQAAACAAACQgCAYgCAUQgEApgGAcIC9ACIAGAAQAWgBAWAAQAqgCAqgBQASgBARgCQAKgBAMgBIABAAQADAAACAAQAKgBAKAAQAZgCAagDQALAAAJABQA7AKAABFIAADEIhGAAIhJA4Ak3H6Ihgg4IAAjEQAAhQBPAAQASACASABQABALABALQAAANgBAtQAAAdgBAyQAAAXAAAaQgDAigQBXQEBEkE/kkAkkCxQAXACAXACQAMAAAMACQAvADAvABAjeC3QACAZAABDIAAAmIAAADQD6AiCsglQADhAgKhAABNBBQAAAFABAEIAAAAQABAfAEAYQACAYAEAUQABAIACAHAD8C0QAAABAAAAQACAHABAHQAGA5gBBRQgBBHAGBm");
	this.shape_9.setTransform(44.5,62.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E7BF8E").s().p("AgDCKQAEgeAEgdQAHhUgdhCQgNgegVgUIgNgLIAzgFQALAAAIACQA7AJAABGIAADCg");
	this.shape_10.setTransform(78.8,93.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F0C48D").s().p("AAAALIgOgEQgkgMgegPQAaAEAcAAQAhAAAggGQATgDASgGIAAAIIAAABQACAdADAZQgygKgfgLg");
	this.shape_11.setTransform(44.7,71.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EEC189").s().p("AjTA5IAAgCQAsgQAmgPQBugpBZg0IAGAAIAsgBIBUgDQALBBgEA/QhZAThuAAQhmAAh5gRg");
	this.shape_12.setTransform(43.6,88);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#00CCCC").s().p("ACZBMQhLALhCATQAAgVAJgPIh6AeIAAgZQgaAGgegBIAFgTIhBAgQgGAEgFABQgHADgEgDQgHgCgEgHQgDgEgCgJQgFgTgCgTQgEgkAGgkQAHgpASgkQAMABAMACQACAZAABCIAAAlIAAADQD6AjCsgmQADg+gKhBIAigDIAXgCIAAACIACANIgOApIABgBQAGgBABAEQABADAAADIgDAHIgrBMIALgOIghA2IAHgSQghAfgnAZIAaglg");
	this.shape_13.setTransform(43.4,91.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0CBFBF").s().p("AkfBcQAPhXADgiIABgwIABhQIAAg6IgCgVIAvADQgTAkgGAqQgHAkAEAkQADATAEATQADAJADAEQAEAHAGACQAFADAHgDQAFgBAGgDIBAghIgEATQAdABAagGIABAaIB6gfQgJAPAAAVQBCgTBLgLIgaAlQAngZAhgeIgHASIAhg3IgLAOIAqhMIADgIQABgDgCgDQgBgEgFABIgBABIAOgoQAGA5gBBRQgBBHAGBlQigCRiPAAQiQAAiAiRg");
	this.shape_14.setTransform(42.2,103.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AEhEbQABhRgGg5IgDgOIAAgBIABAAIAGAAIATgBIANALQAVAUANAeQAfBCgIBVQgDAcgGAfIhKA4QgGhmABhHgAl6GQIAAjEQAAhQBQAAIAjADIACAWIAAA6IgBBPIgBAxQgDAigPBXgAi/DhQABhDgDgZQAvADAvABIC9ACQhaAzhtArQgmAPgsAPgABaCLIi9gCQAGgcAEgpIAFgsIAAgEIAXAEIAMACQAeAQAiAMIAQAFQAfAKAyAKIAGAsIADAPIgZABgAhjCJgAgxAaIgMgCIgXgEQhCgSg2grIgSgjQAbgDANgTQgZACgTgYQgSgVgEgdIgDgxIAAgFIAAgNIABgnQAJgcAUgTQAIgHAWgNQAUgMAJgJQAQgQAIgaQAFgRAFggIACASQABAKAEAHQAFAHAIAFQAJAEAIgEQAEgCAHgIQAIgIAFgCQALgEARAOQAUARAJAAQAKABANgJQAQgJAIgBQAHAAAKADIAQAHQAPAFAUAAIAkgEQAtgEAUAPQAQALAGAWQAFAUgCAWQgBALgHAeQgGAaAAAPIAAAiQgCATgHAMQgCAEgOANQgKAKgCAKQgCAHADAOIAIAWQAIAigYAhQgNARgRANQgSAIgTAFQgSAGgSADQggAGgiAAQgbAAgagEg");
	this.shape_15.setTransform(41.6,67.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#ECBF86").s().p("ACnDgQAZgigJghIgIgXQgDgNACgIQACgJAKgKQAOgOACgDQAIgMABgTIAAgiQABgOAFgaQAIgeAAgLQADgWgGgUQgGgWgQgMQgUgOgtAEIgjAEQgUAAgPgFIgQgIQgKgCgIAAQgIABgPAJQgOAJgJgBQgJgBgUgRQgSgNgLAEQgFABgHAIQgHAJgFACQgHADgJgEQgIgEgFgHQgEgHgCgKIgCgSQgEAfgFARQgIAbgQAQQgJAJgUAMQgWANgIAHQgVASgJAdIAAgFQABgZAGgbQAFgbANgRQASgaAygNIAFgBQAEgWAHgUQAQgeAZgMIgGASQgIAZgDAiQAVgEAWgCQAFgNAIgNQAUggAdgTQgVAjgNAnQBUgIBTAOQgPgdgRgaQAgAOAdAbQAMALAMANQAjAKAQAQQAVAUAHATQAEATAFBDIABAbIAAATQgBAogPAiQgQAkgeAfQAMAJAQgHQgQAVgKAWQgcAVggAPQASgOAMgRg");
	this.shape_16.setTransform(42.1,41.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#1C1E1C").s().p("ADhEWQgQAGgMgIQAegfAQgkQAPgiABgoIAAgTIgBgcQgFhEgEgSQgHgTgVgTQgQgQgjgKQgMgNgMgLQgSgugUgtQAkBGA3A1Qg+hRgjhgQBaBCBNBOQguhnhNhSQA6A1A/A/QgjhHg5g5QgUgUgVgQQA/AaAzA2QAKAKAHAKQBVBlAACKQAAA9gQA3QgXBJg3A8IgIAKQgXAYgaATQAKgWAQgVgAj3EWQgjgmgXgqQgshQAAhjQAAhRAdhEQAVgxAjgpIARgUIAJgJQA1gYAzgUQgoBahEBFQAtgsA4gcQAGAfgMAbQAYggAjgYQgFAkgRAnIAGgRQgZAMgQAeQgHAUgEAWIgFABQgyANgSAZQgNASgFAaQgGAbgBAZIAAAEIgBAoIAAANIAAAFIAEAxQAEAdARAVQATAXAagCQgNAUgbADIARAjQgUgQgTgTg");
	this.shape_17.setTransform(41.9,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000B01").s().p("ABfAXQAUAtASAuQgdgbgggNQARAZAPAeQhTgPhUAIQANgnAVgiQgdATgUAfQgIANgFANQgWACgVAEQADghAIgaQARgoAFgjQgjAXgYAgQAMgbgGgdQg4AagtAsQBEhEAohZQgzATg1AYQBlhhCKAAQBIAAA9AbQAVAQAUATQA5A5AjBGQg/g+g6g0QBNBRAuBnQhNhPhahBQAjBfA+BRQg3g0gkhHg");
	this.shape_18.setTransform(43.2,11.5);

	var maskedShapeInstanceList = [this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_2
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#EE3D3D").ss(5,1,1).p("AH0H0QDPjPAAklQAAkkjPjPQjPjPklAAQkkAAjPDPQjPDPAAEkQAAElDPDPQDPDPEkAAQElAADPjPg");
	this.shape_19.setTransform(44.3,42.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAALDQkkAAjPjPQjPjPAAklQAAkkDPjPQDPjPEkAAQElAADPDPQDPDPAAEkQAAEljPDPQjPDPklAAIAAAAg");
	this.shape_20.setTransform(44.3,42.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19}]}).wait(1));

	// Layer_5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(255,255,255,0.02)").s().p("ArvLzIAA3lIXfAAIAAXlg");
	this.shape_21.setTransform(44.5,41.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(-30.6,-34.1,150.4,151.1), null);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnhHiQjIjHAAkbQAAkaDIjHQDHjIEaAAQEbAADHDIQDIDHAAEaQAAEbjIDHQjHDIkbAAQkaAAjHjIg");
	mask.setTransform(44.3,42.5);

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhLDIAA2FQEmAADODPQDPDPAAEkQAAEljPDPQjODPkmAAIAAAAg");
	this.shape.setTransform(79.7,42.5);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AA5AhQgQAMgWgJQgKgFgFgCQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANg");
	this.shape_1.setTransform(45.8,53.5,0.325,0.325);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F0BD7E").s().p("AATAkIgPgHQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANQgJAHgLAAQgIAAgKgEg");
	this.shape_2.setTransform(45.8,53.5,0.325,0.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AILAMIBIAmQgbgbgFgFQhVhdiAAvQgWAMgZANQBhgrB7A6gAoKAGIhIAmQAbgbAFgEQBVhdCAAvQAWALAZAOQhhgrh7A5g");
	this.shape_3.setTransform(45.3,40.1,0.325,0.325);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#622F0D").s().p("AILAMQh7g6hhArIAvgZQCAgvBVBdIAgAggAoyANQBVhdCAAvIAvAZQhhgrh7A5IhIAmIAggfg");
	this.shape_4.setTransform(45.3,40.1,0.325,0.325);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABigSIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQhoBDhohDQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIQAAABgBACQBiA/BihAQgBgCABgDg");
	this.shape_5.setTransform(46.8,58.8,0.928,0.928);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#772326").s().p("AhogGQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIIgBADQBiA/BihAIAAgFIAAgBIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQg0Ahg0AAQg0AAg0ghg");
	this.shape_6.setTransform(46.8,58.8,0.928,0.928);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F9FEF5").s().p("AgFAGQgCgCgBgEQABgCACgCQACgDADAAQADAAACADQADACAAACQAAAEgDACQgCACgDAAQgDAAgCgCg");
	this.shape_7.setTransform(61.4,43);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F9FEF7").s().p("AAMAdIgMgBIgHgCQgRgDgJAAQgKAAgGgDQgGgCASgWQAPgRAPgFIAIgCIALAAIAIACQAJABAHAFQARAKgCANQgCAOgNAHQgHAEgJABIgIAAgAAMgVQgFACgFAEQgFAHAAAIQAAAKAFAGQAFAEAFABIAGABIACAAQAHgBAHgFQAGgGAAgKQAAgIgGgHQgHgGgHAAIgCgBIgGABg");
	this.shape_8.setTransform(58.7,43.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#090A0B").s().p("AAAAWIgFgBQgFgBgFgEQgGgGAAgKQAAgIAGgHQAFgEAFgCIAFgBIACABQAHAAAHAGQAGAHAAAIQAAAKgGAGQgHAFgHABIgCAAgAACgNQgCACAAAEQAAAEACABQADACACAAQAEAAACgCQAEgBAAgEQAAgEgEgCQgCgCgEAAQgCAAgDACg");
	this.shape_9.setTransform(60.5,43.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F9FEF5").s().p("AgFAFQgCgBAAgEQAAgCACgDQADgCACAAQAEAAACACQACADAAACQAAAEgCABQgCADgEAAQgCAAgDgDg");
	this.shape_10.setTransform(30.3,43.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#090A0B").s().p("AAAAWIgBAAQgJgBgFgFQgHgGAAgKQAAgIAHgHQAFgGAJAAIABgBIAGABQAFACAFAEQAHAHAAAIQAAAKgHAGQgFAEgFACIgGAAgAgNgNQgCADAAADQAAAEACABQACACAEAAQADAAADgCQABgBAAgEQAAgDgBgDQgDgCgDAAQgEAAgCACg");
	this.shape_11.setTransform(31.1,43.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F9FEF7").s().p("AgTAdQgJgBgHgEQgOgHgCgOQAAgNAPgKQAIgFAJgBIAJgCIAKAAIAIADQAPAEAPARQASAWgFACQgHADgKAAQgJAAgRADIgIACIgKABIgJAAgAgTgVQgIAAgFAGQgHAHAAAIQAAAKAHAGQAFAFAIABIACAAIAHAAQAFgCAFgEQAFgGAAgKQAAgIgFgHQgFgEgFgCIgHgBIgCABg");
	this.shape_12.setTransform(32.8,43.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AkUioQgSgqgIguQgHgrABguQABg1ALgsQAPhCAkguQAOgTATgRQADgDADgDQBJg+BugIQAagCAcABQATABASACQB+AMBOBGQANALALANQAKANAJAOQASAdAKAkQADAKACALQAKAvgCA5QgCBkglBOQgYAyglApQhABIhVAXQgqAMguAAQgQAAgNgBQgJgBgIgBQAAAAgCAAQgYgDgVgHAGrmzQAGACAFADQAFACAFACQAIADAHAEQAKAGAPALQAHAFAIAIQABABACABQAFAEAEAFQAQAWgKAmQAlgzAKgpQAIgggHgbQgBAAAAAAQgRg9g8ggQArAIAgAhQACAAAAABQAFAFAFAFQgBgSgGgXQgCgFgBgEQgHgSgKgPQgPgTgRgLQgQgNgbgCQgPgCgcACQgDAAgCAAQgZACgOAEQgVAGgNAMQgHAGgMARQgGAKgHAGQACABACABQAhAXATAwQAJAWAEAgQAOAFAVAIgAkUioQADg3AIgsQAEgVAFgSQACgHACgGQAGgUAIgSQATgBAYAEQAJACANADQADgXAFgVQAFgUAFgTQAJgfAQgcQAAAMAAAMQABA5ARA0QABAEABADQAnACBAAAQAFAAAEAAQgJgsAGgwQABgHABgGQACgIABgGQAEgRAHgQQABANADAOQAHAwAZBMQATAAAUAAQBAgBBRgDQgCgLgDgKQgRg7gbgtQAJAKAKAKQAoAwAVA4QABAAABAAQAIABAGAEQA3AbgUCgAkmpFQgBgBgCgCQgGgGgIgKQgLgNgIgGQgHgEgHgEQgOgIgRgDQgbgEghAEQgUAEgRAFQgFACgFACQgFACgEACQgYANgYAZQgYAagOAlQA4gjAngPQALgHAKACQgMAGgJAFQgaAOgNAUQgPAWgBAiQgCAmARArQAJAWAUAiQgDgXAIgTQACgFAEgHQAFgIAKgNQAFgIAEgGQAGgLADgKQABgEACgDQAEgKAGAAQAFAAADAEQABAAAAACQADADABAFQADAKACAMQAAAFABAHQADgGADgGQAEgIAFgGQAKgMAIgCQAKgEAOAHQAIADAIABQAIABAJACQgGgKgDgHQgIgVACgRQAEgUAagfQAFgIAGgFQAEgFAEgDQACgCACgBIABgBIgBAAQgCgCgCgBQAAgBgBAAgAhKBCIhtgsIAAACIAABPIAABlIAIgHIALgIAhKBCIAAgsQhRgYg+hHQgmgsgVgzADVBnIB1AAQBHAAAABHIAACtIjtC3IgkAhIgnARQjwEBjWnqIAAitQAAhHBIAAIBuAAAhIBwIgxApIgrAkADGCwQgKAugqAjQg1AuhOAAQhNAAg3guQgkgegLgmADVBnIAAhRIgfAQIhLApIgQAIIAPANIANAKIAaAWIA1AsABrAXIAAAvIAAAJAhIBwIBRCcIBhioADVBnIAAACIAABTIgPgMAhKBCIAmAPIgkAf");
	this.shape_13.setTransform(44.5,66.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E78A00").s().p("AhdgDQAzgZAqgmIgigFQAXgNAIgZIgvgDQAPgJAKgPIgYgCIAVgmQgJgIgMABIAGgaIgagBIAAgCIB0AAQBHAAAABHIAACsIjrC2QAKjSAOgGg");
	this.shape_14.setTransform(72.8,98.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#9DACB6").s().p("AiFAnQgkgegLgkIArglQAPAbANAdQAKgiAOghQALAjgCAnIAEgNQANAiACAoQABgGAGgCQAHgCAGADQAEACAGAFIAKAHQAFADAHgBQAFAAACgEQgBAAAAABQAAAAABABQAAAAAAABQAAAAAAABQACADAEAAQAEACAIgEQAWgJAPgSQAQgTAEgXIAGAfIANgwQAJAAAIgHQAGgIAAgJQADAGACAFQgBgcAGgbIA1ArQgKAtgrAjQg1AuhMAAQhOAAg3gug");
	this.shape_15.setTransform(46.1,88.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#BAC1C6").s().p("AAMBfQgDgBgCgDQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgCAEgEABQgHABgGgDIgKgHQgFgGgFgCQgGgDgGACQgHADAAAGQgCgpgOgjIgDANQACglgLgkQgPAhgKAiQgMgdgPgaIAwgpIBSCbIBginIANAKIAbAWQgHAcACAcQgCgGgEgFQABAJgGAIQgIAGgJAAIgOAxIgFggQgEAXgQATQgQASgVAKQgFACgEAAIgEAAg");
	this.shape_16.setTransform(45.7,86);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF9900").s().p("AlDglIAAitQAAhIBJAAIBuAAIAABlIAIgGIAKgIQALAlAkAeQA3AuBNAAQBNAAA2guQAqgjAKguIAPAMIAAhSIAaABIgGAaQAMgBAJAIIgVAlIAYADQgKAPgPAJIAvADQgIAZgXANIAiAFQgqAlgzAaQgOAHgKDRIglAhIgnARQhTBZhOAAQiZAAiMlAg");
	this.shape_17.setTransform(40.3,105);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#C5D7E2").s().p("AjFgJIAAhPIAFAIQAHALAJAHIAGAFIAFAGIgBAJQAAACAVANQAVAMgXALQgXAKAEAHQAFAGAIABQgVAegPAiIgIAHgAC3A/Ig0gsIgbgVIAFgCQAJgDANAAIAVgCIAHAAIAGgDQADgEAAgFIgBgKQgEgVAEgWIAfgQIAABRIAAACIAABSg");
	this.shape_18.setTransform(45.9,77.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#DBE3E8").s().p("AiKAYQgIgBgFgHQgEgGAXgLQAXgLgVgMQgVgMAAgCIABgJIgFgGIgGgFQgJgHgHgMIgFgIIAAgCIBtAsIAmAQIglAdIgwApIgsAlIgKAIQAPgiAVgegABqgJIgPgMIAQgJIBLgoQgEAVAEAVIABALQAAAFgDADIgGADIgHABIgVABQgNABgJACIgFABg");
	this.shape_19.setTransform(44.4,77.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#BB2E8C").s().p("AgaBCIgKgBQAIgYAMgUIAJgQQAEgJABgHQgCgHgEgKQgHgMgBgDQgBgGAAgGIAIgIIADgCIABgBQAIAHAOAHIAUAIQgkAugOBBIgNgBg");
	this.shape_20.setTransform(16.2,15.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#F3CFA2").s().p("AhHCnIAkgfIgmgPIAAgsQhRgZg+hGQgmgsgVgzQADg3AIgsQAagKAIgLQANgTAagNQAZgNAOgRQAJgKACgJQAEANALAMQATAUAbAGQAYAFAcgEQATgDAhgKIABgvQACAJAFAJQALAQARAJQAPAIAVACQALACAaAAIBOABIAVgBQAMgCAGgGQAJgHgCgLQATAbAAArQAAAngRA7QgRA8gSAfQgaAvgoARQgWAJgwACQgxACgWAKQgZALgUAWIAAABIACAAQBCAXBEAPIAAAJIgQAIIAPANIhhCogAgcBXQgXgDgWgHQAWAHAXADgAEBklQgFgIgIgHQALABACAMIAAACIAAAAg");
	this.shape_21.setTransform(44.4,61);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#ECC391").s().p("Ag3D4IARACQAOABAQABQAtAAAqgNIAAAvQhFgOhBgYgAgmD6IgRgCIgCAAIAAgBQAUgWAagLQAVgKAxgCQAwgCAWgJQAogRAagvQASggARg7QARg7AAgmQAAgsgTgbIAAgBQgCgNgLgBQAIAHAFAIQABALgIAHQgGAGgMADIgVAAIhOAAQgagBgLgBQgVgDgPgIQgSgJgKgPQgEgKgCgJIgCAvQghALgTACQgdAEgXgFQgbgGgTgTQgLgMgEgNQgDAIgIAKQgOASgaAMQgZAOgNASQgIAMgaAJQAEgVAFgSIAEgNQAGgUAIgSQATgBAYAEIAWAFQADgWAFgWIAKgnQAJgfAQgcIAAAYQABA5ARA1IACAGQAnACBAAAIAKAAQgKgsAGgvIADgOIADgOQAEgQAHgRQABANADAOQAHAwAYBNIAnAAICRgFIgGgVQgRg7gagtIATAVQAoAvAVA4IABAAQAIABAHAEQA3AbgUCgQgYAxglAqQhBBHhUAYQgqANgtAAQgQgBgOgBgAg3D4IAAAAg");
	this.shape_22.setTransform(47.3,44.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CC3399").s().p("AlJA7IgJgRQgIgVACgRQAEgTAagfQAFgIAGgFQAAAFACAGQABADAGANQAFAKABAGQAAAIgEAJIgJAQQgMATgJAYIgHgBgAFVAnQgLgkgRgcQgJgOgLgNIAMgHQABAFACAFQADAEAGAEIAKAIQALAJADARQAEALAAAUIAAAQIgEgBg");
	this.shape_23.setTransform(44.7,15.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#BD268B").s().p("AAQA2IgRgHIgBgRQAAgUgDgKQgEgRgKgJIgLgIQgGgFgCgEQgDgEAAgFIAJgGQAEgBACgEIAEADQAgAXATAvQAJAWAEAgIgagKg");
	this.shape_24.setTransform(79.4,15.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#583222").s().p("AgVCeQhAAAgngCIgCgGQgRg1gBg5IAAgXQgQAbgJAfIgKAoIABgMQACgKgDgFQgDgFgGgBQgGgBgFADQgHACgJAMQASggAEglQgEAKgJAEIANgnQgMAIgIALQAPgVACgXIggAlQANgeAHgfIgpAaQAYgiAPgmIgKADQBJg9BugIIAyAMQgKAFgHAJQgHAIgFAKIAQAGQAIAEAFAFQAHAHAAAJQABAJgGAGIgIAFQgFADgCAEQgCAGAFAGQAEAGAIADIAOAEQAIAEADAFQg0AdgbAOQAOAMAQAFIgDAOIgDAOQgGAvAKAsIgKAAgABWCJIgTgDQAMgMAEgOQAEgQgKgIQgFgEgRgFQgOgEgDgHQgDgGADgHQACgHAGgFIAJgLQAGgFACgGQACgHgEgHQgEgGgHABIAdgaQgEgFgFgCQgGgCgFABQAFgFAEgIQAEgIABgIIgdgLIAJgJQAEgFAAgGQACgGgFgFQgFgGgFADQAWgPANgXQgNgDgMgFQB+ANBOBFQAMALALANQALANAJAOQgBANADAPQABAMAFAVIgcggIANA0IgVgOQADAggBAZQgJgRgOgNQABAIAFAMIAIAWQAIAZgGASIAAABQgHgDgIgBIgBAAQgVg5gogvIgTgVQAaAtARA7IAGAWIiRAEIAKgVgAHsB5IgDgDQAJABAIACIgGAIIgIgIgAHBBZIgQgIIA2AAQgJAMgFANQgOgMgKgFgAmwBPIgFgVQgBgGgDgDIgBgCQgDgEgFAAQgGAAgEALIgaAIQAogZAmgkIguABIAngbIgRgDQAKgWAQgQQgpgHgggMQAagIAVgNIgpgHIATgMQgQAEgQABQARgGAUgDQAhgFAbAFQARACAOAIIAOAJQAIAGALANIAOAPIADADIABABIAEADIABABIgBABIgEACIgIAIQgGAGgFAHQgaAfgEAUQgCARAIAVIAJARIgRgEQgIgBgIgDQgOgGgKADQgIADgKALIgJAOIgGAMIgBgMgAnPA9QgDAJgGALIghABgAGdBJIgjgNQgFgggIgWQgUgvgggXIgEgDQAGgGAHgKQAMgRAHgGQANgLAVgGQAOgEAZgCIAEAAIgLAGQAmAQApgGQgTALgOARQgPARgIAUIATAKIghApQAVAIAUgDIg9ArQAcAHAegDIgeAWIgLgEg");
	this.shape_25.setTransform(45.9,15.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#462517").s().p("AEaBBIABgCQAGgRgJgaIgIgUQgEgMgCgJQAPANAIAQQABgYgDgfIAVAOIgNg0IAcAgQgFgWgBgLQgCgQAAgNQARAdALAkIAFAVQAKAugCA5QgCBkglBOQAUigg3gbgAkuCfQgHgrABguQABg1ALgrQAPhCAkguQAPgTASgRIAGgGIAKgDQgPAlgYAiIAqgZQgIAegMAgIAfgnQgCAYgPAVQAIgLAMgIIgNAoQAJgFAEgJQgEAlgSAeQAKgKAGgDQAGgCAFAAQAGACADAEQADAEgCALIgBALQgEAVgEAXIgVgFQgYgEgUABQgIASgGAUIgDANQgGASgEAVQgIAsgDA3QgSgqgIgugAIDAlIAFgIQgIgDgJAAIgPgNQAEgNAJgKIg2AAIgJgEIAegWQgeACgbgHIA9grQgVADgVgHIAigrIgTgJQAHgVAPgQQAPgRATgLQgpAFgngPIALgHQAdgCAPACQAaACARANQARALAPATQAKAPAHASIADAJQAGAXABASIgKgKIgCgBQggghgrgIQA8AgARA9IABAAQAHAbgIAfQgKApgmAzQAMgmgRgWgAoKAWQgQgqABgmQABgiAPgWQANgUAagOIAVgLQgKgCgLAHQgnAPg4AjQAPglAXgaQAYgZAZgNIAIgEIALgEQAPAAAQgFIgTANIAqAGQgWAOgZAIQAfALAqAHQgQARgLAWIASACIgoAdIAvgBQgnAjgnAZIAZgIIgDAHIgqAWIAhgBIgJANIgPAVIgGAMQgHATACAXQgTgigKgWgAAzBBQgZhLgHgwQgDgOgBgNQgGAQgFARQgPgGgOgMQAbgOA0gdQgCgGgJgDIgOgFQgHgCgFgGQgGgHADgGQACgDAGgEIAHgFQAGgGgBgJQAAgJgHgGQgFgGgIgEIgQgFQAEgKAHgJQAJgIAJgFIgygNQAZgCAdABQATABASACQAMAFANADQgNAXgWAOQAFgCAFAFQAFAFgCAHQABAFgFAGIgIAJIAcALQAAAIgEAHQgFAIgFAFQAFAAAGACQAFACAFAEIgdAbQAGgCAFAIQADAGgBAHQgCAGgHAGIgJAKQgGAGgCAGQgDAHADAGQADAIAOAEQARAEAFAEQALAIgFAQQgEAOgMALIATADIgKAVIgnAAg");
	this.shape_26.setTransform(44.5,24.9);

	var maskedShapeInstanceList = [this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_2
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFCC00").ss(5,1,1).p("AH0H0QDPjPAAklQAAkkjPjPQjPjPklAAQkkAAjPDPQjPDPAAEkQAAElDPDPQDPDPEkAAQElAADPjPg");
	this.shape_27.setTransform(44.3,42.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAALDQkkAAjPjPQjPjPAAklQAAkkDPjPQDPjPEkAAQElAADPDPQDPDPAAEkQAAEljPDPQjPDPklAAIAAAAg");
	this.shape_28.setTransform(44.3,42.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27}]}).wait(1));

	// Layer_5
	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("rgba(255,255,255,0.02)").s().p("ArvLzIAA3lIXfAAIAAXlg");
	this.shape_29.setTransform(44.5,41.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_29).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-30.6,-34.1,150.4,151.1), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnhHiQjIjHAAkbQAAkaDIjHQDHjIEaAAQEbAADHDIQDIDHAAEaQAAEbjIDHQjHDIkbAAQkaAAjHjIg");
	mask.setTransform(44.3,42.5);

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhLDIAA2FQEmAADODPQDPDPAAEkQAAEljPDPQjODPkmAAIAAAAg");
	this.shape.setTransform(8.9,42.5,1,1,180);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AA5AhQgQAMgWgJQgKgFgFgCQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANg");
	this.shape_1.setTransform(45.8,53.5,0.325,0.325);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F0BD7E").s().p("AATAkIgPgHQgIgDgGAAQgQAEgIAAQgLABgKgJQgKgIgBgMQgCgZAcgKQATgJAaAEQAZAFALAGQASAJAHARQAFAVgOANQgJAHgLAAQgIAAgKgEg");
	this.shape_2.setTransform(45.8,53.5,0.325,0.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(0,0,0,0.098)").ss(1,1,1,3,true).p("AILAMIBIAmQgbgbgFgFQhVhdiAAvQgWAMgZANQBhgrB7A6gAoKAGIhIAmQAbgbAFgEQBVhdCAAvQAWALAZAOQhhgrh7A5g");
	this.shape_3.setTransform(45.3,40.1,0.325,0.325);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#622F0D").s().p("AILAMQh7g6hhArIAvgZQCAgvBVBdIAgAggAoyANQBVhdCAAvIAvAZQhhgrh7A5IhIAmIAggfg");
	this.shape_4.setTransform(45.3,40.1,0.325,0.325);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(9,9,9,0.298)").ss(1,1,1,3,true).p("ABigSIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQhoBDhohDQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIQAAABgBACQBiA/BihAQgBgCABgDg");
	this.shape_5.setTransform(46.8,58.8,0.928,0.928);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#772326").s().p("AhogGQgEABgDgCQgGgEABgGIABgFQAFgGAHADQAIADgCAIIgBADQBiA/BihAIAAgFIAAgBIABgEQAFgGAHADQAIADgCAIQgBAEgCACQgFAEgFgCQg0Ahg0AAQg0AAg0ghg");
	this.shape_6.setTransform(46.8,58.8,0.928,0.928);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F9FEF5").s().p("AgFAGQgCgCgBgEQABgCACgCQACgDADAAQADAAACADQADACAAACQAAAEgDACQgCACgDAAQgDAAgCgCg");
	this.shape_7.setTransform(61.4,43);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F9FEF7").s().p("AAMAdIgMgBIgHgCQgRgDgJAAQgKAAgGgDQgGgCASgWQAPgRAPgFIAIgCIALAAIAIACQAJABAHAFQARAKgCANQgCAOgNAHQgHAEgJABIgIAAgAAMgVQgFACgFAEQgFAHAAAIQAAAKAFAGQAFAEAFABIAGABIACAAQAHgBAHgFQAGgGAAgKQAAgIgGgHQgHgGgHAAIgCgBIgGABg");
	this.shape_8.setTransform(58.7,43.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#090A0B").s().p("AAAAWIgFgBQgFgBgFgEQgGgGAAgKQAAgIAGgHQAFgEAFgCIAFgBIACABQAHAAAHAGQAGAHAAAIQAAAKgGAGQgHAFgHABIgCAAgAACgNQgCACAAAEQAAAEACABQADACACAAQAEAAACgCQAEgBAAgEQAAgEgEgCQgCgCgEAAQgCAAgDACg");
	this.shape_9.setTransform(60.5,43.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F9FEF5").s().p("AgFAFQgCgBAAgEQAAgCACgDQADgCACAAQAEAAACACQACADAAACQAAAEgCABQgCADgEAAQgCAAgDgDg");
	this.shape_10.setTransform(30.3,43.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#090A0B").s().p("AAAAWIgBAAQgJgBgFgFQgHgGAAgKQAAgIAHgHQAFgGAJAAIABgBIAGABQAFACAFAEQAHAHAAAIQAAAKgHAGQgFAEgFACIgGAAgAgNgNQgCADAAADQAAAEACABQACACAEAAQADAAADgCQABgBAAgEQAAgDgBgDQgDgCgDAAQgEAAgCACg");
	this.shape_11.setTransform(31.1,43.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F9FEF7").s().p("AgTAdQgJgBgHgEQgOgHgCgOQAAgNAPgKQAIgFAJgBIAJgCIAKAAIAIADQAPAEAPARQASAWgFACQgHADgKAAQgJAAgRADIgIACIgKABIgJAAgAgTgVQgIAAgFAGQgHAHAAAIQAAAKAHAGQAFAFAIABIACAAIAHAAQAFgCAFgEQAFgGAAgKQAAgIgFgHQgFgEgFgCIgHgBIgCABg");
	this.shape_12.setTransform(32.8,43.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AkUioQgSgqgIguQgHgrABguQABg1ALgsQAPhCAkguQAOgTATgRQADgDADgDQBJg+BugIQAagCAcABQATABASACQB+AMBOBGQANALALANQAKANAJAOQASAdAKAkQADAKACALQAKAvgCA5QgCBkglBOQgYAyglApQhABIhVAXQgqAMguAAQgQAAgNgBQgJgBgIgBQAAAAgCAAQgYgDgVgHAGrmzQAGACAFADQAFACAFACQAIADAHAEQAKAGAPALQAHAFAIAIQABABACABQAFAEAEAFQAQAWgKAmQAlgzAKgpQAIgggHgbQgBAAAAAAQgRg9g8ggQArAIAgAhQACAAAAABQAFAFAFAFQgBgSgGgXQgCgFgBgEQgHgSgKgPQgPgTgRgLQgQgNgbgCQgPgCgcACQgDAAgCAAQgZACgOAEQgVAGgNAMQgHAGgMARQgGAKgHAGQACABACABQAhAXATAwQAJAWAEAgQAOAFAVAIgAkUioQADg3AIgsQAEgVAFgSQACgHACgGQAGgUAIgSQATgBAYAEQAJACANADQADgXAFgVQAFgUAFgTQAJgfAQgcQAAAMAAAMQABA5ARA0QABAEABADQAnACBAAAQAFAAAEAAQgJgsAGgwQABgHABgGQACgIABgGQAEgRAHgQQABANADAOQAHAwAZBMQATAAAUAAQBAgBBRgDQgCgLgDgKQgRg7gbgtQAJAKAKAKQAoAwAVA4QABAAABAAQAIABAGAEQA3AbgUCgAkmpFQgBgBgCgCQgGgGgIgKQgLgNgIgGQgHgEgHgEQgOgIgRgDQgbgEghAEQgUAEgRAFQgFACgFACQgFACgEACQgYANgYAZQgYAagOAlQA4gjAngPQALgHAKACQgMAGgJAFQgaAOgNAUQgPAWgBAiQgCAmARArQAJAWAUAiQgDgXAIgTQACgFAEgHQAFgIAKgNQAFgIAEgGQAGgLADgKQABgEACgDQAEgKAGAAQAFAAADAEQABAAAAACQADADABAFQADAKACAMQAAAFABAHQADgGADgGQAEgIAFgGQAKgMAIgCQAKgEAOAHQAIADAIABQAIABAJACQgGgKgDgHQgIgVACgRQAEgUAagfQAFgIAGgFQAEgFAEgDQACgCACgBIABgBIgBAAQgCgCgCgBQAAgBgBAAgAhKBCIhtgsIAAACIAABPIAABlIAIgHIALgIAhKBCIAAgsQhRgYg+hHQgmgsgVgzADVBnIB1AAQBHAAAABHIAACtIjtC3IgkAhIgnARQjwEBjWnqIAAitQAAhHBIAAIBuAAAhIBwIgxApIgrAkADGCwQgKAugqAjQg1AuhOAAQhNAAg3guQgkgegLgmADVBnIAAhRIgfAQIhLApIgQAIIAPANIANAKIAaAWIA1AsABrAXIAAAvIAAAJAhIBwIBRCcIBhioADVBnIAAACIAABTIgPgMAhKBCIAmAPIgkAf");
	this.shape_13.setTransform(44.5,66.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E78A00").s().p("AhdgDQAzgZAqgmIgigFQAXgNAIgZIgvgDQAPgJAKgPIgYgCIAVgmQgJgIgMABIAGgaIgagBIAAgCIB0AAQBHAAAABHIAACsIjrC2QAKjSAOgGg");
	this.shape_14.setTransform(72.8,98.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#9DACB6").s().p("AiFAnQgkgegLgkIArglQAPAbANAdQAKgiAOghQALAjgCAnIAEgNQANAiACAoQABgGAGgCQAHgCAGADQAEACAGAFIAKAHQAFADAHgBQAFAAACgEQgBAAAAABQAAAAABABQAAAAAAABQAAAAAAABQACADAEAAQAEACAIgEQAWgJAPgSQAQgTAEgXIAGAfIANgwQAJAAAIgHQAGgIAAgJQADAGACAFQgBgcAGgbIA1ArQgKAtgrAjQg1AuhMAAQhOAAg3gug");
	this.shape_15.setTransform(46.1,88.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#BAC1C6").s().p("AAMBfQgDgBgCgDQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgCAEgEABQgHABgGgDIgKgHQgFgGgFgCQgGgDgGACQgHADAAAGQgCgpgOgjIgDANQACglgLgkQgPAhgKAiQgMgdgPgaIAwgpIBSCbIBginIANAKIAbAWQgHAcACAcQgCgGgEgFQABAJgGAIQgIAGgJAAIgOAxIgFggQgEAXgQATQgQASgVAKQgFACgEAAIgEAAg");
	this.shape_16.setTransform(45.7,86);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF9900").s().p("AlDglIAAitQAAhIBJAAIBuAAIAABlIAIgGIAKgIQALAlAkAeQA3AuBNAAQBNAAA2guQAqgjAKguIAPAMIAAhSIAaABIgGAaQAMgBAJAIIgVAlIAYADQgKAPgPAJIAvADQgIAZgXANIAiAFQgqAlgzAaQgOAHgKDRIglAhIgnARQhTBZhOAAQiZAAiMlAg");
	this.shape_17.setTransform(40.3,105);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#C5D7E2").s().p("AjFgJIAAhPIAFAIQAHALAJAHIAGAFIAFAGIgBAJQAAACAVANQAVAMgXALQgXAKAEAHQAFAGAIABQgVAegPAiIgIAHgAC3A/Ig0gsIgbgVIAFgCQAJgDANAAIAVgCIAHAAIAGgDQADgEAAgFIgBgKQgEgVAEgWIAfgQIAABRIAAACIAABSg");
	this.shape_18.setTransform(45.9,77.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#DBE3E8").s().p("AiKAYQgIgBgFgHQgEgGAXgLQAXgLgVgMQgVgMAAgCIABgJIgFgGIgGgFQgJgHgHgMIgFgIIAAgCIBtAsIAmAQIglAdIgwApIgsAlIgKAIQAPgiAVgegABqgJIgPgMIAQgJIBLgoQgEAVAEAVIABALQAAAFgDADIgGADIgHABIgVABQgNABgJACIgFABg");
	this.shape_19.setTransform(44.4,77.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#BB2E8C").s().p("AgaBCIgKgBQAIgYAMgUIAJgQQAEgJABgHQgCgHgEgKQgHgMgBgDQgBgGAAgGIAIgIIADgCIABgBQAIAHAOAHIAUAIQgkAugOBBIgNgBg");
	this.shape_20.setTransform(16.2,15.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#F3CFA2").s().p("AhHCnIAkgfIgmgPIAAgsQhRgZg+hGQgmgsgVgzQADg3AIgsQAagKAIgLQANgTAagNQAZgNAOgRQAJgKACgJQAEANALAMQATAUAbAGQAYAFAcgEQATgDAhgKIABgvQACAJAFAJQALAQARAJQAPAIAVACQALACAaAAIBOABIAVgBQAMgCAGgGQAJgHgCgLQATAbAAArQAAAngRA7QgRA8gSAfQgaAvgoARQgWAJgwACQgxACgWAKQgZALgUAWIAAABIACAAQBCAXBEAPIAAAJIgQAIIAPANIhhCogAgcBXQgXgDgWgHQAWAHAXADgAEBklQgFgIgIgHQALABACAMIAAACIAAAAg");
	this.shape_21.setTransform(44.4,61);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#ECC391").s().p("Ag3D4IARACQAOABAQABQAtAAAqgNIAAAvQhFgOhBgYgAgmD6IgRgCIgCAAIAAgBQAUgWAagLQAVgKAxgCQAwgCAWgJQAogRAagvQASggARg7QARg7AAgmQAAgsgTgbIAAgBQgCgNgLgBQAIAHAFAIQABALgIAHQgGAGgMADIgVAAIhOAAQgagBgLgBQgVgDgPgIQgSgJgKgPQgEgKgCgJIgCAvQghALgTACQgdAEgXgFQgbgGgTgTQgLgMgEgNQgDAIgIAKQgOASgaAMQgZAOgNASQgIAMgaAJQAEgVAFgSIAEgNQAGgUAIgSQATgBAYAEIAWAFQADgWAFgWIAKgnQAJgfAQgcIAAAYQABA5ARA1IACAGQAnACBAAAIAKAAQgKgsAGgvIADgOIADgOQAEgQAHgRQABANADAOQAHAwAYBNIAnAAICRgFIgGgVQgRg7gagtIATAVQAoAvAVA4IABAAQAIABAHAEQA3AbgUCgQgYAxglAqQhBBHhUAYQgqANgtAAQgQgBgOgBgAg3D4IAAAAg");
	this.shape_22.setTransform(47.3,44.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CC3399").s().p("AlJA7IgJgRQgIgVACgRQAEgTAagfQAFgIAGgFQAAAFACAGQABADAGANQAFAKABAGQAAAIgEAJIgJAQQgMATgJAYIgHgBgAFVAnQgLgkgRgcQgJgOgLgNIAMgHQABAFACAFQADAEAGAEIAKAIQALAJADARQAEALAAAUIAAAQIgEgBg");
	this.shape_23.setTransform(44.7,15.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#BD268B").s().p("AAQA2IgRgHIgBgRQAAgUgDgKQgEgRgKgJIgLgIQgGgFgCgEQgDgEAAgFIAJgGQAEgBACgEIAEADQAgAXATAvQAJAWAEAgIgagKg");
	this.shape_24.setTransform(79.4,15.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#583222").s().p("AgVCeQhAAAgngCIgCgGQgRg1gBg5IAAgXQgQAbgJAfIgKAoIABgMQACgKgDgFQgDgFgGgBQgGgBgFADQgHACgJAMQASggAEglQgEAKgJAEIANgnQgMAIgIALQAPgVACgXIggAlQANgeAHgfIgpAaQAYgiAPgmIgKADQBJg9BugIIAyAMQgKAFgHAJQgHAIgFAKIAQAGQAIAEAFAFQAHAHAAAJQABAJgGAGIgIAFQgFADgCAEQgCAGAFAGQAEAGAIADIAOAEQAIAEADAFQg0AdgbAOQAOAMAQAFIgDAOIgDAOQgGAvAKAsIgKAAgABWCJIgTgDQAMgMAEgOQAEgQgKgIQgFgEgRgFQgOgEgDgHQgDgGADgHQACgHAGgFIAJgLQAGgFACgGQACgHgEgHQgEgGgHABIAdgaQgEgFgFgCQgGgCgFABQAFgFAEgIQAEgIABgIIgdgLIAJgJQAEgFAAgGQACgGgFgFQgFgGgFADQAWgPANgXQgNgDgMgFQB+ANBOBFQAMALALANQALANAJAOQgBANADAPQABAMAFAVIgcggIANA0IgVgOQADAggBAZQgJgRgOgNQABAIAFAMIAIAWQAIAZgGASIAAABQgHgDgIgBIgBAAQgVg5gogvIgTgVQAaAtARA7IAGAWIiRAEIAKgVgAHsB5IgDgDQAJABAIACIgGAIIgIgIgAHBBZIgQgIIA2AAQgJAMgFANQgOgMgKgFgAmwBPIgFgVQgBgGgDgDIgBgCQgDgEgFAAQgGAAgEALIgaAIQAogZAmgkIguABIAngbIgRgDQAKgWAQgQQgpgHgggMQAagIAVgNIgpgHIATgMQgQAEgQABQARgGAUgDQAhgFAbAFQARACAOAIIAOAJQAIAGALANIAOAPIADADIABABIAEADIABABIgBABIgEACIgIAIQgGAGgFAHQgaAfgEAUQgCARAIAVIAJARIgRgEQgIgBgIgDQgOgGgKADQgIADgKALIgJAOIgGAMIgBgMgAnPA9QgDAJgGALIghABgAGdBJIgjgNQgFgggIgWQgUgvgggXIgEgDQAGgGAHgKQAMgRAHgGQANgLAVgGQAOgEAZgCIAEAAIgLAGQAmAQApgGQgTALgOARQgPARgIAUIATAKIghApQAVAIAUgDIg9ArQAcAHAegDIgeAWIgLgEg");
	this.shape_25.setTransform(45.9,15.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#462517").s().p("AEaBBIABgCQAGgRgJgaIgIgUQgEgMgCgJQAPANAIAQQABgYgDgfIAVAOIgNg0IAcAgQgFgWgBgLQgCgQAAgNQARAdALAkIAFAVQAKAugCA5QgCBkglBOQAUigg3gbgAkuCfQgHgrABguQABg1ALgrQAPhCAkguQAPgTASgRIAGgGIAKgDQgPAlgYAiIAqgZQgIAegMAgIAfgnQgCAYgPAVQAIgLAMgIIgNAoQAJgFAEgJQgEAlgSAeQAKgKAGgDQAGgCAFAAQAGACADAEQADAEgCALIgBALQgEAVgEAXIgVgFQgYgEgUABQgIASgGAUIgDANQgGASgEAVQgIAsgDA3QgSgqgIgugAIDAlIAFgIQgIgDgJAAIgPgNQAEgNAJgKIg2AAIgJgEIAegWQgeACgbgHIA9grQgVADgVgHIAigrIgTgJQAHgVAPgQQAPgRATgLQgpAFgngPIALgHQAdgCAPACQAaACARANQARALAPATQAKAPAHASIADAJQAGAXABASIgKgKIgCgBQggghgrgIQA8AgARA9IABAAQAHAbgIAfQgKApgmAzQAMgmgRgWgAoKAWQgQgqABgmQABgiAPgWQANgUAagOIAVgLQgKgCgLAHQgnAPg4AjQAPglAXgaQAYgZAZgNIAIgEIALgEQAPAAAQgFIgTANIAqAGQgWAOgZAIQAfALAqAHQgQARgLAWIASACIgoAdIAvgBQgnAjgnAZIAZgIIgDAHIgqAWIAhgBIgJANIgPAVIgGAMQgHATACAXQgTgigKgWgAAzBBQgZhLgHgwQgDgOgBgNQgGAQgFARQgPgGgOgMQAbgOA0gdQgCgGgJgDIgOgFQgHgCgFgGQgGgHADgGQACgDAGgEIAHgFQAGgGgBgJQAAgJgHgGQgFgGgIgEIgQgFQAEgKAHgJQAJgIAJgFIgygNQAZgCAdABQATABASACQAMAFANADQgNAXgWAOQAFgCAFAFQAFAFgCAHQABAFgFAGIgIAJIAcALQAAAIgEAHQgFAIgFAFQAFAAAGACQAFACAFAEIgdAbQAGgCAFAIQADAGgBAHQgCAGgHAGIgJAKQgGAGgCAGQgDAHADAGQADAIAOAEQARAEAFAEQALAIgFAQQgEAOgMALIATADIgKAVIgnAAg");
	this.shape_26.setTransform(44.5,24.9);

	var maskedShapeInstanceList = [this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_2
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#EE3D3D").ss(5,1,1).p("AH0H0QDPjPAAklQAAkkjPjPQjPjPklAAQkkAAjPDPQjPDPAAEkQAAElDPDPQDPDPEkAAQElAADPjPg");
	this.shape_27.setTransform(44.3,42.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAALDQkkAAjPjPQjPjPAAklQAAkkDPjPQDPjPEkAAQElAADPDPQDPDPAAEkQAAEljPDPQjPDPklAAIAAAAg");
	this.shape_28.setTransform(44.3,42.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27}]}).wait(1));

	// Layer_5
	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("rgba(255,255,255,0.02)").s().p("ArvLzIAA3lIXfAAIAAXlg");
	this.shape_29.setTransform(44.5,41.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_29).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-30.6,-34.1,150.4,151.1), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbArIABgZIAAgTIAAgGIgWAAIAAgTIAXAAQABgLADgJQADgJAGgIQAGgHAHgFQAJgEAMAAQAGAAAHADQAHACAHAGIgJAQQgGgDgFgCIgIgCQgFgBgEACIgHACQgDADgCAEIgDAJIgCAOIAoABIgCATIgngBIAAAIIgBAaIAAANIAAAMIAAAOIAAANIgUABIAAglg");
	this.shape.setTransform(-229.2,-4.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAGBVIgIgGQgFgDgCgFIgFgJQgFgLgBgOIACh8IAUAAIgBAhIAAAaIgBAVIAAAQIAAAaQAAAJACAIIACAFIAEAGIAFADQADACAFAAIgCAUQgIgBgFgCg");
	this.shape_1.setTransform(-236.7,-5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAeA4IABgOQgLAHgLAEQgLADgKABQgIAAgGgCQgGgDgFgDQgGgEgCgFQgDgHgBgIQABgHACgGQACgHAEgEQAEgFAGgCIAMgHIANgCIANgBIAMAAIAKACIgDgKIgFgJQgDgDgEgDQgEgDgGABIgHABQgGABgGADIgMAIQgHAFgIAIIgMgPQAKgIAIgFQAIgGAJgDQAHgEAGgBIALgBQAJAAAHADQAHAEAGAEQAEAGAFAHIAFAPIAEARIABARIgBAUIgEAYgAgEAAQgIACgGADQgGAEgDAGQgDAFACAIQABAGAEACQAEACAFABQAHAAAGgCIAMgGIAMgFIAKgGIABgJIgBgKIgLgCIgKgBQgIAAgIACg");
	this.shape_2.setTransform(-246.6,-2.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAaBKIAEgSIACgQIAAgPQAAgLgDgHQgEgHgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgDQALABAJAEQAJAEAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAPIgBAMIgDAMg");
	this.shape_3.setTransform(-259.1,-4.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgUBRIgJgDIgJgFIgIgIQgEgEgDgGIATgIIAFAHIAFAFIAHADIAGACQAHACAHgBQAHgCAFgCIAJgHIAFgHIAEgHIABgHIABgEIAAgNQgGAGgGADIgMAEQgGACgGAAQgLAAgJgEQgKgEgHgIQgIgHgDgJQgFgLAAgNQAAgOAFgKQAFgKAHgHQAHgIAKgDQAJgEAKAAIAMADIANAFQAGAEAFAFIABgTIATABIgBBvQABAGgCAGQgCAHgDAGQgDAGgEAFIgLAJQgGAEgHADQgIACgIABIgCAAQgKAAgJgDgAgOg7QgGADgEAFQgEAFgCAHQgDAHAAAIQAAAIADAHQACAHAEAEQAEAEAHADQAFADAHAAQAGAAAGgCQAGgDAGgEQAEgDAEgFQADgGABgHIAAgMQgBgHgDgGQgEgGgFgEQgFgEgGgCQgHgCgFAAQgHAAgGACg");
	this.shape_4.setTransform(-277.9,0.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAbA2IACggIAAgeIgBgFIgBgGIgCgGQgBgEgDgCQgCgCgDgBQgEgBgEABQgIABgHAKQgIAKgJASIABAZIAAAPIAAAHIgVADIgBgfIgBgYIAAgSIgBgOIAAgVIAWAAIAAAcIAIgKIAIgJIAKgHQAFgCAGAAQAIAAAFABQAGACADADQAEADADAEIAEAJIACAJIABAIIAAAhIgBAkg");
	this.shape_5.setTransform(-290.4,-2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgMBJIABgZIAAgeIABgpIATgBIAAAZIgBAVIAAARIAAANIgBAVgAgEguIgFgDIgDgEIgBgFIABgGIADgEIAFgDIAEgBIAFABIAFADIADAEIABAGIgBAFIgDAEIgFADIgFABIgEgBg");
	this.shape_6.setTransform(-298.9,-4.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAaBKIAEgSIACgQIAAgPQAAgLgDgHQgEgHgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgDQALABAJAEQAJAEAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAPIgBAMIgDAMg");
	this.shape_7.setTransform(-308,-4.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_8.setTransform(-320.7,-2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgLgKIgiABIAAgSIAjgBIAAgsIASAAIAAArIAmgBIgCASIgkABIgBBUIgTAAg");
	this.shape_9.setTransform(-331.5,-4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAeA4IABgOQgMAHgKAEQgMADgJABQgHAAgHgCQgGgDgGgDQgEgEgEgFQgDgHAAgIQAAgHACgGQADgHAEgEQAEgFAGgCIAMgHIANgCIANgBIAMAAIAKACIgDgKIgFgJQgDgDgEgDQgEgDgGABIgIABQgFABgGADIgMAIQgHAFgIAIIgMgPQAJgIAJgFQAJgGAHgDQAIgEAGgBIALgBQAJAAAHADQAHAEAFAEQAGAGADAHIAGAPIAEARIABARIgCAUIgDAYgAgEAAQgIACgGADQgGAEgDAGQgDAFACAIQABAGAEACQAEACAFABQAGAAAHgCIAMgGIAMgFIAKgGIAAgJIAAgKIgLgCIgKgBQgJAAgHACg");
	this.shape_10.setTransform(-342.8,-2.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAvAYIAAgVIgBgPQAAgJgCgDQgCgFgEAAIgFACIgFAEIgFAFIgFAGIgEAHIgDAEIAAAJIABAMIAAAOIAAASIgSABIAAgdIgBgVIgBgPQAAgJgDgDQgCgFgEAAIgFACIgFAFIgFAFIgFAHIgFAHIgDAFIABAyIgUABIgChnIAUgDIAAAdIAHgIIAHgHIAJgHQAFgCAGAAIAIACQADABADADQADADACAEQACAEABAGIAGgIIAHgHQAEgEAFgCQAEgCAGAAQAEABAEABQAEABAEAEQADACACAGQACAEAAAHIABASIABAWIAAAiIgUABIAAgdg");
	this.shape_11.setTransform(-356.9,-2.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgLgKIgiABIABgSIAigBIAAgsIATAAIgBArIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_12.setTransform(-374.9,-4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_13.setTransform(-385.7,-2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAeA4IABgOQgLAHgLAEQgLADgKABQgIAAgGgCQgGgDgFgDQgGgEgCgFQgDgHgBgIQABgHACgGQACgHAEgEQAEgFAGgCIAMgHIANgCIANgBIAMAAIAKACIgDgKIgFgJQgDgDgEgDQgEgDgGABIgHABQgGABgGADIgMAIQgHAFgIAIIgMgPQAKgIAIgFQAIgGAJgDQAHgEAGgBIALgBQAJAAAHADQAHAEAGAEQAEAGAFAHIAFAPIAEARIABARIgBAUIgEAYgAgEAAQgIACgGADQgGAEgDAGQgDAFACAIQABAGAEACQAEACAFABQAHAAAGgCIAMgGIAMgFIAKgGIABgJIgBgKIgLgCIgKgBQgIAAgIACg");
	this.shape_14.setTransform(-398.3,-2.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AABAPIgcAmIgVgGIAlgvIgmgvIAUgEIAdAlIAegmIASAHIgjAtIAlAuIgSAHg");
	this.shape_15.setTransform(-409.7,-2.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_16.setTransform(-420.9,-2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_17.setTransform(-437.9,-2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAaBKIAEgSIACgQIAAgPQAAgLgDgHQgEgHgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgDQALABAJAEQAJAEAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAPIgBAMIgDAMg");
	this.shape_18.setTransform(-450.3,-4.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgLgKIgiABIABgSIAigBIAAgsIATAAIgBArIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_19.setTransform(-461.7,-4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgLgKIgiABIABgSIAhgBIABgsIATAAIgBArIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_20.setTransform(-476.4,-4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_21.setTransform(-487.2,-2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_22.setTransform(-499.1,-2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAGBVIgIgGQgFgDgCgFIgFgJQgFgLgBgOIADh8IATAAIgBAhIAAAaIgBAVIAAAQIAAAaQAAAJABAIIADAFIAEAGIAFADQAEACAEAAIgCAUQgIgBgFgCg");
	this.shape_23.setTransform(-507.3,-5.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_24.setTransform(-516.5,-2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgMBPIgPgFIgOgHIgNgJIAOgQQAIAGAHAEQAIAEAFACIANADQAHABAGgCQAGgBAEgDQAFgDADgEQACgEABgEQAAgEgBgDIgFgFQgDgDgEgBIgJgEIgJgCIgJgCIgMgDIgMgEQgGgDgFgEQgFgDgEgFQgEgGgCgHQgCgHABgKQAAgIADgGQADgGAEgFQAEgFAGgEIALgGIANgDIANgBQAJAAAJADIAJACIAIAEIAJAFIAIAHIgLARIgGgGIgHgFIgIgDIgGgDQgIgCgHAAQgIAAgHADQgHADgEAEQgFAEgDAFQgCAFAAAGQAAAFADAEQADAFAFAEQAFAEAHACQAIADAHABIAOACIANAEIAMAHQAGADADAGQAEAEACAHQACAGgBAIQgBAHgDAFQgDAFgFAEIgJAHIgLAEIgMACIgLABIgOgBg");
	this.shape_25.setTransform(-528.9,-4.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#990000").ss(3,0,0,6.9).p("A42iuMAxuAAAQBIAAAzAzQAzAzAABIQAABJgzAzQgzAzhIAAMgxuAAAQhJAAgzgzQgzgzAAhJQAAhIAzgzQAzgzBJAAg");
	this.shape_26.setTransform(-382.6,-3.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF3333").s().p("A42CuQhJABgzgzQgzg0AAhIQAAhIAzgzQAzgyBJAAMAxtAAAQBJAAAzAyQAzAzAABIQAABIgzA0QgzAzhJgBg");
	this.shape_27.setTransform(-382.6,-3.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-560.7,-22.5,356.2,37.9), null);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(366.6,9.7,2.424,2.424,0,0,0,44.5,45.4);

	this.instance_1 = new lib.Symbol1copy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-366.6,9.7,2.424,2.424,0,0,0,44.5,45.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-548.8,-183.1,1097.7,406);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy2();
	this.instance.parent = this;
	this.instance.setTransform(0,8.1,2.02,2.02,0,0,0,44.5,45.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.12,scaleY:2.12,y:8.2},9).to({scaleX:2.02,scaleY:2.02,y:8.1},10).to({scaleX:2.12,scaleY:2.12,y:8.2},10).to({scaleX:2.02,scaleY:2.02,y:8.1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-151.8,-152.6,303.7,338.4);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0,1.013,1.013);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1.01,scaleY:1.01},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1.01,scaleY:1.01},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-212.9,-36.8,447.7,74.1);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.questxt = new lib.questiontext_mccopy();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(-391.6,-2.9,0.949,0.949,0,0,0,-385.6,-4.1);

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-557.8,-20.4,338.2,36), null);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(646.8,333.5,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(336).to({_off:false},0).wait(32).to({startPosition:12},0).to({alpha:0,startPosition:19},8).wait(1));

	// Layer_12
	this.instance_1 = new lib.Symbol1copy5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(640,316.7,2.02,2.02,0,0,0,44.5,45.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(326).to({_off:false},0).to({regX:44.6,scaleX:2.22,scaleY:2.22,x:640.1,y:316.6},10).wait(32).to({alpha:0},8).wait(1));

	// Layer_7
	this.instance_2 = new lib.Symbol1copy11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1006.5,526.6,2.072,2.072,0,0,0,44.5,45.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(282).to({_off:false},0).to({x:642.4,y:318.5},25).to({_off:true},1).wait(69));

	// VAL
	this.instance_3 = new lib.Symbol1copy13();
	this.instance_3.parent = this;
	this.instance_3.setTransform(640,316.7,2.02,2.02,0,0,0,44.5,45.4);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(308).to({_off:false},0).wait(18).to({alpha:0},8).wait(43));

	// Layer_8
	this.instance_4 = new lib.Symbol1copy_1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(1150.3,609.7,1,1,0,0,0,201,74.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(247).to({_off:false},0).to({_off:true},35).wait(95));

	// Layer_5
	this.instance_5 = new lib.Symbol1copy12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(1011.3,267.8,1,1,0,0,0,41,60.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(217).to({_off:false},0).to({_off:true},30).wait(130));

	// Layer_10
	this.instance_6 = new lib.Tween18("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(581.9,332.6);
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween19("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(203,550.8,1.125,1.197,0,0,0,0.3,0.3);
	this.instance_7.alpha = 0.5;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(132).to({_off:false},0).to({_off:true,regX:0.3,regY:0.3,scaleX:1.13,scaleY:1.2,x:203,y:550.8,alpha:0.5},25).wait(220));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(132).to({_off:false},25).wait(46).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(168));

	// girl
	this.instance_8 = new lib.Tween20("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(583.8,338.1);
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween21("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(935.8,551,1.186,1.168,0,0,0,0.1,0.2);
	this.instance_9.alpha = 0.5;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(132).to({_off:false},0).to({_off:true,regX:0.1,regY:0.2,scaleX:1.19,scaleY:1.17,x:935.8,y:551,alpha:0.5},25).wait(220));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(132).to({_off:false},25).wait(46).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(168));

	// Layer_13
	this.instance_10 = new lib.Symbol4copy("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,325.2,1,1,0,0,0,0,16.6);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(97).to({_off:false},0).to({_off:true},112).wait(168));

	// Layer_6
	this.instance_11 = new lib.Tween15("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(744,115.1,0.941,0.941,0,0,0,0.3,0.2);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(97).to({_off:false},0).wait(106).to({startPosition:18},0).to({alpha:0,startPosition:22},5).to({_off:true},1).wait(168));

	// Layer_3
	this.instance_12 = new lib.Tween5("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,517);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(50).to({_off:false},0).wait(82).to({startPosition:0},0).to({startPosition:0},3).to({startPosition:0},191).to({alpha:0},8).wait(43));

	// Layer_2
	this.instance_13 = new lib.Symbol1copy2();
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,316.7,2.02,2.02,0,0,0,44.5,45.4);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(31).to({_off:false},0).wait(99).to({x:640.1},0).to({regX:44.6,x:640.2},2).to({regX:44.5,x:640},194).to({alpha:0},8).wait(43));

	// Layer_1
	this.questxt = new lib.questiontext_mc();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(640.2,112.4,2.202,2.202,0,0,0,-385.5,-4.2);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(15).to({_off:false},0).to({alpha:1},5).wait(306).to({alpha:0},8).wait(43));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(593.8,320.8,1370,786.6);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;