(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AibRCQgrgRgegeQgfgdgRgrQgSgpAAguQAAgwASgqQARgrAfgcQAegeArgTQApgRAwgBQAvABApARQAqATAfAeQAdAcARArQAUAqgBAwQABAugUApQgRArgdAdQgfAegqARQgpARgvABQgwgBgpgRgAi2FCIgYj1IgCgMIAAgOQAAhAAhgxQAhgyAygqQAxgqA4gpQA4goAzgxQAxguAhg9QAhg7AAhRQAAg2gVgrQgUgqgjgfQgkgfgzgSQgygPg5AAQhXgBg6AUQg8ASgpAYQgpAWgcATQgdATgWAAQg3AAgYguIhhiZQAzguA9gpQA9gnBGgfQBGgdBSgSQBUgRBggBQCBAABqAmQBtAiBNBEQBLBBArBgQApBfAAB0QAAByghBTQghBUgyA9QgxA+g7AtIhwBWQg1AngmAlQgmAmgHAxIgjDbg");
	this.shape.setTransform(0.4,31.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-217.3,-217.3,434.7,434.7);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0000FF").s().p("AhuiiIAsgCIAAAeQALgMANgGQAMgGALgDQAMgEALgBQANAAAOAEQANAEAMAHQAMAHAKAKQALAKAHANQAHAMAEAOQAEAPgBAPQAAASgFAPQgFAPgHAMQgIAMgKALQgKAKgLAHQgLAHgMAEQgNADgMAAQgLAAgNgDQgLgDgNgGQgNgGgNgLIgBCIIglABgAAAh9QgMAAgLAFQgLAEgJAHQgIAHgGAKQgHAJgDAKIAAAmQADAOAGAKQAHAKAJAFQAIAHAKAEQALADAKAAQANABANgGQAMgFAJgJQAJgJAGgNQAFgNABgPQABgPgEgNQgFgOgJgKQgJgKgMgGQgMgGgNAAIgCAAg");
	this.shape.setTransform(275.7,7.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0000FF").s().p("AgVBwQgSgBgNgGQgNgGgJgLQgJgKgFgNQgFgOgDgPQgDgPgBgQIgBgdQAAgVACgWQACgVADgWIApABQgDAZgBAUIgBAiIAAASIACAVQABAMADALQADALAFAKQAFAIAIAFQAHAFAMgCQAOgCARgTQARgTASgmIgBgzIgBgdIAAgRIArgFIACA/IACAxIABAmIABAdIABArIguABIgBg6QgGAMgJALQgIAKgKAIQgKAIgLAEQgJAFgLAAIgCAAg");
	this.shape_1.setTransform(248.8,2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0000FF").s().p("AgpBtQgTgIgPgQQgPgPgIgWQgJgWAAgaQAAgaAJgVQAIgWAPgQQAPgPATgJQATgIAWAAQAWAAAUAJQATAJAPAQQAOAPAJAWQAIAWAAAYQAAAZgIAWQgJAVgOAQQgPAQgTAJQgUAJgWAAQgWAAgTgJgAgchKQgNAHgHAMQgJAMgDAOQgDAPgBAOQAAAOAEAPQAEAOAIAMQAIAMAMAHQANAIAPAAQARAAAMgIQAMgHAJgMQAHgMAEgOQAEgPAAgOQAAgOgEgPQgDgOgIgMQgHgMgNgHQgNgIgRAAQgQAAgMAIg");
	this.shape_2.setTransform(223.8,2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0000FF").s().p("AhaAxIgCgxIgBgnIgBgdIgBgqIAtgBIABAZIAAAYIAAAXIAOgUQAJgKAKgKQALgLALgGQANgIAOgCQAQAAANAHIAMAJQAFAFAFAIQAEAJAEALQADAMABAPIgnAPIgCgRIgEgMIgFgJIgGgGQgHgEgJAAQgGABgGADQgGAEgGAGIgNAOIgNAQIgMAQIgKAOIABAdIAAAbIABAXIABARIgrAFIgCg/g");
	this.shape_3.setTransform(201.5,2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0000FF").s().p("AgrCnQgKgCgJgEQgJgFgJgGQgJgHgIgJQgHgJgFgMIAngRQAEAJAFAGQAGAGAGAEQAGAEAHADQAGADAHABQAOADAPgCQAOgDALgGQAKgFAHgHQAIgIAEgIIAHgPIADgNIABgJIAAgaQgMALgNAGQgMAGgLADQgNAEgMAAQgXAAgUgIQgUgIgPgPQgPgQgIgUQgJgWAAgbQAAgbAKgVQAJgWAQgPQAOgOAUgIQATgIAUAAQANACAOAEQAMAEANAHQAOAHALAMIAAgoIAoACIgBDlQAAANgEANQgDANgGAMQgHAMgIALQgKALgMAIQgNAIgPAGQgOAFgTABIgFABQgVAAgSgGgAgdh5QgNAFgIALQgKAKgEAPQgFAOAAAQQAAARAFAOQAFANAJAJQAJAKAMAGQAMAFAPAAQAMAAANgEQANgFAJgIQALgHAGgMQAIgLACgOIAAgbQgCgOgHgLQgHgMgLgJQgKgIgNgFQgNgEgMAAQgOAAgMAGg");
	this.shape_4.setTransform(174.9,6.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0000FF").s().p("AgsBsQgVgJgOgOQgOgOgHgWQgIgUAAgaQAAgYAJgVQAJgWAOgQQAPgQAUgKQAUgJAXAAQAUAAATAJQASAHAPAPQAOAOAJAUQAJATADAXIitAZQABAOAGAMQAGAKAIAJQAJAHAMAEQALAEANAAQAKAAALgDQAKgDAJgGQAJgHAGgJQAHgKADgMIAmAIQgFASgKAPQgLAPgNAKQgOALgQAGQgRAGgRAAQgagBgUgHgAgQhNQgKADgJAGQgKAIgHALQgIAMgDASIB4gPIgBgEQgIgUgNgLQgOgLgTAAQgIAAgKADg");
	this.shape_5.setTransform(139.5,2.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0000FF").s().p("AA1CZQAHgUACgSIAEghIAAggQgBgVgGgPQgHgOgJgKQgJgJgMgFQgLgEgKAAQgLABgMAGQgKAEgLALQgMAJgKASIgBCFIgnABIgEk0IAvgCIgCB6QAMgMAMgHQAMgHAMgDQAMgEALgCQAWAAATAIQATAIAMAPQAOAPAIAWQAHAUACAbIgBAeIgCAdIgDAbQgBANgDAKg");
	this.shape_6.setTransform(113.9,-2.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0000FF").s().p("AgXgWIhGABIABglIBGgCIABhXIAngDIgCBaIBOgDIgDAlIhLACIgCCuIgpABg");
	this.shape_7.setTransform(90.4,-2.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0000FF").s().p("AhdAyIgCgyIgBgmIgBgdIgBgrIAtgBIABA6QAHgLAIgKQAJgLAKgHQAJgIALgFQALgFAMgBQAPgBAMAEQALADAIAHQAIAGAFAJQAFAJADAJQADAJACAKIABAQQABAiAAAjIgCBIIgpgBIADhBQACgggCggIAAgJIgDgMQgBgHgDgGQgDgGgFgFQgFgFgHgCQgHgDgJACQgQACgPAVQgRAVgTAnIACAzIABAdIAAAQIgrAFIgCg+g");
	this.shape_8.setTransform(57,2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0000FF").s().p("AgZCXIABg0IACg/IAAhVIApgCIgBA0IgBAsIgBAjIAAAbIgBAsgAgLhfQgEgCgFgEQgDgEgCgFQgDgFAAgGQAAgGADgFQACgGADgDQAFgEAEgDQAGgCAFAAQAGAAAFACQAFADAEAEQAEADACAGQADAFAAAGQAAAGgDAFQgCAFgEAEQgEAEgFACQgFACgGAAQgFAAgGgCg");
	this.shape_9.setTransform(39.6,-2.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0000FF").s().p("AgsBsQgVgJgOgOQgOgOgHgWQgIgUAAgaQAAgYAJgVQAJgWAOgQQAPgQAUgKQAUgJAXAAQAUAAATAJQASAHAPAPQAOAOAJAUQAJATADAXIitAZQABAOAGAMQAGAKAIAJQAJAHAMAEQALAEANAAQAKAAALgDQAKgDAJgGQAJgHAGgJQAHgKADgMIAmAIQgFASgKAPQgLAPgNAKQgOALgQAGQgRAGgRAAQgagBgUgHgAgQhNQgKADgJAGQgKAIgHALQgIAMgDASIB4gPIgBgEQgIgUgNgLQgOgLgTAAQgIAAgKADg");
	this.shape_10.setTransform(11.5,2.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0000FF").s().p("AhaAxIgCgxIgBgnIgBgdIgBgqIAtgBIABAZIAAAYIAAAXIAOgUQAJgKAKgKQALgLALgGQANgIAOgCQAQAAANAHIAMAJQAFAFAFAIQAEAJAEALQADAMABAPIgnAPIgCgRIgEgMIgFgJIgGgGQgHgEgJAAQgGABgGADQgGAEgGAGIgNAOIgNAQIgMAQIgKAOIABAdIAAAbIABAXIABARIgrAFIgCg/g");
	this.shape_11.setTransform(-10.8,2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0000FF").s().p("AgVBwQgSgBgNgGQgNgGgJgLQgJgKgFgNQgFgOgDgPQgDgPgBgQIgBgdQAAgVACgWQACgVADgWIApABQgDAZgBAUIgBAiIAAASIACAVQABAMADALQADALAFAKQAFAIAIAFQAHAFAMgCQAOgCARgTQARgTASgmIgBgzIgBgdIAAgRIArgFIACA/IACAxIABAmIABAdIABArIguABIgBg6QgGAMgJALQgIAKgKAIQgKAIgLAEQgJAFgLAAIgCAAg");
	this.shape_12.setTransform(-36.1,2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0000FF").s().p("AgYgWIhFABIABglIBFgCIAChXIAngDIgBBaIBNgDIgDAlIhLACIgBCuIgqABg");
	this.shape_13.setTransform(-58.5,-2.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0000FF").s().p("AgkBsQgVgJgRgQQgRgPgJgWQgKgVAAgYQAAgOAEgPQAEgPAHgNQAIgNALgLQAKgLANgIQANgIAQgEQAPgFAQAAQARAAAQAEQAPAFAOAIQANAHALAMQALALAHAPIAAABIghAUIAAgBQgGgLgHgHQgIgJgJgGQgJgFgKgEQgLgDgLAAQgQABgOAGQgPAGgKALQgLALgGAOQgGAPAAAQQAAAQAGAOQAGAOALALQAKALAPAGQAOAGAQABQAKAAAKgEQAKgCAJgFQAJgFAHgIQAIgHAFgJIABgBIAjATIgBACQgHANgMAKQgLALgNAHQgOAIgPAEQgQAEgPAAQgWAAgVgJg");
	this.shape_14.setTransform(-80.6,2.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0000FF").s().p("AgZCXIABg0IABg/IABhVIApgCIgBA0IgBAsIgBAjIAAAbIgBAsgAgLhfQgEgCgFgEQgDgEgCgFQgCgFgBgGQABgGACgFQACgGADgDQAFgEAEgDQAGgCAFAAQAGAAAFACQAFADAEAEQAEADACAGQACAFABAGQgBAGgCAFQgCAFgEAEQgEAEgFACQgFACgGAAQgFAAgGgCg");
	this.shape_15.setTransform(-98,-2.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#0000FF").s().p("AhuiiIAsgCIAAAeQALgMANgGQAMgGALgDQAMgEALgBQANAAAOAEQANAEAMAHQAMAHAKAKQALAKAHANQAHAMAEAOQAEAPgBAPQAAASgFAPQgFAPgHAMQgIAMgKALQgKAKgLAHQgLAHgMAEQgNADgMAAQgLAAgNgDQgLgDgNgGQgNgGgNgLIgBCIIglABgAAAh9QgMAAgLAFQgLAEgJAHQgIAHgGAKQgHAJgDAKIAAAmQADAOAGAKQAHAKAJAFQAIAHAKAEQALADAKAAQANABANgGQAMgFAJgJQAJgJAGgNQAFgNABgPQABgPgEgNQgFgOgJgKQgJgKgMgGQgMgGgNAAIgCAAg");
	this.shape_16.setTransform(-116.1,7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0000FF").s().p("AgrCnQgJgCgJgEQgKgFgJgGQgJgHgHgJQgIgJgFgMIAngRQAEAJAGAGQAEAGAHAEQAGAEAHADQAGADAHABQAOADAPgCQAOgDAKgGQALgFAHgHQAHgIAFgIIAHgPIADgNIACgJIAAgaQgMALgNAGQgNAGgMADQgNAEgLAAQgXAAgUgIQgUgIgPgPQgPgQgIgUQgJgWAAgbQAAgbAJgVQAKgWAQgPQAPgOATgIQAUgIATAAQANACANAEQAMAEAOAHQANAHANAMIAAgoIAnACIgBDlQAAANgDANQgEANgGAMQgGAMgJALQgKALgMAIQgMAIgQAGQgPAFgRABIgGABQgVAAgSgGgAgeh5QgMAFgIALQgKAKgEAPQgFAOAAAQQAAARAFAOQAFANAJAJQAJAKAMAGQANAFAOAAQAMAAANgEQANgFAKgIQAJgHAIgMQAGgLAEgOIAAgbQgDgOgHgLQgHgMgLgJQgKgIgNgFQgMgEgNAAQgOAAgNAGg");
	this.shape_17.setTransform(-155.1,6.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#0000FF").s().p("AhdAyIgCgyIgBgmIgBgdIgBgrIAtgBIABA6QAHgLAIgKQAJgLAKgHQAJgIALgFQALgFAMgBQAPgBAMAEQALADAIAHQAIAGAFAJQAFAJADAJQADAJACAKIABAQQABAiAAAjIgCBIIgpgBIADhBQACgggCggIAAgJIgDgMQgBgHgDgGQgDgGgFgFQgFgFgHgCQgHgDgJACQgQACgPAVQgRAVgTAnIACAzIABAdIAAAQIgrAFIgCg+g");
	this.shape_18.setTransform(-180.8,2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#0000FF").s().p("AgZCXIABg0IACg/IABhVIAogCIgBA0IgBAsIgBAjIAAAbIgBAsgAgLhfQgEgCgFgEQgEgEgCgFQgCgFAAgGQAAgGACgFQACgGAEgDQAFgEAEgDQAFgCAGAAQAGAAAFACQAFADAEAEQAEADACAGQACAFAAAGQAAAGgCAFQgCAFgEAEQgEAEgFACQgFACgGAAQgGAAgFgCg");
	this.shape_19.setTransform(-198.2,-2.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#0000FF").s().p("AgcBxIgRgDIgSgHQgKgDgJgFQgJgFgJgHIAUgeIAXALQALAFAMAEQAMAEANADQALACAOAAQALAAAHgCQAIgCAFgDIAIgHIAEgIQABgDgBgEQAAgEgCgEIgFgIQgFgEgGgDQgGgDgKgCQgJgCgMAAQgRgBgRgDQgRgDgNgGQgNgGgJgJQgIgLgCgPQgBgOADgMQADgMAHgKQAHgJAJgHQAKgHAMgFQALgFAOgCQAMgCANAAIASAAIAWAEQAMACAMAFQAMAEAKAIIgOAkQgNgHgMgEIgVgGIgVgDQgegCgSAJQgRAIAAARQAAAMAGAGQAHAFALADIAbADIAgADQATADAOAGQANAFAIAIQAJAIADAKQAEAKAAAKQAAATgIANQgIANgMAIQgNAIgRAFQgRADgSABQgRAAgTgEg");
	this.shape_20.setTransform(-215.2,2.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#0000FF").s().p("AgcBxIgRgDIgSgHQgKgDgJgFQgJgFgJgHIAUgeIAXALQALAFAMAEQAMAEANADQALACAOAAQALAAAHgCQAIgCAFgDIAIgHIAEgIQABgDgBgEQAAgEgCgEIgFgIQgFgEgGgDQgGgDgKgCQgJgCgMAAQgRgBgRgDQgRgDgNgGQgNgGgJgJQgIgLgCgPQgBgOADgMQADgMAHgKQAHgJAJgHQAKgHAMgFQALgFAOgCQAMgCANAAIASAAIAWAEQAMACAMAFQAMAEAKAIIgOAkQgNgHgMgEIgVgGIgVgDQgegCgSAJQgRAIAAARQAAAMAGAGQAHAFALADIAbADIAgADQATADAOAGQANAFAIAIQAJAIADAKQAEAKAAAKQAAATgIANQgIANgMAIQgNAIgRAFQgRADgSABQgRAAgTgEg");
	this.shape_21.setTransform(-238.1,2.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#0000FF").s().p("AgZCXIABg0IACg/IABhVIAogCIgBA0IgBAsIgBAjIAAAbIgBAsgAgKhfQgFgCgEgEQgEgEgDgFQgCgFAAgGQAAgGACgFQADgGAEgDQAEgEAFgDQAFgCAFAAQAFAAAGACQAFADAEAEQAEADACAGQACAFAAAGQAAAGgCAFQgCAFgEAEQgEAEgFACQgGACgFAAQgFAAgFgCg");
	this.shape_22.setTransform(-254.1,-2.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#0000FF").s().p("ABgAwIAAgsIgBggQgBgQgEgHQgFgJgHAAQgGAAgEADIgLAIIgKALIgJANIgJAMIgGALIAAASIABAZIABAdIAAAmIgnACIAAg+IgCgsIgCggQgBgQgEgHQgFgJgIAAQgEAAgGADQgFAEgGAFIgLANIgKANIgJAOIgHALIACBpIgpACIgEjWIApgFIABA7IAOgRQAGgJAJgHQAIgHAKgEQAKgFALAAQAJAAAIADQAHACAGAGQAGAGAFAIQAEAJABANIAOgQQAHgJAIgHQAIgGAJgEQAJgFAMAAQAJAAAJADQAIADAGAHQAHAGAEAKQAEAKABANIACAlIABAwIABBGIgpACIgBg+g");
	this.shape_23.setTransform(-276.2,1.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(2,1,1).p("EgqkgFWMBVJAAAQBqAABKBKQBLBLAABpIAACwQAABqhLBKQhKBLhqAAMhVJAAAQhqAAhLhLQhKhKAAhqIAAiwQAAhpBKhLQBLhKBqAAg");
	this.shape_24.setTransform(0.1,1.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("EgqkAFXQhpAAhMhLQhKhKAAhqIAAiwQAAhpBKhLQBMhKBpAAMBVJAAAQBqAABKBKQBLBLAABpIAACwQAABqhLBKQhKBLhqAAg");
	this.shape_25.setTransform(0.1,1.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-298.9,-37.3,598,73.8);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2342BF").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape.setTransform(123,-218.8,0.495,0.495,0,0,0,245.7,-442.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ApXJYQj5j5AAlfQAAlfD5j4QD4j4FfAAQFfAAD5D4QD4D4ABFfQgBFfj4D5Qj5D4lfAAQlfAAj4j4g");
	this.shape_1.setTransform(-1.6,-5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-86.4,-89.9,169.7,169.7);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2342BF").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape.setTransform(150,-270.1,0.611,0.611,0,0,0,245.5,-442.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88.5,-84.2,177.1,168.4);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2342BF").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape.setTransform(0,0,0.481,0.481);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-69.7,-66.2,139.4,132.6), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AibRCQgrgSgegdQgfgdgRgqQgSgpAAgvQAAgwASgqQARgrAfgcQAegeArgTQApgRAwAAQAvAAApARQAqATAfAeQAdAcARArQAUAqgBAwQABAvgUApQgRAqgdAdQgfAdgqASQgpASgvAAQgwAAgpgSgAi2FCIgYj1IgCgMIAAgOQAAhBAhgwQAhgyAygqQAxgqA4gqQA4gnAzgxQAxguAhg9QAhg8AAhQQAAg3gVgqQgUgrgjgeQglgfgygRQgygRg5ABQhXgBg6AUQg8ASgpAYQgpAWgcATQgdATgWAAQg3AAgYguIhhiZQAzguA9goQA9goBGgfQBGgdBSgSQBUgRBfAAQCCAABqAlQBtAiBNBEQBLBBArBgQApBfAAB0QAAByghBTQghBUgyA9QgxA+g7AtIhwBWQg1AngmAlQgmAmgHAwIgjDcg");
	this.shape.setTransform(495.4,307.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape_1.setTransform(495.1,276);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_2.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy13, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2342BF").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape.setTransform(495.3,276.1,1.222,1.222);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape_1.setTransform(495.1,276);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCCCCC").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_2.setTransform(495.1,276.1,0.883,0.883);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("A64a4QrJrIAAvwQAAvvLJrJQLJrIPvAAQPwAALJLIQLJLJgBPvQABPwrJLIQrJLJvwAAQvvAArJrJg");
	this.shape_3.setTransform(495.2,276.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_4.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy7, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2342BF").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(495.3,276.1,1.741,1.741);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_1.setTransform(495.1,275.9,0.848,0.848);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCCCCC").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_2.setTransform(495.1,276.1,0.883,0.883);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("A64a4QrJrIAAvwQAAvvLJrJQLJrIPvAAQPwAALJLIQLJLJgBPvQABPwrJLIQrJLJvwAAQvvAArJrJg");
	this.shape_3.setTransform(495.2,276.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_4.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AibRCQgrgSgegdQgfgdgRgqQgSgpAAgvQAAgwASgqQARgrAfgcQAegeArgTQApgRAwAAQAvAAApARQAqATAfAeQAdAcARArQAUAqgBAwQABAvgUApQgRAqgdAdQgfAdgqASQgpASgvAAQgwAAgpgSgAi2FCIgYj1IgCgMIAAgOQAAhBAhgwQAhgyAygqQAxgqA4gqQA4gnAzgxQAxguAhg9QAhg8AAhQQAAg3gVgqQgUgrgjgeQglgfgygRQgygRg5ABQhXgBg6AUQg8ASgpAYQgpAWgcATQgdATgWAAQg3AAgYguIhhiZQAzguA9goQA9goBGgfQBGgdBSgSQBUgRBfAAQCCAABqAlQBtAiBNBEQBLBBArBgQApBfAAB0QAAByghBTQghBUgyA9QgxA+g7AtIhwBWQg1AngmAlQgmAmgHAwIgjDcg");
	this.shape.setTransform(495.4,307.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape_1.setTransform(495.1,276);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_2.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy5, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2342BF").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape.setTransform(495.3,276.1,1.222,1.222);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape_1.setTransform(495.1,276);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_2.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgvhGIATgBIAAAMQAEgEAGgDIAKgEIAJgCIANACIALAEQAFADAEAFQAFAEADAGQADAFACAHQACAGAAAGQgBAIgCAHQgCAHgDAEQgEAFgEAFIgJAHIgKAGIgMABIgJgBIgLgFQgFgCgGgFIAAA8IgRAAgAAAg2QgFAAgFACQgEACgEADQgEADgDAEQgCAEgBAEIgBARQABAGADAEQADAFAEACQAEADAEACQAFABAEAAQAFAAAGgCQAFgCAEgEQAEgEADgFQACgHAAgGQABgGgCgGQgCgGgEgEQgEgFgGgDQgEgCgGAAIgBAAg");
	this.shape.setTransform(174.3,-7.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAYAXIgGALIgIAHIgJAGQgEACgGAAQgIAAgFgDQgGgDgEgFQgEgEgCgGIgEgMIgBgOIgBgMIABgSIACgUIATAAIgCAUIgBAPIAAAIIABAJIACAKQABAFACAEQACAEAEACQADACAFgBQAGgBAHgIQAIgIAIgRIgBgWIAAgNIAAgHIATgCIABAbIAAAVIABARIAAANIABASIgUABg");
	this.shape_1.setTransform(162.5,-9.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSAwQgIgEgGgGQgHgHgDgKQgEgJAAgMQAAgLAEgJQADgKAHgGQAGgHAIgEQAJgEAJAAQAKAAAIAEQAJAEAGAHQAGAHAEAKQAEAJAAAKQAAALgEAKQgEAJgGAHQgGAHgJAEQgIAEgKAAQgJAAgJgEgAgMggQgFADgEAGQgEAFgBAGIgCAMQAAAGACAHQACAGADAFQAEAFAGAEQAEADAHAAQAHAAAGgDQAFgEAEgFQADgFACgGIACgNIgCgMQgBgGgEgFQgDgGgGgDQgGgDgHAAQgHAAgFADg");
	this.shape_2.setTransform(151.6,-9.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgnAVIgBgVIAAgQIgBgNIAAgTIAUAAIAAALIAAAKIAAALIAGgJIAJgJIAJgIQAFgDAGgBQAIAAAFADIAFAEIAFAGIADAIIACAMIgRAHIgBgIIgCgFIgCgEIgDgCQgDgCgDAAIgGACIgFAEIgFAGIgGAIIgFAHIgEAEIAAAOIAAALIABAKIAAAHIgTADIgBgcg");
	this.shape_3.setTransform(141.8,-9.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgSBJIgJgDQgEgBgEgDIgHgHQgDgEgCgGIARgHIAEAHIAFAEIAFADIAGACQAHABAFgBQAHgBAFgCIAHgGIAFgHIADgGIACgGIAAgEIAAgMQgFAFgGADIgLAEIgJACQgLAAgIgEQgJgEgHgGQgGgHgEgIQgDgKAAgMQAAgMAEgJQAEgJAGgHQAHgGAJgDQAIgEAJAAIAKADIAMAEQAGAEAFAFIAAgSIASABIgBBkIgCALIgEALQgDAGgEAEIgJAJIgMAGIgPADQgJAAgJgDgAgMg1QgGADgDAFQgFAEgBAGQgDAHAAAHQAAAHADAGQABAGAFAEQADAEAGACQAFADAHAAQAEAAAGgCQAFgCAFgEIAHgIQADgFACgGIAAgLQgCgHgDgFIgIgJQgEgDgGgCQgFgCgFAAQgGAAgFACg");
	this.shape_4.setTransform(130.1,-7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgTAvQgJgEgGgGQgGgGgDgJQgEgKAAgKQAAgLAEgJQAEgJAGgIQAHgGAJgFQAIgDAKAAQAJgBAIAEQAIAEAGAFQAGAHAFAJQAEAIABAKIhLAKQAAAHADAEQACAGAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAFgHACQgHACgIAAQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIgBIgHACg");
	this.shape_5.setTransform(114.6,-9.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXBDIAEgRIABgOIABgOQAAgKgDgGQgDgGgEgEQgEgEgFgCQgFgCgFAAQgEABgFACQgEACgFAFQgGADgDAIIgBA6IgRABIgCiHIAVAAIgBA1QAFgFAFgDIAKgFIAKgCQAKAAAIADQAIAEAGAGQAGAHADAJQADAJABAMIAAAMIgBANIgBAMIgCAKg");
	this.shape_6.setTransform(103.4,-11.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_7.setTransform(93.1,-11.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgoAWIgBgWIgBgQIAAgNIgBgSIAVgBIAAAZIAGgJIAIgIQAFgDAEgCQAEgCAGgBQAGAAAGABQAFACADADIAGAGIAEAIIACAIIAAAIIAAAdIAAAgIgSgBIABgcIAAgcIAAgEIgBgFIgCgGQgBgCgDgCIgFgEQgDgBgEABQgHABgGAJQgIAJgHARIAAAWIABANIAAAHIgTACIgBgbg");
	this.shape_8.setTransform(78.5,-9.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKBCIAAgWIABgcIAAglIARAAIAAAWIgBATIAAAPIAAAMIAAATgAgEgpIgEgDIgCgEIgBgEIABgFIACgEIAEgDIAEgBIAFABIAEADIACAEIACAFIgCAEIgCAEIgEADIgFABIgEgBg");
	this.shape_9.setTransform(70.8,-11.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAvQgJgEgGgGQgGgGgDgJQgEgKAAgKQAAgLAEgJQAEgJAGgIQAHgGAJgFQAIgDAKAAQAJgBAIAEQAIAEAGAFQAGAHAFAJQAEAIABAKIhLAKQAAAHADAEQACAGAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAFgHACQgHACgIAAQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIgBIgHACg");
	this.shape_10.setTransform(58.5,-9.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgnAVIgBgVIAAgQIgBgNIAAgTIAUAAIAAALIAAAKIAAALIAGgJIAJgJIAJgIQAFgDAGgBQAIAAAFADIAFAEIAFAGIADAIIACAMIgRAHIgBgIIgCgFIgCgEIgDgCQgDgCgDAAIgGACIgFAEIgFAGIgGAIIgFAHIgEAEIAAAOIAAALIABAKIAAAHIgTADIgBgcg");
	this.shape_11.setTransform(48.7,-9.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAYAXIgGALIgIAHIgJAGQgEACgGAAQgIAAgFgDQgGgDgEgFQgEgEgCgGIgEgMIgBgOIgBgMIABgSIACgUIATAAIgCAUIgBAPIAAAIIABAJIACAKQABAFACAEQACAEAEACQADACAFgBQAGgBAHgIQAIgIAIgRIgBgWIAAgNIAAgHIATgCIABAbIAAAVIABARIAAANIABASIgUABg");
	this.shape_12.setTransform(37.6,-9.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_13.setTransform(27.8,-11.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgPAwQgKgEgHgHQgHgHgEgJQgFgKAAgLQAAgFACgHQACgGADgGQADgFAFgFQAFgFAGgEQAFgDAHgCQAHgCAGAAQAIAAAHACQAGACAGADQAGAEAFAFIAIALIAAAAIgPAJIAAAAIgFgIIgIgGIgIgEIgKgCQgGAAgHADQgGADgEAFQgFAEgDAHQgDAGAAAGQAAAHADAHQADAGAFAFQAEAFAGACQAHADAGAAQAFAAAEgBQAEgBAEgDQAEgCADgDQAEgDACgEIAAgBIAQAJIgBAAQgDAGgFAFIgLAIIgMAFIgOABQgJAAgJgDg");
	this.shape_14.setTransform(18.1,-9.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgKBCIAAgWIAAgcIABglIARAAIAAAWIAAATIAAAPIgBAMIAAATgAgEgpIgEgDIgCgEIgBgEIABgFIACgEIAEgDIAEgBIAFABIAEADIACAEIACAFIgCAEIgCAEIgEADIgFABIgEgBg");
	this.shape_15.setTransform(10.5,-11.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgwhGIAUgBIgBAMQAFgEAGgDIAKgEIAKgCIAMACIAKAEQAGADAEAFQAFAEADAGQADAFACAHQABAGABAGQgBAIgCAHQgCAHgDAEQgDAFgFAFIgKAHIgJAGIgLABIgKgBIgLgFQgGgCgFgFIgBA8IgQAAgAAAg2QgFAAgFACQgEACgEADQgEADgCAEQgEAEgBAEIAAARQABAGAEAEQACAFAEACQADADAFACQAFABAEAAQAGAAAFgCQAFgCAFgEQAEgEACgFQACgHABgGQAAgGgCgGQgCgGgEgEQgEgFgFgDQgFgCgGAAIgBAAg");
	this.shape_16.setTransform(2.6,-7.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgTBJIgIgDQgEgBgEgDIgHgHQgDgEgCgGIARgHIAEAHIAFAEIAGADIAFACQAGABAHgBQAGgBAFgCIAHgGIAFgHIADgGIACgGIAAgEIAAgMQgFAFgGADIgLAEIgKACQgKAAgJgEQgIgEgHgGQgGgHgEgIQgDgKAAgMQAAgMADgJQAFgJAHgHQAGgGAIgDQAJgEAIAAIALADIAMAEQAGAEAFAFIAAgSIASABIgBBkIgBALIgFALQgDAGgEAEIgJAJIgMAGIgPADQgJAAgKgDgAgMg1QgGADgDAFQgEAEgDAGQgCAHAAAHQAAAHACAGQADAGAEAEQAEAEAFACQAFADAGAAQAGAAAFgCQAFgCAFgEIAHgIQAEgFABgGIAAgLQgBgHgEgFIgHgJQgFgDgGgCQgFgCgFAAQgGAAgFACg");
	this.shape_17.setTransform(-14.5,-7.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgoAWIgBgWIAAgQIgBgNIAAgSIAUgBIAAAZIAHgJIAHgIQAFgDAEgCQAEgCAGgBQAHAAAFABQAEACAEADIAGAGIAEAIIABAIIABAIIAAAdIAAAgIgTgBIACgcIAAgcIgBgEIgBgFIgBgGQgCgCgCgCIgFgEQgDgBgEABQgHABgGAJQgIAJgHARIAAAWIAAANIAAAHIgSACIgBgbg");
	this.shape_18.setTransform(-25.7,-9.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgLBCIABgWIAAgcIABglIARAAIAAAWIgBATIAAAPIAAAMIAAATgAgEgpIgEgDIgDgEIgBgEIABgFIADgEIAEgDIAEgBIAFABIAEADIADAEIABAFIgBAEIgDAEIgEADIgFABIgEgBg");
	this.shape_19.setTransform(-33.4,-11.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgMAxIgIgBIgIgCIgHgFIgIgEIAJgOIAKAFIAKAEIAKADIALABIAIgBIAGgCIADgDIABgDIABgEIgBgDIgDgEIgEgDIgHgCIgJgBIgPgBIgNgFQgFgDgEgDQgEgFgBgGQAAgHABgEQABgGAEgEIAGgHQAFgEAFgCIALgDIAKAAIAJAAIAJABIAKADIAKAFIgGARIgLgFIgKgDIgIgBQgNgBgHADQgJAEAAAIQAAAFADACQAEADAEABIAMACIANABQAJACAGACQAFABAFAEQADAEABAEQACAEAAAFQAAAIgDAGQgEAFgGAEQgFADgHACIgQACQgHAAgIgCg");
	this.shape_20.setTransform(-40.8,-9.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAxIgIgBIgIgCIgIgFIgHgEIAJgOIAKAFIAKAEIAKADIALABIAIgBIAGgCIADgDIABgDIABgEIgBgDIgDgEIgEgDIgHgCIgJgBIgPgBIgNgFQgFgDgFgDQgDgFgBgGQgBgHACgEQABgGAEgEIAGgHQAFgEAFgCIALgDIAKAAIAJAAIAJABIAKADIAKAFIgGARIgLgFIgKgDIgIgBQgNgBgHADQgJAEABAIQAAAFACACQADADAFABIAMACIANABQAJACAGACQAGABAEAEQADAEABAEQACAEAAAFQAAAIgDAGQgEAFgGAEQgFADgHACIgPACQgIAAgIgCg");
	this.shape_21.setTransform(-50.8,-9.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgKBCIAAgWIAAgcIABglIARAAIAAAWIgBATIAAAPIAAAMIAAATgAgEgpIgEgDIgCgEIgBgEIABgFIACgEIAEgDIAEgBIAFABIAEADIACAEIACAFIgCAEIgCAEIgEADIgFABIgEgBg");
	this.shape_22.setTransform(-57.9,-11.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAqAVIAAgTIAAgNQgBgIgCgDQgCgEgDAAIgFACIgEADIgEAFIgFAFIgDAGIgDAEIAAAIIAAALIABANIAAAQIgQABIgBgbIAAgTIgBgNQgBgIgCgDQgCgEgDAAIgFACIgEAEIgFAFIgFAGIgEAGIgDAEIABAuIgRABIgDhdIATgCIAAAZIAGgHIAHgHIAIgFQAEgCAFAAIAHABIAFAEQADACACAEIACAJIAGgHIAHgGQADgDAFgCQAEgCAFAAIAHABQAEACADADQADACABAFQACAEAAAGIABAQIABAUIABAfIgTABIAAgbg");
	this.shape_23.setTransform(-67.6,-9.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTAvQgJgEgGgGQgGgGgDgJQgEgKAAgKQAAgLAEgJQAEgJAGgIQAHgGAJgFQAIgDAKAAQAJgBAIAEQAIAEAGAFQAGAHAFAJQAEAIABAKIhLAKQAAAHADAEQACAGAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAFgHACQgHACgIAAQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIgBIgHACg");
	this.shape_24.setTransform(-84.8,-9.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAXBDIAEgRIABgOIABgOQAAgKgDgGQgDgGgEgEQgEgEgFgCQgFgCgFAAQgEABgFACQgEACgFAFQgGADgDAIIgBA6IgRABIgCiHIAVAAIgBA1QAFgFAFgDIAKgFIAKgCQAKAAAIADQAIAEAGAGQAGAHADAJQADAJABAMIAAAMIgBANIgBAMIgCAKg");
	this.shape_25.setTransform(-96,-11.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_26.setTransform(-106.3,-11.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_27.setTransform(-119.5,-11.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgPAwQgKgEgHgHQgHgHgEgJQgFgKAAgLQAAgFACgHQACgGADgGQADgFAFgFQAFgFAGgEQAFgDAHgCQAHgCAGAAQAIAAAHACQAGACAGADQAGAEAFAFIAIALIAAAAIgPAJIAAAAIgFgIIgIgGIgIgEIgKgCQgGAAgHADQgGADgEAFQgFAEgDAHQgDAGAAAGQAAAHADAHQADAGAFAFQAEAFAGACQAHADAGAAQAFAAAEgBQAEgBAEgDQAEgCADgDQAEgDACgEIAAgBIAQAJIgBAAQgDAGgFAFIgLAIIgMAFIgOABQgJAAgJgDg");
	this.shape_28.setTransform(-129.2,-9.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgTAvQgJgEgGgGQgGgGgDgJQgEgKAAgKQAAgLAEgJQAEgJAGgIQAHgGAJgFQAIgDAKAAQAJgBAIAEQAIAEAGAFQAGAHAFAJQAEAIABAKIhLAKQAAAHADAEQACAGAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAFgHACQgHACgIAAQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIgBIgHACg");
	this.shape_29.setTransform(-139.9,-9.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAFBMQgFgCgCgDQgDgDgDgEIgFgIQgEgKgBgNIAChvIASAAIgBAdIAAAYIgBATIAAAOIAAAYQAAAIABAGIADAGIAEAEIAEAEIAHABIgCASQgHgBgFgCg");
	this.shape_30.setTransform(-147.3,-12.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgTAvQgJgEgGgGQgGgGgDgJQgEgKAAgKQAAgLAEgJQAEgJAGgIQAHgGAJgFQAIgDAKAAQAJgBAIAEQAIAEAGAFQAGAHAFAJQAEAIABAKIhLAKQAAAHADAEQACAGAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAFgHACQgHACgIAAQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIgBIgHACg");
	this.shape_31.setTransform(-155.6,-9.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgKBHQgHgBgGgDIgNgHIgMgIIANgPQAGAHAHADIAMAFQAGADAFAAQAGABAGgCIAKgDIAGgHQACgDABgEQAAgEgBgCIgEgFIgHgEIgIgDIgIgDIgIgBIgKgCIgLgEIgKgGQgFgDgEgEQgDgGgCgGQgCgHABgIQAAgIADgFQACgGAEgEQAEgEAFgEQAFgDAGgCIALgDIALgBIARADIAHACIAIADIAIAFIAHAFIgKAQIgGgGIgGgEIgGgCIgGgDIgOgCQgHAAgGACQgGADgEAEQgFAEgCAEQgCAFAAAEQAAAFADAEQACAEAFAEQAFAEAGACQAHACAGABIANACIAMAEIALAFQAFAEADAEQADAFACAGQABAFgBAIQgBAFgCAFQgDAFgEAEQgDADgFADIgKADIgLACIgKABIgMgBg");
	this.shape_32.setTransform(-166.8,-11.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#990000").ss(3,1,1).p("A9bAjQAAA5A4AoQA3AoBPAAMA0+gADQBOAAA2goQA3gnAAg6IAAhCQgBg5g3goQg3gohOAAMg0/AADQhOAAg3AoQg2AoAAA5g");
	this.shape_33.setTransform(4.2,-9.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FF0033").s().p("A8jCEQg4goAAg5IAAhCQAAg5A2goQA3goBPAAMA0+gADQBOAAA3AoQA3AoABA5IAABCQABA6g4AnQg2AohOAAMg0+AADQhOAAg4gog");
	this.shape_34.setTransform(4.2,-9.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-188.9,-28.4,385.1,37.3), null);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy4();
	this.instance.parent = this;
	this.instance.setTransform(205.8,0.6,0.389,0.389,0,0,0,499.9,277.6);

	this.instance_1 = new lib.Symbol1copy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,0.6,0.389,0.389,0,0,0,499.9,277.6);

	this.instance_2 = new lib.Symbol1copy4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-202.1,0.6,0.389,0.389,0,0,0,499.9,277.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-303.5,-99.5,607.1,199.1);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol1copy7();
	this.instance.parent = this;
	this.instance.setTransform(152.7,0.7,0.5,0.5,0,0,0,499.6,277.4);

	this.instance_1 = new lib.Symbol1copy6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-148.3,0.7,0.5,0.5,0,0,0,499.6,277.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-278.5,-128,557.1,256.2);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy5();
	this.instance.parent = this;
	this.instance.setTransform(390.7,4,0.486,0.486,0,0,0,499.7,277.5);

	this.instance_1 = new lib.Symbol1copy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(135.8,4,0.486,0.486,0,0,0,499.8,277.5);

	this.instance_2 = new lib.Symbol1copy4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-119.2,4,0.486,0.486,0,0,0,499.8,277.5);

	this.instance_3 = new lib.Symbol1copy4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-374.2,4,0.486,0.486,0,0,0,499.8,277.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(255,255,255,0.698)").ss(8,1,1).p("Eg/UgTpMB+pAAAQHdAAFSFuQFRFuAAIGIAAAPQAAIGlRFuQlSFundAAMh+pAAAQndAAlSluQlRluAAoGIAAgPQAAoGFRluQFSluHdAAg");
	this.shape.setTransform(6.1,3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.647)").s().p("Eg/UATqQndAAlSluQlRluAAoGIAAgPQAAoGFRluQFSluHdAAMB+pAAAQHdAAFSFuQFRFuAAIGIAAAPQAAIGlRFuQlSFundAAg");
	this.shape_1.setTransform(6.1,3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-518.4,-126.8,1049.1,259.6);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(116.4,3.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03,y:3.3},9).to({scaleX:1,scaleY:1,y:3.4},10).to({scaleX:1.03,scaleY:1.03,y:3.3},10).to({scaleX:1,scaleY:1,y:3.4},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-182.5,-34,598,73.8);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(495.1,276);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1,y:275.9},10).to({scaleX:1,scaleY:1,y:276},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(277.8,58.6,434.7,434.7);


(lib.Symbol1copy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.questiontext_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Tween12("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(5.6,-11.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(946.9,299.5,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(288).to({_off:false},0).wait(33).to({startPosition:13},0).to({alpha:0,startPosition:1},8).wait(1));

	// Layer_11
	this.instance_1 = new lib.Tween11("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(948.5,304.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(273).to({_off:false},0).to({scaleX:1.26,scaleY:1.26,x:948.4},15).to({startPosition:0},19).to({alpha:0},8).wait(15));

	// Layer_9
	this.instance_2 = new lib.Symbol1copy("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(887.8,641.8,1,1,-14.2,0,0,190.6,64.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(196).to({_off:false},0).to({_off:true},30).wait(104));

	// Layer_8
	this.instance_3 = new lib.Symbol1copy10("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(801.2,386.6,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(166).to({_off:false},0).to({_off:true},30).wait(134));

	// Layer_2
	this.instance_4 = new lib.Tween6("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(791,583.5);
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween11("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(951.5,304.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(226).to({_off:false},0).to({_off:true,x:951.5,y:304.1},25).wait(79));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(226).to({_off:false},25).wait(56).to({x:948.5},0).to({alpha:0},7).to({_off:true},1).wait(15));

	// Layer_10
	this.instance_6 = new lib.Symbol2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(953.4,298);
	this.instance_6.alpha = 0.5;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(121).to({_off:false},0).wait(27).to({alpha:0},6).to({_off:true},1).wait(175));

	// Layer_12
	this.instance_7 = new lib.Symbol1copy16();
	this.instance_7.parent = this;
	this.instance_7.setTransform(951.3,300.6,0.394,0.394,0,0,0,499.8,277.7);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(81).to({_off:false},0).to({_off:true},83).wait(166));

	// Layer_5
	this.instance_8 = new lib.Symbol5("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(641.8,127.6);

	this.instance_9 = new lib.questiontext_mccopy2("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(738.1,152,2.282,2.282,0,0,0,-1.4,-2.3);

	this.questiontxt = new lib.questiontext_mccopy2();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(738.1,152,2.282,2.282,0,0,0,-1.4,-2.3);
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9},{t:this.instance_8}]},81).to({state:[]},85).to({state:[{t:this.questiontxt}]},141).to({state:[{t:this.questiontxt}]},7).to({state:[]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(307).to({_off:false},0).to({alpha:0},7).to({_off:true},1).wait(15));

	// Layer_4
	this.instance_10 = new lib.Tween5("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,583.4);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(61).to({_off:false},0).to({alpha:1},7).to({startPosition:0},239).to({alpha:0},7).to({_off:true},1).wait(15));

	// Layer_3
	this.instance_11 = new lib.Tween4("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,296.6,0.8,0.8);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.instance_12 = new lib.Symbol1copy4();
	this.instance_12.parent = this;
	this.instance_12.setTransform(748.6,299.8,0.389,0.389,0,0,0,499.9,277.6);

	this.instance_13 = new lib.Symbol1copy4();
	this.instance_13.parent = this;
	this.instance_13.setTransform(544.6,299.8,0.389,0.389,0,0,0,499.9,277.6);

	this.instance_14 = new lib.Symbol1copy4();
	this.instance_14.parent = this;
	this.instance_14.setTransform(340.7,299.8,0.389,0.389,0,0,0,499.9,277.6);

	this.instance_15 = new lib.Symbol1copy13();
	this.instance_15.parent = this;
	this.instance_15.setTransform(952.5,299.8,0.389,0.389,0,0,0,499.8,277.6);

	this.instance_16 = new lib.Tween13("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(542.8,299.2);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11}]},36).to({state:[{t:this.instance_11}]},7).to({state:[{t:this.instance_14},{t:this.instance_13},{t:this.instance_12}]},78).to({state:[{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_15}]},34).to({state:[{t:this.instance_14},{t:this.instance_13},{t:this.instance_12}]},96).to({state:[{t:this.instance_16}]},56).to({state:[{t:this.instance_16}]},7).to({state:[]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(36).to({_off:false},0).to({alpha:1},7).to({_off:true},78).wait(209));
	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(307).to({_off:false},0).to({alpha:0},7).to({_off:true},1).wait(15));

	// Layer_6
	this.questiontxt_1 = new lib.questiontext_mc();
	this.questiontxt_1.name = "questiontxt_1";
	this.questiontxt_1.parent = this;
	this.questiontxt_1.setTransform(630.2,149.8,2.329,2.329,0,0,0,-1.4,-2.4);
	this.questiontxt_1.alpha = 0;
	this.questiontxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt_1).wait(16).to({_off:false},0).to({alpha:1},6).to({regY:-2.3,y:150},59).to({regY:-2.4,y:149.8},226).to({alpha:0},7).to({_off:true},1).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(559.9,299.8,1449,849);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;