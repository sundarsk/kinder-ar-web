(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhyCJQANACAMgDQALgCAKgFQALgGAIgHIAPgQIAMgRIAJgQIhZjtIAxgGIA+C6IALgkIAPguIAQg0IAQg1IAvAAIgUA9IgRAxIgNAoIgKAdIgRAvIgJAaQgHAOgIAOQgHAOgLAOQgLAMgOALQgOAKgTAHQgTAGgXABg");
	this.shape.setTransform(347.2,11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_1.setTransform(323.1,5.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AAOC5QgMgFgIgHQgJgJgGgJQgGgKgEgLQgLgYgDgfIAHkMIArAAIgCBFIgCA6IgBAuIAAAjIgBA5QAAAUAFAQQABAGAEAGQAEAHAEAFQAFAGAHADQAIADAIAAIgFAqQgPgBgLgEg");
	this.shape_2.setTransform(305.3,-2.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AgaCgIABg3IABhCIABhaIArgCIgBA3IgBAuIgBAlIAAAdIgBAugAgLhkQgFgDgEgEQgFgEgCgFQgCgGAAgGQAAgGACgGQACgFAFgEQAEgFAFgCQAFgDAGAAQAGAAAFADQAGACAEAFQAEAEADAFQACAGAAAGQAAAGgCAGQgDAFgEAEQgEAEgGADQgFACgGAAQgGAAgFgCg");
	this.shape_3.setTransform(292.7,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("ABmAzIAAguIgBgiQgBgRgFgJQgFgIgIAAQgFAAgFADIgLAIIgLAMIgKANIgJANIgHAMIABATIABAaIAAAgIAAAoIgoACIgBhCIgCguIgCgiQAAgRgFgJQgFgIgIAAQgGAAgGADQgFAEgGAGQgGAFgGAIIgKAPIgKAOIgIALIACBwIgqACIgGjkIAtgFIAAA+IAPgRQAHgJAJgIQAJgHAKgFQALgFAMAAQAJAAAIADQAJADAFAGQAHAGAFAJQAFAJABANIAOgRQAHgIAJgHQAIgIAKgEQAKgFAMAAQAKAAAJADQAJAEAHAGQAGAHAFALQAEAKABAPIACAnIACAyIABBLIgtACIAAhCg");
	this.shape_4.setTransform(269.3,4.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AgeB4IgSgEIgUgHQgJgDgKgGQgKgFgJgHIAVggIAYAMIAZAKQAMAEAOADQAMACAOAAQAMAAAIgCIAOgFQAFgEADgEIAEgIIABgHQgBgFgCgEQgCgEgEgEQgEgEgHgEQgHgDgJgCQgKgCgOgBQgSgBgRgDQgSgDgOgHQgOgGgJgKQgJgLgCgQQgBgPADgNQAEgMAHgKQAHgKAKgIQALgHAMgFQAMgFAOgDQAOgCANAAIATABQAMAAAMADQAMADANAEQAMAFALAIIgPAnQgOgIgMgEIgXgGIgVgEQgggCgTAJQgTAJAAASQAAANAHAGQAHAGAMACQANADAQABIAhADQAVAEAOAGQAOAFAJAJQAJAIAEALQAEAKAAALQAAAUgJAOQgIAOgNAJQgOAIgSAEQgSAEgTABQgSAAgUgEg");
	this.shape_5.setTransform(239.5,5.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AguCxQgKgCgKgFQgKgEgJgHQgJgHgIgKQgIgJgFgOIApgRQAEAJAGAGQAFAHAHAEQAGAFAHADIAOAEQAPADARgCQAOgDALgGQALgGAIgIQAIgIAEgIQAFgIADgIIAEgOIABgKIAAgbQgNAMgOAGQgOAHgLADQgOADgMABQgZAAgVgJQgVgIgQgQQgQgRgJgWQgJgWAAgdQAAgdAKgXQAKgWARgQQAQgPAUgIQAVgJAVAAQANACAPAFQAMAEAPAHQAOAIANAMIAAgqIAqABIgCDzQAAAOgDAOQgEAOgGANQgHAMgKAMQgKALgNAJQgNAJgQAGQgPAFgTACIgDAAQgYAAgVgGgAggiBQgNAHgJALQgJALgFAPQgFAPAAARQAAASAFAOQAFAPAKAKQAJAKANAGQANAGAQAAQANAAANgFQANgFALgJQALgHAHgNQAIgMACgPIAAgbQgCgQgIgMQgHgMgLgJQgLgJgOgFQgNgFgNAAQgQAAgNAGg");
	this.shape_6.setTransform(201.2,9.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AhjA1IgCg1IgBgoIgBgfIgBgtIAwgBIABA9QAHgMAJgLQAJgLAKgIQALgIALgFQALgFANgBQARgBAMAEQAMADAIAHQAJAHAFAJQAGAKADAJIAEAUIACASQABAjAAAlIgCBNIgrgBIADhFQABgigBgiIgBgKIgCgNIgFgNQgEgHgFgFQgFgFgHgCQgHgDgKACQgRACgQAWQgSAWgUApIACA3IABAfIAAAQIgtAGIgDhCg");
	this.shape_7.setTransform(174.1,4.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgaCgIABg3IABhCIABhaIArgCIgBA3IgBAuIgBAlIAAAdIgBAugAgLhkQgFgDgEgEQgFgEgCgFQgCgGAAgGQAAgGACgGQACgFAFgEQAEgFAFgCQAFgDAGAAQAGAAAFADQAGACAEAFQAEAEADAFQACAGAAAGQAAAGgCAGQgDAFgEAEQgEAEgGADQgFACgGAAQgGAAgFgCg");
	this.shape_8.setTransform(155.6,-0.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AA5CiQAGgVADgTIADgjIABgiQgBgWgHgQQgHgPgJgKQgKgLgMgEQgMgEgMgBQgLABgMAHQgLAEgMAMQgMAJgKAUIgCCNIgpABIgElIIAxgBIgBCBQALgNAOgHQANgHALgEQANgEAMgCQAYAAAUAIQATAJAOAQQAOAQAJAXQAIAWABAcIAAAfIgCAfIgDAdQgCANgEALg");
	this.shape_9.setTransform(135.8,-0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgmByQgWgJgSgQQgRgRgLgXQgLgWAAgbQAAgPAFgPQAEgPAIgOQAIgOALgMQAMgLAOgJQANgIAQgFQAQgFARAAQATAAARAFQAQAEAOAJQAOAIALAMQAMAMAHAQIABAAIgjAWIgBgBQgFgLgIgJQgIgIgJgGQgLgHgKgDQgMgDgMAAQgRAAgPAHQgOAGgMAMQgMALgGAQQgHAPABAQQgBASAHAPQAGAPAMAMQAMALAOAHQAPAGARAAQALAAALgDQAKgDAKgFQAKgFAIgIQAHgIAGgKIAAgBIAlAVIAAACQgIANgMALQgMALgOAIQgPAIgPAFQgRAEgRAAQgXAAgWgKg");
	this.shape_10.setTransform(108.2,5.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgZgYIhKACIABgnIBKgCIAChdIApgDIgCBfIBTgDIgEAoIhPACIgCC4IgsABg");
	this.shape_11.setTransform(84.6,0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("ABCB5IADgeQgaARgZAHQgYAIgWAAQgPAAgOgEQgOgEgLgIQgLgJgHgMQgHgNAAgRQAAgRAFgOQAFgOAJgKQAJgJAMgIQANgHAOgFQANgFAQgCQAOgCAPAAQAOAAAMABIAVADIgGgVQgFgKgFgJQgHgIgJgFQgJgFgNAAQgIAAgKADQgKACgNAGQgNAHgPALQgPALgSARIgZgfQAVgTASgMQASgMARgHQARgHAOgDQANgCALAAQAUAAAPAHQAPAGAMAMQALALAJAQQAHAQAFASQAFASADATQACASAAATQAAAVgCAXQgDAYgFAbgAgJAAQgSADgNAIQgOAJgGAMQgHANAEAPQADANAJAFQAJAFAMAAQAMAAAPgEQANgEAOgGIAbgNIAVgMIAAgVIgBgWQgKgCgLgCIgYgCQgTAAgQAFg");
	this.shape_12.setTransform(60,4.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("ABmAzIAAguIgBgiQgBgRgFgJQgFgIgIAAQgFAAgFADIgLAIIgLAMIgKANIgJANIgHAMIABATIABAaIAAAgIAAAoIgoACIgBhCIgCguIgCgiQAAgRgFgJQgFgIgIAAQgGAAgGADQgFAEgGAGQgGAFgGAIIgKAPIgKAOIgIALIACBwIgqACIgGjkIAtgFIAAA+IAPgRQAHgJAJgIQAJgHAKgFQALgFAMAAQAJAAAIADQAJADAFAGQAHAGAFAJQAFAJABANIAOgRQAHgIAJgHQAIgIAKgEQAKgFAMAAQAKAAAJADQAJAEAHAGQAGAHAFALQAEAKABAPIACAnIACAyIABBLIgtACIAAhCg");
	this.shape_13.setTransform(29.4,4.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgZgYIhKACIABgnIBKgCIAChdIAogDIgBBfIBSgDIgCAoIhQACIgCC4IgrABg");
	this.shape_14.setTransform(-9.7,0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AgmByQgWgJgSgQQgSgRgKgXQgLgWAAgbQAAgPAEgPQAFgPAIgOQAIgOALgMQAMgLAOgJQANgIAQgFQAQgFARAAQATAAARAFQAQAEAOAJQAOAIALAMQAMAMAHAQIABAAIgjAWIgBgBQgFgLgIgJQgIgIgJgGQgLgHgLgDQgKgDgNAAQgRAAgPAHQgOAGgMAMQgMALgGAQQgHAPABAQQgBASAHAPQAGAPAMAMQAMALAOAHQAPAGARAAQALAAALgDQALgDAJgFQAKgFAHgIQAJgIAFgKIAAgBIAlAVIAAACQgIANgMALQgMALgOAIQgPAIgPAFQgRAEgRAAQgXAAgWgKg");
	this.shape_15.setTransform(-33.2,5.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("ABCB5IADgeQgaARgZAHQgYAIgWAAQgPAAgPgEQgNgEgMgIQgLgJgGgMQgHgNAAgRQAAgRAFgOQAFgOAJgKQAJgJAMgIQAMgHAPgFQANgFAPgCQAOgCAQAAQAOAAAMABIAWADIgIgVQgEgKgFgJQgHgIgJgFQgJgFgNAAQgIAAgKADQgLACgNAGQgMAHgPALQgPALgSARIgZgfQAVgTASgMQATgMARgHQAQgHAOgDQANgCALAAQAUAAAPAHQAPAGAMAMQALALAIAQQAJAQAEASQAGASACATQACASAAATQAAAVgDAXQgBAYgGAbgAgKAAQgRADgNAIQgNAJgHAMQgGANADAPQAEANAIAFQAJAFALAAQANAAAOgEQAOgEAOgGIAagNIAWgMIABgVIgCgWQgKgCgMgCIgXgCQgSAAgSAFg");
	this.shape_16.setTransform(-60.7,4.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AACAgIg/BTIgtgMIBShnIhUhmIAsgLIBBBRIBAhSIAoAPIhNBjIBRBkIgnAPg");
	this.shape_17.setTransform(-85.6,4.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_18.setTransform(-110,5.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_19.setTransform(-147.1,5.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AA5CiQAGgVADgTIADgjIABgiQgBgWgHgQQgHgPgJgKQgKgLgMgEQgMgEgMgBQgLABgMAHQgLAEgMAMQgMAJgKAUIgCCNIgpABIgElIIAxgBIgBCBQALgNAOgHQANgHALgEQANgEAMgCQAYAAAUAIQATAJAOAQQAOAQAJAXQAIAWABAcIAAAfIgCAfIgDAdQgCANgEALg");
	this.shape_20.setTransform(-174.2,-0.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("AgZgYIhJACIAAgnIBKgCIABhdIAqgDIgBBfIBSgDIgDAoIhQACIgCC4IgrABg");
	this.shape_21.setTransform(-199,0.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("AgZgYIhJACIAAgnIBKgCIABhdIAqgDIgBBfIBSgDIgDAoIhQACIgCC4IgrABg");
	this.shape_22.setTransform(-231,0.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#660000").s().p("AgmByQgWgJgSgQQgSgRgKgXQgLgWAAgbQAAgPAEgPQAFgPAIgOQAIgOAMgMQAKgLAOgJQAOgIARgFQAQgFAQAAQASAAARAFQARAEAOAJQAOAIAMAMQALAMAIAQIAAAAIgjAWIAAgBQgGgLgIgJQgIgIgKgGQgJgHgMgDQgLgDgMAAQgQAAgPAHQgQAGgLAMQgLALgHAQQgGAPgBAQQABASAGAPQAHAPALAMQALALAQAHQAPAGAQAAQAMAAAKgDQAKgDAKgFQAKgFAHgIQAJgIAFgKIABgBIAlAVIgBACQgIANgMALQgMALgOAIQgPAIgQAFQgQAEgRAAQgXAAgWgKg");
	this.shape_23.setTransform(-254.5,5.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_24.setTransform(-280.5,5.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#660000").s().p("AAOC5QgMgFgIgHQgJgJgGgJQgGgKgEgLQgLgYgDgfIAGkMIAsAAIgCBFIgCA6IgBAuIAAAjIgCA5QABAUAFAQQACAGADAGQADAHAFAFQAGAGAHADQAHADAIAAIgFAqQgPgBgLgEg");
	this.shape_25.setTransform(-298.4,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_26.setTransform(-318.6,5.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#660000").s().p("AgcCrQgQgEgPgGQgPgGgPgJQgPgJgPgKIAfglQARAPAQAIQAQAJANAEQAPAFANACQAPABANgEQAOgDAKgGQAJgGAGgIQAGgJABgJQABgIgEgGQgDgHgGgFQgHgGgJgEIgTgHIgVgGIgUgEIgZgGQgNgDgNgGQgNgGgMgIQgLgIgJgMQgIgMgEgQQgFgQACgUQABgRAGgOQAGgOAJgKQAJgLAMgIQAMgIAOgFQANgFAPgCQAOgCAOAAQAUABAUAFIASAFIATAIQAKAFAJAGQAJAGAJAIIgYAmQgHgIgHgFIgQgKIgPgIIgPgEQgQgFgQgBQgSAAgPAGQgPAGgLAJQgKAKgFALQgGALAAALQAAALAHAKQAGALAMAIQALAIAQAGQAPAGARACQAQACAPAEQAPADAOAFQAOAGAMAJQAMAIAIALQAIALAEAOQAEAOgDAQQgCAPgGAMQgGALgKAJQgJAIgMAGQgLAGgNADQgNAEgNACIgYABQgQAAgQgDg");
	this.shape_27.setTransform(-345.7,0.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FF6600").ss(3,1,1).p("EA0xgGPMhpeAAAQiOAAhkBxQhlBxAAChIAAAYQAAChBlByQBkBxCOAAMBpeAAAQCNAABjhxQBkhyAAihIAAgYQAAihhkhxQhjhxiNAAg");

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFCC00").s().p("Eg0tAGQQiOAAhjhyQhmhxABihIAAgYQgBihBmhwQBjhyCOAAMBpeAAAQCNAABjByQBjBwAAChIAAAYQAAChhjBxQhjByiNAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-373.1,-41.5,746.3,83);


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhyCJQANACAMgDQALgCAKgFQALgGAIgHIAPgQIAMgRIAJgQIhZjtIAxgGIA+C6IALgkIAPguIAQg0IAQg1IAvAAIgUA9IgRAxIgNAoIgKAdIgRAvIgJAaQgHAOgIAOQgHAOgLAOQgLAMgOALQgOAKgTAHQgTAGgXABg");
	this.shape.setTransform(347.2,11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_1.setTransform(323.1,5.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AAOC5QgMgFgIgHQgJgJgGgJQgGgKgEgLQgLgYgDgfIAHkMIArAAIgCBFIgCA6IgBAuIAAAjIgBA5QAAAUAFAQQABAGAEAGQAEAHAEAFQAFAGAHADQAIADAIAAIgFAqQgPgBgLgEg");
	this.shape_2.setTransform(305.3,-2.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AgaCgIABg3IABhCIABhaIArgCIgBA3IgBAuIgBAlIAAAdIgBAugAgLhkQgFgDgEgEQgFgEgCgFQgCgGAAgGQAAgGACgGQACgFAFgEQAEgFAFgCQAFgDAGAAQAGAAAFADQAGACAEAFQAEAEADAFQACAGAAAGQAAAGgCAGQgDAFgEAEQgEAEgGADQgFACgGAAQgGAAgFgCg");
	this.shape_3.setTransform(292.7,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("ABmAzIAAguIgBgiQgBgRgFgJQgFgIgIAAQgFAAgFADIgLAIIgLAMIgKANIgJANIgHAMIABATIABAaIAAAgIAAAoIgoACIgBhCIgCguIgCgiQAAgRgFgJQgFgIgIAAQgGAAgGADQgFAEgGAGQgGAFgGAIIgKAPIgKAOIgIALIACBwIgqACIgGjkIAtgFIAAA+IAPgRQAHgJAJgIQAJgHAKgFQALgFAMAAQAJAAAIADQAJADAFAGQAHAGAFAJQAFAJABANIAOgRQAHgIAJgHQAIgIAKgEQAKgFAMAAQAKAAAJADQAJAEAHAGQAGAHAFALQAEAKABAPIACAnIACAyIABBLIgtACIAAhCg");
	this.shape_4.setTransform(269.3,4.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AgeB4IgSgEIgUgHQgJgDgKgGQgKgFgJgHIAVggIAYAMIAZAKQAMAEAOADQAMACAOAAQAMAAAIgCIAOgFQAFgEADgEIAEgIIABgHQgBgFgCgEQgCgEgEgEQgEgEgHgEQgHgDgJgCQgKgCgOgBQgSgBgRgDQgSgDgOgHQgOgGgJgKQgJgLgCgQQgBgPADgNQAEgMAHgKQAHgKAKgIQALgHAMgFQAMgFAOgDQAOgCANAAIATABQAMAAAMADQAMADANAEQAMAFALAIIgPAnQgOgIgMgEIgXgGIgVgEQgggCgTAJQgTAJAAASQAAANAHAGQAHAGAMACQANADAQABIAhADQAVAEAOAGQAOAFAJAJQAJAIAEALQAEAKAAALQAAAUgJAOQgIAOgNAJQgOAIgSAEQgSAEgTABQgSAAgUgEg");
	this.shape_5.setTransform(239.5,5.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AguCxQgKgCgKgFQgKgEgJgHQgJgHgIgKQgIgJgFgOIApgRQAEAJAGAGQAFAHAHAEQAGAFAHADIAOAEQAPADARgCQAOgDALgGQALgGAIgIQAIgIAEgIQAFgIADgIIAEgOIABgKIAAgbQgNAMgOAGQgOAHgLADQgOADgMABQgZAAgVgJQgVgIgQgQQgQgRgJgWQgJgWAAgdQAAgdAKgXQAKgWARgQQAQgPAUgIQAVgJAVAAQANACAPAFQAMAEAPAHQAOAIANAMIAAgqIAqABIgCDzQAAAOgDAOQgEAOgGANQgHAMgKAMQgKALgNAJQgNAJgQAGQgPAFgTACIgDAAQgYAAgVgGgAggiBQgNAHgJALQgJALgFAPQgFAPAAARQAAASAFAOQAFAPAKAKQAJAKANAGQANAGAQAAQANAAANgFQANgFALgJQALgHAHgNQAIgMACgPIAAgbQgCgQgIgMQgHgMgLgJQgLgJgOgFQgNgFgNAAQgQAAgNAGg");
	this.shape_6.setTransform(201.2,9.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AhjA1IgCg1IgBgoIgBgfIgBgtIAwgBIABA9QAHgMAJgLQAJgLAKgIQALgIALgFQALgFANgBQARgBAMAEQAMADAIAHQAJAHAFAJQAGAKADAJIAEAUIACASQABAjAAAlIgCBNIgrgBIADhFQABgigBgiIgBgKIgCgNIgFgNQgEgHgFgFQgFgFgHgCQgHgDgKACQgRACgQAWQgSAWgUApIACA3IABAfIAAAQIgtAGIgDhCg");
	this.shape_7.setTransform(174.1,4.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgaCgIABg3IABhCIABhaIArgCIgBA3IgBAuIgBAlIAAAdIgBAugAgLhkQgFgDgEgEQgFgEgCgFQgCgGAAgGQAAgGACgGQACgFAFgEQAEgFAFgCQAFgDAGAAQAGAAAFADQAGACAEAFQAEAEADAFQACAGAAAGQAAAGgCAGQgDAFgEAEQgEAEgGADQgFACgGAAQgGAAgFgCg");
	this.shape_8.setTransform(155.6,-0.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AA5CiQAGgVADgTIADgjIABgiQgBgWgHgQQgHgPgJgKQgKgLgMgEQgMgEgMgBQgLABgMAHQgLAEgMAMQgMAJgKAUIgCCNIgpABIgElIIAxgBIgBCBQALgNAOgHQANgHALgEQANgEAMgCQAYAAAUAIQATAJAOAQQAOAQAJAXQAIAWABAcIAAAfIgCAfIgDAdQgCANgEALg");
	this.shape_9.setTransform(135.8,-0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgmByQgWgJgSgQQgRgRgLgXQgLgWAAgbQAAgPAFgPQAEgPAIgOQAIgOALgMQAMgLAOgJQANgIAQgFQAQgFARAAQATAAARAFQAQAEAOAJQAOAIALAMQAMAMAHAQIABAAIgjAWIgBgBQgFgLgIgJQgIgIgJgGQgLgHgKgDQgMgDgMAAQgRAAgPAHQgOAGgMAMQgMALgGAQQgHAPABAQQgBASAHAPQAGAPAMAMQAMALAOAHQAPAGARAAQALAAALgDQAKgDAKgFQAKgFAIgIQAHgIAGgKIAAgBIAlAVIAAACQgIANgMALQgMALgOAIQgPAIgPAFQgRAEgRAAQgXAAgWgKg");
	this.shape_10.setTransform(108.2,5.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgZgYIhKACIABgnIBKgCIAChdIApgDIgCBfIBTgDIgEAoIhPACIgCC4IgsABg");
	this.shape_11.setTransform(84.6,0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("ABCB5IADgeQgaARgZAHQgYAIgWAAQgPAAgOgEQgOgEgLgIQgLgJgHgMQgHgNAAgRQAAgRAFgOQAFgOAJgKQAJgJAMgIQANgHAOgFQANgFAQgCQAOgCAPAAQAOAAAMABIAVADIgGgVQgFgKgFgJQgHgIgJgFQgJgFgNAAQgIAAgKADQgKACgNAGQgNAHgPALQgPALgSARIgZgfQAVgTASgMQASgMARgHQARgHAOgDQANgCALAAQAUAAAPAHQAPAGAMAMQALALAJAQQAHAQAFASQAFASADATQACASAAATQAAAVgCAXQgDAYgFAbgAgJAAQgSADgNAIQgOAJgGAMQgHANAEAPQADANAJAFQAJAFAMAAQAMAAAPgEQANgEAOgGIAbgNIAVgMIAAgVIgBgWQgKgCgLgCIgYgCQgTAAgQAFg");
	this.shape_12.setTransform(60,4.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("ABmAzIAAguIgBgiQgBgRgFgJQgFgIgIAAQgFAAgFADIgLAIIgLAMIgKANIgJANIgHAMIABATIABAaIAAAgIAAAoIgoACIgBhCIgCguIgCgiQAAgRgFgJQgFgIgIAAQgGAAgGADQgFAEgGAGQgGAFgGAIIgKAPIgKAOIgIALIACBwIgqACIgGjkIAtgFIAAA+IAPgRQAHgJAJgIQAJgHAKgFQALgFAMAAQAJAAAIADQAJADAFAGQAHAGAFAJQAFAJABANIAOgRQAHgIAJgHQAIgIAKgEQAKgFAMAAQAKAAAJADQAJAEAHAGQAGAHAFALQAEAKABAPIACAnIACAyIABBLIgtACIAAhCg");
	this.shape_13.setTransform(29.4,4.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgZgYIhKACIABgnIBKgCIAChdIAogDIgBBfIBSgDIgCAoIhQACIgCC4IgrABg");
	this.shape_14.setTransform(-9.7,0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AgmByQgWgJgSgQQgSgRgKgXQgLgWAAgbQAAgPAEgPQAFgPAIgOQAIgOALgMQAMgLAOgJQANgIAQgFQAQgFARAAQATAAARAFQAQAEAOAJQAOAIALAMQAMAMAHAQIABAAIgjAWIgBgBQgFgLgIgJQgIgIgJgGQgLgHgLgDQgKgDgNAAQgRAAgPAHQgOAGgMAMQgMALgGAQQgHAPABAQQgBASAHAPQAGAPAMAMQAMALAOAHQAPAGARAAQALAAALgDQALgDAJgFQAKgFAHgIQAJgIAFgKIAAgBIAlAVIAAACQgIANgMALQgMALgOAIQgPAIgPAFQgRAEgRAAQgXAAgWgKg");
	this.shape_15.setTransform(-33.2,5.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("ABCB5IADgeQgaARgZAHQgYAIgWAAQgPAAgPgEQgNgEgMgIQgLgJgGgMQgHgNAAgRQAAgRAFgOQAFgOAJgKQAJgJAMgIQAMgHAPgFQANgFAPgCQAOgCAQAAQAOAAAMABIAWADIgIgVQgEgKgFgJQgHgIgJgFQgJgFgNAAQgIAAgKADQgLACgNAGQgMAHgPALQgPALgSARIgZgfQAVgTASgMQATgMARgHQAQgHAOgDQANgCALAAQAUAAAPAHQAPAGAMAMQALALAIAQQAJAQAEASQAGASACATQACASAAATQAAAVgDAXQgBAYgGAbgAgKAAQgRADgNAIQgNAJgHAMQgGANADAPQAEANAIAFQAJAFALAAQANAAAOgEQAOgEAOgGIAagNIAWgMIABgVIgCgWQgKgCgMgCIgXgCQgSAAgSAFg");
	this.shape_16.setTransform(-60.7,4.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AACAgIg/BTIgtgMIBShnIhUhmIAsgLIBBBRIBAhSIAoAPIhNBjIBRBkIgnAPg");
	this.shape_17.setTransform(-85.6,4.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_18.setTransform(-110,5.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_19.setTransform(-147.1,5.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AA5CiQAGgVADgTIADgjIABgiQgBgWgHgQQgHgPgJgKQgKgLgMgEQgMgEgMgBQgLABgMAHQgLAEgMAMQgMAJgKAUIgCCNIgpABIgElIIAxgBIgBCBQALgNAOgHQANgHALgEQANgEAMgCQAYAAAUAIQATAJAOAQQAOAQAJAXQAIAWABAcIAAAfIgCAfIgDAdQgCANgEALg");
	this.shape_20.setTransform(-174.2,-0.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("AgZgYIhJACIAAgnIBKgCIABhdIAqgDIgBBfIBSgDIgDAoIhQACIgCC4IgrABg");
	this.shape_21.setTransform(-199,0.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("AgZgYIhJACIAAgnIBKgCIABhdIAqgDIgBBfIBSgDIgDAoIhQACIgCC4IgrABg");
	this.shape_22.setTransform(-231,0.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#660000").s().p("AgmByQgWgJgSgQQgSgRgKgXQgLgWAAgbQAAgPAEgPQAFgPAIgOQAIgOAMgMQAKgLAOgJQAOgIARgFQAQgFAQAAQASAAARAFQARAEAOAJQAOAIAMAMQALAMAIAQIAAAAIgjAWIAAgBQgGgLgIgJQgIgIgKgGQgJgHgMgDQgLgDgMAAQgQAAgPAHQgQAGgLAMQgLALgHAQQgGAPgBAQQABASAGAPQAHAPALAMQALALAQAHQAPAGAQAAQAMAAAKgDQAKgDAKgFQAKgFAHgIQAJgIAFgKIABgBIAlAVIgBACQgIANgMALQgMALgOAIQgPAIgQAFQgQAEgRAAQgXAAgWgKg");
	this.shape_23.setTransform(-254.5,5.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_24.setTransform(-280.5,5.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#660000").s().p("AAOC5QgMgFgIgHQgJgJgGgJQgGgKgEgLQgLgYgDgfIAGkMIAsAAIgCBFIgCA6IgBAuIAAAjIgCA5QABAUAFAQQACAGADAGQADAHAFAFQAGAGAHADQAHADAIAAIgFAqQgPgBgLgEg");
	this.shape_25.setTransform(-298.4,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#660000").s().p("AgvByQgWgIgPgQQgPgPgHgXQgIgWAAgaQAAgaAJgXQAJgXAQgRQAPgRAWgKQAVgKAYABQAWgBATAJQAUAJAPAPQAPAPAKAUQAJAVADAYIi3AaQABAPAGANQAGAMAKAIQAJAIAMAFQAMAEAOAAQALAAALgDQALgEAJgGQAKgIAHgJQAGgLAEgNIApAIQgGAUgLAQQgLAPgOALQgPAMgRAGQgSAGgSABQgbAAgWgJgAgRhSQgLADgKAHQgKAIgHAMQgJANgDATIB/gQIgBgEQgIgWgOgLQgOgMgVAAQgIAAgLADg");
	this.shape_26.setTransform(-318.6,5.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#660000").s().p("AgcCrQgQgEgPgGQgPgGgPgJQgPgJgPgKIAfglQARAPAQAIQAQAJANAEQAPAFANACQAPABANgEQAOgDAKgGQAJgGAGgIQAGgJABgJQABgIgEgGQgDgHgGgFQgHgGgJgEIgTgHIgVgGIgUgEIgZgGQgNgDgNgGQgNgGgMgIQgLgIgJgMQgIgMgEgQQgFgQACgUQABgRAGgOQAGgOAJgKQAJgLAMgIQAMgIAOgFQANgFAPgCQAOgCAOAAQAUABAUAFIASAFIATAIQAKAFAJAGQAJAGAJAIIgYAmQgHgIgHgFIgQgKIgPgIIgPgEQgQgFgQgBQgSAAgPAGQgPAGgLAJQgKAKgFALQgGALAAALQAAALAHAKQAGALAMAIQALAIAQAGQAPAGARACQAQACAPAEQAPADAOAFQAOAGAMAJQAMAIAIALQAIALAEAOQAEAOgDAQQgCAPgGAMQgGALgKAJQgJAIgMAGQgLAGgNADQgNAEgNACIgYABQgQAAgQgDg");
	this.shape_27.setTransform(-345.7,0.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FF6600").ss(3,1,1).p("EA0xgGPMhpeAAAQiOAAhkBxQhlBxAAChIAAAYQAAChBlByQBkBxCOAAMBpeAAAQCNAABjhxQBkhyAAihIAAgYQAAihhkhxQhjhxiNAAg");

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFCC00").s().p("Eg0tAGQQiOAAhjhyQhmhxABihIAAgYQgBihBmhwQBjhyCOAAMBpeAAAQCNAABjByQBjBwAAChIAAAYQAAChhjBxQhjByiNAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-373.1,-41.5,746.3,83);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhrCBQANABALgCQAKgCAKgFQAJgFAIgHQAIgHAGgIIAMgPIAJgQIhVjfIAugGIA6CvIALgiIAOgrIAPgxIAPgyIAtABIgTA5IgQAuIgNAlIgJAbIgQAsIgJAaQgFANgIANQgIANgJANQgKAMgOAKQgOAJgRAGQgSAHgWABg");
	this.shape.setTransform(152.1,11.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgsBrQgVgIgOgOQgNgPgIgUQgHgVAAgZQgBgYAKgWQAHgWAPgQQAQgQATgJQAUgJAWAAQAWAAARAIQATAIAOAOQAOAPAKATQAJATACAXIisAZQACAOAFALQAFAMAJAHQAJAIALAEQAMAEAMAAQALAAAKgDQALgDAJgGQAIgHAHgJQAGgKADgMIAnAIQgFASgKAPQgLAPgOAKQgNALgQAGQgRAFgSAAQgZAAgUgIgAgQhNQgKADgJAHQgKAHgHAMQgIALgCASIB2gPIgBgEQgHgTgNgMQgNgLgVAAQgHAAgKADg");
	this.shape_1.setTransform(129.6,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AAMCuQgLgFgHgHQgIgHgGgKQgGgJgEgKQgKgWgCgdIAFj9IApAAIgCBCIgBA2IgBArIAAAhIgBA2QAAASAEAPIAFAMQAEAHAEAEQAFAFAHADQAGADAJAAIgGAoQgOgBgLgEg");
	this.shape_2.setTransform(112.7,-2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AgZCXIABg0IABg/IAChVIAogBIgBAzIgBAsIAAAjIgBAbIAAAsgAgKhfIgJgFQgEgFgCgEQgDgGABgGQgBgGADgFQACgFAEgEQAEgEAFgCQAFgCAFgBQAGABAFACQAFACAEAEIAHAJQABAFAAAGQAAAGgBAGIgHAJQgEADgFACQgFACgGABQgFgBgFgCg");
	this.shape_3.setTransform(100.9,0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("ABgAwIAAgrIgBggQAAgRgFgIQgFgIgHAAQgFAAgFADQgGADgFAFIgKALIgJANIgIAMIgHALIAAASIABAYIABAeIAAAmIgmACIgBg+IgBgrIgCggQgBgRgFgIQgEgIgIAAQgFAAgGAEQgFADgFAGQgGAFgFAHIgKAOIgJANIgIAKIACBqIgoACIgFjWIAqgFIABA6IANgQQAHgJAJgHQAIgHAKgEQAJgFAMAAQAIAAAIADQAIADAFAFQAHAGAEAJQAFAIABAMIANgPQAHgJAIgGQAIgHAJgEQAKgFALAAQAJAAAIADQAJADAGAHQAHAGAEAKQAEAKABANIACAlIACAwIAABGIgqACIAAg+g");
	this.shape_4.setTransform(78.9,4.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AgcBxIgRgEIgSgGIgSgJQgKgFgIgGIATgeIAWAKQAMAGAMAEQAMAEANADQAMACANAAQAKAAAJgCQAHgCAFgDIAIgHIADgIQACgDgBgEQAAgEgCgEQgCgEgDgEQgFgDgGgEQgHgDgIgCQgKgCgNgBQgRAAgQgDQgRgDgNgGQgNgHgIgJQgJgKgCgPQgBgOADgMQAEgMAGgKQAHgJAKgHQAKgHALgFQAMgEANgDQANgCAMAAIASABQAKABAMACQAMACAMAFQAMAEAJAIIgNAlQgNgHgMgFIgVgFIgVgEQgegBgRAIQgSAIAAARQAAAMAHAGQAGAFAMADQALACAPABQAOABARACQAUADANAGQAOAFAIAIQAJAIADAKQADAJAAALQAAATgHANQgIANgNAIQgNAIgQAEQgRAEgSABQgSgBgSgDg");
	this.shape_5.setTransform(50.8,5.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AgrCnQgJgCgKgFQgJgEgJgGQgJgHgHgJQgIgJgEgMIAmgRQAEAIAFAHQAGAGAGAEQAGAEAHADIAMAEQAPADAPgCQAOgDALgGQAKgGAHgHQAHgHAFgIIAHgPIADgNIABgJIAAgaQgMALgNAGQgNAGgLADQgMAEgMAAQgXAAgUgIQgUgIgPgPQgPgPgIgVQgJgWAAgbQAAgbAKgVQAJgVAQgPQAPgPATgHQATgIAUAAQANACANAEQAMAEAOAHQANAHAMAMIAAgoIAoABIgCDlQAAANgDANQgDANgGAMQgHAMgJALQgJAKgMAJQgNAIgPAFQgPAGgRABIgGAAQgVAAgSgFgAgeh5QgMAGgIAKQgJALgFAOQgFAOAAARQAAAQAFAOQAFAOAJAJQAJAJAMAGQAMAGAPAAQAMAAANgFQAMgFAKgIQAKgHAHgMQAHgLADgOIAAgaQgCgOgHgMQgIgMgKgIQgKgJgNgEQgNgFgMAAQgOAAgNAGg");
	this.shape_6.setTransform(14.8,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AhdAxIgBgxIgCgmIgBgdIgBgrIAugBIABA6QAGgLAJgKQAIgLAKgHQAKgIAKgFQALgFAMgBQAPgBAMAEQALAEAIAGQAIAGAFAJQAFAJADAJQADAJACAKIABAQQABAiAAAjIgCBIIgpgCIADhAQACgggCggIAAgJIgDgMIgEgNQgDgGgFgFQgFgFgHgCQgHgDgJACQgQACgPAVQgQAVgTAmIABA0IABAdIABAPIgrAGIgDg/g");
	this.shape_7.setTransform(-10.8,5.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgYCXIAAg0IABg/IAChVIApgBIgBAzIgBAsIgBAjIgBAbIgBAsgAgKhfIgJgFQgEgFgCgEQgCgGAAgGQAAgGACgFQACgFAEgEQAEgEAFgCQAFgCAFgBQAGABAFACQAGACADAEIAHAJQACAFAAAGQAAAGgCAGIgHAJQgDADgGACQgFACgGABQgFgBgFgCg");
	this.shape_8.setTransform(-28.1,0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AA1CYQAHgTACgSIAEggIAAghQgBgVgHgPQgGgOgJgKQgJgJgMgEQgLgFgKAAQgLABgMAGQgKAEgLALQgMAJgJATIgCCEIgmABIgEk0IAugCIgBB7QALgNAMgGQANgIALgDQAMgEALgBQAWgBATAJQATAHAMAPQANAPAJAWQAHAUABAcIAAAcIgCAdIgDAbQgBANgDALg");
	this.shape_9.setTransform(-46.8,0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgkBsQgWgKgQgPQgQgPgKgWQgKgVAAgZQAAgOAEgOQAEgPAHgNQAIgNALgLQAKgLAOgHQANgJAPgEQAPgEAPgBQASAAAPAEQAQAFANAIQAOAIALALQAKAMAIAOIAAABIgiATIAAgBQgFgJgHgJQgIgIgJgGQgJgFgKgEQgLgDgMAAQgPABgOAGQgPAGgKALQgLAKgGAQQgHAOABAPQgBARAHANQAGAPALALQAKAKAPAHQAOAGAPAAQALAAAKgDQAKgDAJgEQAJgGAHgHQAIgIAFgJIAAgBIAkAUIgBACQgIANgLAKQgLAKgOAIQgNAHgPAEQgQAFgQAAQgVAAgVgJg");
	this.shape_10.setTransform(-72.8,5.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgXgWIhGABIABglIBFgCIAChXIAngDIgCBaIBOgDIgDAlIhLACIgCCuIgpABg");
	this.shape_11.setTransform(-95,1.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AA+ByIADgcQgZAPgXAHQgXAIgUgBQgOAAgOgDQgNgEgLgIQgKgHgGgMQgHgMAAgRQAAgPAFgNQAFgNAIgKQAJgJALgGQAMgIANgEQANgFAOgBQAOgDAOAAIAYABIAVADQgDgKgEgJQgEgKgFgIQgGgIgJgFQgJgEgMAAQgHAAgJACQgKACgMAGQgMAGgPALQgOAKgQARIgYgeQATgSASgMQARgLAQgGQAPgHAOgCQAMgDAKABQATAAAOAGQAPAGALALQAKALAIAOQAHAQAFARQAFAQACATQACAQAAATQAAATgCAVIgHAxgAgJgBQgRAEgMAIQgMAIgGAMQgGALADAPQADAMAIAEQAIAGAMgBQALAAAOgDIAagKIAZgMIAUgLIAAgUIgBgVQgKgCgKgBQgLgCgLAAQgSAAgQADg");
	this.shape_12.setTransform(-118.2,4.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("ABgAwIAAgrIgBggQAAgRgFgIQgFgIgHAAQgFAAgFADQgGADgFAFIgKALIgJANIgIAMIgHALIAAASIABAYIABAeIAAAmIgmACIgBg+IgBgrIgCggQgBgRgFgIQgEgIgIAAQgFAAgGAEQgFADgFAGQgGAFgFAHIgKAOIgJANIgIAKIACBqIgoACIgFjWIAqgFIABA6IANgQQAHgJAJgHQAIgHAKgEQAJgFAMAAQAIAAAIADQAIADAFAFQAHAGAEAJQAFAIABAMIANgPQAHgJAIgGQAIgHAJgEQAKgFALAAQAJAAAIADQAJADAGAHQAHAGAEAKQAEAKABANIACAlIACAwIAABGIgqACIAAg+g");
	this.shape_13.setTransform(-147,4.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(3,1,1).p("A4ckJMAw5AAAQCJAAAACJIAAEBQAACJiJAAMgw5AAAQiJAAAAiJIAAkBQAAiJCJAAg");
	this.shape_14.setTransform(0,4.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("A4cEKQiJAAAAiJIAAkBQAAiJCJAAMAw4AAAQCKAAAACJIAAEBQAACJiKAAg");
	this.shape_15.setTransform(0,4.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-171.7,-33.1,343.4,66.2);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.path5683 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = getMCSymbolPrototype(lib.path5683, null, null);


(lib.Outlinecopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(2,0,0,6.9).p("AgrguQBRg5BggNQAogIAPAXQAGANgGANQgGAOgNAHQgKAHgPACQgSADgKABQhnAJhbAzQgtAagmAiQArhKBKgzg");
	this.shape.setTransform(193.3,122.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#663300").ss(2,0,0,6.9).p("AAsguQhRg5hggNQgogIgPAXQgGANAGANQAGAOANAHQAKAHAPACQASADAKABQBnAJBbAzQAtAaAmAiQgrhKhKgzg");
	this.shape_1.setTransform(269.9,122.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#663300").ss(2,0,0,6.9).p("Ai7gQQAFhJAxg2QA4g8BNAAQBOAAA3A8QAyA2AGBJQAAAJAAAHQAABUg4A7Qg3A8hOAAQhNAAg4g8Qg3g7AAhUQAAgHABgJgAglg+QACAFAAAFQAAALgIAIQgIAIgMAAQgDAAgDgBAglg+QARgKAUAAQAfAAAWAWQAXAWAAAfQAAAfgXAWQgWAWgfAAQgfAAgWgWQgWgWAAgfQAAgPAGgOQgIgBgGgGQgIgIAAgLQAAgNAIgHQAIgJAMAAQAMAAAIAJQAFAFABAFgACBAIQAAA2gmAlQgYAYgeAJQgQAFgVAAQg1AAglgmQglglAAg2QAAg0AlglQAlgnA1AAQA1AAAmAnQAmAlAAA0g");
	this.shape_2.setTransform(262.3,146.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#663300").ss(2,0,0,6.9).p("Ai7gQQAFhJAxg2QA4g8BNAAQBOAAA3A8QAyA2AGBJQAAAJAAAHQAABUg4A7Qg3A8hOAAQhNAAg4g8Qg3g7AAhUQAAgHABgJgAglg+QARgKAUAAQAfAAAWAWQAWAWAAAfQAAAfgWAWQgWAWgfAAQgfAAgWgWQgWgWAAgfQAAgPAGgOQgIgBgGgGQgIgIAAgLQAAgNAIgHQAIgJAMAAQAMAAAIAJQAFAFABAFQACAFAAAFQAAALgIAIQgIAIgMAAQgDAAgDgBACBAIQAAA2gnAlQgXAYgeAJQgRAFgUAAQg1AAglgmQglglAAg2QAAg0AlglQAlgnA1AAQA1AAAlAnQAnAlAAA0g");
	this.shape_3.setTransform(202.1,146.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#663300").ss(2,0,0,6.9).p("AHFhCQAKAEALAAQATAAAOgNQAMgOAAgQQAAgVgOgOQgNgKgSAAQgQgBgPALQgLALAAAQQghAvgtApQiTCDjRAAQjQAAiTiDQgrgngfgrQgBgQgLgJQgDgCgFgCQgDgGgIgCQgIgBgGABQgRACgLAKQgOAMAAATQAAAOAFAMQALAVAdAAQALAAAKgIQAdAlAiAgQCiCQDhAAQDjAACeiQQAogiAegng");
	this.shape_4.setTransform(232.6,191.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#663300").ss(3,0,0,6.9).p("AKvkjQA6CLAACYQAACZg6CLQg5CHhnBnQhnBoiGA4QiKA7iYAAQiXAAiKg7QiGg4hnhoQhnhng5iHQg6iLAAiZQAAiYA6iLQA5iGBnhoQBnhoCGg5QCKg7CXAAQCYAACKA7QCGA5BnBoQBnBoA5CGg");
	this.shape_5.setTransform(231.9,160.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("AmAOWQizhNiIiKQiJiJhLiyQhOi6AAjKQAAjKBOi4QBLizCJiKQCIiJCzhNQC4hODJAAQDIAAC4BOQCzBNCICJQCKCKBLCzQBNC4AADKQAADKhNC6QhLCyiKCJQiICKizBNQi4BOjIAAQjJAAi4hOg");
	this.shape_6.setTransform(232.2,160.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Outlinecopy4, new cjs.Rectangle(133.2,60.7,198,199.1), null);


(lib.Outlinecopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(2,0,0,6.9).p("AgrguQBRg5BggNQAogIAPAXQAGANgGANQgGAOgNAHQgKAHgPACQgSADgKABQhnAJhbAzQgtAagmAiQArhKBKgzg");
	this.shape.setTransform(193.3,122.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#663300").ss(2,0,0,6.9).p("AAsguQhRg5hggNQgogIgPAXQgGANAGANQAGAOANAHQAKAHAPACQASADAKABQBnAJBbAzQAtAaAmAiQgrhKhKgzg");
	this.shape_1.setTransform(269.9,122.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#663300").ss(2,0,0,6.9).p("Ai7gQQAFhJAxg2QA4g8BNAAQBOAAA3A8QAyA2AGBJQAAAJAAAHQAABUg4A7Qg3A8hOAAQhNAAg4g8Qg3g7AAhUQAAgHABgJgAglg+QACAFAAAFQAAALgIAIQgIAIgMAAQgDAAgDgBAglg+QARgKAUAAQAfAAAWAWQAXAWAAAfQAAAfgXAWQgWAWgfAAQgfAAgWgWQgWgWAAgfQAAgPAGgOQgIgBgGgGQgIgIAAgLQAAgNAIgHQAIgJAMAAQAMAAAIAJQAFAFABAFgACBAIQAAA2gmAlQgYAYgeAJQgQAFgVAAQg1AAglgmQglglAAg2QAAg0AlglQAlgnA1AAQA1AAAmAnQAmAlAAA0g");
	this.shape_2.setTransform(262.3,146.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#663300").ss(2,0,0,6.9).p("Ai7gQQAFhJAxg2QA4g8BNAAQBOAAA3A8QAyA2AGBJQAAAJAAAHQAABUg4A7Qg3A8hOAAQhNAAg4g8Qg3g7AAhUQAAgHABgJgAglg+QARgKAUAAQAfAAAWAWQAWAWAAAfQAAAfgWAWQgWAWgfAAQgfAAgWgWQgWgWAAgfQAAgPAGgOQgIgBgGgGQgIgIAAgLQAAgNAIgHQAIgJAMAAQAMAAAIAJQAFAFABAFQACAFAAAFQAAALgIAIQgIAIgMAAQgDAAgDgBACBAIQAAA2gnAlQgXAYgeAJQgRAFgUAAQg1AAglgmQglglAAg2QAAg0AlglQAlgnA1AAQA1AAAlAnQAnAlAAA0g");
	this.shape_3.setTransform(202.1,146.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#663300").ss(2,0,0,6.9).p("AHFhCQAKAEALAAQATAAAOgNQAMgOAAgQQAAgVgOgOQgNgKgSAAQgQgBgPALQgLALAAAQQghAvgtApQiTCDjRAAQjQAAiTiDQgrgngfgrQgBgQgLgJQgDgCgFgCQgDgGgIgCQgIgBgGABQgRACgLAKQgOAMAAATQAAAOAFAMQALAVAdAAQALAAAKgIQAdAlAiAgQCiCQDhAAQDjAACeiQQAogiAegng");
	this.shape_4.setTransform(232.6,191.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#663300").ss(3,0,0,6.9).p("AKvkjQA6CLAACYQAACZg6CLQg5CHhnBnQhnBoiGA4QiKA7iYAAQiXAAiKg7QiGg4hnhoQhnhng5iHQg6iLAAiZQAAiYA6iLQA5iGBnhoQBnhoCGg5QCKg7CXAAQCYAACKA7QCGA5BnBoQBnBoA5CGg");
	this.shape_5.setTransform(231.9,160.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("AmAOWQizhNiIiKQiJiJhLiyQhOi6AAjKQAAjKBOi4QBLizCJiKQCIiJCzhNQC4hODJAAQDIAAC4BOQCzBNCICJQCKCKBLCzQBNC4AADKQAADKhNC6QhLCyiKCJQiICKizBNQi4BOjIAAQjJAAi4hOg");
	this.shape_6.setTransform(232.2,160.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Outlinecopy3, new cjs.Rectangle(133.2,60.7,198,199.1), null);


(lib.Outlinecopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(2,0,0,6.9).p("AgrguQBRg5BggNQAogIAPAXQAGANgGANQgGAOgNAHQgKAHgPACQgSADgKABQhnAJhbAzQgtAagmAiQArhKBKgzg");
	this.shape.setTransform(193.3,122.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#663300").ss(2,0,0,6.9).p("AAsguQhRg5hggNQgogIgPAXQgGANAGANQAGAOANAHQAKAHAPACQASADAKABQBnAJBbAzQAtAaAmAiQgrhKhKgzg");
	this.shape_1.setTransform(269.9,122.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#663300").ss(2,0,0,6.9).p("Ai7gQQAFhJAxg2QA4g8BNAAQBOAAA3A8QAyA2AGBJQAAAJAAAHQAABUg4A7Qg3A8hOAAQhNAAg4g8Qg3g7AAhUQAAgHABgJgAglg+QARgKAUAAQAfAAAWAWQAXAWAAAfQAAAfgXAWQgWAWgfAAQgfAAgWgWQgWgWAAgfQAAgPAGgOAglg+QACAFAAAFQAAALgIAIQgIAIgMAAQgDAAgDgBQgIgBgGgGQgIgIAAgLQAAgNAIgHQAIgJAMAAQAMAAAIAJQAFAFABAFgACBAIQAAA2gmAlQgYAYgeAJQgQAFgVAAQg1AAglgmQglglAAg2QAAg0AlglQAlgnA1AAQA1AAAmAnQAmAlAAA0g");
	this.shape_2.setTransform(262.3,146.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#663300").ss(2,0,0,6.9).p("ACBAIQAAA2gnAlQgXAYgeAJQgRAFgUAAQg1AAglgmQglglAAg2QAAg0AlglQAlgnA1AAQA1AAAlAnQAnAlAAA0gAglg+QARgKAUAAQAfAAAWAWQAWAWAAAfQAAAfgWAWQgWAWgfAAQgfAAgWgWQgWgWAAgfQAAgPAGgOQgIgBgGgGQgIgIAAgLQAAgNAIgHQAIgJAMAAQAMAAAIAJQAFAFABAFQACAFAAAFQAAALgIAIQgIAIgMAAQgDAAgDgBAi7gQQAFhJAxg2QA4g8BNAAQBOAAA3A8QAyA2AGBJQAAAJAAAHQAABUg4A7Qg3A8hOAAQhNAAg4g8Qg3g7AAhUQAAgHABgJg");
	this.shape_3.setTransform(202.1,146.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#663300").ss(2,0,0,6.9).p("AHFhCQAKAEALAAQATAAAOgNQAMgOAAgQQAAgVgOgOQgNgKgSAAQgQgBgPALQgLALAAAQQghAvgtApQiTCDjRAAQjQAAiTiDQgrgngfgrQgBgQgLgJQgDgCgFgCQgDgGgIgCQgIgBgGABQgRACgLAKQgOAMAAATQAAAOAFAMQALAVAdAAQALAAAKgIQAdAlAiAgQCiCQDhAAQDjAACeiQQAogiAegng");
	this.shape_4.setTransform(232.6,191.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#663300").ss(3,0,0,6.9).p("AKvkjQA6CLAACYQAACZg6CLQg5CHhnBnQhnBoiGA4QiKA7iYAAQiXAAiKg7QiGg4hnhoQhnhng5iHQg6iLAAiZQAAiYA6iLQA5iGBnhoQBnhoCGg5QCKg7CXAAQCYAACKA7QCGA5BnBoQBnBoA5CGg");
	this.shape_5.setTransform(231.9,160.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("AmAOWQizhNiIiKQiJiJhLiyQhOi6AAjKQAAjKBOi4QBLizCJiKQCIiJCzhNQC4hODJAAQDIAAC4BOQCzBNCICJQCKCKBLCzQBNC4AADKQAADKhNC6QhLCyiKCJQiICKizBNQi4BOjIAAQjJAAi4hOg");
	this.shape_6.setTransform(232.2,160.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 2
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AkzEzQh/h/ABi0QAAizB+iAIABAAQB/h+CzAAQC0gBB/B/QCACAgBCzQABC0iAB/Qh/CAi0gBIAAAAQizAAiAh/g");
	this.shape_7.setTransform(231.7,159.9,1.949,1.949);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DBD9D9").s().p("AprJsQkAkAAAlsQAAlqEAkBQEAkAFrAAQFsAAEAEAQEAEBAAFqQAAFskAEAQkAEAlsAAQlrAAkAkAg");
	this.shape_8.setTransform(231.7,159.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000033").s().p("AqCKCQkKkJABl5QgBl4EKkKQELkKF3ABQF5gBEJEKQELEKAAF4QAAF5kLEJQkJELl5AAQl3AAkLkLg");
	this.shape_9.setTransform(231.7,160);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Outlinecopy2, new cjs.Rectangle(133.2,60.7,198,199.1), null);


(lib.Outlinecopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(2,0,0,6.9).p("AlRngQBDgvBPgLQAhgGAMATQAFAKgFALQgFAMgLAFQgHAGgNACQgPACgIABQhVAHhLArQgmAVgfAcQAkg8A9grgAmChIIAAgCQgOgjgBg/QgEhQAgg/QAkhLBIgTQA9gEAuA0QAnAuAKA/QADAJABAKIADAfQABARgCARQgDAsgOAlQgLAfgRAUQgEAGgFAEQgYAZgeAAQgGABghAAQglgBgEABQgGgCgHgBQg5gPgWgwQgBgDgCgDgAjsjsQgFgBgGAAQgSAAgMANQgMANAAASQAAATAMANQAMANASAAQARAAAMgNQAMgNAAgTQAAgCAAgBQADgCABgDQAGgKAAgOQAAgOgGgKQgGgLgIAAQgJAAgGALQgDAGgCAHQgBAFAAAGQAAAOAGAKQAGAKAJAAQAGAAAEgFAhmjKQABARgBARQgDAsgOAkQgLAggSAUQgEAFgEAEQgaAZggAAQgHABgbAAQgXAAgLAAQgLgBgLgBQgygJgYgqQgEgIgEgKAizkNQgegggqAAQgqAAgeAgQgdAgAAAtQAAAtAdAgQAeAgAqAAQAqAAAeggQAdggAAgtQAAgtgdgggAliHOQAAgjAWgUQAVgTAbgGQAAgIAAgFQAAhcBWg9QBThBB5AAQB5AABVBBQBWA9AABcQAWAEATARQAWAZAAAdQAAAhgWAYQgZAZgfAAQgZAAgTgQQgEgDgEgCQgDgBgDgDQgWgYAAghQAAgdAWgZQAMgKALgGQgRg3gtgnQhLhAhsAAQhlAAhJBAQg6AxgOA9QAOAHAJAKQAWAUAAAjQAAAagOATQgDAGgFADQAAAEgDACQgYATgaAAQgkAAgXgZQgWgXAAgfgAFSnZQhEgvhPgLQgggGgNATQgFAKAFALQAFAMALAFQAIAGAMACQAQACAHABQBWAHBLArQAmAVAeAcQgjg8g9grgAEPjsQgFgBgGAAQgRAAgMANQgMANAAASQAAATAMANQAMANARAAQASAAALgNQANgNAAgTQAAgCAAgBQACgCACgDQAGgKAAgOQAAgOgGgKQgGgLgJAAQgIAAgGALQgEAGgCAHQgBAFAAAGQAAAOAHAKQAGAKAIAAQAGAAAFgFAFIkNQgegggqAAQgqAAgdAgQgeAgAAAtQAAAtAeAgQAdAgAqAAQAqAAAeggQAeggAAgtQAAgtgegggAGVjKQABARgBARQgCAsgOAkQgLAggSAUQgEAGgFAEQgZAZgeAAQgGABggAAQgmgBgDABQgHgCgHgBQgygJgXgqQgEgIgEgKIgBgCQgNgjgCg/QgDhQAfg/QAlhLBIgTQA9gEAtA0QAoAuAKA/QACAJACAKIACAfQABARgBARQgDAsgOAlQgLAfgRAUQgEAFgFAEQgZAZggAAQgHABgcAAQgWAAgLAAQgMgBgLgBQg4gPgXgwQgBgDgBgD");
	this.shape.setTransform(231.4,158.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#663300").ss(3,0,0,6.9).p("AKvkjQA7CLAACYQAACZg7CLQg4CHhoBnQhnBoiGA4QiKA7iYAAQiWAAiLg7QiGg4hnhoQhnhng4iHQg7iLAAiZQAAiYA7iLQA4iGBnhoQBnhoCGg5QCLg7CWAAQCYAACKA7QCGA5BnBoQBoBoA4CGg");
	this.shape_1.setTransform(232.2,160.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AmAOWQizhNiIiKQiJiJhLiyQhOi6AAjKQAAjKBOi4QBLizCJiKQCIiJCzhNQC4hODJAAQDIAAC4BOQCzBNCICJQCKCKBLCzQBNC4AADKQAADKhNC6QhLCyiKCJQiICKizBNQi4BOjIAAQjJAAi4hOg");
	this.shape_2.setTransform(232.2,160.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AkzEzQh/h/ABi0QAAizB+iAIABAAQB/h+CzAAQC0gBB/B/QCACAgBCzQABC0iAB/Qh/CAi0gBIAAAAQizAAiAh/g");
	this.shape_3.setTransform(232,159.9,1.949,1.949);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DBD9D9").s().p("AprJsQkAkAAAlsQAAlqEAkBQEAkAFrAAQFsAAEAEAQEAEBAAFqQAAFskAEAQkAEAlsAAQlrAAkAkAg");
	this.shape_4.setTransform(232.1,159.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000033").s().p("AqCKCQkKkJABl5QgBl4EKkKQELkKF3ABQF5gBEJEKQELEKAAF4QAAF5kLEJQkJELl5AAQl3AAkLkLg");
	this.shape_5.setTransform(232.1,160);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Outlinecopy, new cjs.Rectangle(133.2,60.7,198,199.1), null);


(lib.Tween12copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Outlinecopy();
	this.instance.parent = this;
	this.instance.setTransform(345.4,12.2,1.724,1.724,0,0,0,233.5,167.3);

	this.instance_1 = new lib.Outlinecopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-340.9,12.2,1.724,1.724,0,0,0,233.5,167.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.8,-171.6,1027.7,343.3);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Outlinecopy();
	this.instance.parent = this;
	this.instance.setTransform(345.4,12.2,1.724,1.724,0,0,0,233.5,167.3);

	this.instance_1 = new lib.Outlinecopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-340.9,12.2,1.724,1.724,0,0,0,233.5,167.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.8,-171.6,1027.7,343.3);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween23("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(170.2,33.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,343.4,66.2);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.chrcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#28292C").s().p("AgigPQAvgiA5gHQAXgFAJAOQAEAHgEAIQgDAIgIAEQgGAEgJACIgQACQg8AFg2AeQgbAQgWATQAZgrAsgeg");
	this.shape.setTransform(13.9,7.5,1.695,1.695);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#28292C").s().p("AgigPQAvgiA5gHQAXgFAJAOQAEAHgEAIQgDAIgIAEQgGAEgJACIgQACQg8AFg2AeQgbAQgWATQAZgrAsgeg");
	this.shape_1.setTransform(87.4,7.5,1.695,1.695,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E0E0DE").s().p("AiFCPQg3g7AAhUIABgQQAFhJAxg2QA4g8BNABQBOgBA3A8QAyA2AGBJIAAAQQAABUg4A7Qg3A9hOAAQhNAAg4g9gAhahRQglAlAAA0QAAA2AlAlQAlAlA1AAQAUAAARgEQAegJAYgYQAmglAAg2QAAg0gmglQgmgmg1AAQg1AAglAmg");
	this.shape_2.setTransform(81.4,33.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#100F11").s().p("Ag1A2QgWgWABggQAAgOAFgOIAHABQALAAAIgIQAIgIAAgMQAAgFgBgEQARgLATABQAggBAWAXQAWAWgBAeQABAggWAWQgWAWgggBQgeABgXgWg");
	this.shape_3.setTransform(81.4,33.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#547DBE").s().p("AhbBbQgkglgBg2QABg0AkglQAmgmA1AAQA1AAAlAmQAnAlAAA0QAAA2gnAlQgYAYgdAJQgRAEgUAAQg1AAgmglgAhUhQQgHAIAAALQAAAMAHAIQAHAGAIABQgGAOAAAPQAAAfAWAWQAWAWAfAAQAfAAAWgWQAWgWAAgfQAAgfgWgWQgWgWgfAAQgUAAgRAKQgBgGgFgEQgIgJgMAAQgMAAgJAJg");
	this.shape_4.setTransform(81.4,34.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F5F6F1").s().p("AgFAbQgIgCgHgFQgHgIAAgMQAAgLAHgIQAJgIALAAQAMAAAIAIQAFAFABAGQACAEAAAEQAAAMgIAIQgIAIgMAAIgFgBg");
	this.shape_5.setTransform(75,28.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E0E0DE").s().p("AiFCPQg3g7AAhUIABgQQAFhJAxg2QA4g8BNABQBOgBA3A8QAyA2AGBJIAAAQQAABUg4A7Qg3A9hOAAQhNAAg4g9gAhahRQglAlAAA0QAAA2AlAlQAmAlA0AAQAVAAAQgEQAegJAYgYQAmglAAg2QAAg0gmglQglgmg2AAQg0AAgmAmg");
	this.shape_6.setTransform(21.1,33.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#100F11").s().p("Ag0A2QgXgWAAggQAAgOAHgOIAFABQAMAAAIgIQAJgIAAgMQgBgFgCgEQASgLATABQAggBAVAXQAXAWAAAeQAAAggXAWQgVAWgggBQgeABgWgWg");
	this.shape_7.setTransform(21.1,33.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#547DBE").s().p("AhaBbQgmglABg2QgBg0AmglQAlgmA1AAQA1AAAlAmQAmAlAAA0QAAA2gmAlQgYAYgeAJQgQAEgUAAQg1AAglglgAhThQQgIAIAAALQAAAMAIAIQAFAGAJABQgGAOAAAPQAAAfAWAWQAWAWAfAAQAfAAAWgWQAWgWAAgfQAAgfgWgWQgWgWgfAAQgUAAgRAKQgCgGgFgEQgHgJgMAAQgMAAgIAJg");
	this.shape_8.setTransform(21.2,34.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F5F6F1").s().p("AgFAbQgJgCgFgFQgIgIAAgMQAAgLAIgIQAIgIALAAQAMAAAHAIQAFAFACAGQACAEAAAEQAAAMgJAIQgHAIgMAAIgFgBg");
	this.shape_9.setTransform(14.8,28.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2C2828").s().p("AmFAHQgigggdglQgKAIgLAAQgdAAgKgVQgGgMAAgOQAAgTAOgMQALgKARgCQAGgBAIABQAIACAEAGIAHAEQALAJABAQQAfArAqAnQCUCDDQAAQDRAACTiDQAtgpAhgvQgBgQALgLQAPgLARABQASAAANAKQAOAOAAAVQAAAQgMAOQgNANgUAAQgLAAgKgEQgeAngoAiQifCQjiAAQjhAAiiiQg");
	this.shape_10.setTransform(51.7,79);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.instance = new lib.path5683();
	this.instance.parent = this;
	this.instance.setTransform(50,48.1,0.383,0.383,0,0,0,191,193.1);
	this.instance.alpha = 0.219;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FFFF00","rgba(255,255,0,0)"],[0,1],0,-178.1,0,0,-178.1,278.2).s().p("Ar1cLQldiUkNkPQkOkPiTlfQiZlsAAmOQAAmNCZlsQCTlfEOkOQENkPFdiVQFqiaGLAAQGMAAFpCaQFeCVENEPQEOEOCTFfQCZFsAAGNQAAGOiZFsQiTFfkOEPQkNEPleCUQlpCamMAAQmLAAlqiag");
	this.shape_11.setTransform(51.3,47.3,0.383,0.383);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FFFF00","rgba(255,255,0,0)"],[0,1],0,-178.1,0,0,-178.1,278.2).s().p("Ar1cLQldiUkNkPQkOkPiTlfQiZlsAAmOQAAmNCZlsQCTlfEOkOQENkPFdiVQFqiaGLAAQGMAAFpCaQFeCVENEPQEOEOCTFfQCZFsAAGNQAAGOiZFsQiTFfkOEPQkNEPleCUQlpCamMAAQmLAAlqiag");
	this.shape_12.setTransform(51.3,47.3,0.383,0.383);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#EAA81C").s().p("Ar1cBQldiTkNkOQkOkNiTldQiZlqAAmMQAAmLCZlpQCTleEOkNQENkOFdiTQFqiZGLAAQGMAAFpCZQFeCTENEOQEOENCTFeQCZFpAAGLQAAGMiZFqQiTFdkOENQkNEOleCTQlpCZmMAAQmLAAlqiZg");
	this.shape_13.setTransform(51.3,47.3,0.383,0.383);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.instance}]}).wait(1));

	// Layer_3
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(0,0,0,0.2)").s().p("AlOMeQibhCh2h4Qh4h3hBicIgCgEIgFgOIgPgnQguiGAAiSQAAifA4iUIAKgZIACgEQBBibB4h4QB2h4CbhBQCghFCuAAQCvAACgBFQCbBBB2B4QB4B4BCCbIABAEIAOAkIAAADQA0CMAACZQAACXgxCMIgSAuQhCCch4B3Qh2B4ibBCQigBEivAAQiuAAighEg");
	this.shape_14.setTransform(51.2,47.3,0.902,0.902);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(1));

	// Layer_5
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.02)").s().p("AlMMYQiZhCh2h2Qh3h3hAiaIgCgEIgGgPIgOgmQguiFAAiRQAAieA4iSIAKgZIACgEQBAiaB3h4QB2h2CZhBQCfhECtAAQCuAACfBEQCZBBB2B2QB3B4BBCaIABAEIAOAjIABADQAzCLAACYQAACWgxCLIgSAuQhBCah3B3Qh2B2iZBCQifBDiuAAQitAAifhDg");
	this.shape_15.setTransform(51.2,47.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(1));

}).prototype = getMCSymbolPrototype(lib.chrcopy, new cjs.Rectangle(-34.2,-38.6,171,171.9), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween3copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.chrcopy();
	this.instance.parent = this;
	this.instance.setTransform(-106.7,-96.4,2.062,2.062);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.19,scaleY:2.19,x:-106.5,y:-96.3},9).to({scaleX:2.06,scaleY:2.06,x:-106.6,y:-96.4},10).to({scaleX:2.19,scaleY:2.19,x:-106.5,y:-96.3},10).to({scaleX:2.06,scaleY:2.06,x:-106.7,y:-96.4},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.3,-176.1,352.6,354.4);


(lib.Tween3copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.chrcopy();
	this.instance.parent = this;
	this.instance.setTransform(-106.7,-96.4,2.062,2.062);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.3,-176.1,352.6,354.4);


(lib.Tween3copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.chrcopy();
	this.instance.parent = this;
	this.instance.setTransform(-106.7,-96.4,2.062,2.062);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.3,-176.1,352.6,354.4);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(641.9,347.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(344).to({_off:false},0).wait(47).to({startPosition:15},0).to({x:641.8,startPosition:3},8).to({_off:true},1).wait(7));

	// Layer_7
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(291.1,649.5,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(255).to({_off:false},0).to({_off:true},30).wait(122));

	// Layer_5
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(307.1,316.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(225).to({_off:false},0).to({_off:true},30).wait(152));

	// Layer_9
	this.instance_3 = new lib.Outlinecopy3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(299.9,558.8,1.724,1.724,0,0,0,233.5,167.3);
	this.instance_3._off = true;

	this.instance_4 = new lib.Outlinecopy4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(641.4,359.4,2.06,2.06,0,0,0,233.6,167.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(285).to({_off:false},0).to({_off:true,regX:233.6,scaleX:2.06,scaleY:2.06,x:641.4,y:359.4},21).wait(101));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(285).to({_off:false},21).wait(10).to({alpha:0},10).to({_off:true},1).wait(80));

	// Layer_3
	this.instance_5 = new lib.Tween3copy3("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(639.7,343.6);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(139).to({_off:false},0).to({scaleX:0.82,scaleY:0.82,x:985.4,y:547.2,alpha:0.602},20).wait(57).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(185));

	// Layer_4
	this.instance_6 = new lib.Tween3copy3("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(639.7,343.6);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(139).to({_off:false},0).to({regX:0.2,regY:0.1,scaleX:0.84,scaleY:0.84,x:298.3,y:546.8,alpha:0.602},20).wait(57).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(185));

	// Layer_8
	this.instance_7 = new lib.Symbol4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(826.9,121.3,1.02,1.02,0,0,0,170.3,33.1);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(82).to({_off:false},0).to({_off:true},140).wait(185));

	// Layer_10
	this.instance_8 = new lib.Tween3copy6("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(639.7,343.6);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(82).to({_off:false},0).to({_off:true},140).wait(185));

	// Layer_6
	this.instance_9 = new lib.Tween11("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640.8,546.7);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween12copy("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640.8,546.7);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(65).to({_off:false},0).to({_off:true,alpha:1},5).wait(337));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(65).to({_off:false},5).wait(256).to({startPosition:0},0).to({alpha:0},8).to({_off:true},1).wait(72));

	// Layer_2
	this.instance_11 = new lib.Tween3copy4("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(639.7,343.6);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(42).to({_off:false},0).to({alpha:1},5).to({_off:true},35).wait(57).to({_off:false},0).wait(187).to({startPosition:0},0).to({scaleX:1.21,scaleY:1.21},18).wait(47).to({startPosition:0},0).to({alpha:0},8).wait(8));

	// Layer_1
	this.instance_12 = new lib.Tween24("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,124.8);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_13 = new lib.Tween25("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,124.8);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(19).to({_off:false},0).to({_off:true,alpha:1},6).wait(382));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(19).to({_off:false},6).wait(301).to({startPosition:0},0).to({alpha:0},8).to({_off:true},1).wait(72));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(591.8,310.3,1390,827);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;