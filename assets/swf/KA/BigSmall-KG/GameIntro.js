(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#585153").s().p("AgGAJIgDgFIAAgBQgBgGACgCQAEgFAEABQAJACABAFQAAAGgGADQgBADgDAAIgBAAIgFgBg");
	this.shape.setTransform(33.8,-88.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgKA/QgZgOgagJQgPgRAAgXQgBgbAZgVQAWgUAfAAQAfAAAVAUQAYAVAAAbQAAAegYATQgVAUgfAAIgLgGgAAXgqQgCADABAGIAAACIADAEQAFACACgBQADAAACgDQAFgCAAgIQgBgFgJgCIgCAAQgDAAgEAEg");
	this.shape_1.setTransform(30.6,-84.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B66D26").s().p("Ag0AyQgZgUABgeQgBgcAZgVQAWgTAfAAQAfAAAVATQAYAVAAAcQAAAegYAUQgVATgfAAQgfAAgWgTgAAXgrQgCACABAHIAAACIADADIAHACQABAAABAAQAAAAABAAQAAAAABgBQAAAAABgBQAFgDAAgGQgBgHgJgBIgDAAQgDAAgDADg");
	this.shape_2.setTransform(30.6,-85.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#585153").s().p("AgFAIIgDgBIgBgFQgCgEAEgFQAFgEAEAAQAHADABAGQABAHgEADQgBACgEACIgBAAQgDAAgDgEg");
	this.shape_3.setTransform(-4.6,-88.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C1819").s().p("Ag7A5QgZgXAAghQAAggAZgWQAZgXAiAAQAiAAAbAXQAYAWAAAgQAAAQgFAMQgaAHgWAKQgfAMgZASQgUgGgPgNgAAagvQgEAEACAGIABAFIADABQAEAEAEAAQAEgCABgCQAEgDgBgJQgBgFgHgEQgFABgFAEg");
	this.shape_4.setTransform(-8,-84.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B66D26").s().p("Ag7A3QgZgXAAggQAAggAZgVQAZgYAiAAQAiAAAbAYQAYAVAAAgQAAAWgMATIgpAQQgWAJgUAMQgdgDgUgUgAAagxQgEAGACAFIABAEQAAABAAAAQAAAAABAAQAAABABAAQAAAAABAAQAEADAEAAIAFgDQAEgEgBgGQgBgIgHgBIgDAAQgEAAgDACg");
	this.shape_5.setTransform(-8,-85.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6F7074").s().p("AAeAVQgEgBgIgEIgLgFQgKgDgZACQgIABgFgCQgJgDgBgIQgCgKAIgFQAGgEAOAAQAYAAAGACQAIABASAGQAMAGAEADQAIAIgDAIQgCAGgHACIgHABIgGgBg");
	this.shape_6.setTransform(16.1,-59.9,0.768,0.768);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1C1819").s().p("AggBKQgIgDgIgKQgIgLgOgcQgSghgCgTQAAgOAGgNQAHgNALgIIASAAIA9AAIAVABIAUAFQAWAEAIATQAJANgDAYQgGAhgcAaQgcAagiAGIgFAAQgOAAgHgFg");
	this.shape_7.setTransform(17.3,-55.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(51,51,51,0.298)").ss(1,1,1,3,true).p("AGv07QABgBACAAQAEgGACgEQATgaAhgQQAvgWAyAGQAwAHAqAfQAnAeAcAqQAwBJAMBzQANBqgWBMQgMAvgdAjQgZAigjATQgEACgEABQgcAQghAEQgFABgCAAQgBAKgEAKQgNBFgfA+QguBghVBRQgEAGgFADQgPAPgQANQgPAMgOALQgFAIgFAGQgOARgSAQQgSANgUANQgFADgEABQgcAOgdAEQgLACgKACQgIAAgGgCQgGAAgEgBQgMgBgKgEQgNgFgKgIIAAgBQgEgDgEgFQgEACgEAFQgTAUgbAPQgJADgJAFQgGACgEABQgeAJghAAIgBAAQgBAAgBAAQgMAAgMAAQgkgBgpgNIgBAAQgjgGgigIQhAgRg4gcQgSgJgRgMQgMgGgLgIQgQgKgTgOQgigbgjggQgZgZgXgbQgJAHgKAIQhkBFhHA3QhcBEhVBNQhOBIhGBQQhHBRg6BcQg1BagdBiQgdBqAZBqQAaBwBnA5QAmAVAoAIQAXgcAZgXQA8g0BTgTQAVgHASgBQAngGApAFQADgBABgCQBIhSAZhqQAFgUAFgTQAThVAThVQAPhGAchEQAEgIAEgJQAKgZAMgXQANgXANgXQAPgfARgeQAZgoAZgmQAJgOAJgMQAMgXATgTQADgGAGgFQAPgUAUgTAns0eQABAAABgBQAIgJAIgKQASgQATgPQCyiYDyAAQEBAAC6CpQAAACABABQADABABABQAFAGAEADQC+C5AAEEQAAA8gKA4Ah7kIQgbgIgWgMQgSgJgOgLQgPgLgNgOQgXgZgLgkQgHgSgBgWQgEgcAEgcQABgdAIgcQAIgbAKgaQAZg2ApgwQA9hGBSgiQBLghBVACQAFAAAHAAQAKABAKABQBRAIBFAmQBHApAgA2QADAHAEAJQASArgBA3QgBAVgCAWQgCAQgEAOQgLAwgTAoQgDAFgBAFQgJANgIALAGvmwQClCECbCYQBgBgA4BJQBMBiAgBhQAuCGgmBqQgGAPgFALQgRAhgWAcQASADASADQBMAQBAA2QB7BogEC3QgBC5h/CcQg0A/g6AsQgBAAgBABIgBAAQhXBAhnAVIgCAAQiNAehpg8QgagRgZgVQhnhVgOiOAL8JAQAEgCAFAAQAigPAngIQBAgNA7AFAQ5TqQgnAtgqAgQg0Alg8ASQgNAEgPAEQh/AahZhLQgIgHgHgHQg8g8gJhcQgBgVAAgWQABiFBchxQBGhWBYgkQAegLAegGQAtgJAmACQADAAACAAQBIAIA4AvQBZBMgCCEQgCCGhaBxgAHyEiIATAcQAzBMAeAlQAwA9AxApQAfAaAmARAHyEiQgCASgBASQgFA5gNA6QgWBygxBjQgzBphNBOQgWAWgWAVQhAA0hMAkQhpAuhuAAQhbgBhigiQAyCxhECYQgIAQgIAQQgaAugiAiQgoAmgyAYQgfAOgkAJQgbAGgaACQggADgggDQg9gFg7gaQgqgTgpgbQgTgOgSgNQh/hng2inQg8i8BHiiQATgqAYghAFJlOQBnCcAqC6QAfCOgHCMADqNwQAVAnAfA5QAtBNArAyQATAVAYAVQgDgeAAgeQADi5B+ibQBPhjBmg0QASgKAWgIAqBrQQgEACgFACQgiANglgBQgHAAgEgBQgsgDgjgVQhAgogahWQgXhLANhaQAJhJAZg8QAchEAtguQAygyBDgWQBFgWA/AVQAZAIAYANQAFAFAEACQACACADAAAqBrQQgUhPAAhWQAAj0Cpi1AoKntQAAgBgBAAQgzg9gfhFQgXgugNgyAlblYQAAgCAAgDAgTj6QgMACgOgCAmqQxQAEAJABAJQAhBygjBhQgFAOgGALQgOAkgZAdQgxA8hQATQh2AchxhPQhFgvgqhBQgegvgRg3QgsiIA0h2QAOgdARgYQAzhFBXgVQAsgKAqAFQBKAIBHAvQB0BOApCIgArfJHQA3AFA0AWQAuATAwAfQCeBsA7C9QAFAMACAK");
	this.shape_8.setTransform(-0.9,0.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#552D15").s().p("ABACIQgrgcgkgyQgdgkgig8QgYgogLgOIgUgXIACgkIATAcQAzBMAfAlQAuA8AxApQAgAaAlARQgVAIgSAKIgfgQg");
	this.shape_9.setTransform(62.1,44.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#9C5824").s().p("Aj3GHQgsg8gchHQgnhoADhvQAFhrAwhlQAxhmBThHQAwgsA2gXQA5gbA5AAQBigCBRBIQBOBDAgBiQAoB9gjCJQggCChZBrIgaAfIgCABIgBAAQhXBAhnAVIgBAAQgvAKgqAAQhXAAhGgogAAlkHQgeAGgdALQhYAkhGBWQhcBxgBCEIABArQAJBcA8A8IAPAOQBZBLB+gaIAcgIQA8gSA0glQAqggAngtQBahxACiFQACiEhZhMQg4gvhIgIIgFAAIgOAAQggAAglAHg");
	this.shape_10.setTransform(82.4,102.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F3E2B9").s().p("Ao2CYQgahfg8hJQg5hChPgbQhXgdhDAoQgiAWgXAoQgUAjgJAtQgIAmACAuQAAAcAHA6IACAYQgdgvgRg3QgsiIA0h1QANgdARgYQAzhFBXgVQAtgKAqAFQBJAIBIAvQBzBOAqCHQADAJABAJQAhBygjBhQgEAOgGALQgPAkgZAdQALhUgXhWgAOpBcQAliVg6h8QgPgggSgLQgegUg9AaQh6A3g7BRQg7BQgIB6QgCA5AGBYQg8g8gJhcIgBgrQABiEBchxQBGhWBZgkQAdgLAfgGQAtgJAlACIAGAAQBHAIA5AvQBYBMgCCEQgBCFhbBxQgmAtgqAgQg0Alg8ASQBfhfAiiFg");
	this.shape_11.setTransform(7.2,107.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#834719").s().p("Au1GoIgmgbQh/hng2inQg8i7BHiiQAUgqAXghQAXgcAZgXQA8g0BUgTQAUgHASgBQAogGApAFQA2AFA0AWQAvATAvAfQCfBsA7C9QAEAMACAKQAyCwhECYIgQAgQgaAugiAiQALgQAJgRQALgcAJgeQAUg5ADhBQADhbgchcQgbhWg2hQQhViChsgrQhTgihdAUQhdAThCBAQg/A+gZBbQgZBaAVBXQAVBWAlBBQAdA3AlAhIAPAJQBBBVArArIAWAWQgqgTgogbgAHRFoQhnhVgNiOQgEgeAAgeQAEi4B9ibQBQhjBmg0QASgKAVgIQAFgCAEAAQAigPAogIQA/gNA7AFIAkAGQBMAQBBA2QB6BogDC3QgBC4h/CcQg0A/g6AsIAagfQBYhrAgiCQAjiJgoh9QgfhihOhDQhShIhhACQg6AAg6AbQg1AXgxAsQhSBHgxBmQgxBlgEBrQgEBvAoBoQAbBHAsA8QgagRgZgVg");
	this.shape_12.setTransform(6,101.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#D9955D").s().p("AvxI3Qhng4gahwQgZhrAehpQAchjA1hYQA6hdBHhSQBHhQBOhHQBVhNBbhEQBIg4BjhFQiAB1hCA/QiQCEhWBjQhVBfgxBZQg7BrgJBpQgJB1A6BlQAxBTBIAiQgZAXgXAcQgngIgngWgAQxBtQg9iah1h3QgmgphBg0QhIg6gigfQhyhcg5hTQAQgOAOgOQCmCDCbCZQBgBfA3BLQBMBgAgBiQAvCFgnBrIgKAaQgBiAgxiAg");
	this.shape_13.setTransform(-12.3,12);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C27333").s().p("AhAG0QhsgIhUhjQhMhVgah8Qgbh4AOh9QAHgsAKgrQAOgeARgfIAyhOIASgaQANgXASgTQADgFAGgFIABAYQAHBHAbBDQAkBXA7AwQAjAZAoAJQAqAKAogLQAlgMAigeQAagZAagqQBGhpAZh7QAKAFANABIAJABIAFAPQA8CSgMCnQgODKhzCcQg8BUhLAwQhPAwhNAAIgTgBg");
	this.shape_14.setTransform(-14.1,14.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#C47B40").s().p("AgvC3QgogJgjgZQg7gwgkhXQgbhCgHhHIgBgYQAPgUAUgUQA4AdBAARQAiAIAjAFIABAAQAoAOAkABQAOABAMgBIABAAQAjAAAdgJIAKgDQAJgFAJgDQAbgPAUgVQADgEAFgCIAIAIIAAABQAJAIANAEQgZB7hGBoQgaAqgaAZQgiAeglAMQgVAGgVAAQgUAAgUgFg");
	this.shape_15.setTransform(-10.4,-13.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E7D6AC").s().p("AAWAdQAMgWALgXQAIgRACgVIADgJQAHgfgCggQAMAZgBAeIAAAFQgCAWgHAYQgLAggaAoIgWAiIAAgCIgHgHQgEABgEAFQgTAUgbAPQgJADgJAFQA6giAlg/g");
	this.shape_16.setTransform(12,-38.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#9F561B").s().p("AjAKAQgCgKgEgNQg7i9ifhrQgvgfgvgTQg0gXg2gFQAAAAABAAQAAgBABAAQAAAAAAgBQABAAAAgBQBJhRAZhrIAKgmIAlipQAQhGAbhEIgEATQgJArgOBmQgPBdgTCUQAbBwA8BOQAeAmA5A2QBEBBAXAXQAMARAcAfQAZAZAVAMQA0AeBVgOQCQgTB4hbQB3hcA5iGQAnhbAMh3QAJhXgGiCQgIibgXhzQgXhqgphXIAJgFQATgMASgOQBnCcAqC7QAgCPgICLIgCAkQgGA4gMA7QgXBxgxBkQgzBohNBOQgVAXgXAUQg/A0hMAlQhrAuhsAAQhbgBhjgig");
	this.shape_17.setTransform(-12.7,34.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#B66829").s().p("AiMJUQgWgNgZgYQgbgfgNgRQgWgYhEhAQg6g3gdglQg8hOgchwQAUiVAOhdQAOhmAJgrIAFgTIAIgRQAKgZAMgYIAaguQgKArgHArQgNB/AbB4QAaB6BMBVQBUBkBrAIQBWAHBag3QBLgvA8hUQByibAPjLQALiog8iSIgEgPQAGACAIAAIAVgEQAdgEAcgOQApBXAYBrQAXByAICbQAFCDgJBXQgLB3goBaQg4CGh3BdQh4BaiPAUQgbAEgYAAQg0AAgjgUg");
	this.shape_18.setTransform(-12.2,31.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#7A3B14").s().p("AsNG+QALgYAbgpIAmhCQAag1AMhJQAJgtAHhYQADg4AFgkQAFgZAAgPQAMhLAZg2QAZg5Ayg9QAXgcBJhRIAvgzIAjAYIAWAPQASALARAJQgTAUgQAUQgFAFgEAFQgSATgMAXIgSAaIgzBOQgRAfgOAeIgaAvQgMAYgLAZIgHARQgcBDgPBGIgmCqIgKAmQgZBrhIBRQgBABAAAAQAAABgBAAQAAAAgBABQAAAAgBAAQgpgEgoAGQAEgUAJgSgALMGxQgwgpgwg9Qgegmg0hMIgTgcQAIiLgfiOQgri7hmicQARgQAOgRQBRBSArCQQAPA5ATBaQAXCIAEANQAvDdBnB+QAkApAqAfQgFAAgEADQgmgRgggag");
	this.shape_19.setTransform(-3,10.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#BE6C2D").s().p("ApWPrQg9gFg7gaIgWgWQgrgrhBhVIgPgJQglghgdg3QglhBgVhWQgVhYAZhaQAZhbA/g+QBChABdgTQBdgUBTAiQBsArBVCCQA2BQAbBXQAcBcgDBbQgDBBgUA5QgJAegLAcQgJARgLAQQgnAmgyAYQggAOgkAJQgaAGgaACQgQABgRAAQgQAAgQgBgAreErQhXAVgzBFQgRAYgNAdQg0B2AsCIQARA3AdAvQAqBBBFAvQBxBPB2gcQBRgTAwg8QAZgdAPgkQAGgLAEgOQAjhhghhyQgBgJgDgJQgqiIhzhOQhIgvhJgIIgagCQgeAAgfAHgAHHJRQgrgyguhNIg0hgQAXgVAVgWQBNhOAzhpQAxhjAXhxQAMg6AGg5IAUAXQALAOAYAoQAiA9AdAkQAlAxArAcIAfAQQhmA0hQBjQh9CbgEC5QAAAeAEAeQgYgVgTgVgAwOAlQg6hkAJh2QAJhoA7htQAxhYBVhfQBWhkCQiDQBChACAh0IAUgPQAWAbAZAZQAjAgAiAbIguAzQhJBQgXAdQgyA9gZA4QgZA3gNBLQAAAOgEAZQgFAkgDA5QgIBYgIAuQgMBIgaA1IgmBCQgbApgLAYQgJASgEATQgSABgUAHQhUATg8A0QhIgigxhSgAMIgIQhoh9gujdQgEgOgYiJQgShZgQg6QgriQhQhRQAGgGAEgIIAegXQA5BTByBdQAiAfBIA5QBBA0AmApQB1B3A9CbQAxCAABCAQgRAggXAcQg7gFg/ANQgoAIgiAPQgqgggjgog");
	this.shape_20.setTransform(-8.9,51.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCF4E0").s().p("AtoRKQhFgvgqhBIgCgYQgHg6AAgcQgCguAIgmQAJguAUgjQAXgoAigWQBDgoBXAdQBPAbA5BCQA8BKAaBfQAXBWgLBUQgwA8hRATQgfAHgfAAQhWAAhTg6gAJdQDIgPgOQgGhYACg5QAIh7A7hQQA7hRB6g3QA9gaAeAUQASALAPAgQA6B8glCWQgiCFhfBfIgdAIQggAHgeAAQhXAAhDg4gAg6o8IgCAAIgYAAQgkgBgpgNIgBAAQgbgIgVgMQgTgJgNgLQgQgLgMgOQgXgZgLgkQgHgSgBgWQgFgcAFgcQABgdAIgcQAIgbAKgaQAZg2ApgwQA9hGBRgiQBMghBUACIANAAIAUACQBQAIBFAmQBIApAfA2IAHAQQASArgBA3IgCArQgCAQgFAOQgKAwgTAoQgEAFgBAFIgRAYQgEAIgGAGQgOARgSAQQgSANgTANIgJAEQgcAOgeAEIgUAEQgIAAgHgCIgJgBQgNgBgKgEQgMgFgKgIIAVgiQAagnAMghQAGgYADgXIAAgEQABgegNgZQADAggHAfIgEAJQgCAUgIASQgKAYgMAWQgnA/g6AiIgKADQgcAJgiAAg");
	this.shape_21.setTransform(3,32.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#D58A41").s().p("AABJkIAYAAIACAAIgMAAIgOAAgAksIAQgqgXgmgoQhlhkgpiRQgxirAnitQAmi0B2h5QB7iAC1ggQC2ghCTBLQCUBLBkCNQBlCNgCDsQgDDuhKBXQgrAzgxAvQABgGAEgEQATgoAKgxQAFgNACgQIACgrQABg4gSgrIgHgQQgfg2hIgpQhFglhQgIIgUgCIgNAAQhVgDhMAhQhQAig9BGQgpAwgZA2QgKAagIAcQgIAcgBAcQgFAdAFAcQABAVAHATQALAkAXAZQAMANAQAMQhMgFhEgpgAqiCeIgLgBQgcgagWgiQgggzgTg+QgfhvAVhbQAch1BwhGQBeg6BjALQhUBFgxB3QgtBpgCBtQAABqApBZQggANgjAAIgFAAgAK4h7QgRhog/h2Qg+h2hIgPIAFgKQAvgTA4AJQBMAOA0A0QAwAxAYBMQASBDgCBNQgGB4g5BvQgDADgFABQgcAQghADQAqhcgUh6g");
	this.shape_22.setTransform(-5.6,-86);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CD8136").s().p("AqgB0QAChuAthnQAxh3BUhFIAFgEQAEAEAFACQAAABABAAQAAABABAAQAAAAABAAQABABAAAAQipC0AAD0QAABVAVBPIgJAEQgphaAAhqgAKJCQQAAkEi/i5IgJgJIAEgBQBIAPA+B1QA/B3ARBnQAUB6gqBdIgHABQALg4AAg7g");
	this.shape_23.setTransform(-2.7,-102.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CB7E34").s().p("AiwJjQhAgRg4gcQgSgKgRgLIAAgFQBEApBLAEQAOALASAJQAWAMAbAHQgjgFgigIgAliITIgjgZQgigagjggQgZgZgXgbIgBgBQgzg+gfhFQgXgtgNgyQgUhPAAhWQAAjzCpi1IACgBIAQgTQASgQATgPQCyiYDyAAQEBgBC6CqIABADIAEACIAJAJQC+C5AAEDQAAA8gKA3QgBAKgEALQgNBFgfA+QguBghVBRQgEAGgFADQgPAOgQAOIgdAXIARgYQAwguArgzQBKhYADjtQADjthliMQhliOiThLQiThKi3AgQi1Agh6CAQh2B5gnC0QgmCuAxCrQAoCQBlBkQAnAoAqAYIAAAFIgXgOgAlLIcIAAAAg");
	this.shape_24.setTransform(-2.5,-88.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E7A05B").s().p("Ar2FIQhAgogahWQgXhLANhaQAJhIAZg8QAchEAtguQAygyBDgWQBFgWA/AVQAZAIAYANIgFAFQhigLheA5QhwBHgcB0QgWBbAgBvQATA+AgAzQAVAiAdAaQgsgDgjgVgAMrAgQAChMgShDQgYhMgvgyQg0g0hMgOQg5gJguAUQATgaAhgQQAvgWAyAGQAwAHAqAfQAnAeAcAqQAwBJAMBzQANBpgWBMQgMAvgdAjQgZAigjATQA4hwAGh4g");
	this.shape_25.setTransform(-6.2,-105.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");
	this.shape_26.setTransform(0,0,0.9,0.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_27.setTransform(-0.1,-0.1,0.613,0.613);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_28.setTransform(-0.1,-0.1,0.632,0.632);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#003366").s().p("A7JbKQrQrQAAv6QAAv5LQrQQLQrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArQrQg");
	this.shape_29.setTransform(0,0,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-225.6,-225.6,451.3,451.3);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgwB0QgWgIgPgRQgQgPgHgWQgIgXAAgbQAAgaAJgXQAKgYAPgSQARgQAVgLQAWgKAYAAQAWAAAUAJQATAJAQAQQAPAPAKAVQALAVACAZIi6AaQABAQAHAMQAFAMAKAIQAJAJANAFQAMADAOAAQALABALgEQAMgDAJgHQAKgHAGgKQAIgKACgOIAqAIQgFAVgMAPQgLAQgPALQgOAMgRAGQgTAHgSAAQgcAAgWgJgAgShTQgKADgLAIQgKAHgIANQgHAMgEATICBgPIgBgFQgIgVgOgMQgPgNgVAAQgJABgLADg");
	this.shape.setTransform(70.2,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AhkA2IgCg2IgBgpIgBggIgCguIAyAAIAAA+QAIgMAJgLQAJgLAKgJQALgIALgFQAMgGANAAQAQgBANADQAMAEAJAHQAIAHAGAKQAFAJAEAKIAEAUIACASQABAkAAAmIgCBOIgsgCIADhFQACgjgCgiIgBgKIgCgOQgCgGgDgHQgDgHgGgFQgFgFgHgDQgIgCgKABQgRADgQAWQgSAWgUAqIABA4IABAfIABARIgvAGIgChDg");
	this.shape_1.setTransform(43.1,5.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgsB2QgVgJgPgQQgRgSgIgXQgKgXABgdQgBgbAKgYQAIgYARgQQAPgRAVgJQAVgJAXAAQAYAAAVAKQAVAJAQARQAQARAIAYQAJAXAAAaQAAAbgJAYQgIAXgQAQQgQASgVAJQgVAKgYAAQgXAAgVgJgAgfhQQgNAIgIANQgJANgEAQQgDAPAAAPQAAAQADAPQAFAQAJANQAIAMANAIQANAIARAAQASAAANgIQANgIAKgMQAIgNAEgQQAEgPAAgQQAAgPgEgPQgDgQgJgNQgJgNgNgIQgNgIgTAAQgSAAgNAIg");
	this.shape_2.setTransform(16.3,5.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgvC0QgKgCgJgFQgLgEgJgHQgKgHgIgKQgIgKgFgNIAqgTQAEAKAFAGQAGAHAHAFQAGAEAIADQAHADAGABQAQADAQgCQAQgDALgGQALgGAIgIQAHgIAGgIQAFgJACgHIAEgOIABgKIAAgdQgNANgOAGQgOAHgMADQgNADgNABQgZAAgVgJQgXgJgPgQQgRgQgIgXQgKgXABgdQgBgdAKgXQALgXAQgQQARgQAUgIQAWgJAVAAQANACAPAFQANAEAOAIQAPAHANANIAAgrIArABIgBD4QgBAOgEAOQgDAOgHANQgGANgLALQgJAMgNAJQgOAJgQAGQgQAFgTACIgGAAQgXAAgUgGgAggiDQgNAHgKALQgJALgFAPQgFAQgBARQABASAFAPQAFAPAKAKQAJAKAOAGQANAGAQAAQANAAANgFQAOgEALgJQALgIAIgNQAHgMACgPIAAgcQgCgQgHgMQgIgNgLgJQgMgJgOgFQgNgFgNAAQgQAAgNAGg");
	this.shape_3.setTransform(-23.9,10.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgbCiIABg3IABhEIAChcIArgBIgBA3IgBAvIAAAnIgBAdIgBAugAgLhmQgGgDgEgEQgEgEgDgFQgCgGAAgHQAAgGACgGQADgFAEgFQAEgDAGgDQAFgCAGgBQAGABAGACQAFADAFADQAEAFACAFQADAGAAAGQAAAHgDAGQgCAFgEAEQgFAEgFADQgGACgGAAQgGAAgFgCg");
	this.shape_4.setTransform(-42.9,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgTClQgNgDgOgGQgOgHgPgMIAAAcIgoABIgDlMIAvgCIgBB/QAMgMAOgGQANgHAMgDQANgEAMAAQAOAAAPAEQAOAEANAIQANAIALALQALAKAIAOQAIANAEAOQAEAQAAAQQgBATgFAQQgFARgIAOQgIANgLALQgLALgMAIQgMAHgNAFQgNADgNAAQgMAAgOgEgAgZgbQgMAEgJAHQgJAIgHAIQgHAKgEALIgBA0QAEANAHALQAIAKAJAHQAJAHALAEQALADALAAQAPABAOgGQAOgGAKgLQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgKQgJgMgOgGQgOgHgQAAQgNAAgMAFg");
	this.shape_5.setTransform(-62.3,0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("ArAFFQhrAAhNhNQhMhLAAhsIAAiBQAAhsBMhMQBNhMBrAAIWBAAQBsAABLBMQBNBMAABsIAACBQAABshNBLQhLBNhsAAg");
	this.shape_6.setTransform(0,3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-96.5,-36.3,193,72.8);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#585153").s().p("AgCAEIgBgCIAAgBQgBgBAAAAQAAAAAAAAQABgBAAAAQAAgBAAAAQABAAAAgBQABAAAAAAQABAAAAAAQAAAAAAAAQAEAAABADQAAACgDABIgCABIAAAAIgCAAg");
	this.shape.setTransform(15.3,-39.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgEAcQgLgGgLgEQgHgIAAgKQAAgLALgKQAJgJANAAQAOAAAKAJQAKAKAAALQAAANgKAIQgKAKgOgBIgEgCgAAKgSQAAAAAAABQAAAAAAAAQAAABAAABQAAAAAAABIAAAAIABACIADABIACgBQADgBAAgDQgBgDgEAAIgBgBIgDACg");
	this.shape_1.setTransform(13.9,-37.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B66D26").s().p("AgWAWQgLgJAAgNQAAgMALgJQAJgJANAAQAOAAAKAJQAKAJAAAMQAAANgKAJQgKAIgOAAQgNAAgJgIgAAKgSQAAAAAAAAQAAABAAAAQAAABAAAAQAAABAAAAIAAABIABACIADABIACgBQADgBAAgDQgBgDgEAAIgBgBIgDACg");
	this.shape_2.setTransform(13.9,-37.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#585153").s().p("AgBAEIgCgBIAAgCQgBAAAAgBQAAAAAAAAQAAgBABAAQAAgBABAAQAAgBABAAQAAgBABAAQAAAAAAAAQAAAAABAAQADABABADQAAACgCACIgCABIgBAAQAAAAAAAAQAAAAAAAAQAAAAgBgBQAAAAAAAAg");
	this.shape_3.setTransform(-1.6,-39.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C1819").s().p("AgaAZQgLgKABgPQgBgNALgKQALgKAPAAQAPAAAMAKQAKAKAAANQAAAHgCAGQgLADgKAEQgOAGgKAIQgJgDgHgGgAALgUQAAAAAAABQgBAAAAABQAAAAAAABQAAABAAAAIABACIABABQABAAAAABQABAAAAAAQABAAAAAAQAAAAABAAIACgBQACgCgBgDQAAgDgDgBQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAgBABg");
	this.shape_4.setTransform(-3.1,-37.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B66D26").s().p("AgaAZQgLgLABgOQgBgNALgKQALgLAPAAQAPAAAMALQAKAKAAANQABAKgGAIIgSAHIgSAKQgMgCgKgIgAALgVQgCACABADIABABQAAABAAAAQAAAAAAAAQABAAAAAAQAAAAAAAAQABABAAAAQABAAAAAAQABABAAAAQAAAAABgBIACgBQABAAAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAgDgDgBIgBAAIgEABg");
	this.shape_5.setTransform(-3.1,-37.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6F7074").s().p("AAeAVQgEgBgIgEIgLgFQgKgDgZACQgIABgFgCQgJgDgBgIQgCgKAIgFQAGgEAOAAQAYAAAGACQAIABASAGQAMAGAEADQAIAIgDAIQgCAGgHACIgHABIgGgBg");
	this.shape_6.setTransform(7.6,-26.5,0.339,0.339);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1C1819").s().p("AgNAhQgEgCgDgEIgKgRQgIgOgBgJQAAgGADgGQADgFAFgEIAIAAIAaAAIAJABIAJACQAKACADAIQAEAGgBAKQgDAOgMAMQgMALgPADIgCAAQgHAAgCgCg");
	this.shape_7.setTransform(8,-24.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(51,51,51,0.298)").ss(1,1,1,3,true).p("AjZpCQABAAAAAAQAEgEADgFQAIgHAJgGQBOhEBrAAQBxAABSBLQAAABABAAQABABAAAAQABAAABAAQABgDABgCQAJgLAOgHQAVgKAWADQAVADATAOQARANAMASQAWAhAFAyQAGAvgKAiQgFAUgNAQQgLAPgQAIQgBABgCABQgNAHgOABQgCABgBAAAC+pPQADADABABQBUBSAABzQAAAagEAZQgBAEgBAFQgGAegOAcQgUAqgmAkQgBACgDACQgGAGgHAGQgHAFgGAFQgCAEgDACQgGAIgIAHQgIAGgIAFQgDACgBAAQgNAGgNACQgEABgFABQgDAAgDgBQgDAAgBAAQgGgBgEgCQgGgCgEgDIAAgBQgCgBgCgCQgCABgBACQgJAJgMAGQgEACgEACQgCABgCAAQgNAEgOAAIgBAAQAAAAgBAAQgFAAgFAAQgQAAgSgGIgBAAQgMgEgJgFQgIgEgGgFQgHgFgGgGQgKgLgFgQQgDgIAAgJQgCgNACgMQAAgNAEgMQADgMAFgMQALgYASgVQAbgfAkgPQAggOAmABQACAAADAAQAFAAAEABQAkADAeARQAgASAOAYQABADACAEQAIATgBAYQAAAJgBAKQgBAHgCAGQgEAWgJARQgBACgBADQgEAFgDAFAC+i+QBJA6BFBDQAqAqAZAgQAhArAOArQAVA7gRAvQgDAHgCAFQgHAOgKANQAIABAIABQAhAHAdAYQA2AugCBRQAABSg4BEQgXAcgaAUQAAAAgBAAIAAAAQgnAcgtAKIgBAAQg+ANgvgbQgLgHgLgJQgugmgGg/QgBgNAAgNQABhSA4hEQAjgsAtgXQAIgEAJgEQACgBACAAQAPgGASgEQAcgGAaADAHdIrQgRAUgSAOQgXARgbAIQgGABgGACQg4AMgogiQgDgDgDgDQgbgagEgpQAAgJAAgKQAAg7ApgxQAfgmAngQQANgFANgDQAUgEARABQABAAABAAQAgAEAZAUQAnAigBA6QgBA7goAygADcCAIAIAMQAXAiANAQQAVAbAWASQAOAMAQAHADcCAQgBAIAAAIQgDAZgFAaQgKAygWAsQgWAugiAiQgKAKgKAJQAKASANAZQAUAiATAWQAJAJAKAJACRiTQAuBFASBRQAOA/gDA+ABnGEQgcAXghAQQguAVgxAAQgogBgrgPQAWBOgeBDQgEAHgDAHQgMAVgPAPQgRARgWAKQgOAGgQAEQgMADgLABQgPABgOgBQgbgCgagMQgSgIgSgMQgJgGgIgGQg4gugYhJQgahTAfhIQAJgSAKgPQAKgMALgKQAbgXAlgJQAJgDAIAAQARgDASACQABAAABgBQAggkALgvQACgJACgIQAJgmAIglQAHgfAMgeQACgEABgEQAFgLAFgJQAGgLAGgKQAGgNAIgOQALgRALgRQAEgGAEgGQAFgKAIgIQACgDACgCQAHgJAJgIQgIgEgIgFQgFgDgFgEQgHgEgIgGQgPgMgQgOQgLgLgKgMQAAgBAAAAQgXgbgNgeQgLgUgFgWQgJgjAAgmQAAhsBKhQAkak9QgCAAgCABQgPAGgRAAQgDAAgCgBQgTgBgPgJQgdgSgLgmQgKghAFgoQAEggALgbQANgeAUgUQAWgWAdgKQAfgJAcAJQALADAKAGQACACACABQABABABAAAjmjZQgEADgEADQgsAfggAYQgoAegmAiQgiAggfAjQggAjgZApQgYAogMArQgNAvALAvQALAxAuAZQARAKARADAiZiXQAAgBAAgCAg2h0QgPgDgPgDQgcgIgZgMAgIhuQgFABgGgBAlEEBQAYACAXAKQAVAJAVANQBGAwAaBTQACAGABAEAi7HaQABAEABAEQAOAygPArQgCAGgDAFQgGAQgLAMQgWAbgjAIQg0ANgygjQgfgVgSgdQgNgUgIgZQgTg8AXg0QAGgNAHgKQAXgfAmgJQAUgEASACQAhADAfAVQAzAjATA8g");

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#552D15").s().p("AAcA8QgTgMgPgWQgNgQgPgaIgPgYIgJgKIABgQIAIAMQAXAiANAQQAUAaAWASQAOAMAQAHQgJAEgIAEIgOgHg");
	this.shape_9.setTransform(27.8,19.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#9C5824").s().p("AhtCtQgTgbgMgfQgSguACgxQACgvAVgsQAWgtAkggQAWgTAXgKQAagMAYAAQArgBAkAgQAjAdAOAsQARA3gPA8QgOA5gnAvIgMAOIgBABIAAAAQgnAcgtAJIAAAAQgUAEgTAAQgmAAgggRgAAQhzQgNACgMAFQgnAQgfAmQgpAyAAA6IAAATQAEAoAbAbIAGAGQAoAhA3gLIAMgEQAbgIAXgQQASgOARgUQAogyABg6QABg7gnghQgZgVgggDIgCAAIgHgBQgOAAgQAEg");
	this.shape_10.setTransform(36.8,45);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F3E2B9").s().p("Aj5BDQgMgqgagfQgZgdgjgMQgngNgdASQgPAJgKASQgJAPgEAUQgEAQABAVQAAAMADAaIABAKQgNgUgHgZQgUg8AXgzQAGgNAIgKQAWgfAngJQATgEATACQAgADAgAVQAzAjASA7IACAIQAPAygQArIgEALQgHAQgLAMQAFglgKgmgAGeApQAQhBgZg3QgHgOgIgFQgNgJgbAMQg2AYgaAkQgaAjgEA1QgBAZADAnQgagagEgpIgBgTQABg6AogxQAfgmAngQQANgFAOgDQAUgEAQABIADAAQAfAEAZAUQAnAigBA6QAAA6goAyQgRAUgTAOQgXARgaAIQAqgqAPg7g");
	this.shape_11.setTransform(3.6,47.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#834719").s().p("AmiC7IgRgMQg4gtgYhKQgahSAfhHQAJgTAKgOQAKgNALgKQAbgXAlgIQAJgDAIgBQARgCASACQAYACAXAKQAVAIAVAOQBGAvAaBUIADAKQAWBNgeBDIgHAOQgMAUgPAPIAJgOIAJgaQAJgZABgdQABgogMgpQgMglgYgjQglg6gwgTQglgPgpAJQgpAJgdAcQgcAbgLAoQgLAoAJAmQAKAmAQAdQANAYAQAOIAHAEQAcAmATATIAKAKQgSgJgSgMgADOCfQguglgGg/QgBgNAAgOQABhQA4hFQAjgrAtgXQAIgFAJgDIAEgBQAPgHASgDQAcgGAaACIAQADQAhAHAdAYQA2AugCBQQAABRg4BFQgXAcgaATIAMgOQAngvAOg5QAPg8gRg3QgOgsgjgdQgkgggrABQgZAAgaAMQgXAKgWATQgkAggWAtQgVAsgCAvQgCAxASAuQAMAfATAbQgLgIgLgJg");
	this.shape_12.setTransform(3,44.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#B66829").s().p("Ag9EHQgKgFgLgLIgRgVIgognQgagYgNgRQgagigMgyIAPhqQAGgtAEgTIACgIIADgIIAKgVIAMgVIgIAmQgGA4AMA1QAMA2AhAlQAlAsAwAEQAlADAogYQAhgVAaglQAzhEAGhaQAFhKgahAIgCgHIAGABIAJgCQANgCANgGQASAnAKAvQAKAyAEBFQACA5gEAmQgFA1gRAoQgZA7g1ApQg1Aog+AIQgMACgLAAQgXAAgPgJg");
	this.shape_13.setTransform(-5,13.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C27333").s().p("AgcDBQgvgEglgsQgiglgLg3QgMg1AGg3IAHgmIAOgbIAWgiIAIgMQAGgKAIgIIAEgFIAAALQADAfAMAeQAQAmAaAVQAQALARAEQASAFASgFQAQgFAPgOQAMgLALgSQAfgvALg2QAFACAFABIAEAAIACAHQAbBAgFBKQgHBZgyBFQgbAlghAVQgjAVghAAIgJAAg");
	this.shape_14.setTransform(-5.8,6.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#C47B40").s().p("AgUBRQgSgEgPgLQgagVgQgnQgMgcgDggIgBgKIAQgSQAZANAcAHIAeAGIABAAQARAGAQABIALAAIABAAQAPAAANgEIAEgCIAIgDQAMgHAJgJIADgDIAEAEIAAAAQAEAEAGACQgLA2gfAtQgMATgLALQgPANgRAFQgJADgJAAQgIAAgJgCg");
	this.shape_15.setTransform(-4.2,-6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E7D6AC").s().p("AAKANIAKgTQADgIABgJIACgEQADgOgBgOQAFALAAANIAAACQgBAKgDALQgFANgMASIgJAPIAAgBIgDgDIgDADQgJAJgMAGIgIAEQAagPAQgcg");
	this.shape_16.setTransform(5.7,-17.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#7A3B14").s().p("AlYDFQAFgKAMgTIAQgdQAMgXAFghQAEgTADgnIAEgpIACgRQAFghALgYQALgZAWgbIArgwIAUgXIAQALIAKAGIAPAJIgPASIgEAFQgIAIgGAKIgIAMIgWAiIgOAbIgLAUIgKAWIgEAHQgMAdgHAfIgQBLIgFARQgLAwggAjIgBACQgSgCgSADQACgJAEgIgAE8C/QgVgSgVgbQgOgRgWghIgJgNQAEg9gOg+QgThSgthFQAIgIAGgHQAjAkATA/QAHAaAIAnIAMBCQAVBhAuA4QAPASATAOIgEABQgRgHgOgMg");
	this.shape_17.setTransform(-0.9,4.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#D9955D").s().p("Am9D6QgtgZgMgxQgLgvANgvQANgrAXgnQAagpAfgjQAfgkAjggQAlghApgfQAfgYAsgfIhWBQQg/A6gmAsQgmAqgVAnQgaAvgEAuQgEA0AZAsQAWAlAgAPQgLAKgKANQgSgEgRgKgAHaAwQgbhDg0g1QgRgSgcgXQgggagPgNQgygpgagkIAOgNQBJA6BEBDQArAqAYAiQAiAqAOAqQAUA7gRAwIgEALQgBg5gVg4g");
	this.shape_18.setTransform(-5,5.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#BE6C2D").s().p("AkHG7QgbgCgagMIgKgKQgTgTgdglIgGgEQgRgPgNgYQgQgdgJgmQgJgmALgoQALgoAbgcQAegcApgIQApgJAkAPQAwATAmA5QAXAkAMAmQANApgCAoQgBAcgJAaIgJAZIgIAPQgSARgWAKQgOAGgQAEQgLADgMABIgOAAIgOAAgAlDCEQgnAJgWAfQgIAKgGANQgXA0AUA8QAHAZANAUQATAdAeAVQAyAjA0gNQAkgIAVgbQALgMAHgQIAEgLQAQgrgPgyIgCgIQgSg8gzgjQgggVgggDIgMgBQgNAAgNADgADJEGQgTgWgUgiIgXgrIATgTQAigiAXguQAVgsAKgxQAGgaACgZIAJAKIAQAYQAPAbAMAQQARAVATAMIANAHQgtAXgjAsQg3BEgCBSQAAANACANQgLgJgIgJgAnKAQQgZgrAEg0QAEguAagwQAVgnAmgqQAmgsA/g6IBWhQIAJgGIAVAXIAeAaIgUAWIgrAwQgWAbgLAZQgLAYgFAhIgCASIgEApQgDAngEAUQgFAggMAXIgQAdQgMASgFAKQgEAIgCAJQgIAAgJADQglAJgaAXQgggPgWglgAFXgDQgug3gVhiIgMhCQgIgogHgZQgThAgjgkIAEgGIANgKQAaAlAyApQAPANAgAaQAcAXARASQA0A0AbBFQAVA4ABA5QgIANgKANQgagDgcAGQgRAEgPAGQgTgOgPgRg");
	this.shape_19.setTransform(-3.5,22.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#9F561B").s().p("AhUEbIgDgLQgahThGgvQgVgOgVgJQgXgJgYgDIACgBQAggkALgvIAEgRIARhKQAHgfAMgeIgCAJQgEASgGAuIgPBqQAMAxAaAjQANAQAaAYIAoAnIARAVQALALAKAFQAXANAlgFQA/gJA1goQA1gpAZg7QARgoAFg1QAEgmgCg5QgEhFgKgyQgKgvgSgmIAEgDIAQgLQAuBFASBTQAOA/gDA9IgBAQQgDAYgFAaQgKAygWAsQgWAugiAjIgUATQgcAWghARQgvAUgwAAQgoAAgrgPg");
	this.shape_20.setTransform(-5.2,15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCF4E0").s().p("AmAHlQgfgVgSgdIgBgKQgDgaAAgMQgBgVADgQQAEgVAJgPQAKgSAPgJQAegSAmANQAjAMAZAdQAbAgALAqQAKAmgEAlQgWAbgjAIQgOADgOAAQgmAAgkgZgAELHFIgGgGQgDgnABgZQADg2AagjQAagkA2gYQAbgMANAJQAIAFAHAOQAaA3gRBCQgPA7gqAqIgMADQgPADgNAAQgmAAgegZgAgZj8IgBAAIgKAAQgQAAgSgGIgBAAQgMgEgJgFQgIgEgGgFQgHgFgGgGQgKgLgFgQQgDgIAAgJQgCgNACgMQAAgNAEgMIAIgYQALgYASgVQAbgfAkgPQAhgOAlABIAFAAIAJABQAkADAeARQAgASAOAYIADAHQAIATgBAYIgBATIgDANQgEAWgJARIgCAFIgHAKIgFAGQgGAIgIAHIgQALIgEACQgNAGgNACIgJACIgGgBIgEAAQgGgBgEgCQgGgCgEgDIAJgPQAMgSAFgOQADgLABgKIAAgCQAAgNgFgLQABAOgDAOIgCAEQgBAJgDAIIgKAUQgRAcgaAPIgEABQgMAEgPAAg");
	this.shape_21.setTransform(1.7,14.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E7A05B").s().p("AlOCRQgcgSgMgmQgKghAGgnQAEggALgaQAMgeAUgUQAWgXAegJQAegKAcAJQALAEALAGIgCACQgsgFgpAZQgyAfgMA0QgKAnAOAxQAJAcAOAWQAJAPANAMQgTgCgQgJgAFmAOQABghgIgdQgKgigVgWQgXgXgigGQgZgEgUAJQAIgMAPgHQAVgJAWACQAVADASAOQASANAMATQAVAgAFAzQAGAugKAhQgFAVgNAPQgLAPgPAJQAZgyACg1g");
	this.shape_22.setTransform(-2.3,-46.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CD8136").s().p("AkoAzQABgwAUgtQAVg1AlgeIACgCIAEADIACABQhKBPAABrQAAAmAJAjIgEACQgSgoAAgvgAEeA/QAAhyhUhRIgEgEIACgBQAgAHAbA0QAcA0AHAtQAJA2gSApIgDAAQAEgYAAgbg");
	this.shape_23.setTransform(-0.8,-45.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D58A41").s().p("AABEOIAKAAIABAAIgGAAIgFAAgAiEDiQgSgKgRgSQgtgsgShAQgVhLARhMQARhQA0g1QA2g5BQgOQBQgOBBAhQBBAhAsA+QAtA+gBBoQgCBpggAnQgTAWgWAVIACgFQAJgRAEgWIADgNIABgTQABgYgIgTIgDgHQgOgYgggSQgegRgkgDIgJgBIgFAAQgmgBghAOQgjAPgbAfQgSAVgLAYIgIAYQgEAMAAANQgCAMACANQAAAJADAIQAFAQAKALQAGAGAHAFQgigCgegSgAkpBGIgFgBQgMgLgKgPQgOgXgIgaQgOgxAJgpQANgzAxgfQAqgaArAFQglAfgVA0QgUAugBAxQAAAuASAnQgOAGgPAAIgDAAgAEzg2QgHgugcg0Qgbg0gggGIACgFQAVgIAZAEQAhAGAXAXQAVAWALAhQAIAegBAiQgDA1gZAwIgDACQgNAHgOABQASgogJg2g");
	this.shape_24.setTransform(-2.1,-38.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CB7E34").s().p("AhNEOQgcgIgZgMIgQgJIAAgDQAeASAiACQAGAFAIAEQAJAFAMAEIgegGgAicDqIgPgKIgfgaIgVgXIAAgBQgXgbgNgeQgLgUgFgWQgJgjAAgmQAAhrBKhQIABAAIAHgJIARgNQBOhEBrAAQBxAABSBLIABABIABABIAEAEQBUBSAAByQAAAagEAZIgCAJQgGAegOAcQgUAqgmAkIgEAEIgNAMIgNAKIAHgKQAWgVATgWQAggnAChpQABhogtg+Qgsg+hBghQhBghhQAOQhQAOg2A5Qg0A1gRBQQgRBMAVBLQASBAAtAsQARASASAKIAAADIgKgHgAiSDuIAAAAg");
	this.shape_25.setTransform(-0.7,-39.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-56.8,-67.8,113.7,135.7), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgHgHQgGgGgEgKQgDgKAAgMQAAgKAEgKQAEgKAHgHQAGgIAKgEQAJgEAKAAQAJAAAJADQAIAEAHAHQAHAHAEAIQAEAKABAKIhPALQABAHACAEQADAGAEADQAEAEAFACQAGACAFAAQAFAAAEgBQAFgCAEgDIAIgIQACgDACgHIASAEQgDAJgFAGQgEAHgHAGQgGAEgHADQgIACgIABQgMgBgJgDgAgHgjIgJAEQgEAEgEAFQgDAFgCAJIA3gHIAAgCQgEgJgGgFQgGgGgJABIgIABg");
	this.shape.setTransform(82.8,-10.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgqAXIgBgXIgBgRIAAgOIgBgUIAWAAIAAAbIAHgKIAIgIQAEgEAFgCQAFgDAGAAQAGAAAGABQAFACAEADIAGAHIAEAIIACAJIABAIIAAAfIgBAhIgTAAIACgeIAAgdIgBgFIgBgFIgCgGIgDgFQgDgDgDgBQgDgBgFABQgHABgGAKQgIAJgJASIABAYIAAANIAAAHIgTADIgBgdg");
	this.shape_1.setTransform(71.2,-11);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSAzQgJgEgHgHQgHgHgEgKQgEgKAAgNQAAgLAEgKQAEgLAHgGQAHgIAJgDQAJgFAJAAQAKAAAJAFQAKAEAGAHQAHAHAEALQAEAJAAALQAAAMgEAKQgEAJgHAIQgGAHgKAFQgJADgKAAQgJAAgJgDgAgNgiQgFADgEAGQgDAFgCAIQgCAGAAAGQAAAGACAIQACAGAEAFQADAGAGADQAGAEAGAAQAIAAAGgEQAFgDAEgGQAEgFACgGIABgOQAAgGgBgGQgCgIgEgFQgDgGgGgDQgGgDgIAAQgHAAgGADg");
	this.shape_2.setTransform(59.7,-11);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTBOIgJgDIgIgFIgIgIQgDgEgDgGIASgHIAFAGIAFAFIAGADIAGACQAHACAGgBQAHgCAEgCQAFgDADgDQAEgDACgEIADgHIACgGIAAgEIAAgNQgFAGgGADIgLAEIgLABQgLAAgJgDQgJgEgHgHQgHgHgEgJQgEgKAAgNQAAgMAEgKQAFgKAHgHQAHgHAJgDQAJgEAJAAQAFABAHACQAFACAGADQAHADAFAGIAAgTIASABIAABqIgCAMQgBAGgDAFIgHALIgKAJQgGADgHADQgHACgIABIgDAAQgJAAgIgCgAgNg3QgGACgEAFQgEAFgCAHQgCAGAAAIQAAAHACAHQACAGAFAEQAEAEAFADQAGADAHAAQAFAAAGgDQAGgCAEgDQAFgEADgFQADgFACgGIAAgNQgBgGgEgGQgDgFgFgEQgFgEgGgCQgGgCgFAAQgGAAgGADg");
	this.shape_3.setTransform(42.4,-8.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgLBGIABgYIAAgdIABgnIASgBIAAAYIgBAUIAAAQIgBANIAAAUgAgEgrIgEgDIgDgEIgBgFIABgGIADgEIAEgDIAEgBIAFABIAEADIADAEIABAGIgBAFIgDAEIgEADIgFABIgEgBg");
	this.shape_4.setTransform(34.2,-13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgIBHIgLgEQgHgDgFgGIAAANIgRAAIgCiOIAUgBIAAA3QAFgFAGgDIAKgEQAGgCAEAAQAHAAAGACIAMAFIAKAIQAFAEADAGIAGALIABAOQgBAIgBAHQgCAHgEAGQgEAGgEAFQgFAFgFADQgFADgGACQgFACgGAAIgLgCgAgKgLIgJAFQgEADgDADIgFAJIAAAWQABAGADAEIAIAIQAEADAFABQAEACAFAAQAGAAAFgDQAHgCAEgFQAEgEADgHQACgGABgHQAAgHgBgGQgDgHgEgEQgEgFgGgCQgGgDgHAAQgFAAgFACg");
	this.shape_5.setTransform(25.9,-13.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgHgHQgGgGgEgKQgDgKAAgMQAAgKAEgKQAEgKAHgHQAGgIAKgEQAJgEAKAAQAJAAAJADQAIAEAHAHQAHAHAEAIQAEAKABAKIhPALQABAHACAEQADAGAEADQAEAEAFACQAGACAFAAQAFAAAEgBQAFgCAEgDIAIgIQACgDACgHIASAEQgDAJgFAGQgEAHgHAGQgGAEgHADQgIACgIABQgMgBgJgDgAgHgjIgJAEQgEAEgEAFQgDAFgCAJIA3gHIAAgCQgEgJgGgFQgGgGgJABIgIABg");
	this.shape_6.setTransform(8.9,-10.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAYBHQADgKABgHIACgQIABgPQgBgJgDgHQgDgHgEgEQgFgFgFgBQgFgDgFAAQgFABgFADQgFACgFAEQgFAFgEAIIgBA+IgSAAIgBiOIAUgBIAAA4QAFgFAGgEIAKgEIALgDQALAAAIAEQAJADAFAIQAHAGADAKQAEAKABAMIAAAOIgBANIgBANIgDAKg");
	this.shape_7.setTransform(-2.9,-13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgLgKIgfABIAAgRIAggBIABgpIASgBIgBAqIAkgCIgCARIgiABIgCBQIgSABg");
	this.shape_8.setTransform(-13.7,-12.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKgKIghABIABgRIAggBIABgpIARgBIgBAqIAkgCIAAARIgkABIAABQIgTABg");
	this.shape_9.setTransform(-27.7,-12.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgQAyQgKgEgIgIQgIgGgEgLQgEgJgBgMQAAgFACgIQACgGAEgGQADgGAFgFQAFgGAGgDQAGgEAHgCQAHgCAHAAQAIAAAIACQAGACAHAEIALAJQAFAFADAGIABABIgQAJIAAgBIgGgIQgEgDgEgDQgEgDgFgBIgKgBQgHAAgGACQgHADgFAFQgFAFgDAHQgCAGgBAHQABAIACAGQADAHAFAFQAFAFAHADQAGADAHAAQAFAAAEgBIAJgEIAIgGIAGgHIAAgBIAQAJIAAABQgEAGgFAEQgFAFgHAEQgGAEgGACIgPABQgJAAgKgEg");
	this.shape_10.setTransform(-37.9,-10.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgHgHQgGgGgEgKQgDgKAAgMQAAgKAEgKQAEgKAHgHQAGgIAKgEQAJgEAKAAQAJAAAJADQAIAEAHAHQAHAHAEAIQAEAKABAKIhPALQABAHACAEQADAGAEADQAEAEAFACQAGACAFAAQAFAAAEgBQAFgCAEgDIAIgIQACgDACgHIASAEQgDAJgFAGQgEAHgHAGQgGAEgHADQgIACgIABQgMgBgJgDgAgHgjIgJAEQgEAEgEAFQgDAFgCAJIA3gHIAAgCQgEgJgGgFQgGgGgJABIgIABg");
	this.shape_11.setTransform(-49.3,-10.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAGBRIgIgGIgHgHIgEgJQgFgLgBgNIADh1IASAAIgBAeIgBAZIAAAUIAAAPIAAAaQAAAIABAHIADAFIAEAGQACACADABQADACAEgBIgDATQgGAAgFgCg");
	this.shape_12.setTransform(-57.1,-14.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgHgHQgGgGgEgKQgDgKAAgMQAAgKAEgKQAEgKAHgHQAGgIAKgEQAJgEAKAAQAJAAAJADQAIAEAHAHQAHAHAEAIQAEAKABAKIhPALQABAHACAEQADAGAEADQAEAEAFACQAGACAFAAQAFAAAEgBQAFgCAEgDIAIgIQACgDACgHIASAEQgDAJgFAGQgEAHgHAGQgGAEgHADQgIACgIABQgMgBgJgDgAgHgjIgJAEQgEAEgEAFQgDAFgCAJIA3gHIAAgCQgEgJgGgFQgGgGgJABIgIABg");
	this.shape_13.setTransform(-65.9,-10.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgLBLIgOgFIgNgGIgNgJIANgQQAHAHAIADQAHAEAFACQAHACAFABQAHAAAFgBQAGgBAFgDQAEgDACgDQADgEAAgEQABgEgCgCIgEgGIgHgEIgIgDIgJgDIgIgBIgLgDIgMgEQgGgCgFgEQgFgDgDgFQgEgFgCgHQgCgHABgJQAAgIADgGQADgGAEgEQAEgFAFgDIALgGIAMgDIAMgBQAJAAAIADIAIACIAJADIAIAFIAIAGIgLARIgGgGIgHgEIgGgEIgHgCQgHgCgHAAQgHAAgHADQgGACgFAEQgEAEgDAFQgCAFAAAFQAAAFADAEQADAFAFADQAFAEAGACQAHADAHABIANACIANAEIAMAGIAIAIQAEAFABAGQACAGgBAHQgBAHgDAFQgCAFgFAEIgJAGIgKAEIgLACIgLABQgGAAgHgBg");
	this.shape_14.setTransform(-77.7,-12.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#990000").ss(3,1,1).p("AxiAjQABA5AhAoQAhAoAvAAIfkgDQAuAAAggoQAhgnAAg6IAAhCQAAg5ghgoQghgogvAAI/kADQguAAghAoQghAoABA5g");
	this.shape_15.setTransform(3.3,-12.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0033").s().p("AxACEQghgogBg5IABhCQgBg5AhgoQAhgoAuAAIfkgDQAvAAAhAoQAhAoAAA5IAABCQAAA6ghAnQggAoguAAI/kADQgvAAghgog");
	this.shape_16.setTransform(3.3,-12.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-110.4,-31.2,227.6,37.4), null);


(lib.bigcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#585153").s().p("AgCAEIgCgCIAAgBQAAgBAAAAQAAAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQABAAAAgBQABAAAAAAQAAAAAAAAQAEABABADQAAACgDACIgCABIAAAAIgCgBg");
	this.shape.setTransform(18.7,-42.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgFAfQgLgHgNgFQgIgIAAgLQAAgNAMgLQAKgJAPAAQAQAAAKAJQAMALAAANQAAAOgMAKQgKAKgQAAIgFgDgAALgVQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABIAAABIABACIAEAAIACgBQACgCAAgCQAAgEgEAAIgBAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAg");
	this.shape_1.setTransform(17.2,-41.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B66D26").s().p("AgZAYQgMgKAAgOQAAgOAMgJQAKgKAPAAQAQAAAKAKQAMAJAAAOQAAAOgMAKQgKAKgQAAQgPAAgKgKgAALgVQAAAAAAABQAAAAAAABQAAAAAAABQAAABAAAAIAAACIABACIAEABIACgBQACgCAAgEQAAgDgEAAIgCAAIgDABg");
	this.shape_2.setTransform(17.2,-41.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#585153").s().p("AgCAEIgBgBIgBgCQgBgBACgDQACgCACAAQAEACAAADQABACgCACIgDACIgBAAIgCgCg");
	this.shape_3.setTransform(-0.1,-43.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C1819").s().p("AgcAcQgNgLAAgQQAAgPANgLQAMgLAQAAQARAAANALQAMALAAAPQAAAHgDAGQgMAEgMAEQgOAGgMAJQgKgCgHgHgAANgXQgCADABADIAAABIACABQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAIACgCQACgCAAgDQgBgDgDgCQgDAAgCACg");
	this.shape_4.setTransform(-1.8,-41.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B66D26").s().p("AgcAbQgNgLAAgQQAAgPANgKQAMgMAQAAQARAAANAMQAMAKAAAPQAAALgGAJIgVAIIgUAKQgNgBgKgKgAANgXQgCACABADIAAACIACABQAAABABAAQAAAAABAAQAAAAABAAQAAAAABAAIACgBQACgCAAgDQgBgEgDAAIgCAAIgDABg");
	this.shape_5.setTransform(-1.8,-41.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6F7074").s().p("AAeAVQgEgBgIgEIgLgFQgKgDgZACQgIABgFgCQgJgDgBgIQgCgKAIgFQAGgEAOAAQAYAAAGACQAIABASAGQAMAGAEADQAIAIgDAIQgCAGgHACIgHABIgGgBg");
	this.shape_6.setTransform(10.1,-28.9,0.377,0.377);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1C1819").s().p("AgPAkQgEgCgEgFIgLgTQgJgPAAgKQAAgGADgHQADgGAGgEIAIAAIAeAAIAKABIAKACQALACAEAJQAEAHgCALQgDAQgNANQgOANgQACIgDABQgHAAgDgDg");
	this.shape_7.setTransform(10.6,-26.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(51,51,51,0.298)").ss(1,1,1,3,true).p("ADTqQQABgBABAAQABgDACgCQAJgNAQgIQAXgKAZADQAXADAVAQQATAOAOAVQAXAkAGA4QAHA0gLAlQgGAXgOARQgNARgRAJQgCABgCABQgOAIgQABQgCABgBAAQgBAFgBAFQgHAigPAeQgXAvgpAoQgCADgDACQgHAHgIAGQgHAGgHAGQgDADgCADQgHAJgJAHQgJAHgJAGQgDACgCAAQgOAHgOACQgFABgFABQgEAAgDgBQgDAAgCAAQgGgBgFgCQgGgCgFgEIAAgBQgCgBgCgDQgCACgCACQgJAKgNAHQgFACgEACQgDABgCABQgPAEgPAAIgBAAQgBAAAAAAQgGAAgGAAQgSgBgUgGIAAAAQgNgEgLgGQgJgEgHgGQgHgFgGgHQgMgMgFgSQgDgJgBgKQgCgOACgOQAAgOAEgOQAEgNAFgNQAMgbAVgXQAegiAngRQAlgQApABQADAAAEAAQAFAAAFABQAnAEAiASQAjAUAPAbQACADACAFQAJAVgBAbQAAAKgBALQgBAIgCAHQgFAXgKAUQgBACgBADQgEAGgEAGADTqQQADACACACQBdBbAAB/QAAAegFAbAjxqCQAAAAABgBQAEgEAEgFQAJgIAJgHQBXhLB3AAQB+AABbBTQAAABAAAAQACABAAABADTjTQBRBABMBLQAwAvAbAjQAlAwAQAwQAWBBgTA1QgCAHgDAGQgIAQgLAOQAJABAJABQAlAIAgAbQA8AzgCBZQgBBbg+BNQgZAfgdAVQAAAAgBABIAAAAQgrAfgzAKIgBABQhFAOgzgeQgNgIgMgKQgzgqgHhGAF2EbQADgBACgBQARgHATgEQAfgGAdADAISJpQgTAWgUAQQgaASgdAJQgHACgHACQg/ANgrglQgEgEgEgDQgdgegFgtQAAgKAAgLQAAhBAtg3QAjgqArgSQAPgGAPgDQAWgEASABQACAAABAAQAjAEAcAXQArAlgBBBQgBBCgsA3gAChikQAzBNAUBbQAQBGgEBEIAJAOQAZAlAPATQAYAeAYATQAPANASAJAD0COQgBAJAAAJQgDAcgGAcQgLA4gYAxQgZAzgmAmQgKAMgMAJQgfAaglASQgzAXg2AAQgsgBgxgRQAZBXgiBKQgEAIgEAIQgMAXgRARQgTASgZAMQgPAHgSAEQgNADgNABQgQACgPgCQgegCgdgNQgVgJgUgOQgJgGgJgHQg+gzgbhRQgdhdAjhPQAJgUAMgRQALgOAMgLQAdgZAqgKQAJgDAJgBQAUgCAUACQABgBAAgBQAkgoAMg0QACgJADgKQAKgqAJgpQAHgjAOghQACgEACgEQAFgMAGgLQAGgMAGgLQAIgPAIgPQAMgTAMgTQAFgHAEgGQAHgLAIgKQACgCADgDQAIgKAJgJQgJgFgIgFQgGgDgFgEQgIgFgJgHQgRgNgRgQQgNgMgLgNQgEADgFAEQgxAigjAbQgtAhgqAmQgmAjgjAnQgjAngcAuQgaAsgOAwQgOA0AMA0QANA3AyAcQATALAUADAByGvQALAUAPAcQAWAmAVAYQAKALALAJQgBgOAAgPQABhaA+hNQAngwAygZQAJgFAKgEAk6lhQgCABgDABQgQAHgTgBQgDAAgCAAQgVgCgRgKQgggUgNgqQgLglAGgsQAFgkAMgdQAOgiAWgWQAYgZAhgKQAigLAfAKQAMAEAMAHQACACADABQABABABAAAkAjxQAAgBAAAAQgZgegPgiQgMgWgGgZQgKgmAAgrQAAh4BThYAg8iBQgRgDgRgEQgfgIgcgOAiqioQAAgBAAgCAloEeQAaACAaALQAXAKAXAPQBOA0AdBdQACAGABAFAjQIPQABAEABAEQAQA4gRAwQgDAGgCAGQgIASgMAOQgYAdgnAKQg6ANg4gmQghgXgVggQgOgXgJgbQgVhDAZg6QAHgOAIgMQAZgiArgKQAWgFAUACQAkAEAjAYQA5AmAVBDgAgJh6QgGABgHgB");
	this.shape_8.setTransform(1.7,0.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#552D15").s().p("AAfBDQgVgOgRgZQgOgRgRgdIgRgbIgKgLIABgSIAKAOQAZAlAPATQAWAdAYATQAQANASAJQgLAEgJAFIgPgIg");
	this.shape_9.setTransform(32.6,22.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F3E2B9").s().p("AkVBKQgNgugegjQgbghgngNQgrgPghAUQgRALgLAUQgKARgEAVQgEATABAXQAAANADAdIACAMQgPgXgIgbQgWhDAag5QAGgOAJgMQAZgiAqgKQAWgFAVACQAkAEAjAYQA4AmAVBCIACAIQAQA4gRAwIgFAMQgHASgMAOQAFgqgLgqgAHLAtQAThIgdg+QgHgPgJgGQgPgJgeANQg8AagdAoQgcAngEA8QgCAcAEArQgegegEgtIgBgVQABhAAtg3QAigqAsgSQAOgGAPgDQAWgEATABIACAAQAjAEAcAXQAsAlgBBBQgBBBgtA3QgSAWgVAQQgaASgdAJQAvgvAQhBg");
	this.shape_10.setTransform(5.7,53.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#9C5824").s().p("Ah5C/QgWgdgNgiQgTgzABg2QADg1AYgyQAXgxApgjQAYgVAagMQAcgNAcAAQAvgBAoAjQAnAgAPAxQAUA9gRBDQgQA/grA1IgNAPIgBABIgBAAQgrAfgyAKIAAABQgWAFgVAAQgrAAgjgVgAASiAQgPADgNAFQgsASgiAqQgtA3gBBBIABAUQAEAtAeAeIAHAHQAsAlA9gNIAOgEQAdgIAagTQAVgPASgXQAtg3ABhBQABhBgsglQgcgXgjgEIgCAAIgKAAQgOAAgRAEg");
	this.shape_11.setTransform(42.6,50.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#834719").s().p("AnRDQIgTgOQg+gygbhSQgdhbAjhPQAJgVAMgQQALgOAMgLQAegaApgJQAKgDAJgBQATgDAUACQAbADAaALQAWAJAYAPQBNA1AdBdIAEALQAYBWghBKIgIAQQgNAWgQARIAJgQQAGgOAEgOQAKgdABgfQACgtgOgtQgNgqgbgnQgphAg2gVQgogRguAKQgtAKghAfQgfAegMAtQgMAsAKAqQALAqARAgQAPAbASAQIAHAFQAgApAVAVIALAMQgUgKgUgNgADkCxQgygqgHhGQgCgOAAgPQAChaA+hMQAngwAxgaQAJgFALgEIAEgBQARgHATgEQAggHAcADIASADQAmAIAfAaQA8AzgBBaQgBBag+BMQgaAfgcAWIANgQQArg0AQhAQARhDgUg9QgPgwgnghQgogjgvABQgdAAgcANQgaAMgYAVQgpAjgXAyQgYAxgDA1QgBA2ATAzQANAjAWAdQgNgIgMgKg");
	this.shape_12.setTransform(5.1,50.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#D9955D").s().p("AnuEWQgzgbgNg3QgMg1APgzQAOgxAagrQAcgtAjgoQAignAngjQApgmAtgiQAjgbAxgiIhfBZQhHBAgqAxQgqAvgYArQgcA0gFAzQgEA6AcAyQAYAoAjARQgMALgLAOQgTgEgTgLgAIOA1QgdhLg6g6QgTgUgfgaQgkgcgRgPQg3gugcgoIAPgOQBRBBBMBKQAvAvAbAlQAlAvAQAvQAXBCgTA0IgFANQgBg/gYg/g");
	this.shape_13.setTransform(-3.9,6.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C47B40").s().p("AgXBaQgTgFgRgMQgdgXgSgrQgNgggEgjIAAgMIARgTQAcAOAfAIIAiAHIAAAAQATAGASABIANAAIABAAQAQAAAPgEIAFgCIAJgEQANgHAJgKIAEgEIAEAEIAAABQAFAEAGACQgMA8giAzQgNAUgNAMQgRAQgSAFQgKADgKAAQgKAAgKgCg");
	this.shape_14.setTransform(-3,-6.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#9F561B").s().p("AhdE6IgEgLQgdhdhNg1QgYgPgWgJQgagLgbgDIACgBQAkgoAMg0IAFgTIAShTQAIgiANghIgCAJQgEAVgHAyIgRB2QAOA3AdAmQAOATAdAaQAhAgALAMIAUAXQAMAMAKAGQAaAPApgHQBHgJA7gtQA6gtAchCQATgsAGg7QAEgqgDhAQgDhMgMg4QgLg0gUgrIAEgCIATgNQAyBMAVBcQAPBGgEBFIgBARQgCAbgHAdQgLA4gYAwQgZA0glAmQgLALgLAKQgfAaglARQg1AXg1AAQgsAAgwgRg");
	this.shape_15.setTransform(-4.1,17.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#BE6C2D").s().p("AkkHsQgegDgdgMIgLgMQgVgVgggpIgHgFQgTgQgOgbQgSgggKgqQgLgrANgsQAMgtAfgeQAggfAugKQAtgKApARQA1AVAqBAQAaAnANArQAOAtgBAtQgCAfgJAdQgFAOgGAOIgJAQQgTATgZAMQgPAGgSAFIgaAEIgQABIgPgBgAlnCTQgrAKgZAhQgIAMgHAPQgZA5AVBDQAJAbAOAXQAVAgAhAXQA4AnA6gOQAngJAYgeQAMgOAIgSIAFgMQARgwgQg3IgCgJQgVhDg5gmQgjgXgkgEIgMgBQgPAAgPAEgADfEiQgVgYgWgmIgagvQAMgKAKgLQAmgmAZg0QAYgwALg3QAGgdADgcIAKALIARAbQARAeAOASQASAXAVAOIAPAIQgyAagnAwQg+BMgBBbQAAAPABAOQgLgKgKgLgAn8ASQgdgxAFg6QAEgzAdg1QAYgrAqgvQAqgxBGhAIBghZIAJgHIAYAaQARAPARAOIgXAZIgwA1QgYAegMAcQgMAagGAlIgDATIgEAuQgDArgEAXQgHAjgMAaIgTAgIgTAgQgEAJgCAJQgJABgJADQgqAJgdAaQgkgRgXgogAF9gEQgzg9gXhtIgNhJQgJgsgIgdQgVhGgngoIAFgHIAOgLQAcAoA4AuQARAPAjAcQAgAaATAUQA5A6AeBMQAYA/AAA/QgIAPgLAOQgdgDgfAHQgTAEgRAHQgVgQgRgTg");
	this.shape_16.setTransform(-2.3,25.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#7A3B14").s().p("Al/DbIATghIATgfQAMgaAHgkQAEgWADgsIAEgsIADgUQAGglAMgaQAMgcAYgeIAwg1IAXgZIARAMIALAGIARAKIgRAUIgFAFQgIAKgHAKIgJAOIgYAlIgQAfIgMAWIgLAZIgEAIQgOAggHAiIgTBUIgFATQgMA0gkAoIgBACQgUgCgUACQACgJAEgJgAFgDUQgYgUgYgdQgPgTgZgmIgJgNQAEhFgQhEQgUhcgzhNQAJgIAHgIQAnAoAVBGQAIAdAJAsIANBJQAXBsAzA9QARAVAVAPIgFACQgSgJgPgNg");
	this.shape_17.setTransform(0.6,5.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#B66829").s().p("AhFEkQgKgGgMgMIgUgXQgLgMghggQgdgagOgTQgdgmgOg3IARh2QAHgyAEgVIACgJIAEgJIALgYIANgWQgFAVgDAVQgHA+ANA7QANA7AlAqQApAxA1AEQAqADAsgaQAlgYAdgpQA4hMAHhjQAGhSgdhIIgDgHIAIAAIAJgBQAPgCAOgHQAUArALA0QAMA4ADBMQADBAgEAqQgGA7gTAsQgcBCg6AtQg7AthGAJQgNACgLAAQgaAAgSgKg");
	this.shape_18.setTransform(-3.8,15.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#C27333").s().p("AgfDWQg1gEgpgxQglgqgNg8QgNg7AHg9QADgVAFgVIAQgfIAYgmIAJgNQAGgLAJgJIAEgFIABAMQADAjAOAgQARArAdAXQARANAUAEQAUAFATgFQATgGAQgPQANgMANgVQAjg0AMg7QAFACAGAAIAEABIADAHQAdBIgGBSQgHBig4BNQgdApglAYQgnAXglAAIgKAAg");
	this.shape_19.setTransform(-4.8,7.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E7D6AC").s().p("AALAOQAGgKAFgLQAEgJABgKIACgEQADgPgBgQQAGANgBAOIAAACQgBALgDAMQgGAPgMATIgLARIAAgBIgDgDIgEADQgJAKgNAHIgJAEQAcgQASggg");
	this.shape_20.setTransform(8,-18.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCF4E0").s().p("AmsIbQghgXgVggIgBgMQgDgdAAgNQgBgXADgTQAFgWAKgRQALgUARgLQAggUArAPQAnANAcAhQAdAkANAuQALAqgFAqQgYAdgnAKQgPADgPAAQgqAAgqgcgAEpH4IgIgHQgDgrABgcQAEg9AdgnQAdgoA8gaQAegNAOAJQAJAGAIAPQAcA+gSBJQgRBBguAvIgOAEQgQADgPAAQgrAAgggbgAgckYIgBAAIgMAAQgSgBgUgGIAAAAQgNgEgLgGQgJgEgHgGQgHgFgGgHQgMgMgFgSQgDgJgBgKQgCgOACgOQAAgOAEgOIAJgaQAMgbAVgXQAegiAngRQAmgQAoABIAHAAIAKABQAnAEAiASQAjAUAPAbIAEAIQAJAVgBAbIgBAVIgDAPQgFAXgKAUIgCAFIgIAMIgFAGQgHAJgJAHIgSANIgFACQgOAHgOACIgKACIgHgBIgFAAQgGgBgFgCQgGgCgFgEIALgRQAMgTAGgQQADgMABgLIAAgCQABgPgGgMQABAQgDAPIgCAEQgBAKgEAJQgFAMgGAKQgTAfgcARIgFACQgOAEgQAAg");
	this.shape_21.setTransform(3.6,16.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CD8136").s().p("AlJA5QABg2AWgyQAYg6ApgiIACgCIAFADIACABQhTBYAAB3QAAArAKAmIgFACQgUgsABg0gAE+BGQAAh+hdhbIgFgEIACgBQAjAHAfA6QAeA6AJAyQAKA8gVAtIgDABQAFgbAAgeg");
	this.shape_22.setTransform(0.8,-49.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CB7E34").s().p("AhWEsQgfgJgcgOIgRgKIAAgCQAhAUAlACQAIAFAIAFQALAGANAEIgigHgAitEFIgRgMQgRgNgRgQIgXgaIgBgBQgZgegPghQgMgXgGgYQgKgnAAgqQAAh3BThYIABgBIAIgJIASgPQBYhMB2ABQB+AABbBTIABABIACABIAEAFQBdBbAAB+QAAAegFAaIgCALQgHAhgOAfQgYAvgpAoIgEAFIgQANIgOALIAIgMQAYgWAWgZQAjgrACh1QABhzgyhFQgwhGhJgkQhIglhZAQQhZAPg8A/Qg6A8gTBYQgSBVAXBTQAUBIAxAxQATATAVAMIAAACIgLgGgAiiEJIAAAAg");
	this.shape_23.setTransform(0.9,-43.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D58A41").s().p("AABEsIALAAIABAAIgGABIgGgBgAiTD7QgUgMgTgTQgygxgUhHQgXhUAShUQAThZA6g7QA8g/BZgQQBZgQBIAlQBJAlAxBFQAxBFgBB0QgBB0gkArQgWAZgYAXIADgFQAJgUAFgXIADgPIACgVQAAgbgJgVIgDgIQgQgbgjgUQgigSgngEIgKgBIgGAAQgqgBglAQQgnARgeAiQgUAXgMAbIgJAaQgEAOgBAOQgCAOACAOQABAKADAJQAGASALAMQAGAHAIAFQglgCgigUgAlKBNIgGAAQgNgNgLgRQgQgZgJgdQgQg3ALgtQAOg5A3giQAugdAwAGQgpAigYA6QgWAzgBA2QAAAzAUAsQgQAGgRAAIgCAAgAFVg8QgIgzgfg6Qgeg6gkgHIADgFQAXgJAcAEQAlAHAZAZQAYAZALAlQAJAhgBAlQgDA7gbA2IgEACQgOAIgQABQAUgsgKg8g");
	this.shape_24.setTransform(-0.6,-41.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E7A05B").s().p("AlzChQgggUgNgqQgLglAGgsQAFgjAMgdQAOgiAWgWQAYgZAhgKQAigLAfAKQAMAEAMAHIgCACQgwgGgvAdQg3AigOA5QgKAsAPA3QAKAeAQAZQAKARAOANQgVgCgRgKgAGOAPQABgkgJghQgMglgXgZQgagZglgHQgcgEgWAJQAJgNAQgIQAXgKAZADQAXADAVAQQATAOAOAVQAXAkAGA4QAHAzgLAlQgGAXgOARQgNARgRAJQAcg3ADg7g");
	this.shape_25.setTransform(-0.9,-51.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,255,255,0.02)").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_26.setTransform(0,0,0.758,0.758);

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(1));

	// Layer_2
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("A43Y3QqTqTAAukQAAujKTqUQKTqTOkAAQOkAAKTKTQKUKUAAOjQAAOkqUKTQqTKUukAAQukAAqTqUg");

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_28.setTransform(0,0,0.703,0.703);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#003366").s().p("A7KbKQrPrQAAv6QAAv5LPrQQLRrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArRrQg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bigcopy, new cjs.Rectangle(-250.7,-250.7,501.5,501.5), null);


(lib.big = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");
	this.shape.setTransform(311.4,286.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_1.setTransform(311.3,286.3,0.681,0.681);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_2.setTransform(311.3,286.4,0.703,0.703);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#003366").s().p("A7JbKQrQrQAAv6QAAv5LQrQQLQrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArQrQg");
	this.shape_3.setTransform(311.4,286.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.big, new cjs.Rectangle(60.7,35.7,501.5,501.5), null);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(289.4,0.5);

	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(289.4,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.02)").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape.setTransform(287.9,0,0.683,0.683);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A43Y3QqTqTAAukQAAujKTqUQKTqTOkAAQOkAAKTKTQKUKUAAOjQAAOkqUKTQqTKUukAAQukAAqTqUg");
	this.shape_1.setTransform(287.9,0,0.9,0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_2.setTransform(287.9,0,0.632,0.632);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#003366").s().p("A7KbKQrPrQAAv6QAAv5LPrQQLRrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArRrQg");
	this.shape_3.setTransform(287.9,0,0.9,0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");
	this.shape_4.setTransform(-287.9,0,0.9,0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_5.setTransform(-288,-0.1,0.613,0.613);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_6.setTransform(-288,-0.1,0.632,0.632);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#003366").s().p("A7JbKQrQrQAAv6QAAv5LQrQQLQrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArQrQg");
	this.shape_7.setTransform(-287.9,0,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.5,-225.6,1027.1,451.3);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween6("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},7).to({scaleX:1,scaleY:1},7).to({scaleX:1.03,scaleY:1.03},8).to({scaleX:1,scaleY:1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-96.5,-36.3,193,72.8);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigcopy();
	this.instance.parent = this;
	this.instance.setTransform(287.9,0,0.9,0.9);

	this.instance_1 = new lib.big();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-291,-4.5,0.9,0.9,0,0,0,307.9,281.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.5,-225.6,1027.1,451.3);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(354.8,465.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(197).to({_off:false},0).wait(30).to({startPosition:12},0).to({alpha:0,startPosition:1},6).wait(7));

	// Layer_5
	this.instance_1 = new lib.Tween23("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(362,455.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(192).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,x:361.9},15).to({alpha:0},11).wait(22));

	// Layer_1
	this.instance_2 = new lib.Symbol1copy("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(545.8,570.9,1,1,-8.7,0,0,190.6,64.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(169).to({_off:false},0).to({_off:true},23).wait(48));

	// arrow
	this.instance_3 = new lib.Symbol1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(571.4,264.6,1,1,49.5,0,0,36,71);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(137).to({_off:false},0).to({_off:true},32).wait(71));

	// Layer_3
	this.instance_4 = new lib.Tween5("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(770.4,121.4);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(64).to({_off:false},0).to({_off:true},44).wait(132));

	// Layer_4
	this.instance_5 = new lib.Symbol8();
	this.instance_5.parent = this;
	this.instance_5.setTransform(361.2,455.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(108).to({_off:false},0).to({regY:0.1,scaleX:1.21,scaleY:1.21},6).to({regX:0.1,scaleX:1.57,scaleY:1.57,x:361.3},7).to({scaleX:1.89,scaleY:1.89},7).to({scaleX:2.27,scaleY:2.27},7).wait(62).to({_off:true},1).wait(42));

	// Layer_8
	this.instance_6 = new lib.Symbol8();
	this.instance_6.parent = this;
	this.instance_6.setTransform(360.9,457.2,2.265,2.265,0,0,0,0.1,0.1);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(45).to({_off:false},0).to({alpha:1},8).to({_off:true},55).wait(132));

	// Layer_7
	this.instance_7 = new lib.Tween3("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(649.9,455.3);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.instance_8 = new lib.Symbol8();
	this.instance_8.parent = this;
	this.instance_8.setTransform(939.3,455.8);

	this.instance_9 = new lib.Symbol8();
	this.instance_9.parent = this;
	this.instance_9.setTransform(939.3,455.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.02)").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape.setTransform(937.7,455.3,0.683,0.683);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A43Y3QqTqTAAukQAAujKTqUQKTqTOkAAQOkAAKTKTQKUKUAAOjQAAOkqUKTQqTKUukAAQukAAqTqUg");
	this.shape_1.setTransform(937.8,455.3,0.9,0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_2.setTransform(937.8,455.3,0.632,0.632);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#003366").s().p("A7KbKQrPrQAAv6QAAv5LPrQQLRrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArRrQg");
	this.shape_3.setTransform(937.8,455.3,0.9,0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");
	this.shape_4.setTransform(361.9,455.3,0.9,0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_5.setTransform(361.8,455.2,0.613,0.613);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_6.setTransform(361.9,455.2,0.632,0.632);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#003366").s().p("A7JbKQrQrQAAv6QAAv5LQrQQLQrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArQrQg");
	this.shape_7.setTransform(361.9,455.3,0.9,0.9);

	this.instance_10 = new lib.Tween22("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(649.9,455.3);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7}]},45).to({state:[{t:this.instance_7}]},8).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_9},{t:this.instance_8}]},55).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_9},{t:this.instance_8}]},28).to({state:[{t:this.instance_10}]},56).to({state:[{t:this.instance_10}]},5).to({state:[]},1).wait(42));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(45).to({_off:false},0).to({alpha:1},8).to({_off:true},55).wait(132));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(192).to({_off:false},0).to({alpha:0},5).to({_off:true},1).wait(42));

	// Layer_6
	this.questiontxt = new lib.questiontext_mc();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(630.2,149.8,2.329,2.329,0,0,0,-1.4,-2.4);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(20).to({_off:false},0).to({alpha:1},7).wait(165).to({alpha:0},5).to({_off:true},1).wait(42));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(574,290.9,1415,861);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;