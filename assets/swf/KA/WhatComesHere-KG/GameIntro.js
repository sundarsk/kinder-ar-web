(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0000FF").s().p("AgRCbQgEgBgCgDQgDgDgCgEQgCgEAAgEQAAgFACgDQACgEADgDIAGgFQAEgBAFAAIAIABIAGAFIAEAHQACADAAAFQAAAEgCAEIgEAHQgDADgDABIgIACQgFAAgEgCgAgdBZQgCgJAAgHQAAgLAEgJQAEgJAGgIQAGgIAIgGIAPgNIAQgNQAIgGAGgIQAGgJAEgJQAEgKAAgMQAAgNgEgLQgEgMgIgJQgIgJgLgGQgLgGgNAAQgMAAgKAGQgKAEgHAJQgHAJgEALQgDALAAAMIAAAJIABAKIgiAFIgCgNIgBgMQAAgTAHgRQAHgRAMgMQAMgNARgHQARgIAUAAQATAAASAHQARAHAMANQAMANAIARQAHASAAAUQAAAKgCAKQgCAKgDAJQgEAKgHAJQgHAIgKAJIgTAOQgLAHgIAJQgJAIgEALQgFALACAPg");
	this.shape.setTransform(331,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_1.setTransform(298.9,5.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0000FF").s().p("AgiBnQgVgJgPgPQgPgPgKgTQgKgVAAgXQABgOAEgNQAEgOAGgMQAIgNAKgLQAKgKAMgIQANgHAOgEQAOgFAPAAQARAAAOAFQAPADANAIQANAHAKALQAKALAHAOIABABIggATIgBgBQgFgKgHgIQgHgIgJgEQgIgGgKgDQgKgDgLAAQgPAAgNAGQgNAGgLAKQgKALgGANQgFAOAAAPQAAAPAFAOQAGANAKALQALAJANAGQANAHAPgBQAKAAAKgCQAJgDAJgEQAIgFAHgHQAHgIAFgIIABgBIAhATIAAABQgIAMgKAKQgLAKgNAHQgNAHgOAEQgPADgPABQgUAAgUgIg");
	this.shape_2.setTransform(275.6,5.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0000FF").s().p("AhYAvIgCgvIgBgkIgBgcIgBgoIAsgBIAAA3QAHgLAIgJQAIgKAJgHQAJgIAKgEQAKgFAMgBQAOgBALAEQALADAHAGQAIAGAFAJQAFAIADAJIAEARIABAQQABAgAAAhIgCBFIgmgCIACg9QACgegCgfIAAgIIgCgMIgFgMQgDgGgEgEQgFgFgGgCQgHgCgJABQgPACgOAUQgQATgSAlIACAxIABAbIAAAPIgpAFIgCg7g");
	this.shape_3.setTransform(251.6,4.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_4.setTransform(228.4,5.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0000FF").s().p("AgUBqQgRgBgNgGQgLgFgJgKQgIgKgFgNQgGgNgCgOIgEgdIAAgcQAAgUABgUQACgUAEgVIAmABQgDAXgBATIgBAhIAAARIACAUIADAWQADAKAFAJQAFAIAHAFQAHAEALgBQAOgCAQgSQAQgSARglIgBgwIgBgbIAAgQIApgFIACA7IABAvIACAlIAAAbIABAoIgrABIgBg3QgGAMgIAKQgIAKgJAHQgJAIgLAEQgIAEgLAAIgCAAg");
	this.shape_5.setTransform(204.4,4.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0000FF").s().p("ABCChIABiBIgLAJIgMAHIgMAFIgLADQgMADgLABQgVgBgTgHQgUgIgOgOQgOgOgHgUQgJgUABgaQAAgaAIgUQAJgUAPgOQAOgOASgIQATgGASAAQAMAAANADQAMADANAGQAMAGALALIABgfIAlADIgBE+gAgchzQgLAFgJAKQgIAKgEAOQgFANgBAQQABAPAFANQAEAOAJAIQAIAJAMAGQALAFAOAAQALAAAMgEQAMgEAJgIQAKgHAGgKQAIgLACgMIAAgdQgCgNgHgLQgHgLgKgHQgJgIgNgEQgMgEgLAAQgOAAgLAFg");
	this.shape_6.setTransform(178.9,9.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_7.setTransform(155.5,5.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0000FF").s().p("AgbBsIgQgEIgRgGQgJgDgIgFQgJgFgIgHIASgcIAWAKIAWAJQALAFAMACQALACANAAQAKAAAIgCQAHgCAFgDQAEgDADgDIADgIIABgHIgCgHQgCgEgDgDQgEgEgGgDQgGgDgJgCQgJgCgMAAQgQgBgPgDQgQgDgNgFQgMgHgIgIQgIgJgCgPQgBgNADgMQADgLAGgKQAHgIAJgHQAJgGALgFQALgFAMgBQANgDALAAIASABQAKABALACQALACALAEQALAFAKAHIgNAjQgNgHgLgEIgUgFQgKgDgJgBQgdgBgRAHQgQAJAAAQQAAALAGAGQAGAFALACIAZADQAOAAAQADQATADAMAFQANAGAIAHQAIAIADAIQADAKAAAKQAAASgHANQgHALgMAIQgNAIgPADQgQAEgRABQgRAAgSgDg");
	this.shape_8.setTransform(132.9,5.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_9.setTransform(100.9,5.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0000FF").s().p("AAzCRQAGgTACgRIADgfIABgeQgBgUgHgPQgFgNgJgJQgJgJgKgEQgLgFgLAAQgKACgKAFQgKAFgKAJQgMAJgJARIgBB+IglABIgDkkIAsgCIgCB0QALgLAMgHQALgGAKgEQAMgEALgBQAVAAARAHQASAIANAOQAMAOAIAVQAGATACAaIAAAcIgCAbIgDAZQgBANgDAKg");
	this.shape_10.setTransform(76.7,0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0000FF").s().p("AgWgVIhCABIABgjIBCgCIABhTIAkgCIgBBVIBKgDIgDAkIhHACIgCCkIgmABg");
	this.shape_11.setTransform(54.5,1.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0000FF").s().p("AhYAvIgCgvIgBgkIgBgcIgBgoIAsgBIAAA3QAHgLAIgJQAIgKAJgHQAJgIAKgEQAKgFAMgBQAOgBALAEQALADAHAGQAIAGAFAJQAFAIADAJIAEARIABAQQABAgAAAhIgCBFIgmgCIACg9QACgegCgfIAAgIIgCgMIgFgMQgDgGgEgEQgFgFgGgCQgHgCgJABQgPACgOAUQgQATgSAlIACAxIABAbIAAAPIgpAFIgCg7g");
	this.shape_12.setTransform(23,4.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0000FF").s().p("AgYCPIABgxIABg7IAChRIAmgBIgBAwIgBAqIgBAhIAAAaIgBApgAgJhaQgGgCgDgDQgEgEgCgFQgCgFAAgFQAAgGACgFQACgFAEgDQADgEAGgCQAEgDAFAAQAGAAAFADQAEACAEAEQAEADACAFQACAFAAAGQAAAFgCAFQgCAFgEAEQgEADgEACQgFACgGAAQgFAAgEgCg");
	this.shape_13.setTransform(6.5,0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_14.setTransform(-20,5.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0000FF").s().p("AhVAvIgCgvIgBglIgBgbIgBgoIAsgBIAAAYIAAAWIAAAXIANgUQAJgKAJgJQAKgKALgGQAMgHAOgCQAOgBANAHQAGAEAFAFQAFAFAEAHQAFAJADAKQADAMABAOIgmAOQABgJgCgHIgDgMIgGgIIgFgGQgHgEgIAAQgFABgHADIgLAKIgMAOIgNAOIgLAPIgJAOIAAAcIABAZIABAWIABAPIgpAFIgCg7g");
	this.shape_15.setTransform(-41.2,5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_16.setTransform(-64.2,5.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0000FF").s().p("AAzCRQAGgTACgRIADgfIABgeQgBgUgHgPQgFgNgJgJQgJgJgKgEQgLgFgLAAQgJACgLAFQgKAFgKAJQgLAJgKARIgBB+IglABIgDkkIAsgCIgCB0QALgLALgHQAMgGAKgEQAMgEALgBQAVAAARAHQASAIANAOQAMAOAIAVQAGATACAaIAAAcIgCAbIgDAZQgBANgDAKg");
	this.shape_17.setTransform(-88.4,0.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#0000FF").s().p("AgbBsIgQgEIgRgGQgJgDgIgFQgJgFgIgHIASgcIAWAKIAWAJQALAFAMACQALACANAAQAKAAAIgCQAHgCAFgDQAEgDADgDIADgIIABgHIgCgHQgCgEgDgDQgEgEgGgDQgGgDgJgCQgJgCgMAAQgQgBgPgDQgQgDgNgFQgMgHgIgIQgIgJgCgPQgBgNADgMQADgLAGgKQAHgIAJgHQAJgGALgFQALgFAMgBQANgDALAAIASABQAKABALACQALACALAEQALAFAKAHIgNAjQgNgHgLgEIgUgFQgKgDgJgBQgdgBgRAHQgQAJAAAQQAAALAGAGQAGAFALACIAZADQAOAAAQADQATADAMAFQANAGAIAHQAIAIADAIQADAKAAAKQAAASgHANQgHALgMAIQgNAIgPADQgQAEgRABQgRAAgSgDg");
	this.shape_18.setTransform(-122.5,5.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#0000FF").s().p("AgqBmQgTgHgOgOQgNgOgHgUQgHgTAAgYQAAgYAIgUQAIgVAOgPQAOgPATgJQATgIAVgBQAUABASAHQARAIAOAOQANANAJASQAIATADAWIikAXQACANAFALQAFALAJAHQAIAIALADQALAFALAAQAKAAAKgEQAKgCAJgHQAIgGAGgIQAGgKADgMIAlAIQgGASgJAOQgKAOgNAJQgNAKgPAGQgQAFgRABQgYAAgTgIgAgPhJQgKADgIAGQgJAHgHALQgIAMgCAQIBxgOIgBgDQgIgTgMgLQgNgLgTAAQgHABgJACg");
	this.shape_19.setTransform(-144.4,5.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#0000FF").s().p("ABbAtIAAgpIgBgeQgBgQgEgHQgFgIgGAAQgFAAgFADQgFADgFAFIgJAKIgJAMIgIAMIgHAKIABARIAAAXIABAcIAAAkIgkACIgBg7IgBgpIgBgeQgCgQgEgHQgEgIgHAAQgGAAgEADIgKAJQgGAFgFAHIgKANIgIAMIgHAKIACBkIgmACIgFjLIAngEIABA3IANgQIAPgPQAHgGAKgEQAJgFALAAQAIAAAHADQAIACAFAGQAGAFAEAIQAFAJABALIAMgPQAGgIAJgGQAHgHAJgEQAJgEALAAQAIAAAIADQAIADAGAGQAGAGAEAJQAEAKABANIACAjIABAsIABBDIgnACIgBg7g");
	this.shape_20.setTransform(-171.8,4.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#0000FF").s().p("AgnBoQgSgJgOgOQgNgPgJgUQgHgVgBgZQABgZAHgUQAJgVANgOQAOgPASgIQASgIAVAAQAVAAASAJQATAIAOAPQAOAPAHAVQAJAUgBAXQABAYgJAUQgHAUgOAPQgOAQgTAIQgSAJgVAAQgVAAgSgIgAgbhGQgLAHgIALQgIALgDAOQgDAOAAANQAAANADAOQAEAOAIALQAHALAMAHQAMAHAOAAQAQAAAMgHQALgHAIgLQAHgLAEgOQAEgOAAgNQAAgNgEgOQgEgOgHgLQgHgLgMgHQgMgIgQABQgQgBgLAIg");
	this.shape_21.setTransform(-199.1,5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#0000FF").s().p("AgiBnQgVgJgPgPQgPgPgKgTQgJgVAAgXQAAgOADgNQAEgOAHgMQAIgNAKgLQAKgKAMgIQAMgHAPgEQAOgFAPAAQAQAAAQAFQAPADAMAIQANAHAKALQAKALAHAOIAAABIgfATIAAgBQgGgKgHgIQgGgIgKgEQgIgGgKgDQgKgDgLAAQgPAAgNAGQgOAGgKAKQgKALgGANQgFAOAAAPQAAAPAFAOQAGANAKALQAKAJAOAGQANAHAPgBQAKAAAJgCQAKgDAJgEQAIgFAHgHQAHgIAFgIIABgBIAhATIgBABQgHAMgLAKQgKAKgNAHQgNAHgOAEQgPADgPABQgVAAgTgIg");
	this.shape_22.setTransform(-222.4,5.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#0000FF").s().p("AgWgVIhCABIAAgjIBDgCIABhTIAkgCIgBBVIBKgDIgDAkIhHACIgBCkIgoABg");
	this.shape_23.setTransform(-253.5,1.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#0000FF").s().p("AA7BsIADgbQgXAPgXAHQgVAHgUAAQgNAAgNgEQgNgDgJgIQgLgHgFgLQgHgMAAgPQABgPAEgNQAEgMAIgJQAJgIAKgHQALgHANgEQAMgEAOgCQAMgCAOAAIAXABIATACIgFgSQgEgJgFgIQgHgHgIgFQgHgEgMAAQgIAAgIACQgKACgLAGQgLAGgOAKQgNAJgPAQIgYgcQATgRAQgLQARgLAPgGQAOgGANgCQAMgCAKAAQARAAAOAGQANAFALALQALAKAGAOQAIAOAEAQQAEAQADARIABAiQAAASgCAVQgCAVgEAYgAgIAAQgRADgLAHQgLAIgGALQgGALAEAOQACALAIAFQAHAEALAAQALAAANgDQAMgEAMgFIAYgMIATgLIABgTIgBgTIgTgEIgVgBQgRAAgPAEg");
	this.shape_24.setTransform(-275.5,4.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#0000FF").s().p("AAzCRQAFgTADgRIADgfIABgeQgBgUgGgPQgHgNgIgJQgJgJgKgEQgMgFgJAAQgKACgMAFQgJAFgKAJQgLAJgKARIgBB+IglABIgEkkIAtgCIgCB0QAKgLAMgHQAMgGALgEQALgEALgBQAVAAASAHQARAIANAOQAMAOAHAVQAIATAAAaIAAAcIgBAbIgDAZQgCANgDAKg");
	this.shape_25.setTransform(-299.7,0.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#0000FF").s().p("AAGgmIgtCFIg5AEIgljEIAngDIAeCfIA4ifIAjACIAmCgIAaiiIArACIgnDFIg4ACg");
	this.shape_26.setTransform(-327.3,4.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("Eg1CAFUQhrAAhNhNQhMhLAAhsIAAifQAAhrBMhNQBNhMBrAAMBqFAAAQBrAABMBMQBNBNAABrIAACfQAABshNBLQhMBNhrAAg");
	this.shape_27.setTransform(0,0,0.98,0.98);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-358.1,-33.3,716.3,66.6);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape.setTransform(8,-37.7,0.211,0.145,0,-0.1,179.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_1.setTransform(10.3,-56.1,0.339,0.289,0,-24.9,155.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_2.setTransform(8.9,-52.4,0.678,0.579,0,-24.9,155.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_3.setTransform(9.4,-49.4,0.883,0.883,0,-24.9,155.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_4.setTransform(22.3,7.5,0.19,0.13,0,-0.1,179.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_5.setTransform(23.8,-9.2,0.305,0.26,0,-24.9,155.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_6.setTransform(22.7,-6.1,0.61,0.521,0,-24.9,155.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_7.setTransform(23,-3.3,0.795,0.795,0,-24.9,155.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_8.setTransform(8.3,37.5,0.19,0.13,0,-0.1,179.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_9.setTransform(10,20.5,0.305,0.26,0,-24.9,155.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_10.setTransform(8.7,23.8,0.61,0.521,0,-24.9,155.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_11.setTransform(9.1,26.8,0.795,0.795,0,-24.9,155.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_12.setTransform(-9.1,-2.1,0.211,0.145,0,-0.1,179.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_13.setTransform(-7.3,-20.5,0.339,0.289,0,-24.9,155.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_14.setTransform(-8.5,-16.8,0.678,0.579,0,-24.9,155.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_15.setTransform(-7.8,-13.6,0.883,0.883,0,-24.9,155.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_16.setTransform(2.2,16.7,0.19,0.13,0,-0.1,179.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_17.setTransform(3.8,0.1,0.305,0.26,0,-24.9,155.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_18.setTransform(2.7,3.2,0.61,0.521,0,-24.9,155.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_19.setTransform(3.4,6.2,0.795,0.795,0,-24.9,155.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_20.setTransform(12.7,76.2,0.19,0.13,0,-0.1,179.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_21.setTransform(14.2,60.2,0.305,0.26,0,-24.9,155.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_22.setTransform(13.2,63.3,0.61,0.521,0,-24.9,155.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_23.setTransform(13.4,66.5,0.795,0.795,0,-24.9,155.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_24.setTransform(-3.6,64.4,0.19,0.13,0,-0.1,179.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_25.setTransform(-2.1,48.2,0.305,0.26,0,-24.9,155.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_26.setTransform(-3.3,51.4,0.61,0.521,0,-24.9,155.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_27.setTransform(-3,54.7,0.795,0.795,0,-24.9,155.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_28.setTransform(19.7,61,0.19,0.13,0,-0.1,179.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_29.setTransform(21.4,45.2,0.305,0.26,0,-24.9,155.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_30.setTransform(20.2,48.4,0.61,0.521,0,-24.9,155.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_31.setTransform(20.6,51.4,0.795,0.795,0,-24.9,155.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_32.setTransform(-13.5,44.5,0.19,0.13,0,-0.1,179.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_33.setTransform(-11.8,27.8,0.305,0.26,0,-24.9,155.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_34.setTransform(-12.9,31.1,0.61,0.521,0,-24.9,155.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_35.setTransform(-12.3,34,0.795,0.795,0,-24.9,155.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_36.setTransform(31,46.3,0.19,0.13,0,-0.1,179.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_37.setTransform(32.7,29.7,0.305,0.26,0,-24.9,155.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_38.setTransform(31.6,32.8,0.61,0.521,0,-24.9,155.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_39.setTransform(32.1,36,0.795,0.795,0,-24.9,155.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_40.setTransform(5.8,46.9,0.19,0.13,0,-0.1,179.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_41.setTransform(7.6,30.5,0.305,0.26,0,-24.9,155.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_42.setTransform(6.4,33.7,0.61,0.521,0,-24.9,155.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_43.setTransform(6.9,36.9,0.795,0.795,0,-24.9,155.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_44.setTransform(-22.8,24.6,0.19,0.13,0,-0.1,179.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_45.setTransform(-21.1,7.4,0.305,0.26,0,-24.9,155.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_46.setTransform(-22.3,10.9,0.61,0.521,0,-24.9,155.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_47.setTransform(-21.8,14,0.795,0.795,0,-24.9,155.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_48.setTransform(36.9,21.1,0.19,0.13,0,-0.1,179.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_49.setTransform(38.7,4.3,0.305,0.26,0,-24.9,155.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_50.setTransform(37.5,7.4,0.61,0.521,0,-24.9,155.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_51.setTransform(38.1,10.6,0.795,0.795,0,-24.9,155.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_52.setTransform(19.9,32.1,0.19,0.13,0,-0.1,179.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_53.setTransform(21.6,15.1,0.305,0.26,0,-24.9,155.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_54.setTransform(20.6,18.1,0.61,0.521,0,-24.9,155.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_55.setTransform(21.1,21.4,0.795,0.795,0,-24.9,155.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_56.setTransform(-2.5,25.3,0.19,0.13,0,-0.1,179.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_57.setTransform(-0.7,8.3,0.305,0.26,0,-24.9,155.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_58.setTransform(-1.7,11.4,0.61,0.521,0,-24.9,155.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_59.setTransform(-1.2,14.7,0.795,0.795,0,-24.9,155.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_60.setTransform(33.9,-2,0.19,0.13,0,-0.1,179.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_61.setTransform(35.6,-18.7,0.305,0.26,0,-24.9,155.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_62.setTransform(34.4,-15.6,0.61,0.521,0,-24.9,155.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_63.setTransform(35,-12.5,0.795,0.795,0,-24.9,155.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_64.setTransform(16.8,10,0.19,0.13,0,-0.1,179.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_65.setTransform(18.5,-6.7,0.305,0.26,0,-24.9,155.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_66.setTransform(17.4,-3.6,0.61,0.521,0,-24.9,155.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_67.setTransform(17.9,-0.5,0.795,0.795,0,-24.9,155.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_68.setTransform(-24.7,5.9,0.19,0.13,0,-0.1,179.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_69.setTransform(-22.9,-11,0.305,0.26,0,-24.9,155.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_70.setTransform(-24.2,-7.9,0.61,0.521,0,-24.9,155.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_71.setTransform(-23.7,-4.7,0.795,0.795,0,-24.9,155.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_72.setTransform(-3.6,4.3,0.19,0.13,0,-0.1,179.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_73.setTransform(-2,-12.4,0.305,0.26,0,-24.9,155.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_74.setTransform(-3.2,-9.2,0.61,0.521,0,-24.9,155.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_75.setTransform(-2.7,-6.4,0.795,0.795,0,-24.9,155.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_76.setTransform(32.6,-20.7,0.19,0.13,0,-0.1,179.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_77.setTransform(34.1,-37.4,0.305,0.26,0,-24.9,155.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_78.setTransform(32.9,-34.2,0.61,0.521,0,-24.9,155.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_79.setTransform(33.6,-31.2,0.795,0.795,0,-24.9,155.1);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_80.setTransform(13.6,-6.7,0.19,0.13,0,-0.1,179.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_81.setTransform(15.2,-23.4,0.305,0.26,0,-24.9,155.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_82.setTransform(14.3,-20.1,0.61,0.521,0,-24.9,155.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_83.setTransform(14.5,-17.1,0.795,0.795,0,-24.9,155.1);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_84.setTransform(-37.9,-3,0.19,0.13,0,-0.1,179.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_85.setTransform(-36.2,-19.5,0.305,0.26,0,-24.9,155.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_86.setTransform(-37.4,-16,0.61,0.521,0,-24.9,155.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_87.setTransform(-37.1,-13,0.795,0.795,0,-24.9,155.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_88.setTransform(-14.6,-14.2,0.211,0.145,0,-0.1,179.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_89.setTransform(-13.1,-32.5,0.339,0.289,0,-24.9,155.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_90.setTransform(-14,-28.9,0.678,0.579,0,-24.9,155.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_91.setTransform(-13.4,-25.8,0.883,0.883,0,-24.9,155.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_92.setTransform(8,-24.8,0.211,0.145,0,-0.1,179.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_93.setTransform(10.3,-43.3,0.339,0.289,0,-24.9,155.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_94.setTransform(8.9,-39.8,0.678,0.579,0,-24.9,155.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_95.setTransform(9.4,-36.3,0.883,0.883,0,-24.9,155.1);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_96.setTransform(-14.5,-37.7,0.211,0.145,0,-0.1,179.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_97.setTransform(-12.4,-56.1,0.339,0.289,0,-24.9,155.1);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_98.setTransform(-13.8,-52.7,0.678,0.579,0,-24.9,155.1);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_99.setTransform(-13.2,-49.4,0.883,0.883,0,-24.9,155.1);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_100.setTransform(-38.9,-24.3,0.19,0.13,0,-0.1,179.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_101.setTransform(-37.2,-41.1,0.305,0.26,0,-24.9,155.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_102.setTransform(-38,-37.4,0.61,0.521,0,-24.9,155.1);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_103.setTransform(-37.7,-34.9,0.795,0.795,0,-24.9,155.1);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#480F1E").ss(1,1,1,3,true).p("AA5itQgMANgJARQgiA2gJBbQgMB/AxAuQAAADgGADQgEACghgCQgNgCgJAAQgdiCALhZQADg0APhZQANAIAaABQAeADAUgG");
	this.shape_104.setTransform(3,-59.9);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#275D07").ss(1,1,1,3,true).p("AgBAAIADAB");
	this.shape_105.setTransform(8.5,-77.4);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#35151D").s().p("AgNC1QgNgCgJAAQgdiCALhZQADg0APhZQANAIAaABQAeADAUgGIADACQgMANgJARQgiA2gJBbQgMB/AxAuQAAADgGADQgCABgKAAIgZgBg");
	this.shape_106.setTransform(3,-59.9);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_107.setTransform(20.1,-66.4,1.251,1.266,0,-51.1,134.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_108.setTransform(19.3,-68,1.251,1.266,0,-51.1,134.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.6,-80.2,105.3,160.4);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape.setTransform(9.9,-46.4,0.261,0.179,0,-0.2,179.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_1.setTransform(12.7,-69.2,0.419,0.357,0,-24.9,155.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_2.setTransform(10.9,-64.8,0.837,0.715,0,-24.9,155.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_3.setTransform(11.6,-61,1.09,1.09,0,-24.9,155.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_4.setTransform(27.5,9.4,0.235,0.161,0,-0.1,179.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_5.setTransform(29.4,-11.3,0.377,0.321,0,-24.9,155.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_6.setTransform(28,-7.6,0.754,0.643,0,-24.9,155.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_7.setTransform(28.4,-4,0.981,0.981,0,-24.9,155.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_8.setTransform(10.2,46.4,0.235,0.161,0,-0.1,179.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_9.setTransform(12.3,25.4,0.377,0.321,0,-24.9,155.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_10.setTransform(10.6,29.5,0.754,0.643,0,-24.9,155.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_11.setTransform(11.3,33.1,0.981,0.981,0,-24.9,155.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_12.setTransform(-11.3,-2.5,0.261,0.179,0,-0.2,179.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_13.setTransform(-9,-25.2,0.419,0.357,0,-24.9,155.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_14.setTransform(-10.5,-20.8,0.837,0.715,0,-24.9,155.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_15.setTransform(-9.6,-16.8,1.09,1.09,0,-24.9,155.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_16.setTransform(2.7,20.7,0.235,0.161,0,-0.1,179.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_17.setTransform(4.7,0.2,0.377,0.321,0,-24.9,155.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_18.setTransform(3.3,3.9,0.754,0.643,0,-24.9,155.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_19.setTransform(4.2,7.6,0.981,0.981,0,-24.9,155.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_20.setTransform(15.6,94.2,0.235,0.161,0,-0.1,179.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_21.setTransform(17.5,74.4,0.377,0.321,0,-24.9,155.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_22.setTransform(16.2,78.2,0.754,0.643,0,-24.9,155.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_23.setTransform(16.5,82.1,0.981,0.981,0,-24.9,155.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_24.setTransform(-4.5,79.6,0.235,0.161,0,-0.1,179.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_25.setTransform(-2.7,59.6,0.377,0.321,0,-24.9,155.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_26.setTransform(-4.2,63.5,0.754,0.643,0,-24.9,155.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_27.setTransform(-3.7,67.5,0.981,0.981,0,-24.9,155.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_28.setTransform(24.3,75.4,0.235,0.161,0,-0.1,179.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_29.setTransform(26.5,56,0.377,0.321,0,-24.9,155.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_30.setTransform(24.9,59.8,0.754,0.643,0,-24.9,155.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_31.setTransform(25.4,63.4,0.981,0.981,0,-24.9,155.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_32.setTransform(-16.7,55,0.235,0.161,0,-0.1,179.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_33.setTransform(-14.6,34.5,0.377,0.321,0,-24.9,155.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_34.setTransform(-16,38.5,0.754,0.643,0,-24.9,155.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_35.setTransform(-15.2,42,0.981,0.981,0,-24.9,155.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_36.setTransform(38.2,57.2,0.235,0.161,0,-0.1,179.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_37.setTransform(40.4,36.7,0.377,0.321,0,-24.9,155.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_38.setTransform(39,40.6,0.754,0.643,0,-24.9,155.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_39.setTransform(39.7,44.4,0.981,0.981,0,-24.9,155.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_40.setTransform(7.1,57.9,0.235,0.161,0,-0.1,179.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_41.setTransform(9.3,37.8,0.377,0.321,0,-24.9,155.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_42.setTransform(7.8,41.7,0.754,0.643,0,-24.9,155.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_43.setTransform(8.5,45.5,0.981,0.981,0,-24.9,155.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_44.setTransform(-28.2,30.4,0.235,0.161,0,-0.1,179.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_45.setTransform(-26.1,9.3,0.377,0.321,0,-24.9,155.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_46.setTransform(-27.5,13.5,0.754,0.643,0,-24.9,155.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_47.setTransform(-26.9,17.3,0.981,0.981,0,-24.9,155.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_48.setTransform(45.5,26.1,0.235,0.161,0,-0.1,179.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_49.setTransform(47.8,5.4,0.377,0.321,0,-24.9,155.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_50.setTransform(46.2,9.2,0.754,0.643,0,-24.9,155.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_51.setTransform(47,13.1,0.981,0.981,0,-24.9,155.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_52.setTransform(24.6,39.7,0.235,0.161,0,-0.1,179.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_53.setTransform(26.6,18.8,0.377,0.321,0,-24.9,155.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_54.setTransform(25.3,22.5,0.754,0.643,0,-24.9,155.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_55.setTransform(26,26.4,0.981,0.981,0,-24.9,155.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_56.setTransform(-3.1,31.3,0.235,0.161,0,-0.1,179.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_57.setTransform(-0.9,10.4,0.377,0.321,0,-24.9,155.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_58.setTransform(-2.2,14.2,0.754,0.643,0,-24.9,155.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_59.setTransform(-1.5,18.2,0.981,0.981,0,-24.9,155.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_60.setTransform(41.8,-2.4,0.235,0.161,0,-0.1,179.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_61.setTransform(44,-22.9,0.377,0.321,0,-24.9,155.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_62.setTransform(42.4,-19.3,0.754,0.643,0,-24.9,155.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_63.setTransform(43.2,-15.4,0.981,0.981,0,-24.9,155.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_64.setTransform(20.7,12.5,0.235,0.161,0,-0.1,179.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_65.setTransform(22.8,-8.2,0.377,0.321,0,-24.9,155.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_66.setTransform(21.4,-4.4,0.754,0.643,0,-24.9,155.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_67.setTransform(22,-0.6,0.981,0.981,0,-24.9,155.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_68.setTransform(-30.6,7.3,0.235,0.161,0,-0.1,179.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_69.setTransform(-28.3,-13.5,0.377,0.321,0,-24.9,155.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_70.setTransform(-30,-9.8,0.754,0.643,0,-24.9,155.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_71.setTransform(-29.3,-5.8,0.981,0.981,0,-24.9,155.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_72.setTransform(-4.5,5.4,0.235,0.161,0,-0.1,179.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_73.setTransform(-2.5,-15.2,0.377,0.321,0,-24.9,155.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_74.setTransform(-4,-11.3,0.754,0.643,0,-24.9,155.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_75.setTransform(-3.4,-7.9,0.981,0.981,0,-24.9,155.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_76.setTransform(40.2,-25.5,0.235,0.161,0,-0.1,179.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_77.setTransform(42.1,-46,0.377,0.321,0,-24.9,155.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_78.setTransform(40.5,-42.2,0.754,0.643,0,-24.9,155.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_79.setTransform(41.4,-38.5,0.981,0.981,0,-24.9,155.1);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_80.setTransform(16.8,-8.2,0.235,0.161,0,-0.1,179.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_81.setTransform(18.7,-28.8,0.377,0.321,0,-24.9,155.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_82.setTransform(17.6,-24.8,0.754,0.643,0,-24.9,155.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_83.setTransform(17.9,-21.1,0.981,0.981,0,-24.9,155.1);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_84.setTransform(-46.8,-3.6,0.235,0.161,0,-0.1,179.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_85.setTransform(-44.7,-24,0.377,0.321,0,-24.9,155.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_86.setTransform(-46.2,-19.8,0.754,0.643,0,-24.9,155.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_87.setTransform(-45.8,-16,0.981,0.981,0,-24.9,155.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_88.setTransform(-18,-17.4,0.261,0.179,0,-0.2,179.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_89.setTransform(-16.1,-40,0.419,0.357,0,-24.9,155.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_90.setTransform(-17.3,-35.7,0.837,0.715,0,-24.9,155.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_91.setTransform(-16.6,-31.9,1.09,1.09,0,-24.9,155.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_92.setTransform(9.9,-30.5,0.261,0.179,0,-0.2,179.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_93.setTransform(12.7,-53.4,0.419,0.357,0,-24.9,155.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_94.setTransform(10.9,-49.1,0.837,0.715,0,-24.9,155.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_95.setTransform(11.6,-44.9,1.09,1.09,0,-24.9,155.1);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_96.setTransform(-17.9,-46.4,0.261,0.179,0,-0.2,179.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_97.setTransform(-15.3,-69.2,0.419,0.357,0,-24.9,155.1);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_98.setTransform(-17.1,-65.1,0.837,0.715,0,-24.9,155.1);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_99.setTransform(-16.3,-61,1.09,1.09,0,-24.9,155.1);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_100.setTransform(-48.1,-29.9,0.235,0.161,0,-0.1,179.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_101.setTransform(-46,-50.6,0.377,0.321,0,-24.9,155.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_102.setTransform(-47,-46.2,0.754,0.643,0,-24.9,155.1);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_103.setTransform(-46.6,-43,0.981,0.981,0,-24.9,155.1);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#480F1E").ss(1,1,1,3,true).p("ABGjWQgOAQgMAVQgqBCgLBxQgPCeA8A4QABADgIAFQgFACgpgCQgQgDgKAAQglihAOhuQAEhAAShuQAQAKAgACQAmADAYgI");
	this.shape_104.setTransform(3.6,-74);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#275D07").ss(1,1,1,3,true).p("AgBAAIADAB");
	this.shape_105.setTransform(10.4,-95.7);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#35151D").s().p("AgRDgQgQgDgKAAQglihAOhuQAEhAAShuQAQAKAgACQAmADAYgIIAEADQgOAQgMAVQgqBCgLBxQgPCeA8A4QABADgIAFQgCABgMAAIgggBg");
	this.shape_106.setTransform(3.6,-74);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_107.setTransform(24.8,-82,1.545,1.563,0,-51.1,134.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_108.setTransform(23.8,-83.9,1.545,1.563,0,-51.1,134.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-65,-99,130,198);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhVJXQgXgJgRgQQgRgRgJgXQgLgWAAgaQAAgaALgXQAJgYARgQQARgQAXgKQAWgKAbAAQAaAAAWAKQAXAKARAQQAQAQAJAYQALAXAAAaQAAAagLAWQgJAXgQARQgRAQgXAJQgWAKgaAAQgbAAgWgKgAhkCxIgNiGIgBgIIAAgHQAAgjASgbQATgbAbgXQAagYAfgWQAfgWAcgbQAbgZASgiQASghABgsQAAgegMgXQgLgXgUgRQgUgRgbgKQgdgJgeAAQgwAAggALQghAKgWANQgXAMgPALQgQAKgMAAQgeAAgOgZIg1hUQAdgZAggXQAigWAngRQAmgQAtgJQAvgKA0AAQBGAAA8AUQA7ATAqAlQAqAlAXA0QAXA0AABAQAAA/gSAuQgTAugbAiQgcAhggAZIg9AvQgdAWgUAUQgWAVgEAaIgTB5g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.8,-60.9,63.6,121.9);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhVJXQgXgJgRgQQgRgRgJgXQgLgWAAgaQAAgaALgXQAJgYARgQQARgQAXgKQAWgKAbAAQAaAAAWAKQAXAKARAQQAQAQAJAYQALAXAAAaQAAAagLAWQgJAXgQARQgRAQgXAJQgWAKgaAAQgbAAgWgKgAhkCxIgNiGIgBgIIAAgHQAAgjASgbQATgbAbgXQAagYAfgWQAfgWAcgbQAbgZASgiQASghABgsQAAgegMgXQgLgXgUgRQgUgRgbgKQgdgJgeAAQgwAAggALQghAKgWANQgXAMgPALQgQAKgMAAQgeAAgOgZIg1hUQAdgZAggXQAigWAngRQAmgQAtgJQAvgKA0AAQBGAAA8AUQA7ATAqAlQAqAlAXA0QAXA0AABAQAAA/gSAuQgTAugbAiQgcAhggAZIg9AvQgdAWgUAUQgWAVgEAaIgTB5g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.8,-60.9,63.6,121.9);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhFHmQgSgIgOgNQgOgNgIgTQgIgTAAgUQAAgVAIgTQAIgTAOgNQAOgNASgJQATgIAVAAQAVAAARAIQATAJAOANQANANAIATQAIATAAAVQAAAUgIATQgIATgNANQgOANgTAIQgRAHgVAAQgVAAgTgHgAhQCQIgLhtIgBgGIAAgGQAAgcAOgWQAPgWAXgTQAVgTAZgSQAZgSAWgVQAWgVAPgbQAPgbAAgkQAAgYgJgTQgKgTgPgNQgRgOgWgIQgXgHgYAAQgnAAgaAIQgaAJgTAKQgSAKgMAJQgNAIgKAAQgYAAgLgUIgrhFQAWgUAbgSQAcgSAfgOQAfgNAlgHQAlgIAqAAQA5AAAwAQQAwAQAiAeQAiAdATAqQASArAAAzQAAAzgOAmQgPAlgWAbQgWAcgaAUIgyAmQgYARgQAQQgRARgEAWIgPBig");
	this.shape.setTransform(-0.5,3.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy, new cjs.Rectangle(-31.2,-82.6,62.5,165.3), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape.setTransform(8,-34.5,0.211,0.145,0,-0.1,179.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_1.setTransform(10.3,-53,0.339,0.289,0,-24.9,155.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_2.setTransform(8.8,-49.4,0.678,0.579,0,-24.9,155.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_3.setTransform(9.4,-46.4,0.883,0.883,0,-24.9,155.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_4.setTransform(22.2,10.6,0.19,0.13,0,-0.1,179.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_5.setTransform(23.8,-6.1,0.305,0.26,0,-24.9,155.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_6.setTransform(22.7,-3.1,0.61,0.521,0,-24.9,155.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_7.setTransform(23,-0.3,0.795,0.795,0,-24.9,155.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_8.setTransform(8.2,40.6,0.19,0.13,0,-0.1,179.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_9.setTransform(9.9,23.6,0.305,0.26,0,-24.9,155.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_10.setTransform(8.6,26.9,0.61,0.521,0,-24.9,155.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_11.setTransform(9.1,29.9,0.795,0.795,0,-24.9,155.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_12.setTransform(-9.2,1,0.211,0.145,0,-0.1,179.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_13.setTransform(-7.3,-17.3,0.339,0.289,0,-24.9,155.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_14.setTransform(-8.5,-13.8,0.678,0.579,0,-24.9,155.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_15.setTransform(-7.8,-10.6,0.883,0.883,0,-24.9,155.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_16.setTransform(2.2,19.8,0.19,0.13,0,-0.1,179.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_17.setTransform(3.7,3.2,0.305,0.26,0,-24.9,155.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_18.setTransform(2.6,6.2,0.61,0.521,0,-24.9,155.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_19.setTransform(3.4,9.2,0.795,0.795,0,-24.9,155.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_20.setTransform(12.6,79.3,0.19,0.13,0,-0.1,179.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_21.setTransform(14.1,63.3,0.305,0.26,0,-24.9,155.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_22.setTransform(13.1,66.4,0.61,0.521,0,-24.9,155.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_23.setTransform(13.3,69.5,0.795,0.795,0,-24.9,155.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_24.setTransform(-3.7,67.4,0.19,0.13,0,-0.1,179.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_25.setTransform(-2.2,51.3,0.305,0.26,0,-24.9,155.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_26.setTransform(-3.4,54.5,0.61,0.521,0,-24.9,155.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_27.setTransform(-3,57.7,0.795,0.795,0,-24.9,155.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_28.setTransform(19.7,64,0.19,0.13,0,-0.1,179.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_29.setTransform(21.4,48.4,0.305,0.26,0,-24.9,155.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_30.setTransform(20.2,51.5,0.61,0.521,0,-24.9,155.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_31.setTransform(20.5,54.4,0.795,0.795,0,-24.9,155.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_32.setTransform(-13.6,47.6,0.19,0.13,0,-0.1,179.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_33.setTransform(-11.9,31,0.305,0.26,0,-24.9,155.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_34.setTransform(-12.9,34.2,0.61,0.521,0,-24.9,155.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_35.setTransform(-12.4,37.1,0.795,0.795,0,-24.9,155.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_36.setTransform(30.9,49.4,0.19,0.13,0,-0.1,179.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_37.setTransform(32.7,32.8,0.305,0.26,0,-24.9,155.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_38.setTransform(31.6,35.9,0.61,0.521,0,-24.9,155.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_39.setTransform(32.1,39,0.795,0.795,0,-24.9,155.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_40.setTransform(5.7,50,0.19,0.13,0,-0.1,179.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_41.setTransform(7.5,33.6,0.305,0.26,0,-24.9,155.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_42.setTransform(6.3,36.8,0.61,0.521,0,-24.9,155.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_43.setTransform(6.8,39.9,0.795,0.795,0,-24.9,155.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_44.setTransform(-22.8,27.7,0.19,0.13,0,-0.1,179.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_45.setTransform(-21.2,10.6,0.305,0.26,0,-24.9,155.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_46.setTransform(-22.3,14,0.61,0.521,0,-24.9,155.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_47.setTransform(-21.8,17.1,0.795,0.795,0,-24.9,155.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_48.setTransform(36.8,24.2,0.19,0.13,0,-0.1,179.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_49.setTransform(38.6,7.5,0.305,0.26,0,-24.9,155.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_50.setTransform(37.4,10.5,0.61,0.521,0,-24.9,155.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_51.setTransform(38,13.6,0.795,0.795,0,-24.9,155.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_52.setTransform(19.9,35.2,0.19,0.13,0,-0.1,179.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_53.setTransform(21.5,18.3,0.305,0.26,0,-24.9,155.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_54.setTransform(20.5,21.3,0.61,0.521,0,-24.9,155.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_55.setTransform(21,24.4,0.795,0.795,0,-24.9,155.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_56.setTransform(-2.5,28.4,0.19,0.13,0,-0.1,179.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_57.setTransform(-0.8,11.5,0.305,0.26,0,-24.9,155.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_58.setTransform(-1.8,14.5,0.61,0.521,0,-24.9,155.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_59.setTransform(-1.2,17.7,0.795,0.795,0,-24.9,155.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_60.setTransform(33.9,1.1,0.19,0.13,0,-0.1,179.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_61.setTransform(35.6,-15.5,0.305,0.26,0,-24.9,155.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_62.setTransform(34.3,-12.6,0.61,0.521,0,-24.9,155.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_63.setTransform(35,-9.5,0.795,0.795,0,-24.9,155.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_64.setTransform(16.7,13.2,0.19,0.13,0,-0.1,179.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_65.setTransform(18.4,-3.6,0.305,0.26,0,-24.9,155.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_66.setTransform(17.4,-0.6,0.61,0.521,0,-24.9,155.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_67.setTransform(17.8,2.5,0.795,0.795,0,-24.9,155.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_68.setTransform(-24.8,9,0.19,0.13,0,-0.1,179.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_69.setTransform(-23,-7.9,0.305,0.26,0,-24.9,155.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_70.setTransform(-24.3,-4.9,0.61,0.521,0,-24.9,155.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_71.setTransform(-23.8,-1.6,0.795,0.795,0,-24.9,155.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_72.setTransform(-3.7,7.4,0.19,0.13,0,-0.1,179.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_73.setTransform(-2,-9.3,0.305,0.26,0,-24.9,155.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_74.setTransform(-3.3,-6.2,0.61,0.521,0,-24.9,155.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_75.setTransform(-2.8,-3.4,0.795,0.795,0,-24.9,155.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_76.setTransform(32.6,-17.6,0.19,0.13,0,-0.1,179.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_77.setTransform(34.1,-34.2,0.305,0.26,0,-24.9,155.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_78.setTransform(32.8,-31.2,0.61,0.521,0,-24.9,155.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_79.setTransform(33.5,-28.2,0.795,0.795,0,-24.9,155.1);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_80.setTransform(13.6,-3.6,0.19,0.13,0,-0.1,179.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_81.setTransform(15.1,-20.3,0.305,0.26,0,-24.9,155.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_82.setTransform(14.2,-17.1,0.61,0.521,0,-24.9,155.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_83.setTransform(14.5,-14.1,0.795,0.795,0,-24.9,155.1);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_84.setTransform(-38,0.1,0.19,0.13,0,-0.1,179.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_85.setTransform(-36.2,-16.4,0.305,0.26,0,-24.9,155.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_86.setTransform(-37.5,-13,0.61,0.521,0,-24.9,155.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_87.setTransform(-37.1,-10,0.795,0.795,0,-24.9,155.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_88.setTransform(-14.6,-11.1,0.211,0.145,0,-0.1,179.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_89.setTransform(-13.1,-29.4,0.339,0.289,0,-24.9,155.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_90.setTransform(-14,-25.9,0.678,0.579,0,-24.9,155.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_91.setTransform(-13.5,-22.8,0.883,0.883,0,-24.9,155.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_92.setTransform(8,-21.7,0.211,0.145,0,-0.1,179.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_93.setTransform(10.3,-40.2,0.339,0.289,0,-24.9,155.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_94.setTransform(8.8,-36.7,0.678,0.579,0,-24.9,155.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_95.setTransform(9.4,-33.3,0.883,0.883,0,-24.9,155.1);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_96.setTransform(-14.5,-34.5,0.211,0.145,0,-0.1,179.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_97.setTransform(-12.4,-53,0.339,0.289,0,-24.9,155.1);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_98.setTransform(-13.9,-49.6,0.678,0.579,0,-24.9,155.1);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_99.setTransform(-13.3,-46.4,0.883,0.883,0,-24.9,155.1);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_100.setTransform(-39,-21.1,0.19,0.13,0,-0.1,179.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_101.setTransform(-37.3,-38,0.305,0.26,0,-24.9,155.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_102.setTransform(-38,-34.4,0.61,0.521,0,-24.9,155.1);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_103.setTransform(-37.8,-31.8,0.795,0.795,0,-24.9,155.1);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#480F1E").ss(1,1,1,3,true).p("AA5itQgMANgJARQgiA2gJBbQgMCAAxAtQAAADgGADQgFACgggCQgNgCgJAAQgdiCALhZQADg0APhZQANAIAaABQAeADATgG");
	this.shape_104.setTransform(2.9,-57);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#275D07").ss(1,1,1,3,true).p("AgBAAIADAB");
	this.shape_105.setTransform(8.4,-74.5);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#35151D").s().p("AgNC1QgNgCgJAAQgdiCALhZQADg0APhZQANAIAaABQAeADATgGIAEACQgMANgJARQgiA2gJBbQgMCAAxAtQAAADgGADQgDABgJAAIgZgBg");
	this.shape_106.setTransform(2.9,-57);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_107.setTransform(20,-63.3,1.251,1.266,0,-51.1,134.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_108.setTransform(19.3,-64.9,1.251,1.266,0,-51.1,134.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-52.7,-77.1,105.3,160.3), null);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A17446").s().p("AAFEiQADgKgJgSQgHgTgMgoIgNgtQgminAHjzIAAgYQAAgOAHgGQAGgGAQAGQANAGApAZQARAFAKAIQAGAGABAJQAFAOgQAjIghBkQgRA7AFAsQAFAvAkBXQAEALAHANQAHAPACAJQAFAKAAArIAAATIACAHQADAGgDAFQgCAJgdADIgJABQgUAAAAgKg");
	this.shape.setTransform(327.9,152.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_1.setTransform(287.9,131.2,2.324,2.324,48.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_2.setTransform(289.2,128.1,2.324,2.324,48.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F02A2A").s().p("AkhJSQgsgZgxgkQiih9hSjEQhOjGAbjLIAAAAQAUikBThxQAVgYAWgXQBChFBYghQAtgRArgGQAxgEAwANQAzAJCNBLIAxAZQApAAAtgOIBIgiQAqgWAhgHQApgKAoAEQA3AGA5AdQBIAmAxBEQALANAIARQAnA/APBVQAVBlgHCAQgKEHhcCmQg5BmhcBCQhjBFhwAGQgbAEg4gEQg6gDgbADQgdADgyAKIhLAJQhbAAhhgtgAhol5QBRAhBPgVQA5gOBFg1QgjAIgnACQhJAEiGgQQgqgHgagIQgjgLgUgZQAnBJBPAjg");
	this.shape_3.setTransform(331.5,220.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A3193B").s().p("AhTAsQhPgkgnhHQATAZAkAKQAZAIAqAHQCHAQBJgEQAngBAigIQhEAzg6APQgfAIgfAAQgwAAgxgUg");
	this.shape_4.setTransform(329.4,178.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,255,255,0.2)").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_5.setTransform(324.4,194.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(197.2,67.3,254.4,254.4), null);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.2)").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape.setTransform(324.4,194.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(197.2,67.3,254.4,254.4), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(365.6,170.2,0.711,0.711,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(352.5,165.4,0.711,0.711,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(303.6,184.8,0.711,0.711,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(333.9,170.2,0.711,0.711,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(326.5,156.3,0.711,0.711,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(334.2,163.1,0.711,0.711,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(321.8,216,0.711,0.711,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(334.2,218.2,0.711,0.711,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(329.8,208.2,0.711,0.711,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(345.1,200.3,0.711,0.711,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(347.4,218.5,0.711,0.711,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(312.7,243,0.711,0.711,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(339.3,247.5,0.711,0.711,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(377.9,206.7,0.711,0.711,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(374.9,190.5,0.711,0.711,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(354,183.1,0.711,0.711,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(358.1,194.7,0.711,0.711,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(365.5,223.3,0.711,0.711,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(361.9,237.1,0.711,0.711,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(326.2,242.3,0.711,0.711,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(302.6,229.8,0.711,0.711,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(299.9,202.9,0.711,0.711,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(366.2,272.9,0.711,0.711,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(341.4,282.8,0.711,0.711,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(290.1,275.8,0.711,0.711,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(306.9,281.4,0.711,0.711,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(326.8,284.2,0.711,0.711,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(309.7,275.4,0.711,0.711,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(283.8,267,0.711,0.711,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(268.9,256.6,0.711,0.711,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(258.4,238.2,0.711,0.711,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(261.5,248.5,0.711,0.711,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(255.8,224.1,0.711,0.711,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(268.5,248,0.711,0.711,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(259.6,231.5,0.711,0.711,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(256.2,217.5,0.711,0.711,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(328.3,259.2,0.711,0.711,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(324.4,269.2,0.711,0.711,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(313.4,261.6,0.711,0.711,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(343,268.6,0.711,0.711,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(356.6,267.5,0.711,0.711,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(359.5,254.7,0.711,0.711,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(369.6,257.5,0.711,0.711,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(371.6,247,0.711,0.711,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(384.7,244,0.711,0.711,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(390.2,231.9,0.711,0.711,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(297.6,254.7,0.711,0.711,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(286.1,250.7,0.711,0.711,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(277.5,243.4,0.711,0.711,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(281.5,231.8,0.711,0.711,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(269.9,219.1,0.711,0.711,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(282.1,216.5,0.711,0.711,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(278.3,194.8,0.711,0.711,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(266.8,192.2,0.711,0.711,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(286.4,189.9,0.711,0.711,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(273.4,180.5,0.711,0.711,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(290.9,177.7,0.711,0.711,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(284.1,170.4,0.711,0.711,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#5D2408").s().p("AgOA0IgdgHQgHgEgRgEQgOgBgHgFQgNgGgGgKQgHgLAAgJQAAgNAIgJQAJgHAagHQAcgHAggEIAEANIADAJIAAAKIAFATIAAAHQAFAAAIgEQAHgCADgIQACgFAFgFQAEgCAHACQABAAAAAAQABAAAAgBQAAAAAAAAQABgBAAAAQAFgFgPgfQAVgCAUACQAWAAAGAMQAKANAAAGQADAFgDAIQgDAHgBALQgDAKgEAKQgNAPgZAKQgdAHgSAAQgPAAgRgGg");
	this.shape_58.setTransform(309.8,170.6);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#692B0E").s().p("ACWEDQgOgmgxiRQg0iNhMhNQhOhKgUgoQgLgbgngjQAMgVADgKQBdA2BFB5QAGAMAJAOQAkBKAwCAQAGAVASAqIAiBmIAUA5IALAcQADAFAAAFQAPAfgFAFQgVgpgSgyg");
	this.shape_59.setTransform(295.4,133.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#4A1D08").s().p("ACcFUIgGgTIAAgKIgDgJIgDgNIgKgjQgHgfgIgbQgVg4gQggIgGgPIgcg4QgSgegPgeQhNh5hohfQgPgOgGgIQgLgOgBgKQgDgWAPgaIAKgOQAnAjAKAbQAUAoBOBKQBMBNA0CNQAxCRAOAmQASAyAVApQAAABAAAAQAAAAAAABQgBAAAAAAQgBAAAAAAQgHgCgFACQgFAFgCAFQgCAIgIAEQgIADgFAAIAAgIg");
	this.shape_60.setTransform(294.2,136.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#A33D0C").s().p("AgVBZIgygRIgngIQgYgGgNgHQgUgLgKgPQgPgRAAgSQAAgWARgQQALgJAZgHQA6gSA9gHIAKAkQghAEgcAHQgaAHgIAHQgJAJAAANQAAAJAIALQAGAKAMAGQAIAFAOABQAQAEAIAEIAcAHQASAGAOAAQATAAAcgHQAZgKANgPQAFgKACgKQABgLADgHQAEgIgEgFQAAgGgJgNQgGgMgXAAQgUgCgUACQAAgGgEgEIgKgcQA0gDA1ADQAlAAAMAVQAOAVACAIQABAKgBAOQgGAMgCATQgEAUgLAOQgSAVgrARQguAPggAAQgXgBgcgHg");
	this.shape_61.setTransform(309.7,170.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#EC5913").s().p("Ak2JsQhsgzhbhWQiGh+gwigQgJgdgFgdQgNg8AAhAIAAgOQAAjkCJiuQAhgpAngoQBBg8BFgpQArgZAugRQAvgWA4gPQAJAbAHAfQg+AGg5ASQgaAIgKAIQgRARAAAVQAAAUAOAQQALAQAUAKQANAHAYAGIAnAJIAyAQQAdAIAWAAQAhAAAtgPQAqgQATgWQALgOAEgUQACgUAGgMQABgOgBgJQgCgJgOgVQgMgUglgBQg1gDgzADIgVg5QBHgOBKgBQBuABBmAcQCkAwCHB8IAhAjQCvC/AAEBQAACRg5B8QgzB0hkBhQjWDIkpABQipAAiNhAg");
	this.shape_62.setTransform(324.5,222.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("rgba(255,255,255,0.2)").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_63.setTransform(324.4,194.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_63).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(197.2,67.3,254.4,254.4), null);


(lib.qtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgJBUIgDgCIgCgEIgCgEIACgFIACgEIADgCIAFgBIAEABIADACIADAEIABAFIgBAEIgDAEIgDACIgEABIgFgBgAgPAwIgBgIQAAgGACgFQACgFADgEQADgFAFgDIAIgHIAIgGIAIgIQAEgFABgFQADgFAAgHQAAgGgDgHQgCgGgEgFQgEgFgGgDQgGgDgHAAQgGAAgGADQgGACgDAFQgEAFgCAGQgCAGABAGIAAAFIAAAGIgSACIgBgHIgBgGQAAgLADgJQAEgJAHgHQAGgHAKgEQAJgEAKAAQALAAAJAEQAJAEAHAHQAGAHAEAJQAEAKAAALIAAAKIgEALIgFAKQgEAEgFAFIgLAHIgKAJQgEAEgDAGQgDAGACAIg");
	this.shape.setTransform(215.4,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgXA3QgKgEgHgHQgHgIgEgKQgDgLAAgOQAAgLADgLQAFgLAIgJQAHgHALgFQAJgFALAAQALAAAJAEQAKAEAHAHQAHAIAGAJQAEALABALIhYANQABAHADAFQADAHAFADQAEAFAGABQAGACAFAAIALgBIAKgFIAIgIQADgFACgHIATAFQgCAJgGAIQgFAHgGAFQgIAGgIADQgIADgKAAQgMAAgLgEgAgIgnQgFABgFAEQgFAEgDAFQgEAHgCAJIA9gIIgBgCQgDgKgHgGQgIgGgKAAIgIACg");
	this.shape_1.setTransform(198.1,3.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSA3QgLgEgIgIQgJgIgFgLQgFgLAAgNQAAgGACgIIAGgOQAEgHAGgFQAFgGAGgEQAIgEAHgCQAIgDAHAAQAKAAAHADQAIACAIAEQAGAEAFAGQAGAGADAHIABAAIgRALIgBgBQgCgFgEgEQgEgEgEgDQgFgDgFgCIgMgBQgHAAgHADQgIADgFAGQgGAFgDAIQgDAHAAAHQAAAJADAHQADAHAGAGQAFAFAIAEQAHADAHAAQAGAAAFgCIAKgEIAIgGQAEgEACgEIABgBIARAKIAAABQgDAGgHAGQgFAFgHAEQgHAEgIACQgHACgJAAQgKAAgLgFg");
	this.shape_2.setTransform(185.5,3.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgvAZIgBgZIgBgTIAAgPIgBgWIAYAAIAAAdIAIgLIAJgJQAFgEAFgCQAGgDAGAAQAIgBAFACQAGACAEADIAHAIIAEAJIACAKIABAIIABAjIgBAlIgVgBIABghQABgQgBgQIAAgFIgBgGIgDgHIgEgFQgCgDgEgBQgDgBgFABQgIABgHAKQgJALgJATIAAAbIABAPIAAAIIgWACIgBggg");
	this.shape_3.setTransform(172.5,3.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgWA3QgLgEgHgHQgHgIgEgKQgDgLgBgOQAAgLAFgLQAEgLAIgJQAHgHALgFQAKgFAKAAQALAAAKAEQAJAEAHAHQAHAIAGAJQAEALABALIhXANQABAHACAFQADAHAFADQAEAFAGABQAGACAFAAIAMgBIAJgFIAIgIQADgFACgHIATAFQgCAJgGAIQgEAHgIAFQgHAGgIADQgJADgJAAQgMAAgKgEgAgIgnQgFABgFAEQgEAEgFAFQgEAHgBAJIA9gIIgBgCQgDgKgIgGQgGgGgLAAIgIACg");
	this.shape_4.setTransform(160,3.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAcAcIgIALQgEAGgFAEQgFAEgFACQgFACgGAAQgJAAgHgDQgHgEgEgFQgFgFgCgHIgFgPIgCgPIAAgPIABgWIACgWIAWABIgCAXIgBARIAAAJIABALIACALIAEALQADAEADADQAFACAFgBQAHgBAJgJQAJgKAJgTIAAgaIgBgPIAAgJIAVgCIABAgIABAYIACAUIAAAPIAAAWIgXAAg");
	this.shape_5.setTransform(147,3.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAjBXIAChGIgGAFIgHAEIgHADIgFACIgMABQgMAAgKgEQgLgEgHgIQgIgHgEgLQgEgLgBgOQAAgOAGgKQAEgLAIgIQAIgHAKgEQAJgEAKAAQAHAAAHACQAGACAGADQAHADAHAGIAAgRIAUACIAACrgAgOg9QgHADgEAFQgEAFgDAIQgCAGAAAJQAAAJACAGQADAIAEAEQAEAFAHADQAGADAIAAQAFAAAHgCQAGgDAFgEQAFgEAEgEQAEgHABgHIAAgPQgBgHgEgGQgEgFgFgFQgGgEgFgCQgHgCgGgBQgHAAgGAEg");
	this.shape_6.setTransform(133.2,5.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgWA3QgLgEgHgHQgHgIgEgKQgEgLAAgOQAAgLAFgLQAEgLAHgJQAIgHAKgFQALgFALAAQAKAAAKAEQAJAEAIAHQAHAIAFAJQAEALACALIhYANQAAAHADAFQADAHAEADQAFAFAGABQAFACAHAAIALgBIAJgFIAIgIQADgFACgHIATAFQgCAJgFAIQgGAHgHAFQgGAGgJADQgJADgIAAQgNAAgKgEgAgIgnQgFABgFAEQgEAEgFAFQgEAHgBAJIA9gIIAAgCQgFgKgHgGQgHgGgJAAIgJACg");
	this.shape_7.setTransform(120.6,3.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgOA6IgJgCIgJgDIgJgFIgJgFIAJgQIAMAGIAMAFIANADIAMABIAKgBIAGgDIAEgDIACgEIAAgEIgBgEIgDgEIgFgDIgIgDQgFgBgGAAIgRgCQgJgCgGgDQgHgDgEgEQgFgFgBgIQAAgIABgGQACgGADgFIAJgIIALgGQAGgDAGgBIANgBIAJABIAMABIAMAEIALAGIgHATQgHgEgGgCIgLgDIgKgCQgPgBgJAFQgJAEAAAJQAAAGADADQADACAGACIAOABIAQACQAKACAGADQAHACAFAEQAEAEACAFQABAFAAAFQAAAKgDAHQgEAGgHAEQgHAFgIACQgJACgJAAIgSgCg");
	this.shape_8.setTransform(108.4,3.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgXA3QgKgEgHgHQgHgIgEgKQgDgLAAgOQAAgLADgLQAFgLAHgJQAIgHAKgFQALgFALAAQAKAAAKAEQAJAEAIAHQAGAIAFAJQAFALACALIhYANQABAHACAFQADAHAEADQAFAFAGABQAFACAHAAIALgBIAJgFIAIgIQAEgFABgHIAUAFQgDAJgFAIQgGAHgHAFQgGAGgJADQgJADgIAAQgNAAgLgEgAgIgnQgFABgFAEQgEAEgEAFQgFAHgBAJIA9gIIAAgCQgEgKgHgGQgHgGgKAAIgJACg");
	this.shape_9.setTransform(91.1,3.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAbBOIAFgTIACgRIAAgRQgBgLgDgHQgDgHgFgEQgEgGgHgBQgFgDgGAAQgFABgFADQgFACgGAGQgHADgFAKIAABEIgUAAIgCidIAYgBIgBA+QAGgFAGgEIAMgFIAMgDQALAAAKAEQAJAEAHAHQAHAJAEAKQADALABAOIAAAOIgBAPIgCAOIgCAMg");
	this.shape_10.setTransform(78,1.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgMgLIgjABIAAgTIAjgBIABgtIAUgBIgBAtIAogBIgBATIgnABIgBBZIgUAAg");
	this.shape_11.setTransform(66.1,1.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvAZIgBgZIgBgTIAAgPIgBgWIAYAAIAAAdIAIgLIAJgJQAFgEAFgCQAGgDAGAAQAIgBAFACQAGACAEADIAHAIIAEAJIACAKIABAIIABAjIgBAlIgVgBIABghQABgQgBgQIAAgFIgBgGIgDgHIgEgFQgCgDgEgBQgDgBgFABQgIABgHAKQgJALgJATIAAAbIABAPIAAAIIgWACIgBggg");
	this.shape_12.setTransform(49,3.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgMBNIABgaIAAggIABgsIAUAAIgBAaIAAAWIAAASIgBAOIAAAWgAgFgwIgEgDIgDgFIgCgFIACgGIADgFIAEgDQADgBACAAQADAAADABIAEADIAEAFIABAGIgBAFIgEAFIgEADQgDABgDAAQgCAAgDgBg");
	this.shape_13.setTransform(40.1,1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgWA3QgLgEgHgHQgHgIgEgKQgDgLgBgOQAAgLAEgLQAFgLAIgJQAHgHALgFQAKgFAKAAQALAAAJAEQAKAEAHAHQAHAIAGAJQAEALABALIhYANQACAHACAFQADAHAFADQAEAFAGABQAGACAFAAIALgBIAKgFIAIgIQADgFACgHIATAFQgCAJgGAIQgFAHgGAFQgIAGgIADQgIADgKAAQgMAAgKgEgAgIgnQgFABgFAEQgFAEgEAFQgDAHgCAJIA9gIIgBgCQgDgKgIgGQgGgGgLAAIgIACg");
	this.shape_14.setTransform(25.8,3.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AguAZIAAgZIgBgTIgBgPIAAgWIAXAAIAAANIAAAMIABAMIAHgKIAKgLQAFgFAFgEQAHgDAHgBQAIgBAHAEIAGAEIAFAHIAEALQACAFAAAIIgUAIIgBgJIgCgGIgDgEIgDgDQgDgDgFAAIgGADIgGAFIgGAHIgHAIIgGAIIgFAHIAAAPIABAOIAAALIABAJIgWACIgCggg");
	this.shape_15.setTransform(14.3,3.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgWA3QgLgEgHgHQgHgIgEgKQgDgLgBgOQAAgLAEgLQAFgLAIgJQAHgHALgFQAKgFAKAAQALAAAJAEQAKAEAHAHQAHAIAGAJQAEALABALIhYANQACAHACAFQADAHAFADQAEAFAGABQAGACAFAAIALgBIAKgFIAIgIQADgFACgHIATAFQgCAJgGAIQgFAHgGAFQgIAGgIADQgIADgKAAQgMAAgKgEgAgIgnQgFABgFAEQgFAEgEAFQgDAHgCAJIA9gIIgBgCQgDgKgIgGQgGgGgLAAIgIACg");
	this.shape_16.setTransform(2,3.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAcBOIAEgTIABgRIABgRQAAgLgEgHQgDgHgFgEQgFgGgFgBQgGgDgGAAQgEABgHADQgEACgHAGQgFADgFAKIgBBEIgUAAIgCidIAXgBIAAA+QAGgFAGgEIAMgFIALgDQAMAAAJAEQAKAEAGAHQAIAJADAKQAFALAAAOIAAAOIgBAPIgBAOIgDAMg");
	this.shape_17.setTransform(-11.1,1.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgOA6IgJgCIgJgDIgJgFIgJgFIAJgQIAMAGIAMAFIANADIAMABIAKgBIAGgDIAEgDIACgEIAAgEIgBgEIgDgEIgFgDIgIgDQgFgBgGAAIgRgCQgJgCgGgDQgHgDgEgEQgFgFgBgIQAAgIABgGQACgGADgFIAJgIIALgGQAGgDAGgBIANgBIAJABIAMABIAMAEIALAGIgHATQgHgEgGgCIgLgDIgKgCQgPgBgJAFQgJAEAAAJQAAAGADADQADACAGACIAOABIAQACQAKACAGADQAHACAFAEQAEAEACAFQABAFAAAFQAAAKgDAHQgEAGgHAEQgHAFgIACQgJACgJAAIgSgCg");
	this.shape_18.setTransform(-29.5,3.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgXA3QgKgEgHgHQgHgIgEgKQgDgLAAgOQAAgLADgLQAFgLAIgJQAHgHALgFQAJgFALAAQALAAAJAEQAKAEAHAHQAHAIAGAJQAEALABALIhYANQABAHADAFQADAHAFADQAEAFAGABQAGACAFAAIALgBIAKgFIAIgIQADgFACgHIATAFQgCAJgGAIQgFAHgGAFQgIAGgIADQgIADgKAAQgMAAgLgEgAgIgnQgFABgFAEQgFAEgEAFQgDAHgCAJIA9gIIgBgCQgDgKgIgGQgHgGgKAAIgIACg");
	this.shape_19.setTransform(-41.3,3.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAxAYIAAgWIAAgQQgBgIgCgEQgCgEgEAAIgFABIgGAEIgFAGIgFAGIgEAHIgDAFIAAAJIAAAMIABAQIAAATIgTABIgBggIAAgWIgBgQQgBgIgCgEQgDgEgEAAQgCAAgDACIgFAEIgGAGIgFAHIgFAHIgEAFIABA2IgUABIgDhtIAWgCIAAAdIAHgIIAIgIQAEgEAFgCQAFgCAGAAIAIABIAHAEIAFAIIADAKIAHgIIAIgHQAEgEAEgCQAFgCAGAAQAFAAAEABQAEACAEADQADADACAFQACAFAAAHIACATIAAAYIABAkIgWABIAAggg");
	this.shape_20.setTransform(-56.2,3.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgUA4QgKgEgIgIQgHgIgEgLQgFgLAAgOQAAgNAFgLQAEgLAHgIQAIgIAKgEQAKgEAKAAQALAAAKAEQAKAFAHAIQAIAIAFALQAEALAAAMQAAANgEALQgFALgIAIQgHAIgKAFQgKAEgLAAQgKAAgKgEgAgOglQgGADgFAGQgDAGgCAIIgCAOQAAAHACAIQACAHAEAGQAEAGAHAEQAGAEAHAAQAJAAAGgEQAGgEAEgGQAFgGABgHQACgIAAgHIgBgOQgCgIgFgGQgDgGgHgDQgGgEgJAAQgIAAgGAEg");
	this.shape_21.setTransform(-70.9,3.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgSA3QgLgEgJgIQgIgIgFgLQgFgLAAgNQAAgGACgIIAGgOQAEgHAFgFQAGgGAHgEQAGgEAIgCQAIgDAHAAQAJAAAIADQAJACAHAEQAGAEAFAGQAGAGADAHIABAAIgRALIgBgBQgCgFgEgEQgDgEgGgDQgEgDgGgCIgLgBQgHAAgIADQgGADgGAGQgFAFgEAIQgDAHAAAHQAAAJADAHQAEAHAFAGQAGAFAGAEQAIADAHAAQAGAAAFgCIAJgEIAJgGQAEgEACgEIABgBIARAKIAAABQgEAGgFAGQgGAFgHAEQgHAEgHACQgJACgIAAQgKAAgLgFg");
	this.shape_22.setTransform(-83.5,3.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgMgLIgjABIAAgTIAjgBIABgtIAUgBIgBAtIAogBIgBATIgnABIgBBZIgUAAg");
	this.shape_23.setTransform(-100.3,1.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAfA6IACgOQgMAIgMAEQgLADgLAAQgHAAgHgBQgHgCgGgFQgFgDgDgHQgDgFAAgJQAAgJACgGQADgGAEgFQAEgFAGgDIANgGIAOgDIAOgBIAMAAIAKACIgDgKIgFgJQgDgEgEgDQgEgCgHAAIgIABQgFABgGADQgHADgGAFQgIAGgIAJIgMgQQAJgJAKgGIARgJQAHgDAHgBIAMgCQAIAAAIADQAHAEAGAFQAGAGAEAIQADAHADAIQACAJABAJIABASIgBAVIgDAZgAgEAAQgJACgGADQgGAFgDAFQgEAHACAHQACAHAEACQAEACAGAAIANgBIAMgGIAOgGIAKgGIAAgKIAAgKIgLgCIgLgBQgKAAgHACg");
	this.shape_24.setTransform(-112.2,3.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAcBOIAEgTIACgRIAAgRQgBgLgDgHQgDgHgFgEQgFgGgGgBQgFgDgGAAQgEABgGADQgGACgGAGQgGADgFAKIAABEIgUAAIgCidIAXgBIAAA+QAGgFAGgEIAMgFIALgDQAMAAAKAEQAJAEAGAHQAHAJAFAKQAEALAAAOIAAAOIgBAPIgBAOIgDAMg");
	this.shape_25.setTransform(-125.2,1.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAEgUIgYBHIgfACIgUhpIAVgCIAPBWIAfhWIATACIAUBWIAPhXIAWABIgUBqIgeABg");
	this.shape_26.setTransform(-140.1,3.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgLgLIgkABIABgTIAjgBIABgtIATgBIAAAtIAngBIgCATIgmABIgBBZIgVAAg");
	this.shape_27.setTransform(-158.4,1.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgSA3QgLgEgJgIQgIgIgFgLQgFgLAAgNQAAgGACgIIAGgOQAEgHAFgFQAGgGAGgEQAIgEAHgCQAIgDAIAAQAIAAAJADQAHACAHAEQAHAEAGAGQAFAGAEAHIAAAAIgRALIAAgBQgDgFgEgEQgEgEgFgDQgEgDgGgCIgKgBQgIAAgIADQgGADgGAGQgGAFgDAIQgDAHAAAHQAAAJADAHQADAHAGAGQAGAFAGAEQAIADAIAAQAFAAAFgCIAJgEIAJgGIAHgIIAAgBIASAKIAAABQgFAGgFAGQgGAFgHAEQgHAEgHACQgJACgHAAQgLAAgLgFg");
	this.shape_28.setTransform(-169.7,3.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgWA3QgLgEgHgHQgHgIgEgKQgEgLAAgOQAAgLAFgLQAEgLAHgJQAIgHAKgFQALgFALAAQAKAAAKAEQAJAEAIAHQAHAIAEAJQAFALACALIhYANQAAAHADAFQADAHAEADQAFAFAGABQAFACAHAAIALgBIAJgFIAIgIQADgFACgHIATAFQgCAJgFAIQgGAHgHAFQgGAGgJADQgJADgIAAQgNAAgKgEgAgIgnQgFABgFAEQgEAEgFAFQgEAHgBAJIA9gIIAAgCQgFgKgHgGQgHgGgJAAIgJACg");
	this.shape_29.setTransform(-182.2,3.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAGBZQgFgCgDgEIgIgIIgFgKQgFgMgBgPIADiBIAUAAIgBAiIgBAcIAAAWIAAAQIAAAcQAAAKACAHIACAGIAEAGIAGAEQADABAFAAIgDAVQgHAAgGgDg");
	this.shape_30.setTransform(-190.9,-0.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgXA3QgKgEgHgHQgHgIgEgKQgDgLAAgOQAAgLADgLQAFgLAHgJQAIgHAKgFQALgFALAAQAKAAAJAEQAKAEAIAHQAGAIAFAJQAFALACALIhZANQACAHACAFQADAHAEADQAFAFAGABQAFACAHAAIAKgBIAKgFIAIgIQAEgFABgHIAUAFQgDAJgFAIQgGAHgHAFQgGAGgJADQgJADgIAAQgNAAgLgEgAgIgnQgFABgFAEQgEAEgEAFQgFAHgBAJIA9gIIAAgCQgEgKgHgGQgHgGgKAAIgJACg");
	this.shape_31.setTransform(-200.5,3.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgNBTIgPgGIgOgHIgPgJIAPgSQAIAHAIAFIAOAFIANAEQAHAAAHgBQAGgCAFgDQAFgCADgFQACgEABgEQAAgEgBgDIgFgFQgDgEgFgCIgJgDIgKgCIgJgDIgMgDQgHgBgGgDQgGgCgGgFQgFgDgEgFQgEgHgCgHQgDgIABgKQABgIADgGQACgHAFgGQAEgEAGgFIAMgFIAOgEIANgBQAKABAJACIAJADIAJADIAJAGIAJAHIgLASIgHgHIgIgFIgHgDIgHgCIgQgDQgIAAgHADQgIACgFAFQgFAEgCAGQgDAGAAAFQAAAFADAFQADAFAGAEIANAHQAHACAIABIAPADIAOAEIANAHQAFAEAEAGQAEAEACAHQACAHgBAIQgBAHgDAFQgDAGgFAEQgEAFgGACQgGADgGABIgMAEIgMAAQgHAAgIgBg");
	this.shape_32.setTransform(-213.6,1.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#003366").ss(3,1,1).p("EghngC3MBDPAAAQBCAAAtA2QAvA2AABLQAABMgvA2QgtA2hCAAMhDPAAAQhBAAgvg2Qgug2AAhMQAAhLAug2QAvg2BBAAg");
	this.shape_33.setTransform(2.4,2.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#0066CC").s().p("EghnAC4QhBAAgvg2Qgug2AAhMQAAhLAug2QAvg2BBAAMBDPAAAQBCAAAtA2QAvA2AABLQAABMgvA2QgtA2hCAAg");
	this.shape_34.setTransform(2.4,2.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.qtext, new cjs.Rectangle(-230.1,-17,465.1,39.8), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape.setTransform(334.2,148.1,0.261,0.179,0,-0.2,179.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_1.setTransform(337,125.3,0.419,0.357,0,-24.9,155.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_2.setTransform(335.2,129.7,0.837,0.715,0,-24.9,155.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_3.setTransform(335.9,133.5,1.09,1.09,0,-24.9,155.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_4.setTransform(351.8,203.8,0.235,0.161,0,-0.1,179.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_5.setTransform(353.7,183.2,0.377,0.321,0,-24.9,155.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_6.setTransform(352.3,186.8,0.754,0.643,0,-24.9,155.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_7.setTransform(352.7,190.4,0.981,0.981,0,-24.9,155.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_8.setTransform(334.5,240.8,0.235,0.161,0,-0.1,179.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_9.setTransform(336.6,219.8,0.377,0.321,0,-24.9,155.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_10.setTransform(334.9,223.9,0.754,0.643,0,-24.9,155.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_11.setTransform(335.6,227.6,0.981,0.981,0,-24.9,155.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_12.setTransform(313,192,0.261,0.179,0,-0.2,179.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_13.setTransform(315.3,169.3,0.419,0.357,0,-24.9,155.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_14.setTransform(313.8,173.7,0.837,0.715,0,-24.9,155.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_15.setTransform(314.7,177.7,1.09,1.09,0,-24.9,155.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_16.setTransform(327,215.2,0.235,0.161,0,-0.1,179.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_17.setTransform(329,194.7,0.377,0.321,0,-24.9,155.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_18.setTransform(327.6,198.4,0.754,0.643,0,-24.9,155.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_19.setTransform(328.5,202.1,0.981,0.981,0,-24.9,155.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_20.setTransform(339.9,288.7,0.235,0.161,0,-0.1,179.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_21.setTransform(341.8,268.8,0.377,0.321,0,-24.9,155.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_22.setTransform(340.5,272.7,0.754,0.643,0,-24.9,155.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_23.setTransform(340.8,276.5,0.981,0.981,0,-24.9,155.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_24.setTransform(319.8,274,0.235,0.161,0,-0.1,179.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_25.setTransform(321.6,254,0.377,0.321,0,-24.9,155.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_26.setTransform(320.1,258,0.754,0.643,0,-24.9,155.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_27.setTransform(320.6,261.9,0.981,0.981,0,-24.9,155.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_28.setTransform(348.6,269.9,0.235,0.161,0,-0.1,179.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_29.setTransform(350.8,250.4,0.377,0.321,0,-24.9,155.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_30.setTransform(349.2,254.3,0.754,0.643,0,-24.9,155.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_31.setTransform(349.7,257.9,0.981,0.981,0,-24.9,155.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_32.setTransform(307.6,249.4,0.235,0.161,0,-0.1,179.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_33.setTransform(309.7,228.9,0.377,0.321,0,-24.9,155.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_34.setTransform(308.3,232.9,0.754,0.643,0,-24.9,155.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_35.setTransform(309.1,236.5,0.981,0.981,0,-24.9,155.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_36.setTransform(362.5,251.7,0.235,0.161,0,-0.1,179.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_37.setTransform(364.7,231.2,0.377,0.321,0,-24.9,155.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_38.setTransform(363.3,235,0.754,0.643,0,-24.9,155.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_39.setTransform(364,238.9,0.981,0.981,0,-24.9,155.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_40.setTransform(331.4,252.4,0.235,0.161,0,-0.1,179.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_41.setTransform(333.6,232.2,0.377,0.321,0,-24.9,155.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_42.setTransform(332.1,236.2,0.754,0.643,0,-24.9,155.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_43.setTransform(332.8,240,0.981,0.981,0,-24.9,155.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_44.setTransform(296.1,224.9,0.235,0.161,0,-0.1,179.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_45.setTransform(298.2,203.7,0.377,0.321,0,-24.9,155.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_46.setTransform(296.8,208,0.754,0.643,0,-24.9,155.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_47.setTransform(297.4,211.8,0.981,0.981,0,-24.9,155.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_48.setTransform(369.8,220.6,0.235,0.161,0,-0.1,179.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_49.setTransform(372.1,199.9,0.377,0.321,0,-24.9,155.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_50.setTransform(370.5,203.7,0.754,0.643,0,-24.9,155.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_51.setTransform(371.3,207.5,0.981,0.981,0,-24.9,155.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_52.setTransform(348.9,234.2,0.235,0.161,0,-0.1,179.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_53.setTransform(350.9,213.2,0.377,0.321,0,-24.9,155.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_54.setTransform(349.6,216.9,0.754,0.643,0,-24.9,155.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_55.setTransform(350.3,220.9,0.981,0.981,0,-24.9,155.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_56.setTransform(321.2,225.7,0.235,0.161,0,-0.1,179.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_57.setTransform(323.4,204.9,0.377,0.321,0,-24.9,155.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_58.setTransform(322.1,208.6,0.754,0.643,0,-24.9,155.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_59.setTransform(322.8,212.6,0.981,0.981,0,-24.9,155.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_60.setTransform(366.1,192.1,0.235,0.161,0,-0.1,179.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_61.setTransform(368.3,171.5,0.377,0.321,0,-24.9,155.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_62.setTransform(366.7,175.2,0.754,0.643,0,-24.9,155.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_63.setTransform(367.5,179,0.981,0.981,0,-24.9,155.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_64.setTransform(345,206.9,0.235,0.161,0,-0.1,179.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_65.setTransform(347.1,186.3,0.377,0.321,0,-24.9,155.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_66.setTransform(345.7,190,0.754,0.643,0,-24.9,155.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_67.setTransform(346.3,193.8,0.981,0.981,0,-24.9,155.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_68.setTransform(293.7,201.8,0.235,0.161,0,-0.1,179.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_69.setTransform(296,180.9,0.377,0.321,0,-24.9,155.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_70.setTransform(294.3,184.6,0.754,0.643,0,-24.9,155.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_71.setTransform(295,188.7,0.981,0.981,0,-24.9,155.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_72.setTransform(319.8,199.8,0.235,0.161,0,-0.1,179.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_73.setTransform(321.8,179.2,0.377,0.321,0,-24.9,155.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_74.setTransform(320.3,183.1,0.754,0.643,0,-24.9,155.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_75.setTransform(320.9,186.6,0.981,0.981,0,-24.9,155.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_76.setTransform(364.5,169,0.235,0.161,0,-0.1,179.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_77.setTransform(366.4,148.4,0.377,0.321,0,-24.9,155.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_78.setTransform(364.8,152.2,0.754,0.643,0,-24.9,155.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_79.setTransform(365.7,155.9,0.981,0.981,0,-24.9,155.1);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_80.setTransform(341.1,186.3,0.235,0.161,0,-0.1,179.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_81.setTransform(343,165.6,0.377,0.321,0,-24.9,155.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_82.setTransform(341.9,169.6,0.754,0.643,0,-24.9,155.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_83.setTransform(342.2,173.3,0.981,0.981,0,-24.9,155.1);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_84.setTransform(277.5,190.8,0.235,0.161,0,-0.1,179.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_85.setTransform(279.6,170.5,0.377,0.321,0,-24.9,155.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_86.setTransform(278.1,174.6,0.754,0.643,0,-24.9,155.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_87.setTransform(278.5,178.4,0.981,0.981,0,-24.9,155.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_88.setTransform(306.3,177,0.261,0.179,0,-0.2,179.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_89.setTransform(308.2,154.4,0.419,0.357,0,-24.9,155.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_90.setTransform(307,158.7,0.837,0.715,0,-24.9,155.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_91.setTransform(307.7,162.5,1.09,1.09,0,-24.9,155.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_92.setTransform(334.2,163.9,0.261,0.179,0,-0.2,179.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_93.setTransform(337,141.1,0.419,0.357,0,-24.9,155.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_94.setTransform(335.2,145.3,0.837,0.715,0,-24.9,155.1);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_95.setTransform(335.9,149.6,1.09,1.09,0,-24.9,155.1);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_96.setTransform(306.4,148.1,0.261,0.179,0,-0.2,179.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_97.setTransform(309,125.3,0.419,0.357,0,-24.9,155.1);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_98.setTransform(307.2,129.4,0.837,0.715,0,-24.9,155.1);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_99.setTransform(308,133.5,1.09,1.09,0,-24.9,155.1);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#69359A").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_100.setTransform(276.2,164.6,0.235,0.161,0,-0.1,179.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#DC67FF").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_101.setTransform(278.3,143.8,0.377,0.321,0,-24.9,155.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#A74ED5").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_102.setTransform(277.3,148.3,0.754,0.643,0,-24.9,155.1);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#603B92").s().p("AiEB1Qg3g1ACg9QABg8AxgvQAxgvBJgPQBIgOBAAvQA/AwAABLQAABKg3A1QgLALgMAJQgMAIgMAGQgmATguAAQhNAAg3g1g");
	this.shape_103.setTransform(277.7,151.4,0.981,0.981,0,-24.9,155.1);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#480F1E").ss(1,1,1,3,true).p("ABGjWQgOAQgMAVQgqBCgLBxQgPCeA8A4QABADgIAFQgFACgpgCQgQgDgKAAQglihAOhuQAEhAAShuQAQAKAgACQAmADAYgI");
	this.shape_104.setTransform(327.9,120.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#275D07").ss(1,1,1,3,true).p("AgBAAIADAB");
	this.shape_105.setTransform(334.7,98.8);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#35151D").s().p("AgRDgQgQgDgKAAQglihAOhuQAEhAAShuQAQAKAgACQAmADAYgIIAEADQgOAQgMAVQgqBCgLBxQgPCeA8A4QABADgIAFQgCABgMAAIgggBg");
	this.shape_106.setTransform(327.9,120.4);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_107.setTransform(349.1,112.5,1.545,1.563,0,-51.1,134.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_108.setTransform(348.1,110.5,1.545,1.563,0,-51.1,134.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_109.setTransform(324.4,194.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_109).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(197.2,67.3,254.4,254.4), null);


(lib.Symbol1copy3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6B4332").s().p("AAsArIgCgGQgDgKgKgIQgFgDgHgBIABgIQACgFADgCQAFgCAIAAIACAAIACAEIACAFIABACIAEAHQACAGABAJIgDAPQgBAAAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAgAB2ALIACgDQADgHAFgFIADgGIADgFQAIABAIAGQANAMAIAOQgPgGgSADQgHAAgGACIgKACIADgIgAiOgIIgHAAIgMgBIgJgBIADgBIAFgIQADgFAEgCQADgCAHAAQAHACAGACIADAEQgGAFgFAIIgCgBgACAgdIgPABIgCgHIgBgKQAJAAAJAEQAJAFAGAIQgHgCgIABg");
	this.shape_6.setTransform(347.3,263.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D0A815").s().p("AA5BlQgVgDgLAEQgHADgGAHQgFAHAAAGQgIgQgPgVQgEgFgOAJQgCgdgHgLQgdgqgSgxQgNgogFgpQAHABAHgDQAHgCAFgFIAAgDIALAwQAUBPAfBPIADADQANAIARgCQAMgBAPgEIAagJQAXgHAXACQAAAGABAGIABAKIACAHIAPgCIAEAEQADAFAAAFIAAABIgDAFIgEAFQgFAHgDAHIgCADQgdgYgjgIgAiCB2QgCgDgFgBQAFgIAGgGIACgEQACgDAFgDQAFgCAIAAIALgCIALgEIAHgEQAJATAPARIAEAEQgLgCgMAAQgaACgMAAIgJABIgNgBg");
	this.shape_7.setTransform(347,251.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B19015").s().p("AAQCKIgIgDQgFAAgDgBQgEgDgFgGIgJgKQgHgJgJgIIgPAGIgEgEQgPgRgJgTIADgDIAHgIQgPgngKgmQgGgfgLgrIgJgoIAIAEQAFApANAoQASAzAdApQAHAKACAdQAOgIAEAEQAPAVAIARQAAgHAFgHQAGgGAHgEQALgEAVAEQAjAHAdAYIgCAIIgGAEQgMAAgJgEIgIgFIgJgFQgIgFgMAAQgCgDgDgCIgJgCIgCgBQgIAAgFADQgDADgBAEIgBAJIAAABQgBAHACAIIgEgBg");
	this.shape_8.setTransform(347.2,253.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#649FCD").s().p("AgPAoIgIgEIgDgCQgKgGgHgPQgFgLgCgLQgCgOABgQIAAAAIATAAIACAVQAEAQAGAOQADAIAGAAQADAAAEgHQAJgMABAAIALgMQAPgLAUgDQgSAWgPAWQgGAIgDACIAAACQgFAFgGACQgEACgFAAIgFAAg");
	this.shape_9.setTransform(337.7,235.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#73ACD9").s().p("AgmAZQgGgOgEgPIgCgWIAlAAIAUgBQAXgBAVgEIgRAVQgUACgOALIgLAMQgBAAgKANQgEAGgDAAIAAAAQgGAAgDgIg");
	this.shape_10.setTransform(339.5,234.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#37414D").s().p("AASgIIADAAIgpATQAJgcAdAJg");
	this.shape_11.setTransform(401.8,143.6,0.543,0.543,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F7FAFA").s().p("AgHASQgEgDgCgGQgBgGADgHQADgHAGgEQAFgDAEACQAFACABAGQACAHgDAGQgDAHgGAEQgDACgDAAIgEAAg");
	this.shape_12.setTransform(387.7,137.9,0.543,0.543,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(0.5,1,1).p("ABnAAQAABBgeAuQgeAvgrAAQgqAAgegvQgeguAAhBQAAhBAeguQAeguAqAAQArAAAeAuQAeAuAABBg");
	this.shape_13.setTransform(388.6,139.1,0.131,0.131,0,-23.2,156.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#36424B").s().p("AhIBwQgfgvAAhBQAAhBAfguQAeguAqAAQArAAAeAuQAfAugBBBQABBBgfAvQgeAugrAAQgqAAgegug");
	this.shape_14.setTransform(388.6,139.1,0.131,0.131,0,-23.2,156.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FDFAFE").s().p("AgvCGQgkgNgMgxQgLgwATg2QAUg4AogeQAngeAkANQAlANALAwQAMAxgUA2QgUA4gnAeQgcAVgZAAQgMAAgLgEg");
	this.shape_15.setTransform(388.2,139.6,0.306,0.306,0,-3.5,176.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_16.setTransform(283,212.5,0.289,0.289,0,-2.5,177.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_17.setTransform(297.4,211,0.356,0.356,0,-2.5,177.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_18.setTransform(310.2,208.8,0.44,0.44,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_19.setTransform(324.6,204.3,0.543,0.543,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_20.setTransform(395.2,121.5,0.57,0.57,0,35.6,-144.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_21.setTransform(389.9,119.2,0.57,0.57,0,27.2,-152.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_22.setTransform(384.8,118.8,0.57,0.57,0,17.4,-162.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_23.setTransform(379.9,120.5,0.543,0.543,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#4D8C0F").s().p("ACUCBQAPgZALgRQAagoAcgeIAHACIAuAMIgRAHQgOAGgNAIIgCABQgeATgYAYQgTARgPAUIABgEgAAZB/QAWgXAggmIAPgSIAPgSIgWALIgaARQgXAOgTAQIgUATIgPAPIABgEIAMgMIAWgWIATgRQAmgjBAgqIADABIAjALIAJADQghAfgeAiIgQAGQguAUgkAfIgFAFIAEgFgAjnAXIAHgHQAWgUAZgRIABAAQAhgQAngNQAlgNA/gSIAXAOIAQAKIAUAKIADABQgUALgTANQglAYgiAdIgfAPQgeARgiAVQAUgcAYgYQAWgWAZgRQATgNAUgMIAYgOQgUADgUAEQgaAGgYAHQgWAHgWAJIgVAKQgoATgkAaQgeAYgaAbQARgnAggigAjgg5IACAAQAKgLANgJQAMgJAOgHIAVgNQASgIAUgHIAWgHIAOgFIAVAQQgfAGgiANQgjAOg6AgIgJAFIgBABQgMAGgKAGIAXgXg");
	this.shape_24.setTransform(266.9,223);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#38660A").s().p("ACzBWQAIgSAMgRQgcAEgbAKQAegjAhgeIACAAQAEABADACIACABIAUAFIAKADQgcAdgaAoQgLARgPAZQADgTAIgSgAAMBhQAKgXARgVQADgFAEgDIAFgGQAagbAhgRIACgCIAJgFIgfABQgZAEgaAHIgfAIIgGAEIgiANQAhgcAmgZQATgNAUgKQAbAOAcAKIAKAFIARAHQhAAqgmAiIgTARIgXAWIgMANIAIgQgAjjgnQAKgGAMgGIABgCIAJgEQA6ggAjgOQAigNAfgGIAUANIASAOIAOAJQg+ASglAMQgnANghAQIgBAAIABgBIAOgJQATgLAXgKIAfgOQAEAAAEgDIgOAAQgUACgVADIgSAEQgaAGgZAKIgTAJQgXAKgUAQQAJgNALgLg");
	this.shape_25.setTransform(264.9,223.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#6B4332").s().p("AAmAZQgFgGgHgEIAAgEQAEgMAHgEQADgEAEAAQAKAYgEAcQgEgKgIgIgACMgFQADgMAKgNQALAFAHAJQANAMADAOQgXgKgYgFgAiugjQAIgFAJgBQAFgBAGACIgEAKIgCAFQgTgDgPAIQAFgJAHgGg");
	this.shape_26.setTransform(330.8,273.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#D0A815").s().p("AAkBZQgMgBAAAEIABAPQABAIAHAEQgMgGgOAHQgMAGgEANQgOgXgagLQgagLgZAHQAHgDAEgGQAHgFACgIQADgNgEgUQgEgXgNggIgWg3QgOglgEgeIAEAAQANAAALgIQAMAgAIAoIAYBfQAEASADAHQAFAMAIAHIACABIAHAEQAIADAIABQAHABAUABIAGABQAKACAIACQgCgEgEgDIAYABIAqgDQAOAAALACIAFADIACAOIADAAIAJADQAFABABAEIgDAGIAAABQgKANgEAMIgRgCQgUgDgPAEIgFACQgUgxgKgBg");
	this.shape_27.setTransform(332.5,259.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#B19015").s().p("AAECOIgLgHIgUgMQgigQgngBQgLgBgJABIgSgEIgFgBIABgFIAEgKIADgGIAmgHQAQgDAGgIQADgGAAgNQgDgrgZhBIgfhYQAHAFAKAAQAFAeAOAkIAWA3QAMAgAFAXQAEAVgDAMQgDAIgGAGQgEAFgHADQAZgHAaALQAZAMAOAXQAEgOANgFQANgIANAHQgHgFgBgIIgBgOQAAgEAMAAQAKABATAyIgBAAIgLgGQgQgEgKAEQgEABgDAEQgGAEgEANIgBAEQgCAGgBAGQgMgEgPgIgACMA/IADAAIABAEIgEgEg");
	this.shape_28.setTransform(330,260.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#649FCD").s().p("AgHBMQgKAAgHgEIgHgGIgDgEQgJgJgEgTIgCgJIgEgVQgHgngCgoIAVABQgGAkAFAmIABAEQABAKAEAIQADAHAGADQAOANAigLQAXgIATgLIgBACIgPAaQgIAMgKAHIgEAEIgEADQgLAHgMAAIgFAAg");
	this.shape_29.setTransform(319.3,238.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#73ACD9").s().p("Ag5AyQgGgEgDgHQgEgHgBgKIgBgFQgFglAGgkIAfADIARABIBiAEIgeA4IgMAYQgTALgWAJQgQAFgLAAQgOAAgIgHg");
	this.shape_30.setTransform(322.3,236.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#478EC6").s().p("Ag9CgQAygbAggaQArghAYglQAshFgOhZQgJgzgagnQAMAHANALQA2AzAFBHQABAdgHAdQgHAggSAeIgGAJQglA7g/AkQhDAohPAOQgZAEgZABQA1gYA0gcg");
	this.shape_31.setTransform(358.5,210.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#2D7BB8").s().p("Ag4ClQArgnAQgzQALghACg8QADhGAFgYQAIgwAcgqIAQgZIAGBZQAGCdg5BiQghA+g5AjIgXAOQASgfAIggg");
	this.shape_32.setTransform(380.3,192.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#5AA2DB").s().p("AAiDfIgVAAIgigBIhjgEIgRgBIgegDIgVgBIgwgHQg4gJhAgPIgdgHIARgHQBMARBVAJIASACIAZABIAFABIAnACQA7ABA2gHIAZgEQAbgFAbgHQAjgKAhgOQApgRAmgYIAJgFQAdgUAYgWIABgBQAYgYASgcIALgQQANgVAGgVQATg4gUgzQgQgngjgdIABgCQAWAFAVANIACADQAaAnAJAzQAOBZgsBFQgYAlgrAhQghAagyAbQg0Acg1AYIgWABIgqAAg");
	this.shape_33.setTransform(332,209.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E5BC17").s().p("ABJDjIgngDIgFAAIgZgCIgRgBQhVgKhMgRIgvgMIgHgBIgJgDIgVgGIgBAAQgEgCgEgBIgBgBIgKgDIgigLIgDgCIgRgHIgKgEQgcgLgcgOIgDgBIgTgKIgRgKIgXgOIgOgIIgUgOIgTgOIgWgPIgWgRIAegTIATgMIAMgIIA7AbQBKAiAyAWQF3CfD7hlQBOggAyg4QA5g+gChKQgDhAgxg1QASAIAQAKIAOALIAEADQAjAdAQAnQAUA0gTA4QgGAUgNAVIgLAQQgSAcgYAYIgBABQgYAXgdATIgJAGQgmAYgpARQghANgjAKQgbAHgbAGIgaAEQgvAGgzAAIgPAAg");
	this.shape_34.setTransform(312.6,206.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#984233").s().p("AAmATQACABABABQABABAAAAQAAAAAAAAQAAAAgBAAIgDgBIgCgCIAAAAIAAgCQgIgHgJgGQgSgOgVgHQgOgFgPgDIATACQAXABAVAMQANAHAJAFIAIAJIAGAPQgBAAgLgHg");
	this.shape_35.setTransform(405.2,150.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F0C61E").s().p("AomE+QgxgWhKgiIg8gcIAQgJIALgIIAhgTIAIgFIAPgIQAigUAkgTIARgKIAtgWIAPgHIAegOIAQgHIACgBIALgGQATgJATgHQARgIASgGIAYgIQARgHARgEQATgHATgEIAngLIAdgGIAsgHIAHgBQALgCAMgBIAegDQAhgDAfAAIARABQAlAAAjAFIAFABIAhAFIAZAEIAaAHIAGABQAjALAcAOQAwA1ADBAQACBKg5A/QgyA4hOAgQhgAnh0AAQi3AAjnhhgALVkeQgKgFgNgDIgegHQgegGgZgIIgKgEIgGgCIgFgCQgMgFgEgGQgFgFAAgHQAAgFACgEIACgCIADgDQAGgCAIABIATABQAMgDAFgKIACgMIAAgKQABgIAEgFQADgFAGgBQAFgBAFAEIAIAGQAkAmAXA0IAIAWIgIgJQgJgGgNgGQgWgMgXgCIgTgCQAPAEAOAFQAWAGASAPQAJAGAIAIIAAABIAAgBg");
	this.shape_36.setTransform(336.3,180.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#C2DCDE").s().p("AgSAqQgIgQgDgSQgFgZAIgVQADgIAFAAQAGAAACAHIAKAXQAEANAGAHQAIALAMAEIACABIgDACIgDACIgHAHIAAAAQgJAIgFAIIgXgFg");
	this.shape_37.setTransform(391.7,146.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#AFC7C9").s().p("AgXBKIgLgDIgDgOQgSg8AQg+QABgIAEgDQAGgDAIAGQAOAKAEAcIADAVQAEAMAFAGQAGAKAMAEIABAAQgCADAAAFQAAAHAFAGQAEAFALAGIgJAGIgCgBQgMgEgIgLQgGgIgFgNIgJgWQgCgHgGAAQgFAAgDAIQgIAUAFAaQADASAIAQIgLgEg");
	this.shape_38.setTransform(391.1,143.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#2E7EBD").s().p("AhxEgIANgRQASgVAQgYQBfiPgHihQgEgtABgWIACgYQgDgLgBgOQAAgvApgmQAagWAegEQgeAYgQAgQgQAegGAnQgDAfAAAqIADBQQADAvgDAgQgFBbgoA4QgOAWgkAqQgQAVgGAVIAAABQgVgNgVgFg");
	this.shape_39.setTransform(373.8,158.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#3489CD").s().p("AhbFdQgEhHg3gzQgMgLgMgHIgCgDIAAgBQAFgVAQgVQAkgqAQgWQAog4AFhaQACgggCgwIgDhQQgBgqAEgfQAFgnAQgeQAQggAegYQATgDAWAFQAkAJAdAZQARAOAPAUQAVAdAJASQgFgDgFAAQgGABgDAGQgEAEAAAIIAAALIgDAMQgEAKgMACIgUgBQgIAAgGACQgOgNgEgjQgCgWgEgMQgHgTgOgHQgKgEgMACQgMACgJAIQgOANgHAWIgCAFQgIAaAEAaQABANAFAKQAFANAIAKQAIAJALAGIAJAFIAKADIAMAEIAXAFIABAAIgCACIgJAOIgKASQgGAOgEAOIgDAHIgEASQgHAmACA0IAAAEIgQAYQgbArgIAwQgFAYgDBGQgCA8gLAiQgRAygrAnQAHgdgCgdg");
	this.shape_40.setTransform(384.3,168.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#A4B9BB").s().p("AgSBPQgKgGgJgIQgIgLgGgMQgEgLgCgMQgDgZAIgbIABgEQAHgXAPgMQAKgIALgCQAMgDAKAFQAOAHAGATQAFALABAXQAFAhAOANIgDADIgCACIgBAAQgMgEgGgJQgFgHgEgMIgEgVQgEgcgOgKQgHgFgGADQgEADgBAIQgQA8ASA+IADANIgJgFg");
	this.shape_41.setTransform(388.6,141.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_1
	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_42.setTransform(324.4,194.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_42).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy3_1, new cjs.Rectangle(197.2,67.3,254.4,254.4), null);


(lib.gridbox_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0.4)").ss(3,1,1).p("AFLlWIKYAAIAAKtIqYAAAlLlWIKWAAIAAKtAlLFXIKWAAAlLFXIAAqtAlLFXIqXAAIAAqtIKXAA");
	this.shape.setTransform(-41.8,-103.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AFpE5IAApxIJcAAIAAJxgAktE5IAApxIJaAAIAAJxgAvEE5IAApxIJbAAIAAJxg");
	this.shape_1.setTransform(-41.8,-103.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.gridbox_mc, new cjs.Rectangle(-142.8,-138.9,202.1,71.6), null);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol5copy();
	this.instance.parent = this;
	this.instance.setTransform(0,-2.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A17446").s().p("AAEDrQACgIgGgPQgHgPgJggIgLglQgeiHAFjFIAAgUQAAgLAGgEQAFgFANAFQAKAEAhAVQAOAEAIAGQAFAFABAHQAEALgNAdIgbBRQgNAvAEAkQAEAmAdBGQADAJAGALQAGAMABAIQAEAIAAAjIAAAPIABAGQADAFgDAEQgBAHgXACIgIABQgQAAAAgIg");
	this.shape.setTransform(227.5,-34.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_1.setTransform(195.1,-51.5,1.883,1.883,48.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_2.setTransform(196.2,-54,1.883,1.883,48.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A3193B").s().p("AhDAkQhAgdggg6QAQAVAdAIQAUAGAiAGQBtANA7gDQAggBAcgHQg4AqguALQgZAHgaAAQgmAAgogQg");
	this.shape_3.setTransform(228.8,-13);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F02A2A").s().p("AjqHiQgkgVgngdQiDhmhDieQg/igAWikIAAgBQAQiFBEhbQAQgUASgSQA2g4BHgbQAkgNAjgFQAogDAnAKQApAHByA9IAoAUQAhAAAkgLIA7gcQAigRAagGQAigIAgADQAtAFAuAYQA6AeAoA3QAJALAGAOQAgAyAMBGQARBRgFBoQgJDVhKCGQgvBThKA1QhQA4hbAFQgWADgtgDQgvgDgWADQgYACgoAIIg9AIQhJAAhPgkgAhUkxQBCAbA/gRQAvgMA3gqQgcAGgfABQg7AEhtgNQgigGgVgGQgcgJgQgUQAfA6BAAdg");
	this.shape_4.setTransform(230.4,21.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape_5.setTransform(-193,-19.9,0.576,0.576,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_6.setTransform(-203.6,-23.8,0.576,0.576,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_7.setTransform(-243.2,-8.1,0.576,0.576,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_8.setTransform(-218.6,-20,0.576,0.576,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_9.setTransform(-224.7,-31.2,0.576,0.576,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_10.setTransform(-218.5,-25.7,0.576,0.576,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_11.setTransform(-228.5,17.1,0.576,0.576,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_12.setTransform(-218.4,19,0.576,0.576,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_13.setTransform(-222,10.8,0.576,0.576,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_14.setTransform(-209.6,4.5,0.576,0.576,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_15.setTransform(-207.7,19.2,0.576,0.576,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_16.setTransform(-235.8,39,0.576,0.576,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_17.setTransform(-214.3,42.7,0.576,0.576,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_18.setTransform(-183.1,9.7,0.576,0.576,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_19.setTransform(-185.5,-3.5,0.576,0.576,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_20.setTransform(-202.4,-9.5,0.576,0.576,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_21.setTransform(-199.1,0,0.576,0.576,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_22.setTransform(-193.1,23.1,0.576,0.576,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_23.setTransform(-196,34.3,0.576,0.576,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_24.setTransform(-224.9,38.5,0.576,0.576,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_25.setTransform(-244,28.3,0.576,0.576,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_26.setTransform(-246.2,6.5,0.576,0.576,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_27.setTransform(-192.5,63.2,0.576,0.576,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_28.setTransform(-212.6,71.3,0.576,0.576,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_29.setTransform(-254.2,65.6,0.576,0.576,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_30.setTransform(-240.6,70.1,0.576,0.576,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_31.setTransform(-224.4,72.4,0.576,0.576,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_32.setTransform(-238.3,65.3,0.576,0.576,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_33.setTransform(-259.2,58.5,0.576,0.576,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_34.setTransform(-271.4,50.1,0.576,0.576,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_35.setTransform(-279.9,35.1,0.576,0.576,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_36.setTransform(-277.3,43.5,0.576,0.576,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_37.setTransform(-281.9,23.7,0.576,0.576,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_38.setTransform(-271.7,43.1,0.576,0.576,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_39.setTransform(-278.8,29.7,0.576,0.576,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_40.setTransform(-281.6,18.4,0.576,0.576,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_41.setTransform(-223.2,52.2,0.576,0.576,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_42.setTransform(-226.4,60.2,0.576,0.576,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_43.setTransform(-235.3,54.1,0.576,0.576,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_44.setTransform(-211.3,59.8,0.576,0.576,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_45.setTransform(-200.3,58.8,0.576,0.576,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_46.setTransform(-197.9,48.5,0.576,0.576,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_47.setTransform(-189.8,50.8,0.576,0.576,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_48.setTransform(-188.1,42.3,0.576,0.576,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_49.setTransform(-177.6,39.9,0.576,0.576,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_50.setTransform(-173.1,30,0.576,0.576,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_51.setTransform(-248.1,48.5,0.576,0.576,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_52.setTransform(-257.4,45.3,0.576,0.576,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_53.setTransform(-264.3,39.4,0.576,0.576,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_54.setTransform(-261.1,29.9,0.576,0.576,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_55.setTransform(-270.5,19.7,0.576,0.576,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_56.setTransform(-260.6,17.6,0.576,0.576,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_57.setTransform(-263.7,0,0.576,0.576,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_58.setTransform(-273.1,-2.1,0.576,0.576,0,0,180);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_59.setTransform(-257.2,-3.9,0.576,0.576,0,0,180);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_60.setTransform(-267.7,-11.6,0.576,0.576,0,0,180);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_61.setTransform(-253.5,-13.8,0.576,0.576,0,0,180);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_62.setTransform(-259,-19.8,0.576,0.576,0,0,180);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#5D2408").s().p("AgLAqIgYgFQgGgDgNgEQgLgBgGgEQgLgFgEgIQgGgJAAgHQAAgLAGgHQAHgGAVgFQAXgFAagEIADAKIADAIIAAAIIADAQIAAAFQAFAAAGgDQAGgCACgGQACgEAEgFQADgBAGABQABAAAAAAQAAAAABAAQAAAAAAAAQAAgBAAAAQAEgEgMgZQARgCAQACQASAAAFAKQAIALAAAEQADAEgDAHQgCAFgBAJQgCAJgEAHQgKAMgVAIQgXAGgPAAQgLAAgOgFg");
	this.shape_63.setTransform(-238.2,-19.6);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#A33D0C").s().p("AgRBIIgogNIgggHQgTgFgLgGQgQgIgIgMQgMgOAAgPQAAgRANgNQAJgIAUgFQAvgPAygGIAIAdQgaAEgXAGQgVAFgHAFQgHAIAAALQAAAGAGAKQAFAHAKAFQAGAEALABQAOAEAGADIAXAGQAOAEAMAAQAPAAAXgGQAUgHALgMQADgIACgJQABgIADgGQACgHgCgEQAAgEgIgLQgFgKgSAAQgQgCgRACQAAgEgDgEIgIgWQAqgDArADQAeAAAJARQAMAQACAHIAAATQgFAKgCAPQgDAQgJAMQgPARgjANQglANgaAAQgSgBgXgGg");
	this.shape_64.setTransform(-238.2,-19.5);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#EC5913").s().p("Aj8H2QhXgphJhGQhshmgoiBQgHgYgFgXQgKgxAAg0IAAgMQAAi4BviNQAbghAgggQA1gxA3ghQAjgUAlgOQAngSAugMIAMAuQgyAGgvAPQgUAFgJAIQgOANAAARQAAAQANAOQAIAMAQAIQAKAGAUAFIAfAHIApANQAYAGASABQAZAAAmgNQAigNAPgRQAIgMADgQQACgQAFgKIAAgTQgBgHgMgQQgKgRgdAAQgqgDgqADIgRgvQA5gMA8AAQBZAABSAXQCFAnBuBlIAaAcQCPCaAADQQAAB2guBlQgpBehSBOQiuCjjwAAQiIAAh0g0g");
	this.shape_65.setTransform(-226.3,22.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#692B0E").s().p("AB5DRQgLgegoh2Qgqhxg9g/QhAg8gQggQgIgXgggcIAMgZQBLAsA4BiQAFAKAHALQAdA8AnBoQAFARAOAhIAcBTIARAvIAIAWQADAEAAAEQAMAagEADQgRghgPgpg");
	this.shape_66.setTransform(-249.8,-49.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#4A1D08").s().p("AB+EUIgEgQIAAgIIgDgIIgDgKIgIgdIgMguQgQgtgNgaIgGgNIgWguQgPgYgMgYQg+hhhUhOQgNgLgEgGQgJgLgBgJQgDgRANgVIAIgMQAfAcAJAWQAQAhA/A8QA9A/ArBwQAnB3AMAeQAOApARAhQAAAAAAABQAAAAAAAAQAAAAgBAAQAAAAgBAAQgGgBgDABQgEAFgCAEQgCAGgGADQgGADgFAAIAAgGg");
	this.shape_67.setTransform(-250.8,-47.3);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("rgba(0,0,0,0.4)").ss(3,1,1).p("ARmxtMAjQAAAMAAAAjbMgjQAAAARmxtMAAAAjbAxmRuMgjPAAAMAAAgjbMAjPAAAMAjMAAAAxmRuMAAAgjbAxmRuMAjMAAA");

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("ATMQLMAAAggVMAgEAAAMAAAAgVgAwAQLMAAAggVIf/AAMAAAAgVgEgzPAQLMAAAggVMAgDAAAMAAAAgVg");

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("rgba(255,255,255,0.2)").s().p("ArKLYQktktAAmrQAAmqEtktQEukuGqAAQGqAAEuEuQEuEtAAGqQAAGrkuEtQkuEvmqAAQmqAAkukvgAX2LYQktkuAAmqQAAmqEtkuQEuktGrAAQGqAAEuEtQEuEuAAGqQAAGqkuEuQkuEumqAAQmrAAkukugEgunALYQktkuAAmqQAAmqEtkuQEuktGqAAQGrAAEuEtQEuEuAAGqQAAGqkuEuQkuEumrAAQmqAAkukug");
	this.shape_70.setTransform(-0.8,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-339.6,-114.8,679.3,229.8);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-358.1,-33.3,716.3,66.6);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy3_1();
	this.instance.parent = this;
	this.instance.setTransform(-158.2,15.8,1,1,0,0,0,330.2,210.2);

	this.instance_1 = new lib.Symbol1copy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(169.9,15.8,1,1,0,0,0,330.2,210.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-291.2,-127.1,582.5,254.4);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy2();
	this.instance.parent = this;
	this.instance.setTransform(6,12.8,0.81,0.81,0,0,0,330.3,210.2);

	this.instance_1 = new lib.Symbol1copy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(230.1,12.7,0.81,0.81,0,0,0,330.3,210.2);

	this.instance_2 = new lib.Symbol1copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-220.9,12.7,0.81,0.81,0,0,0,330.3,210.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

	// Layer_2
	this.questiongrid = new lib.gridbox_mc();
	this.questiongrid.name = "questiongrid";
	this.questiongrid.parent = this;
	this.questiongrid.setTransform(1.4,0.7,3.398,3.303,0,0,0,-41.6,-103);

	this.timeline.addTween(cjs.Tween.get(this.questiongrid).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-342.6,-118.1,686.5,236.7);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.2,scaleY:1.2},9).to({scaleX:1,scaleY:1},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.8,-60.9,63.6,121.9);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.Symbol1copy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(636.8,317.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(311).to({_off:false},0).wait(27).to({startPosition:12},0).to({alpha:0,startPosition:3},11).wait(6));

	// Layer_10
	this.instance_1 = new lib.Tween12("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(638.1,310.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(294).to({_off:false},0).to({scaleX:1.21,scaleY:1.21},24).to({startPosition:0},15).to({alpha:0},11).wait(11));

	// Layer_6
	this.instance_2 = new lib.Symbol1copy8("synched",30);
	this.instance_2.parent = this;
	this.instance_2.setTransform(928.4,647.7,1,1,-20.7,0,0,190.5,64.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(230).to({_off:false},0).to({_off:true},34).wait(91));

	// Layer_8
	this.instance_3 = new lib.Symbol1copy7("synched",30);
	this.instance_3.parent = this;
	this.instance_3.setTransform(647.9,500.7,1,1,-52.9,0,0,41,60.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(197).to({_off:false},0).to({_off:true},35).wait(123));

	// Layer_7
	this.instance_4 = new lib.Tween11("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(804.3,576.2);
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween12("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(638.1,310.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},266).to({state:[{t:this.instance_5}]},26).to({state:[]},3).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(266).to({_off:false},0).to({_off:true,x:638.1,y:310.1},26).wait(63));

	// Layer_9
	this.instance_6 = new lib.Symbol4();
	this.instance_6.parent = this;
	this.instance_6.setTransform(639.4,307.5);
	this.instance_6.alpha = 0.02;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(126).to({_off:false},0).to({alpha:0.75},25).wait(34).to({alpha:0},5).to({_off:true},1).wait(164));

	// Layer_5
	this.instance_7 = new lib.Tween14("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(709.4,120.4,0.98,0.98);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(80).to({_off:false},0).wait(105).to({startPosition:25},0).to({alpha:0,startPosition:30},5).to({_off:true},1).wait(164));

	// Layer_4
	this.instance_8 = new lib.Tween5("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,574.4);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(64).to({_off:false},0).to({alpha:1},5).wait(248).to({startPosition:0},0).to({alpha:0},7).wait(31));

	// Layer_11
	this.instance_9 = new lib.Tween1("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(646,312.6);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.instance_10 = new lib.Symbol1("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(646,312.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9}]},40).to({state:[{t:this.instance_9}]},7).to({state:[{t:this.instance_9}]},32).to({state:[{t:this.instance_10}]},1).to({state:[]},46).to({state:[{t:this.instance_9}]},59).to({state:[{t:this.instance_9}]},103).to({state:[]},1).wait(66));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(40).to({_off:false},0).to({alpha:1},7).to({startPosition:0},32).to({_off:true},1).wait(105).to({_off:false},0).to({startPosition:0},103).to({_off:true},1).wait(66));

	// Layer_3
	this.instance_11 = new lib.Tween3("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,308.2);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A17446").s().p("AAEDrQACgIgGgPQgHgPgJggIgLglQgeiHAFjFIAAgUQAAgLAGgEQAFgFANAFQAKAEAhAVQAOAEAIAGQAFAFABAHQAEALgNAdIgbBRQgNAvAEAkQAEAmAdBGQADAJAGALQAGAMABAIQAEAIAAAjIAAAPIABAGQADAFgDAEQgBAHgXACIgIABQgQAAAAgIg");
	this.shape.setTransform(868.1,274.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_1.setTransform(835.7,256.9,1.883,1.883,48.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_2.setTransform(836.8,254.4,1.883,1.883,48.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A3193B").s().p("AhDAkQhAgdggg6QAQAVAdAIQAUAGAiAGQBtANA7gDQAggBAcgHQg4AqguALQgZAHgaAAQgmAAgogQg");
	this.shape_3.setTransform(869.4,295.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F02A2A").s().p("AjqHiQgkgVgngdQiDhmhDieQg/igAWikIAAgBQAQiFBEhbQAQgUASgSQA2g4BHgbQAkgNAjgFQAogDAnAKQApAHByA9IAoAUQAhAAAkgLIA7gcQAigRAagGQAigIAgADQAtAFAuAYQA6AeAoA3QAJALAGAOQAgAyAMBGQARBRgFBoQgJDVhKCGQgvBThKA1QhQA4hbAFQgWADgtgDQgvgDgWADQgYACgoAIIg9AIQhJAAhPgkgAhUkxQBCAbA/gRQAvgMA3gqQgcAGgfABQg7AEhtgNQgigGgVgGQgcgJgQgUQAfA6BAAdg");
	this.shape_4.setTransform(871,329.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape_5.setTransform(447.6,288.5,0.576,0.576,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_6.setTransform(437,284.6,0.576,0.576,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_7.setTransform(397.4,300.3,0.576,0.576,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_8.setTransform(422,288.4,0.576,0.576,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_9.setTransform(415.9,277.2,0.576,0.576,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_10.setTransform(422.1,282.7,0.576,0.576,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_11.setTransform(412.1,325.5,0.576,0.576,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_12.setTransform(422.2,327.4,0.576,0.576,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_13.setTransform(418.6,319.2,0.576,0.576,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_14.setTransform(431,312.9,0.576,0.576,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_15.setTransform(432.9,327.6,0.576,0.576,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_16.setTransform(404.8,347.4,0.576,0.576,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_17.setTransform(426.3,351.1,0.576,0.576,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_18.setTransform(457.5,318.1,0.576,0.576,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_19.setTransform(455.1,304.9,0.576,0.576,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_20.setTransform(438.2,298.9,0.576,0.576,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_21.setTransform(441.5,308.4,0.576,0.576,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_22.setTransform(447.5,331.5,0.576,0.576,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_23.setTransform(444.6,342.7,0.576,0.576,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_24.setTransform(415.7,346.9,0.576,0.576,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_25.setTransform(396.6,336.7,0.576,0.576,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_26.setTransform(394.4,314.9,0.576,0.576,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_27.setTransform(448.1,371.6,0.576,0.576,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_28.setTransform(428,379.7,0.576,0.576,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_29.setTransform(386.4,374,0.576,0.576,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_30.setTransform(400,378.5,0.576,0.576,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_31.setTransform(416.2,380.8,0.576,0.576,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_32.setTransform(402.3,373.7,0.576,0.576,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_33.setTransform(381.4,366.9,0.576,0.576,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_34.setTransform(369.2,358.5,0.576,0.576,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_35.setTransform(360.7,343.5,0.576,0.576,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_36.setTransform(363.3,351.9,0.576,0.576,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_37.setTransform(358.7,332.1,0.576,0.576,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_38.setTransform(368.9,351.5,0.576,0.576,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_39.setTransform(361.8,338.1,0.576,0.576,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_40.setTransform(359,326.8,0.576,0.576,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_41.setTransform(417.4,360.6,0.576,0.576,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_42.setTransform(414.2,368.6,0.576,0.576,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_43.setTransform(405.3,362.5,0.576,0.576,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_44.setTransform(429.3,368.2,0.576,0.576,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_45.setTransform(440.3,367.2,0.576,0.576,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_46.setTransform(442.7,356.9,0.576,0.576,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_47.setTransform(450.8,359.2,0.576,0.576,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_48.setTransform(452.5,350.7,0.576,0.576,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_49.setTransform(463,348.3,0.576,0.576,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_50.setTransform(467.5,338.4,0.576,0.576,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_51.setTransform(392.5,356.9,0.576,0.576,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_52.setTransform(383.2,353.7,0.576,0.576,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_53.setTransform(376.3,347.8,0.576,0.576,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_54.setTransform(379.5,338.3,0.576,0.576,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_55.setTransform(370.1,328.1,0.576,0.576,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_56.setTransform(380,326,0.576,0.576,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_57.setTransform(376.9,308.4,0.576,0.576,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_58.setTransform(367.5,306.3,0.576,0.576,0,0,180);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_59.setTransform(383.4,304.5,0.576,0.576,0,0,180);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_60.setTransform(372.9,296.8,0.576,0.576,0,0,180);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_61.setTransform(387.1,294.6,0.576,0.576,0,0,180);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_62.setTransform(381.6,288.6,0.576,0.576,0,0,180);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#EC5913").s().p("Aj7H2QhYgphJhGQhthmgniBQgHgYgFgXQgKgxAAg0IAAgMQAAi4BviNQAcghAfggQA0gxA4ghQAkgUAkgOQAngSAtgMIANAuQgyAGgvAPQgUAFgJAIQgOANAAARQABAQAMAOQAIAMAQAIQALAGATAFIAfAHIApANQAXAGATABQAaAAAlgNQAigNAPgRQAJgMACgQQADgQAFgKIAAgTQgCgHgMgQQgKgRgdAAQgqgDgqADIgRgvQA5gMA8AAQBZAABTAXQCFAnBtBlIAaAcQCPCaAADQQAAB2gvBlQgpBehRBOQiuCjjwAAQiJAAhyg0g");
	this.shape_63.setTransform(414.3,330.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#4A1D08").s().p("AB+ETIgEgPIAAgIIgDgHIgDgLIgIgdIgMguQgQgugNgaIgGgMIgWgtQgPgYgMgZQg+hhhUhOQgNgLgEgGQgJgLgBgJQgDgRANgVIAIgMQAfAcAJAWQAQAhA/A8QA9A+ArByQAnB1AMAfQAOApARAhQAAAAAAABQAAAAAAAAQAAAAgBAAQAAABgBAAQgGgCgDACQgEADgCAFQgCAGgGADQgGADgFAAIAAgHg");
	this.shape_64.setTransform(389.8,261.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#5D2408").s().p("AgLAqIgYgGQgGgDgNgDQgLgBgGgEQgLgFgEgHQgGgKAAgGQAAgLAGgIQAHgFAVgFQAXgHAagDIADALIADAHIAAAIIADAPIAAAGQAFAAAGgDQAGgCACgGQACgFAEgDQADgCAGACQABAAAAgBQAAAAABAAQAAAAAAAAQAAgBAAAAQAEgEgMgZQARgCAQACQASAAAFAKQAIAKAAAFQADAEgDAGQgCAHgBAIQgCAIgEAJQgKAMgVAHQgXAGgPAAQgLAAgOgFg");
	this.shape_65.setTransform(402.4,288.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#A33D0C").s().p("AgRBIIgogNIgggHQgTgFgLgGQgQgIgIgMQgMgOAAgPQAAgRANgNQAJgIAUgFQAvgPAygGIAIAdQgaAEgXAGQgVAFgHAFQgHAIAAALQAAAGAGAKQAFAHAKAFQAGAEALABQAOAEAGADIAXAGQAOAEAMAAQAPAAAXgGQAUgHALgMQADgIACgJQABgIADgGQACgHgCgEQAAgEgIgLQgFgKgSAAQgQgCgRACQAAgEgDgEIgIgWQAqgDArADQAeAAAJARQAMAQACAHIAAATQgFAKgCAPQgDAQgJAMQgPARgjANQglANgaAAQgSgBgXgGg");
	this.shape_66.setTransform(402.4,288.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#692B0E").s().p("AB5DSQgLgfgoh2Qgqhxg9g+QhAg9gQghQgIgVgggcIAMgaQBLAsA4BiQAFAKAHALQAdA8AnBnQAFASAOAhIAcBTIARAvIAIAWQADAEAAAFQAMAZgEAEQgRgigPgog");
	this.shape_67.setTransform(390.8,258.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("rgba(0,0,0,0.4)").ss(3,1,1).p("ARlxtMAjRAAAMAAAAjbMgjRAAAARlxtMAAAAjbAxmRuMgjPAAAMAAAgjbMAjPAAAMAjLAAAAxmRuMAAAgjbAxmRuMAjLAAA");
	this.shape_68.setTransform(640.6,308.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("ATMQLMAAAggVMAgEAAAMAAAAgVgAwBQLMAAAggVMAgBAAAMAAAAgVgEgzOAQLMAAAggVMAgCAAAMAAAAgVg");
	this.shape_69.setTransform(640.6,308.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("rgba(255,255,255,0.2)").s().p("ArKLYQktktAAmrQAAmqEtktQEukuGqAAQGqAAEuEuQEuEtAAGqQAAGrkuEtQkuEvmqAAQmqAAkukvgAX2LYQktkuAAmqQAAmqEtkuQEuktGrAAQGqAAEuEtQEuEuAAGqQAAGqkuEuQkuEumqAAQmrAAkukugEgunALYQktkuAAmqQAAmqEtkuQEuktGqAAQGrAAEuEtQEuEuAAGqQAAGqkuEuQkuEumrAAQmqAAkukug");
	this.shape_70.setTransform(639.8,308.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("rgba(0,0,0,0.4)").ss(3,1,1).p("ARlxtMAjRAAAMAAAAjbMgjRAAAARlxtMAAAAjbAxmRuMAAAgjbMAjLAAAAxmRuMgjPAAAMAAAgjbMAjPAAAAxmRuMAjLAAA");
	this.shape_71.setTransform(640.6,308.4);

	this.instance_12 = new lib.Tween17("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640.6,308.4);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11}]},40).to({state:[{t:this.instance_11}]},7).to({state:[{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},79).to({state:[{t:this.shape_70},{t:this.shape_69},{t:this.shape_71},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},65).to({state:[{t:this.instance_12}]},142).to({state:[{t:this.instance_12}]},7).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(40).to({_off:false},0).to({alpha:1},7).to({_off:true},79).wait(229));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(333).to({_off:false},0).to({alpha:0},7).wait(15));

	// Layer_2
	this.instance_13 = new lib.qtext();
	this.instance_13.parent = this;
	this.instance_13.setTransform(636.4,116.7,1.889,1.889);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(15).to({_off:false},0).to({alpha:1},7).wait(311).to({alpha:0},7).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(556.8,303.8,1450,835);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;