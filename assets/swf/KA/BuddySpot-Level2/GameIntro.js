(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AiEAIQgEgGAAgHQgBg1BDAJQCCgNBIBZQAJAMgHANQh7hIiPAcg");
	this.shape.setTransform(35.9,-32.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AiFAbQBKhYCBALQBEgIgBA2QAAAGgEAGQiOgeh8BKQgGgNAGgMg");
	this.shape_1.setTransform(-33.9,-33.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AglAnQgygpAQgzQAaBTBPgZQAUgFAEgXQADgRALgMQATBIhHAcQgNAGgKAAQgSAAgQgPg");
	this.shape_2.setTransform(-1.5,4.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AhABAQgYgZAAgmQAAgOADgNIAJANQAPANASAAQAUAAAKgNQAOgNAAgRQAAgUgOgOIgJgIQALgEAMAAQAmAAAaAaQAYAaAAAmQAAAmgYAZQgaAagmAAQglAAgcgag");
	this.shape_3.setTransform(36.5,-16.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AhSBSQgigkAAguQAAgvAigkQAkghAuAAQAxAAAgAhQAkAkAAAvQAAAugkAkQggAjgxAAQguAAgkgjgAheAWQAAAnAYAYQAcAaAmAAQAlAAAagaQAYgYAAgnQAAglgYgaQgagaglAAQgNAAgLAEIAJAIQAPAOAAATQAAASgPAMQgKANgUAAQgSAAgPgNIgJgNQgDANAAAOg");
	this.shape_4.setTransform(37.1,-18.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("Ag/BAQgZgZAAgmQAAgOAEgNQABAHAHAGQAPANATAAQAQAAANgNQANgNAAgRQAAgUgNgOIgLgIQANgEALAAQAmAAAYAaQAbAaAAAmQAAAmgbAZQgYAagmAAQglAAgagag");
	this.shape_5.setTransform(-37.5,-16.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AhSBSQgjgkAAguQAAgvAjgkQAjghAxAAQAvAAAhAhQAjAkAAAvQAAAugjAkQghAjgvAAQgxAAgjgjgAhfAWQAAAnAZAYQAbAaAmAAQAlAAAXgaQAcgYAAgnQAAglgcgaQgXgaglAAQgMAAgNAEIALAIQAOAOAAATQAAASgOAMQgNANgQAAQgTAAgPgNQgHgGgBgHQgEANgBAOg");
	this.shape_6.setTransform(-36.8,-18.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AisAMQgxgYgOggQD1AnDigqQgOAgg0AbQhIAkhhAAQhoAAhFgkg");
	this.shape_7.setTransform(-0.6,20.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AjfAKQg6gYgVgnIBAgDQAPAgAwAYQBGAjBpAAQBgAABIgjQAzgcAPgfQAiABAjAAQgWAng7AdQhdAziBgBQiCABhdgzg");
	this.shape_8.setTransform(-0.3,21.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("AskjlQgMhLAAhQQAAixA+iWQglA+gxBWQADhWAdhPQAUhAAig6QBQiKCChIQA3ggA9gSQBCgrBHgZQhLgNhLARQBlgdBoAFQBMADBPAXQAGgfAbgcQAXgbAsgTQAQgJASgFQBmgjBYgQQhMAQhEAxQgcATgaAaQD9AMDFCgQBHApBLA9QBqBXA3CPQAyCCALCxQgLgagJgYQgMgmgYgbQAMBIAABQQAAAkgFAlQgBAkgGAjQARASAIAjQAGAOACAOAp+CDQgZiMgLhPQgQh/ADhqQABhFANg/QA/gOBNgbQBcgbAygWQAkgSAggUQgNAVgWAYQhIBZhpBDQBpgfBag8QA+gnBNh8QAZA/AQAgQAnBNAuA1QATAVAWAVQgnhPgVhQQBUBJCpAcQAaAEAdAFQgdgigagrQgdgygVgmQAoAYA7AJQBAAMCCATQBdAZBKBGQALALAMAKQA5A/AeBNQAhBigKCXQgDAigDAtQBViPAaikANNiEQAGBBgcA3QgpBIhbApQgNAFgOAFQgmA2gvAyQiwC2jlAwQhTARhZAAQhrAAhhgYQjTg2ikipQgegigdgkQAAgGgCgIADMK8IAKAKACuJhICxhcIAAC6IAACaIiJiTACuJhIgmAVIBEBGIgMAMIiFCUIAAACIAAATIBnHOAA7J2IBNAAAA6NeIABAAAA7NcIgBACAA7J2IAADmADALIQAMgCAKAAAFfK/QB3gKB0gMQDKgPAADJIAAHcIpyAAACuG9IAACkAskjlQgMAGgLAIQgWAQgGAZQgOAmAOA3QAVBRBJA8QAyApA8AaIACABIAAAAQAEADAHAAAqJCAQgCgCAAAAQh6iggfjDAjKJnIAdAPIBIAAIAADlIAEADICbAAAjKJnIi8hiIAADBIAACTICFiNQANAAAPAAICACPIAAADIAEAAAj0K8IgNAQAj0K8IAPAQAhlNeIAAATIhlHOIpyAAIAAncQAAjJDNAbQB2ALBzAGAitJ2IhHBGAjKJnIAAixAjKU/IFsAAAhlJ2ICgAA");

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AhLB0IgFgDIAAjkICgAAIAADmIgBABg");
	this.shape_10.setTransform(-2.1,74.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("Ai1DxIBlnNIAAgUIAEAAICbAAIABAAIAAAUIBmHNg");
	this.shape_11.setTransform(-2,110.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("AC2FMIhmnNIAAgUIAAgBICFiUQALgDALAAICICTIAAiZQB4gLBzgMQDKgOAADJIAAHbgAsnFMIAAnbQAAjJDMAaQB2AMB0AFIAACTICEiMIAcAAICBCPIAAACIAAAUIhlHNg");
	this.shape_12.setTransform(-2,101.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("ABQhyIBMAAIBEBGIgLAMIiFCTgAjRgcIgOgQIBGhGIBJAAIAADkg");
	this.shape_13.setTransform(-2,74.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#129293").s().p("ADqAXIgKgJIhEhGIAmgVICxhcIAAC5IAACagAlyAXIAAjAIC8BiIAdAPIhHBGIgNAQIiFCMg");
	this.shape_14.setTransform(-2,68.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("ABEJ4IigAAIhJAAIgcgPIAAixQjUg1ikiqQgegigdgkIgBgOQgZiLgMhPQgQiAADhqQABhEANg/QA/gOBOgcQBcgaAygWQAjgTAhgUQgNAWgXAXQhIBahpBDQBpgfBbg8QA9goBOh7QAZA/AQAfQAnBOAuA0QASAWAXAUQgohOgVhQQBUBJCqAcIA3AJQgdgigagsIgyhYQAnAYA7AJQBBANCBATQBeAZBJBFQAMAMAMAJQA4BAAeBNQAhBigJCXIgHBOQBWiOAaikQARASAIAiQAFAPADAOIAFAaQAFBCgbA3QgpBHhcApIgaAKQgmA3gvAxQiwC2jmAwQhTARhZAAQhqAAhhgYQBhAYBqAAQBZAABTgRIAACkIgmAVgAqCCBQg9gagxgoQhKg9gVhQQgNg4ANglQAHgaAWgQQAKgHAMgHQAgDEB6CfIABACg");
	this.shape_15.setTransform(-0.8,-0.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AqkH2IgBAAIgBgCQgGg8gUhJQgnh4gRg9QgdhmAEhUQAEhnA1hEQAcgjAyggIBdg1QBAgiAcgUQAIgDAIgGQBZhBARhOQAUBHgDBIQgBAtgMAuQAegrAVgwQAghGAJhQQAWBMAQBKQATBXALBVQgBhxgDg7IAAgGQAEAEAAACQBvCWCcBmQgPhCgig+Qgig7gwgvII6CMQBGARAkAQQA4AXAjAkQAcAgAPA0QALAdAJA2QgBAkgGAjQgaCkhVCPIAGhPQAKiXghhiQgehNg5g/QgMgJgLgMQhKhFhdgZQiCgThAgNQg7gJgogYIAyBYQAaAsAdAiIg3gJQipgchUhJQAVBQAnBNQgWgTgTgWQgug0gnhOQgQgfgZg/QhNB7g+AoQhaA8hpAfQBphDBIhaQAWgXANgWQggAUgkATQgyAWhcAaQhNAcg/AOQgNA/gBBDQgDBqAQCAQALBPAZCMQgHAAgEgDg");
	this.shape_16.setTransform(2.7,-37.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AsvF7QgMhLAAhQQAAixA+iVQglA+gxBVQADhVAdhPQAUhAAig6QBQiKCChIQA3ggA9gSQBCgrBHgZQhLgNhLARQBlgdBoAFQBMADBPAXQAGgfAbgcQAXgbAsgTQAQgJASgFQBmgjBYgQQhMAQhEAxQgcATgaAaQD9AMDFCgQBHApBLA9QBqBXA3CPQAyCBALCxIgUgyQgMgmgYgbQAMBIAABQQAAAkgFAlQgJg2gLgeQgPg0gcggQgjgjg4gYQgkgQhGgRIo6iLQAwAvAiA6QAiA+APBCQichlhviVQAAgDgEgEIAAAHQADA7ABBwQgLhUgThXQgQhLgWhMQgJBQggBHQgVAvgeArQAMgtABgtQADhIgUhHQgRBNhZBCQgIAFgIADQgcAVhAAhIhdA1QgyAfgcAjQg1BFgEBnQgEBUAdBnQARA8AnB5QAUBIAGA8Qh6iggfjDg");
	this.shape_17.setTransform(1.1,-60.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-87.3,-135.3,174.6,270.6);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AiEAIQgEgGAAgHQgBg1BDAJQCCgNBIBZQAJAMgHANQh7hIiPAcg");
	this.shape.setTransform(35.9,-32.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AiFAbQBKhYCBALQBEgIgBA2QAAAGgEAGQiOgeh8BKQgGgNAGgMg");
	this.shape_1.setTransform(-33.9,-33.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AglAnQgygpAQgzQAaBTBPgZQAUgFAEgXQADgRALgMQATBIhHAcQgNAGgKAAQgSAAgQgPg");
	this.shape_2.setTransform(-1.5,4.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AhABAQgYgZAAgmQAAgOADgNIAJANQAPANASAAQAUAAAKgNQAOgNAAgRQAAgUgOgOIgJgIQALgEAMAAQAmAAAaAaQAYAaAAAmQAAAmgYAZQgaAagmAAQglAAgcgag");
	this.shape_3.setTransform(36.5,-16.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AhSBSQgigkAAguQAAgvAigkQAkghAuAAQAxAAAgAhQAkAkAAAvQAAAugkAkQggAjgxAAQguAAgkgjgAheAWQAAAnAYAYQAcAaAmAAQAlAAAagaQAYgYAAgnQAAglgYgaQgagaglAAQgNAAgLAEIAJAIQAPAOAAATQAAASgPAMQgKANgUAAQgSAAgPgNIgJgNQgDANAAAOg");
	this.shape_4.setTransform(37.1,-18.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("Ag/BAQgZgZAAgmQAAgOAEgNQABAHAHAGQAPANATAAQAQAAANgNQANgNAAgRQAAgUgNgOIgLgIQANgEALAAQAmAAAYAaQAbAaAAAmQAAAmgbAZQgYAagmAAQglAAgagag");
	this.shape_5.setTransform(-37.5,-16.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AhSBSQgjgkAAguQAAgvAjgkQAjghAxAAQAvAAAhAhQAjAkAAAvQAAAugjAkQghAjgvAAQgxAAgjgjgAhfAWQAAAnAZAYQAbAaAmAAQAlAAAXgaQAcgYAAgnQAAglgcgaQgXgaglAAQgMAAgNAEIALAIQAOAOAAATQAAASgOAMQgNANgQAAQgTAAgPgNQgHgGgBgHQgEANgBAOg");
	this.shape_6.setTransform(-36.8,-18.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AisAMQgxgYgOggQD1AnDigqQgOAgg0AbQhIAkhhAAQhoAAhFgkg");
	this.shape_7.setTransform(-0.6,20.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AjfAKQg6gYgVgnIBAgDQAPAgAwAYQBGAjBpAAQBgAABIgjQAzgcAPgfQAiABAjAAQgWAng7AdQhdAziBgBQiCABhdgzg");
	this.shape_8.setTransform(-0.3,21.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("AskjlQgMhLAAhQQAAixA+iWQglA+gxBWQADhWAdhPQAUhAAig6QBQiKCChIQA3ggA9gSQBCgrBHgZQhLgNhLARQBlgdBoAFQBMADBPAXQAGgfAbgcQAXgbAsgTQAQgJASgFQBmgjBYgQQhMAQhEAxQgcATgaAaQD9AMDFCgQBHApBLA9QBqBXA3CPQAyCCALCxQgLgagJgYQgMgmgYgbQAMBIAABQQAAAkgFAlQgBAkgGAjQARASAIAjQAGAOACAOAp+CDQgZiMgLhPQgQh/ADhqQABhFANg/QA/gOBNgbQBcgbAygWQAkgSAggUQgNAVgWAYQhIBZhpBDQBpgfBag8QA+gnBNh8QAZA/AQAgQAnBNAuA1QATAVAWAVQgnhPgVhQQBUBJCpAcQAaAEAdAFQgdgigagrQgdgygVgmQAoAYA7AJQBAAMCCATQBdAZBKBGQALALAMAKQA5A/AeBNQAhBigKCXQgDAigDAtQBViPAaikANNiEQAGBBgcA3QgpBIhbApQgNAFgOAFQgmA2gvAyQiwC2jlAwQhTARhZAAQhrAAhhgYQjTg2ikipQgegigdgkQAAgGgCgIADMK8IAKAKACuJhICxhcIAAC6IAACaIiJiTACuJhIgmAVIBEBGIgMAMIiFCUIAAACIAAATIBnHOAA7J2IBNAAAA6NeIABAAAA7NcIgBACAA7J2IAADmADALIQAMgCAKAAAFfK/QB3gKB0gMQDKgPAADJIAAHcIpyAAACuG9IAACkAskjlQgMAGgLAIQgWAQgGAZQgOAmAOA3QAVBRBJA8QAyApA8AaIACABIAAAAQAEADAHAAAqJCAQgCgCAAAAQh6iggfjDAjKJnIAdAPIBIAAIAADlIAEADICbAAAjKJnIi8hiIAADBIAACTICFiNAj0K8IAPAQICACPIAAADIAEAAAj0K8IgNAQQANAAAPAAAhlNeIAAATIhlHOIpyAAIAAncQAAjJDNAbQB2ALBzAGAitJ2IhHBGAjKJnIAAixAjKU/IFsAAAhlJ2ICgAA");

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AhLB0IgFgDIAAjkICgAAIAADmIgBABg");
	this.shape_10.setTransform(-2.1,74.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("Ai1DxIBlnNIAAgUIAEAAICbAAIABAAIAAAUIBmHNg");
	this.shape_11.setTransform(-2,110.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("AC2FMIhmnNIAAgUIAAgBICFiUQALgDALAAICICTIAAiZQB4gLBzgMQDKgOAADJIAAHbgAsnFMIAAnbQAAjJDMAaQB2AMB0AFIAACTICEiMIAcAAICBCPIAAACIAAAUIhlHNg");
	this.shape_12.setTransform(-2,101.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("ABQhyIBMAAIBEBGIgLAMIiFCTgAjRgcIgOgQIBGhGIBJAAIAADkg");
	this.shape_13.setTransform(-2,74.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#129293").s().p("ADqAXIgKgJIhEhGIAmgVICxhcIAAC5IAACagAlyAXIAAjAIC8BiIAdAPIhHBGIgNAQIiFCMg");
	this.shape_14.setTransform(-2,68.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("ABEJ4IigAAIhJAAIgcgPIAAixQBhAYBqAAQBZAABTgRQhTARhZAAQhqAAhhgYQjUg1ikiqQgegigdgkIgBgOQgZiLgMhPQgQiAADhqQABhEANg/QA/gOBOgcQBcgaAygWQAjgTAhgUQgNAWgXAXQhIBahpBDQBpgfBbg8QA9goBOh7QAZA/AQAfQAnBOAuA0QASAWAXAUQgohOgVhQQBUBJCqAcIA3AJQgdgigagsIgyhYQAnAYA7AJQBBANCBATQBeAZBJBFQAMAMAMAJQA4BAAeBNQAhBigJCXIgHBOQBWiOAaikQARASAIAiQAFAPADAOIAFAaQAFBCgbA3QgpBHhcApIgaAKQgmA3gvAxQiwC2jmAwIAACkIgmAVgAqCCBQg9gagxgoQhKg9gVhQQgNg4ANglQAHgaAWgQQAKgHAMgHQAgDEB6CfIABACg");
	this.shape_15.setTransform(-0.8,-0.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AqkH2IgBAAIgBgCQgGg8gUhJQgnh4gRg9QgdhmAEhUQAEhnA1hEQAcgjAyggIBdg1QBAgiAcgUQAIgDAIgGQBZhBARhOQAUBHgDBIQgBAtgMAuQAegrAVgwQAghGAJhQQAWBMAQBKQATBXALBVQgBhxgDg7IAAgGQAEAEAAACQBvCWCcBmQgPhCgig+Qgig7gwgvII6CMQBGARAkAQQA4AXAjAkQAcAgAPA0QALAdAJA2QgBAkgGAjQgaCkhVCPIAGhPQAKiXghhiQgehNg5g/QgMgJgLgMQhKhFhdgZQiCgThAgNQg7gJgogYIAyBYQAaAsAdAiIg3gJQipgchUhJQAVBQAnBNQgWgTgTgWQgug0gnhOQgQgfgZg/QhNB7g+AoQhaA8hpAfQBphDBIhaQAWgXANgWQggAUgkATQgyAWhcAaQhNAcg/AOQgNA/gBBDQgDBqAQCAQALBPAZCMQgHAAgEgDg");
	this.shape_16.setTransform(2.7,-37.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AsvF7QgMhLAAhQQAAixA+iVQglA+gxBVQADhVAdhPQAUhAAig6QBQiKCChIQA3ggA9gSQBCgrBHgZQhLgNhLARQBlgdBoAFQBMADBPAXQAGgfAbgcQAXgbAsgTQAQgJASgFQBmgjBYgQQhMAQhEAxQgcATgaAaQD9AMDFCgQBHApBLA9QBqBXA3CPQAyCBALCxIgUgyQgMgmgYgbQAMBIAABQQAAAkgFAlQgJg2gLgeQgPg0gcggQgjgjg4gYQgkgQhGgRIo6iLQAwAvAiA6QAiA+APBCQichlhviVQAAgDgEgEIAAAHQADA7ABBwQgLhUgThXQgQhLgWhMQgJBQggBHQgVAvgeArQAMgtABgtQADhIgUhHQgRBNhZBCQgIAFgIADQgcAVhAAhIhdA1QgyAfgcAjQg1BFgEBnQgEBUAdBnQARA8AnB5QAUBIAGA8Qh6iggfjDg");
	this.shape_17.setTransform(1.1,-60.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-87.3,-135.3,174.6,270.6);


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhVAtIgCgtIgBgjIgBgbIgBgnIAqAAIABA1QAGgLAIgJQAHgJAJgIQAJgHAKgEQAKgFALAAQAOgBAKADQAKADAIAGQAHAGAFAIQAFAIACAJQADAIABAJIACAPIAAA+IgBBDIglgCIACg7QABgdgBgdIAAgJIgCgLIgFgLQgDgGgEgFQgEgEgHgCQgGgCgIABQgPACgOATQgPATgRAkIABAvIABAaIABAOIgoAFIgCg5g");
	this.shape.setTransform(107,4.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AAGgkIgrB/Ig3AEIgki9IAmgDIAcCZIA2iZIAiADIAlCaIAZicIApACIglC+Ig2ACg");
	this.shape_1.setTransform(81.4,4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AglBkQgSgHgNgPQgOgOgIgUQgHgTAAgZQAAgXAHgUQAIgUAOgOQANgOASgIQASgHATAAQAUgBASAJQASAHAOAPQANAOAHAUQAIAUAAAWQAAAXgIAUQgHAUgNAOQgOAOgSAJQgSAHgUABQgTAAgSgIgAgahEQgLAHgIALQgGAKgEAOQgDANAAANQAAANADAOQAEANAHAKQAIALALAHQALAGAOAAQAPAAALgGQAMgHAHgLQAHgKAEgNQADgOABgNQgBgNgDgNQgDgOgIgKQgGgLgMgHQgLgGgQgBQgPABgLAGg");
	this.shape_2.setTransform(56.1,4.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AAxCLQAGgRACgRIADgdIAAgeQgBgTgFgOQgGgNgIgJQgJgIgKgEQgLgEgKAAQgJAAgLAGQgJAEgKAKQgLAHgIASIgCB5IgjABIgEkaIAqgBIAABvQAKgLAKgGQAMgHAKgDQALgEAKAAQAVAAARAGQARAIAMAOQAMANAHAUQAHATABAYIAAAbIgCAbIgCAYIgFAVg");
	this.shape_3.setTransform(32.8,0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgaBnIgPgDIgRgGIgRgHQgIgFgIgGIASgcIAVAKQAKAGALADQALAEAMACQAKACAMAAQALAAAGgCQAIgBAEgDQAEgDADgEIAEgHQAAgDAAgDQAAgEgBgDIgGgHQgEgFgFgCQgHgDgIgCIgUgCQgPgBgPgCQgQgDgLgGQgNgGgIgIQgHgJgCgOQgBgNADgLQADgLAGgIQAGgJAJgHQAJgGAKgFQALgEAMgCQAMgCALAAIARAAIAUADQALADAKAEQAMAEAJAHIgNAhQgMgGgLgEIgTgGIgTgCQgcgCgQAIQgQAIAAAPQABALAGAFQAGAFAKADIAZACQANABAPADQASACAMAFQAMAFAIAIQAHAHAEAJQADAJAAAKQAAARgHALQgHAMgMAIQgMAHgPAEQgPADgQABQgQAAgSgEg");
	this.shape_4.setTransform(9.7,5.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgpBiQgSgHgNgNQgNgOgHgTQgGgSAAgYQAAgWAIgTQAHgUAOgPQANgPATgIQASgJAUAAQATAAARAIQARAHANANQANANAJASQAIASACAVIieAWQACANAFALQAFAKAIAHQAIAHALAEQAKAEALAAQAKAAAKgDQAJgDAIgGQAIgGAGgIQAGgJADgLIAjAHQgFARgJANQgKAOgMAJQgNAKgOAFQgPAGgRAAQgXAAgTgIgAgPhGQgJACgIAHQgJAGgHALQgHAKgCAQIBtgNIgBgDQgHgTgMgKQgNgKgSAAQgGAAgKADg");
	this.shape_5.setTransform(-21.3,4.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgnCZQgJgCgIgEQgJgEgIgGQgJgGgGgIQgHgJgFgLIAkgQQAEAIAEAGIALAKIAMAGIAMADQANADAOgCQAMgCAKgFQAKgGAGgGQAHgHAEgHIAGgOIADgMIABgIIAAgYQgLAKgMAGQgMAFgJADQgMADgLAAQgVAAgSgHQgSgHgOgOQgOgOgHgTQgIgUAAgYQAAgZAJgUQAIgTAOgOQAOgNASgHQARgHATAAQALABANAEQAKAEANAGQAMAHALALIAAglIAkABIgBDSQAAAMgDAMQgDALgGAMQgFALgJAJQgJAKgKAIQgMAHgNAFQgOAFgRABIgFABQgTAAgQgFgAgbhvQgLAFgIAKQgIAJgEANQgFANAAAPQAAAQAFAMQAEANAIAIQAIAJALAFQAMAFANAAQALAAAMgEQALgEAKgIQAJgGAGgLQAHgKACgNIAAgYQgCgNgGgLQgIgLgJgHQgJgIgMgEQgLgEgLAAQgOAAgLAFg");
	this.shape_6.setTransform(-45.6,9.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA5BoIADgaQgXAPgVAGQgVAHgTAAQgNAAgMgDQgNgEgJgHQgKgHgFgLQgGgLAAgOQAAgPAEgMQAFgMAIgJQAHgIALgGQAKgHAMgEQAMgEANgBQAMgCAOAAIAWAAIASACIgFgSQgEgJgFgHQgFgGgJgFQgHgFgMABQgGAAgJACQgJACgLAGQgLAFgNAJQgNAKgPAPIgWgbQASgRAQgKQAQgKAOgGQAPgGALgDQAMgCAJAAQASAAANAHQANAFAJAKQALAKAGANQAIAOAEAPQAEAQACARQACAPAAARQAAARgCAVQgCAUgEAYgAgIAAQgPACgMAIQgLAIgGAKQgFALADANQADALAHAFQAIAEAKAAQAKAAANgDQAMgEALgGQANgEAKgGIATgLIAAgSIgBgTQgJgCgJgCQgKgBgKAAQgQAAgPAEg");
	this.shape_7.setTransform(-69.6,4.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("ABYAsIgBgoIAAgdQAAgPgEgIQgFgGgHAAQgFAAgFACIgJAIQgEADgFAHIgJALIgHALIgGAKIAAARIABAWIABAcIAAAiIgjACIgBg5IgBgoIgCgdQAAgPgFgIQgEgGgHAAQgFAAgFADQgFACgFAGIgKALIgJAMIgIANIgHAJIACBhIgkACIgGjFIAngDIABA0IAMgOQAHgIAHgHQAIgGAIgEQAJgFALABQAIAAAHACQAHADAFAFQAGAFAEAIQAEAIABALIAMgOIAOgPQAHgFAIgFQAKgDAKAAQAIAAAIADQAHACAGAGQAGAGAEAJQAEAJAAANIACAhIACAsIAABAIgmACIAAg5g");
	this.shape_8.setTransform(-95.9,4.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgXCKIACgvIABg6IABhOIAkgBIAAAvIgBAoIgBAgIAAAZIgBAogAgJhXQgFgCgEgDQgDgEgCgFQgCgEAAgGQAAgFACgFQACgFADgDQAEgEAFgCQAFgCAEAAQAFAAAFACQAFACADAEQAEADACAFQACAFAAAFQAAAGgCAEQgCAFgEAEQgDADgFACQgFACgFAAQgEAAgFgCg");
	this.shape_9.setTransform(-115.6,0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(3,1,1).p("AxakFMAi1AAAQCgAAAACgIAADLQAACgigAAMgi1AAAQigAAAAigIAAjLQAAigCgAAg");
	this.shape_10.setTransform(1.5,4.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AxaEGQigAAAAigIAAjLQAAigCgAAMAi1AAAQCgAAAACgIAADLQAACgigAAg");
	this.shape_11.setTransform(1.5,4.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-30.3,259.6,62.2);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("AgsBrQgUgIgPgPQgOgOgGgVQgIgUAAgaQAAgYAJgVQAIgVAPgQQAPgQATgKQAUgJAWAAQAWAAARAJQATAHAOAOQAPAPAJATQAIAUADAWIirAZQABAOAGALQAFALAJAIQAIAIAMAEQAMAEALAAQALAAAKgDQALgEAJgFQAIgHAHgKQAGgJADgMIAnAIQgFASgLAOQgKAPgOALQgNAKgQAHQgRAFgSAAQgYAAgVgIgAgQhNQgKADgJAHQgJAHgIAMQgIAMgCARIB2gOIgBgFQgHgTgNgMQgNgLgVABQgHAAgKACg");
	this.shape.setTransform(181.8,4.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AgrCmIgSgGQgKgFgJgGQgIgHgIgJQgHgJgFgMIAmgRQAEAJAGAGQAFAGAGAEIANAHIANAEQAOADAPgCQAOgDALgGQAKgFAHgHQAHgIAFgHIAHgPIADgNIABgKIAAgaQgMAMgNAGQgNAGgLADQgMADgMAAQgXAAgUgIQgUgIgOgPQgPgPgJgUQgIgWAAgbQAAgbAJgVQAKgVAPgPQAPgPATgHQAUgIATAAQANACANAEQAMAEAOAHQANAHAMAMIAAgoIAnABIgBDkQAAANgDANQgDANgHAMQgGAMgJALQgKALgMAIQgMAIgPAGQgPAFgRABIgDABQgWAAgUgGgAgdh5QgNAGgIAKQgJALgFAOQgFAOAAAQQAAARAFANQAFAOAJAJQAJAKAMAGQANAFAOAAQAMAAANgEQAMgFAKgIQAKgIAHgLQAHgMADgNIAAgaQgCgPgIgLQgHgMgKgIQgKgJgNgEQgNgFgMAAQgOAAgMAGg");
	this.shape_1.setTransform(155.3,8.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("AA9ByIAEgcQgZAPgXAHQgWAHgVAAQgOAAgNgDQgOgEgKgIQgLgIgGgLQgHgMAAgQQABgRAFgMQAEgNAIgKQAJgJAMgHQAKgHAOgEQANgEAOgCQANgDAOAAIAZABIAUADIgGgTQgEgKgFgIQgHgIgIgEQgJgFgLAAQgIAAgJACQgLACgMAGQgMAGgOALQgOAKgQARIgYgeQATgSARgLQATgLAPgHQAPgGANgDQAMgCAMAAQARAAAPAGQAPAGAKALQALALAHAPQAIAPAFAQQAFARACASQACASAAARQAAAUgCAWQgDAVgEAagAgJAAQgRADgMAIQgMAIgGALQgHAMAEAPQADAMAIAFQAIAEALAAQAMAAANgDQANgEANgGIAagMIATgMIABgUIgBgUIgUgEIgWgBQgSAAgQAEg");
	this.shape_2.setTransform(129.3,3.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("ABgAwIgBgrIAAggQgBgQgEgIQgFgIgIAAQgFAAgFACQgFAEgFAFQgFAEgFAGIgJANIgJANIgHAKIABASIABAYIAAAeIAAAlIglADIgBg+IgBgrIgCggQgBgQgFgIQgEgIgIAAQgFgBgGAEIgKAJIgLAMIgKAOIgJANIgHALIABBoIgnADIgGjWIAqgFIABA7IANgQQAHgKAJgGQAIgHAKgEQAJgFAMAAQAIAAAIADQAIACAFAGQAHAGAEAIQAEAJACALIANgPQAHgIAIgHQAIgGAJgFQAKgEALAAQAJAAAIADQAIADAHAGQAGAHAFAKQAEAJAAANIACAlIACAwIABBFIgqADIAAg+g");
	this.shape_3.setTransform(100.5,3.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AgZCWIABg0IABg+IAChUIAogCIgBAzIgBArIAAAjIgBAbIAAAsgAgKheQgFgCgEgFQgEgDgCgGQgDgEABgGQgBgHADgEQACgGAEgDQAEgFAFgBQAFgDAFAAQAFAAAGADQAFABAEAFQAEADACAGQADAEgBAHQABAGgDAEQgCAGgEADQgEAFgFACQgGACgFAAQgFAAgFgCg");
	this.shape_4.setTransform(79.2,-0.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("AgsBrQgVgIgOgPQgNgOgHgVQgIgUAAgaQAAgYAIgVQAJgVAPgQQAOgQAVgKQAUgJAVAAQAVAAATAJQASAHAOAOQAOAPAKATQAIAUADAWIisAZQACAOAGALQAFALAJAIQAJAIALAEQALAEAMAAQALAAALgDQAKgEAJgFQAIgHAHgKQAGgJAEgMIAmAIQgGASgKAOQgKAPgOALQgNAKgRAHQgPAFgTAAQgZAAgUgIgAgQhNQgJADgKAHQgKAHgHAMQgIAMgDARIB3gOIgBgFQgHgTgNgMQgNgLgVABQgHAAgKACg");
	this.shape_5.setTransform(51,4.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("AA1CYQAGgTADgSIADghIABggQgBgWgHgOQgGgOgJgJQgKgKgLgFQgLgDgLAAQgKAAgMAGQgKAEgLALQgLAJgKASIgBCFIgnAAIgEkyIAugCIgBB5QALgLAMgIQANgGAKgEQANgEALgCQAWABATAHQASAJANAPQANAPAIAVQAIAVABAaIgBAdIgBAdIgDAbQgCANgDAKg");
	this.shape_6.setTransform(25.6,-0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgXgWIhGABIABgkIBGgCIABhYIAngCIgCBZIBNgDIgCAlIhLACIgCCtIgpABg");
	this.shape_7.setTransform(2.4,0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("AgsBrQgVgIgOgPQgOgOgGgVQgIgUAAgaQAAgYAIgVQAJgVAPgQQAPgQATgKQAVgJAVAAQAVAAATAJQASAHAOAOQAOAPAKATQAIAUADAWIirAZQABAOAGALQAFALAJAIQAIAIAMAEQALAEAMAAQALAAALgDQAKgEAJgFQAIgHAHgKQAGgJADgMIAnAIQgFASgLAOQgKAPgOALQgNAKgRAHQgPAFgTAAQgZAAgUgIgAgQhNQgKADgJAHQgKAHgHAMQgIAMgDARIB3gOIgBgFQgHgTgNgMQgNgLgVABQgHAAgKACg");
	this.shape_8.setTransform(-30.1,4.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgfBrIhGjJIAogFIA8CqIA+iyIApAGIhODRg");
	this.shape_9.setTransform(-53.5,3.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("AhZAxIgCgxIgCgnIgBgcIgBgrIAugBIAAAZIAAAYIAAAYIAPgUQAIgLAKgKQALgJAMgIQAMgIAOgBQAQgBANAIQAGADAFAGQAGAEAEAJQAFAIADALQAEAMAAAPIgnAPQAAgJgBgIIgEgMQgCgFgDgDIgHgGQgGgFgJAAQgFABgHAEIgMAJIgNAPIgNAPIgLAQIgLAOIABAdIABAbIABAXIABAQIgsAGIgBg/g");
	this.shape_10.setTransform(-74.8,4.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("AgsBrQgVgIgNgPQgOgOgIgVQgHgUAAgaQAAgYAIgVQAJgVAPgQQAPgQAUgKQATgJAXAAQAVAAASAJQASAHAOAOQAOAPAJATQAKAUACAWIisAZQACAOAFALQAGALAJAIQAIAIAMAEQAMAEAMAAQAKAAALgDQAKgEAIgFQAKgHAGgKQAHgJADgMIAmAIQgFASgLAOQgKAPgNALQgOAKgRAHQgQAFgRAAQgaAAgUgIgAgPhNQgKADgKAHQgJAHgIAMQgHAMgEARIB4gOIgBgFQgIgTgOgMQgNgLgTABQgIAAgJACg");
	this.shape_11.setTransform(-99.1,4.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgcBxIgRgEIgSgGIgTgJQgJgEgIgHIATgeIAXALIAXAKIAZAGQALACAOAAQALAAAHgCQAIgCAFgDIAHgHIAEgIIABgGQAAgFgCgDIgGgIQgEgEgGgEQgHgCgJgCQgJgDgNAAQgRgBgQgDQgRgDgNgGQgNgGgIgJQgJgLgCgPQgBgOADgMQADgMAHgJQAHgJAKgHQAJgHAMgFQALgFANgCQANgDAMABIATABQAKAAAMACQALADAMAEQAMAFAKAHIgOAlQgNgHgMgEIgVgGIgUgEQgegBgRAJQgSAHAAASQAAAMAHAFQAGAGALACQAMACAPABIAfADQAUADANAGQAOAGAIAIQAIAIADAJQAEAKAAAKQAAATgHANQgIANgNAIQgNAIgRAEQgQAEgSABQgSAAgSgEg");
	this.shape_12.setTransform(-122.8,4.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgRCZQgMgDgNgGQgNgHgNgLIAAAaIglABIgEkzIAsgBIgBB1QALgLANgGQAMgGALgDQAMgDALAAQANAAAOAEQANADAMAIQAMAHAKAJQAKAKAIANQAHAMAEANQADAOAAAQQgBARgEAPQgFAPgHANQgIANgKAKQgKAKgLAHQgLAHgMAEQgNADgLAAQgLAAgNgDgAAAgdQgMAAgKAEQgLAEgJAHQgJAGgGAIQgGAJgEALIAAAvQADANAHAJQAGAJAJAHQAJAGAKAEQAKADAKAAQAOABAMgGQANgGAKgJQAJgKAGgOQAFgNABgPQABgPgEgOQgFgOgJgKQgJgKgNgGQgMgGgOAAIgCAAg");
	this.shape_13.setTransform(-146.6,-0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("AgnCVQgUgFgQgKQgRgLgOgOQgOgOgJgRQgKgRgGgUQgFgUAAgVQAAgUAFgUQAGgUAKgRQAJgRAOgOQAOgOARgKQAQgLAUgEQATgHAUAAQAVAAATAHQATAEARALQARAKANAOQAOAOAKARQAKARAFAUQAGAUAAAUQAAAVgGAUQgFAUgKARQgKARgOAOQgNAOgRALQgRAKgTAFQgTAGgVgBQgUABgTgGgAgqhpQgVAKgPAPQgPAQgIAUQgJAWAAAWQAAAXAJAVQAIAWAPAPQAPAQAVAJQAUAJAWAAQAPAAAPgEQAOgEAMgIQAMgHALgLQAKgKAHgNQAHgMAEgPQAEgPAAgQQAAgOgEgPQgEgPgHgNQgHgNgKgKQgLgLgMgHQgMgIgOgDQgPgEgPgBQgWABgUAIg");
	this.shape_14.setTransform(-177.1,-0.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(3,1,1).p("A8QlHMA4hAAAQCmAAAACmIAAFDQAACmimAAMg4hAAAQimAAAAimIAAlDQAAimCmAAg");
	this.shape_15.setTransform(0.1,1.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("A8PFIQinAAAAimIAAlDQAAimCnAAMA4gAAAQCmAAAACmIAAFDQAACmimAAg");
	this.shape_16.setTransform(0.1,1.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-198.9,-33.9,398,69.5);


(lib.Symbol9copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AheL4QgtgTghgiQgjgjgUgsQgSgtAAgzQAAg0ASgtQAUguAjghQAhgjAtgUQAtgSAzAAQA1AAAsASQAtAUAhAjQAjAhATAuQAUAtAAA0QAAAzgUAtQgTAsgjAjQghAigtATQgsAUg1AAQgzAAgtgUgAikCWQAAhnAhhMQAhhMArgwQAUgZAXgVIAqgpQATgSANgSQAOgSAAgQQAAgggkgSQgigPglAAQg6AAhEAzQhDAygzBAIkOifQAhhPAxhIQAwhLBHg6QBGg6BhgiQBhgiB8AAQB1AABdAXQBdAXBAAxQBAAxAkBQQAjBRAABuQAABMgZA4QgXA4glAtQgkAtgtAlQguAngoAiQgnAjgeAoQgeAkgIAxg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9copy, new cjs.Rectangle(-54.6,-78,109.3,156), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag0AcIgBgcIgBgVIgBgRIAAgYIAaAAIAAAgQAEgGAFgGQAEgGAGgEQAFgFAGgCQAGgDAHAAQAJgBAGACQAGACAFAEQAFADACAFIAFALIADAKIABAJIAAAnIgBApIgXgBIACglIgBgjIAAgGIgBgHIgDgHIgEgGIgHgEQgEgBgFAAQgJACgIALQgKAMgKAWIABAdIAAAQIAAAJIgYADIgBgjg");
	this.shape.setTransform(152,39.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AADgWIgaBPIgiACIgWh0IAXgCIASBeIAhheIAWABIAWBgIAQhhIAZABIgXB2IgiAAg");
	this.shape_1.setTransform(136.1,39.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgXA+QgLgFgIgJQgIgIgFgNQgFgMAAgPQAAgOAFgMQAFgNAIgIQAIgJALgFQALgFAMABQAMAAALAEQALAFAJAKQAIAIAFANQAFALAAAOQAAAPgFAMQgFAMgIAIQgJAJgLAGQgLAEgMAAQgMABgLgFgAgQgqQgHAFgEAGQgFAHgCAIQgCAJAAAHQAAAIADAIQACAIAEAHQAFAHAHAEQAGAEAJAAQAJAAAHgEQAHgEAFgHQAEgHACgIQADgIAAgIQAAgHgCgJQgCgIgFgHQgEgGgHgFQgHgDgKAAQgJAAgHADg");
	this.shape_2.setTransform(120.4,39.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAfBXIAEgWIACgTIAAgSQAAgMgEgIQgEgIgFgFQgFgFgGgDQgHgCgGAAQgFAAgHAEQgGACgGAGQgGAEgGALIgBBMIgWAAIgCiuIAagBIgBBFQAHgHAHgDIANgHIANgDQAMAAALAEQALAFAHAJQAHAJAFALQAEAMABAPIgBARIAAAQIgCAPIgCANg");
	this.shape_3.setTransform(106,36.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgQBAIgKgCIgJgDIgLgFIgKgHIALgRIAMAGIAOAGIAOADQAGACAIAAIAKgBIAIgDIADgEIADgFIABgEIgCgEIgDgFIgGgEIgJgDIgMgBQgKAAgJgCQgJgCgIgDQgHgEgFgEQgFgGgBgJQAAgIABgHQACgHAEgFQAEgFAFgEQAGgEAGgDQAHgDAIgBQAGgBAHAAIALAAIAMACQAHABAGADQAHADAGAEIgIAUIgOgGIgMgDIgLgCQgSgBgJAFQgKAFAAAJQAAAHADADQAEADAGACIAQABIARACQALACAIADQAHADAFAEQAEAFADAFQACAGAAAGQAAALgEAHQgFAHgHAFQgIAFgJACQgKACgKAAQgJAAgLgCg");
	this.shape_4.setTransform(91.8,39.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgYA9QgMgFgIgIQgIgIgEgMQgFgMABgOQAAgNAEgMQAGgNAIgJQAIgJALgFQAMgFAMAAQALAAALAEQAKAFAIAIQAJAIAFALQAFALABANIhhANQABAIADAHQAEAGAFAFQAEAEAHADQAHACAGAAQAGAAAGgCQAGgCAEgDIAJgJQAEgGACgHIAVAFQgCAKgHAIQgFAJgIAGQgHAGgJADQgKADgKAAQgOAAgLgEgAgIgrQgGABgGAEQgFAEgEAHQgFAHgBAJIBDgIIgBgCQgEgLgHgHQgIgGgLAAIgJACg");
	this.shape_5.setTransform(72.6,39.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgYBfIgLgEQgFgCgFgEIgJgJQgEgFgDgHIAWgKQACAFADAEIAHAGIAHADIAHADQAIABAIgBQAIgBAGgEQAGgDAEgEQAEgEADgEIAEgJIACgHIAAgGIAAgOQgGAGgIAEIgNAFIgOACQgNAAgLgFQgLgFgJgIQgIgJgFgLQgFgMAAgPQAAgQAFgMQAGgMAIgIQAJgJALgEQALgEALAAIAPADIAOAGQAIAEAGAHIABgXIAWABIgBCBQAAAIgCAHIgFAOQgEAHgFAGQgFAGgHAFQgHAFgJADQgIADgKAAIgDABQgLAAgLgDgAgQhEQgHADgFAGQgFAGgDAIQgDAIAAAJQAAAKADAHQADAIAFAFQAFAGAHADQAHADAIAAQAHAAAHgDQAHgCAGgFQAFgEAEgGQAEgHACgIIAAgOQgBgJgFgGQgEgHgGgFQgFgEgIgDQgHgDgGAAQgIAAgHAEg");
	this.shape_6.setTransform(57.5,41.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAjBAIACgPQgOAIgNAEQgMAEgMAAQgIAAgIgCQgHgCgHgEQgGgFgDgGQgDgHgBgJQABgKACgGQADgIAEgGQAFgEAHgEQAGgEAIgDQAHgCAJgBQAGgCAIABIAPAAIALABIgDgKQgCgGgEgEQgDgFgGgCQgEgDgHAAIgJABIgMAFQgIADgHAGQgJAGgJAJIgOgQQALgLALgGQAKgHAJgDQAIgDAIgCIAMgBQAKAAAJADQAIAEAGAGQAHAGAEAJQAEAIADAKQACAJABALIABATIgBAXIgDAcgAgEAAQgKACgHAEQgHAFgDAGQgEAHACAIQACAHAEADQAFACAHAAQAGAAAIgBIAOgGIAOgHIAMgGIAAgMIAAgMIgNgBIgMgBQgKAAgIACg");
	this.shape_7.setTransform(42.7,38.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AA3AbIgBgYIAAgSQAAgJgDgFQgDgEgEAAQgDAAgDACIgFAEIgGAGIgFAHIgFAHIgEAGIAAAKIABAOIAAARIAAAVIgVABIAAgjIgBgYIgBgSQgBgJgCgFQgDgEgEAAQgDAAgDACIgHAFIgGAHIgFAIIgFAHIgFAGIABA7IgWABIgDh4IAXgDIABAhIAIgJIAIgJQAFgEAGgDQAFgCAHAAIAJABQAEACADADQAEADACAFQADAFAAAHIAIgJIAIgJIAKgGQAFgCAHAAIAKABQAEACAEAEQAEADACAGQADAGAAAHIABAVIABAbIAAAnIgXABIAAgjg");
	this.shape_8.setTransform(26.3,38.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgNBVIAAgdIABgjIABgwIAWgBIgBAdIgBAYIAAAUIAAAQIgBAYgAgFg1IgFgEIgEgFIgBgGIABgGIAEgFIAFgEQADgBACAAQADAAADABQADABADADIADAFIABAGIgBAGIgDAFQgDADgDABQgDABgDAAQgCAAgDgBg");
	this.shape_9.setTransform(14.2,36.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgYA9QgMgFgIgIQgIgIgEgMQgFgMABgOQAAgNAEgMQAGgNAIgJQAIgJALgFQAMgFAMAAQALAAALAEQAKAFAIAIQAJAIAFALQAFALABANIhhANQABAIADAHQAEAGAFAFQAEAEAHADQAHACAGAAQAGAAAGgCQAGgCAEgDIAJgJQAEgGACgHIAVAFQgCAKgHAIQgFAJgIAGQgHAGgJADQgKADgKAAQgOAAgLgEgAgIgrQgGABgGAEQgFAEgEAHQgFAHgBAJIBDgIIgBgCQgEgLgHgHQgIgGgLAAIgJACg");
	this.shape_10.setTransform(-1.7,39.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAeBXIAFgWIACgTIAAgSQAAgMgDgIQgFgIgEgFQgGgFgHgDQgFgCgHAAQgFAAgHAEQgGACgGAGQgHAEgFALIAABMIgXAAIgCiuIAagBIgBBFQAHgHAHgDIANgHIANgDQANAAAKAEQAKAFAIAJQAIAJAEALQAEAMABAPIAAARIgBAQIgCAPIgDANg");
	this.shape_11.setTransform(-16.1,36.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgNgMIgnABIAAgVIAogBIAAgyIAWgBIgBAzIAsgCIgBAVIgrABIgBBiIgXABg");
	this.shape_12.setTransform(-29.4,36.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgNgMIgnABIAAgVIAogBIAAgyIAWgBIgBAzIAsgCIgBAVIgrABIgBBiIgXABg");
	this.shape_13.setTransform(-46.5,36.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgUA9QgMgFgKgJQgJgJgFgMQgGgMAAgOQAAgHACgJQACgIAFgHQAEgIAGgGQAGgGAIgEQAHgFAIgCQAJgDAIAAQALAAAIACQAJADAIAEQAHAFAHAGQAGAHAEAIIAAAAIgTAMIAAgBQgDgGgFgEQgDgFgGgDIgLgFQgGgCgHAAQgHAAgJAEQgIADgGAGQgGAGgDAJQgEAIAAAIQAAAJAEAIQADAJAGAGQAGAGAIADQAJAEAHAAIANgCQAFgBAFgDIAJgHQAEgEAEgFIAAgBIAUALIAAABQgFAHgGAGQgHAGgIAEQgHAFgJACQgIACgKAAQgLAAgMgFg");
	this.shape_14.setTransform(-59,39.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgYA9QgMgFgIgIQgIgIgEgMQgFgMABgOQAAgNAEgMQAGgNAIgJQAIgJAMgFQALgFAMAAQAMAAAKAEQALAFAHAIQAJAIAFALQAFALABANIhhANQABAIADAHQAEAGAFAFQAEAEAHADQAHACAGAAQAGAAAGgCQAGgCAEgDIAJgJQAEgGACgHIAVAFQgCAKgHAIQgFAJgIAGQgIAGgIADQgKADgKAAQgOAAgLgEgAgJgrQgFABgGAEQgFAEgEAHQgFAHgBAJIBDgIIgBgCQgEgLgHgHQgIgGgLAAIgKACg");
	this.shape_15.setTransform(-72.9,39.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAHBjQgGgDgEgEQgFgEgDgGIgGgKQgFgNgBgRIADiOIAWAAIgBAlIgBAeIAAAZIAAASIAAAfQAAAKACAJIADAGQABAEADADIAHAEQADACAFAAIgDAWQgIAAgGgCg");
	this.shape_16.setTransform(-82.4,35);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgYA9QgMgFgIgIQgIgIgEgMQgFgMABgOQAAgNAEgMQAFgNAJgJQAIgJAMgFQALgFAMAAQAMAAAKAEQAKAFAIAIQAJAIAFALQAFALABANIhhANQABAIADAHQADAGAGAFQAEAEAHADQAGACAHAAQAGAAAGgCQAGgCAEgDIAJgJQAEgGACgHIAVAFQgDAKgGAIQgFAJgIAGQgIAGgIADQgKADgKAAQgOAAgLgEgAgJgrQgFABgGAEQgFAEgEAHQgFAHgBAJIBDgIIAAgCQgFgLgHgHQgIgGgLAAIgKACg");
	this.shape_17.setTransform(-93.2,39.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgOBbIgRgFIgQgIIgQgLIAQgTQAJAIAJAFQAJAEAHACQAIADAGABQAIAAAHgBQAHgCAFgDQAGgDADgFQADgFABgFQAAgEgCgDIgFgGIgIgGIgLgDIgLgEIgKgCIgNgDIgOgFQgHgDgHgFQgGgDgEgHQgFgGgCgJQgCgIABgLQAAgJADgHQADgIAFgFQAFgGAHgEQAGgEAHgDQAIgDAIgBIAOgBQALAAAKADIAKADIAKAFIAKAFIAKAHIgNAVIgIgHIgIgFIgIgEIgIgDQgJgDgIAAQgJAAgIADQgIAEgGAEQgFAFgDAGQgDAGAAAHQAAAFADAGQAEAFAGAEQAGAFAIADQAJADAIABIARADIAPAEQAIAEAGAEQAGAFAFAFQAEAHACAHQACAHgBAJQgBAHgEAHQgDAGgFAEIgLAIIgNAGIgOACIgNABQgIAAgIgCg");
	this.shape_18.setTransform(-107.7,36.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#660000").ss(3,1,1).p("Ayci4MAk5AAAQA8AAAqAqQArArAAA8IAABOQAAA9grAqQgqArg8AAMgk5AAAQg8AAgqgrQgrgqAAg9IAAhOQAAg8ArgrQAqgqA8AAg");
	this.shape_19.setTransform(21.9,36,1.31,1.161);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0033").s().p("AycC5Qg8gBgqgqQgrgqAAg9IAAhOQAAg8ArgrQAqgpA8AAMAk5AAAQA8AAAqApQArArAAA8IAABOQAAA9grAqQgqAqg8ABg");
	this.shape_20.setTransform(21.9,36,1.31,1.161);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-153.2,13.1,350.3,45.9), null);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("AgaBBQgNgFgIgIQgJgJgEgNQgFgMAAgQQAAgOAGgNQAFgNAJgKQAJgKAMgFQAMgGANAAQANAAALAFQALAFAJAIQAIAJAGAMQAFAMACANIhoAPQABAIADAIQADAGAGAFQAFAFAHADQAHACAHAAQAGAAAHgCQAGgCAFgEQAGgEAEgGQAEgFABgHIAYAEQgEALgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgPAAgMgFgAgJguQgGACgGADQgGAFgEAHQgFAHgBAKIBIgIIgBgDQgFgLgIgIQgIgGgMAAIgKACg");
	this.shape.setTransform(133.6,30.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AgaBlIgLgEQgGgCgFgEIgKgKQgEgFgDgIIAXgKQACAFAEAEIAHAGIAHAEIAIADQAJACAJgCQAIgBAHgEQAGgDAEgFIAHgJIAEgJIADgIIAAgFIAAgQQgHAHgIAEIgOAFIgPACQgOAAgMgFQgMgFgJgJQgJgJgFgMQgFgNAAgRQAAgQAFgNQAGgNAJgJQAKgJALgEQAMgFAMAAIAQAEIAPAGQAIAFAHAHIAAgYIAYAAIAACLQgBAIgCAHQgBAIgEAIQgEAHgGAGQgFAHgIAFQgHAFgJADQgJADgLABIgDAAQgMAAgMgDgAgShJQgHAEgFAGQgGAGgCAJQgDAIAAAKQAAAKADAJQADAIAFAFQAFAGAIADQAHAEAJAAQAHAAAIgDQAHgDAHgFQAGgEAEgHQAEgHACgIIAAgQQgCgJgEgHQgEgHgHgFQgGgFgIgDQgHgDgHAAQgJAAgIAEg");
	this.shape_1.setTransform(117.5,33.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("AAlBFIACgRQgPAKgOADQgNAFgNAAQgIAAgJgCQgHgCgGgFQgHgFgEgHQgDgIgBgJQAAgKADgHQADgJAFgGQAGgEAHgEQAGgFAIgDIARgDIAQgBIAPAAIAMACIgDgNQgDgGgDgEQgEgFgFgDQgGgDgGAAIgLACIgNAFIgQAKQgJAHgJAJIgPgRQAMgMAKgGQALgIAJgDQAKgEAIgCIAOgBQAKAAAKAEQAIADAHAHQAHAGAEAJQAFAKADAKIADAVIACAVIgCAZIgDAdgAgFAAQgKACgIAFQgHAEgEAIQgDAHACAJQABAGAGAEQAEACAHAAQAHABAJgDIAOgGIAQgHIAMgHIABgMIgCgNIgMgBIgNgBQgLAAgJACg");
	this.shape_2.setTransform(101.7,30.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("AA6AdIAAgbIAAgSQgBgKgCgFQgDgFgFAAQgDAAgDACIgGAEIgGAHIgGAIIgFAIIgEAGIAAALIABAPIAAASIAAAWIgXABIAAglIgBgbIgBgSQAAgKgDgFQgDgFgFAAQgDAAgDACIgHAGIgGAHIgGAIIgGAIIgEAGIABBAIgYABIgEiBIAagDIAAAjIAIgKIAKgJQAFgEAGgDQAGgDAHAAQAFABAEABIAIAFQAEAEADAFQACAFABAHIAIgJIAJgJQAFgFAGgCQAFgCAHgBQAGAAAFACQAFACAEAEQAEAEACAGQADAGAAAIIABAWIABAcIABArIgaABIAAglg");
	this.shape_3.setTransform(84.1,30);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AgPBbIABgfIABgmIAAgzIAZgBIgBAfIgBAaIAAAVIAAARIgBAagAgGg5QgDgBgCgDIgEgFIgBgHIABgHIAEgFQACgDADgBQADgBADAAIAHABQADABACADIAEAFQABADAAAEQAAAEgBADIgEAFQgCADgDABIgHABQgDAAgDgBg");
	this.shape_4.setTransform(71.2,27.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("AgaBBQgNgFgIgIQgJgJgEgNQgFgMAAgQQAAgOAGgNQAFgNAJgKQAJgKAMgFQAMgGANAAQANAAALAFQALAFAJAIQAIAJAGAMQAFAMACANIhoAPQABAIADAIQADAGAGAFQAFAFAHADQAHACAHAAQAGAAAHgCQAGgCAFgEQAGgEAEgGQAEgFABgHIAYAEQgEALgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgPAAgMgFgAgJguQgGACgGADQgGAFgEAHQgFAHgBAKIBIgIIgBgDQgFgLgIgIQgIgGgMAAIgKACg");
	this.shape_5.setTransform(54,30.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("AAgBcQAEgLACgLIACgUIAAgTQAAgNgFgJQgEgJgFgFQgGgGgGgDQgIgCgGAAQgGAAgHAEQgGADgHAGQgHAFgGALIAABQIgYABIgCi6IAbgBIAABKQAHgIAHgEIAOgGQAIgDAGAAQAOAAALAEQALAFAIAJQAIAJAFANQAEANABAQIAAASIgBARIgCAQIgDAOg");
	this.shape_6.setTransform(38.6,27.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgOgNIgqABIABgWIApgCIABg1IAYgBIgBA2IAvgCIgCAXIguABIAABpIgZAAg");
	this.shape_7.setTransform(24.5,27.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("AgaBBQgNgFgIgIQgJgJgEgNQgFgMAAgQQAAgOAGgNQAFgNAJgKQAJgKAMgFQAMgGANAAQANAAALAFQALAFAJAIQAIAJAGAMQAFAMACANIhoAPQABAIADAIQADAGAGAFQAFAFAHADQAHACAHAAQAGAAAHgCQAGgCAFgEQAGgEAEgGQAEgFABgHIAYAEQgEALgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgPAAgMgFgAgJguQgGACgGADQgGAFgEAHQgFAHgBAKIBIgIIgBgDQgFgLgIgIQgIgGgMAAIgKACg");
	this.shape_8.setTransform(4.7,30.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgTBBIgqh5IAYgEIAlBnIAlhsIAZAEIgwB/g");
	this.shape_9.setTransform(-9.5,30.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("Ag2AeIgBgeIgBgXIAAgRIgBgaIAcgBIAAAQIAAAOIAAAOIAJgMQAFgHAGgGQAGgGAHgEQAHgEAJgBQAKgBAIAFIAHAFIAGAIQADAFACAHIACAQIgYAJIgBgKIgCgHIgDgGIgEgDQgEgDgFAAQgEABgEACIgHAGIgHAIIgIAKIgHAKIgHAIIABASIAAAQIABAOIAAAJIgaAEIgBgmg");
	this.shape_10.setTransform(-22.5,30.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("AgaBBQgNgFgIgIQgJgJgEgNQgFgMAAgQQAAgOAGgNQAFgNAJgKQAJgKAMgFQAMgGANAAQANAAALAFQALAFAJAIQAIAJAGAMQAFAMACANIhoAPQABAIADAIQADAGAGAFQAFAFAHADQAHACAHAAQAGAAAHgCQAGgCAFgEQAGgEAEgGQAEgFABgHIAYAEQgEALgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgPAAgMgFgAgJguQgGACgGADQgGAFgEAHQgFAHgBAKIBIgIIgBgDQgFgLgIgIQgIgGgMAAIgKACg");
	this.shape_11.setTransform(-37.3,30.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgRBFIgKgDIgLgDIgLgGIgLgHIAMgSIAOAHIAOAFIAPAEQAGACAIAAIAMgBIAHgEIAFgEIACgEIABgFIgBgFIgEgEQgCgDgEgBIgKgEIgNgBIgUgCQgKgCgIgEQgIgEgFgFQgFgGgBgJQgBgJACgHQACgHAEgGQAEgGAGgEIANgHQAHgDAIgBIAPgCIALABIANACIAOAEQAHACAHAFIgJAWQgIgEgHgCIgNgEIgMgCQgSgBgLAFQgKAFAAAKQAAAIAEADQAEADAHACIAQACIASACQAMACAIADQAJADAFAFQAFAEACAGQACAGAAAHQAAALgFAIQgEAIgIAFQgIAFgKACQgKACgLABQgKAAgMgCg");
	this.shape_12.setTransform(-51.7,30.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgKBdIgPgGQgIgEgIgGIAAAQIgXAAIgCi6IAbgBIgBBIQAHgHAIgDQAHgEAHgCIANgDQAIABAJACQAIADAHADQAHAFAGAGQAHAGAEAIQAEAHADAHQACAJAAAJQgBALgCAKQgDAIgFAJQgEAHgGAGQgGAGgHAEQgHAFgIACQgHACgHAAQgGAAgIgCgAgNgOQgHACgFAEQgGAEgDAFQgEAFgDAGIAAAdQACAHAEAHQAEAFAGAEQAFAEAGACQAGACAGAAQAIAAAIgDQAIgDAGgHQAFgGAEgHQADgJABgJQAAgJgDgIQgCgJgGgGQgFgGgIgDQgIgEgJAAQgHAAgGADg");
	this.shape_13.setTransform(-66.1,27.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("AgXBbQgMgEgKgGQgLgGgIgJQgIgIgGgLQgGgKgEgMQgDgMAAgNQAAgMADgMQAEgMAGgKQAGgLAIgIQAIgJALgGQAKgGAMgDQALgEAMAAQAMAAAMAEQAMADAKAGQAKAGAJAJQAIAIAGALQAGAKADAMQAEAMAAAMQAAANgEAMQgDAMgGAKQgGALgIAIQgJAJgKAGQgKAGgMAEQgMADgMAAQgMAAgLgDgAgZg/QgNAFgJAKQgJAJgFANQgGANAAANQAAAOAGANQAFANAJAJQAJAKANAFQAMAGANAAQAJAAAJgDQAJgCAHgFQAIgEAGgHQAGgGAEgIQAFgHACgJQADgJAAgKQAAgJgDgIQgCgJgFgIQgEgIgGgGQgGgHgIgEQgHgFgJgCQgJgDgJAAQgNAAgMAGg");
	this.shape_14.setTransform(-84.7,27.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#660000").ss(3,1,1).p("Azxi4MAniAAAQBBAAAtAqQAuArAAA8IAABPQAAA8guAqQgtArhBAAMgniAAAQhAAAgtgrQgugqAAg8IAAhPQAAg8AugrQAtgqBAAAg");
	this.shape_15.setTransform(22.7,27,1.188,1.188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFCC00").s().p("AzwC4QhAAAgugqQgtgrAAg7IAAhPQAAg8AtgqQAugqBAgBMAnhAAAQBAABAuAqQAtAqAAA8IAABPQAAA7gtArQguAqhAAAg");
	this.shape_16.setTransform(22.7,27,1.188,1.188);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-147.5,3.6,340.5,46.8), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ak6IlQhgAAhDhCQhEhCAAheIAAqFQAAheBEhCQBDhCBgAAIJ1AAQBgAABDBCQBEBCAABeIAAKFQAABehEBCQhDBChgAAg");
	mask.setTransform(46.1,44);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("Ag4ADIgBgEQgBgXAcAEQA3gGAfAmQAEAFgDAFQg1geg8ALg");
	this.shape.setTransform(59.9,39.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("Ag4AMQAfgmA3AFQAdgDgBAXQAAACgCACQg8gMg0AfQgDgGADgEg");
	this.shape_1.setTransform(30,39.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgPARQgVgRAGgWQALAjAigLQAIgCACgJQABgHAFgFQAIAegeAMQgGACgEAAQgIAAgGgGg");
	this.shape_2.setTransform(43.9,55.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AgbAbQgKgLAAgQQAAgGACgFQAAADADACQAHAGAHAAQAJAAAEgGQAGgFAAgHQAAgJgGgFIgEgEQAFgCAFAAQAPAAAMAMQAKALAAAPQAAAQgKALQgMALgPAAQgQAAgMgLg");
	this.shape_3.setTransform(60.2,46.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AgiAjQgPgPAAgUQAAgUAPgPQAPgOATAAQAUAAAPAOQAPAPAAAUQAAAUgPAPQgPAPgUAAQgTAAgPgPgAgoAJQAAARAKAKQANAMAQAAQAPAAALgMQALgKAAgRQAAgOgLgMQgLgLgPAAQgGAAgFABIAFAEQAGAGAAAIQAAAIgGAEQgFAGgJAAQgHAAgGgGQgDgCgBgCQgBAEgBAGg");
	this.shape_4.setTransform(60.4,45.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AgbAbQgKgLAAgQQAAgGACgFQAAADADACQAGAGAIAAQAHAAAGgGQAFgFAAgHQAAgJgFgFIgFgEIAKgCQAQAAALAMQALALAAAPQAAAQgLALQgLALgQAAQgPAAgMgLg");
	this.shape_5.setTransform(28.5,46.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AgjAjQgOgPAAgUQAAgUAOgPQAPgOAUAAQAVAAAPAOQAOAPAAAUQAAAUgOAPQgPAPgVAAQgUAAgPgPgAgoAJQAAARALAKQAMAMAQAAQAPAAALgMQALgKAAgRQAAgOgLgMQgLgLgPAAIgLABIAEAEQAHAGAAAIQAAAIgHAEQgFAGgHAAQgIAAgHgGQgCgCAAgCQgCAEgBAGg");
	this.shape_6.setTransform(28.8,45.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AhKAFQgUgJgGgOQBpARBggSQgGANgWALQgfAPgqAAQgrAAgfgPg");
	this.shape_7.setTransform(44.3,62.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AhfAFQgYgLgKgQIAcgCQAGAOAVAKQAeAPAsAAQAqAAAegPQAWgLAGgOIAeABQgKARgZAMQgoAUg3ABQg3gBgogUg");
	this.shape_8.setTransform(44.4,63.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AlXhhQgFghAAgiQAAhLAahAQgPAagWAlQACglAMgiQAJgbAOgZQAjg7A3gfQAXgNAbgIQAcgSAegLQgggGggAHQAsgMAsACQAgACAiAKQADgOALgMQAKgLASgJQAHgDAIgDQArgPAmgGQghAGgdAVQgLAJgLALQBsAFBUBEQAeARAhAbQAtAlAXA9QAWA4AEBLQgEgLgEgKQgGgQgKgMQAFAfAAAiQAAAQgBAQQgBAPgCAPAkQA4QgLg7gEgiQgHg2ABguQABgdAFgbQAbgGAhgMQAogLAUgKQAQgHAOgJQgGAJgJAKQgfAmgtAdQAtgNAngaQAagRAhg0QALAaAGAOQARAhAUAXQAIAJAJAIQgRghgIgjQAjAgBIAMQAMACAMACQgMgPgMgSQgMgWgJgQQARAKAZAEQAbAFA4AJQAoAKAgAeQAFAFAEAEQAZAbANAhQAOAqgEBBQgCANgBATQAkg8AMhGQAHAIADAOQACAHACAGAlXhhQgFACgFAEQgJAHgDAKQgGAQAGAYQAJAiAfAaQAVARAaALIABABQACABADAAAkVA3QgBgBAAAAQgzhEgOhTAhWC7QhagXhGhIQgNgPgMgPQAAgDgBgDAhWI+IkMAAIAAjLQAAhWBYALQAzAFAxADIAAA+IA5g8QAGAAAGAAIA3A9IABABIBCAAIABAAAhnErIAGAHAhnErIgGAHAhWEHIANAGIgeAeAhJENIAfAAIAABiIAAABIABAAAhWEHIhQgqIAABTAgqFwIAAAJIgsDFICcAAAhWEHIAAhMAFqg4QACAcgMAYQgRAegnARQgGACgGACQgQAXgUAWQhLBOhiAUQgkAHgmAAQgtAAgqgKABTEwQAEAAAFAAABXErIgEAFIg5BAIgBAAABLEEIBMgnIAABPIAABCIg7g+ABXErIAFAFAAaENIAgAAIAdAeAAaFwIAAAAIAAAJIAsDFABLEEIgRAJAAaENIAABjACXEsQAygEAygFQBWgGAABWIAADLIkLAAABLC+IAABGAgqENIBEAA");
	this.shape_9.setTransform(44.5,53.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AggAyIgBgBIAAhiIBDAAIAABiIAAABg");
	this.shape_10.setTransform(43.7,85.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("AhNBnIArjEIAAgJIACAAIBCAAIAAAAIAAAJIAsDEg");
	this.shape_11.setTransform(43.7,100.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#129293").s().p("ABkAKIgFgEIgdgdIARgJIBMgoIAABPIAABBgAieAKIAAhSIBQArIANAGIgeAdIgGAHIg5A7g");
	this.shape_12.setTransform(43.7,83.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("AAigwIAhAAIAdAeIgFAFIg5A/gAhZgLIgGgHIAegeIAfAAIAABhg");
	this.shape_13.setTransform(43.7,85.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#01627F").s().p("ABPCOIgsjFIAAgIIAAgBIA5g/IAJgBIA7A+IAAhBIBkgJQBWgGAABVIAADLgAlZCOIAAjLQAAhVBYALQAzAFAxACIAAA+IA5g8IAMAAIA3A+IAAABIAAAIIgsDFg");
	this.shape_14.setTransform(43.6,97);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AAdEOIhEAAIgfAAIgMgGIAAhMQApAKAtAAQAnAAAjgHIAABGIgQAJgAhSC8QhagXhHhIIgZgeIgBgGQgKg7gFghQgHg3ABgtQABgdAGgcQAagGAigLQAngMAVgKQAQgHANgJQgFAKgKAKQgfAlgtAdQAtgNAngaQAagRAig0IARAoQAQAiAUAWIASASQgRgigJgjQAkAgBIAMIAXAEQgMgOgLgTIgWglQASAJAZAEIBTAOQAoAKAfAeIAKAJQAYAbANAiQAOApgEBBIgDAgQAlg8ALhGQAHAIAEAOIADANIACAMQADAbgMAYQgSAegnARIgLAEQgQAXgVAWQhLBOhiAUQgjAHgnAAQgtAAgpgKgAkSA3QgagLgVgRQgfgagKgiQgFgYAFgPQADgLAKgHIAJgGQAOBUA0BDIAAABg");
	this.shape_15.setTransform(44.2,53.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AkhDXIgBgBQgCgagJgfIgYhOQgMgrACgkQABgsAXgdQAMgPAVgNIAogXQAbgPAMgIIAHgEQAmgcAIghQAIAegBAfQgBATgEAUQANgTAIgUQAOgeADgiQAKAgAHAgQAIAlAFAlIgChKIAAgCIACACQAvBABCAsQgGgdgPgaQgOgZgUgUIDzA7QAeAIAPAGQAYAKAPAQQAMANAHAXQAEAMAFAXIgDAeQgMBGgkA9IADghQAEhBgOgqQgNghgZgaIgJgJQgggegogKIhTgOQgZgEgRgKIAVAmQAMASAMAPIgYgEQhIgMgjggQAIAjARAgIgRgQQgUgXgRghIgRgoQghA0gaARQgnAagtANQAtgdAfgmQAJgKAGgJQgOAJgQAHQgUAKgoALQghAMgbAGQgFAbgBAcQgBAuAHA2QAEAiALA8IgFgBg");
	this.shape_16.setTransform(45.7,37.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AlcCiQgFghAAghQAAhMAag/IgkA+QABgkANghQAIgcAPgYQAig8A4geQAXgOAbgIQAbgSAfgLQgggGggAHQArgMAsACQAhACAiAKQACgOALgLQALgMASgJIAOgFQAsgQAmgGQghAGgdAVQgMAJgLALQBsAFBVBFQAeAQAgAbQAtAlAYA+QAVA2AEBLIgIgVQgFgQgKgLQAFAeAAAjIgCAfQgEgXgEgNQgHgXgMgNQgPgQgYgJQgPgHgegHIj0g7QAUAUAPAYQAOAaAHAdQhCgsgvg+IgCgDIAAADIACBIQgFgjgJglQgGghgKggQgEAigNAfQgJATgNATQAFgTAAgTQACgggJgeQgHAhgnAdIgGADQgMAJgbAOIgoAXQgVANgMAOQgXAegCAsQgCAkANArIAYBOQAJAfACAaQg0hEgOhUg");
	this.shape_17.setTransform(45,27.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(255,255,255,0.02)").s().p("Ak7IlQhfAAhEhCQhDhCAAheIAAqGQAAhdBDhCQBEhCBfAAIJ2AAQBgAABDBCQBEBCAABdIAAKGQAABehEBCQhDBChgAAg");
	this.shape_18.setTransform(46.2,44.1);

	var maskedShapeInstanceList = [this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy18, new cjs.Rectangle(-8.4,-10.8,109.2,109.8), null);


(lib.Symbol1copy14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AlWIlQhVAAg8g8Qg9g9AAhWIAAqrQAAhWA9g9QA8g8BVAAIKsAAQBWAAA8A8QA9A9AABWIAAKrQAABWg9A9Qg8A8hWAAg");
	mask.setTransform(46.1,43.9);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("AgjEiQgRgIgNgNQgNgNgIgRQgHgRAAgTQAAgUAHgRQAIgSANgMQANgOARgHQARgHATAAQAUAAARAHQARAHANAOQANAMAHASQAIARAAAUQAAATgIARQgHARgNANQgNANgRAIQgRAHgUAAQgTAAgRgHgAg+A5QAAgnANgcQAMgdARgTQAHgJAJgIIAPgQQAIgHAFgHQAFgGAAgHQAAgMgOgGQgNgGgNAAQgWAAgaATQgaATgTAZIhng9QANgeASgcQASgcAcgWQAagWAlgNQAlgNAvAAQAsAAAkAJQAjAIAZATQAYATAOAeQANAfAAAqQAAAdgJAVQgJAWgOARQgOARgRAOIghAbQgPAOgLAPQgMAOgDASg");
	this.shape.setTransform(44.7,41.5);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_6
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape_1.setTransform(46.1,43.9,0.459,0.459);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_2.setTransform(46.1,43.9,0.459,0.459);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC00").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_3.setTransform(46.1,44);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("Al8KYQh0AAhShQQhShQAAhxIAAsNQAAhxBShQQBShQB0AAIL5AAQB0AABSBQQBSBQAABxIAAMNQAABxhSBQQhSBQh0AAg");
	this.shape_4.setTransform(46.2,44.2,1.056,1.056);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy14, new cjs.Rectangle(-23.6,-26,139.6,140.4), null);


(lib.Symbol1copy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AlWIlQhVAAg8g8Qg9g9AAhWIAAqrQAAhWA9g9QA8g8BVAAIKsAAQBWAAA8A8QA9A9AABWIAAKrQAABWg9A9Qg8A8hWAAg");
	mask.setTransform(46.1,43.9);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AgyADQgBgCAAgCQgBgVAaAEQAxgFAbAhQAEAFgDAFQgvgbg2AKg");
	this.shape.setTransform(58.1,41.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgyAKQAcghAxAFQAagDgBAUIgBAEQg2gLgvAcQgCgFACgFg");
	this.shape_1.setTransform(31.4,41.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgNAPQgUgPAGgTQALAfAdgKQAIgCABgIQABgGAEgFQAIAbgcALQgEABgDAAQgIAAgFgFg");
	this.shape_2.setTransform(43.8,55.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AgYAYQgJgJAAgPIABgJIAEAFQAFAEAHAAQAIAAAEgEQAEgFAAgHQAAgHgEgGIgEgDQAEgBAEAAQAPAAAKAKQAJAKAAANQAAAPgJAJQgKAKgPAAQgNAAgLgKg");
	this.shape_3.setTransform(58.3,47.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AgfAgQgMgOAAgSQAAgSAMgNQAOgNARAAQATAAAMANQANANAAASQAAASgNAOQgMANgTgBQgRABgOgNgAgjAJQAAAOAJAKQALAJAOABQANgBALgJQAJgKgBgOQABgOgJgKQgLgKgNAAQgFAAgEABIAEAEQAFAFAAAHQAAAHgFAEQgFAFgHAAQgHAAgGgFIgDgEIgBAKg");
	this.shape_4.setTransform(58.5,46.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AgYAYQgJgJAAgPQAAgEABgFQABACADADQAGAEAGAAQAHAAAEgEQAFgFAAgHQAAgHgFgGIgDgDIAIgBQAPAAAJAKQAKAKAAANQAAAPgKAJQgJAKgPAAQgOAAgKgKg");
	this.shape_5.setTransform(30.1,47.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AgfAgQgNgOAAgSQAAgSANgNQAOgNASAAQASAAAMANQAOANAAASQAAASgOAOQgMANgSgBQgSABgOgNgAgjAJQAAAOAJAKQAKAJAPABQANgBAJgJQALgKAAgOQAAgOgLgKQgJgKgNAAIgKABIAEAEQAGAFAAAHQAAAHgGAEQgFAFgGAAQgHAAgGgFQgCgCgBgCQgBAEAAAGg");
	this.shape_6.setTransform(30.3,46.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AhBAEQgSgIgGgMQBdAPBWgQQgGAMgTAJQgbAOgmAAQgnAAgagOg");
	this.shape_7.setTransform(44.2,61.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AhUAEQgWgJgJgOIAZgCQAGAMASAJQAaANAoAAQAlAAAbgNQATgKAGgMIAaAAQgIAPgWALQgkATgxAAQgxAAgjgTg");
	this.shape_8.setTransform(44.3,62.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("AkyhXQgEgcAAgfQAAhDAXg5QgOAXgSAhQABghALgeQAHgYANgWQAfg1AxgbQAVgMAYgHQAZgRAbgJQgdgFgcAGQAmgLAoACQAdABAeAJQACgMAKgKQAJgLAQgHQAGgDAHgCQAngOAigGQgdAGgaATQgLAHgKAKQBhAFBLA9QAbAPAcAYQApAhAVA2QATAyAEBDQgEgKgEgJQgEgOgJgLQAEAcAAAeQAAAOgCAOQAAAOgCANAjyAyQgKg1gEgeQgGgwABgpQAAgaAFgYQAYgFAegLQAjgKATgIQANgHANgIQgFAIgJAJQgbAigoAaQAogMAigXQAYgPAdgvQAKAYAGAMQAPAeARAUQAHAIAJAIQgPgegIgfQAgAcBAALQAKABALACQgLgNgKgQQgLgTgIgPQAPAJAWAEQAZAEAxAIQAkAJAcAbQAEAEAFAEQAVAYAMAdQAMAmgDA5QgCAMgBARQAhg1AKg/QAGAHADANQACAGABAFAkyhXQgEADgEADQgJAGgCAJQgFAPAFAVQAIAeAcAXQATAPAXAKIAAABQAAgBAAAAQgvg8gMhLgAhMCnQhRgVg+hAQgMgNgLgOQAAgCAAgDAj3AxQACABADAAAiUEOIAAA4IAzg1QAFAAAFAAIAxA2IACABIA6AAIABAAIAAAIIAnCwAhMIAIjvAAIAAi2QAAhMBOAKQAtAEAsACAhBDwIgbAbIgFAGAhMDqIALAGIAbAAIA9AAIAdAAIAaAbIgFAEQAFgBAEAAAhcELIAFAGAhMDqIAAhDAhMDqIhIglIAABJAFCgyQACAZgKAVQgQAbgjAPQgFACgFACQgOAVgSATQhDBFhYASQgfAHgiAAQgoAAglgJABOELIAEADAAXFIIAAAAAAXFIIgBAAAgmFHIAAABIACAAAgmFIIAAAIIgmCwICKAAABJEPIgyA5AAXDwIAABYAgmDwIAABXABCDoIgOAIACGEMIAAA6Ig0g4ACGEMQAtgEAsgFQBNgFAABMIAAC2IjuAAABCCpIAAA/IBEgjIAABH");
	this.shape_9.setTransform(44.4,54.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#07BABB").s().p("AhFBcIAnivIAAgIIACAAIA6AAIAAAAIAAAIIAoCvg");
	this.shape_10.setTransform(43.6,96.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#10DADB").s().p("AgcAsIgCgBIAAhWIA9AAIAABXIgBAAg");
	this.shape_11.setTransform(43.6,82.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("ABGB+IgoivIAAgHIAAgBIAzg4IAJgBIA0A4IAAg7IBZgIQBNgGAABNIAAC0gAkzB+IAAi0QAAhNBOAKQAtAFAsACIAAA4IAzg2IAKAAIAxA3IAAABIAAAHIgnCvg");
	this.shape_12.setTransform(43.6,92.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#129293").s().p("ABZAJIgEgEIgagaIAPgIIBDgjIAABGIAAA7gAiMAJIAAhJIBHAmIALAFIgbAaIgFAGIgyA2g");
	this.shape_13.setTransform(43.6,80.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#13B0B1").s().p("AAegrIAeAAIAZAbIgEAEIgzA4gAhPgKIgGgGIAbgbIAcAAIAABWg");
	this.shape_14.setTransform(43.6,82.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AAaDxIg9AAIgbAAIgLgGIAAhDQhRgVg+hAIgXgbIAAgFIgOhTQgGgwABgpQAAgaAFgYQAYgFAegLQAjgKATgIIAagPQgFAIgJAJQgbAigoAaQAogMAigXQAYgPAdgvQAKAYAGAMQAPAeARAUIAQAQQgPgegIgfQAgAcBAALIAVADQgLgNgKgQIgTgiQAPAJAWAEIBKAMQAkAJAcAbIAJAIQAVAYAMAdQAMAmgDA5IgDAdQAhg1AKg/QAGAHADANIADALIACAKQACAZgKAVQgQAbgjAPIgKAEQgOAVgSATQhDBFhYASQgfAHgiAAQgoAAglgJQAlAJAoAAQAiAAAfgHIAAA/IgOAIgAj0AxQgXgKgTgPQgcgXgIgeQgFgVAFgPQACgJAJgGIAIgGQAMBLAvA8IAAABg");
	this.shape_15.setTransform(44.1,54);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AkBDAIgBgBQgCgYgIgbIgVhFQgLgnABggQACgnAUgaQALgNATgMIAjgVQAZgMAKgIIAGgDQAigZAHgdQAHAbgBAbQAAARgFASQAMgRAIgSQAMgbADgfQAJAdAGAdQAHAhAEAgIgBhBIAAgDIABADQArA5A6AmQgFgZgNgXQgNgWgSgTIDZA2QAaAGAOAHQAVAIAOAOQAKAMAGAUQAEAKAEAVIgDAbQgKA/ggA2IACgeQAEg6gNglQgLgegWgWIgJgIQgcgbgjgJIhKgNQgXgDgPgJIATAiQAKAQALANIgVgDQhAgLgggcQAIAfAPAcIgPgPQgSgUgPgdQgGgMgJgYQgeAvgXAPQgjAXgoALQAogZAcgiQAIgJAFgIIgaAPQgTAIgjAKQgdAKgYAGQgFAYgBAZQgBApAGAwIAOBTIgEAAg");
	this.shape_16.setTransform(45.4,39.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("Ak2CQQgEgcgBgfQABhDAXg4IghA3QABggAMgeQAHgYANgWQAfg1AxgbQAVgMAYgHQAZgRAagJQgcgFgdAGQAngLAoACQAcABAeAJQADgMAKgKQAJgLAQgHQAGgDAHgCQAngOAhgGQgcAGgbATQgKAHgKAKQBgAFBMA9QAaAPAdAYQAoAhAWA2QASAxAEBDIgHgTQgEgOgJgLQAEAcAAAeQAAAOgCAOQgDgUgFgMQgFgUgLgMQgOgNgUgJQgOgGgbgHIjZg0QASASANAVQANAYAGAZQg7gngrg4IgBgCIAAACIABBBQgEgggHghQgGgcgJgdQgDAegMAbQgIASgLARQAEgSAAgRQABgbgHgbQgHAdghAZIgHADQgKAIgZANIgjAUQgTALgLAOQgUAagBAnQgCAgALAnIAVBFQAIAcACAXQgug9gMhLg");
	this.shape_17.setTransform(44.8,30.9);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_6
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape_18.setTransform(46.1,43.9,0.459,0.459);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_19.setTransform(46.1,43.9,0.459,0.459);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFCC00").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_20.setTransform(46.1,44);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]}).wait(1));

	// Layer_5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(255,255,255,0.02)").s().p("Al8KYQh0AAhShQQhShQAAhxIAAsNQAAhxBShQQBShQB0AAIL5AAQB0AABSBQQBSBQAABxIAAMNQAABxhSBQQhSBQh0AAg");
	this.shape_21.setTransform(46.2,44.2,1.056,1.056);

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy13, new cjs.Rectangle(-23.6,-26,139.6,140.4), null);


(lib.Symbol1copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ak6IlQhgAAhDhCQhEhCAAheIAAqFQAAheBEhCQBDhCBgAAIJ1AAQBgAABDBCQBEBCAABeIAAKFQAABehEBCQhDBChgAAg");
	mask.setTransform(46.1,44);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F7F8F7").s().p("AgwADQgNgFgEgKQBCgCBBACQgEAJgOAGQgTALgdgBQgbABgVgLg");
	this.shape.setTransform(49.2,66.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#561807").s().p("Ag/ADQgQgHgFgKIARgCQAFAKANAGQAUAJAdAAQAcAAASgJQAPgIAEgIIAUAAQgHAKgQAJQgbAOgjAAQglAAgagOg");
	this.shape_1.setTransform(49.4,67.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAxgBQAPgUAQgLQgCAGAAAGQAAAOAKAJQAIAIAMABIADAAQAIAAAHgDQAFgCAEgEQAKgJAAgOQAAgKgFgIQANAFAPAKIAGAGIgGAPQgVApgdAAQgeAAgngogAimgCIgHgPQADgCAEgEQAOgKANgFQgFAIAAAKQAAAOAKAJQAEAEAFACQAHADAIAAIADAAQAMgBAIgIQAKgJAAgOIgBgMQAPALAPAUQgnAogdAAQgeAAgUgpg");
	this.shape_2.setTransform(47.8,47.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E0D3BC").s().p("ABZAFQgCgCAAgDQAAAAAAgBQAAAAABgBQAAAAAAgBQAAgBABAAQACgCADAAQABAAAAAAQABAAABAAQAAABABAAQAAAAABABQADACAAACQAAADgDACQgCACgDAAQgDAAgCgCgAhjAFQgCgCAAgDQAAAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAQADgCADAAQAAAAABAAQABAAAAAAQABABAAAAQABAAAAABQABAAAAABQAAABABAAQAAABAAAAQAAABAAAAQAAADgCACQgBACgDAAQgDAAgDgCg");
	this.shape_3.setTransform(47.8,45.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("ABsAcQgMgBgIgIQgKgKAAgNQAAgGACgGQAOgJAOgBQAJgCAJACIAOAEQAFAIAAAKQAAANgKAKQgEAEgFACQgHADgIAAIgDAAgABZgBQAAABgBAAQAAAAAAAAQAAABAAAAQAAABAAABQgBADACACQADACADAAQACAAACgCQADgCAAgDQAAgDgDgBQAAAAgBgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgDAAgDACgAhuAcQgIAAgHgDQgFgCgEgEQgKgKAAgNQAAgKAFgIIAOgEQAIgCAKACQAPABAOAJIABAMQAAANgKAKQgIAIgMABIgDAAgAhjgBQAAABAAAAQgBAAAAAAQAAABAAAAQAAABAAABQAAADABACQADACADAAQADAAABgCQACgCAAgDQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQAAAAAAgBQgBAAAAAAQgBgBgBAAQAAAAgBAAQgDAAgDACg");
	this.shape_4.setTransform(47.8,45.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(23,10,4,0.298)").ss(0.3,1,1).p("Ag7gOQAdgFAcAEQAaAEAXAKQAXAIAJAKQgHgHgYgHQgZgGgfADQggABgbAIQgOAAAEgLQACgKAQgCg");
	this.shape_5.setTransform(58.3,38,0.863,0.863,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#311305").s().p("AAwADQgZgGgfADQggABgbAIQgOAAAEgLQACgKAQgCQAdgFAcAEQAaAEAXAKQAXAIAJAKQgHgHgYgHg");
	this.shape_6.setTransform(58.3,38,0.863,0.863,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("rgba(23,10,4,0.298)").ss(0.3,1,1).p("Ag7gOQAdgFAcAEQAaAEAXAKQAXAIAJAKQgHgHgYgHQgZgGgfADQggABgbAIQgOAAAEgLQACgKAQgCg");
	this.shape_7.setTransform(36.9,38,0.863,0.863);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#311305").s().p("AAwADQgZgGgfADQggABgbAIQgOAAAEgLQACgKAQgCQAdgFAcAEQAaAEAXAKQAXAIAJAKQgHgHgYgHg");
	this.shape_8.setTransform(36.9,38,0.863,0.863);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(66,66,66,0.098)").ss(0.3,1,1).p("Aj5idQgCgEgBgFQgehTgBgmQAAglANgJQANgKAEglQAEgmBvgXQAPgCAOgCQBfgPBSARQBhATArADQArABAJAkQAKAhAFAxQAHBKgaA3QgIAUgGATQAEgCAFgCQANgFAMAGQANAJABATQAEAbgFAaQgGAcgNAXQgOAXgXARQgGAFgHAEADag8IAAgBQAAgDACgCIgFhXIgFhOQgBgWgEgNQgFgTgNgIQgOgKgZAEQgMADgcAKQhAAahFACQgrABgpgIQgagEgagJQgJgEgJAFQgKAEgFAJQgIANgCAZQgEArgIBIQgBASgDAUQAAANABAMQgEgjgahKQgRAJgJAWQgMAZACAeQACAcAOAYQAPAWAaAPQAIAEAHACQAAgEAAgEQgEgSgBgmQgBgCAAgCQAAABABAAQAAgCgBgDAjbgsIAAgBQAAgCAAgBAhADMQgCgBgCgBQgFgEgFgDQgIgDgXgJQgFgDgHgCQgcgLgNgKQgXgQgMgfQgKgbgCgVQAAgRgEgVAhADMQAAACAAADQAAAFABAHQABAMgBAJQgDAegNATQgLAPgTAKQgBACgCABQgEABgFADQgFABgGADQgJACgMAEQgiAKhGARQg2ASgfAcQgCADgDACQgQARgNAZADag8IAAABQgCAXAEA6QAAAMAAAKQgCAkgNAWQgMAUgdATQgjAUgQAMQgdAWgSAMQgBAHAAAHQAAAMAAAJQAAADAAACQADAaAMASQALARAWALQADAAADACQAGACAIAEIAAAAQAKADAMADQAiAKBHARQA5AUAfAfQARARAMAZABCDPQgIAGgFADQgFACgEADQgiARgbgJQgLgCgKgGQgGgDgEgEQgJgFgHgFADyiGQgOAngKAj");
	this.shape_9.setTransform(48.5,56.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E58349").s().p("AgRATQgSgIgPgNIgGgHIgIgLIAAgGIAPAKIAKAHQALAHAKACQAcAHAhgQIAJgFIANgJIgBAOIAAAUIABAGIgDABQgTAIgVABQgVgBgSgHg");
	this.shape_10.setTransform(48.5,79.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#8F1721").s().p("Al4gUQANgYAQgRQADgCACgDQAfgdA2gSQBGgQAigLIAVgGIALgEIAJgEIADgCQAPAHAaADQAuAFBOgFIA2gFIAJgBIAOAGIABAAQAJAEAMACQAiALBHAQQA5AUAfAgQAQARANAYQjACxi7AAQi9AAi5ixg");
	this.shape_11.setTransform(48.5,103.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EE9B69").s().p("AhHE5QgZgEgQgHQAUgKAKgPQAOgTACgeQABgJgBgMIAAgMIAIAMIAGAGQAPAPASAIQATAHAUAAQAVAAATgIIADgCQADAaAMASQALARAVALIAHACIgJABIg2AFQgoACgeAAQggAAgXgCgAgUDXQgKgCgLgGIgKgHIgPgKIgFgCIgKgHIgegMIgNgFQgcgLgNgKQgWgQgNgfQgKgbgBgVQgBgRgDgVIgBgIQgDgSgCgmIgBgEIABABIgBgFIABAFIgBgBIABAEQACAmADASIABAIIgPgGQgagOgQgXQgOgYgBgcQgDgeANgZQAIgWARgJQAbBKADAjIAAADIAAABIAAgBIAAgDIAAgZIAEgmQAIhIADgrQACgZAIgNQAGgJAJgEQAKgFAJAEQAaAJAaAEQAoAIArgBQBFgCBAgaQAcgKANgDQAYgEAOAKQAOAIAEATQAEANACAWIAEBOIAFBXIgBAFIAAABIAAABIgBAYIACA5IAAAWQgCAkgNAWQgMAUgdATQgiAUgRAMQgcAWgTAMIgNAJIgJAFQgWALgTAAQgKAAgKgDgADZAeIAAAAgADZAIIgCg5IABgYIAAgBQAJgjAOgnIAJgEQANgFANAGQAMAJABATQAEAbgFAaQgFAcgOAXQgOAXgXARIgNAJIAAgWg");
	this.shape_12.setTransform(48.7,57.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#351004").s().p("Aj1gmQAFgsAWgRQAPgMAlgFQAggEASgBIAHAAQAegBAZADQAXADAqAOQAtANAWADQAmAGAggKQAkgMAPgdQAyA8AAA3QAAAvgqA7IgEhPQgBgUgEgNQgFgTgNgIQgPgKgYAEQgMACgdALQg/AZhFACQgrACgpgIQgagFgagJQgJgDgJAEQgKAEgFAJQgIAOgCAXQgEArgIBJQgihSAMhYg");
	this.shape_13.setTransform(49.2,31.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#451100").s().p("Aj1BdIgDgJQgehTgBglQAAglANgJQANgKAEglQAEgmBvgXIAdgEQBfgPBSARQBgATAsADQArABAIAkQALAhAFAxQAHBJgaA3IgOAnQgOAngKAjIAAgBIABgFIgFhXQAqg6AAgvQAAg3gyg8QgPAcgkAMQggAKgmgFQgWgDgtgOQgqgNgXgDQgZgEgeACIgHAAQgSABggAEQglAEgPAMQgWASgFArQgMBYAiBSIgEAmIABAZQgEgjgahKg");
	this.shape_14.setTransform(48.1,31);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(255,255,255,0.02)").s().p("Ak7IlQhfAAhEhCQhDhCAAheIAAqGQAAhdBDhCQBEhCBfAAIJ2AAQBgAABDBCQBEBCAABdIAAKGQAABehEBCQhDBChgAAg");
	this.shape_15.setTransform(46.2,44.1);

	var maskedShapeInstanceList = [this.shape_15];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy8, new cjs.Rectangle(-8.4,-10.8,109.2,109.8), null);


(lib.Symbol1copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ak6IlQhgAAhDhCQhEhCAAheIAAqFQAAheBEhCQBDhCBgAAIJ1AAQBgAABDBCQBEBCAABeIAAKFQAABehEBCQhDBChgAAg");
	mask.setTransform(46.1,44);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("Ag4ADIgBgEQgBgXAcAEQA3gGAfAmQAEAFgDAFQg1geg8ALg");
	this.shape.setTransform(59.9,39.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("Ag4AMQAfgmA3AFQAdgDgBAXQAAACgCACQg8gMg0AfQgDgGADgEg");
	this.shape_1.setTransform(30,39.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgPARQgVgRAGgWQALAjAigLQAIgCACgJQABgHAFgFQAIAegeAMQgGACgEAAQgIAAgGgGg");
	this.shape_2.setTransform(43.9,55.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AgbAbQgKgLAAgQQAAgGACgFQAAADADACQAHAGAHAAQAJAAAEgGQAGgFAAgHQAAgJgGgFIgEgEQAFgCAFAAQAPAAAMAMQAKALAAAPQAAAQgKALQgMALgPAAQgQAAgMgLg");
	this.shape_3.setTransform(60.2,46.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AgiAjQgPgPAAgUQAAgUAPgPQAPgOATAAQAUAAAPAOQAPAPAAAUQAAAUgPAPQgPAPgUAAQgTAAgPgPgAgoAJQAAARAKAKQANAMAQAAQAPAAALgMQALgKAAgRQAAgOgLgMQgLgLgPAAQgGAAgFABIAFAEQAGAGAAAIQAAAIgGAEQgFAGgJAAQgHAAgGgGQgDgCgBgCQgBAEgBAGg");
	this.shape_4.setTransform(60.4,45.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AgbAbQgKgLAAgQQAAgGACgFQAAADADACQAGAGAIAAQAHAAAGgGQAFgFAAgHQAAgJgFgFIgFgEIAKgCQAQAAALAMQALALAAAPQAAAQgLALQgLALgQAAQgPAAgMgLg");
	this.shape_5.setTransform(28.5,46.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AgjAjQgOgPAAgUQAAgUAOgPQAPgOAUAAQAVAAAPAOQAOAPAAAUQAAAUgOAPQgPAPgVAAQgUAAgPgPgAgoAJQAAARALAKQAMAMAQAAQAPAAALgMQALgKAAgRQAAgOgLgMQgLgLgPAAIgLABIAEAEQAHAGAAAIQAAAIgHAEQgFAGgHAAQgIAAgHgGQgCgCAAgCQgCAEgBAGg");
	this.shape_6.setTransform(28.8,45.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AhKAFQgUgJgGgOQBpARBggSQgGANgWALQgfAPgqAAQgrAAgfgPg");
	this.shape_7.setTransform(44.3,62.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AhfAFQgYgLgKgQIAcgCQAGAOAVAKQAeAPAsAAQAqAAAegPQAWgLAGgOIAeABQgKARgZAMQgoAUg3ABQg3gBgogUg");
	this.shape_8.setTransform(44.4,63.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AlXhhQgFghAAgiQAAhLAahAQgPAagWAlQACglAMgiQAJgbAOgZQAjg7A3gfQAXgNAbgIQAcgSAegLQgggGggAHQAsgMAsACQAgACAiAKQADgOALgMQAKgLASgJQAHgDAIgDQArgPAmgGQghAGgdAVQgLAJgLALQBsAFBUBEQAeARAhAbQAtAlAXA9QAWA4AEBLQgEgLgEgKQgGgQgKgMQAFAfAAAiQAAAQgBAQQgBAPgCAPAkQA4QgLg7gEgiQgHg2ABguQABgdAFgbQAbgGAhgMQAogLAUgKQAQgHAOgJQgGAJgJAKQgfAmgtAdQAtgNAngaQAagRAhg0QALAaAGAOQARAhAUAXQAIAJAJAIQgRghgIgjQAjAgBIAMQAMACAMACQgMgPgMgSQgMgWgJgQQARAKAZAEQAbAFA4AJQAoAKAgAeQAFAFAEAEQAZAbANAhQAOAqgEBBQgCANgBATQAkg8AMhGQAHAIADAOQACAHACAGAlXhhQgFACgFAEQgJAHgDAKQgGAQAGAYQAJAiAfAaQAVARAaALIABABQACABADAAAhWC7QhagXhGhIQgNgPgMgPQAAgDgBgDAkVA3QgBgBAAAAQgzhEgOhTAhWI+IkMAAIAAjLQAAhWBYALQAzAFAxADIAAA+IA5g8QAGAAAGAAIA3A9IABABIBCAAIABAAAhnErIAGAHAhnErIgGAHAhWEHIANAGIgeAeAhJENIAfAAIAABiIAAABIABAAAhWEHIhQgqIAABTAgqFwIAAAJIgsDFICcAAAhWEHIAAhMAFqg4QACAcgMAYQgRAegnARQgGACgGACQgQAXgUAWQhLBOhiAUQgkAHgmAAQgtAAgqgKABLEEIBMgnIAABPIAABCIg7g+ABTEwQAEAAAFAAABXErIgEAFIg5BAIgBAAABXErIAFAFAAaENIAgAAIAdAeAAaFwIAAAAIAAAJIAsDFABLEEIgRAJAAaENIAABjACXEsQAygEAygFQBWgGAABWIAADLIkLAAABLC+IAABGAgqENIBEAA");
	this.shape_9.setTransform(44.5,53.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AggAyIgBgBIAAhiIBDAAIAABiIAAABg");
	this.shape_10.setTransform(43.7,85.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("AhNBnIArjEIAAgJIACAAIBCAAIAAAAIAAAJIAsDEg");
	this.shape_11.setTransform(43.7,100.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#129293").s().p("ABkAKIgFgEIgdgdIARgJIBMgoIAABPIAABBgAieAKIAAhSIBQArIANAGIgeAdIgGAHIg5A7g");
	this.shape_12.setTransform(43.7,83.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("AAigwIAhAAIAdAeIgFAFIg5A/gAhZgLIgGgHIAegeIAfAAIAABhg");
	this.shape_13.setTransform(43.7,85.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#01627F").s().p("ABPCOIgsjFIAAgIIAAgBIA5g/IAJgBIA7A+IAAhBIBkgJQBWgGAABVIAADLgAlZCOIAAjLQAAhVBYALQAzAFAxACIAAA+IA5g8IAMAAIA3A+IAAABIAAAIIgsDFg");
	this.shape_14.setTransform(43.6,97);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AAdEOIhEAAIgfAAIgMgGIAAhMQApAKAtAAQAnAAAjgHIAABGIgQAJgAhSC8QhagXhHhIIgZgeIgBgGQgKg7gFghQgHg3ABgtQABgdAGgcQAagGAigLQAngMAVgKQAQgHANgJQgFAKgKAKQgfAlgtAdQAtgNAngaQAagRAig0IARAoQAQAiAUAWIASASQgRgigJgjQAkAgBIAMIAXAEQgMgOgLgTIgWglQASAJAZAEIBTAOQAoAKAfAeIAKAJQAYAbANAiQAOApgEBBIgDAgQAlg8ALhGQAHAIAEAOIADANIACAMQADAbgMAYQgSAegnARIgLAEQgQAXgVAWQhLBOhiAUQgjAHgnAAQgtAAgpgKgAkSA3QgagLgVgRQgfgagKgiQgFgYAFgPQADgLAKgHIAJgGQAOBUA0BDIAAABg");
	this.shape_15.setTransform(44.2,53.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AkhDXIgBgBQgCgagJgfIgYhOQgMgrACgkQABgsAXgdQAMgPAVgNIAogXQAbgPAMgIIAHgEQAmgcAIghQAIAegBAfQgBATgEAUQANgTAIgUQAOgeADgiQAKAgAHAgQAIAlAFAlIgChKIAAgCIACACQAvBABCAsQgGgdgPgaQgOgZgUgUIDzA7QAeAIAPAGQAYAKAPAQQAMANAHAXQAEAMAFAXIgDAeQgMBGgkA9IADghQAEhBgOgqQgNghgZgaIgJgJQgggegogKIhTgOQgZgEgRgKIAVAmQAMASAMAPIgYgEQhIgMgjggQAIAjARAgIgRgQQgUgXgRghIgRgoQghA0gaARQgnAagtANQAtgdAfgmQAJgKAGgJQgOAJgQAHQgUAKgoALQghAMgbAGQgFAbgBAcQgBAuAHA2QAEAiALA8IgFgBg");
	this.shape_16.setTransform(45.7,37.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AlcCiQgFghAAghQAAhMAag/IgkA+QABgkANghQAIgcAPgYQAig8A4geQAXgOAbgIQAbgSAfgLQgggGggAHQArgMAsACQAhACAiAKQACgOALgLQALgMASgJIAOgFQAsgQAmgGQghAGgdAVQgMAJgLALQBsAFBVBFQAeAQAgAbQAtAlAYA+QAVA2AEBLIgIgVQgFgQgKgLQAFAeAAAjIgCAfQgEgXgEgNQgHgXgMgNQgPgQgYgJQgPgHgegHIj0g7QAUAUAPAYQAOAaAHAdQhCgsgvg+IgCgDIAAADIACBIQgFgjgJglQgGghgKggQgEAigNAfQgJATgNATQAFgTAAgTQACgggJgeQgHAhgnAdIgGADQgMAJgbAOIgoAXQgVANgMAOQgXAegCAsQgCAkANArIAYBOQAJAfACAaQg0hEgOhUg");
	this.shape_17.setTransform(45,27.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(255,255,255,0.02)").s().p("Ak7IlQhfAAhEhCQhDhCAAheIAAqGQAAhdBDhCQBEhCBfAAIJ2AAQBgAABDBCQBEBCAABdIAAKGQAABehEBCQhDBChgAAg");
	this.shape_18.setTransform(46.2,44.1);

	var maskedShapeInstanceList = [this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-8.4,-10.8,109.2,109.8), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AlWIlQhVAAg8g8Qg9g9AAhWIAAqrQAAhWA9g9QA8g8BVAAIKsAAQBWAAA8A8QA9A9AABWIAAKrQAABWg9A9Qg8A8hWAAg");
	mask.setTransform(46.1,43.9);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AgyADQgBgCAAgCQgBgVAaAEQAxgFAbAhQAEAFgDAFQgvgbg2AKg");
	this.shape.setTransform(58.1,41.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgyAKQAcghAxAFQAagDgBAUIgBAEQg2gLgvAcQgCgFACgFg");
	this.shape_1.setTransform(31.4,41.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgNAPQgUgPAGgTQALAfAdgKQAIgCABgIQABgGAEgFQAIAbgcALQgEABgDAAQgIAAgFgFg");
	this.shape_2.setTransform(43.8,55.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AgYAYQgJgJAAgPIABgJIAEAFQAFAEAHAAQAIAAAEgEQAEgFAAgHQAAgHgEgGIgEgDQAEgBAEAAQAPAAAKAKQAJAKAAANQAAAPgJAJQgKAKgPAAQgNAAgLgKg");
	this.shape_3.setTransform(58.3,47.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AgfAgQgMgOAAgSQAAgSAMgNQAOgNARAAQATAAAMANQANANAAASQAAASgNAOQgMANgTgBQgRABgOgNgAgjAJQAAAOAJAKQALAJAOABQANgBALgJQAJgKgBgOQABgOgJgKQgLgKgNAAQgFAAgEABIAEAEQAFAFAAAHQAAAHgFAEQgFAFgHAAQgHAAgGgFIgDgEIgBAKg");
	this.shape_4.setTransform(58.5,46.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AgYAYQgJgJAAgPQAAgEABgFQABACADADQAGAEAGAAQAHAAAEgEQAFgFAAgHQAAgHgFgGIgDgDIAIgBQAPAAAJAKQAKAKAAANQAAAPgKAJQgJAKgPAAQgOAAgKgKg");
	this.shape_5.setTransform(30.1,47.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AgfAgQgNgOAAgSQAAgSANgNQAOgNASAAQASAAAMANQAOANAAASQAAASgOAOQgMANgSgBQgSABgOgNgAgjAJQAAAOAJAKQAKAJAPABQANgBAJgJQALgKAAgOQAAgOgLgKQgJgKgNAAIgKABIAEAEQAGAFAAAHQAAAHgGAEQgFAFgGAAQgHAAgGgFQgCgCgBgCQgBAEAAAGg");
	this.shape_6.setTransform(30.3,46.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AhBAEQgSgIgGgMQBdAPBWgQQgGAMgTAJQgbAOgmAAQgnAAgagOg");
	this.shape_7.setTransform(44.2,61.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AhUAEQgWgJgJgOIAZgCQAGAMASAJQAaANAoAAQAlAAAbgNQATgKAGgMIAaAAQgIAPgWALQgkATgxAAQgxAAgjgTg");
	this.shape_8.setTransform(44.3,62.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("AkyhXQgEgcAAgfQAAhDAXg5QgOAXgSAhQABghALgeQAHgYANgWQAfg1AxgbQAVgMAYgHQAZgRAbgJQgdgFgcAGQAmgLAoACQAdABAeAJQACgMAKgKQAJgLAQgHQAGgDAHgCQAngOAigGQgdAGgaATQgLAHgKAKQBhAFBLA9QAbAPAcAYQApAhAVA2QATAyAEBDQgEgKgEgJQgEgOgJgLQAEAcAAAeQAAAOgCAOQAAAOgCANAjyAyQgKg1gEgeQgGgwABgpQAAgaAFgYQAYgFAegLQAjgKATgIQANgHANgIQgFAIgJAJQgbAigoAaQAogMAigXQAYgPAdgvQAKAYAGAMQAPAeARAUQAHAIAJAIQgPgegIgfQAgAcBAALQAKABALACQgLgNgKgQQgLgTgIgPQAPAJAWAEQAZAEAxAIQAkAJAcAbQAEAEAFAEQAVAYAMAdQAMAmgDA5QgCAMgBARQAhg1AKg/QAGAHADANQACAGABAFAkyhXQgEADgEADQgJAGgCAJQgFAPAFAVQAIAeAcAXQATAPAXAKIAAABQAAgBAAAAQgvg8gMhLgAhMCnQhRgVg+hAQgMgNgLgOQAAgCAAgDAj3AxQACABADAAAiUEOIAAA4IAzg1QAFAAAFAAIAxA2IACABIA6AAIABAAIAAAIIAnCwAhMIAIjvAAIAAi2QAAhMBOAKQAtAEAsACAhBDwIgbAbIgFAGAhMDqIALAGIAbAAIA9AAIAdAAIAaAbIgFAEQAFgBAEAAAhcELIAFAGAhMDqIAAhDAhMDqIhIglIAABJAFCgyQACAZgKAVQgQAbgjAPQgFACgFACQgOAVgSATQhDBFhYASQgfAHgiAAQgoAAglgJABOELIAEADAAXFIIAAAAAAXFIIgBAAAgmFHIAAABIACAAAgmFIIAAAIIgmCwICKAAABJEPIgyA5AAXDwIAABYAgmDwIAABXABCDoIgOAIACGEMIAAA6Ig0g4ACGEMQAtgEAsgFQBNgFAABMIAAC2IjuAAABCCpIAAA/IBEgjIAABH");
	this.shape_9.setTransform(44.4,54.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#07BABB").s().p("AhFBcIAnivIAAgIIACAAIA6AAIAAAAIAAAIIAoCvg");
	this.shape_10.setTransform(43.6,96.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#10DADB").s().p("AgcAsIgCgBIAAhWIA9AAIAABXIgBAAg");
	this.shape_11.setTransform(43.6,82.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("ABGB+IgoivIAAgHIAAgBIAzg4IAJgBIA0A4IAAg7IBZgIQBNgGAABNIAAC0gAkzB+IAAi0QAAhNBOAKQAtAFAsACIAAA4IAzg2IAKAAIAxA3IAAABIAAAHIgnCvg");
	this.shape_12.setTransform(43.6,92.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#129293").s().p("ABZAJIgEgEIgagaIAPgIIBDgjIAABGIAAA7gAiMAJIAAhJIBHAmIALAFIgbAaIgFAGIgyA2g");
	this.shape_13.setTransform(43.6,80.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#13B0B1").s().p("AAegrIAeAAIAZAbIgEAEIgzA4gAhPgKIgGgGIAbgbIAcAAIAABWg");
	this.shape_14.setTransform(43.6,82.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AAaDxIg9AAIgbAAIgLgGIAAhDQhRgVg+hAIgXgbIAAgFIgOhTQgGgwABgpQAAgaAFgYQAYgFAegLQAjgKATgIIAagPQgFAIgJAJQgbAigoAaQAogMAigXQAYgPAdgvQAKAYAGAMQAPAeARAUIAQAQQgPgegIgfQAgAcBAALIAVADQgLgNgKgQIgTgiQAPAJAWAEIBKAMQAkAJAcAbIAJAIQAVAYAMAdQAMAmgDA5IgDAdQAhg1AKg/QAGAHADANIADALIACAKQACAZgKAVQgQAbgjAPIgKAEQgOAVgSATQhDBFhYASQgfAHgiAAQgoAAglgJQAlAJAoAAQAiAAAfgHIAAA/IgOAIgAj0AxQgXgKgTgPQgcgXgIgeQgFgVAFgPQACgJAJgGIAIgGQAMBLAvA8IAAABg");
	this.shape_15.setTransform(44.1,54);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AkBDAIgBgBQgCgYgIgbIgVhFQgLgnABggQACgnAUgaQALgNATgMIAjgVQAZgMAKgIIAGgDQAigZAHgdQAHAbgBAbQAAARgFASQAMgRAIgSQAMgbADgfQAJAdAGAdQAHAhAEAgIgBhBIAAgDIABADQArA5A6AmQgFgZgNgXQgNgWgSgTIDZA2QAaAGAOAHQAVAIAOAOQAKAMAGAUQAEAKAEAVIgDAbQgKA/ggA2IACgeQAEg6gNglQgLgegWgWIgJgIQgcgbgjgJIhKgNQgXgDgPgJIATAiQAKAQALANIgVgDQhAgLgggcQAIAfAPAcIgPgPQgSgUgPgdQgGgMgJgYQgeAvgXAPQgjAXgoALQAogZAcgiQAIgJAFgIIgaAPQgTAIgjAKQgdAKgYAGQgFAYgBAZQgBApAGAwIAOBTIgEAAg");
	this.shape_16.setTransform(45.4,39.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("Ak2CQQgEgcgBgfQABhDAXg4IghA3QABggAMgeQAHgYANgWQAfg1AxgbQAVgMAYgHQAZgRAagJQgcgFgdAGQAngLAoACQAcABAeAJQADgMAKgKQAJgLAQgHQAGgDAHgCQAngOAhgGQgcAGgbATQgKAHgKAKQBgAFBMA9QAaAPAdAYQAoAhAWA2QASAxAEBDIgHgTQgEgOgJgLQAEAcAAAeQAAAOgCAOQgDgUgFgMQgFgUgLgMQgOgNgUgJQgOgGgbgHIjZg0QASASANAVQANAYAGAZQg7gngrg4IgBgCIAAACIABBBQgEgggHghQgGgcgJgdQgDAegMAbQgIASgLARQAEgSAAgRQABgbgHgbQgHAdghAZIgHADQgKAIgZANIgjAUQgTALgLAOQgUAagBAnQgCAgALAnIAVBFQAIAcACAXQgug9gMhLg");
	this.shape_17.setTransform(44.8,30.9);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_6
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape_18.setTransform(46.1,43.9,0.459,0.459);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_19.setTransform(46.1,43.9,0.459,0.459);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFCC00").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_20.setTransform(46.1,44);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]}).wait(1));

	// Layer_5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(255,255,255,0.02)").s().p("Al8KYQh0AAhShQQhShQAAhxIAAsNQAAhxBShQQBShQB0AAIL5AAQB0AABSBQQBSBQAABxIAAMNQAABxhSBQQhSBQh0AAg");
	this.shape_21.setTransform(46.2,44.2,1.056,1.056);

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-23.6,-26,139.6,140.4), null);


(lib.Tween25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol9copy();
	this.instance.parent = this;
	this.instance.setTransform(-3.9,-7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape.setTransform(-0.3,-0.6,1.206,1.206);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_1.setTransform(-0.3,-0.6,1.206,1.206);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_2.setTransform(-0.3,-0.6,2.624,2.624);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("Al8KYQh0AAhShQQhShQAAhxIAAsNQAAhxBShQQBShQB0AAIL5AAQB0AABSBQQBSBQAABxIAAMNQAABxhSBQQhSBQh0AAg");
	this.shape_3.setTransform(0,0,2.772,2.772);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AuCWhQjgAAifieQififAAjiIAA8DQAAjiCfifQCfieDgAAIcEAAQDhAACfCeQCfCfAADiIAAcDQAADiifCfQifCejhAAg");
	this.shape_4.setTransform(-0.3,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183.2,-184.1,366.4,368.3);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy8();
	this.instance.parent = this;
	this.instance.setTransform(355.5,-15.4,2.624,2.624,0,0,0,44.5,45.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape.setTransform(359.6,-18.4,1.2,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_1.setTransform(359.6,-18.4,1.2,1.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333366").s().p("AroUoQjuAAipioQioipAAjuIAA3RQAAjuCoipQCpioDuAAIXRAAQDuAACpCoQCoCpAADuIAAXRQAADuioCpQipCojuAAg");
	this.shape_2.setTransform(359.6,-18.4,1.2,1.2);

	this.instance_1 = new lib.Symbol1copy6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-363.7,-15.4,2.624,2.624,0,0,0,44.5,45.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape_3.setTransform(-359.6,-18.4,1.2,1.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_4.setTransform(-359.6,-18.4,1.2,1.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333366").s().p("AroUoQjuAAipioQioipAAjuIAA3RQAAjuCoipQCpioDuAAIXRAAQDuAACpCoQCoCpAADuIAAXRQAADuioCpQipCojuAAg");
	this.shape_5.setTransform(-359.6,-18.4,1.2,1.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.instance_1},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-518,-176.8,1036.1,355);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween21("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-30.3,259.6,62.2);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween20("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.02,scaleY:1.02},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-198.9,-33.9,398,69.5);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy14();
	this.instance.parent = this;
	this.instance.setTransform(-4.4,3.3,2.624,2.624,0,0,0,44.5,45.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.76,scaleY:2.76},9).to({scaleX:2.62,scaleY:2.62},10).to({scaleX:2.76,scaleY:2.76},10).to({scaleX:2.62,scaleY:2.62},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183.1,-184.1,366.4,368.4);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(-4.1,3.3,2.624,2.624,0,0,0,44.6,45.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.76,scaleY:2.76},9).to({scaleX:2.62,scaleY:2.62},10).to({scaleX:2.76,scaleY:2.76},10).to({scaleX:2.62,scaleY:2.62},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183.1,-184.1,366.4,368.4);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy18();
	this.instance.parent = this;
	this.instance.setTransform(-3.8,3.6,2.624,2.624,0,0,0,44.5,45.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape.setTransform(0,0,1.206,1.206);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_1.setTransform(0,0,1.206,1.206);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_2.setTransform(0,0,2.624,2.624);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy17, new cjs.Rectangle(-159.1,-159.1,318.3,335.4), null);


(lib.Symbol1copy16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(634.8,335.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(363).to({_off:false},0).wait(36).to({startPosition:16},0).to({alpha:0,startPosition:5},9).wait(1));

	// Layer_13
	this.instance_1 = new lib.Symbol1copy17();
	this.instance_1.parent = this;
	this.instance_1.setTransform(639.5,320.2,1,1,0,0,0,0,8.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(339).to({_off:false},0).to({regX:0.1,scaleX:1.1,scaleY:1.1,x:639.6},24).wait(36).to({alpha:0},9).wait(1));

	// Layer_7
	this.instance_2 = new lib.Symbol1copy6();
	this.instance_2.parent = this;
	this.instance_2.setTransform(275.5,547.2,2.624,2.624,0,0,0,44.5,45.4);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(314).to({_off:false},0).to({x:635.7,y:315.2},19).to({_off:true},6).wait(11).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_12
	this.instance_3 = new lib.Symbol1copy_1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(429.2,652.8,1,1,0,0,0,190.5,64.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(279).to({_off:false},0).to({_off:true},35).wait(95));

	// Layer_11
	this.instance_4 = new lib.Symbol1copy16("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(281.5,332.9,1,1,0,0,0,41,60.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(245).to({_off:false},0).to({_off:true},34).wait(130));

	// Layer_10
	this.instance_5 = new lib.Tween22("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(638.4,322.1);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween23("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(638.4,322.1);
	this.instance_6.alpha = 0.699;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(175).to({_off:false},0).to({_off:true,alpha:0.699},10).wait(224));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(175).to({_off:false},10).wait(40).to({startPosition:0},0).to({alpha:0},6).to({_off:true},1).wait(177));

	// Layer_14
	this.instance_7 = new lib.Symbol2copy("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(639.9,312.2);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(140).to({_off:false},0).wait(29).to({startPosition:29},0).to({alpha:0,startPosition:34},5).to({_off:true},1).wait(234));

	// Layer_9
	this.instance_8 = new lib.Symbol8("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(751,100.5,1,1,0,0,0,0.8,0.8);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(140).to({_off:false},0).wait(29).to({startPosition:29},0).to({alpha:0,startPosition:34},5).to({_off:true},1).wait(50).to({_off:false,alpha:1,startPosition:35},0).to({alpha:0,startPosition:1},6).to({_off:true},1).wait(118).to({_off:false,alpha:1,mode:"independent"},0).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_3
	this.instance_9 = new lib.Symbol1copy14();
	this.instance_9.parent = this;
	this.instance_9.setTransform(635.5,315.4,2.624,2.624,0,0,0,44.5,45.4);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape.setTransform(639.5,311.6,1.206,1.206);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_1.setTransform(639.5,311.6,1.206,1.206);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_2.setTransform(639.5,311.6,2.624,2.624);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("Al8KYQh0AAhShQQhShQAAhxIAAsNQAAhxBShQQBShQB0AAIL5AAQB0AABSBQQBSBQAABxIAAMNQAABxhSBQQhSBQh0AAg");
	this.shape_3.setTransform(639.8,312.1,2.772,2.772);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AuCWhQjgAAifieQififAAjiIAA8DQAAjiCfifQCfieDgAAIcEAAQDhAACfCeQCfCfAADiIAAcDQAADiifCfQifCejhAAg");
	this.shape_4.setTransform(639.5,311.5);

	this.instance_10 = new lib.Symbol9copy();
	this.instance_10.parent = this;
	this.instance_10.setTransform(635.9,305.1);

	this.instance_11 = new lib.Tween25("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(639.8,312.2);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9}]},110).to({state:[{t:this.instance_9}]},7).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},58).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_10}]},57).to({state:[{t:this.instance_11}]},118).to({state:[{t:this.instance_11}]},6).to({state:[]},1).wait(52));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(110).to({_off:false},0).to({alpha:1},7).to({_off:true},58).wait(234));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(350).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_4
	this.instance_12 = new lib.Tween17("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,560.5);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(110).to({_off:false},0).to({alpha:1},7).to({startPosition:0},233).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_5
	this.questiontxt = new lib.questiontext_mccopy();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(606.6,43.4,1.641,1.641,0,0,0,0.1,-0.6);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(105).to({_off:false},0).to({alpha:1},12).wait(233).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_6
	this.instance_13 = new lib.Symbol1copy2("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640.1,311.7);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(60).to({_off:false},0).wait(38).to({startPosition:38},0).to({alpha:0,startPosition:4},6).to({_off:true},1).wait(304));

	// Layer_8
	this.instance_14 = new lib.Symbol7("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(642.1,102.7,1,1,0,0,0,0.1,0.8);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(60).to({_off:false},0).wait(38).to({startPosition:38},0).to({alpha:0,startPosition:4},6).to({_off:true},1).wait(245).to({_off:false,x:640.1,y:103.7,alpha:1,mode:"independent"},0).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_1
	this.instance_15 = new lib.Symbol1copy13();
	this.instance_15.parent = this;
	this.instance_15.setTransform(635.7,315,2.624,2.624,0,0,0,44.5,45.4);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(40).to({_off:false},0).to({alpha:1},7).to({regX:44.6,x:635.9},20).to({regX:44.5,x:635.7},283).to({alpha:0},6).to({_off:true},1).wait(52));

	// Layer_2
	this.questiontxt_1 = new lib.questiontext_mc();
	this.questiontxt_1.name = "questiontxt_1";
	this.questiontxt_1.parent = this;
	this.questiontxt_1.setTransform(606.6,58,1.641,1.641,0,0,0,0.1,-0.6);
	this.questiontxt_1.alpha = 0;
	this.questiontxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt_1).wait(20).to({_off:false},0).to({alpha:1},7).wait(78).to({alpha:0},7).to({_off:true},1).wait(296));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(580.8,297.2,1402,848);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;