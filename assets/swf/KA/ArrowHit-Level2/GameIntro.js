(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AAGgpIgyCgIhNAGIgtj3IA4gFIAgDBIBAjBIAzADIAqDDIAcjFIA8ACIgvD6IhMACg");
	this.shape.setTransform(119.4,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgyCCQgWgKgSgTQgSgSgJgaQgKgaAAgfQAAgfAKgaQAJgZASgTQASgSAWgKQAYgKAaAAQAaAAAXAKQAYALASATQASATAJAZQALAagBAdQABAegLAaQgJAZgSATQgRATgYALQgYAKgaAAQgaAAgYgKgAgfhSQgNAIgIANQgIANgEAQQgFAQAAAQQABAQAEAQQAEAQAJANQAIANANAHQANAIARAAQASAAANgIQAOgHAIgNQAIgNAFgQQAEgQAAgQQAAgPgEgRQgEgQgIgNQgJgNgNgIQgOgHgSAAQgSAAgNAHg");
	this.shape_1.setTransform(84.9,5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AhtA5IgCg5IgBgsIgBghIgCg2IA/gBIAAAlIAAAWIABARIAAAHIAAgHQABgGAGgIQAKgNAMgMQANgMAOgIQAPgJARgDQAVgBAQAKQAHAEAHAHQAHAGAGAKQAFALAEANQAEAOACAWIg3ASQgBgPgCgIQgBgIgDgGQgCgEgDgEQgDgEABgBQgLgFgIAAQgFAAgHAEQgGAFgHAGIgPAQIgOASIgNARIgMAQIABAhIABAeIABAaIABAYIg7AGIgDhMg");
	this.shape_2.setTransform(56.7,6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AhtA5IgCg5IgBgsIgBghIgCg2IA/gBIAAAlIAAAWIABARIAAAHIAAgHQABgGAGgIQAKgNAMgMQANgMAOgIQAPgJARgDQAVgBAQAKQAHAEAHAHQAHAGAGAKQAFALAEANQAEAOACAWIg3ASQgBgPgCgIQgBgIgDgGQgCgEgDgEQgDgEABgBQgLgFgIAAQgFAAgHAEQgGAFgHAGIgPAQIgOASIgNARIgMAQIABAhIABAeIABAaIABAYIg7AGIgDhMg");
	this.shape_3.setTransform(28.4,6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("ABDCIIADggQgWASgbAIQgbAIgYAAQgRAAgRgEQgQgFgMgJQgNgKgIgPQgIgOAAgUQAAgUAGgPQAGgQAKgLQAKgMAOgIQANgJAQgFQAQgFARgDQAQgCAQAAIAdABQANABAGACIgHgQQgFgLgGgIQgGgIgKgFQgJgFgMAAQgIAAgLACQgKADgOAGQgNAHgQAMQgQAMgXAUIgggnQAYgYAVgOQAVgNASgIQARgHARgDQAPgDANAAQAVAAASAIQASAHANAOQANANAJARQAJASAGAUQAFAUADAVQACAVAAAUQAAAWgCAaQgDAagGAigAgJADQgTAFgNAIQgNAJgHAMQgHAMAEAOQAEAOAHAEQAJAFALAAQANAAAPgFIAdgKQAPgHANgHIAVgMIABgUIgBgUIgUgFQgMgBgMAAQgUAAgSAEg");
	this.shape_4.setTransform(-3.8,5.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("Ag2CqQgYgKgSgTQgSgSgKgZQgKgaAAghQAAgfALgZQAMgZASgSQASgSAYgJQAXgJATAAQAUAAASAFQAMACAQAJQARAHAGANIACh2IA1AEIgBFcIg5AAIAAgbIAAgCIAAACQAAAEgHAEQgHAGgIADIgOAHQgHACgNACQgJADgOABQgcAAgYgJgAghgjQgNAGgKAMQgKALgFAPQgGAPAAAUQAAASAGAPQAFAQAKALQAKALAOAGQAOAGAQAAQAMAAANgEQANgEALgJQALgIAIgKQAIgMAEgNIAAgvQgDgNgJgMQgIgLgLgIQgMgJgNgFQgNgEgNAAQgPAAgOAHg");
	this.shape_5.setTransform(-51.8,1.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("Ag2CqQgYgKgSgTQgSgSgKgZQgKgaAAghQAAgfALgZQAMgZASgSQASgSAYgJQAXgJATAAQAUAAASAFQAMACAQAJQARAHAGANIACh2IA1AEIgBFcIg5AAIAAgbIAAgCIAAACQAAAEgHAEQgHAGgIADIgOAHQgHACgNACQgJADgOABQgcAAgYgJgAghgjQgNAGgKAMQgKALgFAPQgGAPAAAUQAAASAGAPQAFAQAKALQAKALAOAGQAOAGAQAAQAMAAANgEQANgEALgJQALgIAIgKQAIgMAEgNIAAgvQgDgNgJgMQgIgLgLgIQgMgJgNgFQgNgEgNAAQgPAAgOAHg");
	this.shape_6.setTransform(-85.7,1.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgyCCQgXgKgRgTQgSgSgJgaQgLgaAAgfQAAgfALgaQAJgZASgTQARgSAXgKQAYgKAaAAQAaAAAXAKQAZALARATQASATAKAZQAKAagBAdQABAegKAaQgKAZgSATQgRATgYALQgXAKgbAAQgaAAgYgKgAgfhSQgNAIgIANQgIANgFAQQgEAQAAAQQABAQAEAQQAEAQAJANQAJANAMAHQANAIARAAQASAAANgIQANgHAJgNQAIgNAFgQQAEgQAAgQQAAgPgEgRQgEgQgIgNQgJgNgNgIQgOgHgSAAQgSAAgNAHg");
	this.shape_7.setTransform(-117.2,5.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(3,1,1).p("AzDk/MAmHAAAQBsAABMBMQBMBMAABsIAAB3QAABshMBMQhMBMhsAAMgmHAAAQhsAAhMhMQhMhMAAhsIAAh3QAAhsBMhMQBMhMBsAAg");
	this.shape_8.setTransform(2.5,0.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AzDFAQhsAAhMhNQhMhLAAhsIAAh3QAAhsBMhMQBMhMBsAAMAmHAAAQBsAABMBMQBMBMAABsIAAB3QAABshMBLQhMBNhsAAg");
	this.shape_9.setTransform(2.5,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150.5,-38.4,302.6,76.9);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symboln1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6600").s().p("AkcNAIAAAAQgMgVAFgYIABgGIAAgCIAGgYQAdh6AdhpIAJghQAQg5AQg0IAAgCIAAABQANgqAHgmQAKgzAAgtQAChPgdg6QgchJhkgQQhngQi0AsQgUACgQgDQgbgEgSgPQgTgQgMgcQBDhAA/hHIAbgeQBLhaBFhiIAog7IA8heIAVgiQA5hcA2heIA5hlIASgiQAOgaARgVQAbgiAkgXQAqAeAgAyIAMASICRDiIALARICaDtIAWAiIABAAQA9BdBEBUIADADQBDBTBJBKIAGAGIAMALQgKAjgkADIAAAAIgBAAIgIAAIgMABQh0AGh/gLIgdgCQhygLg5A7IAAAAQgQAOgMASQgXAkgHA2QgDAVgBAYQgBA1AJA6QAIA0AQA5QAaBaAvBjQAQAhAKAeIAIAbQAcBwgzBQIgBACQgMARgPAPQgYAZgeAQIgEACIgBABQhAAfhEAUIgRAEQjCgChoi5g");
	this.shape.setTransform(0.4,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6600").s().p("AAYRBIgUAAQjegHh4jVQgZgtAMg0QA1jkA1isQA0irguhhQgvhikiBKQijAWglipQDLi+CdjvQCMjXB8jlQA4hnBgguIAEADQBLAoAxBOQCrELCtEIQCHDNCqChQgFB6hrAEQiOAJifgPQiggPgCCnQgDCoBlDUQBmDUhhCJQgsA+g9AfQhVAshcAXg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symboln1copy4, new cjs.Rectangle(-77.7,-108.9,155.5,218), null);


(lib.Symboln1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF9900").s().p("AkcNAIAAAAQgMgVAFgYIABgGIAAgCIAGgYQAdh6AdhpIAJghQAQg5AQg0IAAgCIAAABQANgqAHgmQAKgzAAgtQAChPgdg6QgchJhkgQQhngQi0AsQgUACgQgDQgbgEgSgPQgTgQgMgcQBDhAA/hHIAbgeQBLhaBFhiIAog7IA8heIAVgiQA5hcA2heIA5hlIASgiQAOgaARgVQAbgiAkgXQAqAeAgAyIAMASICRDiIALARICaDtIAWAiIABAAQA9BdBEBUIADADQBDBTBJBKIAGAGIAMALQgKAjgkADIAAAAIgBAAIgIAAIgMABQh0AGh/gLIgdgCQhygLg5A7IAAAAQgQAOgMASQgXAkgHA2QgDAVgBAYQgBA1AJA6QAIA0AQA5QAaBaAvBjQAQAhAKAeIAIAbQAcBwgzBQIgBACQgMARgPAPQgYAZgeAQIgEACIgBABQhAAfhEAUIgRAEQjCgChoi5g");
	this.shape.setTransform(0.4,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("AAYRBIgUAAQjegHh4jVQgZgtAMg0QA1jkA1isQA0irguhhQgvhikiBKQijAWglipQDLi+CdjvQCMjXB8jlQA4hnBgguIAEADQBLAoAxBOQCrELCtEIQCHDNCqChQgFB6hrAEQiOAJifgPQiggPgCCnQgDCoBlDUQBmDUhhCJQgsA+g9AfQhVAshcAXg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symboln1copy2, new cjs.Rectangle(-77.7,-108.9,155.5,218), null);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.question_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAFghIgpCCIg+AEIgljIIAugEIAaCdIA0idIApADIAiCeIAXigIAxACIgmDKIg+ACg");
	this.shape.setTransform(209.6,12);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgoBqQgTgIgOgQQgOgOgIgWQgJgUAAgaQAAgYAJgVQAIgWAOgOQAOgPATgJQATgIAVAAQAVAAATAIQAUAKAOAPQAOAPAIAVQAIAVAAAXQAAAYgIAVQgIAVgOAQQgOAOgUAKQgTAIgVAAQgVAAgTgIgAgYhCQgLAGgHALQgHALgDAMQgDANgBANQAAANAEANQAEANAHAKQAHALAKAGQALAHANAAQAPAAALgHQAKgGAHgLQAHgKAEgNQADgNAAgNQAAgMgDgOQgEgMgGgLQgIgLgKgGQgLgGgPAAQgOAAgKAGg");
	this.shape_1.setTransform(181.6,12.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhYAvIgCgvIgBgkIgBgbIgBgrIAygCIABAgIAAASIABANIAAAFIAAgFQAAgFAFgHQAIgKAKgKQALgJAMgHQAMgIAOgCQAQAAANAHQAGADAFAGQAGAFAFAIQAEAJADAKQADAMACASIgsAPQgBgNgBgGIgEgLQgCgEgCgDQgBgBAAAAQAAgBgBgBQAAAAAAAAQAAgBABAAQgJgEgHgBQgFABgEAEIgMAIIgLANIgMAPIgKAOIgKANIABAaIABAYIABAWIAAATIgwAGIgCg+g");
	this.shape_2.setTransform(158.7,12.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhYAvIgBgvIgCgkIgBgbIgBgrIAzgCIAAAgIAAASIAAANIAAAFIAAgFQABgFAGgHQAHgKALgKQAKgJALgHQAMgIAOgCQARAAANAHQAGADAGAGQAFAFAFAIQAEAJAEAKQADAMABASIgtAPQAAgNgCgGIgCgLQgCgEgDgDQgBgBAAAAQAAgBgBgBQAAAAAAAAQAAgBAAAAQgJgEgGgBQgFABgFAEIgLAIIgLANIgMAPIgLAOIgJANIABAaIAAAYIABAWIABATIgwAGIgCg+g");
	this.shape_3.setTransform(135.7,12.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AA2BvIADgaQgSAOgWAGQgWAHgTAAQgPAAgNgDQgNgEgKgIQgLgHgGgNQgGgMAAgQQAAgPAEgNQAFgNAIgJQAJgJALgHQALgHANgEQAMgFAOgBQANgDANAAIAYABQALACAEABIgGgNQgDgJgGgGQgFgHgHgEQgHgEgKAAQgHAAgIABQgJADgLAFQgLAGgNAKQgNAJgSARIgaghQATgTARgLQARgLAPgGQAOgGANgCQAMgDAKAAQASAAAPAGQAOAHALAKQALALAHAOQAHAOAEAQQAFARACASQACAQAAARQAAASgCAUQgCAWgFAbgAgHADQgQADgKAIQgLAHgFAJQgGAKADALQADAMAGADQAHAEAJgBQALAAAMgCIAXgJIAXgLIARgKIABgRQAAgIgBgIIgQgDQgKgCgKAAQgQAAgOAEg");
	this.shape_4.setTransform(109.6,11.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgsCKQgTgIgOgPQgPgPgIgVQgJgUAAgbQABgaAIgUQAKgUAPgPQAPgOATgHQASgIAPAAQARABAPADQAKADANAFQANAHAFAKIAChgIArAEIgCEaIguAAIAAgVIAAgCIAAACQAAAEgFADIgMAGIgMAFQgFADgLABQgHADgMABQgXgBgTgHgAgbgdQgKAGgJAJQgHAKgEALQgFANAAAQQAAAPAFAMQAEAMAIAJQAIAJALAFQAMAFAMAAQAKAAALgDQALgEAJgGQAIgHAGgJQAHgJADgKIAAgnQgCgLgHgJQgHgJgJgHQgJgHgLgEQgKgDgLAAQgNAAgLAFg");
	this.shape_5.setTransform(70.6,8.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgrCKQgUgIgPgPQgOgPgIgVQgJgUABgbQAAgaAJgUQAJgUAPgPQAPgOATgHQASgIAQAAQAQABAOADQALADANAFQANAHAFAKIABhgIAsAEIgBEaIguAAIAAgVIAAgCIAAACQgBAEgFADIgMAGIgMAFQgGADgKABQgHADgMABQgWgBgTgHgAgagdQgMAGgHAJQgIAKgFALQgEANAAAQQAAAPAEAMQAFAMAIAJQAIAJALAFQALAFANAAQALAAAKgDQAKgEAJgGQAJgHAHgJQAGgJAEgKIAAgnQgDgLgHgJQgHgJgJgHQgJgHgLgEQgLgDgKAAQgNAAgKAFg");
	this.shape_6.setTransform(43,8.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgoBqQgTgIgOgQQgOgOgIgWQgJgUABgaQgBgYAJgVQAIgWAOgOQAOgPATgJQATgIAVAAQAVAAAUAIQASAKAPAPQAOAPAIAVQAJAVAAAXQAAAYgJAVQgIAVgOAQQgPAOgSAKQgUAIgVAAQgVAAgTgIgAgZhCQgKAGgHALQgHALgEAMQgCANgBANQABANADANQADANAIAKQAGALALAGQALAHANAAQAOAAALgHQALgGAHgLQAHgKADgNQAEgNAAgNQAAgMgDgOQgDgMgIgLQgHgLgKgGQgLgGgPAAQgOAAgLAGg");
	this.shape_7.setTransform(17.4,12.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgrBoQgUgIgOgOQgNgOgHgUQgHgUAAgZQAAgXAIgVQAIgVAOgPQAPgQATgJQAUgJAVAAQAVAAASAIQASAIAOANQANAOAJATQAJATACAZIihAYQABAJAFAKQAFAKAIAHQAHAGAKAEQAKAEALgBQAKABAJgDQAJgDAIgGQAIgFAFgJQAGgHADgPIAsAJQgGAVgKAPQgKAOgNAKQgNALgQAFQgQAGgSAAQgYAAgUgIgAgOhEQgIACgJAGQgIAGgHALQgGAKgDALIBogNIgBAAQgGgRgMgJQgMgKgRAAQgGAAgJADg");
	this.shape_8.setTransform(-19.3,12.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAvCSQAFgWACgRIAEgeIAAgeQgBgTgGgOQgGgNgHgHQgJgJgJgDQgLgEgIAAQgJABgLAFQgIAEgLAJQgKAIgIAQIgCB/IgsABIgDkoIAzgCIgCByQAFgKALgHQANgGAIgDQAOgFALgBQAWAAATAIQARAIANAOQANAPAIAVQAHATABAcIAAAYIgCAdIgCAaQgCAMgEANg");
	this.shape_9.setTransform(-45.4,7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgagRIhBABIABgpIBBgCIABhTIAsgDIgBBUIBJgCIgDArIhGABIgCCjIguABg");
	this.shape_10.setTransform(-69.3,8.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgagRIhBABIABgpIBBgCIABhTIAsgDIgBBUIBJgCIgDArIhGABIgCCjIguABg");
	this.shape_11.setTransform(-101.5,8.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgkBoQgUgIgQgPQgQgPgKgVQgJgUAAgZQAAgOAEgOQAEgOAHgMQAHgNAKgKQALgLANgIQAMgHAPgEQAPgFAPAAQARAAAPAEQAPAEAOAIQANAHAKAMQAKALAHAOIABADIgmAWIgBgEQgFgJgGgHQgHgHgIgFQgIgFgKgDQgJgDgKABQgOgBgMAGQgNAFgJAKQgKAKgFANQgGANAAANQAAAPAGANQAFANAKAJQAJAJANAGQAMAGAOgBQAJAAAJgCQAJgDAIgEQAIgEAHgHQAGgHAFgHIACgEIAmAVIAAAFQgIANgLAKQgLAJgNAIQgNAHgPAEQgOAEgQAAQgVAAgVgJg");
	this.shape_12.setTransform(-124.3,12.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgrBoQgUgIgOgOQgNgOgHgUQgHgUAAgZQAAgXAIgVQAIgVAOgPQAPgQATgJQAUgJAVAAQAVAAASAIQASAIAOANQANAOAJATQAJATACAZIihAYQABAJAFAKQAFAKAIAHQAHAGAKAEQAKAEALgBQAKABAJgDQAJgDAIgGQAIgFAFgJQAGgHADgPIAsAJQgGAVgKAPQgKAOgNAKQgNALgQAFQgQAGgSAAQgYAAgUgIgAgOhEQgIACgJAGQgIAGgHALQgGAKgDALIBogNIgBAAQgGgRgMgJQgMgKgRAAQgGAAgJADg");
	this.shape_13.setTransform(-149.3,12.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAKCnQgKgFgIgHQgIgHgGgJQgFgKgEgOQgKgRgCgbIAFjxIAuAAIgCBBIgBAzIgBAoIgBAfIgBAzQAAAQAEANIAGALQACAGAFAEQADADAGADQAFACALABIgFAtQgRgBgMgEg");
	this.shape_14.setTransform(-166.9,5.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrBoQgUgIgOgOQgNgOgHgUQgHgUAAgZQAAgXAIgVQAIgVAOgPQAPgQATgJQAUgJAVAAQAVAAASAIQASAIAOANQANAOAJATQAJATACAZIihAYQABAJAFAKQAFAKAIAHQAHAGAKAEQAKAEALgBQAKABAJgDQAJgDAIgGQAIgFAFgJQAGgHADgPIAsAJQgGAVgKAPQgKAOgNAKQgNALgQAFQgQAGgSAAQgYAAgUgIgAgOhEQgIACgJAGQgIAGgHALQgGAKgDALIBogNIgBAAQgGgRgMgJQgMgKgRAAQgGAAgJADg");
	this.shape_15.setTransform(-186.8,12.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgZCaQgPgDgNgFQgOgGgOgIQgNgIgPgLIAfglQASAOAOAHQAOAHANAEQALAEALABQAMABALgDQALgCAIgFQAIgFAFgHQAFgHABgHQAAgGgCgEQgDgFgFgFQgGgEgHgEIgQgGIgSgFIgTgEIgVgFQgMgDgMgFQgMgFgLgIQgKgIgIgKQgIgLgEgPQgEgPACgSQABgQAFgNQAGgMAIgKQAJgKALgHQALgHAMgFQANgFANgCQANgCAMAAQASABASAFIAQAFIASAHIARAKQAJAFAIAKIgYAmIgQgNIgNgJIgNgGIgOgEQgMgEgOAAQgPAAgMAFQgNAEgJAIQgIAIgFAJQgEAJAAAJQAAAJAFAIQAFAIAKAHQAKAHANAFQANAFAPACQANACAOADQAOADANAFQANAGAKAHQALAIAIAKQAHAKAEANQADANgCAPQgCAOgGALQgGALgIAIQgJAHgLAGQgKAFgMADQgLAEgMABIgXACQgOAAgOgEg");
	this.shape_16.setTransform(-212.9,8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#660000").ss(5,1,1).p("Egl0gE+MBLqAAAQBmAABJBJQBIBJAABlIAACOQAABnhIBJQhJBIhmAAMhLqAAAQhnAAhJhIQhIhJAAhnIAAiOQAAhlBIhJQBJhJBnAAg");
	this.shape_17.setTransform(0,7.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF3333").s().p("Egl0AE/QhnAAhJhIQhIhJAAhnIAAiNQAAhmBIhJQBJhJBnAAMBLpAAAQBnAABJBJQBIBJAABmIAACNQAABnhIBJQhJBIhnAAg");
	this.shape_18.setTransform(0,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.question_mc, new cjs.Rectangle(-269.4,-27.2,538.8,68.8), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(139.6,0,0.99,0.99);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02},9).to({scaleX:0.99,scaleY:0.99},10).to({scaleX:1.02,scaleY:1.02},10).to({scaleX:0.99,scaleY:0.99},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.4,-38,299.5,76.1);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.arrow_mccopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.instance = new lib.Symboln1copy4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.36,0.429,90,0,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(51,0,0,0.247)").s().p("AmhGiQititAAj1QAAj0CtitQCtitD0AAQD1AACtCtQCtCtAAD0QAAD1itCtQitCtj1AAQj0AAititg");
	this.shape.setTransform(0.6,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(51,0,0,0.008)").s().p("AnLHLQi+i+AAkNQAAkNC+i+QC/i+EMAAQEOAAC+C+QC+C+AAENQAAENi+C+Qi+C/kOAAQkMAAi/i/g");
	this.shape_1.setTransform(0.6,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.arrow_mccopy6, new cjs.Rectangle(-64.4,-65,130,130), null);


(lib.arrow_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.instance = new lib.Symboln1copy2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.36,0.429,0,180,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(51,0,0,0.247)").s().p("AmhGiQititAAj1QAAj0CtitQCtitD0AAQD1AACtCtQCtCtAAD0QAAD1itCtQitCtj1AAQj0AAititg");
	this.shape.setTransform(0.6,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(51,0,0,0.008)").s().p("AnLHLQi+i+AAkNQAAkNC+i+QC/i+EMAAQEOAAC+C+QC+C+AAENQAAENi+C+Qi+C/kOAAQkMAAi/i/g");
	this.shape_1.setTransform(0.6,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.arrow_mccopy4, new cjs.Rectangle(-64.4,-65,130,130), null);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc8 = new lib.arrow_mccopy4();
	this.mc8.name = "mc8";
	this.mc8.parent = this;
	this.mc8.setTransform(385.9,114.3,1.595,1.595,0,0,0,-0.1,-1.2);

	this.mc3 = new lib.arrow_mccopy4();
	this.mc3.name = "mc3";
	this.mc3.parent = this;
	this.mc3.setTransform(386.2,-138.2,1.595,1.595,0,0,0,0.1,-1.4);

	this.mc2 = new lib.arrow_mccopy4();
	this.mc2.name = "mc2";
	this.mc2.parent = this;
	this.mc2.setTransform(-130,-138.2,1.595,1.595,0,0,0,-0.1,-1.4);

	this.mc8_1 = new lib.arrow_mccopy4();
	this.mc8_1.name = "mc8_1";
	this.mc8_1.parent = this;
	this.mc8_1.setTransform(127.9,114.3,1.595,1.595,0,0,0,-0.1,-1.2);

	this.mc7 = new lib.arrow_mccopy6();
	this.mc7.name = "mc7";
	this.mc7.parent = this;
	this.mc7.setTransform(-130.1,114,1.595,1.595,0,0,0,-0.1,-1.4);

	this.mc6 = new lib.arrow_mccopy4();
	this.mc6.name = "mc6";
	this.mc6.parent = this;
	this.mc6.setTransform(-387.6,116.5,1.595,1.595,0,0,0,0.2,0.2);

	this.mc3_1 = new lib.arrow_mccopy4();
	this.mc3_1.name = "mc3_1";
	this.mc3_1.parent = this;
	this.mc3_1.setTransform(128.2,-138.2,1.595,1.595,0,0,0,0.1,-1.4);

	this.mc1 = new lib.arrow_mccopy4();
	this.mc1.name = "mc1";
	this.mc1.parent = this;
	this.mc1.setTransform(-387.9,-138.2,1.595,1.595,0,0,0,-0.1,-1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc1},{t:this.mc3_1},{t:this.mc6},{t:this.mc7},{t:this.mc8_1},{t:this.mc2},{t:this.mc3},{t:this.mc8}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-490.6,-239.7,984.9,463.2);


(lib.Symbol7copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc7 = new lib.arrow_mccopy6();
	this.mc7.name = "mc7";
	this.mc7.parent = this;
	this.mc7.setTransform(-1.1,-2.1,1.595,1.595,0,0,0,-0.1,-1.4);

	this.timeline.addTween(cjs.Tween.get(this.mc7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7copy2, new cjs.Rectangle(-103.6,-103.6,210,210), null);


(lib.Symbol7copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc7 = new lib.arrow_mccopy6();
	this.mc7.name = "mc7";
	this.mc7.parent = this;
	this.mc7.setTransform(-1.1,-2.1,1.595,1.595,0,0,0,-0.1,-1.4);

	this.timeline.addTween(cjs.Tween.get(this.mc7).to({regX:-0.3,scaleX:1.76,scaleY:1.76,x:-1.4},9).to({regX:-0.1,scaleX:1.6,scaleY:1.6,x:-1.1},10).to({regX:-0.3,scaleX:1.76,scaleY:1.76,x:-1.4},10).to({regX:-0.1,scaleX:1.6,scaleY:1.6,x:-1.1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-103.6,-103.6,213,213);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.Tween2copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(512.8,587.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(250).to({_off:false},0).wait(54).to({startPosition:13},0).to({alpha:0,startPosition:3},10).wait(1));

	// Layer_2
	this.instance_1 = new lib.Symbol7copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(511,588.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(223).to({_off:false},0).to({scaleX:1.1,scaleY:1.1},27).wait(54).to({alpha:0},10).wait(1));

	// hand
	this.instance_2 = new lib.Symbol1copy_1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(619,661.7,1,1,0,0,0,190.5,64.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(189).to({_off:false},0).to({_off:true},34).wait(92));

	// arrow
	this.instance_3 = new lib.Symbol1copy("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(513.2,462.6,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(154).to({_off:false},0).to({_off:true},35).wait(126));

	// Layer_6
	this.instance_4 = new lib.Symbol7copy("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(511,588.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(110).to({_off:false},0).wait(37).to({mode:"independent"},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_3
	this.instance_5 = new lib.Symbol5("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(778.2,111.9,1,1,0,0,0,138.1,2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(70).to({_off:false},0).wait(77).to({startPosition:37},0).to({alpha:0,startPosition:3},6).to({_off:true},1).wait(161));

	// Layer_1
	this.instance_6 = new lib.Tween5("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(640,472);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(45).to({_off:false},0).to({alpha:1},8).wait(255).to({startPosition:0},0).to({alpha:0},6).wait(1));

	// Layer_4
	this.questiontxt = new lib.question_mc();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(640.1,101.1,1.231,1.231,0,0,0,0,0.1);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(20).to({_off:false},0).to({alpha:1},7).wait(281).to({alpha:0},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(410.3,226.2,1839,997);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#333333",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;