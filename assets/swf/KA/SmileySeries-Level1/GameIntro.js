(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ah9CWQAPACANgDQAMgCALgGQAKgGAKgIIARgRQAHgJAGgKIALgRIhjkFIA2gHIBDDNIAOgoIAQgzIARg4IASg7IA0ABIgXBCIgSA2IgPAsIgLAgIgSAzIgLAeQgGAPgJAPQgJAQgLAOQgMAOgQAMQgQALgUAHQgVAIgaABg");
	this.shape.setTransform(190.8,13.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("Ag0B9QgXgJgRgSQgQgQgIgYQgJgYAAgeQAAgcAKgZQAKgaARgTQARgSAXgLQAYgLAaABQAYgBAVAKQAWAJAQARQARARALAWQALAXADAaIjKAeQACAQAGANQAHAOAKAIQALAKANAEQANAFAPAAQAMAAANgEQALgDAKgIQALgHAHgLQAIgLADgPIAtAJQgGAVgMASQgMARgQANQgPAMgUAHQgSAGgVAAQgeABgYgKgAgShaQgMADgMAJQgKAHgJAOQgIAOgEAUICLgRIgBgFQgJgWgQgOQgPgNgXAAQgJABgLADg");
	this.shape_1.setTransform(164.5,6.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AAPDLQgOgFgIgJQgJgIgIgLQgGgLgFgLQgLgbgDgiIAGknIAwAAIgCBNIgCA/IgBAyIAAAnIgBA/QAAAWAEARQADAHAEAHQADAHAGAGQAFAFAJAEQAHADAKAAIgGAvQgRgBgMgFg");
	this.shape_2.setTransform(144.9,-2.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgdCvIABg8IAChJIABhjIAvgCIgBA9IgBAyIgBApIAAAfIgBAzgAgMhvQgGgCgEgFQgFgEgDgGQgCgHAAgGQAAgHACgGQADgGAFgFQAEgEAGgDQAGgCAGgBQAHABAGACQAGADAFAEQAEAFADAGQACAGAAAHQAAAGgCAHQgDAGgEAEIgLAHQgGADgHAAQgGAAgGgDg");
	this.shape_3.setTransform(131,0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("ABwA4IgBgzIAAglQgBgTgFgJQgGgJgIAAQgHgBgFAEQgHADgFAGIgMANIgLAOIgKAPIgHAMIAAAWIABAcIABAjIAAAsIgsACIgBhIIgCgzIgDglQgBgTgEgJQgGgJgJAAQgGAAgGADQgHAEgGAGIgMAPIgNAQIgKAQIgJAMIADB7IgvACIgGj5IAwgGIABBEIAQgTQAJgLAKgHQAJgJALgFQALgFAOAAQAKAAAJADQAKADAGAGQAHAHAFALQAGAKABAOIAQgTQAHgJAKgIQAJgIALgFQALgFAOAAQAKAAAJADQALAEAHAHQAIAIAEALQAGAMAAAPIACArIACA4IABBSIgxACIAAhIg");
	this.shape_4.setTransform(105.4,5.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AghCEIgUgFIgVgHQgLgEgKgGQgLgGgKgHIAXgkIAaANQANAHAOAEQAOAFAPADQAOADAPAAQANAAAJgDQAJgCAGgEQAFgDAEgFIAEgJQABgDAAgFQAAgFgCgEQgDgFgEgEQgFgFgHgDQgIgEgKgDIgagDQgUAAgTgEQgUgDgPgHQgPgIgKgKQgKgMgCgSQgCgRAEgOQAEgOAIgLQAHgLAMgIQALgIAOgFQANgGAPgDQAPgCAPAAIAVAAQANABANADQAOADANAFQAOAGAMAIIgQArQgPgIgOgFIgZgHQgNgDgKgBQgkgCgUAKQgVAKAAAUQAAAOAIAGQAHAGAOADQANADASABQARABAUADQAWADAQAHQAQAGAJAKQAKAJAEAMQAEALAAAMQAAAWgJAPQgJAPgPAKQgPAJgTAFQgUAEgVABQgUAAgWgEg");
	this.shape_5.setTransform(72.6,6.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("Ag0B9QgXgJgRgSQgQgQgIgYQgKgYABgeQAAgcAKgZQAKgaARgTQARgSAXgLQAYgLAaABQAYgBAVAKQAWAJAQARQARARALAWQALAXADAaIjKAeQACAQAGANQAHAOAKAIQAKAKAOAEQANAFAPAAQAMAAANgEQALgDAKgIQALgHAHgLQAIgLADgPIAtAJQgGAVgMASQgLARgRANQgPAMgUAHQgTAGgUAAQgeABgYgKgAgShaQgMADgMAJQgKAHgJAOQgJAOgDAUICLgRIgBgFQgJgWgQgOQgPgNgXAAQgJABgLADg");
	this.shape_6.setTransform(33.2,6.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA+CyQAIgXACgVIAFgmIAAglQgBgZgHgSQgIgQgLgLQgLgMgNgFQgNgEgNgBQgMACgOAGQgLAFgNAMQgNAMgMAVIgBCaIguABIgElnIA1gCIgBCPQANgOAOgJQAPgHANgFQAOgEANgCQAaAAAWAJQAVAJAQASQAPASAJAZQAJAYABAfIAAAiIgCAiIgDAfQgDAPgDAMg");
	this.shape_7.setTransform(3.4,0.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgcgaIhQACIABgsIBQgCIAChmIAugDIgCBpIBagEIgDAsIhYACIgBDKIgxACg");
	this.shape_8.setTransform(-23.9,1.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AA/CyQAGgXADgVIAEgmQACgTgBgSQgBgZgHgSQgIgQgLgLQgLgMgNgFQgNgEgNgBQgMACgOAGQgLAFgOAMQgNAMgLAVIgCCaIgtABIgElnIA2gCIgCCPQANgOAPgJQAOgHANgFQAOgEANgCQAaAAAWAJQAVAJAPASQAQASAJAZQAJAYABAfIAAAiIgBAiIgEAfQgCAPgEAMg");
	this.shape_9.setTransform(-63,0.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgqB9QgZgKgUgSQgSgSgMgZQgMgZAAgdQAAgRAFgQQAFgRAIgQQAJgOANgOQANgMAPgJQAPgKARgFQASgFATAAQAUAAASAFQATAFAPAJQAPAJANANQAMAOAJAQIAAABIgmAYIgBgBQgGgMgJgJQgJgKgKgGQgKgIgNgDQgMgDgNgBQgTAAgQAIQgRAHgNAMQgMANgHASQgIAQAAASQAAATAIARQAHAQAMAOQANAMARAHQAQAIATAAQAMgBALgDQAMgDALgGQAKgGAJgIQAIgJAGgKIABgCIApAXIgBABQgJAPgNANQgNAMgQAJQgQAIgRAFQgSAFgSAAQgaAAgYgLg");
	this.shape_10.setTransform(-93.4,6.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgcgaIhQACIABgsIBQgCIAChmIAugDIgCBpIBagEIgDAsIhYACIgBDKIgxACg");
	this.shape_11.setTransform(-119.3,1.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("ABICFIAEghQgdASgbAIQgbAJgXAAQgRAAgQgFQgQgEgMgJQgMgJgHgOQgIgOAAgTQAAgTAGgPQAGgOAJgMQAKgKAOgIQANgJAPgFQAPgFARgCQAQgDARAAIAcABIAYADQgDgLgFgLQgEgMgHgJQgHgJgKgGQgKgFgOAAQgJAAgLADQgLACgOAHQgOAHgRANQgQAMgTATIgdgiQAXgVAUgOQAVgNASgHQASgIAQgDQAOgDAMAAQAWAAARAIQARAHAMANQANAMAJASQAIARAGAUQAGATACAVQADAVAAAVQAAAWgDAaIgIA4gAgKgBQgUAEgOAKQgPAJgHANQgHAOAEARQAEAOAJAGQAJAFAOAAQANAAAQgEQAPgFAPgGQAQgHAOgHIAXgOIABgXIgCgYQgLgCgNgCQgMgDgNAAQgVAAgSAFg");
	this.shape_12.setTransform(-146.4,5.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("ABthxIhMD9Ig6AGIhXj1IgqELIgygHIBClTIAwAAIBcEMIBVkJIA3gCIA/FaIgxAKg");
	this.shape_13.setTransform(-182.6,1.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(3,1,1).p("A/ZkvMA+zAAAQCgAAAACgIAAEfQAACgigAAMg+zAAAQigAAAAigIAAkfQAAigCgAAg");
	this.shape_14.setTransform(0,3.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("A/ZEwQigAAAAigIAAkfQAAigCgAAMA+zAAAQCgAAAACgIAAEfQAACgigAAg");
	this.shape_15.setTransform(0,3.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-218.5,-38.6,437.1,77.3);


(lib.Symbol7copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C4E50C").s().p("AoGNeQhfg9hXhXQi1i0hMjhQg4ijAAi6QAAm7E5k5QAigiAkgeQA9goBBgeQCohPDDgOQArgDAsAAQB2AABtAWQCBAaB0A6QCXBLCECEQE5E4AAG6QAAG6k5E5QgjAjgkAeQj9CmlCAAQlAAAj9ilg");
	this.shape.setTransform(101.4,110.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D9F350").s().p("AHHOYQE5k5AAm6QAAm6k5k4QiEiEiXhLQh0g6iAgaQhtgWh3AAQgsAAgrADQjDAOioBPQhBAeg9AoQBuheB9g6QCphPDCgPQArgCAtAAQB2AABtAWQCAAaB1A6QCXBLCDCDQE5E5AAG6QAAG6k5E5QhVBVhfA+QAkgeAjgjg");
	this.shape_1.setTransform(126,98.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7copy, new cjs.Rectangle(0,0,213.7,213.7), null);


(lib.Option1_mccopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("ACBAqQiEhdh9BdQgGAFgIgBQgIgCgGgGQgEgHABgIQABgIAGgFQCUhvCcBvQAHAEABAJQACAIgFAGQgFAHgIACIgEAAQgGAAgFgEg");
	this.shape.setTransform(-2.1,38,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("Ai8AyQgJgFAAgZQAFguASgbIACAIQAEAPAMAXQAMAWgCALQgBALgIAHQgIAGgIADIgGABQgFAAgGgEgACsA1QgIgCgIgHQgIgHgBgLQgCgLAMgWQAMgXAEgOIACgJQASAbAFAuQAAAZgJAFQgGAEgFAAIgGgBg");
	this.shape_1.setTransform(1.1,20.7,2.664,2.664);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AB9AlQgPgPAAgWQAAgVAPgOQAQgPAVgBQAVABAPAPQAQAOAAAVQAAAWgQAPQgPAOgVABQgVgBgQgOgAjGAlQgPgPAAgWQAAgVAPgOQAQgPAVgBQAVABAPAPQAQAOAAAVQAAAWgQAPQgPAOgVABQgVgBgQgOg");
	this.shape_2.setTransform(-1.9,-11.8,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_4
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EE257D").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkOQAegeAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_3.setTransform(-5.7,1.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EC5698").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_4.setTransform(15.5,-8.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer_5
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_5.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy3, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhFBUQAHABAHgBQAIgCAGgDQAGgDAFgFIAJgKIAIgKIAGgJIg4iSIAfgEIAlByIAIgVIAJgdIAKgfIAJgiIAdABIgMAlIgKAfIgJAXIgGASIgKAdIgGARIgJAQIgLARQgGAIgJAHQgJAGgMAEQgLAEgPABg");
	this.shape.setTransform(94.5,29.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgdBGQgNgFgJgKQgJgJgFgOQgFgNAAgRQAAgPAGgOQAFgPAKgKQAKgKANgGQANgGAOAAQANAAAMAFQAMAFAKAKQAJAJAGAMQAGANACAPIhxAQQABAJAEAHQAEAIAFAFQAGAFAIADQAHACAIAAQAHAAAHgCQAGgCAGgEQAGgEAEgGQAEgHACgIIAZAFQgDAMgHAKQgHAKgIAHQgJAGgLAEQgLAEgLAAQgQAAgOgFgAgKgyQgHACgGAEQgGAFgFAIQgFAHgCAMIBOgKIgBgCQgFgNgJgIQgIgHgNAAIgLACg");
	this.shape_1.setTransform(79.8,25.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAIByQgHgDgEgFQgGgFgDgGQgEgGgDgHQgHgOgBgTIAEilIAaAAIgBArIgCAjIAAAdIAAAVIgBAjQABAMACAKIADAIIAGAHIAHAFQAFACAFAAIgEAaQgJAAgHgDg");
	this.shape_2.setTransform(68.8,20.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgQBiIABghIABgpIAAg3IAagBIAAAhIgBAcIAAAXIgBASIAAAcgAgGg9IgGgEIgEgGQgCgDAAgEQAAgEACgEIAEgFQACgDAEgBQADgCADAAQAEAAADACQADABADADIAEAFQACAEAAAEQAAAEgCADIgEAGIgGAEQgDABgEAAQgDAAgDgBg");
	this.shape_3.setTransform(61,22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AA+AfIAAgcIAAgUQAAgLgDgFQgEgGgFAAQgCAAgEACIgGAFIgHAIIgHAIIgEAIIgGAHIABALIAAAQIABAUIAAAYIgZACIAAgpIgBgcIgBgUQAAgLgDgFQgEgGgEAAQgEAAgEADIgHAFIgHAIIgGAJIgGAJIgFAGIACBFIgaACIgEiMIAbgDIAAAmIAJgKQAFgGAGgFQAFgEAHgDQAFgDAIAAQAGAAAFACQAFABADAEQAEAEADAFQADAGABAIIAJgKIAKgKIAKgHQAHgDAHAAQAGAAAGACQAFACAEAEQAFAEACAHQADAGAAAJIABAYIABAeIABAuIgbACIgBgpg");
	this.shape_4.setTransform(46.7,25.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSBKIgLgDIgMgEIgMgGQgGgDgFgEIAMgUIAPAHIAPAHIAQAEIAQABIANgBIAHgDIAGgFIACgFIAAgEQAAgDgBgDIgDgFQgDgCgFgCIgJgEIgPgBQgKgBgLgCQgLgCgJgEQgJgEgFgFQgGgHgBgKQgBgJACgIQADgIAEgGQAEgGAHgFQAGgEAIgDQAIgDAIgCQAIgBAIAAIAMAAIAPACIAOAFQAJADAGAEIgJAYQgIgEgIgDIgOgEIgOgCQgTgBgMAFQgKAGgBALQAAAIAFADQAEAEAHACIASACIAUACQANACAJAEQAIADAFAFQAGAFACAGQACAHAAAHQAAAMgEAJQgGAIgIAFQgJAFgKADQgLADgMAAIgXgCg");
	this.shape_5.setTransform(28.4,26);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgdBGQgNgFgJgKQgJgJgFgOQgFgNAAgRQAAgPAGgOQAFgPAKgKQAKgKANgGQANgGAOAAQANAAAMAFQAMAFAKAKQAJAJAGAMQAGANACAPIhxAQQABAJAEAHQAEAIAFAFQAGAFAIADQAHACAIAAQAHAAAHgCQAGgCAGgEQAGgEAEgGQAEgHACgIIAZAFQgDAMgHAKQgHAKgIAHQgJAGgLAEQgLAEgLAAQgQAAgOgFgAgKgyQgHACgGAEQgGAFgFAIQgFAHgCAMIBOgKIgBgCQgFgNgJgIQgIgHgNAAIgLACg");
	this.shape_6.setTransform(6.3,25.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAjBjQAEgMABgMIADgVIAAgVQgBgOgEgKQgEgJgGgGQgGgGgHgDQgIgDgHAAQgGABgIAEQgGADgIAGQgHAGgHAMIAABWIgaABIgCjIIAegBIgBBPQAHgIAIgEIAQgHIAPgEQAOAAAMAFQAMAGAJAJQAIAKAGAOQAFANAAASIAAATIgBATIgCARIgDAPg");
	this.shape_7.setTransform(-10.3,22.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPgOIgtABIAAgZIAtgBIABg4IAZgCIAAA6IAygCIgCAZIgxABIgBBwIgaABg");
	this.shape_8.setTransform(-25.6,23);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAjBjQAEgMABgMIADgVIAAgVQgBgOgEgKQgEgJgGgGQgGgGgHgDQgIgDgHAAQgGABgIAEQgGADgIAGQgHAGgHAMIAABWIgaABIgCjIIAegBIgBBPQAHgIAIgEIAQgHIAPgEQAOAAAMAFQAMAGAJAJQAIAKAGAOQAFANAAASIAAATIgBATIgCARIgDAPg");
	this.shape_9.setTransform(-47.5,22.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgXBGQgOgGgLgJQgKgLgHgNQgGgPAAgQQAAgIACgKQADgJAFgJQAFgJAHgGQAHgIAIgFQAIgFAKgDQALgDAJAAQAMAAAKADQAKADAJAEQAIAGAIAIQAHAGAFAKIAAABIgWANIgBgBIgHgMIgMgJQgFgDgHgDQgHgBgIAAQgJAAgKADQgJAFgHAGQgHAIgEAJQgEAJAAAKQAAALAEAJQAEAJAHAIQAHAGAJAFQAKAEAJAAQAIAAAGgCIAMgFIALgIIAIgLIAAgBIAYANIgBABQgFAIgIAIQgHAGgIAFQgJAFgKADQgLACgKAAQgNAAgOgGg");
	this.shape_10.setTransform(-64.5,25.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgPgOIgtABIAAgZIAtgBIABg4IAZgCIAAA6IAygCIgCAZIgxABIgBBwIgaABg");
	this.shape_11.setTransform(-79,23);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAoBKIADgSQgRAKgPAFQgOAEgOAAQgJAAgJgCQgJgCgGgFQgHgFgEgIQgEgIAAgLQAAgKADgJQADgIAGgGQAFgGAHgEQAIgFAIgDQAJgDAJgBIASgBIAQAAIANACIgEgNQgDgGgDgFQgEgFgGgDQgFgDgIAAIgLABIgOAFQgIAEgJAHQgJAHgLALIgQgTQANgMALgIQAMgHAKgEIASgGIAPgCQAMAAAJAFQAKAEAHAHQAHAHAFAJQAFAKADALQAEALABAMQABALAAAMIgBAaIgFAggAgGAAQgLACgIAFQgHAFgEAIQgEAHACAKQACAIAFADQAGADAHAAQAIAAAJgCIAQgHIAQgHIANgIIABgNIgBgNIgOgCIgNgCQgMAAgLADg");
	this.shape_12.setTransform(-94.1,25.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA9g/IgrCNIgfAEIgxiJIgXCVIgcgEIAki9IAbAAIA0CVIAviUIAeAAIAjDBIgbAFg");
	this.shape_13.setTransform(-114.4,23);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#660303").ss(3,1,1).p("AvxinIfjAAQBCAAAvAvQAvAvAABCIAAAPQAABCgvAvQgvAvhCAAI/jAAQhCAAgvgvQgvgvAAhCIAAgPQAAhCAvgvQAvgvBCAAg");
	this.shape_14.setTransform(-12.3,24.9,1.215,1.215);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF3333").s().p("AvxCoQhCAAgvgvQgvgvAAhCIAAgPQAAhCAvgvQAvgvBCAAIfjAAQBCAAAvAvQAvAvAABCIAAAPQAABCgvAvQgvAvhCAAg");
	this.shape_15.setTransform(-12.3,24.9,1.215,1.215);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-155.9,0.6,287.2,46.2);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Option1_mccopy3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333333").s().p("Ai7gYQgGgGgBgIQAAgIAFgGQAGgHAIAAQAIgBAHAGQCmCRCbiRQAGgFAIAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQhbBVheAAQhdAAhihUg");
	this.shape_6.setTransform(-1.9,33.8,2.664,2.664);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#333333").s().p("ACBAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOgAjCAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOg");
	this.shape_7.setTransform(-1.9,-13,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// Layer_3
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF6633").s().p("Am/LpQhTg1hLhLQicichCjCQgwiNAAihQAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQCDBABxByQEPEOAAF+QAAF9kPEPQgdAegfAaQjbCQkXAAQkUAAjaiPg");
	this.shape_8.setTransform(-6.7,0.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhyhxiChBQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQBfhRBtgyQCRhECogNQAlgCAnAAQBmAABeATQBvAXBlAyQCCBBBxBxQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_9.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8}]}).wait(1));

	// Layer_5
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_10.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy3_1, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ai7gYQgGgGgBgIQAAgIAFgGQAGgHAIAAQAIgBAHAGQCmCRCbiRQAGgFAIAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQhbBVheAAQhdAAhihUg");
	this.shape.setTransform(-1.9,33.8,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOgAjCAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOg");
	this.shape_1.setTransform(-1.9,-13,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C4E50C").s().p("Am/LpQhTg1hLhLQicichCjCQgwiNAAihQAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQCDBABxByQEPEOAAF+QAAF9kPEPQgdAegfAaQjbCQkXAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D9F350").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhyhxiChBQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQBfhRBtgyQCRhECogNQAlgCAnAAQBmAABeATQBvAXBlAyQCCBBBxBxQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy2, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Option1_mccopy3();
	this.instance.parent = this;
	this.instance.setTransform(3,22.1,1.297,1.297,0,0,0,-2,-2.5);

	this.instance_1 = new lib.Option1_mccopy3_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(303.1,22.1,1.297,1.297,0,0,0,-2.2,-2.5);

	this.instance_2 = new lib.Option1_mccopy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-297.7,22.1,1.297,1.297,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-423,-103.8,852.1,251.5);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween22("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(217,38.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,437.1,77.3);


(lib.Symbol8copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ai7gYQgGgGgBgIQAAgIAFgGQAGgHAIAAQAIgBAHAGQCmCRCbiRQAGgFAIAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQhbBVheAAQhdAAhihUg");
	this.shape.setTransform(107.1,149.2,3.084,3.084);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOgAjCAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOg");
	this.shape_1.setTransform(107.1,95,3.084,3.084);

	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(106.9,106.8,1,1,0,0,0,106.9,106.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy4, new cjs.Rectangle(0,0,213.7,213.7), null);


(lib.Symbol8copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ai7gYQgGgGgBgIQAAgIAFgGQAGgHAIAAQAIgBAHAGQCmCRCbiRQAGgFAIAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQhbBVheAAQhdAAhihUg");
	this.shape.setTransform(107.1,149.2,3.084,3.084);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOgAjCAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOg");
	this.shape_1.setTransform(107.1,95,3.084,3.084);

	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(106.9,106.8,1,1,0,0,0,106.9,106.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy3, new cjs.Rectangle(0,0,213.7,213.7), null);


(lib.Symbol8copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ai7gYQgGgGgBgIQAAgIAFgGQAGgHAIAAQAIgBAHAGQCmCRCbiRQAGgFAIAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQhbBVheAAQhdAAhihUg");
	this.shape.setTransform(107.1,149.2,3.084,3.084);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOgAjCAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOg");
	this.shape_1.setTransform(107.1,95,3.084,3.084);

	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(106.9,106.8,1,1,0,0,0,106.9,106.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy2, new cjs.Rectangle(0,0,213.7,213.7), null);


(lib.Symbol8copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ai7gYQgGgGgBgIQAAgIAFgGQAGgHAIAAQAIgBAHAGQCmCRCbiRQAGgFAIAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQhbBVheAAQhdAAhihUg");
	this.shape.setTransform(107.1,149.2,3.084,3.084);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOgAjCAhIgEgEQgKgNAAgQQAAgSAOgOQAOgOATAAQATAAAOAOQAOAOAAASQAAAQgKANIgEAEQgOAOgTAAQgTAAgOgOg");
	this.shape_1.setTransform(107.1,95,3.084,3.084);

	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(106.9,106.8,1,1,0,0,0,106.9,106.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy, new cjs.Rectangle(0,0,213.7,213.7), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Option1_mccopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol8copy();
	this.instance.parent = this;
	this.instance.setTransform(-1.9,-2.7,1,1,0,0,0,106.9,106.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("ANlUOI7JAAQiwAAh8h8IgBgBQh8h8AAiwIAA7JQAAiwB8h9IABAAQB8h8CwAAIbJAAQCwAAB9B8IAAAAQB8B9AACwIAAbJQAACwh8B8IAAABQh9B8iwAAIAAAAgAtkTDIbJAAQCRAABnhnIAAABIAAgBIAAAAQBmhmAAiRIAA7JQAAiRhmhnIAAAAIAAAAIAAAAQhnhmiRAAI7JAAQiRAAhmBmIAAAAIgBAAIABAAQhnBnAACRIAAbJQAACRBnBmIgBAAIABABIAAgBQBmBnCRAAgARdxcgAxbxcg");
	this.shape.setTransform(-1.3,0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AtkTDQiRAAhmhnQhnhmAAiRIAA7JQAAiRBnhnQBmhmCRAAIbJAAQCRAABnBmIAAAAIAAAAIAAAAQBmBnAACRIAAbJQAACRhmBmIAAAAIAAABIAAgBQhnBniRAAgAxcRcIABAAIAAABgAxcxcIABAAIAAAAg");
	this.shape_1.setTransform(-1.3,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_7
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.02)").s().p("ANlVZI7JAAQjPAAiTiSIAAAAIAAgBIAAAAQiSiSAAjPIAA7JQAAjPCSiTIAAAAIAAAAIAAAAQCTiSDPAAIbJAAQDPAACTCSIAAAAIAAAAIAAAAQCSCTAADPIAAbJQAADPiSCSIAAAAIAAABIAAAAQiTCSjOAAIgBAAgARdSnIBKhKIh/g1gAymRdIBKBKIA1h/gAQownIB/g1IhKhKgAymxcIB/A1Ig1h/g");
	this.shape_2.setTransform(-1.3,0.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy5, new cjs.Rectangle(-138.1,-135.9,273.8,273.8), null);


(lib.Option1_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol8copy();
	this.instance.parent = this;
	this.instance.setTransform(-1.9,-2.7,1,1,0,0,0,106.9,106.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("ANlUOI7JAAQiwAAh8h8IgBgBQh8h8AAiwIAA7JQAAiwB8h9IABAAQB8h8CwAAIbJAAQCwAAB9B8IAAAAQB8B9AACwIAAbJQAACwh8B8IAAABQh9B8iwAAIAAAAgAtkTDIbJAAQCRAABnhnIAAABIAAgBIAAAAQBmhmAAiRIAA7JQAAiRhmhnIAAAAIAAAAIAAAAQhnhmiRAAI7JAAQiRAAhmBmIAAAAIgBAAIABAAQhnBnAACRIAAbJQAACRBnBmIgBAAIABABIAAgBQBmBnCRAAgARdxcgAxbxcg");
	this.shape.setTransform(-1.3,0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AtkTDQiRAAhmhnQhnhmAAiRIAA7JQAAiRBnhnQBmhmCRAAIbJAAQCRAABnBmIAAAAIAAAAIAAAAQBmBnAACRIAAbJQAACRhmBmIAAAAIAAABIAAgBQhnBniRAAgAxcRcIABAAIAAABgAxcxcIABAAIAAAAg");
	this.shape_1.setTransform(-1.3,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_7
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.02)").s().p("ANlVZI7JAAQjPAAiTiSIAAAAIAAgBIAAAAQiSiSAAjPIAA7JQAAjPCSiTIAAAAIAAAAIAAAAQCTiSDPAAIbJAAQDPAACTCSIAAAAIAAAAIAAAAQCSCTAADPIAAbJQAADPiSCSIAAAAIAAABIAAAAQiTCSjOAAIgBAAgARdSnIBKhKIh/g1gAymRdIBKBKIA1h/gAQownIB/g1IhKhKgAymxcIB/A1Ig1h/g");
	this.shape_2.setTransform(-1.3,0.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy4, new cjs.Rectangle(-138.1,-135.9,273.8,273.8), null);


(lib.Option1_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Option1_mccopy4();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-20.5,0.761,0.761,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy, new cjs.Rectangle(-105.9,-122.1,208.3,208.3), null);


(lib.Option1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Option1_mccopy5();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-20.5,0.761,0.761,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mc, new cjs.Rectangle(-105.9,-122.1,208.3,208.3), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.Option1_mccopy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Option1_mccopy5();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-20.5,0.761,0.761,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy10, new cjs.Rectangle(-105.9,-122.1,208.3,208.3), null);


(lib.Option1_mccopy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Option1_mccopy5();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-20.5,0.761,0.761,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy9, new cjs.Rectangle(-105.9,-122.1,208.3,208.3), null);


(lib.Option1_mccopy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Option1_mccopy5();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-20.5,0.761,0.761,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy8, new cjs.Rectangle(-105.9,-122.1,208.3,208.3), null);


(lib.Option1_mccopy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Option1_mccopy5();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,-20.5,0.761,0.761,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-2.6,scaleX:0.78,scaleY:0.78,y:-20.6},9).to({regX:-2.4,scaleX:0.76,scaleY:0.76},10).to({regX:-2.3,scaleX:0.78,scaleY:0.78},10).to({regY:-2.5,scaleX:0.76,scaleY:0.76,y:-20.5},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-105.9,-122.1,208.3,208.3);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(637.9,305.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(329).to({_off:false},0).wait(55).to({startPosition:10},0).to({alpha:0,startPosition:15},5).wait(1));

	// hand
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(292.1,657.7,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(273).to({_off:false},0).to({_off:true},32).wait(85));

	// arrow
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(339.5,380.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(241).to({_off:false},0).to({_off:true},32).wait(117));

	// VALIDATE
	this.instance_3 = new lib.Symbol8copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(344.1,557.5,1.082,1.082,0,0,0,107,107);
	this.instance_3._off = true;

	this.instance_4 = new lib.Symbol8copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(640.2,293.4,1.082,1.082,0,0,0,107,107);

	this.instance_5 = new lib.Symbol8copy3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(640.2,293.4,1.082,1.082,0,0,0,107,107);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},305).to({state:[{t:this.instance_4}]},21).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},20).to({state:[{t:this.instance_5}]},35).to({state:[{t:this.instance_5}]},5).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(305).to({_off:false},0).to({_off:true,x:640.2,y:293.4},21).wait(64));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(329).to({_off:false},0).to({scaleX:1.31,scaleY:1.31},20).wait(35).to({alpha:0},5).wait(1));

	// 3
	this.instance_6 = new lib.Symbol8copy4();
	this.instance_6.parent = this;
	this.instance_6.setTransform(643.8,287.4,1.019,1.019,0,0,0,107.2,106.9);
	this.instance_6._off = true;

	this.instance_7 = new lib.Symbol8copy();
	this.instance_7.parent = this;
	this.instance_7.setTransform(942,562.5,1.121,1.121,0,0,0,107.3,107);
	this.instance_7.alpha = 0.398;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(150).to({_off:false},0).to({_off:true,regX:107.3,regY:107,scaleX:1.12,scaleY:1.12,x:942,y:562.5,alpha:0.398},21).wait(219));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(150).to({_off:false},21).wait(44).to({alpha:0},5).to({_off:true},1).wait(169));

	// 2
	this.instance_8 = new lib.Symbol8copy();
	this.instance_8.parent = this;
	this.instance_8.setTransform(643.8,287.4,1.019,1.019,0,0,0,107.2,106.9);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(150).to({_off:false},0).to({regX:107.3,scaleX:1.12,scaleY:1.12,x:643.9,y:563.5,alpha:0.398},21).wait(44).to({alpha:0},5).to({_off:true},1).wait(169));

	// smly 1
	this.instance_9 = new lib.Symbol8copy();
	this.instance_9.parent = this;
	this.instance_9.setTransform(641.8,287.4,1.019,1.019,0,0,0,107.2,106.9);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(150).to({_off:false},0).to({regX:107.3,scaleX:1.12,scaleY:1.12,x:343.8,y:560.6,alpha:0.398},21).wait(44).to({alpha:0},5).to({_off:true},1).wait(169));

	// Layer_2
	this.instance_10 = new lib.Symbol9("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,105.9,1,1,0,0,0,217,38.6);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(93).to({_off:false},0).wait(122).to({startPosition:28},0).to({alpha:0,startPosition:33},5).to({_off:true},1).wait(169));

	// Layer_3
	this.instance_11 = new lib.Tween16("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,539.9);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(70).to({_off:false},0).to({alpha:1},6).wait(250).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(58));

	// Layer_1
	this.instance_12 = new lib.Option1_mc();
	this.instance_12.parent = this;
	this.instance_12.setTransform(639.7,314,1.335,1.335,0,0,0,-2.3,-2.6);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_13 = new lib.Option1_mccopy8();
	this.instance_13.parent = this;
	this.instance_13.setTransform(639.7,314,1.335,1.335,0,0,0,-2.3,-2.6);

	this.instance_14 = new lib.Option1_mccopy7("synched",33);
	this.instance_14.parent = this;
	this.instance_14.setTransform(639.7,314.1,1.335,1.335,0,0,0,-2.3,-2.5);

	this.instance_15 = new lib.Option1_mccopy("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(639.7,314.1,1.335,1.335,0,0,0,-2.3,-2.5);

	this.instance_16 = new lib.Option1_mccopy9();
	this.instance_16.parent = this;
	this.instance_16.setTransform(639.7,314,1.335,1.335,0,0,0,-2.3,-2.6);

	this.instance_17 = new lib.Option1_mccopy10();
	this.instance_17.parent = this;
	this.instance_17.setTransform(639.7,314,1.335,1.335,0,0,0,-2.3,-2.6);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},44).to({state:[{t:this.instance_13}]},6).to({state:[{t:this.instance_14}]},43).to({state:[{t:this.instance_15}]},57).to({state:[{t:this.instance_16}]},71).to({state:[{t:this.instance_17}]},108).to({state:[{t:this.instance_17}]},20).to({state:[{t:this.instance_17}]},35).to({state:[{t:this.instance_17}]},5).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(44).to({_off:false},0).to({_off:true,alpha:1},6).wait(340));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(329).to({_off:false},0).to({regY:-2.5,scaleX:1.62,scaleY:1.62,x:639.5,y:318.3},20).wait(35).to({alpha:0},5).wait(1));

	// Layer_6
	this.instance_18 = new lib.Tween6("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(663,64.5,1.866,1.866,0,0,0,0.1,0.1);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(20).to({_off:false},0).to({alpha:1},6).wait(300).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(58));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(599.8,328.2,1364,786);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;