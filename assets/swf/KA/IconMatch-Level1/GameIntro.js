(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ah3BAIgChAIgCgxIgBglIgCg2IA7gCIABBKQAIgOALgNQALgNAMgKQANgKANgGQAOgGAQgCQATgBAPAFQAOAEAKAJQALAIAGALQAHALAEAMQAEAMABAMIACAVQACArgBAtIgCBcIg0gCIAEhSQABgpgCgpIAAgMIgDgQQgCgIgEgIQgEgIgGgGQgGgGgJgDQgJgDgMACQgUADgUAaQgVAbgYAyIACBBIABAlIABAUIg3AHIgDhPg");
	this.shape.setTransform(391.7,4.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Ag0CMQgagLgSgUQgTgUgKgbQgLgcAAgiQAAghALgcQAKgcATgTQASgUAagLQAYgKAcAAQAcAAAZALQAZALATAVQASAUAMAbQAKAcAAAfQAAAggKAcQgMAbgSAUQgTAVgZALQgZALgcAAQgcAAgYgKgAgkhfQgRAKgJAPQgLAPgEASQgFATABASQAAASAEATQAGASAKAPQAKAPAQAKQAPAJAUAAQAVAAARgJQAPgKAKgPQALgPAEgSQAFgTAAgSQAAgSgEgTQgFgSgKgPQgKgPgQgKQgQgJgWAAQgVAAgPAJg");
	this.shape_1.setTransform(359.9,4.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgNgcQgNgbAAgfQAAgTAGgTQAEgSAKgQQAKgRAOgPQANgOAQgKQASgJASgGQAUgGAVAAQAWAAAUAFQAUAGARAKQARAKAOAOQAOAPAJATIAAABIgrAaIAAgCQgHgNgJgLQgJgKgMgHQgMgIgNgDQgOgFgOAAQgVABgRAHQgTAJgOAOQgOANgHATQgIASAAAVQAAAUAIATQAHASAOAOQAOANATAIQARAJAVAAQANgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgOANQgPANgRALQgRAJgUAFQgUAFgTAAQgcAAgbgLg");
	this.shape_2.setTransform(328.5,5.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AggDBIAChCIABhQIABhtIA0gDIgBBDIgBA3IgBAuIAAAiIgCA4gAgNh6QgHgCgFgFQgFgFgCgGQgEgIAAgHQAAgHAEgHQACgHAFgFQAFgFAHgCQAGgEAHAAQAIAAAGAEQAHACAEAFQAFAFAEAHQACAHAAAHQAAAHgCAIQgEAGgFAFQgEAFgHACQgGADgIAAQgHAAgGgDg");
	this.shape_3.setTransform(306.3,-1.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("Ag3DVQgMgDgMgFQgNgFgKgJQgMgIgKgLQgJgMgGgQIAxgWQAFALAHAIQAHAIAIAGQAIAFAJADQAHAEAJABQATAEATgCQASgEANgHQANgIAKgJQAJgJAGgKIAJgTQADgKABgHIABgMIAAghQgPAOgQAIQgRAIgPAEQgPAEgPAAQgeAAgagKQgagKgSgUQgTgTgLgbQgLgbAAgjQAAgjAMgbQAMgbAUgTQATgTAZgJQAYgKAZAAQARACARAFQAQAFARAJQASAJAOAPIABgyIAyABIgBElQAAARgFARQgEAQgIAQQgIAPgLAOQgNANgPALQgPALgUAGQgTAHgXACIgGAAQgbAAgYgHgAgmibQgPAHgLAOQgMANgGASQgGASgBAVQABAVAGASQAGASAMALQALANAQAHQAPAHAUAAQAQAAAPgGQAQgGANgKQANgKAJgOQAJgPADgSIAAghQgCgTgKgOQgJgQgOgKQgNgLgQgGQgQgGgQAAQgSAAgQAIg");
	this.shape_4.setTransform(267.4,10.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("Ah3BAIgChAIgCgxIgBglIgCg2IA7gCIABBKQAIgOALgNQALgNAMgKQANgKANgGQAOgGAQgCQATgBAPAFQAOAEAKAJQALAIAGALQAHALAEAMQAEAMABAMIACAVQACArgBAtIgCBcIg0gCIAEhSQABgpgCgpIAAgMIgDgQQgCgIgEgIQgEgIgGgGQgGgGgJgDQgJgDgMACQgUADgUAaQgVAbgYAyIACBBIABAlIABAUIg3AHIgDhPg");
	this.shape_5.setTransform(234.7,4.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AggDBIABhCIAChQIABhtIA0gDIgBBDIgBA3IgBAuIAAAiIgBA4gAgNh6QgHgCgEgFQgGgFgCgGQgDgIgBgHQABgHADgHQACgHAGgFQAEgFAHgCQAGgEAHAAQAHAAAHAEQAGACAFAFQAGAFACAHQADAHABAHQgBAHgDAIQgCAGgGAFQgFAFgGACQgHADgHAAQgHAAgGgDg");
	this.shape_6.setTransform(212.4,-1.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("ABFDDQAHgYAEgYIAEgpIAAgqQAAgbgJgTQgIgSgMgMQgMgMgOgGQgOgFgPgBQgNABgPAHQgNAHgOANQgPALgMAYIgCCqIgyABIgEmKIA6gCIgBCcQAOgPAQgJQAQgJAOgFQAPgFAPgCQAdAAAYALQAXAKAQATQASATAJAcQAKAbACAiIAAAlIgDAlIgDAjQgDAQgEANg");
	this.shape_7.setTransform(188.6,-1.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgNgcQgNgbAAgfQAAgTAGgTQAEgSAKgQQAKgRANgPQAOgOAQgKQASgJASgGQAUgGAUAAQAXAAAUAFQAUAGARAKQARAKAOAOQANAPAKATIAAABIgrAaIAAgCQgHgNgJgLQgJgKgMgHQgMgIgOgDQgNgFgPAAQgUABgRAHQgTAJgOAOQgOANgHATQgJASABAVQgBAUAJATQAHASAOAOQAOANATAIQARAJAUAAQAOgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgPANQgOANgRALQgRAJgUAFQgUAFgUAAQgbAAgbgLg");
	this.shape_8.setTransform(155.3,5.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgegdIhZACIABgvIBZgDIAChwIAygDIgDByIBkgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_9.setTransform(127,-0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("ABQCSIADgkQgfAUgeAJQgeAJgZAAQgTAAgRgFQgRgFgNgKQgNgKgJgPQgIgPAAgVQAAgUAGgRQAHgQAKgNQALgLAPgJQAOgJARgGQAQgGATgCQASgDARAAIAgABIAaAEIgIgZQgFgNgIgKQgHgKgLgGQgLgGgQAAQgKAAgLADQgNADgPAIQgQAHgSAOQgSANgVAVIgfglQAYgXAXgPQAWgPAUgIQAUgIARgDQAQgDAOAAQAXAAATAIQATAIAOAOQANAOAKATQAKASAFAWQAHAWACAXQADAWAAAXQAAAZgDAcQgCAcgHAhgAgMgBQgVAFgQAKQgPAKgIAPQgIAPAEATQAEAPALAGQAKAGAPAAQAOAAASgFQAQgFARgHIAggPIAZgPIABgaIgBgaQgMgDgOgCQgOgCgOAAQgXAAgVAFg");
	this.shape_10.setTransform(97.4,4.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AB7A9IAAg3IgBgpQgBgVgGgKQgGgLgKAAQgGAAgHAEQgGAEgHAGIgMAOIgMAQIgLAQIgJAOIABAXIABAgIAAAmIAAAwIgwADIgBhQIgCg3IgDgpQAAgVgGgKQgGgLgKAAQgHABgHAEQgHAEgHAHQgHAHgGAJIgNASIgMARIgJANIACCHIgzADIgHkTIA2gGIABBLIARgVQAJgLALgJQAKgJANgFQAMgHAPABQALAAAKADQAKAEAHAGQAIAIAGALQAGALABAPIARgUQAJgLAKgIQALgJAMgFQAMgFAOAAQAMAAAKADQALAEAIAJQAJAHAFANQAFAMABASIADAvIACA9IABBaIg2ADIAAhQg");
	this.shape_11.setTransform(60.5,4.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgegdIhZACIABgvIBZgDIABhwIAzgDIgCByIBjgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_12.setTransform(13.3,-0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgOgcQgMgbAAgfQAAgTAGgTQAEgSAKgQQAJgRAPgPQANgOAQgKQARgJATgGQAUgGAVAAQAVAAAVAFQAUAGARAKQARAKAOAOQAOAPAJATIAAABIgqAaIgBgCQgHgNgJgLQgJgKgMgHQgMgIgNgDQgOgFgOAAQgUABgTAHQgSAJgNAOQgOANgJATQgHASAAAVQAAAUAHATQAJASAOAOQANANASAIQATAJAUAAQANgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgOANQgOANgSALQgRAJgUAFQgUAFgTAAQgcAAgbgLg");
	this.shape_13.setTransform(-15,5.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("ABPCSIAEgkQgfAUgeAJQgeAJgZAAQgTAAgRgFQgSgFgNgKQgNgKgIgPQgIgPAAgVQAAgUAHgRQAFgQALgNQALgLAPgJQAOgJARgGQARgGASgCQASgDARAAIAgABIAaAEIgIgZQgFgNgIgKQgHgKgLgGQgLgGgQAAQgKAAgLADQgNADgQAIQgPAHgSAOQgSANgVAVIgfglQAYgXAXgPQAWgPAVgIQATgIARgDQAQgDAOAAQAXAAATAIQATAIAOAOQAOAOAJATQAJASAGAWQAHAWACAXQADAWAAAXQAAAZgDAcQgDAcgGAhgAgMgBQgVAFgQAKQgQAKgHAPQgIAPAEATQAEAPALAGQAKAGAOAAQAQAAARgFQAQgFARgHIAggPIAZgPIABgaIgBgaQgMgDgOgCQgOgCgOAAQgXAAgVAFg");
	this.shape_14.setTransform(-48,4.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AACAnIhMBjIg2gOIBih8Ihlh8IA2gMIBOBgIBNhiIAxASIheB4IBjB4IgvATg");
	this.shape_15.setTransform(-78,4.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgJgbAAghQgBgeALgcQALgcATgVQATgUAZgMQAbgMAcAAQAbAAAXALQAXAKATATQASARAMAZQALAZAEAdIjdAgQACASAHAPQAHAPALAJQALAKAPAGQAPAFAQAAQANAAAOgEQANgFALgHQALgJAJgLQAJgMADgRIAxALQgGAWgOATQgNAUgRANQgRANgWAIQgUAHgXAAQghABgagKgAgUhiQgNADgNAJQgMAJgJAPQgJAPgFAWICZgSIgBgGQgKgZgRgOQgQgPgaAAQgKAAgMAFg");
	this.shape_16.setTransform(-107.3,5.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgKgbAAghQABgeAKgcQALgcATgVQATgUAagMQAagMAcAAQAaAAAYALQAXAKATATQASARAMAZQAMAZACAdIjcAgQACASAHAPQAHAPALAJQAMAKAOAGQAPAFAQAAQANAAAOgEQANgFAMgHQAKgJAJgLQAIgMAEgRIAyALQgIAWgNATQgMAUgSANQgSANgUAIQgVAHgXAAQghABgagKgAgVhiQgNADgMAJQgLAJgKAPQgKAPgDAWICZgSIgCgGQgKgZgQgOQgSgPgZAAQgKAAgNAFg");
	this.shape_17.setTransform(-152,5.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("ABEDDQAIgYAEgYIAEgpIAAgqQgBgbgIgTQgIgSgMgMQgMgMgOgGQgPgFgNgBQgOABgPAHQgNAHgOANQgPALgMAYIgCCqIgyABIgFmKIA8gCIgCCcQAOgPAQgJQAQgJANgFQARgFAOgCQAcAAAZALQAXAKAQATQARATAKAcQALAbAAAiIAAAlIgCAlIgDAjQgDAQgEANg");
	this.shape_18.setTransform(-184.6,-1.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgfgdIhYACIABgvIBYgDIADhwIAxgDIgCByIBkgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_19.setTransform(-214.4,-0.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AgegdIhZACIABgvIBYgDIAChwIAzgDIgCByIBjgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_20.setTransform(-253,-0.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgNgcQgNgbAAgfQAAgTAGgTQAEgSAKgQQAKgRANgPQAOgOAQgKQASgJASgGQAUgGAVAAQAWAAAUAFQAUAGARAKQARAKAOAOQAOAPAJATIAAABIgrAaIAAgCQgHgNgJgLQgJgKgMgHQgMgIgOgDQgNgFgOAAQgVABgRAHQgTAJgOAOQgOANgHATQgIASAAAVQAAAUAIATQAHASAOAOQAOANATAIQARAJAVAAQANgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgOANQgPANgRALQgRAJgUAFQgUAFgTAAQgcAAgbgLg");
	this.shape_21.setTransform(-281.3,5.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgJgbAAghQgBgeALgcQALgcATgVQATgUAZgMQAbgMAcAAQAbAAAXALQAYAKASATQASARAMAZQALAZAEAdIjdAgQACASAHAPQAHAPALAJQALAKAPAGQAPAFAQAAQAOAAANgEQANgFALgHQALgJAJgLQAIgMAEgRIAxALQgGAWgNATQgOAUgRANQgRANgWAIQgUAHgXAAQghABgagKgAgUhiQgNADgNAJQgLAJgKAPQgKAPgEAWICZgSIgBgGQgKgZgRgOQgQgPgaAAQgKAAgMAFg");
	this.shape_22.setTransform(-312.6,5.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#660000").s().p("AAQDfQgOgGgJgJQgLgKgIgMQgHgMgFgMQgNgdgDglIAHlEIA1AAIgDBUIgCBFIgBA3IAAArIgCBFQABAYAFATQADAIAEAHQAEAIAGAGQAGAGAJAEQAIAEALAAIgHAzQgSAAgOgGg");
	this.shape_23.setTransform(-334.1,-4.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgKgbAAghQAAgeALgcQALgcATgVQATgUAZgMQAagMAdAAQAaAAAYALQAXAKATATQASARAMAZQAMAZADAdIjdAgQACASAHAPQAHAPALAJQALAKAPAGQAPAFAQAAQANAAAOgEQANgFALgHQALgJAJgLQAJgMADgRIAxALQgGAWgOATQgMAUgSANQgRANgWAIQgUAHgXAAQghABgagKgAgVhiQgNADgMAJQgMAJgJAPQgJAPgFAWICZgSIgBgGQgJgZgSgOQgQgPgaAAQgKAAgNAFg");
	this.shape_24.setTransform(-358.3,5.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#660000").s().p("AghDOQgTgEgTgIQgSgIgSgKQgSgKgSgNIAlgsQAUARAUALQATAKAQAFQASAGAQACQASABAQgDQAQgFAMgGQAMgIAHgKQAHgKABgMQABgJgEgIQgEgIgIgGQgIgHgLgEQgKgGgMgDIgagHIgYgGQgOgCgQgEQgQgFgQgHQgQgGgOgLQgNgJgKgOQgLgPgFgTQgFgUACgYQABgUAHgRQAHgQAMgNQALgOAOgJQAPgJAQgHQAQgFASgEQARgCARAAQAYACAYAFIAWAHIAXAKQALAFALAIQAMAHAKAKIgdAtQgIgJgJgHQgJgHgKgFIgSgJIgSgGQgTgFgTAAQgXgBgSAIQgSAGgMAMQgNAKgGAOQgHAOAAANQAAANAIAMQAIANAOAKQANAKATAHQATAHAUADQATACATAEQASAEARAIQARAGAOALQAOAJAKAOQAKANAEAQQAFARgDAUQgDASgHAOQgIAOgLAKQgLAKgOAIQgOAGgPAFQgQAEgPACQgQACgOAAQgTAAgTgEg");
	this.shape_25.setTransform(-390.9,-0.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FF6600").ss(5,1,1).p("Eg9ZgGkMB60AAAQB0AABRBnQBSBmAACRIAACNQAACQhSBnQhRBnh0AAMh60AAAQh0AAhRhnQhThnAAiQIAAiNQAAiRBThmQBRhnB0AAg");
	this.shape_26.setTransform(0,0.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFCC00").s().p("Eg9YAGlQh1AAhRhnQhThnABiQIAAiNQgBiRBThmQBRhnB1AAMB6yAAAQB1AABRBnQBSBmgBCRIAACNQABCQhSBnQhRBnh1AAg");
	this.shape_27.setTransform(0,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-423.4,-43.8,846.9,89.3);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ah3BAIgChAIgCgxIgBglIgCg2IA7gCIABBKQAIgOALgNQALgNAMgKQANgKANgGQAOgGAQgCQATgBAPAFQAOAEAKAJQALAIAGALQAHALAEAMQAEAMABAMIACAVQACArgBAtIgCBcIg0gCIAEhSQABgpgCgpIAAgMIgDgQQgCgIgEgIQgEgIgGgGQgGgGgJgDQgJgDgMACQgUADgUAaQgVAbgYAyIACBBIABAlIABAUIg3AHIgDhPg");
	this.shape.setTransform(391.7,4.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Ag0CMQgagLgSgUQgTgUgKgbQgLgcAAgiQAAghALgcQAKgcATgTQASgUAagLQAYgKAcAAQAcAAAZALQAZALATAVQASAUAMAbQAKAcAAAfQAAAggKAcQgMAbgSAUQgTAVgZALQgZALgcAAQgcAAgYgKgAgkhfQgRAKgJAPQgLAPgEASQgFATABASQAAASAEATQAGASAKAPQAKAPAQAKQAPAJAUAAQAVAAARgJQAPgKAKgPQALgPAEgSQAFgTAAgSQAAgSgEgTQgFgSgKgPQgKgPgQgKQgQgJgWAAQgVAAgPAJg");
	this.shape_1.setTransform(359.9,4.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgNgcQgNgbAAgfQAAgTAGgTQAEgSAKgQQAKgRAOgPQANgOAQgKQASgJASgGQAUgGAVAAQAWAAAUAFQAUAGARAKQARAKAOAOQAOAPAJATIAAABIgrAaIAAgCQgHgNgJgLQgJgKgMgHQgMgIgNgDQgOgFgOAAQgVABgRAHQgTAJgOAOQgOANgHATQgIASAAAVQAAAUAIATQAHASAOAOQAOANATAIQARAJAVAAQANgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgOANQgPANgRALQgRAJgUAFQgUAFgTAAQgcAAgbgLg");
	this.shape_2.setTransform(328.5,5.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AggDBIAChCIABhQIABhtIA0gDIgBBDIgBA3IgBAuIAAAiIgCA4gAgNh6QgHgCgFgFQgFgFgCgGQgEgIAAgHQAAgHAEgHQACgHAFgFQAFgFAHgCQAGgEAHAAQAIAAAGAEQAHACAEAFQAFAFAEAHQACAHAAAHQAAAHgCAIQgEAGgFAFQgEAFgHACQgGADgIAAQgHAAgGgDg");
	this.shape_3.setTransform(306.3,-1.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("Ag3DVQgMgDgMgFQgNgFgKgJQgMgIgKgLQgJgMgGgQIAxgWQAFALAHAIQAHAIAIAGQAIAFAJADQAHAEAJABQATAEATgCQASgEANgHQANgIAKgJQAJgJAGgKIAJgTQADgKABgHIABgMIAAghQgPAOgQAIQgRAIgPAEQgPAEgPAAQgeAAgagKQgagKgSgUQgTgTgLgbQgLgbAAgjQAAgjAMgbQAMgbAUgTQATgTAZgJQAYgKAZAAQARACARAFQAQAFARAJQASAJAOAPIABgyIAyABIgBElQAAARgFARQgEAQgIAQQgIAPgLAOQgNANgPALQgPALgUAGQgTAHgXACIgGAAQgbAAgYgHgAgmibQgPAHgLAOQgMANgGASQgGASgBAVQABAVAGASQAGASAMALQALANAQAHQAPAHAUAAQAQAAAPgGQAQgGANgKQANgKAJgOQAJgPADgSIAAghQgCgTgKgOQgJgQgOgKQgNgLgQgGQgQgGgQAAQgSAAgQAIg");
	this.shape_4.setTransform(267.4,10.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("Ah3BAIgChAIgCgxIgBglIgCg2IA7gCIABBKQAIgOALgNQALgNAMgKQANgKANgGQAOgGAQgCQATgBAPAFQAOAEAKAJQALAIAGALQAHALAEAMQAEAMABAMIACAVQACArgBAtIgCBcIg0gCIAEhSQABgpgCgpIAAgMIgDgQQgCgIgEgIQgEgIgGgGQgGgGgJgDQgJgDgMACQgUADgUAaQgVAbgYAyIACBBIABAlIABAUIg3AHIgDhPg");
	this.shape_5.setTransform(234.7,4.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AggDBIABhCIAChQIABhtIA0gDIgBBDIgBA3IgBAuIAAAiIgBA4gAgNh6QgHgCgEgFQgGgFgCgGQgDgIgBgHQABgHADgHQACgHAGgFQAEgFAHgCQAGgEAHAAQAHAAAHAEQAGACAFAFQAGAFACAHQADAHABAHQgBAHgDAIQgCAGgGAFQgFAFgGACQgHADgHAAQgHAAgGgDg");
	this.shape_6.setTransform(212.4,-1.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("ABFDDQAHgYAEgYIAEgpIAAgqQAAgbgJgTQgIgSgMgMQgMgMgOgGQgOgFgPgBQgNABgPAHQgNAHgOANQgPALgMAYIgCCqIgyABIgEmKIA6gCIgBCcQAOgPAQgJQAQgJAOgFQAPgFAPgCQAdAAAYALQAXAKAQATQASATAJAcQAKAbACAiIAAAlIgDAlIgDAjQgDAQgEANg");
	this.shape_7.setTransform(188.6,-1.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgNgcQgNgbAAgfQAAgTAGgTQAEgSAKgQQAKgRANgPQAOgOAQgKQASgJASgGQAUgGAUAAQAXAAAUAFQAUAGARAKQARAKAOAOQANAPAKATIAAABIgrAaIAAgCQgHgNgJgLQgJgKgMgHQgMgIgOgDQgNgFgPAAQgUABgRAHQgTAJgOAOQgOANgHATQgJASABAVQgBAUAJATQAHASAOAOQAOANATAIQARAJAUAAQAOgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgPANQgOANgRALQgRAJgUAFQgUAFgUAAQgbAAgbgLg");
	this.shape_8.setTransform(155.3,5.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgegdIhZACIABgvIBZgDIAChwIAygDIgDByIBkgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_9.setTransform(127,-0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("ABQCSIADgkQgfAUgeAJQgeAJgZAAQgTAAgRgFQgRgFgNgKQgNgKgJgPQgIgPAAgVQAAgUAGgRQAHgQAKgNQALgLAPgJQAOgJARgGQAQgGATgCQASgDARAAIAgABIAaAEIgIgZQgFgNgIgKQgHgKgLgGQgLgGgQAAQgKAAgLADQgNADgPAIQgQAHgSAOQgSANgVAVIgfglQAYgXAXgPQAWgPAUgIQAUgIARgDQAQgDAOAAQAXAAATAIQATAIAOAOQANAOAKATQAKASAFAWQAHAWACAXQADAWAAAXQAAAZgDAcQgCAcgHAhgAgMgBQgVAFgQAKQgPAKgIAPQgIAPAEATQAEAPALAGQAKAGAPAAQAOAAASgFQAQgFARgHIAggPIAZgPIABgaIgBgaQgMgDgOgCQgOgCgOAAQgXAAgVAFg");
	this.shape_10.setTransform(97.4,4.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AB7A9IAAg3IgBgpQgBgVgGgKQgGgLgKAAQgGAAgHAEQgGAEgHAGIgMAOIgMAQIgLAQIgJAOIABAXIABAgIAAAmIAAAwIgwADIgBhQIgCg3IgDgpQAAgVgGgKQgGgLgKAAQgHABgHAEQgHAEgHAHQgHAHgGAJIgNASIgMARIgJANIACCHIgzADIgHkTIA2gGIABBLIARgVQAJgLALgJQAKgJANgFQAMgHAPABQALAAAKADQAKAEAHAGQAIAIAGALQAGALABAPIARgUQAJgLAKgIQALgJAMgFQAMgFAOAAQAMAAAKADQALAEAIAJQAJAHAFANQAFAMABASIADAvIACA9IABBaIg2ADIAAhQg");
	this.shape_11.setTransform(60.5,4.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgegdIhZACIABgvIBZgDIABhwIAzgDIgCByIBjgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_12.setTransform(13.3,-0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgOgcQgMgbAAgfQAAgTAGgTQAEgSAKgQQAJgRAPgPQANgOAQgKQARgJATgGQAUgGAVAAQAVAAAVAFQAUAGARAKQARAKAOAOQAOAPAJATIAAABIgqAaIgBgCQgHgNgJgLQgJgKgMgHQgMgIgNgDQgOgFgOAAQgUABgTAHQgSAJgNAOQgOANgJATQgHASAAAVQAAAUAHATQAJASAOAOQANANASAIQATAJAUAAQANgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgOANQgOANgSALQgRAJgUAFQgUAFgTAAQgcAAgbgLg");
	this.shape_13.setTransform(-15,5.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("ABPCSIAEgkQgfAUgeAJQgeAJgZAAQgTAAgRgFQgSgFgNgKQgNgKgIgPQgIgPAAgVQAAgUAHgRQAFgQALgNQALgLAPgJQAOgJARgGQARgGASgCQASgDARAAIAgABIAaAEIgIgZQgFgNgIgKQgHgKgLgGQgLgGgQAAQgKAAgLADQgNADgQAIQgPAHgSAOQgSANgVAVIgfglQAYgXAXgPQAWgPAVgIQATgIARgDQAQgDAOAAQAXAAATAIQATAIAOAOQAOAOAJATQAJASAGAWQAHAWACAXQADAWAAAXQAAAZgDAcQgDAcgGAhgAgMgBQgVAFgQAKQgQAKgHAPQgIAPAEATQAEAPALAGQAKAGAOAAQAQAAARgFQAQgFARgHIAggPIAZgPIABgaIgBgaQgMgDgOgCQgOgCgOAAQgXAAgVAFg");
	this.shape_14.setTransform(-48,4.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AACAnIhMBjIg2gOIBih8Ihlh8IA2gMIBOBgIBNhiIAxASIheB4IBjB4IgvATg");
	this.shape_15.setTransform(-78,4.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgJgbAAghQgBgeALgcQALgcATgVQATgUAZgMQAbgMAcAAQAbAAAXALQAXAKATATQASARAMAZQALAZAEAdIjdAgQACASAHAPQAHAPALAJQALAKAPAGQAPAFAQAAQANAAAOgEQANgFALgHQALgJAJgLQAJgMADgRIAxALQgGAWgOATQgNAUgRANQgRANgWAIQgUAHgXAAQghABgagKgAgUhiQgNADgNAJQgMAJgJAPQgJAPgFAWICZgSIgBgGQgKgZgRgOQgQgPgaAAQgKAAgMAFg");
	this.shape_16.setTransform(-107.3,5.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgKgbAAghQABgeAKgcQALgcATgVQATgUAagMQAagMAcAAQAaAAAYALQAXAKATATQASARAMAZQAMAZACAdIjcAgQACASAHAPQAHAPALAJQAMAKAOAGQAPAFAQAAQANAAAOgEQANgFAMgHQAKgJAJgLQAIgMAEgRIAyALQgIAWgNATQgMAUgSANQgSANgUAIQgVAHgXAAQghABgagKgAgVhiQgNADgMAJQgLAJgKAPQgKAPgDAWICZgSIgCgGQgKgZgQgOQgSgPgZAAQgKAAgNAFg");
	this.shape_17.setTransform(-152,5.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("ABEDDQAIgYAEgYIAEgpIAAgqQgBgbgIgTQgIgSgMgMQgMgMgOgGQgPgFgNgBQgOABgPAHQgNAHgOANQgPALgMAYIgCCqIgyABIgFmKIA8gCIgCCcQAOgPAQgJQAQgJANgFQARgFAOgCQAcAAAZALQAXAKAQATQARATAKAcQALAbAAAiIAAAlIgCAlIgDAjQgDAQgEANg");
	this.shape_18.setTransform(-184.6,-1.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgfgdIhYACIABgvIBYgDIADhwIAxgDIgCByIBkgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_19.setTransform(-214.4,-0.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AgegdIhZACIABgvIBYgDIAChwIAzgDIgCByIBjgDIgEAwIhgACIgCDeIg1ACg");
	this.shape_20.setTransform(-253,-0.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("AguCKQgbgLgWgVQgUgTgNgcQgNgbAAgfQAAgTAGgTQAEgSAKgQQAKgRANgPQAOgOAQgKQASgJASgGQAUgGAVAAQAWAAAUAFQAUAGARAKQARAKAOAOQAOAPAJATIAAABIgrAaIAAgCQgHgNgJgLQgJgKgMgHQgMgIgOgDQgNgFgOAAQgVABgRAHQgTAJgOAOQgOANgHATQgIASAAAVQAAAUAIATQAHASAOAOQAOANATAIQARAJAVAAQANgBANgDQAMgEAMgGQAMgHAJgJQAKgKAGgLIABgCIAtAaIgBABQgKARgOANQgPANgRALQgRAJgUAFQgUAFgTAAQgcAAgbgLg");
	this.shape_21.setTransform(-281.3,5.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgJgbAAghQgBgeALgcQALgcATgVQATgUAZgMQAbgMAcAAQAbAAAXALQAYAKASATQASARAMAZQALAZAEAdIjdAgQACASAHAPQAHAPALAJQALAKAPAGQAPAFAQAAQAOAAANgEQANgFALgHQALgJAJgLQAIgMAEgRIAxALQgGAWgNATQgOAUgRANQgRANgWAIQgUAHgXAAQghABgagKgAgUhiQgNADgNAJQgLAJgKAPQgKAPgEAWICZgSIgBgGQgKgZgRgOQgQgPgaAAQgKAAgMAFg");
	this.shape_22.setTransform(-312.6,5.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#660000").s().p("AAQDfQgOgGgJgJQgLgKgIgMQgHgMgFgMQgNgdgDglIAHlEIA1AAIgDBUIgCBFIgBA3IAAArIgCBFQABAYAFATQADAIAEAHQAEAIAGAGQAGAGAJAEQAIAEALAAIgHAzQgSAAgOgGg");
	this.shape_23.setTransform(-334.1,-4.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#660000").s().p("Ag5CKQgagLgSgSQgSgUgJgZQgKgbAAghQAAgeALgcQALgcATgVQATgUAZgMQAagMAdAAQAaAAAYALQAXAKATATQASARAMAZQAMAZADAdIjdAgQACASAHAPQAHAPALAJQALAKAPAGQAPAFAQAAQANAAAOgEQANgFALgHQALgJAJgLQAJgMADgRIAxALQgGAWgOATQgMAUgSANQgRANgWAIQgUAHgXAAQghABgagKgAgVhiQgNADgMAJQgMAJgJAPQgJAPgFAWICZgSIgBgGQgJgZgSgOQgQgPgaAAQgKAAgNAFg");
	this.shape_24.setTransform(-358.3,5.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#660000").s().p("AghDOQgTgEgTgIQgSgIgSgKQgSgKgSgNIAlgsQAUARAUALQATAKAQAFQASAGAQACQASABAQgDQAQgFAMgGQAMgIAHgKQAHgKABgMQABgJgEgIQgEgIgIgGQgIgHgLgEQgKgGgMgDIgagHIgYgGQgOgCgQgEQgQgFgQgHQgQgGgOgLQgNgJgKgOQgLgPgFgTQgFgUACgYQABgUAHgRQAHgQAMgNQALgOAOgJQAPgJAQgHQAQgFASgEQARgCARAAQAYACAYAFIAWAHIAXAKQALAFALAIQAMAHAKAKIgdAtQgIgJgJgHQgJgHgKgFIgSgJIgSgGQgTgFgTAAQgXgBgSAIQgSAGgMAMQgNAKgGAOQgHAOAAANQAAANAIAMQAIANAOAKQANAKATAHQATAHAUADQATACATAEQASAEARAIQARAGAOALQAOAJAKAOQAKANAEAQQAFARgDAUQgDASgHAOQgIAOgLAKQgLAKgOAIQgOAGgPAFQgQAEgPACQgQACgOAAQgTAAgTgEg");
	this.shape_25.setTransform(-390.9,-0.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FF6600").ss(5,1,1).p("Eg9ZgGkMB60AAAQB0AABRBnQBSBmAACRIAACNQAACQhSBnQhRBnh0AAMh60AAAQh0AAhRhnQhThnAAiQIAAiNQAAiRBThmQBRhnB0AAg");
	this.shape_26.setTransform(0,0.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFCC00").s().p("Eg9YAGlQh1AAhRhnQhThnABiQIAAiNQgBiRBThmQBRhnB1AAMB6yAAAQB1AABRBnQBSBmgBCRIAACNQABCQhSBnQhRBnh1AAg");
	this.shape_27.setTransform(0,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-423.4,-43.8,846.9,89.3);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhyA9IgCg9IgCgvIgBgkIgCg0IA4gBIABBHQAIgOALgMQAKgNAMgJQAMgKANgGQANgGAPgBQATgBAOAEQAOAFAKAIQAJAIAHAKQAGALAEAMQAEALABALIACAVQACApgBArIgCBYIgygBIAEhPQABgogCgnIAAgMIgDgPIgGgPQgEgIgFgFQgGgGgJgDQgIgDgMABQgTADgTAaQgUAZgXAwIACA/IABAkIAAATIg1AGIgChMg");
	this.shape.setTransform(240,5.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgyCGQgYgKgSgTQgRgTgLgbQgLgaAAghQAAggALgaQALgbARgTQASgTAYgKQAYgKAaAAQAbAAAYALQAYALASATQASATAKAbQAKAaAAAeQAAAfgKAaQgKAbgSATQgSATgYALQgYALgbAAQgaAAgYgKgAgjhbQgPAJgKAPQgJAOgFASQgEASAAARQAAARAFASQAEASAKAPQAKAOAPAJQAPAJATAAQAVAAAPgJQAPgJAJgOQAKgPAFgSQAFgSAAgRQAAgRgFgSQgEgSgJgOQgKgPgPgJQgPgJgWAAQgUAAgPAJg");
	this.shape_1.setTransform(209.5,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AgtCEQgZgKgVgUQgUgTgMgaQgNgaAAgfQABgRAFgSQAFgSAJgQQAJgQAOgNQANgOAQgKQAPgJATgFQATgGATAAQAVAAAUAFQATAFARAKQAQAJANAPQANAOAJARIAAACIgoAYIgBgCQgGgMgKgKQgJgKgLgGQgLgIgNgDQgNgEgOAAQgTAAgSAHQgRAIgNANQgOANgHATQgIARAAATQAAAUAIASQAHARAOAOQANANARAIQASAHATABQANgBAMgDQANgDALgHQAKgGAKgJQAIgJAHgLIABgCIArAZIgBABQgJAQgOANQgOANgQAJQgRAJgTAFQgTAFgTAAQgaAAgbgLg");
	this.shape_2.setTransform(179.4,5.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AgeC5IABg/IAChNIABhpIAygCIgBBAIgCA1IgBArIgBAiIAAA1gAgMh0QgHgDgFgFQgFgEgCgHQgDgGAAgHQAAgIADgGQACgGAFgFQAFgFAHgDQAFgDAHAAQAHAAAHADQAGADAFAFQAEAFAEAGQACAGAAAIQAAAHgCAGQgEAHgEAEQgFAFgGADQgHACgHAAQgHAAgFgCg");
	this.shape_3.setTransform(158,-0.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("Ag1DNQgLgDgMgFQgLgFgLgIQgLgIgJgLQgJgMgGgPIAvgUQAFAKAHAIQAGAHAIAFQAHAGAJADQAHADAIABQASAEATgCQARgDANgIQAMgHAJgIQAJgJAGgKQAFgKADgIIAFgQIABgMIAAggQgPAOgQAIQgQAHgNAEQgQAEgOAAQgdAAgYgKQgZgKgSgSQgSgTgLgZQgKgbAAghQAAghALgaQAMgbATgSQATgSAXgJQAYgKAYAAQAQACARAGQAOAFARAIQARAJAOAOIAAgwIAwABIgBEZQAAAQgEAQQgEAQgIAPQgIAPgLANQgLANgPAKQgPAKgTAHQgSAGgWACIgGAAQgaAAgXgGgAgkiVQgPAHgLANQgLANgGARQgGASAAAUQAAAUAGARQAHARALALQAKAMAQAHQAPAHASAAQAPAAAPgGQAPgFANgKQAMgKAJgOQAIgOAEgRIAAggQgDgRgJgPQgJgOgNgLQgMgKgQgFQgQgGgPAAQgRAAgPAHg");
	this.shape_4.setTransform(120.7,11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AhyA9IgCg9IgCgvIgBgkIgCg0IA4gBIABBHQAIgOALgMQAKgNAMgJQAMgKANgGQANgGAPgBQATgBAOAEQAOAFAKAIQAJAIAHAKQAGALAEAMQAEALABALIACAVQACApgBArIgCBYIgygBIAEhPQABgogCgnIAAgMIgDgPIgGgPQgEgIgFgFQgGgGgJgDQgIgDgMABQgTADgTAaQgUAZgXAwIACA/IABAkIAAATIg1AGIgChMg");
	this.shape_5.setTransform(89.4,5.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AgfC5IACg/IABhNIAChpIAxgCIgBBAIgBA1IAAArIgBAiIgBA1gAgNh0QgGgDgEgFQgFgEgDgHQgDgGAAgHQAAgIADgGQADgGAFgFQAEgFAGgDQAGgDAHAAQAHAAAGADQAHADAFAFQAEAFADAGQADAGAAAIQAAAHgDAGQgDAHgEAEQgFAFgHADQgGACgHAAQgHAAgGgCg");
	this.shape_6.setTransform(68,-0.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("ABCC7QAHgXADgXIAEgoIABgnQgBgagIgTQgIgSgLgLQgMgMgOgFQgOgFgNAAQgMABgPAHQgMAGgOAMQgOALgMAXIgCCjIgvABIgFl6IA5gCIgCCWQAOgPAPgJQAPgIAOgEQAPgFANgCQAcAAAXAJQAWAKAQATQAQATALAaQAIAZACAiIAAAjIgCAkIgEAhQgDAPgDANg");
	this.shape_7.setTransform(45.2,-0.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgsCEQgagKgVgUQgUgTgMgaQgNgaAAgfQAAgRAGgSQAEgSAKgQQAJgQANgNQANgOAQgKQARgJASgFQASgGAUAAQAWAAATAFQATAFARAKQAQAJANAPQANAOAJARIABACIgqAYIAAgCQgHgMgJgKQgIgKgMgGQgLgIgNgDQgNgEgOAAQgTAAgSAHQgRAIgNANQgNANgIATQgHARAAATQAAAUAHASQAIARANAOQANANARAIQASAHATABQANgBAMgDQAMgDAMgHQAKgGAJgJQAKgJAGgLIABgCIArAZIAAABQgKAQgOANQgOANgQAJQgRAJgSAFQgUAFgTAAQgbAAgZgLg");
	this.shape_8.setTransform(13.2,5.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgdgcIhVACIABgtIBVgCIAChsIAwgDIgCBuIBfgDIgDAuIhdACIgBDVIgzABg");
	this.shape_9.setTransform(-14,0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("ABMCMIAEgiQgeASgdAJQgcAJgZAAQgSAAgQgFQgRgEgMgJQgNgLgIgOQgIgOAAgVQAAgTAGgQQAGgQAKgMQALgLAOgJQAOgIAQgGQAQgFASgCQAQgDASAAIAeABIAZAEQgDgNgFgMQgFgMgHgJQgHgJgLgHQgKgFgPgBQgKAAgLAEQgMACgPAIQgPAHgRANQgSANgUAUIgdgkQAXgWAWgOQAWgPATgHQATgJAQgCQAPgDANAAQAXAAASAIQARAHAOAOQANANAJASQAJASAGAVQAGAVADAWQACAWAAAVQAAAYgCAbQgDAbgGAggAgLgBQgVAEgPAKQgPAKgHAOQgIAPAFASQADAPAKAFQAKAGAOAAQAOAAARgFIAggLIAfgPIAYgOIABgZQAAgMgBgNQgMgCgOgCQgNgDgNAAQgWAAgUAFg");
	this.shape_10.setTransform(-42.4,5.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AB2A6IgBg0IAAgoQgBgUgGgKQgGgJgJAAQgGAAgGADQgHAEgGAGQgGAGgGAHQgGAIgFAIIgLAPIgIANIABAXIABAdIAAAlIAAAuIgvADIAAhNIgCg0IgDgoQAAgUgGgKQgGgJgJAAQgHAAgGAEQgHAEgHAHQgHAGgGAJIgMARIgLAQIgJANIACCBIgxADIgHkHIA0gGIABBIIAQgVQAJgKAKgJQAKgIAMgGQAMgFAPAAQAKAAAKADQAJADAHAHQAIAHAFALQAGAKABAPIAQgTQAJgKAKgIQAKgJALgFQAMgFAOAAQALAAAKADQAKAEAIAIQAIAHAFANQAFAMABARIADAtIACA6IABBWIg0ADIAAhNg");
	this.shape_11.setTransform(-77.8,4.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgdgcIhVACIABgtIBVgCIAChsIAwgDIgCBuIBfgDIgDAuIhdACIgBDVIgzABg");
	this.shape_12.setTransform(-123.1,0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgsCEQgagKgVgUQgUgTgMgaQgNgaAAgfQAAgRAGgSQAEgSAKgQQAJgQANgNQANgOAQgKQARgJASgFQASgGAUAAQAWAAATAFQATAFAQAKQARAJANAPQANAOAJARIABACIgqAYIAAgCQgHgMgJgKQgIgKgMgGQgLgIgNgDQgNgEgOAAQgTAAgSAHQgRAIgNANQgNANgIATQgHARAAATQAAAUAHASQAIARANAOQANANARAIQASAHATABQANgBAMgDQAMgDAMgHQAKgGAJgJQAKgJAGgLIABgCIArAZIAAABQgKAQgOANQgOANgQAJQgRAJgSAFQgUAFgTAAQgbAAgZgLg");
	this.shape_13.setTransform(-150.2,5.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("ABMCMIAEgiQgeASgdAJQgcAJgZAAQgSAAgQgFQgRgEgMgJQgNgLgIgOQgIgOAAgVQAAgTAGgQQAGgQAKgMQALgLAOgJQAOgIAQgGQAQgFASgCQAQgDASAAIAeABIAZAEQgDgNgFgMQgFgMgHgJQgHgJgLgHQgKgFgPgBQgKAAgLAEQgMACgPAIQgPAHgRANQgSANgUAUIgdgkQAXgWAWgOQAWgPATgHQATgJAQgCQAPgDANAAQAXAAASAIQARAHAOAOQANANAJASQAJASAGAVQAGAVADAWQACAWAAAVQAAAYgCAbQgDAbgGAggAgLgBQgVAEgPAKQgPAKgHAOQgIAPAFASQADAPAKAFQAKAGAOAAQAOAAARgFIAggLIAfgPIAYgOIABgZQAAgMgBgNQgMgCgOgCQgNgDgNAAQgWAAgUAFg");
	this.shape_14.setTransform(-182,5.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AACAlIhJBgIg0gPIBfh2Ihhh3IAzgMIBLBdIBKhfIAuASIhZBzIBeBzIgtASg");
	this.shape_15.setTransform(-210.6,5.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("Ag3CEQgYgKgSgSQgRgSgJgZQgJgaAAgfQAAgdALgbQAKgbASgTQATgUAYgMQAZgKAbgBQAaABAWAJQAWAKASASQARASAMAXQALAYADAcIjUAeQACASAHAOQAHANAKAKQAMAKANAFQAPAFAPAAQANAAANgEQANgEALgIQAKgHAIgMQAIgMAEgPIAvAKQgGAWgNASQgNATgRANQgQAMgUAIQgUAGgWABQgggBgZgJgAgThfQgNAEgMAJQgLAIgJAOQgJAOgEAWICTgSIgBgEQgKgZgQgOQgQgOgZABQgJAAgMADg");
	this.shape_16.setTransform(-238.8,5.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(3,1,1).p("EgmpgFPMBNUAAAQCiAAAACjIAAFZQAACjiiAAMhNUAAAQijAAAAijIAAlZQAAijCjAAg");
	this.shape_17.setTransform(0,6.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("EgmpAFQQijAAAAijIAAlZQAAijCjAAMBNUAAAQCiAAAACjIAAFZQAACjiiAAg");
	this.shape_18.setTransform(0,6.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-265.2,-40.2,530.5,81.9);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-2.4,63.4,4.714,4.714);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(0.3,-41.5,4.714,4.714);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("AsYUmQiSheiFiEQnenfAAqjQAAqlHeneQA1g1A2guQBdg9BkguQEBh4EqgWQBBgFBEAAQC1AACnAjQDEAoCyBYQBjAxBdBAQB9BXBzB0QHfHeAAKkQAAKknfHeQg0A1g3AvQmFD+nrAAQnqAAmCj9g");
	this.shape_2.setTransform(-8.3,6.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AK3V/QHfneAAqlQAAqjnfnfQhzhzh9hXQhdhAhjgyQiyhYjDgnQiogji1AAQhEAAhCAEQkqAWkBB4QhkAvhcA9QG9l7JcgBQGKABFICiQDpB2DHDFQHeHfAAKjQAAKlneHfQiCCBiRBgQA3gvA0g1g");
	this.shape_3.setTransform(29.3,-12.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-163.3,-163.3,326.8,326.7);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-2.2,57.4,4.276,4.276);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(0.3,-37.7,4.276,4.276);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("ArOSsQiFhWh4h4QmymyAApkQAApmGymyQAwgwAxgpQBUg4BbgqQDohtEPgUQA8gEA8AAQCkAACYAgQCzAkChBQQBZAsBUA7QBzBOBoBpQGyGyAAJlQAAJlmyGyQgwAwgxAqQlhDnm+AAQm8AAlejlg");
	this.shape_2.setTransform(-7.5,5.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AJ2T9QGzmzAAplQAAplmzmxQhohphyhPQhUg6hZgtQiihQixgkQiYggilABQg9AAg7ADQkPAUjpBtQhaAqhUA4QGTlZIkABQFlAAEqCTQDTBrC0CzQGyGyAAJlQAAJlmyGzQh2B1iDBXQAygrAvgvg");
	this.shape_3.setTransform(26.6,-11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148.1,-148.1,296.4,296.4);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-2.3,63.4,4.714,4.714);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(0.4,-41.5,4.714,4.714);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("AsYUmQiSheiFiEQnenfAAqjQAAqlHeneQA1g1A2guQBdg9BkguQEBh4EqgWQBBgFBEAAQC1AACnAjQDEAoCyBYQBjAxBdBAQB9BXBzB0QHfHeAAKkQAAKknfHeQg0A1g3AvQmFD+nrAAQnqAAmCj9g");
	this.shape_2.setTransform(-8.3,6.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AK3V/QHfneAAqlQAAqjnfnfQhzhzh9hXQhdhAhjgyQiyhYjDgnQiogji1AAQhEAAhCAEQkqAWkBB4QhkAvhcA9QG9l7JcgBQGKABFICiQDpB2DHDFQHeHfAAKjQAAKlneHfQiCCBiRBgQA3gvA0g1g");
	this.shape_3.setTransform(29.3,-12.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-163.3,-163.3,326.8,326.7);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-2.2,57.4,4.276,4.276);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(0.3,-37.7,4.276,4.276);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("ArOSsQiFhWh4h4QmymyAApkQAApmGymyQAwgwAxgpQBUg4BbgqQDohtEPgUQA8gEA8AAQCkAACYAgQCzAkChBQQBZAsBUA7QBzBOBoBpQGyGyAAJlQAAJlmyGyQgwAwgxAqQlhDnm+AAQm8AAlejlg");
	this.shape_2.setTransform(-7.5,5.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AJ2T9QGzmzAAplQAAplmzmxQhohphyhPQhUg6hZgtQiihQixgkQiYggilABQg9AAg7ADQkPAUjpBtQhaAqhUA4QGTlZIkABQFlAAEqCTQDTBrC0CzQGyGyAAJlQAAJlmyGzQh2B1iDBXQAygrAvgvg");
	this.shape_3.setTransform(26.6,-11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148.1,-148.1,296.4,296.4);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-2.2,57.4,4.276,4.276);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(0.3,-37.7,4.276,4.276);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("ArOSsQiFhWh4h4QmymyAApkQAApmGymyQAwgwAxgpQBUg4BbgqQDohtEPgUQA8gEA8AAQCkAACYAgQCzAkChBQQBZAsBUA7QBzBOBoBpQGyGyAAJlQAAJlmyGyQgwAwgxAqQlhDnm+AAQm8AAlejlg");
	this.shape_2.setTransform(-7.5,5.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AJ2T9QGzmzAAplQAAplmzmxQhohphyhPQhUg6hZgtQiihQixgkQiYggilABQg9AAg7ADQkPAUjpBtQhaAqhUA4QGTlZIkABQFlAAEqCTQDTBrC0CzQGyGyAAJlQAAJlmyGzQh2B1iDBXQAygrAvgvg");
	this.shape_3.setTransform(26.6,-11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148.1,-148.1,296.4,296.4);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(161,226.7,4.714,4.714);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(163.7,121.8,4.714,4.714);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("AsYUmQiSheiFiEQnenfAAqjQAAqlHeneQA1g1A2guQBdg9BkguQEBh4EqgWQBBgFBEAAQC1AACnAjQDEAoCyBYQBjAxBdBAQB9BXBzB0QHfHeAAKkQAAKknfHeQg0A1g3AvQmFD+nrAAQnqAAmCj9g");
	this.shape_2.setTransform(155,169.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AK3V/QHfneAAqlQAAqjnfnfQhzhzh9hXQhdhAhjgyQiyhYjDgnQiogji1AAQhEAAhCAEQkqAWkBB4QhkAvhcA9QG9l7JcgBQGKABFJCiQDoB2DHDFQHeHfAAKjQAAKlneHfQiCCBiRBgQA3gvA0g1g");
	this.shape_3.setTransform(192.7,150.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy, new cjs.Rectangle(0,0,326.8,326.7), null);


(lib.question = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-3.4,33,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(-1.9,-26.2,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AqqQzQhyhJhnhnQjXjXhbkLQhCjDAAjeQAAoPF0l0QAqgpAqgkQCChvCWhGQDJhdDngSQAzgDA0AAQCNAACDAbQCZAfCLBFQC0BaCbCbQF1F0AAIPQAAIOl1F1QhlBmhwBJQkuDGmAAAQl9AAktjFg");
	this.shape_4.setTransform(-3,-1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer_4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFCC00").ss(6,1,1).p("Ap3xKITvAAQDBAACJCIQCJCJAADCIAATvQAADCiJCIQiJCJjBAAIzvAAQjCAAiJiJQiIiIAAjCIAAzvQAAjCCIiJQCJiIDCAAg");
	this.shape_5.setTransform(-1.6,-2.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ap3PnQiYAAhshrQhrhsAAiYIAAzvQAAiYBrhsIAAAAIAAAAIAAAAQBshrCYAAITvAAQCYAABsBrIAEAEQBnBrAACVIAATvQAACYhrBrIgEAFQhrBniVAAg");
	this.shape_6.setTransform(-1.6,-2.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CCCCCC").s().p("Ap3RLQjCAAiJiJQiIiIAAjCIAAzvQAAjCCIiJQCJiIDCAAITvAAQDBAACJCIQCJCJAADCIAATvQAADCiJCIQiJCJjBAAgAvmp3IAATvQAACYBrBsQBsBrCYAAITvAAQCVAABrhnIAEgFQBrhrAAiYIAAzvQAAiVhnhrIgEgEQhshriYAAIzvAAQiYAAhsBrIAAAAIAAAAIAAAAQhrBsAACYg");
	this.shape_7.setTransform(-1.6,-2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.question, new cjs.Rectangle(-130.1,-128.4,254.4,254.4), null);


(lib.Option1_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-3.4,33,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(-1.9,-26.2,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy4, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AnmEuQgXAAgQgQQgPgOAAgXIAAgLQAAjhCdidQCfidDgAAQDhAACdCdQCfCdAADhIAAALQAAAXgQAOQgPAQgWAAg");
	this.shape.setTransform(-3.4,33);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(-1.9,-26.2,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4BD7FA").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy3, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Tween14copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.05,scaleY:1.05},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148.1,-148.1,296.4,296.4);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Option1_mccopy3();
	this.instance.parent = this;
	this.instance.setTransform(372,0.4,1.769,1.769,0,0,0,-2.3,-2.5);

	this.instance_1 = new lib.Option1_mccopy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-373,0.3,1.769,1.769,0,0,0,-2.3,-2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-543.9,-171.5,1087.9,343);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.question();
	this.instance.parent = this;
	this.instance.setTransform(3.6,-2.5,1.605,1.605,0,0,0,-2.1,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-202,-204.6,408.4,408.4);


(lib.Symbol6copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween5("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(202,204.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6copy3, new cjs.Rectangle(0,0,408.4,408.4), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween22("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(263.7,40.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,530.5,81.9);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(641.9,351.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(300).to({_off:false},0).wait(51).to({startPosition:5},0).to({alpha:0,startPosition:10},5).wait(1));

	// Layer_12
	this.instance_1 = new lib.Symbol6copy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(642.2,347.2,1,1,0,0,0,204.2,204.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(292).to({_off:false},0).to({scaleX:1.21,scaleY:1.21},18).wait(41).to({alpha:0},5).wait(1));

	// Layer_9
	this.instance_2 = new lib.handanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(251.6,635.7,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(240).to({_off:false},0).to({_off:true},32).wait(85));

	// Layer_8
	this.instance_3 = new lib.arrowanim("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(265.1,316.6,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(208).to({_off:false},0).to({_off:true},32).wait(117));

	// Layer_10
	this.instance_4 = new lib.Symbol4copy();
	this.instance_4.parent = this;
	this.instance_4.setTransform(268,534.5,1,1,0,0,0,163.3,163.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(272).to({_off:false},0).to({regY:163.4,scaleX:0.9,scaleY:0.9,x:644.5,y:344.5},17).wait(62).to({alpha:0},5).wait(1));

	// Layer_7
	this.instance_5 = new lib.Tween14("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(645.7,343.3);
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween15("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(1013.4,533.4);
	this.instance_6.alpha = 0.699;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(145).to({_off:false},0).to({_off:true,x:1013.4,y:533.4,alpha:0.699},15).wait(197));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(145).to({_off:false},15).wait(39).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(152));

	// Layer_6
	this.instance_7 = new lib.Tween16("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(645.7,343.3);
	this.instance_7._off = true;

	this.instance_8 = new lib.Tween17("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(268,533.4);
	this.instance_8.alpha = 0.699;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(145).to({_off:false},0).to({_off:true,x:268,y:533.4,alpha:0.699},15).wait(197));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(145).to({_off:false},15).wait(39).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(152));

	// Layer_13
	this.instance_9 = new lib.Tween14copy("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(645.7,343.3);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(96).to({_off:false},0).wait(103).to({startPosition:23},0).to({alpha:0,startPosition:28},5).to({_off:true},1).wait(152));

	// Layer_5
	this.instance_10 = new lib.Symbol5("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(781.2,107.2,1,1,0,0,0,263.7,41);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(96).to({_off:false},0).wait(103).to({mode:"independent"},0).to({alpha:0},5).to({_off:true},1).wait(152));

	// Layer_4
	this.instance_11 = new lib.Tween6("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,533.8);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(72).to({_off:false},0).to({alpha:1},7).to({startPosition:0},210).to({alpha:0},5).to({_off:true},1).wait(62));

	// Layer_3
	this.instance_12 = new lib.Tween5("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,347.7);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(44).to({_off:false},0).to({alpha:1},7).to({_off:true},243).wait(63));

	// Layer_2
	this.instance_13 = new lib.Tween23("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,111.5);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.instance_14 = new lib.Tween24("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,111.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(20).to({_off:false},0).to({_off:true,alpha:1},6).wait(331));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(20).to({_off:false},6).wait(263).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(62));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(558.5,293,1447,859);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;