(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgdB0IgRgEIgTgGIgTgJQgJgFgJgHIAUgfIAXALIAYAKQAMAEANADQAMACAOAAQALAAAIgCQAIgCAFgDIAIgHIAEgIQABgEgBgDQAAgFgCgEQgCgEgEgEQgEgEgGgDQgHgDgJgCQgKgCgNgBQgRgBgRgCQgRgDgNgHQgOgGgJgJQgIgLgCgPQgCgPAEgMQADgMAHgKQAHgKAKgHQAKgHAMgFQAMgFANgCQANgDANAAIASABIAXAEQAMACAMAFQAMAEALAIIgPAlQgNgHgMgEIgWgGQgLgDgKAAQgfgCgRAIQgSAJAAARQAAANAGAFQAHAGAMACQALADAQABQAOAAASADQAUADAOAGQAOAFAIAJQAIAIAEAKQAEAKAAAKQAAAUgIANQgIANgNAJQgNAIgRAEQgSAEgSABQgSgBgTgDg");
	this.shape.setTransform(177.6,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AguCTQgVgJgOgQQgQgPgIgWQgJgWAAgcQAAgbAJgWQAKgWAQgOQAPgPAVgIQATgIAUAAQANAAAOAEQAMADAOAGQAOAHAMAMIABhoIAnADIgBEsIgqAAIABgZIgNAKIgNAHIgMAFIgMAEQgNADgMABQgXAAgVgIgAgfgjQgNAGgIALQgKALgFANQgFAQAAAQQAAARAFAPQAGAOAJAKQAJALANAGQANAGAPAAQAMAAAMgFQAMgDALgIQAJgIAIgKQAIgKAEgNIAAgrQgEgOgHgJQgJgLgKgIQgLgIgMgEQgMgEgMgBQgPABgNAGg");
	this.shape_1.setTransform(151.4,1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AANCyQgLgFgIgHQgIgHgGgKIgLgTQgJgYgDgdIAGkDIApAAIgBBDIgCA4IgBArIAAAiIgBA3QAAAUAEAOQACAHAEAGQADAHAFAFQAFAEAGADQAHADAJAAIgGApQgOAAgLgFg");
	this.shape_2.setTransform(133.8,-2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgqBwQgUgJgPgQQgOgQgJgWQgJgWABgbQgBgaAJgWQAJgXAOgPQAPgQAUgJQAUgIAWAAQAWAAAUAJQAVAJAOAQQAPAQAJAWQAJAWAAAZQAAAagJAWQgJAWgPAQQgOAQgVAJQgUAJgWAAQgWAAgUgIgAgdhMQgMAIgJAMQgHAMgEAPQgEAPAAAOQAAAOAEAPQAEAPAIAMQAIAMANAIQAMAHAQAAQARAAANgHQAMgIAIgMQAIgMAFgPQADgPAAgOQAAgOgDgPQgEgPgIgMQgIgMgNgIQgMgHgSAAQgRAAgMAHg");
	this.shape_3.setTransform(114.1,5.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA2CcQAHgUACgTIAEghIABghQgCgWgGgPQgHgOgJgKQgKgKgLgEQgLgEgMAAQgKABgMAFQgKAFgMALQgMAJgKATIgBCHIgpABIgDk6IAvgCIgBB8QALgMANgHQAMgHAMgEQANgEALgBQAXAAATAIQASAIAOAQQANAPAJAWQAHAVACAbIgBAeIgBAeIgEAbQgCAOgCAKg");
	this.shape_4.setTransform(88.1,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIABhaIAogCIgBBbIBPgDIgDAnIhNACIgCCxIgqABg");
	this.shape_5.setTransform(53.2,1.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AA/B1IADgdQgZAQgYAHQgXAHgVAAQgOAAgOgEQgOgDgLgIQgKgIgHgMQgGgNAAgQQAAgRAFgNQAFgNAIgKQAJgJAMgHQALgHAOgFQANgEAPgCQAOgDAOAAIAZABIAVADQgDgKgEgKQgDgKgGgIQgHgIgIgFQgJgFgMAAQgIAAgKADQgKACgMAGQgMAGgPALQgPALgQARIgZgeQAUgTASgMQASgLAQgHQAQgHANgCQANgCALAAQASAAAPAGQAPAGALAMQALALAIAPQAIAPAEARQAFARADATQACASAAASQAAAUgDAWQgCAXgFAagAgJAAQgSADgMAIQgMAIgHAMQgGAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVQgKgCgLgCQgLgCgMAAQgSAAgQAFg");
	this.shape_6.setTransform(29.6,4.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA3CcQAGgUACgTIAEghIAAghQgBgWgGgPQgHgOgJgKQgKgKgMgEQgLgEgLAAQgKABgMAFQgLAFgLALQgMAJgKATIgBCHIgpABIgDk6IAvgCIgBB8QAMgMAMgHQAMgHAMgEQANgEALgBQAXAAATAIQASAIAOAQQANAPAJAWQAHAVACAbIgBAeIgBAeIgEAbQgCAOgCAKg");
	this.shape_7.setTransform(3.6,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIABhaIAogCIgBBbIBPgDIgDAnIhNACIgCCxIgqABg");
	this.shape_8.setTransform(-20.2,1.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIAChaIAngCIgBBbIBPgDIgDAnIhNACIgBCxIgrABg");
	this.shape_9.setTransform(-51,1.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgtBuQgVgIgOgQQgPgOgHgWQgIgUAAgbQAAgYAJgWQAJgWAPgRQAPgQAUgJQAVgKAXAAQAVAAATAIQATAJAOAOQAPAPAJAUQAJAUADAXIiwAZQABAPAGALQAGAMAJAIQAIAIAMAEQAMAEANAAQALAAAKgDQALgEAJgGQAJgHAHgJQAGgJADgNIAoAIQgGASgLAPQgKAPgOALQgOALgQAGQgRAGgSAAQgaAAgVgIgAgQhOQgKACgKAIQgKAGgHAMQgIANgDARIB6gOIgBgFQgIgUgNgLQgOgMgUAAQgIABgKADg");
	this.shape_10.setTransform(-73.3,5.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgSA2IgiAhIgCA9IgsADIAKk7IAuABIgIDBIBqhqIAfAZIhMBKIBYBzIgkAbg");
	this.shape_11.setTransform(-96.5,0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgdB0IgRgEIgTgGIgTgJQgJgFgJgHIAUgfIAXALIAYAKQAMAEANADQAMACAOAAQALAAAIgCQAIgCAFgDIAIgHIAEgIIAAgHQAAgFgCgEQgCgEgEgEQgEgEgGgDQgHgDgJgCQgKgCgNgBQgRgBgRgCQgRgDgNgHQgOgGgJgJQgIgLgCgPQgCgPAEgMQADgMAHgKQAHgKAKgHQAKgHAMgFQAMgFANgCQANgDANAAIASABIAXAEQAMACAMAFQAMAEALAIIgPAlQgNgHgMgEIgWgGQgLgDgKAAQgfgCgRAIQgSAJAAARQAAANAGAFQAHAGAMACQALADAQABQAOAAASADQAUADAOAGQAOAFAIAJQAIAIAEAKQAEAKAAAKQAAAUgIANQgIANgNAJQgNAIgRAEQgSAEgSABQgSgBgTgDg");
	this.shape_12.setTransform(-120.9,6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AA/B1IADgdQgZAQgYAHQgXAHgVAAQgOAAgOgEQgOgDgLgIQgKgIgHgMQgGgNAAgQQAAgRAFgNQAFgNAIgKQAJgJAMgHQALgHAOgFQANgEAPgCQAOgDAOAAIAZABIAVADQgDgKgEgKQgDgKgGgIQgHgIgIgFQgJgFgMAAQgIAAgKADQgKACgMAGQgMAGgPALQgPALgQARIgZgeQAUgTASgMQASgLAQgHQAQgHANgCQANgCALAAQASAAAPAGQAPAGALAMQALALAIAPQAIAPAEARQAFARADATQACASAAASQAAAUgDAWQgCAXgFAagAgJAAQgSADgMAIQgMAIgHAMQgGAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVQgKgCgLgCQgLgCgMAAQgSAAgQAFg");
	this.shape_13.setTransform(-146,4.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgSCcQgMgCgNgHQgOgGgNgMIAAAbIgnABIgDk7IAtgBIgBB4QALgLANgGQANgGALgEQANgDALAAQAOAAANAEQAOAEAMAHQANAIAKAKQAKAKAIAMQAHANAEANQAEAPAAAQQgBASgFAPQgEAQgIANQgIANgKAKQgKAKgMAIQgLAHgNADQgMAEgMABQgMgBgNgEgAAAgeQgMAAgLAEQgMAFgIAGQgJAHgHAJQgHAIgDALIgBAxQAEAMAHALQAHAJAJAHQAIAHALADQAKADALAAQAOABANgGQANgFAJgLQAKgKAGgOQAGgNABgQQABgPgFgOQgFgPgJgKQgKgKgNgHQgMgGgPAAIgBAAg");
	this.shape_14.setTransform(-171.4,0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FF0000").ss(3,1,1).p("A+ElYMA8JAAAQBrAABMBMQBNBMAABsIAACpQAABshNBLQhMBNhrAAMg8JAAAQhrAAhNhNQhMhLAAhsIAAipQAAhsBMhMQBNhMBrAAg");
	this.shape_15.setTransform(0,0,0.97,0.97);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("A+EFZQhrAAhNhMQhMhNAAhrIAAipQAAhsBMhMQBNhMBrAAMA8JAAAQBrAABMBMQBNBMAABsIAACpQAABrhNBNQhMBMhrAAg");
	this.shape_16.setTransform(0,0,0.97,0.97);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-213.4,-34.9,427,70);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000066").s().p("AgsCMQgUgHgPgPQgOgPgIgVQgJgWABgaQAAgbAJgUQAJgVAPgOQAPgOATgIQATgHATAAQANAAANADQAMACANAHQANAGAMAMIAChlIAkADIgBEiIgnAAIAAgZQgGAFgHAEIgLAIIgNAEIgLAEQgNADgKAAQgXABgUgJgAgeghQgMAFgJALQgJAKgEANQgGAOAAARQABAQAFAOQAEAOAKAKQAIAKANAGQAMAFAQAAQAKAAAMgDQAMgFAKgHQAJgHAHgKQAIgKAEgMIAAgqQgDgMgIgKQgIgKgJgIQgKgHgNgEQgMgFgLAAQgPAAgMAHg");
	this.shape.setTransform(136.7,1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000066").s().p("AgrBqQgVgIgOgPQgNgOgHgVQgIgTAAgaQABgXAIgWQAIgVAOgQQAQgPATgKQATgIAXgBQAUABASAHQASAJAOAOQAOAOAJASQAJAUADAWIiqAZQACAOAFAKQAFALAJAIQAIAIAMAEQAMAEAMAAQAKAAAKgDQALgEAIgFQAJgHAGgJQAGgJADgMIAmAHQgFASgKAOQgKAPgOAKQgNALgQAGQgQAFgRAAQgaAAgTgHgAgQhMQgJADgJAHQgKAHgIALQgHAMgCARIB1gPIgBgDQgIgTgNgMQgNgLgTAAQgIABgKACg");
	this.shape_1.setTransform(112.4,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AhrifIArgCIAAAdQALgLAMgGIAWgJQAMgEALgBQANAAANAEQAOAEALAHQAMAHAKAKQAKAKAHAMQAHAMAEAOQAEAOgBAPQAAARgFAPQgEAPgHAMQgIAMgKAKQgKAKgLAHQgLAHgMAEQgMADgLAAQgLAAgNgDQgLgDgNgGQgNgGgMgLIgBCGIglABgAgWh2QgLAEgIAHQgJAHgGAJQgGAJgDAKIAAAmQADANAGAKQAGAKAJAFQAIAHAKADQAKAEALAAQAMAAAMgFQAMgFAKgJQAJgJAFgNQAFgNABgOQABgOgEgOQgFgNgJgKQgIgKgNgGQgMgGgOAAQgMABgKAEg");
	this.shape_2.setTransform(87.9,10.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000066").s().p("AhrifIArgCIAAAdQALgLAMgGIAWgJQAMgEALgBQANAAANAEQAOAEALAHQAMAHAKAKQAKAKAHAMQAHAMAEAOQAEAOgBAPQAAARgFAPQgEAPgHAMQgIAMgKAKQgKAKgLAHQgLAHgMAEQgMADgLAAQgLAAgNgDQgLgDgNgGQgNgGgMgLIgBCGIglABgAgWh2QgLAEgIAHQgJAHgGAJQgGAJgDAKIAAAmQADANAGAKQAGAKAJAFQAIAHAKADQAKAEALAAQAMAAAMgFQAMgFAKgJQAJgJAFgNQAFgNABgOQABgOgEgOQgFgNgJgKQgIgKgNgGQgMgGgOAAQgMABgKAEg");
	this.shape_3.setTransform(61.8,10.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000066").s().p("AgoBrQgTgIgPgPQgOgPgIgWQgIgVAAgaQAAgZAIgVQAIgWAOgPQAPgPATgJQATgHAVgBQAVABAUAIQATAJAOAPQAPAQAIAVQAIAVAAAYQAAAZgIAVQgIAVgPAPQgOAQgTAJQgUAIgVAAQgVABgTgJgAgchJQgMAIgIALQgHALgEAPQgEAPAAANQAAAOAEAOQAEAPAIALQAIAMAMAHQAMAHAPAAQAQAAANgHQAMgHAHgMQAIgLAEgPQAEgOAAgOQAAgNgEgPQgDgPgIgLQgIgLgMgIQgMgHgRAAQgQAAgMAHg");
	this.shape_4.setTransform(36.1,5.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000066").s().p("AhYAxIgCgxIgBgmIgBgcIgBgqIAtgBIAAAZIAAAXIAAAXIAOgTQAIgLALgKQAKgKALgHQANgHAOgCQAPAAANAHQAGADAFAGQAGAFAEAIQAFAIADALQADAMABAPIgmAOIgCgQIgEgMIgFgJIgGgFQgHgFgIAAQgGABgGAEQgGADgGAGIgNAOIgMAQIgMAPIgKAOIABAdIABAaIAAAXIABAPIgqAGIgCg9g");
	this.shape_5.setTransform(14.1,5.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000066").s().p("AgsCMQgUgHgOgPQgPgPgIgVQgJgWAAgaQABgbAIgUQAKgVAPgOQAPgOATgIQATgHATAAQANAAANADQALACAOAHQANAGAMAMIAChlIAlADIgBEiIgpAAIAAgZQgFAFgHAEIgLAIIgNAEIgLAEQgMADgMAAQgWABgUgJgAgeghQgMAFgIALQgJAKgGANQgEAOAAARQgBAQAGAOQAFAOAJAKQAJAKAMAGQANAFAOAAQAMAAALgDQAMgFAJgHQAKgHAHgKQAIgKADgMIABgqQgEgMgHgKQgIgKgKgIQgJgHgMgEQgMgFgMAAQgOAAgNAHg");
	this.shape_6.setTransform(-12,1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000066").s().p("AgcBvIgQgDIgSgHIgSgIQgJgFgJgGIATgeIAXALQALAFAMAEQALAEANADQALACANAAQALAAAIgCQAHgDAFgCQAFgDADgFIADgHIABgGIgCgIQgCgFgEgDQgEgEgGgDQgGgEgJgBIgWgDQgQAAgRgDQgQgEgNgFQgNgHgIgJQgJgKgBgPQgCgOADgLQAEgMAGgJQAHgJAJgIQAKgGAMgFQALgFANgCQAMgCAMAAIASAAIAWAEQAMACALAEQAMAFAKAIIgOAkQgNgHgLgFIgVgFQgLgDgJgBQgegCgRAJQgRAIAAARQAAAMAGAFQAHAGALACQALACAPABIAfADQATAEANAFQAOAFAIAIQAIAIADAKQAEAJAAAKQAAATgIANQgHAMgNAJQgNAHgQAEQgRAEgRABQgRgBgTgDg");
	this.shape_7.setTransform(-46.5,5.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000066").s().p("AgXgWIhEACIABgkIBEgCIABhXIAmgCIgBBYIBMgDIgDAlIhJACIgCCrIgpABg");
	this.shape_8.setTransform(-66.9,1.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000066").s().p("AgZCUIABgzIACg9IABhUIAngCIgBAzIgBArIAAAjIAAAaIgBArgAgKhdIgJgGQgEgEgCgFQgCgFAAgGQAAgGACgFQACgFAEgEQAEgEAFgBQAFgDAFAAQAGAAAEADQAGABADAEQAFAEABAFQADAFAAAGQAAAGgDAFQgBAFgFAEIgJAGQgEACgGAAQgFAAgFgCg");
	this.shape_9.setTransform(-81.3,0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000066").s().p("AgVBtQgSAAgMgGQgNgGgJgKQgIgLgFgNQgFgNgEgPQgCgPgBgPIgBgdQAAgVACgUQACgWADgVIAoABQgDAYgBAUIgBAiIAAARIABAVIAFAWQACAMAGAIQAEAJAIAFQAHAEAMgBQAOgCARgTQAQgTASglIgBgyIgBgdIgBgQIArgFIACA9IABAwIACAnIABAcIABAqIgtAAIgBg4QgGAMgJAKQgIAKgKAIQgJAIgLAEQgJAFgLAAIgCgBg");
	this.shape_10.setTransform(-99.3,5.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000066").s().p("AhYAxIgCgxIgBgmIgBgcIgBgqIAtgBIAAAZIAAAXIAAAXIAOgTQAIgLALgKQAKgKALgHQANgHAOgCQAPAAANAHQAGADAFAGQAGAFAEAIQAFAIADALQADAMABAPIgmAOIgCgQIgEgMIgFgJIgGgFQgHgFgIAAQgGABgGAEQgGADgGAGIgNAOIgMAQIgMAPIgKAOIABAdIABAaIAAAXIABAPIgqAGIgCg9g");
	this.shape_11.setTransform(-121.7,5.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000066").s().p("Ag3BXIABgzIAAgmIAAgOIgsgBIAAglIAvAAQACgUAGgTQAGgTALgPQAMgPAQgJQARgJAYAAQANAAAOAFQAPAEAOANIgTAgQgLgHgKgDQgKgDgIgBQgJgBgIACQgIACgGAEQgGAEgEAIQgEAIgDALQgDAMgCARIBSAAIgEAnIhQgBIAAAQIgBA1IAAAZIAAAbIAAAbIAAAbIgpAAIABhJg");
	this.shape_12.setTransform(-140.9,0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#003399").ss(3,0,0,6.9).p("A24kaMAtxAAAQCFAAAACEIAAEtQAACEiFAAMgtxAAAQiFAAAAiEIAAktQAAiECFAAg");
	this.shape_13.setTransform(0.1,0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("A24EbQiEAAAAiEIAAktQAAiECEAAMAtxAAAQCFAAAACEIAAEtQAACEiFAAg");
	this.shape_14.setTransform(0.1,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-161.2,-31.6,322.5,63.2);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#3B8F05","#3A8C05","#286405","#215405","#276105","#358205","#3B8F05"],[0,0.024,0.357,0.529,0.667,0.918,1],-39,-5,138.1,-5).s().p("AiCClQjqjsgWlFIAAgWQADAyAKA0IgCg7QgBgXADgWQAEAjAKAgQAGAfAKAcIgVAcQAeB6A6BqIAbg4QA6BYBRBMQCtCmDcA2QACAcAAA4IAFAdQAyALAwAHQkqgijcjeg");
	this.shape.setTransform(-49.7,48.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6FAA15").s().p("AC2FuQAAgJgBgIQgFhAgEgbQgGgwgMgTIgXgbQgPgUgDgMQgCgVARglQATgqAAgRQAAgMgFgjQgGgfAGgWIABgHQAGgRASgpQDtANDdgyQgDAIgKALQgRAOAAANQAAANASASIAQASIAGALQAJAlgbAfQgnAfgOAUQgTAaANAeQAUAsADATIghAjIg9BCQgNAGgOAIQgyAagOAXQgJAOACATQhMAOhOAAQgcAAgZgCgAl1B7QhRhMg6hXQATgjALgLQALgQArgbQAjgXAHgZQAIgRgOhEIAbAKQB7AqCDAZQAFAZAVAjQAKATAFAOQAMAggDAVQgEAbgxA9QgsA1AFAiQAGAkA2AiIBNAwQAeAdAFA2IABAHQjbg2iuingApvkrQgJghgEgiIAAgBIBJAjQAAAWgQAfQgGAFgPAZg");
	this.shape_1.setTransform(-23.4,42.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#2FBA05","#21C205","#21C205","#38CF05","#61E505","#70ED05"],[0.012,0.51,0.51,0.635,0.875,1],-148.9,25.6,28,25.6).s().p("AgsgVIBZghQggA2gjA3QgZgkADgog");
	this.shape_2.setTransform(60.3,21.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#388008").s().p("AiHDfIAHgJQAcgZAagbQAIgIAKgHQARgSAHgTQBKhsAoiSQANgyAKg4QAdgPAbgQQgCAIgGAJQgbAzAEAVQAEAOAQARIAKALQhEDIiiCbQgrAsgvAhQAGglATgWg");
	this.shape_3.setTransform(67.8,34.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#93FF05","#86F205","#65D005","#63CE05","#69D605","#88FD05"],[0,0.173,0.486,0.498,0.58,1],-170.5,-1.4,6.6,-1.4).s().p("AgShOIAIAIQAlAdAIAfQAEAVgSAlQgaAQgdAPQAMhLAEhSg");
	this.shape_4.setTransform(81.8,1.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#5ECF31","#61D035","#6AD340","#79D753","#8EDD6E","#A9E691","#CAEFBB","#DAF4CF"],[0.004,0.173,0.329,0.486,0.639,0.788,0.937,1],-0.2,-67.1,-2.2,135.6).s().p("AjTFFIAhgjIAAAEQABAshfA1gAB+BeQgGgUgTgiIgrg8IgEgFQAlg5Agg2IAYgvQA4hrAfhkQAMAPAbASQgEBSgMBMQgKA3gNAzQgoCShLBrQANgegGgkg");
	this.shape_5.setTransform(52.6,29.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#032B3F").s().p("AAAAYQgLgCgIgHIgDgBIgDgEQgEgFgBgEQAAgJALgHIAAgBQAFgDAGgBQADgDAEAAIABAAIACAAQALAAAKAHIAFAFQACAGABAGIgCAJQgDAEgDABQgEAFgGAAIgEACIgHACIgCAAg");
	this.shape_6.setTransform(10.3,-57.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#82E93E").s().p("AtWGzIAAhfIADAKIA7lGQBBihCBiGQAOgPAPgMQD8juFbAAQFuAAEFEJQAYAYAWAbQBgByA0CCIAEAVQgRgrgVgpQgVgJg2gMQgHAAgEgCQgNgTgPgQQgagjgfgfQhChGhJgyQAiABATAHQAKAEAaAQQASAOAVAFIA1ADQhohziMhOQgLAKgMAGQgSAJgbAbIgNAMQhqgth0gPQADgKAFgMQAOgqgKgXIgLgNQg3gIg5AAQiPAAiKAsIAEAGQAmAbBGgIQAmgDAvgIQARAEAVARQASASALAEQhfADhWAUQgGgFgFgDQgcgMgTAGQgZAOgVAEQgrAMgogEIhDgLQhQAyhDBAQgfAdgdAhQhdBpg6CBQArgGAhgOQh/DGgKD6IAAABQgCAWAAAWIACA7QgJg0gEgyg");
	this.shape_7.setTransform(-3,-37.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#A2DB4D").s().p("AgKIbIAAgDQASgogBgZQAAgKgMgbQgKgSAFgRQADgKATgPQATgNADgHQAGgYgchAQgbg+AIggQAEgQAZglQAagjAGgUQACgKgFgkQgDgeAJgMQAEgLASgKQATgKAFgGQAdgggBgXQgFgfACgjQADgMATguQAOgdgGgUIACAAIAIgBIAbBDQAhBNASAeQAQAaAGA3QAIA4ANAZIAWAjQALASAFARQADAHAAAiQACAYALASQAGAIAeAaQAaARADAUQADAMgOAaQgPAaAAAJQgCAeAiAwQAlAtgEAfIAAAEQixAoi7AAQgvAAgugCgAofG0IgbgJIgBgHQgLg9AegZIAJgEQATgKAugPQAqgNABggIgWhbQgHgyAxgRQANgDBAAAQAmAAANgeIAThbQAOg+AkgPQAOgFAcAHQAeAIAKgBQAVgHADgGQACgCAJgXQAJgMAMghQAMgZAWgHIAigKQAVgFAegLQAsgQAyg3IACAEQgnBTgMAPQgRAOgJAZQgLAdgGAIQgSAYg0AEQg7AGgSAOQgWAPADASQAAANAGAeQgCA2goAhQg4AsgOAZQgTAkAdA/QAeBIgDAWQgFAYgtAjQgpAigFAdQgCAPADASQiDgah7gqgAJrGhQAbhEAKgdQAQg2gfguQgSgjhNgvQhDgrgOgqQgGgQADgoQADgmgJgRQgLgcgHgDQgNgBgRgJQgmgMgQgMQgigXgRg0QgSg1g/gcIgzgOQAGAAAFgEQADgBACgEQA6AABCAXQARAGAdgCQAegDAOAFQAcALAPAqQAPAxAJAJQAOAQAPAAIAkgDQAxAKAUAjQAjBFADADQATAUA6AJIAMADQAvAKAPARQAPAWgBAqQgGAvAJASQAEAEAEAGQgfBkg4BrIgYAvIhbAiQACgTADgSgAsuFCQAJj6B/jGQAhgNAWgWQATgPARgdQAKgXARgKQAFgEAMgBIAPgBQAYAAAEgEQARgKAsg3QAjgyAmgMQAYgGAqAEQAyAGAUgDIAugMQAdgIASACQAMACASANQATAOAIADQAnARATgIQAdgPAdADQAWABAXASIAhAgQgbgDg7AVIhNAXQgVAFg5gIQgwgHgdAMQgrAagbALQgcACgMADQgYADgQARQgJAJgTAhQgOAdgOAIQgLAFgZgDQgcgCgNAEQgeAQgVA2IgFAQQgTAugPAOQgFAFgXADQgTADgJALQgKANAEAXQADAegCAJQgFAjgnAUQg2AegLAOQgbAfALAhQAFAVARAvIAAAHIhJgjgALZi1IgfgPQgOgHgpglQgggegcgJQgPgDgfgCQggAAgNgFQgNgEgNgPQgOgQgJgFQgpgSgSAJQgdATgXACIhTgCQglAAgXAHQAAgHgCgFQA/gkBugpQAhgMArAIQA+ALANAAIAWAAQBJAyBCBGQAfAfAaAjQAPAQANATIgcgIgABFmKQgVgOgkAAQgogEgKgFQgigLgbgoQgXgigUgPQBWgUBfgDIADABQABACAcAEQAWACAFAJQAFAFgCAMQgCAPABAEQABAXAQAEQAZAFAPATQAPAagWBMQgHABgEADIAAABQgggrgmgXgACdlQIADgHIgCAHgACulvQAXgrAAgSQABgUgNgQQgTgaAAgHQgGgMAIgYQBzAPBqAtQgTAPgQAGQgPAGgkAJQgfAFgVAMQg+ArgMALQgGAGgCAGQACgGADgHg");
	this.shape_8.setTransform(-5.6,-26.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#2D6508").s().p("AADOLIgDAAIgCAAIgIAAIgWAAIgOgCIAAACIgfgDIgrgEQgwgHgygLIgFgdQAAg4gCgcIgBgHQgFg2gfgdIhNgwQg2gigFgkQgGgiAsg0QAxg+AEgbQADgWgMgfQgFgPgKgTQgVgjgEgZQgEgRACgPQAFgeAqghQAsgkAFgYQAEgVgfhHQgdg/ATgkQAOgZA5gtQAoggABg2QgFgfAAgNQgEgSAXgPQASgOA7gGQA0gEASgYQAHgJAKgcQAJgZAQgOQAMgPAohUIADABQAIAIALABQAGAUgOAdQgTAtgDAMQgCAkAFAeQACAYgeAgQgEAGgUAKQgRAKgEALQgJAMAEAeQAFAlgCAKQgGAUgbAjQgaAkgDARQgJAgAcA9QAdA/gGAZQgDAGgUAOQgTAPgDAKQgGARAKASQAOAbAAAKQABAZgTAoIAAACQgTApgFASIgCAHQgFAVAFAfQAGAlgBAMQABAQgUAqQgRAmADAUQACAMAQAUIAWAbQAMATAGAxQAEAbAFA/QACAIgBAKQAZABAcAAQBNAABMgOQgBgTAIgOQAPgXAxgaQAOgIANgFQBfg1gBgsIAAgEQgDgUgUgsQgMgdATgbQANgUAnggQAbgfgJglIgFgKIgRgTQgSgRAAgOQAAgMARgPQAKgLADgIIAAgEQAFgfglgtQgjgxACgdQABgKAPgZQANgagDgMQgDgTgZgSQgegagGgHQgMgSgCgYQAAgigDgHQgFgRgLgSIgVgjQgOgZgHg6QgGg3gRgaQgSgdgghNIgchEIAEgCIAzANQA/AcARA1QARA1AiAXQARAMAlAMQARAJANAAQAIAEALAcQAIARgDAmQgDApAGAPQAPAqBDAsQBMAvATAjQAfAtgQA2QgKAdgbBEQgEARgCAUQgCAoAYAkIAFAGIArA9QASAhAGAVQAGAkgNAeQgGAUgSARQgJAHgJAIQgaAbgcAaIgHAJQgTAWgGAkQAvghArgrQCkicBDjJIgKgLQgPgRgEgNQgFgVAbgzQAGgKACgHQASglgEgXQgIgegmgdIgIgHQgbgSgMgPQgEgGgEgEQgJgSAGgvQACgrgPgVQgQgRgugMIgNgCQg6gKgSgTQgEgDgihFQgUgjgxgKIglADQgPAAgNgRQgJgIgQgxQgOgqgdgLQgNgFgeADQgdACgSgHQhBgWg6AAIACgKQAWgHAlAAIBUABQAWgBAegTQASgJApASQAJAEANARQANAPANAEQAOAFAgAAQAfABAPADQAcAJAfAfQAqAlANAHIAgAPIAcAIQAEABAGAAQA2ANAWAJQAVApARAsQA8CagBCqIAAAFIAAAIIAAAFQABAwgGA1IgJA4QgPBaghBTIgDAKQhACgiACDQj+D+lfAJgAtpDUIAVgcQACgFAEgEQAQgYAFgGQAQgfAAgWIAAgHQgQgugFgVQgLggAbgfQAKgOA3geQAngUAFgjQABgJgCgfQgFgWAKgOQAJgKATgDQAXgDAGgFQAPgOASgvIAGgRQAUg2AfgPQAMgFAcADQAZADALgGQAPgHANgdQATghAJgJQARgRAYgDQALgDAcgCQAbgLArgaQAegMAwAHQA4AIAVgGIBPgWQA5gVAcADIghghQgWgRgWgCQgegCgeAOQgSAJgngSQgJgDgSgNQgSgOgMgBQgTgCgdAHIguANQgTADgzgGQgpgEgZAGQgmAMgiAyQgsA3gSAKQgDADgZAAIgPABQgMACgFAEQgQAJgLAYQgRAdgSAPQgXAWghANQggANgrAIQA6iCBdhpQAcghAfgdQBEhABQgyIBDALQAoAEAqgNQAWgDAYgOQAUgHAcAMQAFAEAGAFQATAPAYAhQAbApAhAKQALAGAoAEQAkAAAUAOQAmAWAgAsQgLAHAAAJQABAEAEAGQgxA3gsAQQgdALgXAFIghAKQgWAGgMAaQgMAggKAMQgIAYgDACQgDAGgUAGQgLABgegHQgcgHgNAFQglAPgNA+IgTBcQgOAdgmAAQg/ABgNADQgyARAIAyIAVBaQgBAggpANQguAPgUAJIgJAFQgdAZAKA9IABAHQAOBDgIARQgHAZgjAXQgrAbgLAQQgLALgTAjIgbA4Qg6hqgeh6gABypRQgKgIgLAAIgDAAIADgGIgEAGQgEAAgDADQAWhMgPgZQgPgTgYgGQgQgDgBgYQgCgDADgPQABgMgEgGQgFgIgWgDQgbgDgCgCIgDgCQgKgDgTgSQgVgSgRgDQgvAIgmADQhGAHgmgaIgEgGQCKgtCQAAQA4AAA3AIIALAOQAKAXgOAqQgGAMgCAJQgHAZAFAMQABAGASAbQAOAQgBAUQAAARgYAsQgDAGgBAHQABgGAGgGQAMgLA/grQAUgNAfgEQAkgJAPgHQARgFASgPIAOgMQAbgbARgKQAMgFALgKQCMBNBoBzIg0gCQgVgFgSgOQgagQgKgFQgUgGghgBIgWAAQgNAAg/gMQgqgHghAMQhvApg/AjIgFgEg");
	this.shape_9.setTransform(1.2,0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88.5,-90.9,177,181.9);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(2,1,1).p("Ap2pQQgYAaADAmQAEAnAeAcIRbQaQAeAcAnABQAnACAZgaQAYgbgDgnQgEgmgegcIxbwaQgegcgngCQgngBgZAbg");
	this.shape.setTransform(162.2,3.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCCCC").s().p("AIrI+QgQgCgMgLIgBgBIxcwaQgNgMgCgRIAAgCQgDgWAOgOIABgCQAOgPAUABQAFAAAFABQANADAKAKIABAAIRcQaQAPAOAAATQACATgNAPIgCACQgJAMgTABIgGABIgEAAg");
	this.shape_1.setTransform(162.2,3.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999999").s().p("AI3JqQgngBgegcIxbwaQgegcgEgnQgDgmAYgaQAZgbAnABQAnACAeAcIRbQaQAeAcAEAmQADAngYAbQgXAZglAAIgEgBgApRovIgBACQgOAOADAWIAAACQACARANAMIRcQaIABABQAMALAQACQAGAAAEgBQATgBAJgMIACgCQANgPgCgTQAAgTgPgOIxcwaIgBAAQgKgKgNgDQgFgBgFAAIgCAAQgSAAgOAOg");
	this.shape_2.setTransform(162.2,3.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(2,1,1).p("Ah/AAQAAA1AlAmQAmAlA0AAQA1AAAmglQAlgmAAg1QAAg0glgmQgmglg1AAQg0AAgmAlQglAmAAA0gAjHAAQAABTA7A7QA6A6BSAAQBTAAA7g6QA6g7AAhTQAAhSg6g7Qg7g6hTAAQhSAAg6A6Qg7A7AABSg");
	this.shape_3.setTransform(104.9,-51);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AhaBbQglgmAAg1QAAg1AlglQAmglA0AAQA1AAAmAlQAlAlAAA1QAAA1glAmQgmAlg1AAQg0AAgmglg");
	this.shape_4.setTransform(104.8,-51);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQglAmAAA0QAAA1AlAmQAmAmA0AAQA1AAAmgmQAkgmABg1QgBg0gkgmQgmglg1AAQg0AAgmAlg");
	this.shape_5.setTransform(104.9,-51);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#330000").ss(2,1,1).p("Ah/AAQAAA1AlAmQAmAlA0AAQA1AAAmglQAlgmAAg1QAAg0glgmQgmglg1AAQg0AAgmAlQglAmAAA0gAjHAAQAABTA7A7QA6A6BSAAQBTAAA7g6QA6g7AAhTQAAhSg6g7Qg7g6hTAAQhSAAg6A6Qg7A7AABSg");
	this.shape_6.setTransform(104.9,-51);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQglAmAAA0QAAA1AlAmQAmAmA0AAQA1AAAmgmQAkgmABg1QgBg0gkgmQgmglg1AAQg0AAgmAlg");
	this.shape_7.setTransform(104.9,-51);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#666666").ss(2,1,1).p("AJ3pQQAYAagDAmQgEAngeAcIxbQaQgeAcgnABQgnACgZgaQgYgbADgnQAEgmAegcIRbwaQAegcAngCQAngBAZAbg");
	this.shape_8.setTransform(-161.3,3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CCCCCC").s().p("Ao1I9QgSgBgKgMIgBgBQgNgPACgUQAAgSAPgOIRcwaIAAgBQALgKANgCQAFgCAFAAQAUAAAOAPIABABQAOAOgDAWIAAACQgCARgNANIxcQaIgBAAQgMALgQACIgEAAIgHgBg");
	this.shape_9.setTransform(-161.3,3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#999999").s().p("Ap2JSQgYgbADgnQAEgmAegcIRbwaQAegcAngCQAngBAZAbQAYAagDAmQgEAngeAcIxbQaQgeAcgnABIgEABQglAAgXgZgAIwo9QgFAAgFABQgNADgKAKIgBAAIxcQaQgPAPAAASQgCAUANAOIABACQAKAMASABQAFABAGAAQAQgCAMgLIABgBIRcwaQANgMACgRIAAgCQADgWgOgOIgBgCQgOgOgTAAIgBAAg");
	this.shape_10.setTransform(-161.3,3.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#330000").ss(2,1,1).p("ACAAAQAAA1glAmQglAlg2AAQg0AAglglQgmgmAAg1QAAg0AmgmQAlglA0AAQA2AAAlAlQAlAmAAA0gADIAAQAABTg6A7Qg7A6hTAAQhSAAg6g6Qg7g7AAhTQAAhSA7g7QA6g6BSAAQBTAAA7A6QA6A7AABSg");
	this.shape_11.setTransform(-103.9,-51);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AhaBbQglgmAAg1QAAg1AlglQAmglA0AAQA1AAAmAlQAlAlAAA1QAAA1glAmQgmAlg1AAQg0AAgmglg");
	this.shape_12.setTransform(-103.9,-51);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQgkAmgBA0QABA1AkAmQAmAmA1AAQA0AAAmgmQAlgmAAg1QAAg0glgmQgmglg0AAQg1AAgmAlg");
	this.shape_13.setTransform(-103.9,-51);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#330000").ss(2,1,1).p("ACAAAQAAA1glAmQglAlg2AAQg0AAglglQgmgmAAg1QAAg0AmgmQAlglA0AAQA2AAAlAlQAlAmAAA0gADIAAQAABTg6A7Qg7A6hTAAQhSAAg6g6Qg7g7AAhTQAAhSA7g7QA6g6BSAAQBTAAA7A6QA6A7AABSg");
	this.shape_14.setTransform(-103.9,-51);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQgkAmgBA0QABA1AkAmQAmAmA1AAQA0AAAmgmQAlgmAAg1QAAg0glgmQgmglg0AAQg1AAgmAlg");
	this.shape_15.setTransform(-103.9,-51);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#660000").ss(2,1,1).p("EAgCgJ/IABAAQBGgaBBg7QAUgTAVgXQAOgQANgRQA5hKAhhVQAphsgRhOQgThVhXgUMhGrAAAQhWAUgTBVQgRBOAoBsQAhBVA5BKQANARAPAQQAUAXAVATQBAA7BGAaIABAAMBADAAAIjEX0QgfEkkmBJMgvxAAAQklhJggkkIjE30EAgDgKJIgBAKATGmJQBLAAAyA1QAxA1gFBLIhIOlQgFBDg2AuQg1AvhGAAQhHAAgvgvQgZgZgLgeQgJgbACgfIA1ulQAEhLA4g1QApgnAzgKQAUgEAVAAgAJ2mJQBMAAAzA1QAzA1gDBLIgpOlQgDBDg1AuQgzAvhHAAQhGAAgxgvQgqgogEg4QgBgIAAgJIAXulQAChLA2g1QAvguA+gGQALgBALAAgAABmJQBMAAA1A1QA1A1gBBLIgJOlQgBBDgyAuQgyAvhHAAQhGAAgygvQgygugBhDIgJulQgBhLA1g1QA1g1BLAAgEggCgKJIABAKApumJQALAAALABQA+AGAvAuQA2A1ACBLIAXOlQAAAIAAAIQgEA4grApQgxAvhGAAQhHAAg0gvQgzgugDhDIgqulQgDhLAzg1QAzg1BMAAgAy9mJQAUAAAUAEQAzAKApAnQA4A1AFBLIA1OlQABAfgJAbQgLAegZAZQgvAvhGAAQhHAAg1gvQg2gugFhDIhHulQgGhLAxg1QAyg1BMAAg");
	this.shape_16.setTransform(-0.1,50);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#B60B09").s().p("A3xPoQj+hCgcj+IgBgBIi828MA+RAAAIi9W8IAAABQgcD+j+BCgA7BKdIAAACQAXDHDCA3MAvQAAAQDDg3AXjHIAAgCICy1nMg7nAAAgEgiJgOcQgUgTgUgXIgcghMBGbAAAIgcAhQgUAXgUATg");
	this.shape_17.setTransform(0,70);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#940907").s().p("A34PcQklhKggkkIjE3zMBADAAAIjEXzQgfEkkmBKgA8MJoIABABQAcD+D+BCMAvjAAAQD+hCAcj+IAAgBIC928Mg+RAAAgAP7I8QgZgZgLgeQAnAcA1AAQBDAAAygsQAygsAEg/IA7txQAEhFgwgyQgigkgugKQAUgEAVAAQBLAAAyA1QAxA2gGBKIhHOlQgFBDg2AvQg1AuhHAAQhGAAgvgugAHTI8QgqgpgEg3QAIANANAMQAuAsBDAAQBDAAAwgsQAxgsACg/IAitxQAChFgvgyQgrgrg7gGIAWgBQBMAAAzA1QAzA2gDBKIgpOlQgDBDg1AvQgzAuhHAAQhGAAgxgugAh3I8QgygvgBhDIgJulQgBhKA1g2QA1g1BLAAQBMAAA0A1QA2A2gBBKIgJOlQgBBDgzAvQgxAuhHAAQhGAAgygugAh3peQgzAyACBFIAHNxQABA/AvAsQAwAsBCAAQBDAAAvgsQAwgsAAg/IAItxQABhFgzgyQgxgxhIgBQhFABgyAxgAq9I8Qg0gvgChDIgqulQgDhKAzg2QAzg1BMAAIAVABQg7AGgqArQgwAyADBFIAhNxQACA/AxAsQAwAsBDAAQBDAAAvgsQAMgNAJgNQgDA4grApQgxAuhGAAQhHAAg0gugAzkI8Qg2gvgFhDIhIulQgFhKAxg2QAyg1BLAAQAVAAAUAEQgvAKgjAkQguAyAFBFIA5NxQAEA/AyAsQAyAsBDAAQA2gBAngcQgLAfgZAZQgvAuhHAAQhGAAg1gugEAgCgOFIABgKIgBAKMhADAAAIgBgKIABAKIgBAAQhGgahBg8MBETAAAQhBA8hGAagEggBgOFg");
	this.shape_18.setTransform(0,76.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#D70503").s().p("A3oSjQjCg3gXjGIAAgCIiy1nMA7nAAAIiyVnIAAACQgXDGjDA3gASdlHQgzALgpAmQg4A2gEBKIg1OlQgCAgAJAbQALAeAZAZQAvAuBHAAQBGAAA1guQA2gvAFhDIBIulQAFhKgxg2Qgyg1hLAAQgVAAgUAEgAJglKQg+AHgvAtQg2A2gCBKIgXOlIABASQAEA3AqApQAxAuBGAAQBHAAAzguQA1gvADhDIApulQADhKgzg2Qgzg1hMAAIgWABgAh/kWQg1A2ABBKIAJOlQABBDAyAvQAyAuBGAAQBHAAAyguQAygvABhDIAJulQABhKg1g2Qg1g1hMAAIAAAAQhLAAg1A1gArtkWQgzA2ADBKIAqOlQADBDAzAvQA0AuBHAAQBGAAAxguQArgpAEg4IAAgRIgXulQgChKg2g2Qgvgtg+gHIgWgBIAAAAQhMAAgzA1gA07kWQgxA2AGBKIBHOlQAFBDA2AvQA1AuBHAAQBGAAAvguQAZgZALgfQAJgagBggIg1ulQgFhKg4g2QgpgmgzgLQgUgEgUAAIgBAAQhLAAgyA1gEgjNgLhQg5hJghhWQgohrARhOQAThVBWgVMBGrAAAQBXAVATBVQARBOgpBrQghBWg5BJg");
	this.shape_19.setTransform(-0.1,43.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-238.3,-76,476.5,252), null);


(lib.InstructionText_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgaBnIgPgDIgRgGIgQgIQgJgEgIgGIASgcIAUAKQALAFALAEQALAEALACQALACANAAQAJAAAIgBQAGgDAFgDIAHgFIADgHQABgDAAgEQAAgEgCgEIgFgHQgDgDgHgDQgFgDgJgCQgIgCgMAAQgQgBgPgCQgPgDgMgGQgMgFgHgJQgJgJgBgOQgCgNAEgLQACgLAHgIQAGgJAJgGQAJgHALgEQAKgFAMgCQAMgCALAAIARABIAUACIAWAHQAKAEAKAHIgNAiQgMgHgLgDIgTgGQgKgDgJAAQgbgCgQAIQgRAHABAQQgBALAHAGQAFAEALADIAYADQANAAAQACQASADAMAGQANAEAHAIQAIAHACAJQAEAJAAAJQAAASgHAMQgHAMgMAHQgMAHgPAEQgPAEgRAAQgQAAgRgEg");
	this.shape.setTransform(262.6,42);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgpCDQgSgIgOgOQgNgNgIgUQgIgUAAgYQAAgZAJgTQAIgTAOgOQAOgNASgHQASgHASgBQALABAMADQALACANAGQAMAHALAKIABhdIAjACIgBENIglAAIAAgWIgLAIIgMAHIgLAFIgLADQgLADgKAAQgWAAgSgHgAgbgfQgMAFgIAKQgIAKgFAMQgEANAAAPQAAAPAEANQAFANAIAKQAJAJALAFQAMAGAOAAQAKgBALgDQALgEAJgHQAJgGAGgJQAHgKAEgLIAAgnQgDgMgHgIQgHgLgKgGQgJgIgLgDQgLgEgLAAQgNAAgLAGg");
	this.shape_1.setTransform(239.2,37.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AALCfQgKgEgGgGQgIgHgFgJQgGgIgDgJQgJgVgCgaIAFjoIAlAAIgCA8IgBAyIgBAnIAAAeIgBAyQAAARAEANIAFAMQADAFAEAEQAEAFAGADQAGACAIAAIgFAlQgNAAgKgFg");
	this.shape_2.setTransform(223.4,34.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AglBkQgSgHgOgPQgNgOgIgUQgHgUAAgYQAAgXAHgUQAIgUANgOQAOgOASgIQARgIAUABQAUgBASAJQASAIANAPQANANAIAVQAIATAAAWQAAAXgIAUQgIATgNAPQgNAPgSAIQgSAHgUAAQgUABgRgIgAgahDQgLAHgIAKQgGALgEAOQgDANAAAMQAAANADANQAEANAHALQAHALALAHQAMAGAOABQAPgBALgGQALgHAIgLQAHgLAEgNQADgNABgNQgBgMgDgNQgDgOgIgLQgGgKgMgHQgMgHgPAAQgPAAgLAHg");
	this.shape_3.setTransform(205.8,41.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAxCLQAFgRADgRIADgeIABgdQgBgUgHgNQgFgNgIgJQgKgIgJgEQgLgEgKAAQgJABgLAFQgJAEgKAJQgLAJgIAQIgCB6IgjABIgEkaIArgBIgCBvQAKgLALgGQAMgHAKgDQALgDAKgCQAVAAAQAIQARAHAMAOQANANAHAUQAHATABAYIgBAbIgBAbIgCAYIgFAVg");
	this.shape_4.setTransform(182.5,37.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgVgUIhAABIABgiIBAgBIABhRIAjgCIgBBSIBHgDIgDAiIhEACIgCCfIgmABg");
	this.shape_5.setTransform(151.4,37.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AA4BoIAEgaQgXAPgVAGQgVAHgTAAQgNAAgMgEQgNgDgJgHQgKgHgFgLQgGgLAAgPQAAgPAEgLQAFgMAIgJQAHgIALgHQAKgGAMgEQAMgEANgCQAMgCAOAAIAWABIASACIgFgSQgEgIgFgHQgGgIgIgEQgHgEgMAAQgGAAgJACQgJACgLAFQgLAGgNAKQgNAJgPAPIgWgbQARgQARgLQAQgKAOgGQAOgGAMgCQAMgCAJAAQARAAAOAFQANAGAJAKQALAKAGAOQAHANAEAPQAFAQACARQACAPAAARQAAARgCAUQgCAVgFAXgAgIAAQgPACgMAIQgLAHgGALQgFAKADAOQACALAIAEQAHAFALAAQAKAAANgEQAMgDALgGIAYgLIASgKIAAgSIgBgTIgSgEIgUgBQgQAAgPAEg");
	this.shape_6.setTransform(130.2,41);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAxCLQAFgRACgRIAEgeIABgdQgCgUgFgNQgGgNgJgJQgIgIgLgEQgKgEgJAAQgKABgKAFQgKAEgLAJQgJAJgKAQIgBB6IgkABIgDkaIAqgBIgBBvQALgLALgGQALgHAKgDQALgDALgCQAUAAARAIQAQAHANAOQAMANAHAUQAHATABAYIAAAbIgBAbIgEAYIgEAVg");
	this.shape_7.setTransform(107,37.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgWgUIg/ABIABgiIA/gBIAChRIAjgCIgBBSIBHgDIgDAiIhFACIgBCfIglABg");
	this.shape_8.setTransform(85.6,37.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWgUIg/ABIABgiIA/gBIAChRIAjgCIgBBSIBHgDIgDAiIhFACIgBCfIglABg");
	this.shape_9.setTransform(58.1,37.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgoBiQgTgHgNgNQgMgOgIgTQgGgTAAgXQAAgWAIgTQAHgUAOgPQANgPATgJQASgHAVgBQASAAASAIQAQAHANAOQANANAIARQAJASACAUIidAXQABANAFAKQAFALAIAHQAIAHALAEQAKADAMAAQAJAAAKgCQAJgEAIgFQAJgGAFgJQAGgIADgLIAjAHQgFARgJANQgKANgMAKQgMAKgPAFQgQAFgPABQgYgBgSgHgAgOhHQgJADgJAGQgJAHgHAKQgHAMgCAPIBtgNIgBgEQgHgSgMgKQgMgKgSAAQgHAAgJACg");
	this.shape_10.setTransform(38.2,41.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgQAwIgeAeIgCA3IgoACIAKkZIAoABIgGCsIBehfIAcAXIhEBCIBPBnIggAXg");
	this.shape_11.setTransform(17.4,37.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgaBnIgPgDIgRgGIgRgIQgIgEgIgGIASgcIAVAKQAKAFALAEQALAEALACQALACAMAAQAKAAAHgBQAHgDAFgDIAHgFIADgHQACgDgBgEQAAgEgCgEIgFgHQgEgDgFgDQgHgDgIgCQgJgCgLAAQgQgBgPgCQgPgDgMgGQgMgFgIgJQgHgJgCgOQgCgNAEgLQACgLAHgIQAGgJAJgGQAJgHAKgEQALgFAMgCQAMgCALAAIAQABIAVACIAWAHQAKAEAJAHIgMAiQgMgHgLgDIgTgGQgKgDgJAAQgcgCgPAIQgQAHgBAQQAAALAHAGQAFAEALADIAYADQANAAAQACQASADAMAGQAMAEAIAIQAHAHADAJQAEAJAAAJQAAASgHAMQgHAMgMAHQgMAHgPAEQgQAEgQAAQgPAAgSgEg");
	this.shape_12.setTransform(-4.5,42);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA5BoIACgaQgWAPgWAGQgUAHgTAAQgNAAgMgEQgMgDgKgHQgKgHgFgLQgGgLAAgPQAAgPAFgLQAEgMAHgJQAIgIAKgHQALgGAMgEQAMgEANgCQAMgCANAAIAXABIATACIgHgSQgDgIgGgHQgFgIgHgEQgJgEgKAAQgIAAgIACQgJACgLAFQgLAGgNAKQgNAJgPAPIgWgbQARgQAQgLQARgKAOgGQAOgGAMgCQALgCALAAQAQAAANAFQANAGALAKQAKAKAHAOQAGANAFAPQAEAQACARQACAPAAARQAAARgCAUQgCAVgEAXgAgIAAQgPACgMAIQgLAHgFALQgGAKADAOQADALAHAEQAIAFAKAAQALAAAMgEQAMgDAMgGIAWgLIATgKIABgSIgCgTIgTgEIgTgBQgRAAgOAEg");
	this.shape_13.setTransform(-26.9,41);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgQCMQgKgCgNgHQgMgFgMgKIAAAXIgiABIgDkZIAogBIgBBsQALgLALgFQALgGAKgDQAMgDAJAAQANAAAMAEQAMADALAHQALAGAKAJQAJAKAHALQAGALAEAMQADANAAAOQgBAQgEAOQgEAOgHALQgHAMgJAKQgJAJgLAGQgKAHgLADQgLADgLABQgKgBgMgDgAgVgXQgKADgIAHQgIAGgGAIQgGAHgDAKIAAAsQADALAGAJQAGAJAIAGQAIAFAJAEQAKADAJAAQANAAALgGQAMgFAJgIQAIgKAFgMQAGgMAAgOQABgOgEgMQgEgOgJgIQgIgKgMgGQgMgFgNAAQgLAAgKAEg");
	this.shape_14.setTransform(-49.6,37.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgpBiQgSgHgNgNQgMgOgIgTQgGgTAAgXQAAgWAIgTQAIgUANgPQAOgPASgJQASgHAVgBQASAAASAIQAQAHANAOQANANAJARQAIASACAUIieAXQACANAFAKQAFALAIAHQAIAHAKAEQALADAMAAQAJAAAJgCQAKgEAIgFQAIgGAHgJQAFgIADgLIAjAHQgFARgJANQgJANgNAKQgNAKgOAFQgPAFgQABQgXgBgUgHgAgPhHQgJADgIAGQgJAHgHAKQgHAMgDAPIBugNIgBgEQgHgSgMgKQgNgKgRAAQgHAAgKACg");
	this.shape_15.setTransform(-83,41.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAxCLQAFgRACgRIAEgeIABgdQgCgUgFgNQgGgNgJgJQgIgIgLgEQgKgEgJAAQgKABgKAFQgKAEgLAJQgJAJgKAQIgBB6IgkABIgDkaIAqgBIgBBvQALgLALgGQALgHAKgDQALgDALgCQAUAAARAIQAQAHANAOQAMANAHAUQAHATABAYIAAAbIgBAbIgEAYIgEAVg");
	this.shape_16.setTransform(-106.3,37.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgWgUIg/ABIABgiIA/gBIAChRIAjgCIgBBSIBHgDIgDAiIhFACIgBCfIglABg");
	this.shape_17.setTransform(-127.7,37.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgWgUIg/ABIABgiIBAgBIABhRIAjgCIgBBSIBHgDIgDAiIhEACIgCCfIgmABg");
	this.shape_18.setTransform(-155.3,37.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AghBjQgTgIgPgPQgQgOgJgTQgJgUAAgXQAAgMAEgOQAEgNAHgMQAHgMAJgKQAKgKAMgHQAMgHAOgEQAOgFAOAAQAQAAAOAEQAPAEAMAHQAMAIAKAKQAKALAGANIABAAIgfATIAAgBQgFgKgHgHQgHgHgIgFQgIgGgKgDQgKgCgKAAQgOAAgNAFQgNAGgKAKQgKAKgGANQgFANAAAOQAAAPAFANQAGAOAKAJQAKAKANAGQANAGAOAAQAKAAAJgDQAJgCAIgFQAIgFAHgGQAHgHAFgIIAAgBIAgASIAAABQgHAMgLAJQgKAKgMAHQgNAGgNAEQgPAEgOAAQgUAAgTgIg");
	this.shape_19.setTransform(-175.5,41.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgpBiQgSgHgNgNQgMgOgIgTQgGgTAAgXQAAgWAIgTQAIgUANgPQAOgPASgJQASgHAVgBQATAAARAIQAQAHANAOQANANAIARQAJASACAUIieAXQACANAFAKQAFALAIAHQAIAHAKAEQALADAMAAQAJAAAKgCQAJgEAIgFQAJgGAFgJQAGgIADgLIAjAHQgFARgJANQgJANgNAKQgMAKgPAFQgPAFgQABQgYgBgTgHgAgOhHQgKADgIAGQgJAHgHAKQgGAMgEAPIBugNIgBgEQgHgSgMgKQgNgKgRAAQgHAAgJACg");
	this.shape_20.setTransform(-197.9,41.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AALCfQgKgEgGgGQgIgHgFgJQgGgIgDgJQgJgVgCgaIAFjoIAlAAIgCA8IgBAyIgBAnIAAAeIgBAyQAAARAEANIAFAMQADAFAEAEQAEAFAGADQAGACAIAAIgFAlQgNAAgKgFg");
	this.shape_21.setTransform(-213.2,34.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgoBiQgTgHgNgNQgMgOgIgTQgGgTAAgXQAAgWAIgTQAHgUAOgPQANgPATgJQASgHAUgBQAUAAAQAIQARAHANAOQANANAIARQAJASACAUIidAXQABANAFAKQAFALAIAHQAIAHALAEQAKADALAAQAKAAAKgCQAJgEAIgFQAJgGAFgJQAGgIADgLIAjAHQgFARgJANQgKANgMAKQgMAKgPAFQgPAFgRABQgWgBgTgHgAgOhHQgJADgJAGQgJAHgGAKQgIAMgCAPIBtgNIgBgEQgHgSgMgKQgMgKgTAAQgGAAgJACg");
	this.shape_22.setTransform(-230.6,41.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgXCTQgOgDgNgFQgOgGgMgHIgagRIAbgfQAOAMAOAIQAOAHALADQANAFALABQANABAMgDQALgCAJgGQAIgFAFgHQAFgHABgIQABgHgDgGQgDgFgGgFQgGgFgHgDIgRgHIgRgFIgRgDIgWgFIgXgIQgLgFgKgHQgKgHgHgKQgIgKgDgOQgEgOABgSQABgOAFgMQAFgMAIgJQAIgJALgHQAKgHAMgEQALgFANgBQANgCALAAQARABASAEIAPAFIAQAHQAJADAIAGIAPAMIgUAhQgGgHgHgFIgNgJIgNgGIgNgEQgOgEgNAAQgQAAgNAFQgNAFgJAIQgJAIgEAJQgFAKAAAKQAAAJAFAJQAGAJAKAHQAKAHANAFQANAFAPACQANABAOAEQANACAMAFQAMAFAKAIQAKAHAHAJQAHAKADAMQAEALgCAPQgCAMgGAKQgFAKgIAIQgIAHgKAFQgKAFgLADQgLADgLACIgWABQgNAAgNgDg");
	this.shape_23.setTransform(-253.9,37.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAr+gEkMhX7AAAQh5AAhXBVQhVBXAAB4QAAB5BVBWQBXBWB5AAMBX7AAAQB5AABWhWQBWhWAAh5QAAh4hWhXQhWhVh5AAg");
	this.shape_24.setTransform(1,35.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF3333").s().p("Egr9AElQh5AAhXhWQhVhWAAh5QAAh4BVhXQBXhVB5AAMBX7AAAQB5AABWBVQBWBXAAB4QAAB5hWBWQhWBWh5AAg");
	this.shape_25.setTransform(1,35.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mccopy2, new cjs.Rectangle(-311.2,4.5,624.4,61.7), null);


(lib.InstructionText_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgpCDQgSgIgOgOQgNgOgIgTQgIgUAAgZQAAgZAJgSQAIgUAOgNQAOgNASgHQASgIASABQALAAAMADQALADANAFQAMAGALAMIABheIAjADIgBENIglAAIAAgYIgLAJIgMAHIgLAFIgLADQgLADgKAAQgWAAgSgHgAgbggQgMAGgIAKQgIAJgFANQgEANAAAQQAAAPAEANQAFANAIAIQAJAKALAGQAMAEAOAAQAKAAALgDQALgEAJgGQAJgHAGgKQAHgJAEgLIAAgnQgDgMgHgJQgHgKgKgHQgJgGgLgEQgLgEgLAAQgNAAgLAFg");
	this.shape.setTransform(253.8,35.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgoBiQgTgHgNgOQgMgNgIgTQgGgTAAgXQAAgWAIgUQAHgTAOgPQANgPATgIQASgJAVABQASAAASAHQAQAHANANQANAOAIARQAJASACAVIidAWQABANAFAKQAFALAIAHQAIAHALAEQAKAEAMAAQAJgBAKgDQAJgDAIgFQAJgGAFgIQAGgJADgMIAjAHQgFARgJAOQgKANgMAKQgMAKgPAFQgQAGgPgBQgYABgSgIgAgOhGQgJACgJAHQgJAGgHALQgHAKgCARIBtgOIgBgDQgHgTgMgKQgMgKgSAAQgHAAgJADg");
	this.shape_1.setTransform(231.2,39.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhkiUIAogBIAAAaQALgKALgFQALgGAKgDQALgDAKgBQAMAAAMAEQANADALAHQAKAGAKAJQAJAJAHAMQAGALAEANQADANAAAOQgBAQgEAOQgEAOgHAKQgHAMgJAJQgJAKgKAGQgLAGgLAEQgLADgLAAQgKAAgMgDQgKgDgMgFQgMgFgMgLIgBB9IgiAAgAAAhyQgLAAgKAEQgKAEgHAHQgJAGgFAJQgGAIgCAKIgBAiQADAMAGAKQAGAJAIAFQAIAGAJADQAJADAKABQAMAAALgFQALgFAJgIQAIgIAFgMQAFgMABgNQAAgOgEgMQgEgNgIgJQgIgJgLgGQgLgFgMAAIgCAAg");
	this.shape_2.setTransform(208.4,44.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhkiUIAogBIAAAaQALgKALgFQALgGAKgDQALgDAKgBQAMAAAMAEQANADALAHQAKAGAKAJQAJAJAHAMQAGALAEANQADANAAAOQgBAQgEAOQgEAOgHAKQgHAMgJAJQgJAKgKAGQgLAGgLAEQgLADgLAAQgKAAgMgDQgKgDgMgFQgMgFgMgLIgBB9IgiAAgAAAhyQgLAAgKAEQgKAEgHAHQgJAGgFAJQgGAIgCAKIgBAiQADAMAGAKQAGAJAIAFQAIAGAJADQAJADAKABQAMAAALgFQALgFAJgIQAIgIAFgMQAFgMABgNQAAgOgEgMQgEgNgIgJQgIgJgLgGQgLgFgMAAIgCAAg");
	this.shape_3.setTransform(184.2,44.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmBkQgRgIgNgOQgOgOgIgUQgHgUAAgYQAAgXAHgUQAIgUAOgOQANgOARgIQASgHAUgBQAUAAASAJQASAIANAPQANAOAIATQAIAUAAAWQAAAXgIAUQgIAUgNAOQgNAPgSAHQgSAJgUgBQgUAAgSgHgAgahEQgLAIgIAKQgGALgEANQgDAOAAAMQAAANADANQAEAOAHALQAIAKALAHQAKAHAPgBQAPABALgHQALgHAIgKQAIgLADgOQADgNAAgNQAAgMgDgOQgEgNgGgLQgIgKgLgIQgLgGgQAAQgPAAgLAGg");
	this.shape_4.setTransform(160.2,39.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhSAtIgCgtIgBgjIgBgaIgBgnIAqgBIAAAXIABAVIAAAWIANgSQAHgKAKgJQAKgJAKgHQALgHANgBQAPgBAMAHQAFADAFAFQAFAFAFAHQAEAIADAKQADALABAOIgkAOQAAgJgCgHIgDgLIgFgIIgFgFQgHgFgIAAQgFABgGAEIgLAJIgMANIgLAOIgLAPIgJAMIAAAbIABAZIABAVIAAAOIgnAFIgCg5g");
	this.shape_5.setTransform(139.8,39.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgpCDQgSgIgOgOQgNgOgIgTQgIgUAAgZQAAgZAJgSQAIgUAOgNQAOgNASgHQASgIASABQALAAAMADQALADANAFQAMAGALAMIABheIAjADIgBENIglAAIAAgYIgLAJIgMAHIgLAFIgLADQgLADgKAAQgWAAgSgHgAgbggQgMAGgIAKQgIAJgFANQgEANAAAQQAAAPAEANQAFANAIAIQAJAKALAGQAMAEAOAAQAKAAALgDQALgEAJgGQAJgHAGgKQAHgJAEgLIAAgnQgDgMgHgJQgHgKgKgHQgJgGgLgEQgLgEgLAAQgNAAgLAFg");
	this.shape_6.setTransform(115.4,35.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaBoIgPgEIgRgFIgQgIQgJgFgIgGIASgcIAUALQALAFALADQALAEALACQALADANAAQAKgBAHgCQAGgCAFgCIAHgHIADgHQACgDgBgDQAAgEgBgDIgGgIQgDgDgHgDQgFgDgJgBQgIgCgMgBQgPgBgQgDQgPgCgMgGQgMgGgHgHQgJgKgBgOQgCgNAEgLQADgLAGgJQAGgIAJgHQAJgGALgFQAKgDAMgCQAMgDALAAIARABIAUADIAVAGQALAEAKAHIgNAhQgMgGgLgEIgTgFQgKgCgJgBQgbgBgQAHQgRAHABARQAAAKAFAFQAHAGAKABIAYADQANABAQADQASADAMAFQANAFAHAGQAIAIACAJQAEAIAAAKQAAASgHALQgHAMgMAIQgMAHgPAEQgPADgRABQgQAAgRgDg");
	this.shape_7.setTransform(83.4,40.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgWgVIg/ACIABgiIA/gCIAChQIAjgCIgBBRIBHgCIgDAjIhFABIgBCfIglABg");
	this.shape_8.setTransform(64.4,36);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgXCKIABgvIABg6IABhNIAmgCIgCAvIgBAoIAAAgIgBAZIAAAogAgJhXQgFgCgDgDQgEgDgCgFQgCgFAAgFQAAgGACgFQACgEAEgEQADgDAFgCQAEgDAFAAQAGAAAEADQAFACAEADQADAEACAEQACAFAAAGQAAAFgCAFQgCAFgDADQgEADgFACQgEACgGAAQgFAAgEgCg");
	this.shape_9.setTransform(51,35.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUBmQgQgBgMgFQgMgGgHgJQgJgKgFgMQgFgNgCgNIgEgcIAAgbIABgnIAFgoIAlABIgDApIgBAgIAAAQIACATQAAALADAKQADALAFAIQAEAIAHAEQAHAFAKgCQAOgCAPgRQAPgSARgjIAAguIgBgbIgBgPIAngEIACA5IACAsIABAkIABAaIABAnIgqABIgBg1QgFALgIAKQgIAJgJAIQgJAHgKAEQgIAEgKAAIgDAAg");
	this.shape_10.setTransform(34.2,39.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhSAtIgCgtIgBgjIgBgaIgBgnIAqgBIAAAXIABAVIAAAWIANgSQAHgKAKgJQAKgJAKgHQALgHANgBQAPgBAMAHQAFADAFAFQAFAFAFAHQAEAIADAKQADALABAOIgkAOQAAgJgCgHIgDgLIgFgIIgFgFQgHgFgIAAQgFABgGAEIgLAJIgMANIgLAOIgLAPIgJAMIAAAbIABAZIABAVIAAAOIgnAFIgCg5g");
	this.shape_11.setTransform(13.4,39.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgzBRIABgvIAAgjIAAgOIgpAAIAAgjIArAAQACgTAGgSQAGgRAKgOQALgOAPgJQAQgHAWgBQAMAAAOAFQANADANAMIgSAfQgKgHgJgCQgJgDgIgCQgIAAgIABQgHACgGAEQgGAEgDAHQgEAHgDALQgDALgBAPIBMABIgEAkIhKgBIAAAPIgBAyIAAAWIAAAaIAAAZIAAAZIgmABIABhFg");
	this.shape_12.setTransform(-4.5,35.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgpBiQgSgHgNgOQgNgNgGgTQgHgTAAgXQAAgWAIgUQAIgTANgPQAOgPASgIQASgJAUABQATAAARAHQARAHANANQANAOAJARQAIASACAVIieAWQACANAFAKQAFALAIAHQAIAHAKAEQALAEALAAQAKgBAJgDQAKgDAIgFQAIgGAHgIQAFgJADgMIAkAHQgGARgJAOQgJANgNAKQgNAKgOAFQgQAGgQgBQgWABgUgIgAgPhGQgJACgIAHQgJAGgHALQgGAKgEARIBugOIgBgDQgHgTgMgKQgNgKgSAAQgGAAgKADg");
	this.shape_13.setTransform(-35.2,39.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAxCLQAFgRACgRIAEgeIABgdQgCgUgFgNQgGgNgJgJQgIgIgLgEQgKgEgKAAQgJABgKAFQgKAEgLAJQgJAJgKAQIgBB6IgkABIgDkaIArgBIgCBvQALgLALgGQALgHAKgDQALgDALgCQAUAAARAIQAQAHAMAOQANANAHAUQAHATABAYIgBAbIAAAbIgDAYIgFAVg");
	this.shape_14.setTransform(-58.5,35.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgWgVIg/ACIABgiIA/gCIAChQIAjgCIgBBRIBHgCIgDAjIhFABIgBCfIglABg");
	this.shape_15.setTransform(-79.9,36);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgpBiQgSgHgNgOQgMgNgIgTQgGgTAAgXQAAgWAIgUQAIgTANgPQAOgPASgIQASgJAVABQASAAASAHQAQAHANANQANAOAJARQAIASACAVIieAWQACANAFAKQAFALAIAHQAIAHAKAEQALAEAMAAQAJgBAJgDQAKgDAIgFQAIgGAHgIQAFgJADgMIAjAHQgFARgJAOQgJANgNAKQgNAKgOAFQgPAGgQgBQgXABgUgIgAgPhGQgJACgIAHQgJAGgHALQgHAKgDARIBugOIgBgDQgHgTgMgKQgNgKgRAAQgHAAgKADg");
	this.shape_16.setTransform(-109.6,39.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgcBjIhBi5IAmgFIA2CcIA5ijIAmAFIhIDAg");
	this.shape_17.setTransform(-131.1,39.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AhSAtIgCgtIgBgjIgBgaIgBgnIAqgBIAAAXIABAVIAAAWIANgSQAHgKAKgJQAKgJAKgHQALgHANgBQAPgBAMAHQAFADAFAFQAFAFAFAHQAEAIADAKQADALABAOIgkAOQAAgJgCgHIgDgLIgFgIIgFgFQgHgFgIAAQgFABgGAEIgLAJIgMANIgLAOIgLAPIgJAMIAAAbIABAZIABAVIAAAOIgnAFIgCg5g");
	this.shape_18.setTransform(-150.8,39.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgoBiQgTgHgNgOQgMgNgIgTQgGgTAAgXQAAgWAIgUQAHgTAOgPQANgPATgIQASgJAVABQASAAASAHQAQAHANANQANAOAIARQAJASACAVIidAWQABANAFAKQAFALAIAHQAIAHAKAEQALAEAMAAQAJgBAKgDQAJgDAIgFQAJgGAFgIQAGgJADgMIAjAHQgFARgJAOQgKANgMAKQgMAKgPAFQgQAGgPgBQgYABgSgIgAgOhGQgJACgJAHQgJAGgHALQgHAKgCARIBtgOIgBgDQgHgTgMgKQgMgKgSAAQgHAAgJADg");
	this.shape_19.setTransform(-172.9,39.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgaBoIgPgEIgRgFIgRgIQgIgFgIgGIASgcIAVALQAKAFALADQALAEAMACQAKADAMAAQALgBAGgCQAIgCAEgCIAHgHIAEgHQABgDgBgDQAAgEgCgDIgFgIQgEgDgFgDQgHgDgIgBQgJgCgLgBQgPgBgQgDQgPgCgMgGQgMgGgIgHQgHgKgCgOQgBgNADgLQACgLAHgJQAGgIAJgHQAJgGAKgFQALgDAMgCQAMgDALAAIAQABIAVADIAWAGQAKAEAJAHIgMAhQgMgGgLgEIgUgFQgJgCgJgBQgcgBgQAHQgPAHgBARQAAAKAHAFQAFAGALABIAYADQANABAQADQASADAMAFQAMAFAIAGQAHAIADAJQAEAIAAAKQAAASgHALQgHAMgMAIQgLAHgQAEQgQADgQABQgQAAgRgDg");
	this.shape_20.setTransform(-194.7,40.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgQCMQgKgDgNgGQgMgFgMgLIAAAZIgiAAIgDkZIAogCIgBBsQALgJALgGQALgGAKgCQAMgDAJgBQANAAAMAEQAMADALAHQALAGAKAKQAJAJAHALQAGALAEAMQADANAAAPQgBAPgEAOQgEAOgHAMQgHALgJAJQgJAKgLAHQgKAFgLAEQgLAEgLgBQgKAAgMgDgAgVgXQgKADgIAHQgIAGgGAHQgGAIgDAKIAAArQADAMAGAIQAGAKAIAFQAIAHAJACQAKADAJAAQANABALgFQAMgFAJgKQAIgIAFgNQAGgMAAgOQABgOgEgNQgEgMgJgJQgIgKgMgFQgMgGgNAAQgLAAgKAEg");
	this.shape_21.setTransform(-216.5,35.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgkCJQgSgGgPgJQgPgJgNgNQgMgNgKgQQgJgQgFgRQgEgTAAgTQAAgTAEgSQAFgSAJgQQAKgQAMgMQANgNAPgKQAPgJASgFQASgFASAAQATAAARAFQASAFAQAJQAQAKAMANQANAMAJAQQAIAQAFASQAFASABATQgBATgFATQgFARgIAQQgJAQgNANQgMANgQAJQgQAJgSAGQgRAFgTAAQgTAAgRgFgAgnhgQgSAIgOAOQgOAPgIATQgIATAAAVQAAAWAIATQAIATAOAOQAOAPASAIQATAIAUAAQAOAAANgDQANgEALgHQAMgHAJgJQAKgKAGgMQAHgMADgNQAFgOAAgOQAAgOgFgNQgDgOgHgLQgGgMgKgKQgJgJgMgHQgLgHgNgEQgNgDgOAAQgUAAgTAIg");
	this.shape_22.setTransform(-244.5,35.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAr+gEkMhX7AAAQh5AAhXBVQhVBXAAB4QAAB5BVBWQBXBWB5AAMBX7AAAQB5AABWhWQBWhWAAh5QAAh4hWhXQhWhVh5AAg");
	this.shape_23.setTransform(1,35.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#0066FF").s().p("Egr9AElQh5AAhXhWQhVhWAAh5QAAh4BVhXQBXhVB5AAMBX7AAAQB5AABWBVQBWBXAAB4QAAB5hWBWQhWBWh5AAg");
	this.shape_24.setTransform(1,35.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mccopy, new cjs.Rectangle(-311.2,4.7,624.4,61.6), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.inside_fruitcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#85B022").s().p("AimA1QgEgZAhgbQBHg5BggIQBKgFBAAaQgrgIgrABQg6ADg2ATQg6AUgpAkQgSAQgPAWQgDgGgBgHg");
	this.shape.setTransform(-13.1,-28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#769F1A").s().p("AiRA/QgOgFgGgKQAPgWASgQQApgkA6gUQA1gTA6gDQAsgBAqAIIACABQg2AEgxARQg7AUgqAkIgfAcQgTAPgRAGQgIADgJAAQgLAAgMgGg");
	this.shape_1.setTransform(-12.8,-26.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F7BE51").s().p("AguC3QAJgogHgnQgFgggZg6QgUgsgFgYQgEgdAHgdIAAAAQAHgeATgaIgBABQARgVAWgPIAAABQAWgOAYgFIAAAAIADAAQAQANAiBSQArBngLBJQgHAjgTAiQgTAggbAZQgbAZggANIgBAAQgMAGgNACQAIgTAEgUg");
	this.shape_2.setTransform(-2.2,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F7AF28").s().p("AhjEPQgQAAgGgHQgFgHADgKQACgHAHgLQARgaAGggQAHgfgFgfQgFgcgWgzQgXgygFgcQgHglAKgpQAKgmAYghQAWgcAdgTQAegTAggGIAQgBQAmAEAzB4QAxB3gQBTQgIArgXApQgWAmghAeQgiAfgmAQQgoAQgpAAIgEAAgAhig4QAFAYAUAsQAZA6AEAgQAHAngIAoQgEAUgIATQAMgCANgGIAAAAQAggNAbgZQAcgZASggQATgiAHgjQAMhJgrhnQgjhSgQgNIgCAAIgBAAQgXAFgWAOIAAgBQgXAPgQAVIAAgBQgSAagIAeIAAAAQgHAdAFAdg");
	this.shape_3.setTransform(-2.2,-0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F89C1A").s().p("AiCExQhCgCgVgLQgWgLgGgQIgCgJQA1gCAfgCQA3gFArgKQA0gMAqgWQAugYAggkQAfghATguQARgqAIgwQAQhwgshnQgPgjgUgdQAqAcAfArQARAXAMAbIAAAEQAUAxAGA7IABACQAFBUgjBNQgiBLhAA2QggAbgkATQgkATgmAKQgqALgqAAIgYgBg");
	this.shape_4.setTransform(5.1,4.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FBA910").s().p("AjCEWIARgkQAZg2gfhUIgbhDQgPgmgFgcQgIgqAKgrQAJgpAYglQAYgkAigbIABgBQAWgQAXgLIAdgMQAsgNAuAEQAAAGgCAFQAFAAAEADIADAAIAAgCIAAgLQAhAEAhAMQAjANAcATQAUAdAPAjQAsBngQBvQgIAxgRAqQgTAugfAhQggAkguAYQgrAWgzAMQgrAKg3AFQgfACg1ACQgBgMAGgQgAgSkPQghAGgfATQgcASgWAdQgYAggKAnQgLAoAIAmQAEAbAXAzQAXAzAEAbQAFAggGAeQgHAggRAbQgHALgBAHQgDAKAEAGQAGAIAQAAQArABAqgRQAogRAhgeQAhgeAWgnQAXgoAIgrQAQhTgyh4Qgyh3gmgFIgPACg");
	this.shape_5.setTransform(0.3,-0.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C88014").s().p("AADAUQgDgCgFAAQACgFAAgHIAAgFIgCgTIALgBIAAAaIAAAMIAAABIgBAAIgCAAg");
	this.shape_6.setTransform(3.5,-31.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.027)").s().p("AlHFIQiIiIAAjAQAAi/CIiIQCIiIC/AAQDAAACJCIQCHCIAAC/QAADAiHCIQiJCIjAABQi/gBiIiIg");
	this.shape_7.setTransform(-0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy2, new cjs.Rectangle(-46.5,-46.6,92.9,92.9), null);


(lib.inside_fruitcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#3B8F05","#3A8C05","#286405","#215405","#276105","#358205","#3B8F05"],[0,0.024,0.357,0.529,0.667,0.918,1],-14.4,-1.8,51.2,-1.8).s().p("AgwA9QhWhXgIh4IAAgIQABASADATIgBgVIABgRQACANAEAMIAFAWIgHAKQALAtAVAnIAKgUQAWAgAeAcQA/A9BSAUIAAAfIADALIAkAHQhvgNhRhSg");
	this.shape.setTransform(-18.3,17.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6FAA15").s().p("ABECIIgBgHIgDghQgCgSgFgHIgIgKQgGgIgBgEQgBgIAHgOQAHgPAAgGIgCgRQgCgMACgIIAAgCIAJgWQBYAFBSgTQgBADgEAEQgGAGAAAEQAAAFAGAHIAGAHIACAEQAEANgKAMQgPALgFAHQgHAKAFALQAHAQABAHIgMANIgWAZIgKAFQgTAJgFAJQgDAFAAAHQgcAFgdAAIgTAAgAiJAuQgegdgWgfQAHgNAEgEQAEgGAQgKQANgJADgJQADgGgFgZIAKADQAtAQAxAJQACAJAHANQAEAHACAGQAEALgBAIQgBAKgSAWQgRAUACAMQACAOAUAMIAdASQAKALACAUIABACQhRgUhAg9gAjmhuQgDgMgCgNIAAAAIAbANQAAAIgGALIgHALg");
	this.shape_1.setTransform(-8.6,15.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#2FBA05","#21C205","#21C205","#38CF05","#61E505","#70ED05"],[0.012,0.51,0.51,0.635,0.875,1],-55.1,9.5,10.4,9.5).s().p("AgQgHIAhgNIgZApQgJgOABgOg");
	this.shape_2.setTransform(22.4,7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#388008").s().p("AgxBSIACgCIAUgUIAHgGQAGgGADgHQAbgoAOg2QAFgSAEgVIAVgMIgDAHQgKATABAHQACAFAGAHIADAEQgZBKg7A5QgQAQgSANQADgOAHgJg");
	this.shape_3.setTransform(25.2,12.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#93FF05","#86F205","#65D005","#63CE05","#69D605","#88FD05"],[0,0.173,0.486,0.498,0.58,1],-63.1,-0.5,2.5,-0.5).s().p("AgGgcIADADQANAKADAMQABAHgGAOIgUALQAEgcACgdg");
	this.shape_4.setTransform(30.4,0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#5ECF31","#61D035","#6AD340","#79D753","#8EDD6E","#A9E691","#CAEFBB","#DAF4CF"],[0.004,0.173,0.329,0.486,0.639,0.788,0.937,1],-0.1,-24.8,-0.8,50.2).s().p("AhOB4IAMgNIAAACQABAQgjAUgAAvAjQgDgIgGgMIgRgWIgBgCIAagpIAJgRQAUgoALglQAFAGAKAGQgCAfgEAcQgDAUgGATQgOA2gcAnQAFgLgCgNg");
	this.shape_5.setTransform(19.6,10.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#032B3F").s().p("AAAAJQgEgBgCgCIgCgBIgBgBIgBgEQAAgCAEgDIAAAAIAEgCIACgBIAAAAIABAAQAEAAAEADIABACIABADIAAAEIgCACIgEABIgCABIgCABIgBAAg");
	this.shape_6.setTransform(3.9,-21.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A2DB4D").s().p("AgDDIIAAgBQAGgPAAgJQgBgEgEgKQgDgHACgGQABgEAHgFQAGgFACgDQACgJgKgXQgKgXADgMQACgGAIgOQAKgNACgHQABgEgCgNQgBgLADgEQACgEAGgEQAIgEABgCQALgMAAgIQgDgMACgNIAHgVQAGgLgCgHIABAAIACgBIAKAZQAMAdAHALQAGAJACAVQADAUAFAJIAIANQAEAHACAGQABADAAAMQABAJAEAHIAOAMQAJAHABAHQABAFgFAJQgFAKgBADQgBALANASQAOARgBALIAAACQhCAOhGAAIghAAgAjICiIgKgEIgBgCQgEgXALgJIAEgCQAGgDASgGQAPgFAAgMIgIghQgDgTATgGQAFgBAXAAQAOAAAFgLIAHghQAGgXANgGQAFgCAKADQALADAFgBQAHgCABgCIAEgKQAEgEAEgMQAFgKAHgCIANgEIATgGQAQgGASgUIABABQgOAfgFAGQgGAFgDAJQgEALgDADQgGAJgTABQgWACgGAGQgJAFABAHIACAPQAAAUgPAMQgVARgFAJQgHANAKAYQAMAagCAIQgBAJgRANQgPANgCALIAAAMQgwgKgtgPgADmCbIANgkQAGgUgLgRQgIgNgbgSQgagQgFgPQgCgGABgOQABgOgDgHQgEgKgDgBQgFgBgGgDQgOgEgGgFQgMgIgHgUQgGgTgYgLIgTgFIAFgBIABgCQAWAAAYAIQAGADAMgBQAKgBAGACQAKAEAGAPQAFASAEAEQAFAGAFAAIANgBQATADAHANIAOAbQAHAHAWAEIAEAAQASAEAFAGQAGAIgBAQQgCARADAHIADAEQgMAlgUAnIgJASIghAMIACgNgAktB3QADhcAvhJQAMgFAJgIQAHgFAGgLQAEgJAGgDQACgCAFAAIAFgBQAJAAACgBQAGgEAQgUQANgTAOgEQAJgCAQABQASACAIgBIARgEQAKgDAHABQAEAAAHAFIAKAGQAPAHAGgDQAKgGAMABQAHABAJAGIANAMQgLgBgWAIIgcAIQgHACgWgDQgRgCgLAEQgQAKgKAEIgPACQgIABgGAGQgEADgHANQgFAKgGADQgDACgJgBQgLgBgFACQgLAGgIAUIgBAGQgIAQgFAFQgCACgJABQgGABgEAEQgEAFACAJIAAAOQgBANgPAIQgUALgEAFQgKALAEANIAIAZIAAACIgbgNgAEOhCIgLgGQgFgCgQgOQgLgLgLgEIgRgBQgMAAgFgCQgEgCgGgFQgFgGgDgCQgPgHgHAEQgLAHgIAAIgfAAQgOAAgIACIgBgEQAXgOApgPQANgEAPADIAcAEIAIAAQAcASAYAaQALAMAKANIAKANIgKgDgAAZiRQgHgFgNAAQgOgCgEgCQgNgEgKgPQgJgMgHgGQAggHAjgBIAAAAIALACQAJABABADQACACgBAFIAAAHQABAIAFACQAKACAFAHQAGAJgJAcIgEACIAAAAQgMgQgOgIgAA6h8IACgCIgBACgABAiHQAJgQAAgHQAAgHgEgGQgIgKAAgCQgCgFADgJQArAGAnARQgHAFgGACQgFADgOADQgLACgIAEIgbAUIgEAFIACgFg");
	this.shape_7.setTransform(-2,-9.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#82E93E").s().p("Ak8ChIAAgkIABAFIAWh5QAYg7AwgyIALgKQBdhYCAAAQCIAABgBiIARATQAkAqATAwIACAIIgOgfQgIgDgUgFIgEAAIgLgNQgJgNgMgMQgYgagbgSQAMAAAHADIAOAHQAGAFAIACIAUABQgngqg0gdQgEAEgEACQgHADgKAKIgFAFQgngRgrgGIADgIQAFgPgDgJIgEgFQgVgDgVAAQg0AAgzARIABACQAOAKAagDQAOgBASgDQAGABAIAHQAGAGAEACQgjABggAHIgEgDQgKgEgHACQgJAFgIACQgQAEgPgBIgZgEQgdASgZAYIgWAXQgjAngVAvQAQgCAMgFQgvBJgEBcIAAABIgBAQIABAWQgDgTgCgTg");
	this.shape_8.setTransform(-1,-14);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#2D6508").s().p("AABFQIgBAAIAAAAIgDAAIgIAAIgGgBIAAABIgLgBIgQgCIgkgGIgCgLIAAgfIgBgDQgCgUgMgKIgcgSQgUgNgCgNQgCgNAQgTQATgXABgKQABgIgFgMQgBgFgEgHQgIgNgCgJIAAgMQACgLAPgNQARgNABgJQACgIgMgZQgKgYAHgNQAFgJAVgRQAPgMAAgUIgCgQQgBgHAJgFQAGgGAWgCQAUgBAGgJQADgDAEgLQACgJAGgFQAFgGAOgfIABABQADACAFABQACAHgGALIgHAVQgCANADAMQAAAIgLAMQgBACgIAEQgFAEgCAEQgDAEABAMQACANgBAEQgCAHgKANQgJAOgCAGQgDAMALAWQAKAXgCAJQgCADgGAFQgIAFgBAEQgCAGADAHQAFAKABAEQAAAJgHAPIAAABIgJAVIAAADQgCAIACALIABASQABAGgIAQQgGAOABAHQABAFAFAHIAJAKQAEAHACASIAEAiIAAAGIAUABQAcAAAcgFQgBgHADgGQAGgIATgKIAJgFQAkgTgBgRIAAgBQgBgHgHgRQgFgLAHgJQAFgIAOgMQALgLgEgOIgCgEIgGgHQgHgGAAgFQABgFAFgFQAEgEACgDIAAgCQABgLgOgRQgNgSABgLQABgDAFgKQAFgJgBgFQgBgGgJgHIgOgMQgEgHgBgJQAAgMgBgDQgCgGgEgHIgIgNQgFgJgDgVQgCgVgGgJQgHgLgMgdIgKgZIABgBIATAFQAYALAGATQAHAUAMAIQAGAFAOAEQAGADAFABQADABAEAKQADAHgBAOQgBAPACAGQAFAPAaAQQAbASAIANQALAQgGAUIgNAkIgCANQgCAPAJAOIACACIAQAWQAHANACAHQACAOgFALQgCAHgHAHIgGAFIgUAUIgCADQgHAIgDAOQARgNARgQQA8g5AZhLIgDgEQgHgGgBgFQgCgIALgTIACgGQAHgOgBgIQgEgLgNgKIgEgDQgJgHgFgFIgDgEQgDgHACgRQABgQgGgIQgFgGgSgEIgEgBQgWgEgHgHIgOgbQgHgNgTgDIgNABQgFAAgFgGQgEgEgFgSQgGgPgKgEQgGgCgKABQgMABgGgDQgYgIgWAAIABgEQAIgCAOAAIAfAAQAIAAALgHQAHgEAPAHQADACAFAGQAGAFAEACQAFACAMAAIARABQALAEALALQAQAOAFACIALAGIAKADIAFAAQATAFAJADIAOAgQAVA5AAA/IAAABIAAADIAAACQAAASgBATIgEAVQgGAhgMAfIgBAEQgXA7gwAxQheBeiCADgAlDBPIAIgLIACgDIAIgLQAGgMAAgIIAAgCIgIgZQgEgMAKgLQAEgFAUgLQAPgIABgNIAAgOQgCgJAEgFQAEgEAGgBQAJgBACgCQAFgFAIgRIABgGQAIgUALgGQAFgCALABQAJABADgCQAGgDAFgKQAHgNAEgDQAGgGAIgBIAPgCQAKgEAQgKQALgEARACQAWADAHgCIAdgIQAVgIALABIgNgMQgIgGgHgBQgMgBgLAGQgGADgPgHIgKgGQgHgFgEAAQgHgBgKADIgRAEQgIABgSgCQgQgBgJACQgOAEgNATQgQAUgGAEQgCABgJAAIgFABQgFAAgCACQgGADgEAJQgGALgHAFQgJAIgMAFQgMAFgQADQAWgwAignIAXgXQAZgYAdgSIAZAEQAOABAQgEQAIgCAJgFQAIgCAKAEIAEADQAHAGAJAMQAKAPANAEQAEACAPACQANAAAGAFQAOAIAMAQQgDADAAADIABAEQgSAUgPAGIgUAGIgNAEQgHACgFAKQgEAMgEAEIgEAKQgBACgHACQgFABgLgDQgKgDgFACQgNAGgGAXIgHAiQgFALgOAAQgXAAgFABQgTAGADATIAIAhQAAALgPAFQgSAGgGADIgEACQgLAJAEAXIABACQAEAZgDAHQgCAJgNAIQgQAKgEAGQgEAEgHANIgKAVQgVgngMgtgAArjbQgFgDgDAAIgBAAIABgCIgCACIgDABQAJgcgGgJQgFgHgKgCQgFgCgBgIIAAgHQABgFgCgCQgBgDgJgBIgKgCIAAAAQgEgCgHgGQgIgHgHgBQgRADgOABQgaADgOgKIgCgCQA0gRA1AAQAUAAAUADIAFAFQADAJgFAPIgDAIQgDAJACAFQAAACAIAKQAEAGAAAHQAAAHgJAQIgCAFIAEgFIAbgUQAIgEALgCQAOgDAFgDQAGgCAHgFIAFgFQAKgKAHgDQAEgCAEgEQA0AdAnAqIgUgBQgIgCgGgFIgOgHQgHgDgNAAIgIAAIgcgEQgPgDgNAEQgpAPgXAOIgBgCg");
	this.shape_9.setTransform(0.5,-0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(255,255,255,0.027)").s().p("AlHFIQiIiIAAjAQAAi/CIiIQCIiIC/AAQDAAACJCIQCHCIAAC/QAADAiHCIQiJCIjAABQi/gBiIiIg");
	this.shape_10.setTransform(-0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy, new cjs.Rectangle(-46.5,-46.6,92.9,92.9), null);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.98,scaleY:0.98},7).to({scaleX:1,scaleY:1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-213.4,-34.9,427,70);


(lib.Tween13copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,13.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88.5,-77.4,177,181.9);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,13.5,1.03,1.03);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:0.1,scaleX:0.94,scaleY:0.94},7).to({regY:0,scaleX:1,scaleY:1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.1,-80.1,182.3,187.4);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-2.1,-2.2);
	this.instance.alpha = 0;

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFCC00").ss(5,1,1).p("AREAAQAAHFlAFAQk/E/nFAAQnEAAlAk/Qk/lAAAnFQAAnEE/lAQFAk/HEAAQHFAAE/E/QFAFAAAHEg");
	this.shape.setTransform(0,13.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AsEMEQk/k/AAnFQAAnEE/k/QFAlAHEAAQHFAAE/FAQFAE/AAHEQAAHFlAE/Qk/FAnFAAQnEAAlAlAg");
	this.shape_1.setTransform(0,13.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.7,-98.2,223.5,223.4);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.question = new lib.inside_fruitcopy2();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(214.3,0.5,2.333,2.333);

	this.question_1 = new lib.inside_fruitcopy();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(-213.7,0.5,2.333,2.333);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.question_1},{t:this.question}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-322.3,-108.3,644.8,216.7);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.questxt = new lib.InstructionText_mccopy2();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(-1,-41.5,1.152,1.152,0,0,0,0.1,0);
	this.questxt.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-359.6,-36.3,719.3,71), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween15("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},7).to({scaleX:1,scaleY:1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-161.2,-31.6,322.5,63.2);


(lib.Nbbaskt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0.1,-49.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Nbbaskt, new cjs.Rectangle(-238.2,-126,476.5,252), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween18copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,-13.5);

	this.instance_1 = new lib.Tween12("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,-13.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.7,-111.7,223.5,223.4);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,-13.5);

	this.instance_1 = new lib.Tween12("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,-13.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.7,-111.7,223.5,223.4);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween18copy("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.7,-111.7,223.5,223.4);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.RedBasket = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.Nbbaskt();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,3.7,0.383,0.383,0,0,0,-0.1,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.4)").s().p("ApHA2QjxgWAAggQAAgeDxgXQDygWFVAAQFWAADxAWQDyAXAAAeQAAAgjyAWQjxAWlWAAQlVAAjygWg");
	this.shape.setTransform(-2.5,49.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 8
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.027)").s().p("AsuHpIAAvRIZdAAIAAPRg");
	this.shape_1.setTransform(-5.4,4.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.RedBasket, new cjs.Rectangle(-93.6,-44.8,182.4,101.9), null);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc2 = new lib.RedBasket();
	this.mc2.name = "mc2";
	this.mc2.parent = this;
	this.mc2.setTransform(4.4,34,1.728,1.728,0,0,0,0.1,0.1);

	this.question = new lib.inside_fruitcopy();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(1.5,-23.6,2.333,2.333);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.question},{t:this.mc2}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-157.6,-132.4,315.1,265);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc3 = new lib.RedBasket();
	this.mc3.name = "mc3";
	this.mc3.parent = this;
	this.mc3.setTransform(218.5,29.3,1.728,1.728,0,0,0,0.1,0.1);

	this.mc2 = new lib.RedBasket();
	this.mc2.name = "mc2";
	this.mc2.parent = this;
	this.mc2.setTransform(-209.6,29.3,1.728,1.728,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc2},{t:this.mc3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-371.5,-48.4,743.1,176.2);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(419.1,529.1,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(281).to({_off:false},0).wait(27).to({x:426.1,y:536.5,startPosition:8},0).to({alpha:0},7).wait(5));

	// Layer_12
	this.instance_1 = new lib.Tween17("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(426,553.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(275).to({_off:false},0).to({scaleX:1.25,scaleY:1.25},6).to({startPosition:0},27).to({alpha:0},7).wait(5));

	// Layer_11
	this.instance_2 = new lib.Symbol1copy("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(515.2,584.8,1,1,0,0,0,190.5,64.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(240).to({_off:false},0).to({_off:true},35).wait(45));

	// Layer_9
	this.instance_3 = new lib.Symbol1copy3("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(433.6,381,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(204).to({_off:false},0).to({_off:true},36).wait(80));

	// Layer_7
	this.question = new lib.inside_fruitcopy();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(426.3,529.9,2.333,2.333);
	this.question.alpha = 0;
	this.question._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question).wait(178).to({_off:false},0).to({alpha:0.941},8).wait(26).to({alpha:0},11).to({_off:true},1).wait(96));

	// Layer_1
	this.instance_4 = new lib.Symbol10("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(640,267.3);

	this.instance_5 = new lib.Tween14("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(778.4,118.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_5},{t:this.instance_4}]},138).to({state:[]},86).to({state:[{t:this.instance_5}]},47).to({state:[{t:this.instance_5}]},5).to({state:[]},1).wait(43));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(138).to({_off:false},0).to({_off:true},86).wait(47).to({_off:false},0).to({alpha:0},5).to({_off:true},1).wait(43));

	// Layer_4
	this.instance_6 = new lib.Tween18("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(640,267.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(116).to({_off:false},0).to({startPosition:0},5).to({_off:true},156).wait(43));

	// Layer_10
	this.instance_7 = new lib.Symbol7("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(782.7,117.9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(51).to({_off:false},0).wait(43).to({mode:"independent"},0).to({alpha:0},9).to({_off:true},1).wait(216));

	// Layer_3
	this.instance_8 = new lib.Tween11("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,558.1);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(24).to({_off:false},0).to({alpha:1},8).to({startPosition:0},239).to({alpha:0},5).to({_off:true},1).wait(43));

	// choices
	this.instance_9 = new lib.Tween6("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640,249.5);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(35).to({_off:false},0).to({alpha:1},8).to({startPosition:0},33).to({y:529.5},18).to({startPosition:0},10).to({startPosition:0},1).to({startPosition:0},166).to({alpha:0},5).to({_off:true},1).wait(43));

	// Layer_5
	this.instance_10 = new lib.Symbol8();
	this.instance_10.parent = this;
	this.instance_10.setTransform(640.1,116.2,1,1,0,0,0,0,-3.4);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.questxt = new lib.InstructionText_mccopy2();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(639.1,78.1,1.152,1.152,0,0,0,0.1,0);
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(94).to({_off:false},0).to({_off:true,regX:0.1,regY:0,scaleX:1.15,scaleY:1.15,x:639.1,y:78.1,alpha:1},10).wait(216));
	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(94).to({_off:false},10).wait(167).to({alpha:0},5).to({_off:true},1).wait(43));

	// Layer_2
	this.questxt_1 = new lib.InstructionText_mccopy();
	this.questxt_1.name = "questxt_1";
	this.questxt_1.parent = this;
	this.questxt_1.setTransform(639.1,78.1,1.152,1.152,0,0,0,0.1,0);
	this.questxt_1.alpha = 0;
	this.questxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt_1).wait(15).to({_off:false},0).to({alpha:1},8).wait(71).to({alpha:0},9).to({_off:true},1).wait(216));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(111.8,274.7,1919,907);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;