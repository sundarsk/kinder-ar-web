
///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 2, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg;
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;

var qno = [];
var chpos = [];
var objArr = [];
var quesArr = []
var quesMcArr = []
var chposArr = []

/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var tween
var chpos = 0;
var question1, question2;

var btnX = [150, 890]
var btnY = [450, 450]
var ary=[0,1,0,1,0,1,0,1,0,1]
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var qno = [];
var choiceArr = [];
var xArr = [];
var yArr = [];
var tweenMcArr = [];
var choicePos = [1, 2, 1, 2, 1, 2, 1, 2, 1, 2]
var qno1 = []
var qno2 = []
var qno1 = []


//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////

function init() {

    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "MysteryWord-Level1/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(

            { id: "choice", src: gameAssetsPath + "ChoiceImages1.png" },
            // {id: "choice2", src: gameAssetsPath+"ChoiceImages2.png"},
            { id: "question1", src: gameAssetsPath + "question1.png" },
            { id: "question", src: gameAssetsPath + "question.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
        )
        preloadAllAssets()
        stage.update();
    }

}

//=====================================================================//



function doneLoading1(event) {

    var event = assets[i];
    var id = event.item.id;
    console.log(" doneLoading ")
    loaderBar.visible = false;
    stage.update();


    if (id == "questionText") {
        questionText = new createjs.Bitmap(preload.getResult('questionText'));
        container.parent.addChild(questionText);
        questionText.visible = false;

    }

    if (id == "choice") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice")],
            "frames": { "regX": 50, "height": 309, "count": 0, "regY": 50, "width": 317 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        choice = new createjs.Sprite(spriteSheet1);
        container.parent.addChild(choice);
        choice.visible = false;
        //



    }

    if (id == "question") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question")],
            "frames": { "regX": 50, "height": 315, "count": 0, "regY": 50, "width": 316 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        question = new createjs.Sprite(spriteSheet1);
        question.visible = false;
        container.parent.addChild(question);

    };

    if (id == "question1") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question1")],
            "frames": { "regX": 50, "height": 315, "count": 0, "regY": 50, "width": 315 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        question1 = new createjs.Sprite(spriteSheet1);
        question1.visible = false;
        container.parent.addChild(question1);

    };
    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("D044BEAA30B3F146B78DA97BB48504C8");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = true;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }


}

function tick(e) {

    stage.update();
}
/////////////////////////////////////////////////////////////////=======GAME START========///////////////////////////////////////////////////////////////////

function handleClick(e) {
    toggleFullScreen()
    CreateGameStart()
    CreateGameElements();
    interval = setInterval(countTime, 1000);



}
function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Complete the shape";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";

    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 20
    bg.visible = true

    container.parent.addChild(questionText);
    questionText.visible = true;
   
    for (i =0; i < choiceCnt; i++) {
        choiceArr[i] = choice.clone()
        container.parent.addChild(choiceArr[i])
        choiceArr[i].visible = true;
        choiceArr[i].x = btnX[i];
        choiceArr[i].y = btnY[i];
      
        choiceArr[i].name = i;
         
    }



    question.visible = true;
    container.parent.addChild(question)
    question.x = 520; question.y = 215;
    
    question1.visible = false;
    container.parent.addChild(question1)
    question1.x = 520; question1.y = 215;
  
   ary.sort(randomSort)



   pickques();
}
function helpDisable() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = false
    }
}

function helpEnable() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = true
    }
}

//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    chpos = [];
      qno1=[]
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    boardMc.boardMc.openMc.mouseEnabled = true;
    qno = between(0, 11);
    
    question.gotoAndStop(qno[0]);
    question1.visible = false;
   question.visible = true;
    
    if(ary[cnt]==0)
    {
        console.log("if")
        qno1.push(0,1)
        } 
        else
        {
            console.log("else")
        qno1.push(1,0)
        } 
         console.log(qno1)
    for (i = 0; i < choiceCnt; i++) {
        
        choiceArr[qno1[i]].gotoAndStop(qno[i]);
        choiceArr[qno1[i]].name =  qno1[i]
       
    }
    
    console.log( qno1)
  
    ans =qno1[0]

    enablechoices();
    rst = 0;
    gameResponseTimerStart();

    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
}
function enablechoices() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].addEventListener("click", answerSelected);
        choiceArr[i].addEventListener("mouseover", onRoll_over);
        choiceArr[i].addEventListener("mouseout", onRoll_out)
        choiceArr[i].cursor = "pointer";
        choiceArr[i].visible = true;
        choiceArr[i].alpha = 1;
    }

}
function disablechoices() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].removeEventListener("click", answerSelected);
        choiceArr[i].removeEventListener("mouseover", onRoll_over);
        choiceArr[i].removeEventListener("mouseout", onRoll_out)
        choiceArr[i].cursor = "default";
        choiceArr[i].visible = false;
        choiceArr[i].alpha = .5;

    }
    boardMc.boardMc.openMc.mouseEnabled = false;
}
function onRoll_over(e) {
    e.currentTarget.alpha = .5;
    stage.update();
}
function onRoll_out(e) {
    e.currentTarget.alpha = 1;
    stage.update();
}

function answerSelected(e) {
    e.preventDefault();
    uans = e.currentTarget.name;

    gameResponseTimerStop();
    // pauseTimer();
    // console.log(ans+" =correct= "+uans)
    if (ans == uans) {
        getValidation("correct");
         question.visible = false;        
           question1.visible = true;       
            question1.gotoAndStop(qno[0])       
       setTimeout(correct, 500)
    } else {
        getValidation("wrong");
         disablechoices();
    }

  
}
 function correct()
       {    
            getValidation("correct");   
               disablechoices();
            }
//   gameResponseTimerStop();    
// //    pauseTimer();   
//     console.log(ans + " =correct= " + uans)   
//      if (ans == uans) {      
//            question.visible = false;        
//            question1.visible = true;       
//             question1.gotoAndStop(qno[0])       
//        setTimeout(correct, 500)
//        function correct()
//        {    
//             getValidation("correct");   
//                disablechoices();
//             }