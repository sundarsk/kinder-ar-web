(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3366").s().p("Ag4CFQgZgKgRgSQgRgSgKgaQgIgZgBggQAAgeALgbQALgbASgUQASgTAZgMQAZgLAcAAQAZAAAYAKQAWAKASASQARARAMAYQALAZADAcIjWAfQACARAHAOQAHAOALAKQALAKAOAFQAOAFAQAAQANAAANgEQAMgEALgIQAMgIAHgMQAIgLAFgQIAwAKQgIAXgMASQgNASgRAOQgRANgUAHQgUAHgWAAQggAAgagKgAgUhgQgMAEgMAIQgMAJgJAPQgJAOgEAWICUgSIgCgFQgJgZgQgOQgRgOgYAAQgKAAgMAEg");
	this.shape.setTransform(161,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3366").s().p("AgtCGQgagLgVgUQgUgTgMgaQgNgbAAgfQAAgRAFgSQAFgSAKgQQAJgRANgNQAOgOAQgKQAQgKATgFQATgGATAAQAWAAATAFQAUAGAQAKQARAJANAOQANAPAJASIABABIgpAZIgBgCQgGgNgKgJQgJgLgLgHQgMgHgNgEQgNgDgOAAQgTAAgSAHQgSAIgNAOQgNANgIASQgIASAAATQAAAUAIASQAIASANAOQANANASAIQASAHATAAQANAAANgDQAMgEALgGQALgGAJgJQAKgJAGgMIABgBIArAZIAAABQgKAQgOANQgOANgQAJQgRAKgTAFQgTAFgUAAQgbAAgagLg");
	this.shape_1.setTransform(130.4,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3366").s().p("Ag3CFQgagKgRgSQgSgSgJgaQgJgZAAggQAAgeALgbQALgbASgUQASgTAZgMQAZgLAbAAQAaAAAYAKQAWAKASASQARARAMAYQAMAZADAcIjXAfQACARAHAOQAHAOALAKQAKAKAPAFQAOAFAPAAQAOAAANgEQANgEALgIQALgIAIgMQAHgLAEgQIAxAKQgIAXgNASQgMASgRAOQgRANgUAHQgUAHgXAAQgfAAgZgKgAgUhgQgNAEgLAIQgMAJgJAPQgJAOgFAWICVgSIgBgFQgJgZgRgOQgQgOgaAAQgJAAgMAEg");
	this.shape_2.setTransform(100,6.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3366").s().p("AgfC7IABhAIAChOIABhpIAzgCIgCBAIgBA2IgBAsIgBAhIAAA2gAgNh1QgHgDgEgFQgFgFgDgGQgCgHAAgHQAAgHACgHQADgGAFgFQAEgFAHgDQAGgDAHAAQAHAAAGADQAHADAFAFQAFAFADAGQADAHAAAHQAAAHgDAHQgDAGgFAFQgFAFgHADQgGACgHAAQgHAAgGgCg");
	this.shape_3.setTransform(78.7,0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3366").s().p("AiIjJIA3gCIgBAkQAPgNAPgIQAPgIANgEQAPgEAOgBQARAAAQAEQARAFAPAJQAPAJAMAMQANANAJAPQAJAPAFASQAFASgBATQgBAVgGATQgFATgJAPQgKAPgMANQgNANgOAJQgOAIgPAFQgPAEgPABQgOgBgQgEQgNgEgRgHQgQgHgQgOIgBCpIgvABgAAAibQgPAAgNAFQgOAGgLAJQgKAIgIAMQgIAMgDANIgBAvQAEAQAIAMQAIANALAHQAKAJANAEQANAEANAAQAQABAPgGQAQgIALgLQALgLAHgQQAHgQABgSQABgTgFgQQgGgRgLgNQgLgNgQgHQgOgHgRAAIgCAAg");
	this.shape_4.setTransform(56.3,12.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF3366").s().p("Ag2DPQgLgDgMgFQgMgFgLgIQgLgJgJgLQgJgLgGgQIAwgUQAFAKAGAIQAHAIAIAFQAIAFAIADQAIAEAHABQATAEATgCQARgEANgHQANgHAJgJQAIgJAGgKQAGgKADgJQADgIABgIIABgLIAAghQgPAPgPAHQgRAIgNADQgQAEgPABQgcAAgZgKQgZgKgSgTQgTgTgKgaQgLgaAAgiQAAgiAMgaQALgbAUgSQASgSAZgJQAXgKAZAAQAQACARAGQAOAEARAJQARAJAPAPIAAgyIAxACIgCEcQAAAQgEAQQgEAQgIAPQgIAPgLAOQgMANgPAKQgPAKgSAHQgTAGgWACIgHABQgaAAgXgHgAgliXQgPAIgLANQgLAMgGASQgGASAAAUQAAAUAGASQAGARAMALQALAMAPAHQAPAHASAAQAQAAAPgFQAQgGAMgKQANgKAIgOQAJgOADgSIAAggQgCgSgJgOQgJgPgNgKQgNgLgQgFQgQgGgPAAQgSAAgPAHg");
	this.shape_5.setTransform(7.9,12.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF3366").s().p("AhzA9IgDg9IgBgwIgBgkIgCg1IA5gBIABBIQAIgOALgNQAKgMAMgKQAMgKANgGQAOgGAPgBQATgBAOAFQAOAEAKAIQAKAIAGALQAHALADALQAEAMACALIACAVQABAqAAArIgCBaIgzgCIAEhQQABgogCgoIAAgLIgDgPQgCgIgEgIQgEgIgGgGQgGgGgIgDQgJgDgLACQgUADgTAaQgUAagYAwIACBAIABAkIABATIg2AHIgChOg");
	this.shape_6.setTransform(-23.8,6.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF3366").s().p("AgfC7IABhAIABhOIAChpIAygCIgBBAIgBA2IAAAsIgBAhIgBA2gAgNh1QgGgDgFgFQgFgFgDgGQgCgHAAgHQAAgHACgHQADgGAFgFQAFgFAGgDQAGgDAHAAQAHAAAGADQAHADAFAFQAEAFADAGQADAHABAHQgBAHgDAHQgDAGgEAFQgFAFgHADQgGACgHAAQgHAAgGgCg");
	this.shape_7.setTransform(-45.3,0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF3366").s().p("AgjCNIgVgFIgXgIQgLgEgMgHQgLgGgLgIIAYgmIAdAOIAdAMQAOAFAQADQAPADAQAAQAOAAAKgCIAPgHQAGgEAEgFIAEgJQACgEgBgFQAAgFgCgFQgDgFgEgEQgFgFgIgEQgIgEgMgCQgLgDgQgBQgVgBgVgDQgVgEgQgIQgQgHgLgMQgKgMgCgTQgCgSAEgPQAEgPAIgLQAJgMAMgJQAMgJAOgFQAPgGAQgDQAQgDAPAAIAXABQANABAPADQAOADAPAGQAOAFANAJIgRAuQgRgJgOgFIgagHQgOgDgMgBQgmgDgVALQgWAKAAAVQAAAPAIAHQAIAHAPADQAOADATABQARABAWADQAYAEARAHQAQAHALAKQAKAKAEAMQAFAMAAANQAAAXgKARQgJAPgQALQgQAKgVAFQgVAEgWABQgWAAgXgEg");
	this.shape_8.setTransform(-66.4,7.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF3366").s().p("AgjCNIgVgFIgXgIQgLgEgMgHQgLgGgLgIIAYgmIAdAOIAdAMQAOAFAQADQAPADAQAAQAOAAAKgCIAPgHQAGgEAEgFIAEgJQACgEgBgFQAAgFgCgFQgDgFgEgEQgFgFgIgEQgIgEgMgCQgLgDgQgBQgVgBgVgDQgVgEgQgIQgQgHgLgMQgKgMgCgTQgCgSAEgPQAEgPAIgLQAJgMAMgJQAMgJAOgFQAPgGAQgDQAQgDAPAAIAXABQANABAPADQAOADAPAGQAOAFANAJIgRAuQgRgJgOgFIgagHQgOgDgMgBQgmgDgVALQgWAKAAAVQAAAPAIAHQAIAHAPADQAOADATABQARABAWADQAYAEARAHQAQAHALAKQAKAKAEAMQAFAMAAANQAAAXgKARQgJAPgQALQgQAKgVAFQgVAEgWABQgWAAgXgEg");
	this.shape_9.setTransform(-94.6,7.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF3366").s().p("AgfC7IABhAIAChOIABhpIAygCIgBBAIgBA2IgBAsIgBAhIAAA2gAgNh1QgHgDgEgFQgFgFgDgGQgCgHAAgHQAAgHACgHQADgGAFgFQAEgFAHgDQAGgDAHAAQAHAAAGADQAHADAFAFQAEAFADAGQAEAHAAAHQAAAHgEAHQgDAGgEAFQgFAFgHADQgGACgHAAQgHAAgGgCg");
	this.shape_10.setTransform(-114.4,0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF3366").s().p("AB4A7IgBg1IgBgoQgBgUgFgKQgHgKgJAAQgGAAgGAEQgHADgGAGIgMAOIgMAQIgLAPIgIANIABAXIABAeIABAlIAAAvIgwACIgBhNIgCg1IgCgoQAAgUgHgKQgFgKgKAAQgHAAgGAEQgGAEgIAHQgGAHgHAIIgMASIgMAQIgJANIADCDIgyACIgHkKIA0gFIABBIIARgUQAJgLAKgJQAKgIANgGQAMgGAPAAQAKAAAKAEQAJADAHAHQAIAHAFALQAGALABAPIARgUQAIgKAKgIQALgJALgFQAMgGAOAAQALAAAKAEQALAEAIAIQAIAIAFAMQAFAMABARIACAuIACA7IABBXIgzACIAAhNg");
	this.shape_11.setTransform(-141.8,5.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(3,1,1).p("A5elTMAy9AAAQCgAAAACgIAAFnQAACgigAAMgy9AAAQigAAAAigIAAlnQAAigCgAAg");
	this.shape_12.setTransform(1.8,7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("A5dFUQihAAAAigIAAlnQAAigChAAMAy7AAAQChAAAACgIAAFnQAACgihAAg");
	this.shape_13.setTransform(1.8,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-180.7,-41,363.1,83.5);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#98C8F7").s().p("AnXgQQBug9CBg3QE5iBF7hGQAMAZAAAmIAACcIgDAAQn1CKlqD7QgoAaglAcg");
	this.shape.setTransform(-16.7,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EEBE25").s().p("Ap+H3IAAg4IP4AAIAHAAQByAAA4A4gAGBGLIgDAAQgEgCgBgCQgCgGADgCQAAgDAFgFIACAAQAzgOAmgmQBAhBgJiBIAAgFQgjgCgYgcQgjgcABgoIAAjSQgBgnAjgeQAZgeApAAIACAAQAAgNgCgNQgKhEg0gzQgmgpgzgPIgCAAQgFAAAAgBIAAgDQgDgEACgEQABgEAEgDIADAAIAEAAQAOADAPAFQAlATAiAiQA3A3ALBLQADAOAAAPIAAACQAeAFAVAXQAeAeAAAnIAADSQAAAogeAcQgbAcgiACQAKCMhFBJQgsArg4APIgEAAg");
	this.shape_1.setTransform(0,20);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#84C0FC").s().p("AFmHJIs9AAIAAjHQAlgcAogaQFqj6H1iLIADAAIAAIOQAABkhaAQIgYAAgAnXnHIM9AAQAuAAAcATQASALAKAVQl7BGk5CBQiBA3huA9g");
	this.shape_2.setTransform(-16.7,13.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F7FFC2").s().p("AlQBiIAAjDIJAAAQAnABAdAdQAdAcAAAnQAAAogdAfQgdAbgnAAg");
	this.shape_3.setTransform(-30.2,-54.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E0AE10").s().p("AH5KjIv4AAIAAg5IM8AAIAYAAQBagPAAhlIAAoOIAAicQAAgmgMgYQgKgWgSgLQgbgTgvAAIs8AAIAAh9II/AAQAoAAAcgbQAdgfAAgoQAAgogdgcQgcgdgoAAIo/AAIAAg8IMeAAQDhAAAADlIAACqIgEAAQgDADgBAEQgDAEAEADIAAAEQAAABAFAAIACAAIAANbIgCAAQgFAEAAAEQgEACADAFQABADADACIAEAAIAAA0IgHAAg");
	this.shape_4.setTransform(-12.7,-2.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-63.9,-70.3,127.8,140.6);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EEBE25").s().p("AquIdIAAg9IREAAIAIAAQB6ABA9A8gAGeGpIgEAAQgEgCgBgEQgCgFADgCQAAgEAFgFIADAAQA2gQApgpQBFhFgJiLIAAgEQgngDgZgeQglgfAAgqIAAjiQAAgqAlggQAbggAtAAIABAAQAAgPgBgNQgMhKg4g1Qgpgsg2gRIgDAAQgFAAAAAAIAAgFQgDgEACgDQABgFAEgDIAEAAIAEAAQAPADAPAGQAqAUAjAkQA7A7AMBSQADAPAAAPIAAABQAgAHAYAYQAfAgABAqIAADiQgBAqgfAfQgdAeglADQALCWhLBOQgvAvg8AQIgEAAg");
	this.shape.setTransform(0,21.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#98C8F7").s().p("An6gRQB3hCCKg6QFQiLGXhLQANAaAAApIAACnIgDAAQobCVmFENQgrAdgnAeg");
	this.shape_1.setTransform(-18,6.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#84C0FC").s().p("AGAHqIt6AAIAAjUQAngfArgcQGFkMIbiWIADAAIAAI1QgBBshgAQIgaAAgAn6npIN6AAQAyAAAeAUQATAMALAXQmXBLlQCLQiKA7h3BCg");
	this.shape_2.setTransform(-18,14.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F7FFC2").s().p("AlpBpIAAjRIJrAAQAqAAAfAgQAfAeAAAqQAAArgfAhQgfAdgqAAg");
	this.shape_3.setTransform(-32.5,-58.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E0AE10").s().p("AIeLVIxDAAIAAg9IN6AAIAaAAQBggQAAhtIAAo1IAAinQAAgpgNgbQgKgXgUgMQgdgUgyAAIt6AAIAAiHIJqAAQArAAAegcQAfghAAgsQAAgqgfgfQgegfgrAAIpqAAIAAhAINaAAQDxAAAAD2IAAC2IgEAAQgEAEgBAEQgCAEADAEIAAAEQAAABAGAAIACAAIAAOaIgCAAQgGAFAAAEQgDADACAFQABAEAEABIAEAAIAAA4IgIAAg");
	this.shape_4.setTransform(-13.6,-3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.6,-75.5,137.3,151.1);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhKE/QgVgWAAgfQAAgeAVgWQAWgVAfAAQAdAAAWAVQAVAWAAAeQAAAfgVAWQgWAWgdgBQgfABgWgWgAhFCEIgIgYQgIgTAAgPQAAgdAIgaQAIgZAPgXQAPgXAtgyQAtgxAAghQAAg/hSAAQgoAAgrAoIgvhXQA6gtBfAAQBIAAAxAoQAxApAABCQAAAugSAhQgSAfgsArQgtAqgOAcQgNAcAAAjQAAAGAGAgg");
	this.shape.setTransform(-11.7,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AxRLFIAA2JMAijAAAIAAWJg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-110.6,-70.9,221.3,141.8);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F7FFC2").s().p("AlNBhIAAjBII7AAQAnAAAcAdQAdAdgBAmQABAngdAfQgcAbgnAAg");
	this.shape.setTransform(33.4,15.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#98C8F7").s().p("AnSgPQBtg9CAg2QE1iAF4hFQALAYAAAmIAACaIgCAAQnxCJlmD4QgoAagkAcg");
	this.shape_1.setTransform(46.7,75.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EEBE25").s().p("Ap4HzIAAg4IPuAAIAIAAQBwAAA4A4gAF+GHIgEAAQgEgBgBgDQgCgFADgDQABgDAFgEIACAAQAxgPAngmQA/hAgJh/IAAgFQgjgCgYgcQghgcAAgnIAAjRQAAgmAhgeQAagdApAAIABAAQAAgOgBgNQgLhDgzgyQgngogxgPIgCAAIgGgBIAAgEQgDgDACgEQABgEAEgDIAEAAIAEAAQAOADAOAFQAmASAgAiQA2A2AMBMQADANAAAPIAAABQAeAGAVAWQAdAeAAAmIAADRQAAAngdAcQgbAcgjACQALCKhFBJQgrArg3AOIgEAAg");
	this.shape_2.setTransform(63.3,89.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#84C0FC").s().p("AFjHEIs1AAIAAjEQAkgcAogbQFmj2HxiKIACAAIAAIJQAABkhZAOIgXAAgAnSnDIM1AAQAtAAAcATQASAKAKAWQl4BFk1CAQiAA2htA9g");
	this.shape_3.setTransform(46.7,82.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E0AE10").s().p("AH0KdIvvAAIAAg5IM1AAIAYAAQBZgOAAhkIAAoJIAAibQAAglgMgZQgJgWgTgKQgbgTguAAIs1AAIAAh8II7AAQAnAAAcgbQAdgegBgpQABgmgdgcQgcgdgnAAIo7AAIAAg8IMXAAQDgABAADiIAACpIgFAAQgDADgBAEQgDADAEAEIAAAEIAFAAIADAAIAANUIgDAAQgFAEAAADQgEACADAFQABAEADABIAFAAIAAA0IgIAAg");
	this.shape_4.setTransform(50.8,66.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,126.6,139.3), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.QuestionText_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape.setTransform(271.2,35.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYBLQgPgHgLgKQgMgLgHgPQgHgPAAgRQAAgJADgKQADgKAFgJQAFgJAIgIQAHgHAJgFQAJgGALgDQAKgDALAAQAMAAAKADQALADAJAFQAKAGAHAHQAHAIAFAKIABAAIgXAPIgBgBQgDgIgFgFQgFgGgHgEQgGgDgHgCQgIgCgHgBQgLABgKAEQgJAEgIAHQgHAIgFAKQgEAKAAAKQAAALAEALQAFAJAHAIQAIAHAJAEQAKAEALABIAOgCQAHgCAGgDQAGgEAFgFQAFgFAEgGIAAgCIAYAOIAAABQgFAJgIAIQgIAGgJAFQgJAGgLACQgLADgKAAQgPAAgOgFg");
	this.shape_1.setTransform(254.2,35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_2.setTransform(237.3,35.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgRBoIABgjIABgsIABg6IAbgBIgBAjIAAAeIgBAYIAAATIAAAegAgHhBIgGgEQgDgCgBgEQgCgEAAgEQAAgEACgDQABgEADgDIAGgEQAEgCADAAQAEAAADACQAEACADACIAEAHQACADAAAEQAAAEgCAEIgEAGQgDADgEABQgDACgEAAQgDAAgEgCg");
	this.shape_3.setTransform(225.4,31.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhLhvIAegBIAAAUQAHgIAKgEIAPgHQAJgCAGgBQAKAAAJADQAKADAHAFQAJAEAHAHQAHAHAFAJQAFAIACAKQADAKAAAKQgBAMgDALQgDAKgFAIQgGAJgGAHQgHAHgIAFQgHAFgJACQgJADgHAAIgRgDIgQgGQgJgEgJgIIgBBeIgaABgAgQhTQgHADgGAFQgGAFgEAHQgEAGgDAHIAAAaQACAJAFAHQAEAHAGAEQAGAEAHADQAHACAIAAQAIABAJgEQAIgEAHgFQAFgHAFgJQADgJABgKQABgKgDgJQgEgKgGgHQgGgHgJgEQgIgEgKAAQgIAAgIADg");
	this.shape_4.setTransform(213,38.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgdBzIgOgFIgMgHIgLgLQgFgGgDgJIAagLQADAGAEAEQADAEAEADQAFADAEACQAFACAEAAQAKADAKgCQAKgCAHgEQAHgEAFgEQAFgFADgGIAFgKIADgJIAAgHIAAgSQgIAIgJAEQgJAFgIACIgQACQgQAAgOgFQgOgGgKgKQgKgLgGgOQgGgPABgSQAAgTAFgPQAHgOALgKQALgLANgFQANgFAOAAQAIABAJADIASAHQAJAFAJAJIAAgcIAbABIgBCdIgCASQgDAJgEAJQgEAIgHAHQgGAIgJAFQgIAGgKAEQgLADgMABIgDABQgOAAgNgEgAgUhTQgIAEgHAHQgFAHgEAKQgDAKAAALQAAALADAKQADAJAHAGQAGAHAJAEQAIAEAKAAQAIAAAIgDQAJgEAHgFQAHgFAFgIQAFgIACgKIAAgSQgCgJgFgIQgFgJgHgFQgHgGgJgDQgIgDgJAAQgKAAgIAEg");
	this.shape_5.setTransform(186.1,38.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhAAiIgBgiIgBgaIgBgUIAAgdIAfgBIABAoQAEgIAGgHQAGgHAHgFQAGgGAHgDQAIgDAIgBQALgBAHADQAIACAGAFQAFAEAEAGQADAGACAHIADAMIACAMIAAAvIgBAxIgcgBIACgsQABgWgCgWIAAgGIgBgJIgEgIQgCgFgDgDQgDgDgFgCQgFgBgGAAQgLACgKAOQgMAPgNAaIABAkIABATIAAALIgdAEIgCgrg");
	this.shape_6.setTransform(168.5,35);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgRBoIABgjIABgsIABg6IAbgBIgBAjIAAAeIgBAYIAAATIAAAegAgHhBIgGgEQgDgCgBgEQgCgEAAgEQAAgEACgDQABgEADgDIAGgEQAEgCADAAQAEAAADACQAEACADACIAEAHQACADAAAEQAAAEgCAEIgEAGQgDADgEABQgDACgEAAQgDAAgEgCg");
	this.shape_7.setTransform(156.5,31.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgTBOIgMgDIgNgEIgMgGIgMgIIANgVIAQAIQAHAEAJACIARAFQAHACAKAAQAHAAAGgCIAIgDIAFgFIADgFIABgFIgCgGIgEgFIgHgFIgLgEQgGgBgJAAQgLgBgMgCQgLgCgJgEQgJgFgGgFQgGgHgBgLQgBgKACgIQACgIAFgHQAFgGAGgFQAHgFAIgDQAIgEAJgBQAJgCAIAAIANABIAPACIAQAFQAIADAHAFIgJAZQgJgFgIgCIgPgEIgOgDQgVgBgMAGQgMAGAAALQAAAJAFAEQAEADAIACQAIACALAAIAVACQANADAKAEQAJADAGAFQAFAGADAHQACAGAAAIQAAANgFAJQgFAIgJAGQgJAGgMACQgLADgNABQgLgBgNgCg");
	this.shape_8.setTransform(144.9,35.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgTBOIgMgDIgNgEIgMgGIgMgIIANgVIAQAIQAHAEAJACIARAFQAHACAKAAQAHAAAGgCIAIgDIAFgFIADgFIABgFIgCgGIgEgFIgHgFIgLgEQgGgBgJAAQgLgBgMgCQgLgCgJgEQgJgFgGgFQgGgHgBgLQgBgKACgIQACgIAFgHQAFgGAGgFQAHgFAIgDQAIgEAJgBQAJgCAIAAIANABIAPACIAQAFQAIADAHAFIgJAZQgJgFgIgCIgPgEIgOgDQgVgBgMAGQgMAGAAALQAAAJAFAEQAEADAIACQAIACALAAIAVACQANADAKAEQAJADAGAFQAFAGADAHQACAGAAAIQAAANgFAJQgFAIgJAGQgJAGgMACQgLADgNABQgLgBgNgCg");
	this.shape_9.setTransform(129.2,35.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgRBoIABgjIABgsIABg6IAbgBIgBAjIAAAeIgBAYIAAATIAAAegAgHhBIgGgEQgDgCgBgEQgCgEAAgEQAAgEACgDQABgEADgDIAGgEQAEgCADAAQAEAAADACQAEACADACIAEAHQACADAAAEQAAAEgCAEIgEAGQgDADgEABQgDACgEAAQgDAAgEgCg");
	this.shape_10.setTransform(118.1,31.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("ABCAhIAAgeIAAgVQgBgMgDgFQgDgGgGAAQgDAAgDACQgEACgEAEIgGAHIgHAJIgGAJIgFAHIABAMIABARIAAAUIAAAaIgaACIAAgrIgBgeIgCgVQAAgMgDgFQgEgGgFAAQgEAAgDADIgIAGIgHAIIgHAKIgGAJIgFAHIABBIIgcACIgDiTIAdgEIAAAoIAJgLIALgLQAGgEAHgDQAGgEAIAAQAGAAAFACQAGACADAEQAFAEADAGQADAGABAIIAJgLIAKgKQAGgFAGgDQAGgDAIAAQAGAAAGACQAGADAFAEQAEAEADAHQADAHAAAJIACAaIABAgIAAAwIgdACIAAgrg");
	this.shape_11.setTransform(102.9,34.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_12.setTransform(75.8,35.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAlBpQAEgNACgNIACgWIABgWQgBgPgEgKQgFgKgGgGQgHgHgHgDQgIgDgIAAQgGABgIAEQgHADgIAHQgIAGgHANIgBBbIgbABIgCjUIAggBIgBBUQAHgIAJgFIAQgHQAJgDAHgBQAPAAANAFQANAGAJAKQAJALAFAOQAGAOAAATIAAAUIgBAUIgCASIgDAQg");
	this.shape_13.setTransform(58.2,31.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgQgPIgwABIABgaIAwgBIABg9IAagBIgBA9IA2gBIgCAaIg0ABIgBB3IgcABg");
	this.shape_14.setTransform(42.1,32.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgQgPIgwABIABgaIAwgBIABg9IAagBIgBA9IA2gBIgCAaIg0ABIgBB3IgcABg");
	this.shape_15.setTransform(21.3,32.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgYBLQgPgHgLgKQgMgLgHgPQgHgPAAgRQAAgJADgKQADgKAFgJQAFgJAIgIQAHgHAJgFQAJgGALgDQAKgDALAAQAMAAAKADQALADAJAFQAKAGAHAHQAHAIAFAKIABAAIgXAPIgBgBQgDgIgFgFQgFgGgHgEQgGgDgHgCQgIgCgHgBQgLABgKAEQgJAEgIAHQgHAIgFAKQgEAKAAAKQAAALAEALQAFAJAHAIQAIAHAJAEQAKAEALABIAOgCQAHgCAGgDQAGgEAFgFQAFgFAEgGIAAgCIAYAOIAAABQgFAJgIAIQgIAGgJAFQgJAGgLACQgLADgKAAQgPAAgOgFg");
	this.shape_16.setTransform(6,35.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_17.setTransform(-10.9,35.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAJB4QgIgDgFgFQgFgGgEgGQgEgGgDgHQgHgQgCgUIAFitIAbAAIgBAtIgBAlIgBAeIAAAWIgBAmQABANADAJIADAJIAGAHIAIAGQAEACAGAAIgEAbQgJAAgIgDg");
	this.shape_18.setTransform(-22.5,30.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_19.setTransform(-35.6,35.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgRBvQgLgCgKgEQgKgEgJgGIgUgMIAUgYQALAJALAGQAKAFAJADQAKADAIABQAJABAJgCQAJgCAGgEQAGgEAEgFQAEgGABgGQAAgFgCgEQgCgFgEgDQgFgEgGgCIgMgFIgNgEIgNgDIgQgDQgJgCgJgEQgIgEgHgFQgIgFgFgIQgGgIgDgKQgCgKABgOQAAgLAEgIQAEgJAGgHQAGgIAIgEQAIgGAIgDQAJgDAKgCQAJgBAJAAQAMABAOADIALAEIANAFIAMAHIALAJIgPAYIgJgIIgKgHIgKgEIgKgEQgKgDgLAAQgLAAgKAEQgKAEgGAGQgHAGgEAHQgDAHAAAIQAAAHAEAGQAEAHAIAFQAHAGAKADQAKAEALACIAUADIATAGQAJAEAIAFQAHAFAGAIQAFAHACAJQADAIgCALQgBAKgEAHQgEAIgGAFQgGAGgIAEQgHAEgJACQgIACgIABIgQABQgKAAgKgCg");
	this.shape_20.setTransform(-53.2,32.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFCC00").ss(3,1,1).p("A8LjMMA4XAAAQB3AAAAB7IAACjQAAB7h3AAMg4XAAAQh3AAAAh7IAAijQAAh7B3AAg");
	this.shape_21.setTransform(109.2,35);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E21649").s().p("A8LDNQh3AAAAh7IAAijQAAh7B3AAMA4XAAAQB3AAAAB7IAACjQAAB7h3AAg");
	this.shape_22.setTransform(109.2,35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.QuestionText_mc, new cjs.Rectangle(-84.5,8.7,387.6,48.3), null);


(lib.questioncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(2.5,1,1).p("A5lAAMAzLAAA");
	this.shape.setTransform(0.1,0,1.43,1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(2.5,1,1).p("AAA3dMAAAAu7");
	this.shape_1.setTransform(0.2,0,1.43,1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(2.5,1,1).p("EgkmgXdMBJNAAAMAAAAu7MhJNAAAg");
	this.shape_2.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3A3737").s().p("Aj3E3IAAg4IFnAAQAWAAAOgSQASgOAAgXQAAgUgSgSQgOgPgWAAIlnAAIAAhhIFnAAQAWAAAOgQQASgPAAgVQAAgVgSgQQgOgSgWAAIlnAAIAAheIFnAAQAWAAAOgRQASgPAAgVQAAgXgSgQQgOgPgWAAIlnAAIAAg0IF6AAQBmAAAMBWQACAMABASIAABTIAABdIAADVQgBB0h0AAg");
	this.shape_3.setTransform(24.8,58.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EEBE25").s().p("ApoAXIAAguISvAAQAVAUANAag");
	this.shape_4.setTransform(61.7,2.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#282727").s().p("AEhIhQg9AAguguQgpgsAAhBIAAkjIFuAAIAHAAIAAEjQAABBguAsQgnAuhAAAgAoBgfIAAhsIFnAAQAWAAAPAPQARATAAAUQAAAXgRAOQgPARgWAAgAoBjrIAAhsIFnAAQAWAAAPASQARAPAAAWQAAAWgRAPQgPAQgWAAgAoBm2IAAhqIFnAAQAWAAAPAPQARAQAAAWQAAAWgRAOQgPASgWgBg");
	this.shape_5.setTransform(51.4,87.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgaC2Qg6gGgqguQgUgUgMgVQgYgoAAgvQAAhJA4g3QA1g3BLAAQBKAAA2A3QA1A3AABJQAAAvgUAoQgOAVgTAUQgsAug5AGg");
	this.shape_6.setTransform(93.5,38.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#DEB50A").s().p("ACpDIQAUgoAAgvQABhLg2g1Qg1g4hLAAQhLAAg0A4Qg4A1AABLQAAAvAXAoIglAAQg3hAAAhdQAAhVA2hDIAQgUQBJhGBlAAQBnAABGBGQAMAKAEAKQA4BDAABVQAABdg4BAg");
	this.shape_7.setTransform(92.9,27.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FECF09").s().p("AGKGUIluAAIqOAAIAAhKIF6AAQB1AAAAh0IAAjWIGcAAIAdAAIAbAAIEiAAIAACxQAACHhSA2Qg2AkhaACIgHAAgAHrhcQA3hAAAhdQAAhXg3hDIB5AAIAEBXIALDggAiDhcIAAhUQAAgRgDgNQgMhVhmAAIl6AAIAAhwILgAAQg2BDAABXQAABdA3BAg");
	this.shape_8.setTransform(62.7,56.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#212B28").s().p("AFREEQA5gGArguQAUgUAOgVIAUAAICIAAIAABdgAiDEEIAAhdIDyAAIAlAAQANAVAUAUQAqAuA6AGgAHriOQgFgKgMgKQhGhFhnAAQhmAAhJBFIgQAUIrgAAIAAh1ITRAAIAFB1g");
	this.shape_9.setTransform(62.7,30.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhiCdQgSgNgNgNQgUgUgKgVQgYgoAAgvQAAhKA2g2QA1g3BMgBQBJABA5A3QA2A2AABKQAAAvgYAoQgKAVgUAUQgfAfgfAMQgeAIgdgBQg6AAgxgYg");
	this.shape_10.setTransform(-94.2,38.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#DEB50A").s().p("ACYDIQAYgoAAgvQAAhLg1g1Qg5g4hKAAQhLAAg2A4Qg1A1AABLQAAAvAXAoIgVAAQg3hAAAhdQAAhVA1hDQAHgKAKgKQBHhGBnAAQBmAABHBGIARAUQA1BDAABVQAABdg1BAg");
	this.shape_11.setTransform(-93.5,27.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EEBE25").s().p("ApsAXQANgaAUgUIS4AAIAAAug");
	this.shape_12.setTransform(-62,2.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#212B28").s().p("AkYEEQATgBAUgIQAfgMAfgfQAUgUAKgVIAmAAIDrAAIAABdgAp3EEIAAhdICNAAIAVAAQAKAVAUAUQANANASANQAfAVAqAFgAhviOIgRgUQhHhFhlAAQhpAAhGBFQgKAKgIAKIiKAAQAAhCAXgzITYAAIAAB1g");
	this.shape_13.setTransform(-63.2,30.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#282727").s().p("AlqIhQhAAAgsguQgogsAAhBIAAkjIF0AAIAAEjQAABBgsAsQgrAug8AAgACZgfQgVAAgSgRQgPgOAAgXQAAgUAPgTQASgPAVAAIFmAAIAABsgACZjrQgVAAgSgQQgPgPAAgWQAAgWAPgPQASgSAVAAIFmAAIAABsgACZm2QgVABgSgSQgPgOAAgWQAAgWAPgQQASgPAVAAIFmAAIAABqg");
	this.shape_14.setTransform(-51.1,87.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3A3737").s().p("AiKE3QhzAAAAh0IAAjVIAAhdIAAhTQAAgSACgMQANhWBkAAIGIAAIAAA0IllAAQgVAAgSAPQgPAQABAXQgBAVAPAPQASARAVAAIFlAAIAABeIllAAQgVAAgSASQgPAQABAVQgBAVAPAPQASAQAVAAIFlAAIAABhIllAAQgVAAgSAPQgPASABAUQgBAXAPAOQASASAVAAIFlAAIAAA4g");
	this.shape_15.setTransform(-25.4,58.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FECF09").s().p("AgQGUIl0AAIgNAAQhcgBg3glQhTg2AAiHIAAixIEoAAIAZAAIAeAAIGUAAIAADWQAAB0BzAAIGJAAIAABKgAhvhcQA2hAAAhdQAAhXg2hDILnAAIAABwImJAAQhkAAgMBVQgDANAAARIAABUgAp3hcIAAk3ICKAAQg1BDAABXQAABdA4BAg");
	this.shape_16.setTransform(-63.2,56.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#98C8F7").s().p("AkOETQA9iJBoh9QCWigDih/IAAFeQh9BehlBpg");
	this.shape_17.setTransform(-27.6,-39);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#84C0FC").s().p("AD7HMQBlhpB9heIAADHgAloHMIgNAAQhngJAAhrIAAqvQAAhGApgbQAegTAtAAINFAAIAAFyQjiB+iXChQhnB9g9CJg");
	this.shape_18.setTransform(-48.2,-57.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#EEBE25").s().p("Ao0H8QA5g5B2AAIAEAAIQAAAIAAA5gAmBGPIgEAAQg2gPgsgsQhFhKAHiMQgjgDgYgcQgegdAAgnIAAjVQAAgnAegeQAUgWAggHIAAgBQAChcBDhFQAsguA2gPIAEAAIAFAAQAEAEAAAEQABADgBAEQAAAEgFABIgEAAQgyAPgmApQg9A7gEBXIACAAQApAAAcAeQAgAeAAAnIAADVQAAAnggAdQgaAcglADIAAAEQgICCBBBCQAmAmAyAPIAEAAQAFAEAAADQABADgBAFQAAADgEACIgFAAg");
	this.shape_19.setTransform(-64.4,-50.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F7FFC2").s().p("AjNBiQgqAAgagbQgegeAAgpQAAgnAegdQAageAqAAIH9AAIAADEg");
	this.shape_20.setTransform(-30.9,-125.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E0AE10").s().p("AoAKoIAAg0IAFAAQAEgBAAgEQABgFgBgCQAAgEgFgEIgEAAIAAtiIAEAAQAFgBAAgEQABgDgBgEQAAgEgEgDIgFAAIAAisQAAjmDlAAIMcAAIAAA7In+AAQgpAAgaAeQgeAdAAAnQAAApAeAfQAaAbApAAIH+AAIAAB/ItFAAQgtAAgeATQgpAbAABFIAAKwQAABqBnAJIANAAIEoAAIE7AAIDiAAIAAA6g");
	this.shape_21.setTransform(-51.8,-73.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 2
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#993300").ss(5,1,1).p("EglNgX+IAGAAMBKVAAAMAAAAv9MhKVAAAIgGAAg");
	this.shape_22.setTransform(0.2,0.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("EAlIAX/MAAAgv9MAAAAv9MhKVAAAMAAAgv9MBKVAAAIAFAAMAAAAv9g");
	this.shape_23.setTransform(0.8,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy, new cjs.Rectangle(-240.5,-155.9,481.3,312), null);


(lib.option1copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(6,1,1).p("AtBnSIAAOkQAABFBGAAIX3AAQBGAAAAhFIAAukQAAhEhGAAI33AAQhGAAAABEg");
	this.shape.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#98C8F7").s().p("Ak0gKQBIgoBUgkQDNhVD5gtQAHAQAAAZIAABmIgCAAQlIBajtCkQgaASgYASg");
	this.shape_1.setTransform(-50.1,8.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EEBE25").s().p("AmiFKIAAglIKaAAIAFAAQBKABAlAkgAD9EDIgDAAQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBAAAAgBQgBgEACgBQAAgCADgDIACAAQAhgJAZgaQAqgqgGhVIAAgCQgXgCgQgTQgWgSAAgZIAAiLQAAgZAWgUQARgTAbAAIABAAIgBgSQgHgsgighQgZgaghgLIgCAAIgDAAIAAgDQgBAAAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQABgDACgCIADAAIACAAIATAGQAZALAVAWQAkAkAIAzQACAJAAAJIAAABQATAEAPAOQATAUAAAZIAACLQAAAZgTASQgSATgXACQAHBbguAwQgdAcgkAKIgCAAg");
	this.shape_2.setTransform(-39.2,17.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#84C0FC").s().p("ADqErIoeAAIAAiCQAYgSAagSQDtiiFIhcIACAAIAAFYQAABCg7AKIgQAAgAk0kqIIeAAQAfAAASAMQAMAIAHAOQj5AujNBUQhUAkhIApg");
	this.shape_3.setTransform(-50.1,12.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E0AE10").s().p("AFLG6IqaAAIAAglIIfAAIAQAAQA6gKAAhCIAAlYIAAhmQAAgZgHgQQgHgPgMgHQgSgMgeAAIofAAIAAhSIF5AAQAaAAATgSQATgUAAgbQAAgZgTgTQgTgTgaAAIl5AAIAAgnIILAAQCUAAAACWIAABvIgDAAQgCACgBADQAAABAAAAQAAABAAAAQAAABAAABQAAAAABABIAAACIADABIACAAIAAIyIgCAAQgDADAAACQgCACABADQAAABABAAQAAABAAAAQABAAAAABQABAAAAAAIADAAIAAAiIgFAAg");
	this.shape_4.setTransform(-47.5,2.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F7FFC2").s().p("AjcBAIAAh/IF5AAQAaAAATAUQATASAAAZQAAAagTAUQgTASgaAAg");
	this.shape_5.setTransform(-59,-31.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// ClickArea
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0)").s().p("AtTH4IAAvvIanAAIAAPvg");
	this.shape_6.setTransform(2,-0.2,1.053,1.233);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer 5
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1E66D2").ss(6,1,1).p("AtBnSIAAOkQAABFBGAAIX3AAQBGAAAAhFIAAukQAAhEhGAAI33AAQhGAAAABEg");
	this.shape_7.setTransform(0.1,0);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AsSInQhIAAAAhHIAAu/QAAhHBIAAIYlAAQBIAAAABHIAAO/QAABHhIAAgAL8IWQBGAAAAhEIAAukQAAhDhGAAI33AAQhGAAAABDQAAhDBGAAIX3AAQBGAAAABDIAAOkQAABEhGAAI33AAQhGAAAAhEIAAukIAAOkQAABEBGAAIX3AAg");
	this.shape_8.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.option1copy5, new cjs.Rectangle(-87.7,-62.3,179.4,124.4), null);


(lib.option1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(6,1,1).p("AtBnSIAAOkQAABFBGAAIX3AAQBGAAAAhFIAAukQAAhEhGAAI33AAQhGAAAABEg");
	this.shape.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_8
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EEBE25").s().p("AmsAQQAJgRAOgOINCAAIAAAfg");
	this.shape_1.setTransform(40.6,-52.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#282727").s().p("Aj6F4QgsAAgfgeQgbggAAgtIAAjJIEBAAIAADJQAAAtgfAgQgdAegqAAgABpgVQgOABgMgMQgLgKAAgQQAAgOALgNQAMgKAOAAID4AAIAABKgABpiiQgOAAgMgLQgLgKAAgQQAAgOALgMQAMgLAOgBID4AAIAABLgABpktQgOgBgMgLQgLgLAAgOQAAgQALgLQAMgKAOAAID4AAIAABKg");
	this.shape_2.setTransform(48.2,6.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3A3737").s().p("AhfDXQhQgBABhPIAAiTIAAhBIAAg5QAAgMABgIQAJg8BFAAIEPAAIAAAkIj3AAQgOAAgNAKQgKAMAAAPQAAAPAKAKQANAMAOAAID3AAIAABBIj3AAQgOAAgNAMQgKALAAAPQAAAOAKALQANAKAOABID3AAIAABCIj3AAQgOABgNAJQgKANAAAOQAAAQAKAKQANAMAOAAID3AAIAAAng");
	this.shape_3.setTransform(66,-13.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhEBsQgMgIgJgJQgOgOgGgPQgRgbAAghQAAgyAlgmQAlgmA0AAQAzAAAnAmQAlAmAAAyQAAAhgRAbQgGAPgOAOQgVAVgWAIQgUAFgVAAQgnAAgjgRg");
	this.shape_4.setTransform(18.4,-27.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DEB50A").s().p("ABpCKQARgcAAggQAAg0glglQgoglgygBQg0ABglAlQglAlAAA0QAAAgAQAcIgOAAQgngsAAhAQAAg7AlguIAMgOQAxgwBHAAQBGAAAxAwIAMAOQAlAuAAA7QAABAglAsg");
	this.shape_5.setTransform(18.9,-35);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#212B28").s().p("AjBC0QANgCAOgEQAVgJAWgVQAOgOAGgPIAbAAICiAAIAABBgAm0C0IAAhBIBiAAIAOAAQAHAPAOAOQAJAJAMAJQAWAOAdAEgAhMhhIgMgPQgxgvhGAAQhIAAgxAvIgMAPIhgAAQAAgvAQgiINZAAIAABRg");
	this.shape_6.setTransform(39.8,-32.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FECF09").s().p("AgLEXIkBAAIgJAAQg/AAgmgaQg6gmAAhdIAAh6IDNAAIARABIAVgBIEXAAIAACUQAABPBPABIEQAAIAAAzgAhMhAQAlgsAAhAQAAg8glgtIIBAAIAABMIkQAAQhFAAgIA8QgCAIAAAMIAAA5gAm0hAIAAjVIBgAAQglAtAAA8QAABAAnAsg");
	this.shape_7.setTransform(39.8,-14.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// ClickArea
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0)").s().p("AtTH4IAAvvIanAAIAAPvg");
	this.shape_8.setTransform(2,-0.2,1.053,1.233);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

	// Layer 5
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1E66D2").ss(6,1,1).p("AtBnSIAAOkQAABFBGAAIX3AAQBGAAAAhFIAAukQAAhEhGAAI33AAQhGAAAABEg");
	this.shape_9.setTransform(0.1,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AsSInQhIAAAAhHIAAu/QAAhHBIAAIYlAAQBIAAAABHIAAO/QAABHhIAAgAL8IWQBGAAAAhEIAAukQAAhDhGAAI33AAQhGAAAABDQAAhDBGAAIX3AAQBGAAAABDIAAOkQAABEhGAAI33AAQhGAAAAhEIAAukIAAOkQAABEBGAAIX3AAg");
	this.shape_10.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.option1copy3, new cjs.Rectangle(-87.7,-62.3,179.4,124.4), null);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ch1 = new lib.option1copy5();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(305.5,15.1,1.639,1.639,0,0,0,0.1,0.1);

	this.ch1_1 = new lib.option1copy3();
	this.ch1_1.name = "ch1_1";
	this.ch1_1.parent = this;
	this.ch1_1.setTransform(-311.6,15.1,1.639,1.639,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ch1_1},{t:this.ch1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.5,-87.3,911.1,203.9);


(lib.Tween2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.questioncopy();
	this.instance.parent = this;
	this.instance.setTransform(0.5,12.8,0.994,0.994,0,0,0,0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238.7,-142.4,478.5,310.2);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhOFQQgXgXAAggQAAghAXgWQAXgXAgAAQAgAAAWAXQAXAWAAAhQAAAggXAXQgWAWggAAQggAAgXgWgAhJCLIgJgZQgIgVAAgQQAAgeAJgbQAIgbAQgXQAQgYAvg1QAwg0AAgjQAAhDhWAAQgqABguAqIgxhcQA9gwBkABQBLAAA0AqQAzArAABGQAAAxgSAhQgTAigvAtQgwAsgOAeQgOAeAAAjQAAAIAHAhg");
	this.shape.setTransform(104.5,-62.1);

	this.instance = new lib.questioncopy();
	this.instance.parent = this;
	this.instance.setTransform(0.5,12.8,0.994,0.994,0,0,0,0.1,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238.7,-142.4,478.5,310.2);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhOFQQgXgXAAggQAAghAXgWQAXgXAgAAQAgAAAWAXQAXAWAAAhQAAAggXAXQgWAWggAAQggAAgXgWgAhJCLIgJgZQgIgVAAgQQAAgeAJgbQAIgbAQgXQAQgYAvg1QAwg0AAgjQAAhDhWAAQgqABguAqIgxhcQA9gwBkABQBLAAA0AqQAzArAABGQAAAxgSAhQgTAigvAtQgwAsgOAeQgOAeAAAjQAAAIAHAhg");
	this.shape.setTransform(104.5,-62.1);

	this.instance = new lib.questioncopy();
	this.instance.parent = this;
	this.instance.setTransform(0.5,12.8,0.994,0.994,0,0,0,0.1,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238.7,-142.4,478.5,310.2);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween19("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(180.8,41);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,363.1,83.5);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.1,scaleY:1.1},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-110.6,-70.8,221.3,141.8);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape_1.setTransform(-12,-29.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_2.setTransform(-12,-28.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_3.setTransform(22.2,15.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_4.setTransform(22.2,15.8);

	this.instance_1 = new lib.Symbol3copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance_1.alpha = 0.801;
	this.instance_1.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance_1.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(65,-70.9);

	this.instance_1 = new lib.Tween2copy2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.8,-12.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-239.5,-155,478.4,310.1);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(65,-70.9);

	this.instance_1 = new lib.Tween2copy2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.8,-12.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-239.5,-155,478.4,310.1);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(641.8,317.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(296).to({_off:false},0).wait(61).to({startPosition:7},0).to({alpha:0,startPosition:11},6).wait(1));

	// Layer_6
	this.instance_1 = new lib.Tween20("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(640.9,317);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween21("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(640.9,316.9,1.2,1.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(296).to({_off:false},0).to({_off:true,scaleX:1.2,scaleY:1.2,y:316.9},13).wait(55));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(296).to({_off:false},13).wait(48).to({startPosition:0},0).to({alpha:0},6).wait(1));

	// Layer_2
	this.instance_3 = new lib.Tween13("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(881.8,598.2);
	this.instance_3._off = true;

	this.instance_4 = new lib.Tween14("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(705.8,246.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(273).to({_off:false},0).to({_off:true,x:705.8,y:246.1},21).wait(70));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(273).to({_off:false},21).wait(63).to({startPosition:0},0).to({alpha:0},6).wait(1));

	// hand
	this.instance_5 = new lib.handanim("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(926.3,649.6,1,1,0,0,0,41,60.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(242).to({_off:false},0).to({_off:true},31).wait(91));

	// arrrow
	this.instance_6 = new lib.arrowanim("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(951.3,450.5,1,1,0,0,0,41,60.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(211).to({_off:false},0).to({_off:true},31).wait(122));

	// Layer_8
	this.instance_7 = new lib.Symbol4();
	this.instance_7.parent = this;
	this.instance_7.setTransform(705,245.9,1,1,0,0,0,63.3,69.7);
	this.instance_7.alpha = 0.898;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(144).to({_off:false},0).wait(35).to({alpha:0},6).to({_off:true},1).wait(178));

	// Layer_9
	this.instance_8 = new lib.Symbol1("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(756.9,242.3);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(86).to({_off:false},0).to({_off:true},58).wait(220));

	// Layer_1
	this.instance_9 = new lib.Symbol5("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(780.2,105.9,1,1,0,0,0,181.5,41.8);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(86).to({_off:false},0).wait(93).to({startPosition:18},0).to({alpha:0,startPosition:24},6).to({_off:true},1).wait(178));

	// Layer_7
	this.instance_10 = new lib.Tween4("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,576.5);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(69).to({_off:false},0).to({alpha:1},6).to({startPosition:0},221).to({alpha:0},5).to({_off:true},1).wait(62));

	// Layer_5
	this.instance_11 = new lib.Tween2("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,304.3);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.instance_12 = new lib.Tween2copy("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,304.3);

	this.instance_13 = new lib.Tween2copy2("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,304.3);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11}]},44).to({state:[{t:this.instance_12}]},6).to({state:[{t:this.instance_13}]},75).to({state:[{t:this.instance_13},{t:this.instance_12}]},61).to({state:[{t:this.instance_13}]},108).to({state:[{t:this.instance_13}]},63).to({state:[{t:this.instance_13}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(44).to({_off:false},0).to({_off:true,alpha:1},6).wait(314));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(125).to({_off:false},0).wait(61).to({startPosition:0},0).wait(108).to({startPosition:0},0).wait(63).to({startPosition:0},0).to({alpha:0},6).wait(1));

	// Layer_4
	this.questxt = new lib.QuestionText_mc();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(640.1,67.8,1.8,1.8,0,0,0,109.2,11.4);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(19).to({_off:false},0).to({alpha:1},5).wait(272).to({alpha:0},5).to({_off:true},1).wait(62));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(600.9,299.9,1353,826);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;