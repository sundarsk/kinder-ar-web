(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhJIJQgVgIgPgOQgOgOgJgVQgIgTAAgXQAAgWAIgUQAJgVAOgOQAPgOAVgJQATgIAXAAQAWAAATAIQAUAJAPAOQAOAOAJAVQAIAUABAWQgBAXgIATQgJAVgOAOQgPAOgUAIQgTAJgWAAQgXAAgTgJgAhWCaIgMh1IgBgGIAAgHQAAgeAQgXQAPgYAYgUQAXgVAbgTQAbgTAYgXQAYgXAQgcQAQgdAAgnQgBgagJgUQgKgUgRgPQgRgPgZgIQgYgIgaAAQgpAAgcAJQgdAJgUAMQgUAKgNAJQgOAJgKAAQgaAAgMgVIgvhKQAZgWAdgTQAdgTAigPQAhgOAogIQAogJAtAAQA+AAAzASQAzARAlAgQAlAfAUAuQAUAtAAA4QAAA2gQAoQgQAogYAeQgYAdgcAWIg1AoQgZATgTASQgRASgEAXIgRBpg");
	this.shape.setTransform(0.2,3.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.6,-87.5,63.4,175.1);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgwhHIAUAAIAAAMQAFgFAFgCIAKgEIAKgCIALABIAMAGQAFADAFAEQAEAFADAFQADAFACAHQACAGgBAGQAAAIgCAHQgCAGgEAFQgDAFgEAFIgKAHIgKAGIgLABIgJgBIgLgFQgGgCgFgFIgBA7IgQABgAAAg2QgFAAgEACQgGACgDADQgEADgDAEQgCAEgBAEIgBARQACAGACAEQADAFAEACQADADAFACQAEABAGAAQAEAAAGgCQAFgCAFgEQADgEADgFQACgHAAgGQABgGgCgGQgCgGgEgEQgEgFgGgDQgEgCgGAAIgBAAg");
	this.shape.setTransform(123.6,4.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AAYAYIgGAKIgIAIIgJAFQgEACgGAAQgIAAgFgDQgGgDgEgFQgEgEgCgGIgEgMIgBgOIgBgMIABgSIACgUIATABIgCATIgBAPIAAAIIABAJIACAKQABAFACAEQACAEAEACQADACAFgBQAGgBAHgIQAIgIAIgRIgBgWIAAgNIAAgHIATgCIABAcIAAAUIABARIAAANIABASIgUABg");
	this.shape_1.setTransform(111.8,2.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgRAwQgJgEgHgGQgGgHgEgKQgDgJAAgMQAAgLADgJQAEgKAGgGQAHgHAJgEQAIgEAJAAQAJAAAJAEQAJAEAGAHQAHAHAEAKQADAJAAAKQAAALgDAKQgEAJgHAHQgGAHgJAEQgJAEgJAAQgJAAgIgEgAgMggQgGADgDAGQgDAFgCAGIgBAMQAAAGABAHQACAGAEAFQADAFAGAEQAEADAHAAQAHAAAFgDQAGgEADgFQAEgFACgGIABgNIgBgMQgCgGgDgFQgEgGgFgDQgFgDgIAAQgGAAgGADg");
	this.shape_2.setTransform(100.9,2.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgnAVIgBgVIAAgRIgBgMIAAgSIAUgBIAAALIAAAKIAAAKIAGgIIAJgKIAJgHQAFgDAGgBQAIAAAFADIAFAEIAFAGIADAIIACAMIgRAHIgBgIIgCgFIgCgEIgDgCQgDgDgDABIgGACIgFAFIgFAFIgGAIIgFAHIgEAEIAAAOIAAALIABAKIAAAHIgTADIgBgcg");
	this.shape_3.setTransform(91.1,2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgSBJIgIgDQgFgBgEgDIgHgHQgDgEgCgGIAQgHIAFAHIAFAEIAGADIAFACQAGABAHgBQAFgBAFgCIAIgGIAFgHIADgGIACgGIAAgEIAAgMQgFAFgGADIgKAEIgKACQgKAAgKgEQgIgEgGgGQgHgHgEgIQgDgKgBgMQAAgMAFgJQADgJAIgHQAGgGAJgDQAIgEAIAAIAMADIALAEQAGAEAFAFIAAgSIARABIAABkIgBALIgFALQgCAGgEAEIgKAJIgMAGIgOADQgKAAgJgDgAgNg1QgFADgEAFQgDAEgCAGQgDAHAAAHQAAAHADAGQACAGADAEQAFAEAEACQAGADAHAAQAFAAAFgCQAGgCAEgEIAIgIQACgFABgGIAAgLQgBgHgCgFIgIgJQgFgDgFgCQgGgCgFAAQgGAAgGACg");
	this.shape_4.setTransform(79.4,4.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgTAvQgJgDgGgHQgGgGgDgJQgEgKAAgKQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgDAKAAQAJAAAIADQAIAEAGAFQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_5.setTransform(63.9,2.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AAXBDIAEgRIACgOIAAgOQAAgKgEgGQgCgGgEgEQgFgEgEgCQgFgCgFAAQgEABgFACQgEACgGAFQgEADgFAIIAAA6IgSABIgBiHIAUAAIAAA1QAFgFAFgDIAKgFIAKgCQAKAAAIADQAIAEAGAGQAGAHADAJQADAJABAMIAAAMIgBANIgBAMIgDAKg");
	this.shape_6.setTransform(52.7,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_7.setTransform(42.4,0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgoAWIgBgWIgBgQIAAgNIgBgSIAUgBIABAZIAHgJIAIgIQAEgDAEgCQAFgCAFgBQAGAAAFABQAGACADADIAGAGIADAIIADAIIAAAIIABAdIgBAgIgSgBIABgcIAAgcIAAgEIgBgFIgDgGQAAgCgDgCIgFgEQgDgBgEABQgHABgGAJQgHAJgJARIABAWIAAANIAAAHIgSACIgBgbg");
	this.shape_8.setTransform(27.8,2.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgKBCIAAgWIABgcIAAglIASAAIgBAWIAAATIAAAPIgBAMIAAATgAgEgpIgEgDIgCgEIgCgEIACgFIACgEIAEgDIAEgBIAFABIAEADIACAEIABAFIgBAEIgCAEIgEADIgFABIgEgBg");
	this.shape_9.setTransform(20.1,0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgTAvQgJgDgGgHQgGgGgDgJQgEgKAAgKQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgDAKAAQAJAAAIADQAIAEAGAFQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgHIAEgKIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLAAgJgDgAgGghQgFABgEADQgEADgDAFQgEAFgBAIIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_10.setTransform(7.8,2.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgnAVIgBgVIAAgRIgBgMIAAgSIAUgBIAAALIAAAKIAAAKIAGgIIAJgKIAJgHQAFgDAGgBQAIAAAFADIAFAEIAFAGIADAIIACAMIgRAHIgBgIIgCgFIgCgEIgDgCQgDgDgDABIgGACIgFAFIgFAFIgGAIIgFAHIgEAEIAAAOIAAALIABAKIAAAHIgTADIgBgcg");
	this.shape_11.setTransform(-2,2.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AAYAYIgGAKIgIAIIgJAFQgEACgGAAQgIAAgFgDQgGgDgEgFQgEgEgCgGIgEgMIgBgOIgBgMIABgSIACgUIATABIgCATIgBAPIAAAIIABAJIACAKQABAFACAEQACAEAEACQADACAFgBQAGgBAHgIQAIgIAIgRIgBgWIAAgNIAAgHIATgCIABAcIAAAUIABARIAAANIABASIgUABg");
	this.shape_12.setTransform(-13.1,2.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_13.setTransform(-22.9,0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgPAwQgKgEgHgHQgHgHgEgJQgFgKAAgLQAAgFACgHQACgGADgGQADgFAFgFQAFgFAGgEQAFgDAHgCQAHgCAGAAQAIAAAHACQAGACAGADQAGAEAFAFIAIALIAAAAIgPAJIAAAAIgFgIIgIgGIgIgEIgKgCQgGAAgHADQgGADgEAFQgFAEgDAHQgDAGAAAGQAAAHADAHQADAGAFAFQAEAFAGACQAHADAGAAQAFAAAEgBQAEgBAEgDQAEgCADgDQAEgDACgEIAAgBIAQAJIgBAAQgDAGgFAFIgLAIIgMAFIgOABQgJAAgJgDg");
	this.shape_14.setTransform(-32.6,2.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgKBCIAAgWIABgcIAAglIASAAIgBAWIAAATIAAAPIgBAMIAAATgAgEgpIgEgDIgCgEIgCgEIACgFIACgEIAEgDIAEgBIAFABIAEADIACAEIABAFIgBAEIgCAEIgEADIgFABIgEgBg");
	this.shape_15.setTransform(-40.2,0.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AgvhHIATAAIAAAMQAEgFAGgCIAKgEIAJgCIANABIALAGQAFADAFAEQAEAFADAFQADAFACAHQABAGAAAGQAAAIgCAHQgCAGgDAFQgDAFgFAFIgJAHIgKAGIgMABIgJgBIgLgFQgFgCgGgFIAAA7IgRABgAAAg2QgFAAgEACQgFACgEADQgEADgDAEQgCAEgBAEIgBARQABAGADAEQADAFAEACQAEADAEACQAFABAFAAQAEAAAGgCQAFgCAEgEQAFgEACgFQACgHAAgGQABgGgCgGQgCgGgEgEQgEgFgGgDQgEgCgGAAIgBAAg");
	this.shape_16.setTransform(-48.1,4.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AgSBJIgJgDQgEgBgEgDIgHgHQgDgEgCgGIARgHIAEAHIAFAEIAFADIAGACQAHABAFgBQAGgBAGgCIAHgGIAFgHIADgGIACgGIAAgEIAAgMQgFAFgGADIgLAEIgJACQgLAAgIgEQgJgEgHgGQgGgHgEgIQgDgKAAgMQAAgMADgJQAFgJAGgHQAHgGAJgDQAIgEAJAAIAKADIAMAEQAGAEAFAFIAAgSIASABIgBBkIgCALIgEALQgDAGgEAEIgJAJIgMAGIgPADQgJAAgJgDgAgMg1QgGADgDAFQgFAEgBAGQgDAHAAAHQAAAHADAGQABAGAFAEQADAEAGACQAFADAHAAQAEAAAGgCQAFgCAFgEIAHgIQADgFACgGIAAgLQgCgHgDgFIgIgJQgEgDgGgCQgFgCgFAAQgGAAgFACg");
	this.shape_17.setTransform(-65.2,4.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AgoAWIgBgWIgBgQIAAgNIgBgSIAUgBIABAZIAGgJIAIgIQAFgDAEgCQAEgCAGgBQAGAAAGABQAFACADADIAGAGIAEAIIACAIIAAAIIABAdIgBAgIgSgBIABgcIAAgcIAAgEIgBgFIgCgGIgEgEIgFgEQgDgBgEABQgHABgGAJQgIAJgHARIAAAWIABANIAAAHIgTACIgBgbg");
	this.shape_18.setTransform(-76.4,2.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0000").s().p("AgKBCIAAgWIABgcIAAglIARAAIAAAWIAAATIAAAPIgBAMIAAATgAgEgpIgEgDIgCgEIgBgEIABgFIACgEIAEgDIAEgBIAFABIAEADIACAEIABAFIgBAEIgCAEIgEADIgFABIgEgBg");
	this.shape_19.setTransform(-84.1,0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0000").s().p("AgMAxIgIgBIgIgCIgIgFIgHgEIAIgOIAKAFIALAEIALADIAKABIAIgBIAGgCIADgDIABgDIABgEIgBgDIgCgDIgFgEIgHgCIgKgBIgNgBIgOgFQgFgDgFgDQgDgEgBgHQgBgHACgEQABgGADgEIAHgHQAFgDAFgDIALgDIAKgBIAJABIAJABIALAEIAJAEIgGARIgLgFIgJgDIgJgBQgNgBgHADQgIAFAAAHQAAAFACACQAEADAFABIALACIAOABQAIACAGACQAFABAEAEQAEADABAFQACAEAAAFQAAAIgDAGQgEAGgFADQgGADgIACIgOACQgIAAgIgCg");
	this.shape_20.setTransform(-91.5,2.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF0000").s().p("AgMAxIgHgBIgJgCIgIgFIgHgEIAIgOIAKAFIAKAEIAMADIAKABIAIgBIAFgCIAEgDIABgDIABgEIgBgDIgCgDIgFgEIgHgCIgKgBIgNgBIgOgFQgGgDgEgDQgDgEgBgHQgBgHACgEQABgGADgEIAHgHQAFgDAFgDIALgDIAKgBIAIABIAKABIALAEIAJAEIgGARIgLgFIgJgDIgJgBQgNgBgIADQgHAFAAAHQAAAFADACQACADAGABIALACIAOABQAIACAGACQAFABAEAEQAEADACAFQABAEAAAFQAAAIgDAGQgEAGgFADQgGADgIACIgOACQgHAAgJgCg");
	this.shape_21.setTransform(-101.5,2.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF0000").s().p("AgKBCIAAgWIABgcIAAglIASAAIgBAWIAAATIAAAPIgBAMIAAATgAgEgpIgEgDIgCgEIgCgEIACgFIACgEIAEgDIAEgBIAFABIAEADIACAEIABAFIgBAEIgCAEIgEADIgFABIgEgBg");
	this.shape_22.setTransform(-108.6,0.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF0000").s().p("AAqAVIAAgTIAAgNQgBgIgCgDQgCgEgDAAIgFACIgEADIgEAFIgFAFIgDAGIgDAEIAAAIIAAALIABANIAAAQIgQABIgBgbIAAgTIgBgNQgBgIgCgDQgCgEgDAAIgFACIgEAEIgFAFIgFAGIgEAGIgDAEIABAuIgRABIgDhdIATgCIAAAZIAGgHIAHgHIAIgFQAEgCAFAAIAHABIAFAEQADACACAEIACAJIAGgHIAHgGQADgDAFgCQAEgCAFAAIAHABQAEACADADQADACABAFQACAEAAAGIABAQIABAUIABAfIgTABIAAgbg");
	this.shape_23.setTransform(-118.3,1.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AywCFQguAAghghQggggAAgvIAAgpQAAguAgghQAhghAuAAMAlgAAAQAvAAAgAhQAhAhAAAuIAAApQAAAvghAgQggAhgvAAg");
	this.shape_24.setTransform(1.2,1.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-132.4,-15,264.9,30.1);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FBB71F").s().p("AgehZQBwhxCLgzQgtATAcDDQgSAJgPAMQgsAegRAwQj4gShSDUQAqjCCUiVg");
	this.shape.setTransform(-47.1,-41.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6600").s().p("AgQBJQgLgBgLgEQgPgGgLgOQgQgXAGgdQABgGACgEQAJgZAVgQQAIgGAKgEQAUgJATABQAcABATATIACAEQAQAXgFAdIAAABQgHAegbAUQgOAMgQAEQgLAEgLAAIgGgBg");
	this.shape_1.setTransform(-22.1,-32.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC0000").s().p("AgYB0QgVgCgPgHQgXgJgRgWQgbgkAKguIAFgPQAPgnAhgaQANgKAPgGQAdgOAgABQAtAEAbAeQADACABAEQAbAigKAuIAAACQgLAvgoAgQgYASgZAGQgSAGgSAAIgGAAgAgXhBQgKAEgJAGQgVAQgIAZQgDAEgBAGQgGAdARAXQALAOAOAGQALAEAMABQAPABANgEQAPgEAPgMQAagUAHgeIAAgBQAGgdgRgXIgCgEQgSgTgdgBIgEAAQgRAAgRAIg");
	this.shape_2.setTransform(-22,-32.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AgfCQQgZgBgTgJQgegLgVgcQghgsANg6IAFgUQASgwArgfQAPgMASgKQAlgRAqABQA2AEAjAlIAFAIQAiAsgNA6IAAACQgMA8g0AmQgeAYgfAHQgWAHgYAAIgHgBgAglhmQgOAGgOAKQghAagPAnIgEAPQgKAuAaAkQARAWAXAJQAQAHAUACQAXABATgHQAZgGAYgSQApggALgvIAAgCQAJgugagiQgBgEgEgCQgagegugEIgEAAQgdAAgcANg");
	this.shape_3.setTransform(-22,-32.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF6699").s().p("AjOhJQAAhSARhNQBTjUD3ASIgFAVQgNA6AhAtQAVAbAeALQkFGVA3EfQjPjQAAklg");
	this.shape_4.setTransform(-50.3,7.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#99CC00").s().p("Am8lrQAggHAdgYQA0gmANg8IAAgBQLpChASFCQgrC+iTCRQh2B4iSAyQjth8jGreg");
	this.shape_5.setTransform(24.7,17.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00CCFF").s().p("AlsDyQg3keEFmWQATAKAaABQAcABAZgHQDFLeDtB7QhuAmh/AAQkmAAjPjQg");
	this.shape_6.setTransform(-13.7,25.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F37942").s().p("AkMBkQgrgCglASQgcjCAtgTQAAAAABAAQAAAAAAAAQABgBAAAAQAAAAAAAAQBzgpCGAAQD8AAC8CXQjyh5kpD5Qgjglg2gDg");
	this.shape_7.setTransform(8.1,-56.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6666FF").s().p("Al5hlQANg8gigrIgFgJQEqj6DwB6QAgAZAeAdQDPDRAAElQAABXgSBQQgRlDrqigg");
	this.shape_8.setTransform(30.6,-21.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-70.9,-70.9,141.9,141.9);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6600").s().p("AgUBbQgOgBgOgGQgSgIgNgQQgVgcAIgkQABgIAEgGQAKgeAagUQALgHALgGQAYgKAZABQAjACAWAXQABADACACQAVAcgIAkIAAACQgJAkggAZQgSAPgTAFQgOAEgPAAIgGAAg");
	this.shape.setTransform(-27.2,-40.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FBB71F").s().p("AglhuQCKiLCsg/Qg3AXAiDwQgWAMgTAPQg2AlgWA7QkwgWhnEFQA0jvC3i4g");
	this.shape_1.setTransform(-58.2,-51);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC0000").s().p("AgeCPQgagCgTgJQgcgMgVgaQghgtAMg5IAGgSQASgwApggQARgMASgIQAkgRAnABQA5AEAgAlQAFADABAFQAgArgLA4IAAADQgOA6gyAnQgeAXgeAIQgWAGgYAAIgGAAgAgdhRQgMAFgLAIQgaAUgKAeQgEAGgBAIQgHAkAUAbQAOARASAIQANAFAPACQATABAPgGQAUgFARgOQAhgZAIglIAAgBQAIglgVgbQgCgCgBgDQgWgYgjgBIgGAAQgVAAgVAJg");
	this.shape_2.setTransform(-27.2,-40.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AgmCyQgggCgXgLQglgOgZgiQgqg2AQhIIAGgZQAWg7A2gmQATgPAWgMQAugVAzABQBEAFArAtIAGALQApA1gPBJIAAACQgQBKhAAvQgkAdgnAKQgbAIgfAAIgHgBgAguh+QgSAHgQANQgpAggSAwIgGASQgMA5AgAtQAWAaAbAMQAUAJAZACQAcABAYgIQAfgHAegXQAygnANg6IAAgDQAMg5ghgqQgBgFgEgDQghglg4gEIgGAAQgkAAgiAQg");
	this.shape_3.setTransform(-27.1,-40.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF6699").s().p("Aj+haQAAhlAUhfQBnkGExAWIgGAaQgRBIAqA3QAZAiAlANQlBH1BDFiQj/kBAAlqg");
	this.shape_4.setTransform(-62.1,9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#99CC00").s().p("AoknAQAngKAkgdQBAguAQhLIAAgCQOYDHAWGPQg1Dpi1C0QiSCTi0A+QkliYj0uKg");
	this.shape_5.setTransform(30.5,21.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00CCFF").s().p("AnCErQhElhFDn2QAXAMAgACQAjABAfgIQDzOKElCYQiIAwidAAQlrAAkAkCg");
	this.shape_6.setTransform(-16.9,32);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F37942").s().p("AlMB7Qg0gBguAUQgijvA3gXQAAAAABAAQAAAAABAAQAAAAAAgBQABAAAAAAQCOgzCkAAQE3AADpC7QkriWluE0QgrgthEgFg");
	this.shape_7.setTransform(10.1,-70.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6666FF").s().p("AnRh9QAOhKgpg2IgGgKQFvk1EqCXQAnAfAlAkQEAEBAAFqQAABsgWBiQgWmPuYjFg");
	this.shape_8.setTransform(37.7,-26.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-87.6,-87.6,175.2,175.3);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween13_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AhJIJQgVgIgPgOQgOgOgJgVQgIgTAAgXQAAgWAIgUQAJgVAOgOQAPgOAVgJQATgIAXAAQAWAAATAIQAUAJAPAOQAOAOAJAVQAIAUABAWQgBAXgIATQgJAVgOAOQgPAOgUAIQgTAJgWAAQgXAAgTgJgAhWCaIgMh1IgBgGIAAgHQAAgeAQgXQAPgYAYgUQAXgVAbgTQAbgTAYgXQAYgXAQgcQAQgdAAgnQgBgagJgUQgKgUgRgPQgRgPgZgIQgYgIgaAAQgpAAgcAJQgdAJgUAMQgUAKgNAJQgOAJgKAAQgaAAgMgVIgvhKQAZgWAdgTQAdgTAigPQAhgOAogIQAogJAtAAQA+AAAzASQAzARAlAgQAlAfAUAuQAUAtAAA4QAAA2gQAoQgQAogYAeQgYAdgcAWIg1AoQgZATgTASQgRASgEAXIgRBpg");
	this.shape_1.setTransform(0.2,3.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.6,-87.5,63.4,175.1);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("At3JNQiGq5J8vbQAuAWA+AEQBGACA8gRQHhb7JBEsQkMBdk1AAQrNAAn4n7g");
	this.shape.setTransform(461.7,339);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("Aw5t0QBMgSBIg7QB+hcAgiSIAAgDQcWGIArMRQhpHNlkFhQkhEkliB6QpCksnh77g");
	this.shape_1.setTransform(555.1,319);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FBB71F").s().p("AhJjaQERkSFSh8QhsAtBDHZQgsAYgmAdQhpBKgrB1QpagrjKICQBnnXFplsg");
	this.shape_2.setTransform(380.3,175.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF6600").s().p("AgoCzQgdgDgagLQgjgPgbggQgog4APhIQACgOAHgMQAUg8A0gnQAUgOAXgLQAwgVAwADQBGADAsAuQACAGADADQApA3gPBIIAAAEQgQBIhBAxQgiAdgnAJQgbAKgcAAIgOgBg");
	this.shape_3.setTransform(441.4,196.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF6699").s().p("An2izQAAjHApi7QDJoDJaArIgLAyQghCPBSBsQAyBCBJAbQp7PbCFK5Qn3n5AArLg");
	this.shape_4.setTransform(372.8,293.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC0000").s().p("Ag9EaQgxgEgngSQg3gWgpg1QhBhYAYhwIALglQAkheBRhAQAhgYAjgOQBIgjBOACQBvAJBABJQAJAGACAJQBABUgWBxIAAAGQgbBxhjBNQg7Aug8AOQgrANgtAAIgQAAgAg7igQgXALgVAOQgzAngVA8QgGAMgDAOQgOBIAoA4QAaAgAkAPQAaALAdADQAlADAggMQAmgJAjgdQBAgxARhIIAAgEQAPhIgpg3QgDgDgCgGQgsguhGgDIgLgBQgrAAgqATg");
	this.shape_5.setTransform(441.5,196.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF3333").s().p("AhNFgQg9gEgvgWQhJgbgxhDQhShsAgiOIAMgxQArh1BphLQAmgeAsgXQBagpBmACQCFAJBVBaQAJANADAIQBRBpgdCRIAAADQggCSh+BcQhIA7hMASQg1APg+AAIgPAAgAhbj4QgjAOghAYQhRBAgkBeIgLAlQgYBwBBBYQApA1A3AWQAnASAxAEQA3ACAxgPQA8gOA7guQBjhNAbhxIAAgGQAWhxhAhUQgCgJgJgGQhAhJhvgJIgLAAQhIAAhDAhg");
	this.shape_6.setTransform(441.6,196.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F37942").s().p("AqPDyQhngChaAoQhDnXBsguQADAAACgEQEYhjFEAAQJnAAHKFxQpOkorSJgQhVhaiFgJg");
	this.shape_7.setTransform(514.9,137.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6666FF").s().p("AuWj4QAeiRhRhqQgEgIgJgMQLUphJMEpQBMA9BJBHQH4H7AALKQAADUgrDDQgssS8WmHg");
	this.shape_8.setTransform(569.4,223.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape_9.setTransform(495.1,276);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CCCCCC").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_10.setTransform(495.1,276.1,0.883,0.883);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF3333").s().p("A64a4QrJrIAAvwQAAvvLJrJQLJrIPvAAQPwAALJLIQLJLJgBPvQABPwrJLIQrJLJvwAAQvvAArJrJg");
	this.shape_11.setTransform(495.2,276.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9}]}).wait(1));

	// Layer_1
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_12.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy7, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AYEgNQAAAHAAAGQAAIGkpGMIjri5IADmbIAAgEIDvlRgAHo24IhtETIDuE/IACADIGGBwIDxLcAnW2/QDehFD6AAQEBAADlBMQFMBrENENQBNBNBABTIjdCtAzhuGQBHhgBahaQAognApgkQCSiCClhUQBtg4B1gmAwBrmIGZhzIAHgBIDplLIhekaA4DAIQAAgEAAgEQAAn/EimHA4DAIIEIgPID6rfIjgigAzeOKIDWitAngW8QlRhskPkPQhXhZhHheQkjmFgCn9Av4E5IgQGkIJuG8IGSh8IAEABIF+CWIBaENQjcBEj2AAQkAAAjihJAv4E5IkDlAApqC+ImOB7AgEQaIAImXATbOSQhGBbhUBUQkUEVlZBrAJcCzIGUCKApktUIDnFTAJntjIj8FiAmaSZIhGEjAPwLZIp2HbATOugQEzGJADIK");
	this.shape.setTransform(495.2,276);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("AF6TWIJ2nbIDrC6QhGBahUBUQkUEVlZBrgAw/RjQhYhZhHheIDWisIJuG7IhGEjQlRhskOkPgAppDgIDsq/ILoAAIDxK0IpYHRgA4DAhQAAn+EimHIDgChIj6LdIkIAQIAAgJgATiALIjwrcIDcitQEzGJADIJgAl3yDIhfkZQDehGD6AAQEBAADlBMIhtETg");
	this.shape_1.setTransform(495.2,272.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AngWZIBGkiIGSh9IAEABIF+CWIl+iWIAAgDIgEACImSB9Ipum8IARmlIkElAIEEFAIgRGlIjWCsQkimFgDn8IEIgQID6reIjgihQBIhgBahaQAognAogkQCTiBCkhVQBug4B0glIBfEZIjqFMIgHABImZBzIGZhzIAFAEIACgFIDqlMILyAAIDuE/Ijuk/IBtkTQFMBrENENQBNBOBABSIjcCtIDwLdIjvFQIgDADIADACIAAgFIDvlQIEiAJIAAANQAAIGkpGNIjri6IADmaIgDGaIp2HbIBaENQjcBEj2AAQkAABjihKgAgEP4IAImXIJYnRIjxq0ID8liIj8FiIroAAIjmlTIDmFTIjsK/IJtHGgAPwEaImUiKgAv3EWIGOh7gAJnuGIAEAAIGHBwImHhwIgCgDgAl9okg");
	this.shape_2.setTransform(495.2,279.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_3.setTransform(495.1,275.9,0.848,0.848);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_4.setTransform(495.1,276.1,0.883,0.883);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF3333").s().p("A64a4QrJrIAAvwQAAvvLJrJQLJrIPvAAQPwAALJLIQLJLJgBPvQABPwrJLIQrJLJvwAAQvvAArJrJg");
	this.shape_5.setTransform(495.2,276.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer_1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_6.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape.setTransform(495.1,276);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_1.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy5, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6699").s().p("AndiqQAAi9AniyQC/npI8ApIgLAvQgfCIBOBnQAvA/BGAaQpbOpB+KWQnenfAAqog");
	this.shape.setTransform(378.9,292.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("AtKIvQiAqVJcuqQAsAVA7ADQBCACA6gQQHIaiIlEdQj/BYkmAAQqoAAnfnig");
	this.shape_1.setTransform(463.3,335.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#99CC00").s().p("AwDtIQBIgRBFg3QB4hYAdiLIAAgDQa8F0ApLrQhkG1lSFPQkSEWlRB0QolkdnJ6ig");
	this.shape_2.setTransform(552.1,316.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FBB71F").s().p("AhGjPQEEkEFBh3QhmAsA/HBQgqAWgjAcQhkBHgqBvQo7gqjAHqQBinAFWlag");
	this.shape_3.setTransform(386.1,180.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3333").s().p("AhJFOQg6gDgsgVQhGgagvg/QhOhnAfiHIALgvQAqhvBjhHQAkgcAqgXQBWgmBgACQB/AIBQBWQAJAMADAHQBNBlgcCJIAAADQgeCLh4BXQhEA4hJARQgyAOg6AAIgPAAgAhWjsQgiAOgfAXQhNA9giBYIgKAkQgXBqA9BUQAoAyA0AVQAlARAvAEQA0ACAugOQA5gOA4gsQBehIAahsIAAgGQAVhrg9hQQgCgIgIgGQg+hFhpgJIgJAAQhFAAhAAfg");
	this.shape_4.setTransform(444.3,200.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF6600").s().p("AglCqQgcgCgZgLQgigOgZgfQgmg1AOhEQACgOAGgLQAUg5AxglQATgOAWgKQAugUAuADQBCADApAsQACAFAEADQAmA1gOBEIAAADQgQBFg9AvQghAbgkAJQgZAJgbAAIgNgBg");
	this.shape_5.setTransform(444.1,200.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC0000").s().p("Ag6EMQgvgEglgRQg0gVgngyQg9hUAWhqIALgkQAihYBNg9QAfgXAhgOQBEghBKACQBqAJA9BFQAJAGACAIQA8BQgVBrIAAAGQgaBsheBIQg3Asg6AOQgoAMgqAAIgQAAgAg4iYQgWAKgUAOQgxAlgTA5QgGALgDAOQgOBEAnA1QAZAfAiAOQAZALAbACQAjADAfgLQAkgJAhgbQA9gvAQhFIAAgDQAOhEgng1QgDgDgCgFQgqgshCgDIgKgBQgpAAgoASg");
	this.shape_6.setTransform(444.2,200.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F37942").s().p("ApuDlQhigBhVAmQhAm/BmgsQAEAAABgEQEKheE0AAQJIABGzFeQowkZquJBQhRhWh+gJg");
	this.shape_7.setTransform(513.9,144.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6666FF").s().p("AtojrQAdiKhOhlQgDgHgIgMQKvpCIvEaQBHA6BGBDQHeHiAAKmQAADKgoC5Qgprr68lzg");
	this.shape_8.setTransform(565.7,226.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("A4AYAQp8p8AAuEQAAuDJ8p9QJ9p9ODAAQOEAAJ8J9QJ9J9AAODQAAOEp9J8Qp8J+uEgBQuDABp9p+g");
	this.shape_9.setTransform(495.1,276);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

	// Layer_1
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_10.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgwBMIgCiWIAVgBIgBANQAGgEAFgEIAKgDQAHgCADgBQAHAAAGACIAMAFIAKAIQAFAFADAFQAEAGACAHQABAGAAAHIgCAQIgGALQgEAGgEAEQgFAGgFACQgFAEgGACQgGABgFAAIgLgBQgFgBgGgEQgFgCgGgFIgBA+gAgKg3QgFACgEADQgEAEgDAEIgEAJIAAARQABAGADAFQADAFAEACQAEADAFACQAEACAFAAQAGgBAFgCQAGgCAEgEQAEgEADgGQADgGAAgHQAAgHgCgGQgCgGgEgFQgEgEgFgDQgGgDgHAAQgFAAgFACg");
	this.shape.setTransform(182.9,-5.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAzQgJAAgFgDQgHgDgDgEQgEgFgDgGIgEgNIgCgPIAAgNIABgTIADgUIATAAIgDAVIAAAQIAAAIIAAAJIADALIADAJQADAEADACQADADAGgBQAGgBAIgJQAHgJAJgRIgBgXIAAgOIAAgHIATgCIACAcIAAAWIABASIABANIAAAUIgVAAIgBgaIgHAKQgDAFgFAEQgEADgGACQgDACgEAAIgCAAg");
	this.shape_1.setTransform(170.5,-7.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSAyQgJgEgHgHQgGgHgEgKQgFgKAAgMQAAgLAFgKQAEgKAGgHQAHgIAJgDQAJgEAJAAQAKAAAKAEQAIAEAHAHQAHAHAEAKQADAKAAALQAAAMgDAJQgEAKgHAIQgHAHgIAEQgKAEgKAAQgJAAgJgEgAgNgiQgFAEgEAFQgDAGgCAGIgCANIACANQACAHADAFQAEAGAGADQAGAEAGAAQAIAAAFgEQAGgDAEgGQAEgFACgHIABgNIgBgNQgCgGgEgGQgEgFgFgEQgGgDgIAAQgHAAgGADg");
	this.shape_2.setTransform(159,-7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgpAWIgBgWIAAgRIgBgOIAAgUIAVAAIAAAMIAAALIAAALIAHgJIAJgKQAEgEAFgEQAGgEAGgBQAIAAAGAEIAFAEIAFAGIADAKIACALIgSAHIgBgIIgBgFIgDgEIgCgCQgEgDgEAAIgFADIgGAEIgFAGIgGAIIgGAHIgEAGIAAAOIAAAMIABAKIAAAIIgUADIgBgeg");
	this.shape_3.setTransform(148.7,-7.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgTBNIgIgDIgJgFQgFgDgDgEIgGgKIASgIIAFAHIAFAFIAGADIAGACQAGABAHgBIAMgEIAIgGIAEgHIAEgHIACgGIAAgEIAAgMQgFAFgHADIgLAEIgKACQgLAAgJgEQgJgEgHgHQgHgHgEgIQgEgKAAgNQAAgMAFgKQAEgKAHgHQAHgHAJgDQAJgEAJAAIALADQAGACAGADQAGADAGAGIAAgTIASABIAABpIgCAMIgFAMIgHAKIgKAJQgFAEgHACQgHADgIAAIgCABQgKAAgIgDgAgNg3QgFACgFAFQgEAFgCAGQgCAHAAAIQAAAHACAGQACAHAEADQAFAFAGADQAFACAHAAQAFAAAFgCQAHgCAEgEQAFgDADgFQADgFACgHIAAgMQgBgGgEgGQgDgFgFgEIgLgGQgGgCgEAAQgIAAgFADg");
	this.shape_4.setTransform(136.4,-5.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgUAyQgKgEgGgHQgGgHgEgJQgDgKAAgLQAAgLAEgKQAEgKAHgHQAHgIAIgEQAKgEAKAAQAJAAAJAEQAIADAHAHQAGAHAEAJQAFAIABALIhPALQAAAGAEAGQACAFAEADQAEAEAFACQAFACAGAAIAKgCQAEgBAEgDQAEgDADgEQAEgFABgFIASADQgDAJgFAGQgFAHgGAFQgGAFgIADQgHACgIAAQgMAAgJgDgAgHgjQgFABgEAEQgEADgDAFQgEAGgCAIIA3gHIgBgCQgDgJgGgFQgGgFgJAAIgIABg");
	this.shape_5.setTransform(120.2,-7.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAYBGQADgJACgIIABgPIABgPQgBgKgDgHQgDgGgEgEQgEgEgFgCQgGgCgFAAQgEAAgGADIgKAHQgEADgFAJIgBA9IgRAAIgCiNIAVgBIgBA5QAFgGAGgDIALgFIAKgDQALAAAIAEQAJAEAFAHQAGAHAFAJQADAKABAMIgBANIAAAOIgCAMIgCALg");
	this.shape_6.setTransform(108.4,-10);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgKgKIggABIAAgRIAggBIABgoIARgCIAAAqIAjgCIgBASIgjABIAABPIgTABg");
	this.shape_7.setTransform(97.6,-9.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgqAXIgBgXIgBgRIAAgNIgBgUIAVAAIABAaIAHgKIAIgIQAFgEAEgCQAFgCAGAAQAHgBAFACQAFABAEADIAGAHIAEAJIACAIIAAAIIABAfIgBAhIgTAAIABgeIAAgdIAAgEIgBgGIgCgGIgEgFQgCgCgDgBQgDgBgFAAQgHABgGAKQgIAKgJARIABAYIAAANIABAHIgUADIgBgdg");
	this.shape_8.setTransform(82.2,-7.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgLBGIABgYIAAgdIABgnIASgBIgBAYIAAAUIAAAQIgBAMIAAAVgAgEgrIgEgDIgDgEIgBgFIABgFIADgEIAEgDIAEgCQADABACABIAEADIADAEIABAFIgBAFIgDAEIgEADIgFABIgEgBg");
	this.shape_9.setTransform(74.2,-10);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUAyQgKgEgGgHQgGgHgEgJQgDgKAAgLQAAgLAEgKQAEgKAHgHQAHgIAIgEQAKgEAKAAQAJAAAJAEQAIADAHAHQAGAHAFAJQAEAIABALIhPALQAAAGAEAGQACAFAEADQAEAEAFACQAFACAGAAIAKgCQAEgBAEgDQAFgDADgEQADgFAAgFIATADQgDAJgFAGQgFAHgGAFQgGAFgIADQgHACgIAAQgMAAgJgDgAgHgjQgEABgFAEQgEADgDAFQgEAGgCAIIA3gHIgBgCQgDgJgGgFQgGgFgJAAIgIABg");
	this.shape_10.setTransform(61.3,-7.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgpAWIgBgWIAAgRIgBgOIAAgUIAVAAIAAAMIAAALIAAALIAHgJIAJgKQAEgEAFgEQAGgEAGgBQAIAAAGAEIAFAEIAFAGIADAKIACALIgSAHIgBgIIgBgFIgDgEIgCgCQgEgDgEAAIgFADIgGAEIgFAGIgGAIIgGAHIgEAGIAAAOIAAAMIABAKIAAAIIgUADIgBgeg");
	this.shape_11.setTransform(51,-7.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgJAzQgJAAgGgDQgFgDgEgEQgEgFgDgGIgDgNIgCgPIgBgNIABgTIADgUIASAAIgBAVIgBAQIAAAIIABAJIABALIAEAJQACAEAEACQADADAGgBQAGgBAIgJQAHgJAJgRIgBgXIAAgOIAAgHIAUgCIABAcIABAWIAAASIABANIAAAUIgVAAIAAgaIgHAKQgEAFgFAEQgFADgEACQgEACgEAAIgCAAg");
	this.shape_12.setTransform(39.4,-7.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgKgKIggABIAAgRIAggBIABgoIARgCIAAAqIAjgCIgBASIgjABIAABPIgTABg");
	this.shape_13.setTransform(29,-9.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgQAyQgKgEgIgHQgHgIgFgJQgEgKAAgMQAAgGACgGQABgHAEgGQADgGAFgFQAFgFAGgEQAGgEAHgCQAHgCAHAAQAIAAAHACQAHACAHAEIALAJQAFAFADAHIAAAAIgPAJIAAAAIgGgJIgIgGQgEgDgFgBIgKgBQgHAAgGACQgHADgFAFQgFAFgDAHQgDAHAAAGQAAAIADAGQADAHAFAFQAFAFAHADQAGADAHAAQAFAAAEgCIAJgDIAIgGIAFgHIABgBIAQAJIAAABQgEAGgFAEQgFAFgGAEQgHADgHACQgHACgHAAQgKAAgJgEg");
	this.shape_14.setTransform(18.9,-7.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgLBGIABgYIAAgdIABgnIASgBIgBAYIAAAUIAAAQIgBAMIAAAVgAgEgrIgEgDIgDgEIgBgFIABgFIADgEIAEgDIAEgCQADABACABIAEADIADAEIABAFIgBAFIgDAEIgEADIgFABIgEgBg");
	this.shape_15.setTransform(10.9,-10);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgxBMIgBiWIAUgBIAAANQAGgEAFgEIALgDQAGgCAEgBQAGAAAGACIAMAFIAKAIQAFAFADAFQADAGACAHQACAGAAAHIgDAQIgFALQgDAGgFAEQgFAGgFACQgGAEgFACQgGABgFAAIgKgBQgFgBgGgEQgGgCgHgFIAAA+gAgKg3QgFACgEADQgEAEgDAEIgEAJIgBARQACAGADAFQADAFAEACQAEADAFACQAFACAEAAQAGgBAFgCQAGgCAEgEQAEgEADgGQADgGAAgHQABgHgCgGQgDgGgEgFQgEgEgGgDQgGgDgGAAQgFAAgFACg");
	this.shape_16.setTransform(2.6,-5.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgTBNIgJgDIgIgFQgFgDgDgEIgFgKIASgIIADAHIAGAFIAGADIAGACQAHABAGgBIALgEIAJgGIAEgHIAEgHIABgGIABgEIAAgMQgGAFgFADIgLAEIgLACQgLAAgJgEQgKgEgGgHQgHgHgEgIQgEgKAAgNQAAgMAEgKQAFgKAHgHQAHgHAJgDQAJgEAJAAIAMADQAFACAGADQAGADAGAGIAAgTIASABIgBBpQABAGgCAGIgFAMIgGAKIgKAJQgGAEgHACQgHADgIAAIgCABQgKAAgIgDgAgNg3QgFACgEAFQgFAFgCAGQgCAHAAAIQAAAHACAGQACAHAFADQADAFAHADQAFACAHAAQAFAAAGgCQAGgCAEgEQAFgDADgFQADgFABgHIAAgMQgBgGgDgGQgDgFgFgEIgKgGQgGgCgGAAQgGAAgGADg");
	this.shape_17.setTransform(-15.4,-5.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgqAXIgBgXIgBgRIAAgNIgBgUIAVAAIABAaIAHgKIAIgIQAFgEAEgCQAFgCAGAAQAHgBAFACQAFABAEADIAGAHIAEAJIACAIIAAAIIABAfIgBAhIgTAAIABgeIAAgdIAAgEIgBgGIgCgGIgEgFQgCgCgDgBQgDgBgFAAQgHABgGAKQgIAKgJARIABAYIAAANIABAHIgUADIgBgdg");
	this.shape_18.setTransform(-27.2,-7.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgLBGIABgYIAAgdIABgnIASgBIgBAYIAAAUIAAAQIgBAMIAAAVgAgEgrIgEgDIgDgEIgBgFIABgFIADgEIAEgDIAEgCQADABACABIAEADIADAEIABAFIgBAFIgDAEIgEADIgFABIgEgBg");
	this.shape_19.setTransform(-35.2,-10);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgMA0IgIgCIgJgCIgIgFIgJgFIAJgOIALAGIALAEIALADIALABIAJgBIAGgDIADgDIACgDIAAgEIgBgDIgCgDIgFgEIgIgCIgKgBIgOgCQgIgCgGgDQgGgDgEgDQgEgFgBgHQAAgHABgEQACgHACgDQADgFAFgDQAFgEAFgCQAGgCAFgBIAMgBIAIABIAKABIALADQAGACAEAEIgHARQgFgEgGgCIgJgCIgKgCQgNgBgJAEQgHAEgBAIQABAFADADQADACAFACIAMABIAOACQAJABAGADQAGACAEADQAEAEABAEQACAFAAAFQABAIgEAHQgEAFgFAEQgHAEgIACIgPACQgIAAgIgCg");
	this.shape_20.setTransform(-43,-7.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgNA0IgIgCIgIgCIgJgFIgIgFIAJgOIALAGIALAEIALADIALABIAIgBIAHgDIADgDIACgDIAAgEIgBgDIgDgDIgEgEIgHgCIgKgBIgPgCQgIgCgGgDQgHgDgDgDQgEgFgBgHQgBgHACgEQABgHADgDQAEgFAEgDQAEgEAGgCQAFgCAHgBIALgBIAIABIAKABIALADQAFACAFAEIgGARQgHgEgFgCIgKgCIgJgCQgOgBgHAEQgJAEABAIQAAAFACADQADACAGACIAMABIAOACQAJABAGADQAHACADADQAEAEABAEQADAFAAAFQAAAIgEAHQgEAFgGAEQgGAEgHACIgQACQgIAAgJgCg");
	this.shape_21.setTransform(-53.5,-7.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgLBGIABgYIAAgdIABgnIASgBIgBAYIAAAUIAAAQIgBAMIAAAVgAgEgrIgEgDIgDgEIgBgFIABgFIADgEIAEgDIAEgCQADABACABIAEADIADAEIABAFIgBAFIgDAEIgEADIgFABIgEgBg");
	this.shape_22.setTransform(-60.9,-10);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAsAWIAAgUIAAgOQAAgHgDgEQgCgEgDAAQgBAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABIgFADIgEAFIgFAGIgDAGIgEAFIABAIIAAALIAAAOIAAARIgRABIAAgdIgBgUIgBgOQAAgHgCgEQgCgEgEAAIgFACIgFAEIgFAGIgEAGIgEAGIgEAEIABAxIgSABIgDhiIAUgDIAAAbIAGgHIAHgHQAEgEAFgCQAEgCAFAAIAIABIAGAEIAEAHQADAEAAAFIAGgHIAHgHIAIgFQAEgCAGAAIAIACQAEABADADQADADABAEQACAFABAGIABARIAAAVIABAhIgUABIAAgdg");
	this.shape_23.setTransform(-71.1,-8.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAyQgKgEgGgHQgGgHgDgJQgEgKAAgLQAAgLAEgKQAEgKAGgHQAIgIAJgEQAJgEAJAAQALAAAIAEQAIADAHAHQAHAHAEAJQAEAIABALIhPALQABAGACAGQADAFAEADQAEAEAFACQAGACAEAAIAKgCQAFgBAEgDQAFgDADgEQACgFABgFIASADQgCAJgFAGQgFAHgGAFQgGAFgHADQgIACgJAAQgKAAgKgDgAgHgjQgFABgEAEQgEADgEAFQgDAGgBAIIA2gHIAAgCQgEgJgGgFQgGgFgKAAIgHABg");
	this.shape_24.setTransform(-89.1,-7.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAZBGQADgJAAgIIACgPIAAgPQAAgKgDgHQgDgGgEgEQgFgEgFgCQgFgCgFAAQgEAAgFADIgKAHQgGADgEAJIgBA9IgSAAIgCiNIAWgBIAAA5QAFgGAFgDIALgFIAKgDQAKAAAJAEQAIAEAHAHQAFAHAEAJQAEAKAAAMIAAANIgBAOIgBAMIgCALg");
	this.shape_25.setTransform(-100.9,-10);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgKgKIggABIAAgRIAggBIABgoIARgCIAAAqIAjgCIgBASIgjABIAABPIgTABg");
	this.shape_26.setTransform(-111.8,-9.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgKgKIggABIAAgRIAggBIABgoIARgCIAAAqIAjgCIgBASIgjABIAABPIgTABg");
	this.shape_27.setTransform(-125.7,-9.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgQAyQgKgEgIgHQgHgIgFgJQgEgKAAgMQAAgGACgGQABgHAEgGQADgGAFgFQAFgFAGgEQAGgEAHgCQAHgCAHAAQAIAAAHACQAHACAHAEIALAJQAFAFADAHIAAAAIgPAJIAAAAIgGgJIgIgGQgEgDgFgBIgKgBQgHAAgGACQgHADgFAFQgFAFgDAHQgDAHAAAGQAAAIADAGQADAHAFAFQAFAFAHADQAGADAHAAQAFAAAEgCIAJgDIAIgGIAFgHIABgBIAQAJIAAABQgEAGgFAEQgFAFgGAEQgHADgHACQgHACgHAAQgKAAgJgEg");
	this.shape_28.setTransform(-135.8,-7.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgUAyQgKgEgGgHQgGgHgEgJQgDgKAAgLQAAgLAEgKQAEgKAHgHQAHgIAIgEQAKgEAKAAQAJAAAJAEQAIADAHAHQAGAHAFAJQAEAIABALIhPALQAAAGAEAGQACAFAEADQAEAEAFACQAFACAGAAIAKgCQAEgBAEgDQAFgDADgEQADgFAAgFIASADQgCAJgFAGQgFAHgGAFQgGAFgIADQgHACgIAAQgMAAgJgDgAgHgjQgEABgFAEQgEADgDAFQgEAGgCAIIA3gHIgBgCQgDgJgGgFQgGgFgJAAIgIABg");
	this.shape_29.setTransform(-147,-7.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAGBRIgIgGQgEgEgDgEQgDgEgBgFQgFgKgBgOIADh0IASAAIgBAfIgBAZIAAAUIAAAOIAAAZQAAAJABAGIADAGIADAFIAGAEIAHABIgDATQgGAAgFgCg");
	this.shape_30.setTransform(-154.8,-11.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgHgHQgGgHgDgJQgEgKAAgLQAAgLAEgKQAEgKAHgHQAGgIAKgEQAJgEAKAAQAKAAAIAEQAIADAHAHQAGAHAFAJQAEAIABALIhPALQABAGADAGQACAFAEADQAEAEAFACQAGACAFAAIAJgCQAFgBAEgDQAFgDADgEQACgFABgFIASADQgCAJgFAGQgEAHgHAFQgGAFgHADQgIACgIAAQgLAAgKgDgAgHgjQgEABgFAEQgEADgEAFQgDAGgBAIIA2gHIAAgCQgEgJgGgFQgGgFgJAAIgIABg");
	this.shape_31.setTransform(-163.5,-7.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgLBKIgOgEIgNgHIgNgIIAOgPQAHAFAHAFIANAFIALADQAHAAAFgCQAGgBAFgDQAEgCACgEQADgDAAgFQABgDgCgCQgBgDgDgDIgHgEIgIgDIgJgDIgIgBIgLgDIgMgEIgKgGQgFgDgEgFQgEgFgBgHQgCgHAAgJQABgIACgFQADgGAEgFQAEgEAFgEQAFgEAGgCQAGgCAHAAIALgBIASACIAHACIAJAEIAIAEIAIAHIgLAQIgGgGIgHgEIgGgDIgHgDQgHgBgHAAQgHgBgHADQgGACgFAFQgEADgCAGQgDAEAAAFQAAAFADAFQADAEAFAEQAFADAHACQAGADAHABIAOACIAMAEIAMAGQAFAEADAEQAEAFABAGQACAGgBAHQgBAGgDAGIgHAIIgJAHIgKAEIgLACIgLABIgNgCg");
	this.shape_32.setTransform(-175.3,-9.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#990000").ss(3,1,1).p("A+5AlQAAA8A6AqQA7ApBSAAMA3ogACQBSAAA5gqQA6gqgBg8IAAhGQAAg7g6grQg7gphRAAMg3oAACQhTAAg6AqQg4AqAAA8g");
	this.shape_33.setTransform(4.2,-8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FF0033").s().p("A9/CLQg6gqAAg8IAAhGQAAg8A4gqQA6gqBTAAMA3ogACQBRAAA7ApQA6ArAAA7IAABGQABA8g6AqQg5AqhSAAMg3oAACQhSAAg7gpg");
	this.shape_34.setTransform(4.2,-8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-198.5,-27.5,404.4,39.1), null);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween11("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-0.1,1.6);

	this.instance_1 = new lib.Symbol1copy5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.8,0.6,0.389,0.389,0,0,0,499.8,277.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.5,-99.5,199.1,199.1);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol1copy7();
	this.instance.parent = this;
	this.instance.setTransform(152.7,-23.3,0.5,0.5,0,0,0,499.6,277.4);

	this.instance_1 = new lib.Symbol1copy6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-148.3,-23.3,0.5,0.5,0,0,0,499.6,277.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-278.5,-152,557.1,256.2);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy5();
	this.instance.parent = this;
	this.instance.setTransform(390.7,4,0.486,0.486,0,0,0,499.7,277.5);

	this.instance_1 = new lib.Symbol1copy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(135.8,4,0.486,0.486,0,0,0,499.8,277.5);

	this.instance_2 = new lib.Symbol1copy4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-119.2,4,0.486,0.486,0,0,0,499.8,277.5);

	this.instance_3 = new lib.Symbol1copy4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-374.1,4,0.486,0.486,0,0,0,499.8,277.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(255,255,255,0.698)").ss(8,1,1).p("Eg/UgTpMB+pAAAQHdAAFSFuQFRFuAAIGIAAAPQAAIGlRFuQlSFundAAMh+pAAAQndAAlSluQlRluAAoGIAAgPQAAoGFRluQFSluHdAAg");
	this.shape.setTransform(6.1,3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.647)").s().p("Eg/UATqQndAAlSluQlRluAAoGIAAgPQAAoGFRluQFSluHdAAMB+pAAAQHdAAFSFuQFRFuAAIGIAAAPQAAIGlRFuQlSFundAAg");
	this.shape_1.setTransform(6.1,3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-518.4,-126.8,1049.1,259.6);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(3.2,3.7);

	this.instance_1 = new lib.Symbol1copy5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.8,0.6,0.389,0.389,0,0,0,499.8,277.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.5,-99.5,199.1,199.1);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.05,scaleY:1.05},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.5,-99.5,199.1,199.2);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy3();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.questiontext_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Tween12("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(10.8,-10.9,1.06,1.06,0,0,0,0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-0.3,scaleX:1.09,scaleY:1.09,y:-11},9).to({regY:-0.2,scaleX:1.06,scaleY:1.06,y:-10.9},10).to({regY:-0.3,scaleX:1.09,scaleY:1.09,y:-11},10).to({regY:-0.2,scaleX:1.06,scaleY:1.06,y:-10.9},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129.7,-26.6,280.8,31.9);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(945.8,301.7,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(286).to({_off:false},0).wait(43).to({startPosition:19},0).to({alpha:0,startPosition:10},7).wait(1));

	// Layer_15
	this.instance_1 = new lib.Tween7("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(950.7,299.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(276).to({_off:false},0).to({scaleX:1.1,scaleY:1.1},16).to({startPosition:0},37).to({alpha:0},7).wait(1));

	// Layer_2
	this.instance_2 = new lib.Tween6("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(790.9,561.9);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(228).to({_off:false},0).to({regX:0.1,regY:0.1,scaleX:0.81,scaleY:0.81,x:950.9,y:300.8},25).to({startPosition:0},22).to({_off:true},1).wait(61));

	// Layer_10
	this.instance_3 = new lib.Symbol1copy("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(911.9,638.9,1,1,-17.4,0,0,190.6,64.4);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(192).to({_off:false},0).to({_off:true},36).wait(109));

	// Layer_9
	this.instance_4 = new lib.Symbol1copy8("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(635.3,458.7,1,1,-38.9,0,0,41.1,60.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(158).to({_off:false},0).to({_off:true},34).wait(145));

	// Layer_14
	this.instance_5 = new lib.Symbol3("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(950.7,299.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(105).to({_off:false},0).to({_off:true},41).wait(191));

	// Layer_5
	this.instance_6 = new lib.questiontext_mccopy2("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(738.1,152,2.282,2.282,0,0,0,-1.4,-2.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(105).to({_off:false},0).to({_off:true},41).wait(191));

	// Layer_4
	this.instance_7 = new lib.Tween5("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(640,583.4);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(40).to({_off:false},0).to({alpha:1},5).to({startPosition:0},199).to({startPosition:0},85).to({alpha:0},7).wait(1));

	// Layer_11
	this.instance_8 = new lib.Tween13("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(953.9,302.8);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(20).to({_off:false},0).to({alpha:1},6).to({_off:true},227).wait(84));

	// Layer_3
	this.instance_9 = new lib.Tween4("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640,296.6,0.8,0.8);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(20).to({_off:false},0).to({startPosition:0},6).to({regX:0.1,regY:0.1,x:640.1,y:296.7},249).to({regX:0,regY:0,x:640,y:296.6},54).to({alpha:0},7).wait(1));

	// Layer_6
	this.questiontxt = new lib.questiontext_mc();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(630.2,149.8,2.329,2.329,0,0,0,-1.4,-2.4);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(5).to({_off:false},0).to({alpha:1},6).wait(318).to({alpha:0},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(559.9,299.8,1449,849);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;