(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgIQAOgJAQgBQASgCAPAJQAGADAHAHQAGAFAFAKQAFAJAEAOQAEAMABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAEgHAIQgHAGgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape.setTransform(135.4,5.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgcCpIACg6IAAhGIAChgIAtgCIgBA7IgBAxIgBAnIAAAeIAAAxgAgLhqQgHgDgDgEQgFgEgCgGQgDgGAAgHQAAgGADgGQACgGAFgEQADgFAHgCQAFgDAGAAQAHAAAFADQAGACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAGgEAEQgFAEgGADQgFACgHAAQgGAAgFgCg");
	this.shape_1.setTransform(116.2,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("ABGCAIADgfQgbARgaAIQgbAIgWAAQgQAAgPgEQgQgEgLgJQgMgJgHgNQgHgNAAgTQAAgSAGgOQAEgPAKgLQAJgKAOgIQAMgHAPgFQAOgFARgDQAPgCAQAAQAPAAANABIAWADIgGgWQgFgLgHgJQgGgIgKgGQgKgFgNAAQgJAAgKACQgMADgNAHQgOAGgPAMQgRAMgRASIgcghQAWgUATgNQAVgNARgHQARgHAPgDQAOgDAMAAQAVAAAQAIQAQAHAMAMQANAMAJARQAHAQAGATQAFATADAVQACATAAAUQAAAWgDAZQgCAZgFAdgAgKAAQgTADgNAJQgPAJgGANQgHANAEARQADAOAKAFQAJAFAMAAQANAAAPgEQAOgEAPgHIAdgNIAWgNIABgXIgBgXQgLgCgNgCQgLgCgMAAQgVAAgSAFg");
	this.shape_2.setTransform(95.1,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("Ah7i2IAxgCIAAAhQANgNAOgGQAOgHALgEQAOgEAMgBQAPAAAPAEQAQAFANAIQAOAIALALQAMALAIAOQAIAOAEAQQAEAQAAARQgBAUgFARQgFARgJANQgIAPgLALQgMALgMAIQgNAIgOAFQgNAEgOAAQgMgBgPgDQgMgEgPgGQgPgHgOgMIgBCYIgqABgAAAiMQgOAAgMAFQgMAFgKAIQgKAHgGALQgHALgEALIAAArQADAPAHALQAIALAJAHQAKAHAMAEQALAEAMAAQAPABANgGQAOgGALgKQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgMQgKgLgOgHQgNgGgPAAIgCAAg");
	this.shape_3.setTransform(67.2,11.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_4.setTransform(28.6,1.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgoB5QgYgKgTgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAFgQAIgPQAIgOANgNQALgMAPgJQAPgJAQgFQASgFASAAQATAAASAFQASAEAPAJQAPAJAMANQALANAIAQIABABIglAXIgBgCQgGgLgIgJQgJgJgKgHQgKgGgMgEQgMgDgMAAQgSAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHASAAQAMAAALgDQALgDAKgGQAKgFAJgJQAIgIAGgKIABgBIAmAWIAAABQgIAPgNAMQgNALgPAJQgQAIgQAFQgSAEgRAAQgZAAgXgKg");
	this.shape_5.setTransform(3.8,6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AgyB5QgXgJgQgRQgPgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQARgSAWgKQAWgLAZAAQAYAAAUAKQAWAJAPAQQAQAQALAWQAKAVADAaIjCAcQACAQAGANQAGAMAKAJQAKAJAMAFQANAEAOAAQAMAAANgEQALgDAKgHQAKgIAHgKQAHgLAEgOIArAJQgGAVgMAQQgLARgQAMQgPAMgSAGQgSAHgVAAQgcAAgXgJgAgShXQgLAEgLAHQgKAIgJAOQgJANgDATICHgQIgCgEQgIgXgPgMQgPgNgXAAQgIAAgLADg");
	this.shape_6.setTransform(-23.8,6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgIQAOgJAQgBQASgCAPAJQAGADAHAHQAGAFAFAKQAFAJAEAOQAEAMABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAEgHAIQgHAGgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_7.setTransform(-48.9,5.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgIQAOgJAQgBQASgCAPAJQAGADAHAHQAGAFAFAKQAFAJAEAOQAEAMABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAEgHAIQgHAGgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_8.setTransform(-73.9,5.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AguB7QgWgKgQgRQgRgSgJgYQgKgYAAgeQAAgdAKgYQAJgZARgRQAQgRAWgKQAVgJAZAAQAYAAAXAKQAWAKAQASQARARAJAYQAJAZAAAbQAAAcgJAZQgJAYgRARQgQASgWAKQgXAKgYAAQgZAAgVgJgAgghTQgOAIgJANQgIANgFARQgDAQAAAQQgBAQAFAQQAEARAKANQAIANAOAIQANAJASAAQATAAAOgJQANgIAKgNQAIgNAFgRQAEgQAAgQQAAgQgEgQQgEgRgJgNQgIgNgPgIQgOgJgTAAQgTAAgNAJg");
	this.shape_9.setTransform(-101.6,5.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgoB5QgZgKgSgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAEgQAJgPQAJgOAMgNQAMgMAOgJQAOgJARgFQASgFARAAQAUAAASAFQARAEAPAJQAPAJAMANQANANAHAQIABABIgmAXIAAgCQgGgLgIgJQgJgJgJgHQgLgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQAMAAAMgDQALgDAKgGQAKgFAIgJQAIgIAHgKIAAgBIAnAWIAAABQgJAPgMAMQgNALgPAJQgQAIgRAFQgRAEgSAAQgYAAgXgKg");
	this.shape_10.setTransform(-129.1,6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(3,1,1).p("A08kWMAp5AAAQCgAAAACgIAADtQAACgigAAMgp5AAAQigAAAAigIAAjtQAAigCgAAg");
	this.shape_11.setTransform(-0.3,6.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("A08EXQigAAAAigIAAjtQAAigCgAAMAp5AAAQCgAAAACgIAADtQAACgigAAg");
	this.shape_12.setTransform(-0.3,6.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-151.9,-36.7,303.2,73.5);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330404").s().p("AAbhYIAGAAQAjAAAPAQQAGAIACAKQAEAGACAOQAEAbgDAOQgBAWgNAMQgIAJgWAHIgBAAIgfALIgMADIg9AKQgYADgXAFQA5hOBEhjg");
	this.shape.setTransform(89.2,-5.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F0909").s().p("AiMCoQhLgIhKgLQhwgThtgZIg5gNIgGAAQiMgkiNguIgvgPQgBgBAAAAQAAAAgBAAQAAgBAAAAQgBAAAAAAQgEgCgEgBQgFgBgDgCQgHgEgFgGQgFgHgDgQIgKg0QgFgbABgZQAAgXAQgLQAPgKATAGIADABIABAAIAnARIAHABIBkAqQBbAcBaAYIAGABIA5AOQAHADAGAAIApAJIABACQBTASBTAPQBLANBKAIQEsApElgPIBEgEIAygEIBQgJIAhgEQBAgIBAgKIAagFIAngIQAjgHAbgDIABAAIAQgBQhFBkg6BOIgRABQhAAKhAAHQgRABgPADQgoAEgpADQgZADgZABIhEAFQhdAEhdAAQjOAAjJgXg");
	this.shape_1.setTransform(-3.6,-11.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66B4EE").s().p("AEiAyQgUgQAAgYQAAgXAUgQQASgRAaAAIAKAAQANACAMAIQgEASgQAWIgFAKQgSAagRAXQgLgEgIgJgAg4AKQAAgXATgQQASgRAYAAQASAAAPAIQgaAvgxApQgTgQAAgYgAlcAjQgUgRABgYQgBgXAUgRQARgQAYAAQAWAAASAMIgNAXQgIATgWAdQgKAPgJALQgLgEgIgIg");
	this.shape_2.setTransform(8.2,-23.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#56ACEC").s().p("AEVA3QARgXASgaIAFgJQAQgXAEgSQAEADADAEQASAQAAAXQAAAYgSAQQgNANgTAEIgKAAQgOAAgLgEgAhFAqQAygpAZgvQAGAEAFAFQARAQAAAXQAAAYgRAQQgRARgaAAQgZAAgSgRgAlpAnQAJgLAKgPQAWgdAIgTIANgXIAFAEQASARAAAXQAAAYgSARQgSAQgbAAQgLAAgLgEg");
	this.shape_3.setTransform(11.4,-22.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9C5050").s().p("AHEA/Ig1gHQi7gbi/gFIgegBQjDgGhgADQhmADg6ATQAAgFACgEQACgWANgUIAPgPQASgSAXgKQAagMAigEQAEAAADgCIAjAAQAQgCATACIESAJIDUAJIAWABIA3ADIAkADIAcADQANABAJAEQAKAFAJAIIAIAKQASAXAOAxIADAOQgEgGgFgDg");
	this.shape_4.setTransform(13.6,-39.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#850F0F").s().p("AguJyQg7gHg5gKIghgFQhwgXhtgeIg/gTQhdgehbgoQg7hWgmhYQgbhCgOhDQAVAJAUAKQCFA4CUAlQADAAADACIA5ANQBpAWB0ANIAhADQA5AGA7ADQCzALDGgMQBqgFBugMIBEgHQAZgFAZgCIBxgRIAYgDQimDGipB7Ig/AqIhJACIhVACQiVAAiPgSgAH2g4QgMgFgKgKIgDgCQgZgZABgiQAAgkAbgaQAagaAoAAQAEAAAGADQAgABAYAWQAbAaAAAkQAAAkgbAZQgbAagnAAQgZAAgTgLgAH2irQgTAPgBAYQABAYATAQQAIAJALAEQAMAFANAAIAKgBQATgEAOgNQARgQABgYQgBgYgRgPQgDgFgFgCQgLgIgOgCIgKAAQgaAAgSARgACYhHIgCgCIgBAAQgZgZAAgiQAAgkAcgaQAbgaAoAAQAYAAAVALIAUAPQAbAaAAAkQABAigZAZIgDACQgbAagmAAQgoAAgbgagACvirQgTAPAAAYQAAAYATAQQASASAZAAQAZAAASgSQASgQAAgYQAAgYgSgPQgEgGgHgEQgOgHgSAAQgZAAgSARgAiLhIQAAAAAAAAQAAAAAAAAQAAAAAAAAQAAgBAAAAQgLgGgJgHQgbgaAAglQAAgjAbgaQAbgZAmAAQAoAAAbAZQAbAaAAAjQAAAlgbAaQgJAHgKAGQgVAMgbAAQgYAAgVgLgAiIi8QgTARAAAXQAAAYATARQAJAIAKAEQAMAEALAAQAaAAATgQQASgRAAgYQAAgXgSgRIgGgFQgSgLgVAAQgZAAgRAQgAgeooIAAgGIAFgSIADgUQACgEAAgEIAGgaQATgHAcgBQBvgNB1AVIAGABQgyA4g4AzIgfgEQhAgJgtAEQgeACgbAIQACgPAEgQg");
	this.shape_5.setTransform(-13,-9.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6D0A0A").s().p("AiXJRQCph7CljGQA0gIA0gKIBBgMIA+gMIArgIQgKBQghBLQgoBdhJBWIgOAEQg/APhBANIhxATIgxAHQgiAEghADQhIAJhHAFIA+gqgAnIoPQA3gzAzg4QAjAHAXAJIAPAFIgBAEQgBAQgCANIgCANIAAAEIgHApIgFAaQhPgShSgNg");
	this.shape_6.setTransform(48.8,-8.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E2E2E2").s().p("AjVMPIgOgEIgUgHQgigMghgOQgygXgugdIg3gkIgDgEQgfgXgfgbQgdgagbgeQhJhQg4hSQBcAoBdAeIA+ATQBtAeBwAXIAiAFQA4AKA8AHQC3AXDCgHIBJgCQgkAXglAVQiVBTiuA5QhJAXhMASIgVgGgAhhD9Qg8gDg4gGIgigDQh0gNhpgWIg5gNQgDgCgCAAQiUgliFg4QgUgKgWgJIgGgYIgDgRQgFgjgCgjQgDgpADgmQADAAAEADQABAAAAAAQABAAAAAAQAAABABAAQAAAAAAAAIAwAQQCMAuCNAlIAFAAIA5ALQBuAaBvASQBLALBLAJQEjAhEugPIBDgFQAZgBAZgCQApgDAogFQAQgCAQgBQBBgIA/gKIASgBQg9BPguA5IgPASIgYADIhxARQgZACgZAFIhDAHQhvAMhpAFQhoAHhiAAQhZAAhWgGgAhhhcQhKgJhMgOQhTgOhTgTIACgBQAbhNAdgkQAcgfAegVQAYgQAagKIACAAQA5gTBmgDQBggDDDAGIAeABQDAAFC6AbIA1AHQAGADADAGQASAZAABNIABB9IhRAJIgyADIhDAFQhPAEhPAAQjYAAjbgegAGskOQgbAaAAAkQAAAiAYAZIADACQAKAKANAFQASALAaAAQAmAAAbgaQAbgZAAgkQAAgkgbgaQgYgWgfgBQgHgDgDAAQgpAAgaAagABkkOQgbAaAAAkQAAAiAZAZIAAAAIACACQAbAaAoAAQAnAAAagaIADgCQAZgZAAgiQAAgkgcgaIgUgPQgVgLgYAAQgoAAgbAagAjTkeQgbAaAAAjQAAAlAbAaQAJAHALAGQAAAAAAABQAAAAAAAAQAAAAAAAAQABAAAAAAQAUALAYAAQAbAAAWgMQAJgGAJgHQAcgaAAglQAAgjgcgaQgagZgpAAQgmAAgbAZgAhsncIAFgeQACgKAAgLIAEgUIAGggIADgSQAbgIAfgCQAsgEBBAJIAfAEQg+A5hFAzIgyAlIgiAAQgDACgFAAIAFgZgADRrCQh0gVhuANQgdABgUAHIADgLQADgNAEgMIADgFQAEgLAHgIQASgUAagCQAYgBAYABQAOAAAOACIAcABQAOADAOABIA3AIIAzAJIAYAGIgwA1IgHgBg");
	this.shape_7.setTransform(-7.9,-1.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#D4C5C5").s().p("AoEMJQgcgEgcgFIgmgKQBMgSBIgXQCwg5CUhTQAmgVAjgXQBHgGBHgIQAigDAigFIAygGIBxgUQBAgMBAgPIAOgEIgOAQQgiAngpAmQgZAWgcAWIgzAkIg+AkQgzAdg7AXIgIACQguARgyAOQgsALgtAJIgeAEQhUAOhbADIgnABQhVAAhPgNgAFbCYQAug5A8hPQAYgFAYgDIA+gJIAMgDIAfgLIABAAQAEAaAAAfQAAAngFAnIgrAIIg+AMIhBAMQg0AKg0AIIAPgSgAnGnkQgTgCgQACIAyglQBGgzA9g5QBTAMBOATIgDAUIgBAFIgIAkIgEAKQgGAYgIAXIgCAFgAiPrQQgXgJgkgHIAxg1IAMAGQAHAEAFAGIAAAEQAFALgDAhQgCAFAAAFIgOgFg");
	this.shape_8.setTransform(34,1.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D71B2B").s().p("AhIAzQgwAAgVgFIADg8IAKAEQBJAVA/AAIAZgBQAogEAYgPQAWgNAKgWIABAAQAJgDADgEQgPAagZAUQgbAWgjASIgHADQghAOhDAAIgFgBg");
	this.shape_9.setTransform(59.7,-62.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E63544").s().p("Ah7AvIgLgEQABgsACgsIAAgNQAQgIAJgCQALAAALAEQANADAbAQQAoAXAqAPQAiALAiAIQAQADAKgCIADAAIgCAAQgKAWgVANQgZAQgnAEIgaABQg/AAhIgWg");
	this.shape_10.setTransform(59.2,-68.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D1D7D7").s().p("AgWChIgVgBQARiCARhNIAHglQAUhWASAHQAEAJACAOIACAIIAAANQgCAsgBAtIgCA8IgHCGIg2gDg");
	this.shape_11.setTransform(41.6,-61.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.1,-80.5,198.3,161.1);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgIAQgBQASgBAPAIQAGADAHAHQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape.setTransform(260.1,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgcCpIABg6IABhGIAChgIAugCIgCA7IgBAxIgBAnIAAAeIgBAxgAgLhqQgHgDgDgEQgFgEgCgGQgDgGAAgHQAAgGADgGQACgGAFgEQADgFAHgCQAFgDAGAAQAHAAAGADQAFACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAGgEAEQgFAEgFADQgGACgHAAQgGAAgFgCg");
	this.shape_1.setTransform(240.9,0.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("ABGCAIADgfQgcARgZAIQgaAIgXAAQgQAAgPgEQgQgEgLgJQgMgJgHgNQgHgNAAgTQAAgSAGgOQAEgPAKgLQAJgKAOgIQAMgHAPgFQAPgFAQgDQAPgCAQAAQAPAAANABIAWADIgGgWQgFgLgHgJQgGgIgKgGQgKgFgNAAQgJAAgKACQgMADgNAHQgNAGgQAMQgQAMgTASIgbghQAWgUAUgNQATgNASgHQASgHAOgDQAOgDAMAAQAVAAAQAIQAQAHANAMQAMAMAJARQAHAQAGATQAFATADAVQACATAAAUQAAAWgDAZQgCAZgFAdgAgKAAQgTADgNAJQgOAJgHANQgHANAEARQADAOAKAFQAIAFANAAQANAAAPgEQAOgEAQgHIAcgNIAWgNIABgXIgBgXQgLgCgMgCQgMgCgMAAQgVAAgSAFg");
	this.shape_2.setTransform(219.8,5.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("Ah7i2IAxgCIAAAhQANgNAOgGQAOgHALgEQAOgEAMgBQAPAAAPAEQAQAFANAIQAOAIALALQAMALAIAOQAIAOAEAQQAEAQAAARQgBAUgFARQgFARgJANQgIAPgLALQgMALgMAIQgNAIgOAFQgNAEgOAAQgMgBgPgDQgMgEgPgGQgPgHgOgMIgBCYIgqABgAAAiMQgOAAgMAFQgMAFgKAIQgKAHgGALQgHALgEALIAAArQADAPAHALQAIALAJAHQAKAHAMAEQALAEAMAAQAPABANgGQAOgGALgKQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgMQgKgLgOgHQgNgGgPAAIgCAAg");
	this.shape_3.setTransform(191.9,11.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_4.setTransform(153.4,1.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgoB5QgYgKgTgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAFgQAIgPQAJgOAMgNQAMgMAOgJQAPgJAQgFQASgFARAAQAUAAASAFQASAEAPAJQAOAJANANQAMANAHAQIABABIglAXIgBgCQgFgLgJgJQgJgJgKgHQgKgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQANAAALgDQALgDAKgGQAKgFAIgJQAIgIAHgKIABgBIAmAWIAAABQgIAPgNAMQgNALgPAJQgQAIgQAFQgSAEgSAAQgYAAgXgKg");
	this.shape_5.setTransform(128.5,6.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgQgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAQAQAKAWQAKAVADAaIjCAcQACAQAGANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAAMgEQALgDAKgHQAKgIAHgKQAHgLAEgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgLAIgIAOQgJANgCATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_6.setTransform(101,6.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgIAQgBQASgBAPAIQAGADAHAHQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_7.setTransform(75.9,6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgIAQgBQASgBAPAIQAGADAHAHQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_8.setTransform(50.8,6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AguB7QgWgKgQgRQgQgSgKgYQgJgYAAgeQAAgdAJgYQAKgZAQgRQAQgRAWgKQAVgJAZAAQAZAAAVAKQAXAKAQASQAQARAKAYQAKAZAAAbQAAAcgKAZQgKAYgQARQgQASgXAKQgVAKgZAAQgZAAgVgJgAgghTQgOAIgJANQgJANgDARQgFAQAAAQQABAQAEAQQAFARAIANQAKANANAIQAOAJARAAQATAAAOgJQAOgIAIgNQAKgNAEgRQAEgQAAgQQAAgQgEgQQgEgRgJgNQgJgNgOgIQgNgJgUAAQgSAAgOAJg");
	this.shape_9.setTransform(23.2,6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgpB5QgYgKgSgRQgTgSgLgYQgLgXAAgdQAAgPAFgRQAFgQAIgPQAJgOALgNQAMgMAPgJQAPgJAQgFQASgFARAAQAUAAASAFQARAEAPAJQAPAJAMANQANANAHAQIABABIgmAXIAAgCQgGgLgIgJQgJgJgJgHQgLgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQAMAAALgDQAMgDAKgGQAKgFAIgJQAJgIAFgKIABgBIAnAWIAAABQgJAPgNAMQgMALgPAJQgPAIgSAFQgRAEgSAAQgYAAgYgKg");
	this.shape_10.setTransform(-4.4,6.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_11.setTransform(-44,6.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AA8CsQAHgXADgUIAEglIAAgkQgBgXgHgSQgHgPgLgLQgKgLgNgEQgMgGgMAAQgMACgNAGQgMAFgMAMQgNAKgLAVIgCCVIgsABIgElaIA0gCIgBCJQAMgOAOgHQAOgIAMgEQAOgEANgCQAZAAAVAJQAVAJAOAQQAPARAJAZQAJAXABAfIgBAgQAAARgBAQIgDAeQgDAPgDALg");
	this.shape_12.setTransform(-72.8,0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_13.setTransform(-99,1.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_14.setTransform(-132.9,1.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("AgoB5QgYgKgTgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAFgQAIgPQAIgOANgNQALgMAPgJQAPgJAQgFQASgFARAAQAUAAASAFQASAEAPAJQAPAJAMANQALANAIAQIABABIglAXIgBgCQgGgLgIgJQgJgJgKgHQgKgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQANAAALgDQALgDAKgGQAKgFAIgJQAJgIAGgKIABgBIAmAWIAAABQgIAPgNAMQgNALgPAJQgQAIgQAFQgSAEgSAAQgYAAgXgKg");
	this.shape_15.setTransform(-157.8,6.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_16.setTransform(-185.3,6.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#663300").s().p("AAODEQgNgFgIgIQgJgJgGgKQgHgLgFgLQgKgZgDghIAGkcIAuAAIgCBJIgCA9IgBAxIAAAlIgBA9QAAAVAFAQIAFAOQAEAHAFAFQAGAGAHADQAIADAJAAIgGAtQgQAAgMgFg");
	this.shape_17.setTransform(-204.1,-2.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#663300").s().p("AgyB5QgXgJgQgRQgPgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQARgSAWgKQAXgLAYAAQAYAAAUAKQAWAJAPAQQAQAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAKAJAMAFQAOAEANAAQANAAAMgEQALgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgMAQQgLARgQAMQgPAMgSAGQgSAHgVAAQgcAAgXgJgAgShXQgMAEgKAHQgLAIgIAOQgJANgDATICHgQIgBgEQgJgXgPgMQgPgNgXAAQgIAAgLADg");
	this.shape_18.setTransform(-225.5,6.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#663300").s().p("AgdC1QgRgEgQgGQgRgHgPgKQgQgJgQgKIAhgnQASAPARAJQARAJAOAFQAQAFANACQARABAOgEQANgDALgHQAKgGAHgJQAGgJABgKQABgIgEgGQgDgHgHgHQgHgFgKgFIgUgIIgWgFIgVgFQgNgCgOgEQgOgDgOgHQgOgGgMgIQgMgJgJgNQgJgMgEgRQgFgRACgWQABgSAGgOQAGgPAKgMQAKgLANgIQAMgIAPgFQAOgGAQgCQAPgDAOAAQAWACAVAFIATAGIAUAIIAUALQAKAHAJAJIgZAnQgHgHgJgGIgQgLIgQgIIgPgFQgSgFgQgBQgUAAgQAHQgQAGgLAKQgLAJgFAMQgGAMAAANQAAALAHAKQAHALAMAJQAMAJAQAGQARAGASADQAQABAQAFQARADAPAGQAOAHANAJQAMAIAJAMQAIALAFAPQADAPgCARQgCAPgHANQgHAMgKAJQgJAJgNAHQgMAFgNAFQgOADgOACIgaABQgQAAgRgDg");
	this.shape_19.setTransform(-254.2,1.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#CC9900").ss(6,1,1).p("EgsugGPMBZdAAAQB+AABaBsQBaBrAACZIAAA/QAACZhaBrQhaBsh+AAMhZdAAAQh+AAhahsQhahrAAiZIAAg/QAAiZBahrQBahsB+AAg");

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFCC00").s().p("EgsuAGQQh+AAhahsQhahsAAiZIAAg+QAAiYBahsQBahsB+AAMBZdAAAQB+AABaBsQBaBsAACYIAAA+QAACZhaBsQhaBsh+AAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.9,-43,639.8,86);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330404").s().p("AAbhYIAGAAQAjAAAPAQQAGAIACAKQAEAGACAOQAEAbgDAOQgBAWgNAMQgIAJgWAHIgBAAIgfALIgMADIg9AKQgYADgXAFQA5hOBEhjg");
	this.shape.setTransform(89.2,-5.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F0909").s().p("AiMCoQhLgIhKgLQhwgThtgZIg5gNIgGAAQiMgkiNguIgvgPQgBgBAAAAQAAAAgBAAQAAgBAAAAQgBAAAAAAQgEgCgEgBQgFgBgDgCQgHgEgFgGQgFgHgDgQIgKg0QgFgbABgZQAAgXAQgLQAPgKATAGIADABIABAAIAnARIAHABIBkAqQBbAcBaAYIAGABIA5AOQAHADAGAAIApAJIABACQBTASBTAPQBLANBKAIQEsApElgPIBEgEIAygEIBQgJIAhgEQBAgIBAgKIAagFIAngIQAjgHAbgDIABAAIAQgBQhFBkg6BOIgRABQhAAKhAAHQgRABgPADQgoAEgpADQgZADgZABIhEAFQhdAEhdAAQjOAAjJgXg");
	this.shape_1.setTransform(-3.6,-11.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#66B4EE").s().p("AEiAyQgUgQAAgYQAAgXAUgQQASgRAaAAIAKAAQANACAMAIQgEASgQAWIgFAKQgSAagRAXQgLgEgIgJgAg4AKQAAgXATgQQASgRAYAAQASAAAPAIQgaAvgxApQgTgQAAgYgAlcAjQgUgRABgYQgBgXAUgRQARgQAYAAQAWAAASAMIgNAXQgIATgWAdQgKAPgJALQgLgEgIgIg");
	this.shape_2.setTransform(8.2,-23.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#56ACEC").s().p("AEVA3QARgXASgaIAFgJQAQgXAEgSQAEADADAEQASAQAAAXQAAAYgSAQQgNANgTAEIgKAAQgOAAgLgEgAhFAqQAygpAZgvQAGAEAFAFQARAQAAAXQAAAYgRAQQgRARgaAAQgZAAgSgRgAlpAnQAJgLAKgPQAWgdAIgTIANgXIAFAEQASARAAAXQAAAYgSARQgSAQgbAAQgLAAgLgEg");
	this.shape_3.setTransform(11.4,-22.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9C5050").s().p("AHEA/Ig1gHQi7gbi/gFIgegBQjDgGhgADQhmADg6ATQAAgFACgEQACgWANgUIAPgPQASgSAXgKQAagMAigEQAEAAADgCIAjAAQAQgCATACIESAJIDUAJIAWABIA3ADIAkADIAcADQANABAJAEQAKAFAJAIIAIAKQASAXAOAxIADAOQgEgGgFgDg");
	this.shape_4.setTransform(13.6,-39.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#850F0F").s().p("AguJyQg7gHg5gKIghgFQhwgXhtgeIg/gTQhdgehbgoQg7hWgmhYQgbhCgOhDQAVAJAUAKQCFA4CUAlQADAAADACIA5ANQBpAWB0ANIAhADQA5AGA7ADQCzALDGgMQBqgFBugMIBEgHQAZgFAZgCIBxgRIAYgDQimDGipB7Ig/AqIhJACIhVACQiVAAiPgSgAH2g4QgMgFgKgKIgDgCQgZgZABgiQAAgkAbgaQAagaAoAAQAEAAAGADQAgABAYAWQAbAaAAAkQAAAkgbAZQgbAagnAAQgZAAgTgLgAH2irQgTAPgBAYQABAYATAQQAIAJALAEQAMAFANAAIAKgBQATgEAOgNQARgQABgYQgBgYgRgPQgDgFgFgCQgLgIgOgCIgKAAQgaAAgSARgACYhHIgCgCIgBAAQgZgZAAgiQAAgkAcgaQAbgaAoAAQAYAAAVALIAUAPQAbAaAAAkQABAigZAZIgDACQgbAagmAAQgoAAgbgagACvirQgTAPAAAYQAAAYATAQQASASAZAAQAZAAASgSQASgQAAgYQAAgYgSgPQgEgGgHgEQgOgHgSAAQgZAAgSARgAiLhIQAAAAAAAAQAAAAAAAAQAAAAAAAAQAAgBAAAAQgLgGgJgHQgbgaAAglQAAgjAbgaQAbgZAmAAQAoAAAbAZQAbAaAAAjQAAAlgbAaQgJAHgKAGQgVAMgbAAQgYAAgVgLgAiIi8QgTARAAAXQAAAYATARQAJAIAKAEQAMAEALAAQAaAAATgQQASgRAAgYQAAgXgSgRIgGgFQgSgLgVAAQgZAAgRAQgAgeooIAAgGIAFgSIADgUQACgEAAgEIAGgaQATgHAcgBQBvgNB1AVIAGABQgyA4g4AzIgfgEQhAgJgtAEQgeACgbAIQACgPAEgQg");
	this.shape_5.setTransform(-13,-9.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6D0A0A").s().p("AiXJRQCph7CljGQA0gIA0gKIBBgMIA+gMIArgIQgKBQghBLQgoBdhJBWIgOAEQg/APhBANIhxATIgxAHQgiAEghADQhIAJhHAFIA+gqgAnIoPQA3gzAzg4QAjAHAXAJIAPAFIgBAEQgBAQgCANIgCANIAAAEIgHApIgFAaQhPgShSgNg");
	this.shape_6.setTransform(48.8,-8.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E2E2E2").s().p("AjVMPIgOgEIgUgHQgigMghgOQgygXgugdIg3gkIgDgEQgfgXgfgbQgdgagbgeQhJhQg4hSQBcAoBdAeIA+ATQBtAeBwAXIAiAFQA4AKA8AHQC3AXDCgHIBJgCQgkAXglAVQiVBTiuA5QhJAXhMASIgVgGgAhhD9Qg8gDg4gGIgigDQh0gNhpgWIg5gNQgDgCgCAAQiUgliFg4QgUgKgWgJIgGgYIgDgRQgFgjgCgjQgDgpADgmQADAAAEADQABAAAAAAQABAAAAAAQAAABABAAQAAAAAAAAIAwAQQCMAuCNAlIAFAAIA5ALQBuAaBvASQBLALBLAJQEjAhEugPIBDgFQAZgBAZgCQApgDAogFQAQgCAQgBQBBgIA/gKIASgBQg9BPguA5IgPASIgYADIhxARQgZACgZAFIhDAHQhvAMhpAFQhoAHhiAAQhZAAhWgGgAhhhcQhKgJhMgOQhTgOhTgTIACgBQAbhNAdgkQAcgfAegVQAYgQAagKIACAAQA5gTBmgDQBggDDDAGIAeABQDAAFC6AbIA1AHQAGADADAGQASAZAABNIABB9IhRAJIgyADIhDAFQhPAEhPAAQjYAAjbgegAGskOQgbAaAAAkQAAAiAYAZIADACQAKAKANAFQASALAaAAQAmAAAbgaQAbgZAAgkQAAgkgbgaQgYgWgfgBQgHgDgDAAQgpAAgaAagABkkOQgbAaAAAkQAAAiAZAZIAAAAIACACQAbAaAoAAQAnAAAagaIADgCQAZgZAAgiQAAgkgcgaIgUgPQgVgLgYAAQgoAAgbAagAjTkeQgbAaAAAjQAAAlAbAaQAJAHALAGQAAAAAAABQAAAAAAAAQAAAAAAAAQABAAAAAAQAUALAYAAQAbAAAWgMQAJgGAJgHQAcgaAAglQAAgjgcgaQgagZgpAAQgmAAgbAZgAhsncIAFgeQACgKAAgLIAEgUIAGggIADgSQAbgIAfgCQAsgEBBAJIAfAEQg+A5hFAzIgyAlIgiAAQgDACgFAAIAFgZgADRrCQh0gVhuANQgdABgUAHIADgLQADgNAEgMIADgFQAEgLAHgIQASgUAagCQAYgBAYABQAOAAAOACIAcABQAOADAOABIA3AIIAzAJIAYAGIgwA1IgHgBg");
	this.shape_7.setTransform(-7.9,-1.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#D4C5C5").s().p("AoEMJQgcgEgcgFIgmgKQBMgSBIgXQCwg5CUhTQAmgVAjgXQBHgGBHgIQAigDAigFIAygGIBxgUQBAgMBAgPIAOgEIgOAQQgiAngpAmQgZAWgcAWIgzAkIg+AkQgzAdg7AXIgIACQguARgyAOQgsALgtAJIgeAEQhUAOhbADIgnABQhVAAhPgNgAFbCYQAug5A8hPQAYgFAYgDIA+gJIAMgDIAfgLIABAAQAEAaAAAfQAAAngFAnIgrAIIg+AMIhBAMQg0AKg0AIIAPgSgAnGnkQgTgCgQACIAyglQBGgzA9g5QBTAMBOATIgDAUIgBAFIgIAkIgEAKQgGAYgIAXIgCAFgAiPrQQgXgJgkgHIAxg1IAMAGQAHAEAFAGIAAAEQAFALgDAhQgCAFAAAFIgOgFg");
	this.shape_8.setTransform(34,1.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D71B2B").s().p("AhIAzQgwAAgVgFIADg8IAKAEQBJAVA/AAIAZgBQAogEAYgPQAWgNAKgWIABAAQAJgDADgEQgPAagZAUQgbAWgjASIgHADQghAOhDAAIgFgBg");
	this.shape_9.setTransform(59.7,-62.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E63544").s().p("Ah7AvIgLgEQABgsACgsIAAgNQAQgIAJgCQALAAALAEQANADAbAQQAoAXAqAPQAiALAiAIQAQADAKgCIADAAIgCAAQgKAWgVANQgZAQgnAEIgaABQg/AAhIgWg");
	this.shape_10.setTransform(59.2,-68.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D1D7D7").s().p("AgWChIgVgBQARiCARhNIAHglQAUhWASAHQAEAJACAOIACAIIAAANQgCAsgBAtIgCA8IgHCGIg2gDg");
	this.shape_11.setTransform(41.6,-61.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.1,-80.5,198.3,161.1);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol2copy12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCFF00").s().p("AiyD4QBlgHBfhoQBphuAmicIAIglQAJgtABglQgBA9gRBJQgHAbgLAdQgoBxhOBXQhbBkhgAGIgQABg");
	this.shape.setTransform(71.2,-2.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#003300").s().p("AmRJLIAUgNIHjpTQA/g3BCgOQgGg2ATg0IApgyQglBcAHBbQhEgThRBnInjJTIgJABQgUAAAFgegAm5JiIl2jaIgwhIIGKDsQAEANAMAHIAOAigACDBTIA6hHQArgrAsAuIAAADQAGAqAOAkQAaBEAqAfgAFQAxQgwhsAnidQAoifBnhwQBohuBqAAQBsAAAvBuQAdBDAABVQgDhGgag4QgrhphmAAQhpAAhiBpQhkBrglCZQgmCVAvBrQAqBfBVAJQADACAEAAIAAABQhsAAgxhxgAjEmcIC0jMIgFBcIAAAFIgCAFIh4B9g");
	this.shape_1.setTransform(2.6,6.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000B0D").s().p("AiRAtIA2iYIACgFIAAgFIDrBpIh2CDg");
	this.shape_2.setTransform(9.4,-34.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#006600").s().p("AssHMIgCAAIhPhxIAtAbIAwBIIF2DaIAQAXIAYAfgAGrEQIgwgXQgqgfgahEQgOgkgGgqIAAgDQgPhlAgiCQAgh9BBhmQCWi7CSgRQAbgDAYACIABAAQBOAXAkBhQAzCEgvC0QgeB+hBBjQgeAuglApQh0CDhzAAQgbAAgYgJgAHlmxQhnBwgoCfQgnCdAwBsQAxBxBsAAIAHAAIAHAAIAQgBQBfgGBchkQBOhXAphxQAKgdAHgbQAShJAAg9QAAhVgdhDQgvhuhsAAQhqAAhoBugAEDiOQgTA0AGA2QhCAOg/A2gAiAlPIB4h9Ig2CZgAgGnWIAFhcIADgFIEqB8IhHBOgAKcpzQAlgsgOgsQAogJAMAcQAABJgfANg");
	this.shape_3.setTransform(1.1,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#99FF00").s().p("AmZKeQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAInmkZQAQgNAkAAIAAADIHaETIMLunQBuiIBsgvIATgIQATgHAQgDIAVgDIm1inQiOgriRCDIADgDQCWiSCWAvIDZBSIA/AYIC8hcQAEAEACAGIi5BVIBaAiIAgALIAsARIAeALIAEADIAGACQgYgCgbADQiSARiWC7IsTO0QgdgQgTAKgAIKFKQhVgIgqhgQgvhrAmiVQAliZBjhrQBjhoBpAAQBlAAAsBoQAaA4ADBGQgBAmgJAsQANhmgjhOQgshlhiAAQhjAAhgBlQhdBngnCPQgiCQAtBmQAmBVBKAPg");
	this.shape_4.setTransform(-0.9,-10.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#009900").s().p("AlDLnIgigSIgYgfIgQgXIACAAIgOgjQgMgHgEgNImKjrIgtgcIghgSQgEgLAAgHIHcEXIAHgXIANgMQABAAAAgBQAAAAABAAQAAgBABAAQABAAAAAAQAUgKAcAQIMTu0QhBBmggB9QggCDAPBlQgsgwgrAtIg6BHImwIVQARAqgkAdQgMAEgKAAQgPAAgKgJgAEgiHIiOCuInjJTIgUANQgGAlAegHIHjpUQBRhnBEATQgHhaAlhcgAIZDdQgEAAgDgCIABAAQhLgPgmhVQgthmAjiQQAmiPBdhnQBghlBjAAQBiAAAtBlQAiBOgNBmIgIAlQglCchqBuQhfBphlAGIgHABIgHgBgALlpcQAfgNAAhJQgMgbgoAIQg+ALAIBCIhbgiIC5hVIACAAQAgAOAEA6QABA5gcAdg");
	this.shape_5.setTransform(-1.8,0.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00CC00").s().p("As6GXIAAgDQgFgYAPgKIHmEZIgNALIgHAYgAr9FoIACgEIM0urQCSiDCOArIG1CnIgVADQgRADgSAGIgTAJQhsAvhuCHIsLOogABmnSIi0DNIA2AWIBAAcICuBKIB2iDIBHhPIkqh8gAJCpbICxhSQA1AAAUAOIi7Bcg");
	this.shape_6.setTransform(-9.2,-8.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.502)").s().p("AmPFhImMjfIOdnlIJ8A8IAeAVIl8CaQgFAAgDAEIr9HNIAAADQgOAIgMAAQgJAAgHgDg");
	this.shape_7.setTransform(12.4,43);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy12, new cjs.Rectangle(-92,-78.5,184.1,157.1), null);


(lib.Symbol2copy11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgqBMQgVgNAAgdQAAgdASgfQATggAagNQAZgPASALQAVAMAAAdQAAAfgRAdQgSAggcAQQgNAHgMAAQgKAAgIgFg");
	this.shape.setTransform(-14,-82.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.4)").s().p("AhREWQhYhRgliNQgliMAihzQAjhzBVgYQBWgVBUBTQBYBRAmCNQAmCLgkBzQghBzhVAXQgTAFgTAAQhBAAhFhBg");
	this.shape_1.setTransform(31.2,-37.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AqvCcQK9nEKiDO");
	this.shape_2.setTransform(-0.1,-24.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("AqolmIACAAQAsjrB9igQAMgVARgUQDPjxEgADQEgAIDGD4QBRBlAtBzQAsBsAQB5QABAPADANQAEA/gBA8QgIFdjAFQQhlC0iFBJQg2AihAAOQgfAEgTA8QgGAagNAMIAAAFIAAABIAwAzIAUAAQgXAtgvAUQgDgBAAAAABCOhIACgRQgggYgJgYQgbgwggAAQgBgBgEgCQgBACgDAAQgagHgZgIQglgOgVgHQgQgIgUgJQinhdiHjRQivkGgZkaQgGg7ABg8QgBhJAOhEQAAgIAAgGABEPpIAAAcQgLAGgLgDIAUhnIACBIIAFhXAqrlWQADgKAAgG");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(219,167,104,0.612)").s().p("AgKAzIAThmIACBHIAAAcQgIAEgGAAIgHgBg");
	this.shape_4.setTransform(5.7,98.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(100,38,0,0.612)").s().p("AgjA5IAAgcIAEhWIAvAzIAVAAQgYAsgtAUIgDgBg");
	this.shape_5.setTransform(10.5,97.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF77BB").s().p("AgDgbIADgRIADACIAAABIgDBWg");
	this.shape_6.setTransform(7,95.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(196,46,9,0.612)").s().p("AACgHIgBAMIgCADQACgJABgGg");
	this.shape_7.setTransform(-68.2,-35.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],20,-16.4,0,20,-16.4,49.2).s().p("AqxCIQgBhIAOhEQJtlZKuCBQAsBsAQB5QqjjNq8HEQgGg8ABg8g");
	this.shape_8.setTransform(-0.4,-34.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],23.9,-50.7,0,23.9,-50.7,78.6).s().p("ABBKNQgfgXgJgYQgagwgigBIgFgDQAAABAAAAQgBAAAAABQAAAAgBAAQAAAAgBAAQgbgGgYgIIg6gVIglgRQimhdiIjRQivkGgZkaQK9nFKiDOQABAPADANQAEA+gBA8QgHFfjBFPQhlC0iEBJQg2AhhBAOQgfAFgSA7QgGAbgOALIAAAGg");
	this.shape_9.setTransform(0.3,25.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#C7FA10","#00B606"],[0.031,1],17.2,5.1,0,17.2,5.1,56).s().p("AqMFKIABAAQAsjrB+ifQAMgVAQgUQDPjxEgADQEgAIDGD4QBRBkAtBzQquiAptFYIABgOgAqIFKIAGgGQACgDgCgEg");
	this.shape_10.setTransform(-2.7,-68.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy11, new cjs.Rectangle(-70.4,-104.3,140.8,208.6), null);


(lib.Symbol2copy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C0C0C").ss(0.3,0,0,4).p("AKHiDQgJgFgLAIIgCAFQgCACAAAFAJvhtIADAEQAFAGAPgCQAUgLgGgQQgDgBgCgBApoBbIgFAAQgBAAgEAAQgDAAgCAAIgDAAIgBACAqSBrQgCAEAAAEQACAKAJAFQALAEAMgBQANgDAKgIQAIgHgCgGQAAgJgGgDIgCgC");
	this.shape.setTransform(23.8,-6.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgSAJQgHgCgBgBIgBgCQAAgFAGgCQAIgFAJAAIABgCIADAAIAEAAIAGAAIAEAAIAEAAQAFACACADIADACIAAABQABAJgcACIgKACIgJgCg");
	this.shape_1.setTransform(-39.4,4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#009900").s().p("ABqCSQjChGg/g+IhQi+IANgFQAJgDAIgBQAJgCAIACIBVA7IB4BXQCKBbBBAkIAAABIAIBiIh+gpg");
	this.shape_2.setTransform(-67.4,-8.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#434249").s().p("AABAXQAXhAAEgoIAAAMIgOBFQgPA+gaAUQAVghAHgag");
	this.shape_3.setTransform(19,17.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ADAEAC").s().p("AAtBoIgCAAQgFgEgBgDQgNgNgLgQQg3hJgQg1IgBgtIAAgCIAHAAQAKgBAKAHQAQAKANAWIACAAIAAAGQAFAwAhA+QAJAVAOAOIAAADQAAARgIACg");
	this.shape_4.setTransform(41.9,12.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#191815").s().p("AgmCrQATgMAEgMIgDgHIAFgFQAmgqALh1QAGg5gBgyQgFgrgpgMQAkgCAOAqQAIAUgBAWQAFAxgPBHQgGAkgIAbQgPA2ghAhQgSASgOADg");
	this.shape_5.setTransform(79.1,1.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgDgHIADAAIAAANIAEAAQgDACgEAAg");
	this.shape_6.setTransform(82.6,-23.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#252424").s().p("AgPAEIACAAIABgEQAIgKAKAAIAEAAIAGAAIABABQAAAFgNAEIgRALQgEgDACgEg");
	this.shape_7.setTransform(87.5,-18.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#D7D7D9").s().p("AqJCCQgJgFgCgKQAAgEACgEQABACAIACQAIACALgCQAcgCgBgLIAAgBQAGADAAAJQACAGgIAHQgKAIgNADIgFABQgJAAgJgEgAJyhpIgDgEIATgMQANgEAAgFIAFACQAGAQgUALIgHABQgJAAgEgFgAJxh7IACgFQALgIAJAFIgDAAQgLAAgIAKIgCAFQAAgFACgCg");
	this.shape_8.setTransform(23.8,-6.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#464433").s().p("AgIADIAAgFIARAAIAAAFg");
	this.shape_9.setTransform(85,-25.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#7F7F7F","#828282","#979797","#A8A8A8","#B4B4B4","#BBBBBB","#BDBDBD"],[0.176,0.18,0.255,0.337,0.443,0.592,1],4,0,-4.3,0).s().p("AgRAPIgSgBIgFAAIAAgOQACAFAOABQABAAAAAAQABAAAAAAQABAAAAgBQAAAAABAAQgEAAgFgIIgEgJIgCAAIAIgEIADAAIAFAAQALgDAQAHQASAIAPAKIgxAMgAgYgFIASAAIAAgHIgSAAg");
	this.shape_10.setTransform(86.6,-24.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#382010").s().p("AgHACIgDAAIAAgDIAAgCIADAAIAAgCIABgBIAAgBIABAAIACgBIADAAIACAIQAGAIADAAQAAAAgBABQAAAAAAAAQgBAAAAAAQgBABAAAAQgOgCgBgGg");
	this.shape_11.setTransform(83.3,-25);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#4F4343").s().p("AAAAyQgQgFgUAIIgGhHQBCgXATgOIgMAyQgLAogBAVQgKgHgJABg");
	this.shape_12.setTransform(36.7,-2.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FFCC4D","#5B2B00"],[0,1],6.3,19.4,-3.9,-21.3).s().p("AhbDOQghgFgTgUQgOgOgIgVQghg/gHgxIAAgFIgBgIIAAgEIgCgIIACgiQANhEBCgfIBPgnQBZgmArgBIA7gHIACAAIADABQAHAAAHACQAqAMAFArQABAygGA5QgLB1gnAqIgFAFQgLAPgaASQg6AphhAOQgJACgLAAQgNAAgPgEg");
	this.shape_13.setTransform(62.3,3.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#7F7F7F","#898989","#A4A4A4","#BABABA","#CBCBCB","#D6D6D6","#DDDDDD","#DFDFDF"],[0.176,0.192,0.251,0.318,0.396,0.49,0.624,1],19.3,1.3,0,19.3,1.3,48.8).s().p("AqHB4IAAgBIgCgIIAJgGQANgGASgEIDwggQBrgLAIACICGgFQCrACAEgBIArAAQAtgDAXgQICMg8QCSg9AlACIABAAICGgdQAFgBADgBIASACIgaAIQgLgBgPAGIhfAZQguAHh8AvIhzAvQgmAbg+AJIg3AAQg7gEhaAAIhPAAQjJAKiYAaIhtAYQgKgCgFAEIgDAEg");
	this.shape_14.setTransform(19.8,-11.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0F1815").s().p("AAoATQATg0ABgsQAIg3gjgcIgYgLQAjgFAQAkQAJARgBAXQABAPgBAWQgDAngYBBQgHAZgVAhIgeAsQgTATggAKQBKgyAihng");
	this.shape_15.setTransform(15,15.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FFCC4D","#5B2B00"],[0,1],0,20.6,0,-20.5).s().p("Ai0DEQgWgFgXgbQgvgygHhjIAAhNQgCgWAQgZQAggzBXgRIB6gUQCOgPBjAPIADABIAHAEIAZALQAjAcgIA3QgBAsgTA1QgjBmhKAyQgEAEgGADQgfASg5APQg+APhBAAQg0AAg1gKg");
	this.shape_16.setTransform(-6.9,17.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#282828").s().p("AolERQgJgIgPgaQgZgxgVhQQgFgZgHgUQgQgrgSAOIgFgSIgIhhIADAAIADgFQAEgDALACIBtgYQCYgbDJgKIBPAAQBbAAA5AFIA4AAQA9gJAngbIBygwQB9gwAtgGIBggZQAPgGALAAIAagIIAJADIAxgMIAABaIgSATQgSAhgFA2IgRBWQgbBfgoAzQgaAagrAWQheAshrgSQgNgBgTgRQgIgEgHgHIAHACQAIgCAAgRIAAgDQATAUAgAFQAbAGAWgEQBigOA6gpQAagSAKgPIAEAHQgEAMgTAMIgPAQQAPgDASgSQAighAOg2QAIgaAHgkQAPhIgFgxQABgWgJgUQgNgqglACQgHgCgHAAIgDgBIgCAAIg7AHQgsABhaAmIhPAnQhBAfgNBEIgDAjIADAIIAAAEIAAAIQgOgXgQgKQACgVAKgpIAMgyQgTAOhCAXIAFBIQAVgIAPAFIgHAAIAAADQgQgGgoARIgSALQgXARgGAaIgTA/QgVBHgQAQIgtBEQhGBIhrARQg1APg/ACIgLAAQh5AAgzg3gAlBhsIh7AUQhWARghAzQgPAYACAWIAABOQAHBjAvAyQAXAbAWAFQB3AWBygbQA5gPAegSQAHgDADgEQAggKATgTIAggsQAZgTAQg/IAOhGIAAgLQABgVgBgPQAAgXgIgRQgQgkgkAFIgIgEIgCgBQgygIg8AAQg8AAhIAIgApyg3QgJAAgIAFQgFACAAAGIAAACQgDADAAAFQADAKAJAFQALAEALgBQAOgDAKgIQAIgHgDgHQABgJgGgCIgCgCQgDgDgFgCIgEAAIgEAAIgGAAIgEAAIgEAAgAJ5kBIACADQAGAHAPgCQAUgMgHgPIgEgDIgBgBIgHAAQgJgEgLAIIgDAEQgBADAAAEIgCAAQgCAEAEAEg");
	this.shape_17.setTransform(22.8,8.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#004800").s().p("AhNCPIgIgEIglgiQgRgNALgOIARgNID1iNQBLg0BBgLQAggHATAFIIQCTIgCAAIgEAAIgEAAIgIAFIgCABIgBAAIAAACIgBABIAAABIgDAAIAAACIAAAFIAAAQIiGAeQgvgIhNgOIh+gbQgPgDgVgLIgQgNQgHgLgwgXIgugVQgggpgrAZQgWANgOAVQiJB6hXA9QgPAFgLAAIgHgBgAtUACIAJAAQA5AEBGALQgEAZgTAKQgJAFgUAEg");
	this.shape_18.setTransform(-0.6,-27);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy10, new cjs.Rectangle(-90.6,-41.4,181.5,83), null);


(lib.Symbol2copy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E794BE").s().p("AgfApQAZgeAPgoIADgGQAIgXAEgVQAEATABASQADANAAAPQgBAggMAfQgEAQgIAPg");
	this.shape.setTransform(64.8,-31.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F3409A").s().p("Ai/iFQBEhABeADQBcACBCBGQAJAHAFAIQAHAIADAHQAnA7AABHQgCAigKAdQgNApgeAjQgJALgJAIgACHA2IgCAGQgQAogZAfIAmAnQAJgQAEgPQALgfABghQABgPgDgNQgBgTgEgTQgFAWgIAXg");
	this.shape_1.setTransform(52.4,-40.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E794BE").s().p("AgSAxQgLgfgCggQAAgPADgNQACgSAEgTQADAVAJAXIACAGQAQAoAYAeIglAnQgIgPgFgQg");
	this.shape_2.setTransform(-64.8,-31.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F3409A").s().p("AiHCxQgegjgOgpQgKgdgCgiQAAhHAmg7IAKgPIAPgPQBBhGBbgCQBggDBEBAIk2FJQgJgIgIgLgAiZAvQgDANABAPQACAhAKAfQAGAPAIAQIAmgnQgYgfgSgoIgCgGQgIgXgDgWQgEATgDATg");
	this.shape_3.setTransform(-52.4,-40.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#6666FF").s().p("AH4CiQgDh2hUhSQhYhXh8AAImZAAQh8AAhYBXQhUBSgEB2IguAAQABiFBfhfQBghfCGAAIHBAAQCFAABhBfQBfBfABCFg");
	this.shape_4.setTransform(0.4,-70.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_5.setTransform(-17.8,-38.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_6.setTransform(-23.7,-38.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#993300").s().p("AANCKQgNgOAAgUQAAgKADgJIjXiPQgnghhOg8QBHAtBDAlIDICQIAEgDQAPgQATAAQAVAAAMAOIAJAKIBzg5QgEgIAGgNQAFgJALgEIARgDQAHAAAHgDIAegEQAdgEASgIQgXAOgVAVQgFAKgFAFQgGAJgGAFQgKAMgMADQgDACgEAAIgIAAIgKgDQgGgEgCgGIh0A7QADAEAAAGQAAAUgPAOQgNAOgVAAQgTAAgPgOg");
	this.shape_7.setTransform(-4.4,-10);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgUAgQgHgLAAgUQAAgWAHgKQAGgLAOAAQAPAAAGALQAHALAAAUQAAAVgHALQgGALgPAAQgOAAgGgLgAgGgYQgCAFAAATQAAAUACAFQACAEAEAAQAEAAACgEQACgEAAgVQAAgSgBgFQgCgFgFAAQgEAAgCAEg");
	this.shape_8.setTransform(-35,-22.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_9.setTransform(-40.8,-23);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgYAWIAQgDQABAKAHAAQAKAAAAgXIAAgDQgFAIgJAAQgKAAgGgIQgGgIAAgKQAAgLAHgIQAGgIANAAQALAAAGAHQAGAGACAJQACAKAAAKQAAArgbAAQgTAAgFgVgAgGgZQgCADAAAGQAAANAIAAQAJAAAAgNQAAgGgCgDQgDgDgEAAQgDAAgDADg");
	this.shape_10.setTransform(-43.8,-1.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgUAkQgHgGAAgKQAAgGADgFQADgEAIgEQgLgHAAgMQgBgKAHgHQAGgHANAAQAMAAAHAGQAGAGAAAJQAAAMgLAGQANAHAAAOQAAAKgHAHQgHAIgOAAQgOAAgGgHgAgLATQAAAFADADQADADAFAAQAFAAADgDQADgDAAgEQAAgDgDgDQgCgDgJgFQgIAFAAAIgAgFgbQgDACAAAEQAAAFAFADIAIAFQAFgGABgGQAAgEgDgDQgDgDgEAAQgDAAgDADg");
	this.shape_11.setTransform(-38.3,20.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgNApQAAgmAXgbIgiAAIABgQIAwAAIAAAOQgTAZgBAqg");
	this.shape_12.setTransform(-25.3,38.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgaABQAAgVAIgLQAHgLANAAQAJAAAHAGQAGAFACALIgPAFQgCgNgIAAQgEAAgDAFQgDAFAAASQAFgKAJAAQAJAAAGAHQAHAGAAAMQAAAOgIAHQgHAHgLAAQgbAAAAgqgAgIAQQAAAHACADQADADADAAQAJAAAAgOQAAgNgJAAQgIAAAAAOg");
	this.shape_13.setTransform(-0.4,44.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgSAkQgHgHgCgKIARgEQABAMAJAAQAKAAAAgOQAAgFgDgEQgDgEgEABQgGAAgDAGIgOgDIADgtIArAAIgCAQIgcAAIgBATQAFgIAIAAQAJABAHAGQAHAHAAAMQAAAPgIAIQgIAHgMAAQgLAAgHgGg");
	this.shape_14.setTransform(20.5,38.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AABAqIAAgTIgdAAIAAgPIAcgxIATAAIAAAxIAJAAIAAAPIgJAAIAAATgAgOAJIARAAIAAggg");
	this.shape_15.setTransform(37.2,23.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgRAlQgIgGgCgNIAQgDQABANAKAAQAEAAACgDQADgDAAgGQAAgLgMAAIgFAAIAAgMIAFAAIAGgBQACgBABgCQACgDAAgEQAAgKgIAAQgGAAgDALIgQgDQAHgWATAAQAMAAAGAGQAHAGAAALQAAANgMAFQANADAAAQQABAMgIAGQgIAHgMAAQgJAAgIgGg");
	this.shape_16.setTransform(43,2.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgaAqIAAgQQAXgUAFgGQAFgJAAgHQAAgKgHAAQgEAAgCAEQgDAEgBAIIgQgEQAEgbAXAAQAMAAAHAHQAGAHAAAKQAAALgHAIQgHAKgTAOIAiAAIgBAQg");
	this.shape_17.setTransform(37.4,-21.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_18.setTransform(22.6,-37.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgaAqIAAgQQAXgTAEgHQAGgIAAgIQAAgKgHAAQgFAAgCAEQgCAEAAAHIgRgDQAEgbAWAAQANAAAGAHQAHAHAAAKQAAAKgHAJQgIALgTANIAjAAIgBAQg");
	this.shape_19.setTransform(4,-43);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_20.setTransform(-1.9,-43);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF9900").s().p("AmCHrQgcgXgagZIgBAAQhMhNgrhYQg/h9AAiYQAAkCC3i4QC3i3ECAAQEBAAC3C3QC3C4AAECQAAECi1C4IgCAAQi3C2kBAAQjdAAimiGgAmbmaQiqCqAADxQAACYBFB9QApBIA8A9QAPAQASAQQAfAZAgAVQCKBcCxAAQDZAACiiKQARgQAOgQQCsiqAAjwQAAjxisiqQiqirjwABQjxgBiqCrg");
	this.shape_21.setTransform(0.2,0.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF6600").s().p("Ak7HpQgggVgfgZQgSgQgPgQQg8g9gphIQhFh9AAiYQAAjxCqiqQCqirDxABQDwgBCqCrQCsCqAADxQAADwisCqQgOAQgRAQQiiCKjZAAQixAAiKhcgAiAoGQiGAehpBkIgLALQhkBjglCCQgUBHAABOQAABBAPA+QASBSAsBEQAfA1AsAsIAFAFQAdAdAiAZIALAJQBLA0BYAWQBDASBKABQBKgBBDgSQBWgWBNg0QAlgbAjgjIADgBQCdieAAjdQAAhMgThDQgkiEhmhnQhmhniCgjQhFgVhOABQhCAAg+ARg");
	this.shape_22.setTransform(0.2,0.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AiGHwQhEgRg9glQgbgQgYgVQgYgSgZgYIgCgDQgngngcgrQgvhIgThTQgPg6AAhBQAAhMAUhEQAih6BghhIAKgJQBlhiCDgeQA5gOBCAAQBLAABDATQB7AjBgBhIABACQBjBiAiB+QARBBAABIQAABGgPBAQgiCAhmBlIAAACQguAugzAfQg9AlhFARQhBAThFAAQhHAAhBgTg");
	this.shape_23.setTransform(0.1,0.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CC3333").s().p("AiNIGQhYgXhKg0IgMgJQghgZgdgdIgGgEQgrgtggg0QgshFgRhSQgQg+AAhBQAAhOAUhHQAliCBlhjIAKgKQBphlCGgeQA+gRBDAAQBOAABEAVQCCAjBmBmQBnBoAjCDQATBEAABLQAADeidCdIgDABQgjAkgkAaQhNA0hXAXQhDAShJAAQhLAAhDgSgAh6nzQiDAdhlBjIgKAJQhgBhgiB6QgUBDgBBMQAABBAPA7QAUBTAuBIQAdArAnAmIACADQAYAZAZARQAYAVAbARQA9AkBEASQBAASBIAAQBFAABBgSQBFgSA9gkQAyggAvgtIAAgDQBmhkAiiAQAPhAAAhHQAAhHgRhBQgih/hjhhIgBgCQhghih7giQhEgThKAAQhCAAg5AOg");
	this.shape_24.setTransform(0.2,0.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#993333").s().p("ABjEKIkjnlIBPguIEzITg");
	this.shape_25.setTransform(35,60.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#993333").s().p("AjBEKIEzoTIBQAuIklHlg");
	this.shape_26.setTransform(-32.3,60.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy9, new cjs.Rectangle(-71.6,-87,143.2,174.1), null);


(lib.Symbol2copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiWLTQgdgcAAgpQAAgoAdgdQAegeApAAQAoAAAdAeQAdAdgBAoQABApgdAcQgdAegoAAQgpAAgegegAmbF5QgdgcAAgpQAAgoAdgeQAegdAoAAQApAAAdAdQAeAeAAAoQAAApgeAcQgdAdgpAAQgoAAgegdgAHVFBQgcgXAAgoQAAgpAcgkQAfgkAogKQApgKAcAWQAfAXgBAoQABAogfAlQgcAkgpAJQgOADgMAAQgYAAgVgOgApiAjQgcgegBgnQABgpAcgeQAfgdAnAAQApAAAdAdQAcAeAAApQAAAngcAeQgdAcgpAAQgnAAgfgcgABQiEQgdgcAAgpQAAgpAdgfQAdgbApgBQAoABAeAbQAeAfAAApQAAApgeAcQgeAcgoAAQgpAAgdgcgAjplbQgegdABgpQgBgpAegdQAcgdApgBQApABAcAdQAeAdABApQgBApgeAdQgcAdgpgBQgpABgcgdgAFjlxQgdgeAAgoQAAgpAdgeQAdgdApAAQAoAAAdAdQAeAeAAApQAAAogeAeQgdAbgoAAQgpAAgdgbgAAupIQgcgdAAgpQAAgnAcggQAegbApAAQAoAAAeAbQAdAgAAAnQAAApgdAdQgeAdgoAAQgpAAgegdg");
	this.shape.setTransform(-9.3,-1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A6A6A6").s().p("AgHgBIACgCIADgBIAKAHIgFABg");
	this.shape_1.setTransform(-74,-37.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FE4E4E","#FF0000"],[0.349,1],-20.5,26.6,36.1,-27.1).s().p("Al7CHQgcgZgDgUQgDgWABgTIApqKILmHDQASASAGANQAPAZADArIhJKNg");
	this.shape_2.setTransform(-38.8,29.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FEA3A3").s().p("AFGH3QgQgMgIgKQgJgMABgDQADgJAIAAQAMAUAdAGQAcABAPgGQALgJACAHQABACgBAFIgMAJQgLAHgSAIQgIAEgHAAQgLAAgJgIgAlOBVQgGgBgFgDIgEgCQgWgKgNgKQgegWAUgXQAkguCShtIAigZQAbgUBOhBQBJg5C4iAIBPg0IAPgIQAwgfgwAnIgBADIgBAAQgIAFgIAJQhwBdhDA2QhAAyhMA7QhWBAgXAVQgaARgSAPQghAYgQAPIgxAlQgZAQgKAeIgDAJIAPAQIgPgNQADAIAHAJQAJAPAAACIgBAKQAFAEAJADQAOAJgDAAQgDAAgagLg");
	this.shape_3.setTransform(-28,-40);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#FE5656","#FF0000"],[0.349,0.961],-35.9,-0.2,35.9,-0.2).s().p("AlnJPIA8p1QADgaALgWQALgVAMgKII0nyQAqgiAHAOQAFAQAEAHIgZHFQgCBLgRA1QgUA0gmAmIpLJDQgKACgJAAQgLgDAAgug");
	this.shape_4.setTransform(43.8,26);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FE5656").s().p("AgSKyQgCgHAAgYIA/rDQAAgfgVgQIsKnNQgGgIgCgLQAAgFAEgDIAEgBQAEgCAFACIAzAfIKuGUQgBADAJALQAIALAQAMQAPAMATgIQASgIAMgHIAMgKQABgFgBgCIJ/olQAWgLAFALIABAIQAAACgIALIqZI7QgQASgDAgIhCLKQgLAWgHAGIgCABQgCAAgDgGg");
	this.shape_5.setTransform(2.4,21.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FD3E3E").s().p("AAJOgQgFgCgFAAIgPgBQgIgFgIgBQgKgEgGgFIgUgMIBIqNQgDgrgOgaQgGgNgSgSIrnnBIACgsQAAgNAGgGQABgCADgEIALAHQgDADAAAFQABAKAHAIIMJHNQAWAPAAAgIg/LEQAAAXACAIQADAHAEgDQAGgFAMgXIBCrKQADghAQgSIKZo5QAHgMAAgCIAAgIIACgDIABAAQAIARAAATQAAABAAAAQABABAAAAQAAABAAABQABAAAAABQgCACAAAGIAAAIIgBAJIgBA5QgEgGgFgQQgHgPgqAjIo1HwQgMAKgLAWQgLAVgDAaIg8J2QAAAvALACQAJAAAKgCIgJAJQgPANgSAGIgEABIgQACIgGACIgKgCgArolvQgEgCgEACIgLgIIKvoMIAPgKQAdgUAdAAIACAAQAEAAADACQAFgCALAHIgtAlIABgDQAvgngvAfIgPAIIhPA0Qi4CAhKA5QhOBBgbAUIgiAZQiSBtgkAvQgUAXAeAWIgIADg");
	this.shape_6.setTransform(2.1,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FE6767","#FF0000"],[0.349,1],-65.7,16.5,76,-3).s().p("AAfHoQgSgIgJgGIqWmHIgPgRIADgJQAKgeAZgQIAxglQAQgOAhgZQASgPAagQQAXgVBWhBQBNg6BAgzQBDg2BwhdQAIgIAIgFIABgBIAugkQAAAAAAABQABAAAAABQAAAAAAAAQAAABAAAAIKWGAIAFALQAJASgDARQgBALgFAMIpUH6QgSAPgOAAQgEAAgEgBg");
	this.shape_7.setTransform(4.5,-43.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FD3535").s().p("AAHE1QgcgGgNgUQgHAAgDAJIqvmTIAIgDQANAKAVAKIAFACQAFADAFABQAxAUgfgSQgJgDgEgEIABgKQAAgCgJgPQgHgJgDgIIAPANIKXGGQAJAHARAHQAPAIAYgWIJVn6QAFgMAAgLQADgQgIgSIgFgLIA/AkQALAGAKAQIgDADQgFgLgWALIp/IlQgCgHgLAJQgMAFgVAAIgKAAg");
	this.shape_8.setTransform(5.9,-22.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy8, new cjs.Rectangle(-80.1,-92.9,160.4,186), null);


(lib.Symbol2copy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282727").s().p("AHQE8QgjAAgbgaQgYgaAAgmIAAioIDVAAIAEAAIAACoQAAAmgbAaQgXAagkAAgAn8E8QglAAgagaQgXgaAAgmIAAioIDYAAIAACoQAAAmgaAaQgYAagkAAgAjRgSQgMABgKgLQgJgIAAgNQAAgMAJgLQAKgIAMAAIGfAAQANAAAJAIQAKALAAAMQAAANgKAIQgJALgNgBgAjRiIQgMAAgKgKQgJgIAAgNQAAgMAJgKQAKgKAMAAIGfAAQANAAAJAKQAKAKAAAMQAAANgKAIQgJAKgNAAgAjRj+QgMABgKgKQgJgJAAgMQAAgNAJgKQAKgJAMAAIGfAAQANAAAJAJQAKAKAAANQAAAMgKAJQgJAKgNgBg");
	this.shape.setTransform(0.2,50.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A3737").s().p("AjfC0QhDAAAAhDIAAh7IAAg2IAAgwIABgSQAHgxA7AAIG+AAQA7AAAIAxQABAIAAAKIAAAwIAAA2IAAB7QAABDhEAAgAjiBeQgIALAAALQAAANAIAJQAKAKANAAIGfAAQANAAAIgKQALgJAAgNQAAgLgLgLQgIgJgNAAImfAAQgNAAgKAJgAjigWQgIAJAAAMQAAAMAIAJQAKAJANAAIGfAAQANAAAIgJQALgJAAgMQAAgMgLgJQgIgKgNAAImfAAQgNAAgKAKgAjiiMQgIAJAAANQAAAMAIAJQAKAKANAAIGfAAQANAAAIgKQALgJAAgMQAAgNgLgJQgIgJgNAAImfAAQgNAAgKAJg");
	this.shape_1.setTransform(-0.4,34);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AISBqQgigEgZgaQgLgMgHgMQgOgXAAgcQAAgqAggfQAfghAsAAQAsAAAfAhQAeAfAAAqQAAAcgLAXQgIAMgLAMQgaAaghAEgApZBbQgKgHgIgIQgMgMgFgMQgOgXAAgcQAAgqAfgfQAfghAsAAQArAAAhAhQAfAfAAAqQAAAcgOAXQgFAMgMAMQgSASgSAHQgRAEgRAAQgiAAgdgOg");
	this.shape_2.setTransform(-0.3,22.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DEB50A").s().p("AJ/B0QALgXAAgcQAAgrgegeQgfghgsAAQgsAAgfAhQggAeAAArQAAAcAOAXIgWAAQggglAAg2QAAgxAfgnIAKgMQAqgoA7AAQA8AAApAoQAHAGACAGQAhAnAAAxQAAA2ghAlgAnEB0QAOgXAAgcQAAgrgfgeQghghgrAAQgsAAgfAhQgfAeAAArQAAAcAOAXIgNAAQggglAAg2QAAgxAfgnIAKgMQApgoA9AAQA7AAApAoIAKAMQAfAnAAAxQAAA2gfAlg");
	this.shape_3.setTransform(-0.2,15.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FECF09").s().p("AJUDrIjVAAIr0AAIjYAAIgHAAQg2gBgggWQgwgfAAhOIAAhnICsAAIAOABIASgBIDrAAIAAB8QAABDBDAAIG/AAQBEAAAAhDIAAh8IDvAAIARABIAQgBICoAAIAABnQAABOgvAfQggAVg0ACIgEAAgAKMg1QAgglAAg2QAAgzgggmIBHAAIACAyIAGCCgAEjg1IAAgxQAAgKgCgHQgHgyg7ABIm/AAQg6gBgIAyIgBARIAAAxIiJAAQAfglAAg2QAAgzgfgmINbAAQgfAmAAAzQAAA2AgAlgArag1IAAi0IBQAAQgeAmAAAzQAAA2AgAlg");
	this.shape_4.setTransform(-0.3,33);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#212B28").s().p("AIzCXQAhgEAZgaQAMgMAHgMIAMAAIBPAAIAAA2gAEjCXIAAg2ICNAAIAVAAQAHAMAMAMQAYAaAiAEgAoOCXQALgBAMgEQASgHASgSQALgMAGgMIAWAAICJAAIAAA2gAraCXIAAg2IBSAAIAMAAQAGAMALAMQAIAIAKAHQASAMAZADgAKMhSQgDgGgGgGQgpgog8AAQg7AAgrAoIgJAMItbAAIgKgMQgpgog7AAQg8AAgpAoIgLAMIhQAAQAAgmAOgeIWcAAIADBEg");
	this.shape_5.setTransform(-0.3,17.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F7FFC2").s().p("Ak8A5QgZAAgOgPQgRgSgBgYQABgWARgRQAOgRAZAAIJ5AAQAXAAAQARQARARAAAWQAAAYgRASQgQAPgXAAg");
	this.shape_6.setTransform(1.8,-73.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#98C8F7").s().p("AmxD8QAkhQA8hIQCCiLDkhfQC4hNDdgnQAIAOAAAWIAABbIgCAAQknBSjSCRQhkBFhMBPg");
	this.shape_7.setTransform(11.6,-31.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#84C0FC").s().p("AHmELIpoAAQBNhPBjhFQDTiREnhSIABAAIAAE0QAAA7g1AIIgOAAgAnlELIgHAAQg8gFAAg+IAAmPQAAgoAYgQQARgLAaAAIPLAAQAcAAAPALQALAHAFANQjdAoi4BMQjjBgiDCKQg7BJgkBPg");
	this.shape_8.setTransform(-0.3,-33.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E0AE10").s().p("AJRGLIymAAIAAgfIADAAQABAAAAAAQAAAAABAAQAAgBAAAAQAAAAAAgBQABgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAgCgCgDIgDAAIAAn2IADAAQAAAAABgBQAAAAABAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBIgDAAIAAhjQAAiGCFAAIOiAAQCEAAAACGIAABjIgDAAIgCAEQgBABAAABQAAAAAAABQAAAAAAABQABAAAAABIAAACQAAAAAAAAQABABAAAAQAAAAABAAQABAAAAAAIACAAIAAH2IgCAAQgDADAAACQAAAAgBAAQAAABAAAAQAAABAAABQAAAAABABQAAABAAAAQAAAAABABQAAAAAAAAQABAAAAAAIADAAIAAAfIgFAAgAoTihQgYAQAAAoIAAGPQAAA+A8AFIAHAAICsAAIC4AAIJnAAIAPAAQA0gIAAg7IAAk0IAAhbQAAgWgHgOQgGgNgLgHQgPgLgcABIvLAAQgagBgRALgAlRlWQgRARAAAWQAAAZARASQAPAPAZAAIJ5AAQAXAAAQgPQARgSAAgZQAAgWgRgRQgQgRgXAAIp5AAQgZAAgPARg");
	this.shape_9.setTransform(-0.1,-42.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EEBE25").s().p("ArQE1QAbg9BeAAIABAAISmAAIAFAAQBZAAAeA9gAJVDZIgCAAQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAgBgBAAQAAgBAAgBQAAAAABgBQAAAAAAAAQAAgCAEgDIABAAQAegIAWgXQAlgmgFhMIAAgBQgVgCgOgQQgUgRAAgYIAAh6QAAgXAUgSQAPgRAZAAIAAAAIAAgPQgHgogegdQgWgZgegIIgBAAQgBAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAAAIAAgCQAAgBAAAAQgBgBAAAAQAAgBAAAAQABgBAAgBIADgEIACAAIACAAIARAGQAWAKAUAUQAfAgAHAsQABAJABAIIAAABQARADANANQARASAAAXIAAB6QAAAYgRARQgQAQgUACQAGBRgoArQgaAZghAIIgCAAgApWDZIgBAAQgggIgagZQgogrAEhRQgTgCgPgQQgRgRAAgYIAAh6QAAgXARgSQAMgNASgDIAAgBQACg1AmgoQAagbAggJIABAAIADAAQABABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAgBABQAAAAAAABQAAAAgBAAQAAABgBAAIgDAAQgcAIgXAZQgiAigDAyIABAAQAYAAAQARQATASAAAXIAAB6QAAAYgTARQgPAQgVACIAAABQgFBMAlAmQAXAXAcAIIADAAQADADAAACQAAAAAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAABgBAAQAAAAAAABQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_10.setTransform(0,-28.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy7, new cjs.Rectangle(-74.5,-82.3,149,164.6), null);


(lib.Symbol2copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330404").s().p("AAbhYIAGAAQAjAAAPAQQAGAIACAKQAEAGACAOQAEAbgDAOQgBAWgNAMQgIAJgWAHIgBAAIgfALIgMADIg9AKQgYADgXAFQA5hOBEhjg");
	this.shape.setTransform(89.2,-5.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F0909").s().p("AiMCoQhLgIhKgLQhwgThtgZIg5gNIgGAAQiMgkiNguIgvgPQgBgBAAAAQAAAAgBAAQAAgBAAAAQgBAAAAAAQgEgCgEgBQgFgBgDgCQgHgEgFgGQgFgHgDgQIgKg0QgFgbABgZQAAgXAQgLQAPgKATAGIADABIABAAIAnARIAHABIBkAqQBbAcBaAYIAGABIA5AOQAHADAGAAIApAJIABACQBTASBTAPQBLANBKAIQEsApElgPIBEgEIAygEIBQgJIAhgEQBAgIBAgKIAagFIAngIQAjgHAbgDIABAAIAQgBQhFBkg6BOIgRABQhAAKhAAHQgRABgPADQgoAEgpADQgZADgZABIhEAFQhdAEhdAAQjOAAjJgXg");
	this.shape_1.setTransform(-3.6,-11.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#56ACEC").s().p("AEVA3QARgXASgaIAFgJQAQgXAEgSQAEADADAEQASAQAAAXQAAAYgSAQQgNANgTAEIgKAAQgOAAgLgEgAhFAqQAygpAZgvQAGAEAFAFQARAQAAAXQAAAYgRAQQgRARgaAAQgZAAgSgRgAlpAnQAJgLAKgPQAWgdAIgTIANgXIAFAEQASARAAAXQAAAYgSARQgSAQgbAAQgLAAgLgEg");
	this.shape_2.setTransform(11.4,-22.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#66B4EE").s().p("AEiAyQgUgQAAgYQAAgXAUgQQASgRAaAAIAKAAQANACAMAIQgEASgQAWIgFAKQgSAagRAXQgLgEgIgJgAg4AKQAAgXATgQQASgRAYAAQASAAAPAIQgaAvgxApQgTgQAAgYgAlcAjQgUgRABgYQgBgXAUgRQARgQAYAAQAWAAASAMIgNAXQgIATgWAdQgKAPgJALQgLgEgIgIg");
	this.shape_3.setTransform(8.2,-23.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9C5050").s().p("AHEA/Ig1gHQi7gbi/gFIgegBQjDgGhgADQhmADg6ATQAAgFACgEQACgWANgUIAPgPQASgSAXgKQAagMAigEQAEAAADgCIAjAAQAQgCATACIESAJIDUAJIAWABIA3ADIAkADIAcADQANABAJAEQAKAFAJAIIAIAKQASAXAOAxIADAOQgEgGgFgDg");
	this.shape_4.setTransform(13.6,-39.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6D0A0A").s().p("AiXJRQCph7CljGQA0gIA0gKIBBgMIA+gMIArgIQgKBQghBLQgoBdhJBWIgOAEQg/APhBANIhxATIgxAHQgiAEghADQhIAJhHAFIA+gqgAnIoPQA3gzAzg4QAjAHAXAJIAPAFIgBAEQgBAQgCANIgCANIAAAEIgHApIgFAaQhPgShSgNg");
	this.shape_5.setTransform(48.8,-8.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#850F0F").s().p("AguJyQg7gHg5gKIghgFQhwgXhtgeIg/gTQhdgehbgoQg7hWgmhYQgbhCgOhDQAVAJAUAKQCFA4CUAlQADAAADACIA5ANQBpAWB0ANIAhADQA5AGA7ADQCzALDGgMQBqgFBugMIBEgHQAZgFAZgCIBxgRIAYgDQimDGipB7Ig/AqIhJACIhVACQiVAAiPgSgAH2g4QgMgFgKgKIgDgCQgZgZABgiQAAgkAbgaQAagaAoAAQAEAAAGADQAgABAYAWQAbAaAAAkQAAAkgbAZQgbAagnAAQgZAAgTgLgAH2irQgTAPgBAYQABAYATAQQAIAJALAEQAMAFANAAIAKgBQATgEAOgNQARgQABgYQgBgYgRgPQgDgFgFgCQgLgIgOgCIgKAAQgaAAgSARgACYhHIgCgCIgBAAQgZgZAAgiQAAgkAcgaQAbgaAoAAQAYAAAVALIAUAPQAbAaAAAkQABAigZAZIgDACQgbAagmAAQgoAAgbgagACvirQgTAPAAAYQAAAYATAQQASASAZAAQAZAAASgSQASgQAAgYQAAgYgSgPQgEgGgHgEQgOgHgSAAQgZAAgSARgAiLhIQAAAAAAAAQAAAAAAAAQAAAAAAAAQAAgBAAAAQgLgGgJgHQgbgaAAglQAAgjAbgaQAbgZAmAAQAoAAAbAZQAbAaAAAjQAAAlgbAaQgJAHgKAGQgVAMgbAAQgYAAgVgLgAiIi8QgTARAAAXQAAAYATARQAJAIAKAEQAMAEALAAQAaAAATgQQASgRAAgYQAAgXgSgRIgGgFQgSgLgVAAQgZAAgRAQgAgeooIAAgGIAFgSIADgUQACgEAAgEIAGgaQATgHAcgBQBvgNB1AVIAGABQgyA4g4AzIgfgEQhAgJgtAEQgeACgbAIQACgPAEgQg");
	this.shape_6.setTransform(-13,-9.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E2E2E2").s().p("AjVMPIgOgEIgUgHQgigMghgOQgygXgugdIg3gkIgDgEQgfgXgfgbQgdgagbgeQhJhQg4hSQBcAoBdAeIA+ATQBtAeBwAXIAiAFQA4AKA8AHQC3AXDCgHIBJgCQgkAXglAVQiVBTiuA5QhJAXhMASIgVgGgAhhD9Qg8gDg4gGIgigDQh0gNhpgWIg5gNQgDgCgCAAQiUgliFg4QgUgKgWgJIgGgYIgDgRQgFgjgCgjQgDgpADgmQADAAAEADQABAAAAAAQABAAAAAAQAAABABAAQAAAAAAAAIAwAQQCMAuCNAlIAFAAIA5ALQBuAaBvASQBLALBLAJQEjAhEugPIBDgFQAZgBAZgCQApgDAogFQAQgCAQgBQBBgIA/gKIASgBQg9BPguA5IgPASIgYADIhxARQgZACgZAFIhDAHQhvAMhpAFQhoAHhiAAQhZAAhWgGgAhhhcQhKgJhMgOQhTgOhTgTIACgBQAbhNAdgkQAcgfAegVQAYgQAagKIACAAQA5gTBmgDQBggDDDAGIAeABQDAAFC6AbIA1AHQAGADADAGQASAZAABNIABB9IhRAJIgyADIhDAFQhPAEhPAAQjYAAjbgegAGskOQgbAaAAAkQAAAiAYAZIADACQAKAKANAFQASALAaAAQAmAAAbgaQAbgZAAgkQAAgkgbgaQgYgWgfgBQgHgDgDAAQgpAAgaAagABkkOQgbAaAAAkQAAAiAZAZIAAAAIACACQAbAaAoAAQAnAAAagaIADgCQAZgZAAgiQAAgkgcgaIgUgPQgVgLgYAAQgoAAgbAagAjTkeQgbAaAAAjQAAAlAbAaQAJAHALAGQAAAAAAABQAAAAAAAAQAAAAAAAAQABAAAAAAQAUALAYAAQAbAAAWgMQAJgGAJgHQAcgaAAglQAAgjgcgaQgagZgpAAQgmAAgbAZgAhsncIAFgeQACgKAAgLIAEgUIAGggIADgSQAbgIAfgCQAsgEBBAJIAfAEQg+A5hFAzIgyAlIgiAAQgDACgFAAIAFgZgADRrCQh0gVhuANQgdABgUAHIADgLQADgNAEgMIADgFQAEgLAHgIQASgUAagCQAYgBAYABQAOAAAOACIAcABQAOADAOABIA3AIIAzAJIAYAGIgwA1IgHgBg");
	this.shape_7.setTransform(-7.9,-1.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#D4C5C5").s().p("AoEMJQgcgEgcgFIgmgKQBMgSBIgXQCwg5CUhTQAmgVAjgXQBHgGBHgIQAigDAigFIAygGIBxgUQBAgMBAgPIAOgEIgOAQQgiAngpAmQgZAWgcAWIgzAkIg+AkQgzAdg7AXIgIACQguARgyAOQgsALgtAJIgeAEQhUAOhbADIgnABQhVAAhPgNgAFbCYQAug5A8hPQAYgFAYgDIA+gJIAMgDIAfgLIABAAQAEAaAAAfQAAAngFAnIgrAIIg+AMIhBAMQg0AKg0AIIAPgSgAnGnkQgTgCgQACIAyglQBGgzA9g5QBTAMBOATIgDAUIgBAFIgIAkIgEAKQgGAYgIAXIgCAFgAiPrQQgXgJgkgHIAxg1IAMAGQAHAEAFAGIAAAEQAFALgDAhQgCAFAAAFIgOgFg");
	this.shape_8.setTransform(34,1.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D71B2B").s().p("AhIAzQgwAAgVgFIADg8IAKAEQBJAVA/AAIAZgBQAogEAYgPQAWgNAKgWIABAAQAJgDADgEQgPAagZAUQgbAWgjASIgHADQghAOhDAAIgFgBg");
	this.shape_9.setTransform(59.7,-62.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E63544").s().p("Ah7AvIgLgEQABgsACgsIAAgNQAQgIAJgCQALAAALAEQANADAbAQQAoAXAqAPQAiALAiAIQAQADAKgCIADAAIgCAAQgKAWgVANQgZAQgnAEIgaABQg/AAhIgWg");
	this.shape_10.setTransform(59.2,-68.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D1D7D7").s().p("AgWChIgVgBQARiCARhNIAHglQAUhWASAHQAEAJACAOIACAIIAAANQgCAsgBAtIgCA8IgHCGIg2gDg");
	this.shape_11.setTransform(41.6,-61.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy6, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Tween5copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy9();
	this.instance.parent = this;
	this.instance.setTransform(317.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(107.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-102.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2copy12();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-312.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy8();
	this.instance_4.parent = this;
	this.instance_4.setTransform(107.4,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy7();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-312.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Tween5copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy9();
	this.instance.parent = this;
	this.instance.setTransform(317.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(107.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-102.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2copy12();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-312.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy8();
	this.instance_4.parent = this;
	this.instance_4.setTransform(107.4,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy7();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-312.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Tween5copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy6();
	this.instance.parent = this;
	this.instance.setTransform(317.2,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(317.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(107.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2copy11();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-102.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-312.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy8();
	this.instance_5.parent = this;
	this.instance_5.setTransform(107.4,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_6 = new lib.Symbol2copy7();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-312.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Tween5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy6();
	this.instance.parent = this;
	this.instance.setTransform(317.2,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(317.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(107.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2copy11();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-102.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-312.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy8();
	this.instance_5.parent = this;
	this.instance_5.setTransform(107.4,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_6 = new lib.Symbol2copy7();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-312.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy6();
	this.instance.parent = this;
	this.instance.setTransform(317.2,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(317.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(107.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2copy11();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-102.4,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-312.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy8();
	this.instance_5.parent = this;
	this.instance_5.setTransform(107.4,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_6 = new lib.Symbol2copy6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-102.4,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_7 = new lib.Symbol2copy7();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-312.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(150.4,36.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,303.2,73.5);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol2copy20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.1,scaleY:1.1},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.1,-80.5,198.3,161.1);


(lib.Symbol2copy19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.1,scaleY:1.1},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.1,-80.5,198.3,161.1);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy2, new cjs.Rectangle(-99.1,-80.5,198.3,161.1), null);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(-99.1,-80.5,198.3,161.1), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol1("synched",7);
	this.instance.parent = this;
	this.instance.setTransform(152.8,155.1,2.5,2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(2.6,59.6,304,196), null);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(959.5,586.6,2.5,2.5);
	this.instance._off = true;

	this.instance_1 = new lib.Symbol7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(959.5,587.3,1,1,0,0,0,152.8,155.8);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},303).to({state:[{t:this.instance}]},59).to({state:[{t:this.instance_1}]},7).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(303).to({_off:false},0).wait(59).to({startPosition:0},0).to({_off:true,regX:152.8,regY:155.8,scaleX:1,scaleY:1,y:587.3,alpha:0,mode:"independent"},7).wait(1));

	// Layer_13
	this.instance_2 = new lib.fxSymbol1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(537.4,336.5,2.5,2.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(210).to({_off:false},0).to({_off:true},23).wait(137));

	// Layer_7
	this.instance_3 = new lib.arrowanim("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(543.2,203.7,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(130).to({_off:false},0).to({_off:true},36).wait(67).to({_off:false,x:959.2,y:451.7},0).to({_off:true},31).wait(106));

	// Layer_6
	this.instance_4 = new lib.handanim("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(446.2,405.5,1,1,0,0,0,41,60.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({_off:false},0).to({_off:true},36).wait(62).to({_off:false,x:862.2,y:653.5},0).to({_off:true},30).wait(76));

	// Layer_8
	this.instance_5 = new lib.Symbol2copy20("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(957.2,572.2,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(89).to({_off:false},0).to({_off:true},41).wait(240));

	// Layer_3
	this.instance_6 = new lib.Symbol2copy19("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(537.6,328.5,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(89).to({_off:false},0).to({_off:true},41).wait(240));

	// Layer_2
	this.instance_7 = new lib.Symbol4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(768.3,129.2,1,1,0,0,0,150.1,36.8);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(63).to({_off:false},0).wait(60).to({startPosition:19},0).to({alpha:0,startPosition:25},6).to({_off:true},1).wait(240));

	// Layer_11
	this.instance_8 = new lib.Symbol2copy20();
	this.instance_8.parent = this;
	this.instance_8.setTransform(957.2,572.2,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_8._off = true;

	this.instance_9 = new lib.Symbol2copy2();
	this.instance_9.parent = this;
	this.instance_9.setTransform(957.2,572.1,1.034,1.034,0,0,0,3.2,-0.8);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(294).to({_off:false},0).to({_off:true,regX:3.2,regY:-0.8,scaleX:1.03,scaleY:1.03,y:572.1},19).wait(57));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(294).to({_off:false},19).wait(49).to({alpha:0},7).wait(1));

	// Layer_10
	this.instance_10 = new lib.Symbol2copy19();
	this.instance_10.parent = this;
	this.instance_10.setTransform(537.6,328.5,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_10._off = true;

	this.instance_11 = new lib.Symbol2copy();
	this.instance_11.parent = this;
	this.instance_11.setTransform(537.7,328.5,0.985,0.985,0,0,0,3.3,-0.9);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(202).to({_off:false},0).to({_off:true,regX:3.3,scaleX:0.99,scaleY:0.99,x:537.7},21).wait(147));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(202).to({_off:false},21).wait(139).to({alpha:0},7).wait(1));

	// Layer_1
	this.instance_12 = new lib.Tween5("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,451.1);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_13 = new lib.Tween5copy("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,451.1);

	this.instance_14 = new lib.Tween5copy2("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,451.1);

	this.instance_15 = new lib.Tween5copy3("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(640,451.1);

	this.instance_16 = new lib.Tween5copy4("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(640,451.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},46).to({state:[{t:this.instance_12}]},9).to({state:[{t:this.instance_13}]},147).to({state:[{t:this.instance_14}]},21).to({state:[{t:this.instance_15}]},71).to({state:[{t:this.instance_16}]},19).to({state:[{t:this.instance_12}]},49).to({state:[{t:this.instance_12}]},7).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(46).to({_off:false},0).to({alpha:1},9).to({_off:true},147).wait(160).to({_off:false},0).to({alpha:0},7).wait(1));

	// Layer_5
	this.instance_17 = new lib.Tween2("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(640,136.1);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(20).to({_off:false},0).to({alpha:1},9).to({startPosition:0},333).to({alpha:0},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(593.8,320.8,1370,778);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;