(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgVA9IgnAmIgCBGIgyADIAMlmIA0ABIgJDbIB5h4IAjAcIhWBVIBkCCIgpAeg");
	this.shape.setTransform(333.7,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgqB9QgZgKgTgSQgTgSgLgZQgMgYAAgeQAAgQAFgQQAFgSAIgPQAJgPANgNQAMgMAPgJQAPgKASgFQARgGATAAQAUABASAFQATAFAPAIQAQAJAMAOQAMAOAJAQIAAABIgmAXIgBgBQgGgMgIgJQgJgJgLgHQgKgHgNgDQgMgEgNAAQgSAAgRAHQgRAIgMAMQgMANgIAQQgHASAAARQAAAUAHAQQAIARAMANQAMAMARAHQARAIASAAQAMAAAMgEQALgDALgGQAKgGAJgIQAIgJAGgKIABgCIApAXIgBABQgJAPgNANQgNAMgPAJQgQAJgSAEQgSAFgSgBQgZAAgZgKg");
	this.shape_1.setTransform(305,6.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("ABICFIAEghQgdASgbAIQgbAIgXAAQgRAAgQgEQgPgFgMgIQgMgJgIgOQgHgOAAgTQAAgTAGgPQAFgOAKgMQAKgKANgJQANgHAQgFQAPgGAQgCQAQgCARAAIAcABIAYACIgIgXQgEgKgHgKQgHgIgKgGQgKgGgOAAQgJAAgKADQgMADgOAGQgOAIgRAMQgQAMgTATIgcgiQAWgVAVgOQAUgNASgHQASgIAQgCQAOgDANAAQAVgBARAIQAQAHANANQANAMAJASQAIARAGATQAFAUADAWQACAUAAAUQAAAXgCAaQgDAZgFAfgAgKAAQgUADgOAKQgOAJgHANQgIAOAFARQADAOAJAGQAKAFANAAQANAAAQgEIAegLQAQgHAOgIIAXgNIAAgXQABgLgCgNQgLgCgNgCQgMgCgNgBQgVABgSAFg");
	this.shape_2.setTransform(274.9,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ag4ClQgZgLgQgUQgRgUgJgcQgJgbAAghIAwgFQAAAVADAQQADAQAGALQAGAMAHAHQAIAIAIAEQATALAYAAQASgBANgGQAOgFAKgJQAJgIAHgLQAGgLAEgMQADgMACgNQABgNAAgOIgBgfIgBgoIgBgpIgCgkIgDgaIgCgKIAzgBIABALIACAdIAAAoIABAvIABAuIAAAoQAAAdgKAYQgKAYgSARQgRARgZALQgXAKgdACIgJAAQgbAAgVgJg");
	this.shape_3.setTransform(243.5,1.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AghCDIgUgEIgVgHQgLgEgKgGQgLgGgKgIIAXgjIAaANQANAHAPAFQANAEAPADQAOACAPAAQANABAJgDQAJgDAFgDQAGgEADgEQAEgFABgEQABgDgBgFQABgEgCgGQgDgEgFgFQgEgEgHgEQgIgDgKgCQgMgDgOgBQgUAAgUgEQgTgEgPgGQgPgIgKgKQgKgMgCgSQgCgQAEgPQAEgNAIgMQAIgKALgJQALgHAOgGQANgGAPgCQAPgDAOAAIAWABIAaAEQANADAOAFQAOAFALAIIgPArQgPgIgPgEIgYgHQgNgDgLgCQgjgBgUAJQgVAKAAAUQAAAOAIAHQAHAFAOAEQANACARABQARABAUADQAXADAQAIQAPAGAKAJQAKAJAEALQAEAMAAAMQAAAWgJAQQgJAOgPAJQgPAKgTAEQgUAFgVABQgUAAgWgFg");
	this.shape_4.setTransform(203.3,6.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("ABICFIAEghQgdASgbAIQgbAIgXAAQgRAAgQgEQgPgFgMgIQgMgJgIgOQgHgOAAgTQAAgTAGgPQAFgOAKgMQAKgKANgJQANgHAQgFQAPgGAQgCQAQgCARAAIAcABIAYACIgIgXQgEgKgHgKQgHgIgKgGQgKgGgOAAQgJAAgKADQgMADgOAGQgOAIgRAMQgQAMgTATIgcgiQAWgVAVgOQAUgNASgHQASgIAQgCQAOgDANAAQAVgBARAIQAQAHANANQANAMAJASQAIARAGATQAFAUADAWQACAUAAAUQAAAXgCAaQgDAZgFAfgAgKAAQgUADgOAKQgOAJgHANQgIAOAFARQADAOAJAGQAKAFANAAQANAAAQgEIAegLQAQgHAOgIIAXgNIAAgXQABgLgCgNQgLgCgNgCQgMgCgNgBQgVABgSAFg");
	this.shape_5.setTransform(174.7,5.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AA+CxQAHgXADgVQADgVABgRQABgTAAgSQgCgZgHgRQgHgQgLgMQgLgLgNgFQgNgEgNAAQgMABgNAGQgMAGgNAMQgOAKgLAVIgBCbIguABIgElmIA2gCIgCCNQANgNAOgIQAPgIAMgFQAPgEANgCQAaAAAVAKQAWAJAPARQAQASAJAZQAJAYABAfIgBAiIgBAhIgEAgQgCAPgEAMg");
	this.shape_6.setTransform(145.1,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgbgbIhRACIABgrIBRgCIABhlIAtgDIgBBnIBagCIgEArIhWACIgCDKIgxACg");
	this.shape_7.setTransform(105.5,1.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("ABICFIAEghQgdASgbAIQgbAIgXAAQgRAAgQgEQgPgFgMgIQgMgJgIgOQgHgOAAgTQAAgTAGgPQAFgOAKgMQAKgKANgJQANgHAQgFQAPgGAQgCQAQgCARAAIAcABIAYACIgIgXQgEgKgHgKQgHgIgKgGQgKgGgOAAQgJAAgKADQgMADgOAGQgOAIgRAMQgQAMgTATIgcgiQAWgVAVgOQAUgNASgHQASgIAQgCQAOgDANAAQAVgBARAIQAQAHANANQANAMAJASQAIARAGATQAFAUADAWQACAUAAAUQAAAXgCAaQgDAZgFAfgAgKAAQgUADgOAKQgOAJgHANQgIAOAFARQADAOAJAGQAKAFANAAQANAAAQgEIAegLQAQgHAOgIIAXgNIAAgXQABgLgCgNQgLgCgNgCQgMgCgNgBQgVABgSAFg");
	this.shape_8.setTransform(78.5,5.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AA+CxQAHgXADgVQADgVABgRQABgTAAgSQgCgZgHgRQgHgQgLgMQgLgLgNgFQgNgEgNAAQgMABgNAGQgMAGgNAMQgOAKgLAVIgBCbIguABIgElmIA2gCIgCCNQANgNAOgIQAPgIAMgFQAPgEANgCQAaAAAVAKQAWAJAPARQAQASAJAZQAJAYABAfIgBAiIgBAhIgEAgQgCAPgEAMg");
	this.shape_9.setTransform(49,0.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgcgbIhQACIABgrIBQgCIAChlIAtgDIgBBnIBagCIgDArIhYACIgCDKIgvACg");
	this.shape_10.setTransform(21.8,1.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AACAjIhFBaIgxgNIBZhxIhchwIAxgMIBHBZIBGhaIAsARIhVBtIBaBtIgrARg");
	this.shape_11.setTransform(-15,6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgwB/QgWgKgRgRQgRgTgKgZQgKgZAAgfQAAgeAKgZQAKgZARgSQARgSAWgKQAXgKAZABQAagBAWALQAXAKARATQARARAKAaQAKAZAAAcQAAAdgKAaQgKAYgRATQgRASgXALQgWAKgaAAQgZAAgXgKgAghhWQgPAIgJAOQgJANgEASQgEARAAAQQAAAQAFARQAEASAJANQAKANAOAJQAOAJASAAQATAAAPgJQAOgJAJgNQAKgNAEgSQAFgRAAgQQAAgQgFgRQgEgSgJgNQgJgOgOgIQgPgJgUAAQgTAAgOAJg");
	this.shape_12.setTransform(-42,6.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgVCyQgNgEgQgGQgPgIgPgNIAAAeIgsABIgDlmIAygCIgBCKQAOgNAOgHQAPgIAMgCQAOgEANgBQAQAAAQAFQAPAEAOAIQAOAJAMAMQAMALAIAOQAJAOAEAQQAEARAAASQgBAUgFASQgFARgJAPQgJAPgMALQgLAMgNAJQgNAHgPAFQgOAFgOAAQgNgBgPgEgAgbgdQgNAEgKAIQgKAIgHAJQgIALgEAMIAAA4QAEANAIAMQAHALAKAIQAKAHAMAEQAMADAMABQAQABAPgHQAPgGALgMQALgLAGgQQAHgQABgSQABgRgFgRQgGgPgKgMQgLgMgPgIQgPgGgRgBQgOABgNAFg");
	this.shape_13.setTransform(-70.9,0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgzB9QgYgKgRgRQgQgRgIgYQgJgXAAgeQAAgcAKgZQAKgaARgSQASgTAWgLQAYgKAaAAQAYAAAVAJQAWAKAQAQQARARAKAWQAMAXACAaIjJAdQACARAHANQAGANAKAJQALAJANAFQANAFAPAAQAMAAANgEQALgEAKgHQALgIAHgKQAIgLADgPIAtAJQgGAVgMASQgMARgQAMQgPANgUAGQgSAHgVAAQgeAAgXgJgAgShaQgMAEgLAIQgLAIgIAOQgKANgDAUICLgRIgBgEQgJgXgQgNQgPgNgXAAQgJAAgLADg");
	this.shape_14.setTransform(-113.2,6.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AA+CxQAHgXADgVQADgVABgRQABgTAAgSQgCgZgHgRQgHgQgLgMQgLgLgNgFQgNgEgNAAQgMABgNAGQgMAGgNAMQgOAKgLAVIgBCbIguABIgElmIA2gCIgCCNQANgNAOgIQAPgIAMgFQAPgEANgCQAaAAAVAKQAWAJAPARQAQASAJAZQAJAYABAfIgBAiIgBAhIgEAgQgCAPgEAMg");
	this.shape_15.setTransform(-142.9,0.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgcgbIhQACIABgrIBQgCIAChlIAugDIgCBnIBagCIgEArIhWACIgDDKIgvACg");
	this.shape_16.setTransform(-170.1,1.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AgcgbIhQACIABgrIBRgCIABhlIAugDIgCBnIBagCIgEArIhWACIgDDKIgwACg");
	this.shape_17.setTransform(-205.1,1.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgqB9QgZgKgTgSQgTgSgLgZQgMgYAAgeQAAgQAFgQQAFgSAIgPQAJgPANgNQAMgMAPgJQAPgKASgFQARgGATAAQAUABASAFQATAFAPAIQAQAJAMAOQAMAOAJAQIAAABIgmAXIgBgBQgGgMgIgJQgJgJgLgHQgKgHgNgDQgMgEgNAAQgSAAgRAHQgRAIgMAMQgMANgIAQQgHASAAARQAAAUAHAQQAIARAMANQAMAMARAHQARAIASAAQAMAAAMgEQALgDALgGQAKgGAJgIQAIgJAGgKIABgCIApAXIgBABQgJAPgNANQgNAMgPAJQgQAJgSAEQgSAFgSgBQgZAAgZgKg");
	this.shape_18.setTransform(-230.8,6.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgzB9QgYgKgRgRQgQgRgIgYQgJgXAAgeQAAgcAKgZQAKgaARgSQARgTAXgLQAYgKAaAAQAYAAAWAJQAVAKAQAQQARARAKAWQAMAXACAaIjJAdQACARAHANQAGANALAJQAKAJANAFQANAFAPAAQANAAALgEQAMgEALgHQAKgIAIgKQAHgLADgPIAtAJQgGAVgMASQgMARgQAMQgPANgUAGQgSAHgVAAQgeAAgXgJgAgThaQgLAEgLAIQgLAIgIAOQgKANgDAUICLgRIgBgEQgJgXgQgNQgPgNgXAAQgJAAgMADg");
	this.shape_19.setTransform(-259.2,6.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AAPDKQgNgFgJgIQgKgJgGgLQgHgLgFgLQgLgagDgiIAGkmIAwAAIgCBMIgCA+IgBAzIAAAmIgCA/QABAWAFASQACAHAEAGQAEAIAFAFQAGAFAHAEQAIADAKABIgGAuQgRAAgMgGg");
	this.shape_20.setTransform(-278.6,-2.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("Ag0B9QgXgKgQgRQgRgRgIgYQgJgXAAgeQAAgcAKgZQAKgaARgSQARgTAYgLQAXgKAaAAQAYAAAVAJQAWAKAQAQQARARAKAWQALAXADAaIjIAdQACARAGANQAHANAJAJQAKAJAOAFQAOAFAOAAQANAAAMgEQAMgEAJgHQALgIAHgKQAIgLAEgPIAtAJQgHAVgMASQgMARgQAMQgQANgSAGQgUAHgUAAQgeAAgYgJgAgShaQgMAEgLAIQgLAIgJAOQgJANgDAUICLgRIgBgEQgJgXgQgNQgPgNgXAAQgJAAgLADg");
	this.shape_21.setTransform(-300.6,6.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("AgeC7QgRgEgRgHQgRgHgQgJQgRgKgQgLIAigoQASAQASAJQASAJAOAFQAQAGAPABQAQABAPgDQAOgEALgGQALgHAGgJQAHgJABgLQABgIgEgHQgEgHgHgGQgHgGgKgFIgVgIIgWgGIgWgFIgcgGQgPgEgOgGQgOgGgNgKQgMgIgJgNQgKgNgEgSQgFgRABgXQACgSAGgPQAHgPAKgMQAKgMANgIQANgJAPgGQAPgFAQgDQAQgCAPAAQAVABAXAGIATAGQALADAKAGQALAEAKAHQAKAHAJAJIgaApQgHgIgJgGQgIgHgIgEQgJgFgIgDIgQgGQgSgFgRAAQgUAAgRAGQgRAHgLAKQgLAKgGAMQgGANAAAMQAAAMAHALQAHALANAJQAMAJARAHQARAGATADQARABARAFQARADAPAGQAPAHANAJQANAJAJAMQAJAMAEAPQAEAPgDASQgCARgHAMQgHANgKAJQgKAKgNAGQgMAGgOAEQgOAEgOACIgcACQgQAAgSgEg");
	this.shape_22.setTransform(-330.3,1.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FF6600").ss(5,1,1).p("Eg3CgFrMBuGAAAQBoAABIBIQBKBMAABnIAADhQAABohKBIQhIBLhoAAMhuGAAAQhoAAhJhLQhKhIAAhoIAAjhQAAhnBKhMQBJhIBoAAg");
	this.shape_23.setTransform(0.7,1.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFCC00").s().p("Eg3BAFsQhpAAhJhLQhKhIAAhoIAAjgQAAhoBKhMQBJhIBpAAMBuFAAAQBoAABIBIQBKBMAABoIAADgQAABohKBIQhIBLhoAAg");
	this.shape_24.setTransform(0.7,1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-394.1,-38.9,788.2,79.1);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgVA9IgnAmIgCBGIgyADIAMlmIA0ABIgJDbIB5h4IAjAcIhWBVIBkCCIgpAeg");
	this.shape.setTransform(333.7,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgqB9QgZgKgTgSQgTgSgLgZQgMgYAAgeQAAgQAFgQQAFgSAIgPQAJgPANgNQAMgMAPgJQAPgKASgFQARgGATAAQAUABASAFQATAFAPAIQAQAJAMAOQAMAOAJAQIAAABIgmAXIgBgBQgGgMgIgJQgJgJgLgHQgKgHgNgDQgMgEgNAAQgSAAgRAHQgRAIgMAMQgMANgIAQQgHASAAARQAAAUAHAQQAIARAMANQAMAMARAHQARAIASAAQAMAAAMgEQALgDALgGQAKgGAJgIQAIgJAGgKIABgCIApAXIgBABQgJAPgNANQgNAMgPAJQgQAJgSAEQgSAFgSgBQgZAAgZgKg");
	this.shape_1.setTransform(305,6.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("ABICFIAEghQgdASgbAIQgbAIgXAAQgRAAgQgEQgPgFgMgIQgMgJgIgOQgHgOAAgTQAAgTAGgPQAFgOAKgMQAKgKANgJQANgHAQgFQAPgGAQgCQAQgCARAAIAcABIAYACIgIgXQgEgKgHgKQgHgIgKgGQgKgGgOAAQgJAAgKADQgMADgOAGQgOAIgRAMQgQAMgTATIgcgiQAWgVAVgOQAUgNASgHQASgIAQgCQAOgDANAAQAVgBARAIQAQAHANANQANAMAJASQAIARAGATQAFAUADAWQACAUAAAUQAAAXgCAaQgDAZgFAfgAgKAAQgUADgOAKQgOAJgHANQgIAOAFARQADAOAJAGQAKAFANAAQANAAAQgEIAegLQAQgHAOgIIAXgNIAAgXQABgLgCgNQgLgCgNgCQgMgCgNgBQgVABgSAFg");
	this.shape_2.setTransform(274.9,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ag4ClQgZgLgQgUQgRgUgJgcQgJgbAAghIAwgFQAAAVADAQQADAQAGALQAGAMAHAHQAIAIAIAEQATALAYAAQASgBANgGQAOgFAKgJQAJgIAHgLQAGgLAEgMQADgMACgNQABgNAAgOIgBgfIgBgoIgBgpIgCgkIgDgaIgCgKIAzgBIABALIACAdIAAAoIABAvIABAuIAAAoQAAAdgKAYQgKAYgSARQgRARgZALQgXAKgdACIgJAAQgbAAgVgJg");
	this.shape_3.setTransform(243.5,1.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AghCDIgUgEIgVgHQgLgEgKgGQgLgGgKgIIAXgjIAaANQANAHAPAFQANAEAPADQAOACAPAAQANABAJgDQAJgDAFgDQAGgEADgEQAEgFABgEQABgDgBgFQABgEgCgGQgDgEgFgFQgEgEgHgEQgIgDgKgCQgMgDgOgBQgUAAgUgEQgTgEgPgGQgPgIgKgKQgKgMgCgSQgCgQAEgPQAEgNAIgMQAIgKALgJQALgHAOgGQANgGAPgCQAPgDAOAAIAWABIAaAEQANADAOAFQAOAFALAIIgPArQgPgIgPgEIgYgHQgNgDgLgCQgjgBgUAJQgVAKAAAUQAAAOAIAHQAHAFAOAEQANACARABQARABAUADQAXADAQAIQAPAGAKAJQAKAJAEALQAEAMAAAMQAAAWgJAQQgJAOgPAJQgPAKgTAEQgUAFgVABQgUAAgWgFg");
	this.shape_4.setTransform(203.3,6.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("ABICFIAEghQgdASgbAIQgbAIgXAAQgRAAgQgEQgPgFgMgIQgMgJgIgOQgHgOAAgTQAAgTAGgPQAFgOAKgMQAKgKANgJQANgHAQgFQAPgGAQgCQAQgCARAAIAcABIAYACIgIgXQgEgKgHgKQgHgIgKgGQgKgGgOAAQgJAAgKADQgMADgOAGQgOAIgRAMQgQAMgTATIgcgiQAWgVAVgOQAUgNASgHQASgIAQgCQAOgDANAAQAVgBARAIQAQAHANANQANAMAJASQAIARAGATQAFAUADAWQACAUAAAUQAAAXgCAaQgDAZgFAfgAgKAAQgUADgOAKQgOAJgHANQgIAOAFARQADAOAJAGQAKAFANAAQANAAAQgEIAegLQAQgHAOgIIAXgNIAAgXQABgLgCgNQgLgCgNgCQgMgCgNgBQgVABgSAFg");
	this.shape_5.setTransform(174.7,5.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AA+CxQAHgXADgVQADgVABgRQABgTAAgSQgCgZgHgRQgHgQgLgMQgLgLgNgFQgNgEgNAAQgMABgNAGQgMAGgNAMQgOAKgLAVIgBCbIguABIgElmIA2gCIgCCNQANgNAOgIQAPgIAMgFQAPgEANgCQAaAAAVAKQAWAJAPARQAQASAJAZQAJAYABAfIgBAiIgBAhIgEAgQgCAPgEAMg");
	this.shape_6.setTransform(145.1,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgbgbIhRACIABgrIBRgCIABhlIAtgDIgBBnIBagCIgEArIhWACIgCDKIgxACg");
	this.shape_7.setTransform(105.5,1.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("ABICFIAEghQgdASgbAIQgbAIgXAAQgRAAgQgEQgPgFgMgIQgMgJgIgOQgHgOAAgTQAAgTAGgPQAFgOAKgMQAKgKANgJQANgHAQgFQAPgGAQgCQAQgCARAAIAcABIAYACIgIgXQgEgKgHgKQgHgIgKgGQgKgGgOAAQgJAAgKADQgMADgOAGQgOAIgRAMQgQAMgTATIgcgiQAWgVAVgOQAUgNASgHQASgIAQgCQAOgDANAAQAVgBARAIQAQAHANANQANAMAJASQAIARAGATQAFAUADAWQACAUAAAUQAAAXgCAaQgDAZgFAfgAgKAAQgUADgOAKQgOAJgHANQgIAOAFARQADAOAJAGQAKAFANAAQANAAAQgEIAegLQAQgHAOgIIAXgNIAAgXQABgLgCgNQgLgCgNgCQgMgCgNgBQgVABgSAFg");
	this.shape_8.setTransform(78.5,5.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AA+CxQAHgXADgVQADgVABgRQABgTAAgSQgCgZgHgRQgHgQgLgMQgLgLgNgFQgNgEgNAAQgMABgNAGQgMAGgNAMQgOAKgLAVIgBCbIguABIgElmIA2gCIgCCNQANgNAOgIQAPgIAMgFQAPgEANgCQAaAAAVAKQAWAJAPARQAQASAJAZQAJAYABAfIgBAiIgBAhIgEAgQgCAPgEAMg");
	this.shape_9.setTransform(49,0.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgcgbIhQACIABgrIBQgCIAChlIAtgDIgBBnIBagCIgDArIhYACIgCDKIgvACg");
	this.shape_10.setTransform(21.8,1.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AACAjIhFBaIgxgNIBZhxIhchwIAxgMIBHBZIBGhaIAsARIhVBtIBaBtIgrARg");
	this.shape_11.setTransform(-15,6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgwB/QgWgKgRgRQgRgTgKgZQgKgZAAgfQAAgeAKgZQAKgZARgSQARgSAWgKQAXgKAZABQAagBAWALQAXAKARATQARARAKAaQAKAZAAAcQAAAdgKAaQgKAYgRATQgRASgXALQgWAKgaAAQgZAAgXgKgAghhWQgPAIgJAOQgJANgEASQgEARAAAQQAAAQAFARQAEASAJANQAKANAOAJQAOAJASAAQATAAAPgJQAOgJAJgNQAKgNAEgSQAFgRAAgQQAAgQgFgRQgEgSgJgNQgJgOgOgIQgPgJgUAAQgTAAgOAJg");
	this.shape_12.setTransform(-42,6.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgVCyQgNgEgQgGQgPgIgPgNIAAAeIgsABIgDlmIAygCIgBCKQAOgNAOgHQAPgIAMgCQAOgEANgBQAQAAAQAFQAPAEAOAIQAOAJAMAMQAMALAIAOQAJAOAEAQQAEARAAASQgBAUgFASQgFARgJAPQgJAPgMALQgLAMgNAJQgNAHgPAFQgOAFgOAAQgNgBgPgEgAgbgdQgNAEgKAIQgKAIgHAJQgIALgEAMIAAA4QAEANAIAMQAHALAKAIQAKAHAMAEQAMADAMABQAQABAPgHQAPgGALgMQALgLAGgQQAHgQABgSQABgRgFgRQgGgPgKgMQgLgMgPgIQgPgGgRgBQgOABgNAFg");
	this.shape_13.setTransform(-70.9,0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgzB9QgYgKgRgRQgQgRgIgYQgJgXAAgeQAAgcAKgZQAKgaARgSQASgTAWgLQAYgKAaAAQAYAAAVAJQAWAKAQAQQARARAKAWQAMAXACAaIjJAdQACARAHANQAGANAKAJQALAJANAFQANAFAPAAQAMAAANgEQALgEAKgHQALgIAHgKQAIgLADgPIAtAJQgGAVgMASQgMARgQAMQgPANgUAGQgSAHgVAAQgeAAgXgJgAgShaQgMAEgLAIQgLAIgIAOQgKANgDAUICLgRIgBgEQgJgXgQgNQgPgNgXAAQgJAAgLADg");
	this.shape_14.setTransform(-113.2,6.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AA+CxQAHgXADgVQADgVABgRQABgTAAgSQgCgZgHgRQgHgQgLgMQgLgLgNgFQgNgEgNAAQgMABgNAGQgMAGgNAMQgOAKgLAVIgBCbIguABIgElmIA2gCIgCCNQANgNAOgIQAPgIAMgFQAPgEANgCQAaAAAVAKQAWAJAPARQAQASAJAZQAJAYABAfIgBAiIgBAhIgEAgQgCAPgEAMg");
	this.shape_15.setTransform(-142.9,0.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgcgbIhQACIABgrIBQgCIAChlIAugDIgCBnIBagCIgEArIhWACIgDDKIgvACg");
	this.shape_16.setTransform(-170.1,1.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AgcgbIhQACIABgrIBRgCIABhlIAugDIgCBnIBagCIgEArIhWACIgDDKIgwACg");
	this.shape_17.setTransform(-205.1,1.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgqB9QgZgKgTgSQgTgSgLgZQgMgYAAgeQAAgQAFgQQAFgSAIgPQAJgPANgNQAMgMAPgJQAPgKASgFQARgGATAAQAUABASAFQATAFAPAIQAQAJAMAOQAMAOAJAQIAAABIgmAXIgBgBQgGgMgIgJQgJgJgLgHQgKgHgNgDQgMgEgNAAQgSAAgRAHQgRAIgMAMQgMANgIAQQgHASAAARQAAAUAHAQQAIARAMANQAMAMARAHQARAIASAAQAMAAAMgEQALgDALgGQAKgGAJgIQAIgJAGgKIABgCIApAXIgBABQgJAPgNANQgNAMgPAJQgQAJgSAEQgSAFgSgBQgZAAgZgKg");
	this.shape_18.setTransform(-230.8,6.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgzB9QgYgKgRgRQgQgRgIgYQgJgXAAgeQAAgcAKgZQAKgaARgSQARgTAXgLQAYgKAaAAQAYAAAWAJQAVAKAQAQQARARAKAWQAMAXACAaIjJAdQACARAHANQAGANALAJQAKAJANAFQANAFAPAAQANAAALgEQAMgEALgHQAKgIAIgKQAHgLADgPIAtAJQgGAVgMASQgMARgQAMQgPANgUAGQgSAHgVAAQgeAAgXgJgAgThaQgLAEgLAIQgLAIgIAOQgKANgDAUICLgRIgBgEQgJgXgQgNQgPgNgXAAQgJAAgMADg");
	this.shape_19.setTransform(-259.2,6.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AAPDKQgNgFgJgIQgKgJgGgLQgHgLgFgLQgLgagDgiIAGkmIAwAAIgCBMIgCA+IgBAzIAAAmIgCA/QABAWAFASQACAHAEAGQAEAIAFAFQAGAFAHAEQAIADAKABIgGAuQgRAAgMgGg");
	this.shape_20.setTransform(-278.6,-2.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("Ag0B9QgXgKgQgRQgRgRgIgYQgJgXAAgeQAAgcAKgZQAKgaARgSQARgTAYgLQAXgKAaAAQAYAAAVAJQAWAKAQAQQARARAKAWQALAXADAaIjIAdQACARAGANQAHANAJAJQAKAJAOAFQAOAFAOAAQANAAAMgEQAMgEAJgHQALgIAHgKQAIgLAEgPIAtAJQgHAVgMASQgMARgQAMQgQANgSAGQgUAHgUAAQgeAAgYgJgAgShaQgMAEgLAIQgLAIgJAOQgJANgDAUICLgRIgBgEQgJgXgQgNQgPgNgXAAQgJAAgLADg");
	this.shape_21.setTransform(-300.6,6.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("AgeC7QgRgEgRgHQgRgHgQgJQgRgKgQgLIAigoQASAQASAJQASAJAOAFQAQAGAPABQAQABAPgDQAOgEALgGQALgHAGgJQAHgJABgLQABgIgEgHQgEgHgHgGQgHgGgKgFIgVgIIgWgGIgWgFIgcgGQgPgEgOgGQgOgGgNgKQgMgIgJgNQgKgNgEgSQgFgRABgXQACgSAGgPQAHgPAKgMQAKgMANgIQANgJAPgGQAPgFAQgDQAQgCAPAAQAVABAXAGIATAGQALADAKAGQALAEAKAHQAKAHAJAJIgaApQgHgIgJgGQgIgHgIgEQgJgFgIgDIgQgGQgSgFgRAAQgUAAgRAGQgRAHgLAKQgLAKgGAMQgGANAAAMQAAAMAHALQAHALANAJQAMAJARAHQARAGATADQARABARAFQARADAPAGQAPAHANAJQANAJAJAMQAJAMAEAPQAEAPgDASQgCARgHAMQgHANgKAJQgKAKgNAGQgMAGgOAEQgOAEgOACIgcACQgQAAgSgEg");
	this.shape_22.setTransform(-330.3,1.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FF6600").ss(5,1,1).p("Eg3CgFrMBuGAAAQBoAABIBIQBKBMAABnIAADhQAABohKBIQhIBLhoAAMhuGAAAQhoAAhJhLQhKhIAAhoIAAjhQAAhnBKhMQBJhIBoAAg");
	this.shape_23.setTransform(0.7,1.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFCC00").s().p("Eg3BAFsQhpAAhJhLQhKhIAAhoIAAjgQAAhoBKhMQBJhIBpAAMBuFAAAQBoAABIBIQBKBMAABoIAADgQAABohKBIQhIBLhoAAg");
	this.shape_24.setTransform(0.7,1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-394.1,-38.9,788.2,79.1);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgTA5IgkAjIgDBCIguADIALlPIAwABIgIDNIBxhwIAhAaIhRBQIBdB5IgmAcg");
	this.shape.setTransform(192,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgnB1QgXgJgSgRQgSgRgLgXQgLgXAAgcQAAgPAFgQQAEgPAJgPQAIgOALgMQAMgMAOgIQAOgJARgFQAQgFARAAQATAAARAFQARAEAPAJQAPAJALAMQAMAMAIAQIAAABIgkAWIgBgBQgFgLgIgJQgIgJgKgGQgKgGgMgEQgLgDgNAAQgRAAgPAHQgQAHgLALQgMAMgHAQQgGAQAAAQQAAASAGAQQAHAPAMAMQALAMAQAHQAPAGARAAQAMAAALgDQALgDAJgFQAKgGAIgIQAIgIAGgJIABgCIAmAWIgBABQgIAOgNAMQgMALgOAIQgPAIgRAFQgQAEgSAAQgXAAgXgKg");
	this.shape_1.setTransform(165.2,5.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("ABEB8IADgfQgbARgaAIQgYAHgWAAQgQAAgOgDQgQgFgKgIQgMgIgGgNQgIgNABgSQAAgRAFgOQAFgOAJgLQAKgKALgHQANgIAOgFQAOgEARgDQAOgCAPAAQAPAAAMABIAWADQgCgLgFgKQgEgLgGgIQgHgJgJgFQgJgFgOAAQgIAAgKACQgLADgNAGQgNAHgQALQgPAMgRARIgbggQAVgTATgNQAUgMAQgHQARgHAPgDQANgCAMAAQATAAARAHQAPAGAMAMQAMAMAIAQQAIAQAFATQAGASACAUQACASAAAUQAAAVgDAYQgBAYgGAcgAgKgBQgSAEgOAJQgNAIgGANQgHANAEAQQADANAJAFQAIAFANAAQANAAAPgEIAcgKIAbgOIAVgMIABgWIgBgWIgWgEQgMgCgMAAQgTAAgSAEg");
	this.shape_2.setTransform(137,5.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ag0CbQgXgKgQgTQgQgTgIgaQgIgagBgeIAugFQgBAUADAPQADAOAGALQAFALAHAHQAHAHAIAEQASAKAWAAQARgBANgFQAMgFAJgIQAJgIAGgKQAGgKAEgMQADgLABgMIABgZIAAgdIgBglIgCgnIgCgiIgCgZQgBgJgBAAIAwgBIABALIABAbIABAmIABArIAAArIABAmQAAAbgKAWQgJAXgRAQQgQAQgXAKQgXAJgaACIgJAAQgZAAgTgIg");
	this.shape_3.setTransform(107.6,1.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AgfB7IgSgEIgUgHIgUgJQgKgFgJgIIAVghIAYAMQANAGANAFQANAEANADQANACAPAAQAMAAAIgCIAOgFQAFgEADgEIAEgIQABgEAAgEQAAgEgCgFIgGgIQgFgEgHgEQgHgDgKgCQgKgDgOAAQgSgBgSgDQgSgDgPgHQgOgHgJgKQgJgLgCgQQgCgQAEgNQADgNAIgKQAHgKAKgIQALgIANgFQAMgFAPgDQANgCAOAAIAUABIAYADQANADAMAFQANAFALAIIgPAoQgOgIgNgEQgNgFgKgCQgMgDgKAAQghgCgTAJQgTAJAAASQAAANAHAHQAHAFANADQAMADARAAQAPABATADQAVADAPAHQAOAGAJAIQAJAJAEALQAEAKAAAMQAAAUgIAOQgJAOgOAJQgOAJgSAEQgSAEgUABQgTAAgUgEg");
	this.shape_4.setTransform(70,6.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("ABEB8IADgfQgbARgaAIQgYAHgWAAQgQAAgPgDQgPgFgKgIQgMgIgGgNQgIgNABgSQgBgRAGgOQAFgOAJgLQAJgKAMgHQANgIAOgFQAOgEARgDQAOgCAPAAQAPAAAMABIAWADQgCgLgFgKQgEgLgGgIQgHgJgJgFQgJgFgOAAQgIAAgKACQgLADgNAGQgNAHgQALQgPAMgRARIgbggQAVgTATgNQAUgMAQgHQARgHAPgDQANgCAMAAQATAAARAHQAPAGAMAMQAMAMAIAQQAIAQAFATQAFASADAUQACASAAAUQAAAVgDAYQgBAYgGAcgAgJgBQgTAEgOAJQgNAIgGANQgHANAEAQQADANAJAFQAIAFANAAQANAAAPgEIAcgKIAbgOIAVgMIABgWIgBgWIgWgEQgMgCgMAAQgTAAgRAEg");
	this.shape_5.setTransform(43.3,5.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AA6CmQAHgVACgUIAEgjIABgjQgBgYgHgQQgHgPgKgKQgKgLgNgEQgMgFgMAAQgLABgNAGQgKAFgNALQgMAKgLAUIgBCQIgrABIgElOIAygCIgBCEQAMgNAOgHQANgIAMgEQANgEANgCQAYAAAUAJQAUAJAOAQQAPARAIAXQAJAWABAeIgBAfIgBAgIgDAdQgDAOgDALg");
	this.shape_6.setTransform(15.6,0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgagZIhLACIABgoIBLgCIAChfIAqgDIgCBhIBVgDIgDApIhSACIgBC9IgtABg");
	this.shape_7.setTransform(-21.4,1.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("ABDB8IADgfQgaARgZAIQgZAHgWAAQgQAAgPgDQgOgFgMgIQgKgIgIgNQgGgNgBgSQAAgRAGgOQAFgOAJgLQAKgKAMgHQAMgIAPgFQAOgEAPgDQAPgCAQAAQAOAAAMABIAWADQgDgLgEgKQgEgLgGgIQgHgJgJgFQgKgFgMAAQgJAAgKACQgLADgNAGQgNAHgPALQgQAMgSARIgaggQAVgTAUgNQATgMARgHQAQgHAOgDQAOgCALAAQAVAAAPAHQAQAGAMAMQAMAMAIAQQAIAQAGATQAEASADAUQACASAAAUQAAAVgCAYQgCAYgFAcgAgKgBQgSAEgNAJQgNAIgHANQgHANAEAQQADANAJAFQAJAFAMAAQANAAAOgEIAcgKIAcgOIAWgMIABgWIgBgWIgXgEQgMgCgLAAQgUAAgSAEg");
	this.shape_8.setTransform(-46.7,5.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AA6CmQAHgVACgUIAEgjIABgjQgBgYgHgQQgHgPgKgKQgKgLgNgEQgMgFgMAAQgLABgNAGQgKAFgNALQgMAKgLAUIgBCQIgrABIgElOIAygCIgBCEQAMgNAOgHQANgIAMgEQANgEANgCQAYAAAUAJQAUAJAOAQQAPARAIAXQAJAWABAeIgBAfIgBAgIgDAdQgDAOgDALg");
	this.shape_9.setTransform(-74.4,0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgZgZIhMACIABgoIBLgCIAChfIAqgDIgBBhIBUgDIgDApIhRACIgDC9IgtABg");
	this.shape_10.setTransform(-99.8,1.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AACAhIhABUIgugMIBThpIhWhpIAugLIBCBSIBBhTIAqAQIhQBlIBUBmIgoAQg");
	this.shape_11.setTransform(-134.1,5.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgsB3QgWgJgPgRQgQgRgJgYQgKgXABgdQgBgcAKgXQAJgYAQgRQAPgRAWgJQAUgJAYAAQAYAAAVAKQAWAKAPARQAQARAKAXQAIAYABAaQgBAcgIAXQgKAXgQARQgPASgWAJQgVAKgYAAQgYAAgUgJgAgfhQQgNAIgJAMQgIANgEAQQgEAQAAAPQAAAPAEAQQAEAQAJANQAJANANAIQANAIARAAQASAAANgIQAOgIAJgNQAIgNAEgQQAFgQAAgPQAAgPgFgQQgDgQgIgNQgJgMgOgIQgNgIgTAAQgSAAgNAIg");
	this.shape_12.setTransform(-159.4,5.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgTCmQgNgDgOgGQgPgHgOgNIAAAdIgoABIgElPIAwgCIgCCBQANgMAOgHQANgHAMgDQANgDAMgBQAPAAAOAFQAPAEANAIQANAHALALQALALAIANQAIAOAEAOQAEAQAAARQgBASgFARQgFAQgIAOQgIAOgLALQgLALgMAIQgNAHgNAFQgNAEgNAAQgMgBgOgEgAgZgcQgMAFgJAHQgKAIgHAIQgHAKgEALIAAA0QAEAOAHAKQAHAKAKAIQAJAHALADQALAEALAAQAPAAAOgGQAOgGAKgKQALgLAGgPQAGgOABgRQABgRgFgPQgFgPgKgKQgKgMgOgHQgOgGgQAAQgNAAgMAEg");
	this.shape_13.setTransform(-186.4,0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(3,1,1).p("A/VlFMA+rAAAQCaAAAACZIAAFZQAACZiaAAMg+rAAAQiaAAAAiZIAAlZQAAiZCaAAg");
	this.shape_14.setTransform(0,0.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("A/UFGQibAAABiZIAAlZQgBiZCbAAMA+qAAAQCZAAAACZIAAFZQAACZiZAAg");
	this.shape_15.setTransform(0,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-217.4,-36.4,434.9,72.9);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhHuAj9QhbAAhBhBQhAhAAAhbMAAAhBBQAAhbBAhAQBBhBBbAAMCPdAAAQBbAABBBBQBABAAABbMAAABBBQAABbhABAQhBBBhbAAg");
	this.shape.setTransform(0,0,1.234,1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E0D8D8").s().p("EhHuAlNQh8AAhYhYQhYhYAAh8MAAAhBBQAAh8BYhYQBYhYB8AAMCPdAAAQB8AABYBYQBYBYAAB8MAAABBBQAAB8hYBYQhYBYh8AAg");
	this.shape_1.setTransform(0,0,1.234,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-603.7,-238,1207.6,476.2);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.springcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0.1,2,0,0.1,2,10.4).ss(2,1,1).p("ABggUQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgCAMABAG");
	this.shape.setTransform(0.9,31,1,0.706,0,180,0,0.1,2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-10.7,0,10.7,0).ss(2,1,1).p("ABggRQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgBAHAAAF");
	this.shape_1.setTransform(0.6,47.8,1,0.706,0,0,0,0.1,2.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0,0,0,0,0,10.4).ss(2,1,1).p("AhagRQALAPBPANQBQAOALgO");
	this.shape_2.setTransform(1,43.1,1.004,1.915,0,-1.3,-4.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-9.9,0,9.9,0).ss(2,1,1).p("ABdAKQALgahpADQhqAEAQAX");
	this.shape_3.setTransform(1,35.4,1.029,1.908,0,-1.3,-13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0.1,2,0,0.1,2,10.4).ss(2,1,1).p("ABggUQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgCAMABAG");
	this.shape_4.setTransform(1.3,9.4,1,0.706,0,180,0,0.1,2.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-10.7,0,10.7,0).ss(2,1,1).p("ABggRQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgBAHAAAF");
	this.shape_5.setTransform(0.9,26.2,1,0.706,0,0,0,0.1,2.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0,0,0,0,0,10.4).ss(2,1,1).p("AhagRQALAPBPANQBQAOALgO");
	this.shape_6.setTransform(1.3,21.5,1.004,1.915,0,-1.3,-4.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-9.9,0,9.9,0).ss(2,1,1).p("ABdAKQALgahpADQhqAEAQAX");
	this.shape_7.setTransform(1.4,13.8,1.029,1.908,0,-1.3,-13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0.1,2,0,0.1,2,10.4).ss(2,1,1).p("ABggUQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgCAMABAG");
	this.shape_8.setTransform(1.6,-12.3,1,0.706,0,180,0,0.1,2.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-10.7,0,10.7,0).ss(2,1,1).p("ABggRQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgBAHAAAF");
	this.shape_9.setTransform(1.3,4.5,1,0.706,0,0,0,0.1,2.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0,0,0,0,0,10.4).ss(2,1,1).p("AhagRQALAPBPANQBQAOALgO");
	this.shape_10.setTransform(1.7,-0.2,1.004,1.915,0,-1.3,-4.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-9.9,0,9.9,0).ss(2,1,1).p("ABdAKQALgahpADQhqAEAQAX");
	this.shape_11.setTransform(1.7,-7.9,1.029,1.908,0,-1.3,-13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0.1,2,0,0.1,2,10.4).ss(2,1,1).p("ABggUQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgCAMABAG");
	this.shape_12.setTransform(2,-33.7,1,0.706,0,180,0,0.1,2.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-10.7,0,10.7,0).ss(2,1,1).p("ABggRQAFgZgSgLQgRgLgqAOQgqANglAdQglAdgDAZQgBAHAAAF");
	this.shape_13.setTransform(1.6,-16.9,1,0.706,0,0,0,0.1,2.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().rs(["#FFFF00","#FF9900","#CC3300"],[0.063,0.302,1],0,0,0,0,0,10.4).ss(2,1,1).p("AhagRQALAPBPANQBQAOALgO");
	this.shape_14.setTransform(2,-21.7,1.004,1.915,0,-1.3,-4.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().ls(["#660000","#CC6600","#FF6600"],[0.004,0.541,1],-9.9,0,9.9,0).ss(2,1,1).p("ABdAKQALgahpADQhqAEAQAX");
	this.shape_15.setTransform(2.1,-29.3,1.029,1.908,0,-1.3,-13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.2,-44.2,25.1,95.5);


(lib.browTween1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#351000").s().p("ADmg1IgEgCQgEgFgDgGQgGgOAGgLQAEgGALgKQAMgJAHgDIAFAGQEYgOgPD3QhfivjGACgAkBhxIAFgGQAHADAMAJQALAKAEAGQAGALgGAOQgDAGgEAFIgEACQjGgChfCvQgPj3EYAOg");
	this.shape.setTransform(3,12);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-49.4,0,104.9,23.9);


(lib.browTween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#351000").s().p("ADmg1IgEgCQgEgFgDgGQgGgOAGgLQAEgGALgKQAMgJAHgDIAFAGQEYgOgPD3QhfivjGACgAkBhxIAFgGQAHADAMAJQALAKAEAGQAGALgGAOQgDAGgEAFIgEACQjGgChfCvQgPj3EYAOg");
	this.shape.setTransform(3,12);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-49.4,0,104.9,23.9);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(603.8,238.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy, new cjs.Rectangle(0,0,1207.6,476.2), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween17("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(216,36.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03,x:215.9,y:36.4},9).to({scaleX:1,scaleY:1,x:216,y:36.5},10).to({scaleX:1.03,scaleY:1.03,x:215.9,y:36.4},10).to({scaleX:1,scaleY:1,x:216,y:36.5},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,434.9,72.9);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.jackboxRedcopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop()*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC00").s().p("A7lKQIiM0eMA7jAAAIiMUeg");
	this.shape.setTransform(10.7,421.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC6600").s().p("A9xAeIBVg7MA47AAAIBTA7g");
	this.shape_1.setTransform(10.7,352.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.instance = new lib.springcopy3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(25.7,471.5,2.968,0.385,0,0.4,1.2,5,48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,0,0,0.502)").ss(1,1,1,3,true).p("AckggIAGgBIAlAAA9OAhIAnAA");
	this.shape_2.setTransform(13,260.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC3300").s().p("A5dh6MAy7gAfIh5EZMgu7AAag");
	this.shape_3.setTransform(5.7,-22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("A4fziMAu7gAZMADhAn2Mgz5AACg");
	this.shape_4.setTransform(13,118.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("A9OX0MACGgvHMAz6gAgMAEdAukIglAAMgEVguCMgy7AAeMgCBAung");
	this.shape_5.setTransform(13,111.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FF9900","#CC6600"],[0,0.992],-14.7,-15.4,14.7,-15.4).s().p("AAaVPMgDggn2IB5kaMAEUAuCIgGABg");
	this.shape_6.setTransform(176.5,109.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FF9900","#CC6600"],[0,0.992],-13.9,-0.3,13.9,-0.3).s().p("AgC3TICGEVMgBdAneIiqC0g");
	this.shape_7.setTransform(-157,114.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#BCEA04","#87A703"],[0,0.992],-7.1,-301.9,-7.1,0.9).s().p("AgCgBIAFgBIAAAFg");
	this.shape_8.setTransform(196.1,257.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("A5lIDIAAwFMAzLAAAIAAQFg");
	this.shape_9.setTransform(13.6,300.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#FF9900","#993300"],[0,0.992],-35.6,1.6,2.3,1.6).s().p("AhRIDIAAwFICjQFg");
	this.shape_10.setTransform(185.6,300.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FF9900","#993300"],[0,0.992],-39.8,2.4,14.3,2.4).s().p("AhwIDIDhwFIAAQFg");
	this.shape_11.setTransform(-161.5,300.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF9900").s().p("AcnH8IikwFIAhgcIDORMgA5uolIAnAcIjjQFIhHArg");
	this.shape_12.setTransform(10.7,300.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("A5iAOIgngbMA0TAAAIghAbg");
	this.shape_13.setTransform(13.3,247.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(0,0,0,0.502)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_14.setTransform(13.5,453.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.2)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_15.setTransform(13.5,453.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 4
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(255,255,255,0.008)").s().p("EghjAbSMAAAg2jMBDHAAAMAAAA2jg");
	this.shape_16.setTransform(7.5,365.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(1));

}).prototype = getMCSymbolPrototype(lib.jackboxRedcopy4, new cjs.Rectangle(-207.3,-41.1,429.6,581.5), null);


(lib.jackboxRedcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop()*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC00").s().p("A7lKQIiM0eMA7jAAAIiMUeg");
	this.shape.setTransform(10.7,421.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC6600").s().p("A9xAeIBVg7MA47AAAIBTA7g");
	this.shape_1.setTransform(10.7,352.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.instance = new lib.springcopy3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(25.7,471.5,2.968,0.385,0,0.4,1.2,5,48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,0,0,0.502)").ss(1,1,1,3,true).p("AckggIAGgBIAlAAA9OAhIAnAA");
	this.shape_2.setTransform(13,260.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC3300").s().p("A5dh6MAy7gAfIh5EZMgu7AAag");
	this.shape_3.setTransform(5.7,-22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("A4fziMAu7gAZMADhAn2Mgz5AACg");
	this.shape_4.setTransform(13,118.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("A9OX0MACGgvHMAz6gAgMAEdAukIglAAMgEVguCMgy7AAeMgCBAung");
	this.shape_5.setTransform(13,111.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FF9900","#CC6600"],[0,0.992],-14.7,-15.4,14.7,-15.4).s().p("AAaVPMgDggn2IB5kaMAEUAuCIgGABg");
	this.shape_6.setTransform(176.5,109.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FF9900","#CC6600"],[0,0.992],-13.9,-0.3,13.9,-0.3).s().p("AgC3TICGEVMgBdAneIiqC0g");
	this.shape_7.setTransform(-157,114.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#BCEA04","#87A703"],[0,0.992],-7.1,-301.9,-7.1,0.9).s().p("AgCgBIAFgBIAAAFg");
	this.shape_8.setTransform(196.1,257.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("A5lIDIAAwFMAzLAAAIAAQFg");
	this.shape_9.setTransform(13.6,300.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#FF9900","#993300"],[0,0.992],-35.6,1.6,2.3,1.6).s().p("AhRIDIAAwFICjQFg");
	this.shape_10.setTransform(185.6,300.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FF9900","#993300"],[0,0.992],-39.8,2.4,14.3,2.4).s().p("AhwIDIDhwFIAAQFg");
	this.shape_11.setTransform(-161.5,300.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF9900").s().p("AcnH8IikwFIAhgcIDORMgA5uolIAnAcIjjQFIhHArg");
	this.shape_12.setTransform(10.7,300.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("A5iAOIgngbMA0TAAAIghAbg");
	this.shape_13.setTransform(13.3,247.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(0,0,0,0.502)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_14.setTransform(13.5,453.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.2)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_15.setTransform(13.5,453.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 4
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(255,255,255,0.008)").s().p("EghjAbSMAAAg2jMBDHAAAMAAAA2jg");
	this.shape_16.setTransform(7.5,365.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(1));

}).prototype = getMCSymbolPrototype(lib.jackboxRedcopy3, new cjs.Rectangle(-207.3,-41.1,429.6,581.5), null);


(lib.face_happycopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(1,1,1).p("AL6msQAIgvAjgiQAtgsA/ABQA/ABAsAsQAsAtgBBAQgBA/gtArQgsAsg/gBQg/AAgsgtQgEgEgDgDQglgrABg6QAAgOACgMQkaBJjpEfQgEAMgDAHIgFgFQgmAwgkA0AERDRQABgFACgEQAehiA6hQQAdgoAYgVIgDDqIAVABQCOgpBfgbQC8g2AHgBQgFAKhBBjQg/BdgRAuQAmAKCdBNQCNBFBYAyIhYACQg0AAhQgXQhQgWgfAAQgBAHAAANQAAAYAjBRQAoBdAKAsIAAAIQgCgDgCgCQhRhvhhhDQhGgxATgIQhGgYhHgZQigg2imgdQifgcidAjQigAkiOBQQiaBXiaBXQAAABgBAAIAAgDQAAgRgIgaQAAgHgChkQAAgBAAgCQgDAAgGADQgIAGhyBbQhwBXgMAAQAAgSAIgRQAIgRAAgeQAAhrAtg+QATgbAzgjQgDAAgygLQgygLgJgGQAYgKCsgwQCdgrADgIQgdgLhWgwQhYgygWgTQAAgBgBAAQgBgBgBgBIAGAAQA3ACAvACIAFANAKWCUIAKgUQAHkJB1ikAD3hEQgHAHgFAHAiTpZQCaALCFDaQAwBPAjBZQAdBLAAAaQAAALgEARQgBADAAAEAEODHQgmgyhJhhQgCgCgBgBQg3hHgOgPQgBAAgBAAQgBgBAAAAQAAgBAAAAQgBAAAAABQgCAUAABPQgBA4gJABQADAAAAACQgBADABABQgGAEAAABQgBAAgGAAQgBAAjvjTIgDAAQgDANAAA5QAAAlASAqQARAjAAABIgBASIAAAAQgMgBgegIQhiieh3hzIAfABQBCAABIjFQAbhIAThMQAIgjAFgYAiTpZQgHANgLALQgJAHgIAGQgZAQgfAAQgqAAgegfQgegeABgqQAAgqAfgeQAfgeAqAAQAqABAdAeQAeAfAAAqQgBAbgMAVgAtimkQgJAMgMAMQgaAZgfAKQgXAHgaAAQg9AAgrgsQgrgsAAg+QABg+AsgrQAsgrA+ABQA+ABArAsQArAsgBA9QgBAtgXAkgAmaihQgCgBgBgBQjFi8kAhFAiKCSIg3giQhAgSiSg2QjmhTgagFQAFAHBWBZQBEBHAAAaQgQAAjjgLAuwlpQDeDygVD9");
	this.shape.setTransform(3.9,-74.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("AKCDvQhHgxATgJIiNgwQigg3imgcQiegdieAjQigAkiOBQIk0CvIgBgDQAAgRgIgZIgChrIAAgEQgDAAgGAEIh6BgQhwBXgMAAQAAgRAIgSQAIgRAAgeQAAhqAtg/QATgaAzgkIg1gLQgygLgJgGQAYgKCsguQCdgsADgIQgdgLhWgwQhYgxgWgUIADgDIBmAFIDzAKQAAgZhEhIQhWhagFgHQAaAFDmBVQCSA2BAARIA3AiIg3giQAeAJAMAAIAAAAIABgSIgRgkQgSgqAAglQAAg6ADgNIADAAQDwDUABAAIAHAAIAFgFQAAAAgBgBQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQAJAAABg4QABhRACgUIAAABIABAAIACAAQAOAPA3BIIADAEIBvCSIAGACIgDAIIADgIQAfhjA5hPQAdgqAYgUIgDDrIAVAAIDthEIgKAUIAKgUIDDg3QgFAKhBBjQg/BdgRAvQAmAJCdBMQCNBGBYAxIhYACQg0AAhQgXQhQgWgfAAIgBAUQAAAYAjBSQAoBdAKArIgEADQhRhuhghDgAspi5IgFgMgADNiDg");
	this.shape_1.setTransform(11,-41.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCFF00").s().p("AgBCYQg/gBgsgtIgGgHQglgqAAg6QABgNACgMQAIgvAjgjQAtgrA+AAQA/ABArAtQAsAtgBA+QAAA/gtAsQgsArg9AAIgCAAg");
	this.shape_2.setTransform(95.1,-114.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CCFF").s().p("AgBCVQg9gBgrgsQgrgsAAg9QABg9AsgrQAsgrA9AAQA+ABArAsQArAsgBA9QgBAtgXAjQgJANgMAMQgZAZggAKQgWAHgYAAIgCAAg");
	this.shape_3.setTransform(-95.3,-125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#99CC00").s().p("AjOE5IhwiTIACgGQAlg1AlgvIAGAEQADgHADgMQDpkdEahKQgDANAAANQAAA7AlAqQh1CkgHEJIjsBEIgVgBIACjrQgXAVgeApQg5BQgfBjg");
	this.shape_4.setTransform(51.7,-86);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0099FF").s().p("AitERQAUj/jejwQAggKAagaQAMgLAIgNQEBBFDEC8IACACQB4ByBiCfQhAgRiTg2QjkhVgbgFQAFAHBWBaQBDBIAAAZIjxgKg");
	this.shape_5.setTransform(-53,-88.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF3333").s().p("AhkCZIgDAAQgCANAAA6QgBAlATAqIARAkIgBASIAAAAQgMAAgegJQhiifh3hzIAfABQBCAABIjEQAahHAUhNQAIgiAEgYQAJgGAJgIQALgLAHgNQCaALCFDaQAwBPAiBYQAdBMAAAZQABALgEASIgBAGIgNAPQglAvglA1IgEADQg3hIgPgPIgBAAIgBAAIAAgCIgBABQgCAUgBBRQAAA4gKAAQABAAABAAQABAAAAABQABAAAAAAQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABABAAIgGAFIgHAAQgBAAjwjUg");
	this.shape_6.setTransform(-4,-98.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF9900").s().p("AjwLKIABACIgBABgAlkCpIADgCIgDADIAAgBgAD+n/QgqAAgegfQgegfAAgpQABgqAegeQAfgeAqAAQAqABAeAeQAeAfgBAqQAAAbgMAVQgIANgLALQgIAHgJAGQgYAQgeAAIgBAAg");
	this.shape_7.setTransform(-45.3,-78.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// brows
	this.instance = new lib.browTween1copy2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(2.7,-20.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// lips
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgJAzQhIgBhGgVQhGgSgig8QDkBBEWhFIAEATQgzAyhCATQg0ATg4AAQgUAAgTgDg");
	this.shape_8.setTransform(2.4,62);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#990000").s().p("AiRgEQBMgfBDADIAGAAIAHAAQBHgEAlANQAPAGAMAIIgRAJQg/AihCAAQhGAAhLgmg");
	this.shape_9.setTransform(4.9,74.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("AlihlQAsAUAuAOIAIACQAiA9BFARQBHAVBIABQBLAJBIgZQBBgTA0gyIAcgcIBKgXQhZCLhhA7QgMgIgPgGQglgNhHAEIgHAAIgHAAQhCgDhMAfQhwg6h5iRg");
	this.shape_10.setTransform(2.5,63.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8}]}).wait(1));

	// eyes
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0099FF").s().p("AhMBbQgggmAAg1QAAg0AggnQANgOAPgJQAWgOAaAAQAsAAAgAlQAhAnAAA0QAAApgTAfIgOATQggAmgsAAQgsAAgggmgAAkA8QAGAIAJAAQAJAAAHgIQAGgIAAgLQAAgLgGgHQgGgHgHgBIABgPQAAgbgQgTQgGgHgIgFQgDgKgHgIQgNgPgSgBQgJAAgHAFQgIADgHAIQgMAQAAAVQAAAPAGALIgCANQAAAaAQAVQARASAWAAQARAAAPgMQABAEADADg");
	this.shape_11.setTransform(-20.3,22.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("Ah4COQgwg7AAhTQAAhSAwg7QAyg7BGAAQBGAAAyA7QAyA7gBBSQABBTgyA7QgyA7hGAAQhGAAgyg7gAgyhnQgOAJgNAPQggAmAAA0QAAA1AgAmQAfAmAtAAQAsAAAggmIANgSQAUggAAgpQAAg0ghgmQggglgsAAQgaAAgXANgAAiBHQgDgDgBgDQgCgGAAgGQAAgLAGgIQAHgHAIAAIADAAQAIABAGAGQAGAIAAALQAAAKgGAIQgHAIgKAAQgIAAgHgIgAgwAIQgEgEgDgGQgFgLAAgPQAAgVAMgQQAGgHAJgEQAGgEAKAAQASAAANAPQAHAIADAKQADAJAAAKQAAAWgNAOQgNAQgSAAQgSAAgNgQg");
	this.shape_12.setTransform(-20.1,21.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgmAsQgQgVAAgaIABgNQADAGAEAFQANAPASAAQASAAANgPQAMgPAAgWQAAgKgCgJQAHAFAGAHQAQATAAAbIgBAPIgDAAQgIAAgHAIQgHAHAAALQAAAHADAFQgOAMgSAAQgWAAgQgSg");
	this.shape_13.setTransform(-20.3,22.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgmArQgQgUAAgZQAAgIABgGQADAGAEAFQANAOASAAQASAAANgOQAMgQAAgWQAAgJgCgJQAHAFAGAHQAQATAAAcIgBAOIgDAAQgIAAgHAHQgHAJAAAKQAAAHADAGQgOALgSAAQgWAAgQgTg");
	this.shape_14.setTransform(26.7,22.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0099FF").s().p("AhMBbQgfgmgBg1QABg0AfgmQANgPAPgJQAWgOAaAAQAsAAAhAmQAfAmAAA0QAAApgSAfIgNATQghAmgsAAQgsAAgggmgAAkA8QAHAIAIAAQAKAAAGgIQAHgIgBgLQABgKgHgJQgFgFgIgCIABgPQAAgbgQgTQgGgHgHgFQgEgKgGgIQgNgQgTAAQgJAAgHAEQgIAEgGAIQgNAPAAAWQAAAPAGALQgBAHgBAGQABAbAQATQAQATAWAAQARAAAPgLQABADADADg");
	this.shape_15.setTransform(26.7,22.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ah4COQgwg7gBhTQABhSAwg7QAzg7BFAAQBGAAAxA7QAzA7AABSQAABTgzA7QgxA7hGAAQhFAAgzg7gAgyhnQgPAJgNAPQgfAmAAA0QAAA1AfAmQAgAmAuAAQArAAAggmIAOgTQASgfAAgpQAAg0gggmQgggmgrAAQgbAAgXAOgAAjBHQgEgDgBgDQgCgGAAgHQgBgKAIgIQAGgIAIAAIAEAAQAHACAFAGQAHAIAAAKQAAAMgHAHQgGAIgKAAQgIAAgGgIgAgwAIQgEgFgCgEQgHgMAAgPQAAgWANgPQAGgHAIgEQAIgFAJAAQASAAANAQQAHAIADALQACAIABAKQAAAWgNAOQgNAQgSAAQgSAAgNgQg");
	this.shape_16.setTransform(26.9,21.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	// nose
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFEAD5").s().p("AgRARQgGgHAAgKQAAgJAGgHQAIgHAJAAQAKAAAIAHQAGAHAAAJQAAAKgGAHQgIAHgKAAQgJAAgIgHg");
	this.shape_17.setTransform(4.9,39.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFCC99").s().p("AgpAqQgRgRAAgZQAAgXARgSQASgSAXAAQAZAAARASQARASAAAXQAAAZgRARQgRARgZABQgXgBgSgRgAgEgbQgHAHAAAKQAAAKAHAGQAGAHAKAAQALAAAHgHQAGgGAAgKQAAgKgGgHQgHgHgLgBQgKABgGAHg");
	this.shape_18.setTransform(3.7,40.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FEB367").s().p("AhABBQgbgbAAgmQAAgmAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAmQAAAmgbAbQgbAbgmAAQglAAgbgbgAgRg0QgSARAAAYQAAAYASARQARASAYAAQAYAAASgSQARgRAAgYQAAgYgRgRQgSgSgYAAQgYAAgRASg");
	this.shape_19.setTransform(1.3,42.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]}).wait(1));

	// base
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#993300").ss(0.6,1,1).p("AI4hUQADg+A6kOQARhOAWhfAozi4QgCgUgUhzQgRhjgeirAoshvQgBgUgCgVIAAAAQgCgOgBgNAorhoQAAgBgBgBQACAAABAAQgBgCgCgDQgKgQgWgWQgagbgYgLQgLgFgKgBQAAADAAACIhaAAQgeAcgGALQgBABgBABQAAABAAAAQgCAKAAA1QAABgBcA5QBFAqBKAAApwgNQAFghAGgTQADgLADgGQAPgYAkACIABAAQAAABAAAAQACAhABAhQADBnAAACAojhpQgBgBgDAAQgBAAgBAAQABABAAACQgCgBgBAAAoshqQAAgDAAgCArUgcQgEgvAvgUQAJgEAKgDQAQgEATgBQALgBAMAAQAhAAAPACAoohnQABAAAAABAonBHQgBACgCAnAI2A4QACBZAMBbQgWBzhdBIQhkBNh6AkQhxAig8AKQgxAIgFACQgGgCg4gIQhHgKhwgiQh6gkhkhNQhehIgWhzQAJg+AEg+AI4hUQARgkAggsQALgOAJgNQANACAQgEQAIgCAKgDQAFgCAFgCQATgGAHAAQAOAAAMAFQAsATAABgQAABihGAfQgiAPhTAAQgZAAgMAAQgCgeABgcQAAgKAAgJQAMgYARgTIAfABQAEADAEADQAFAEADAFQAEAFAFAKALQiAQgPAAgOADQghAIgcAUQgHAFgGAFQgNALgKAMAI4hUQgCAfgBAg");
	this.shape_20.setTransform(3.3,29);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF9D3C").s().p("Ag1HTQgWgWAAggQAAggAWgXQAXgWAfAAQAgAAAWAWQAXAXAAAgQAAAggXAWQgWAXggAAQgfAAgXgXgAJJDnQAMgYARgTIAfABIAIAGIgIgGIgfgBQAKgMANgLIANgKQAcgUAhgHQghAHgcAUIgNAKQgNALgKAMQgRATgMAYQABggACgfQARgkAggsIAUgaQANABAQgEIASgFIAKgEQATgGAHAAQAPAAALAFQglAOgKAFQgQAIAAARQAAAQAIATIAFAKIAZAuQgQgNgVgGQgRgFgMAAQgRAAgCADQgDAFAAAbIABAOQgVgGgRANQgLAIgUAaIAAgTgAodDXQgMghgXAAIgMADIgFACQADgLAEgGIAAgBQAOgVAeAAIABAAIAAAAIACAAIACAAIACAAIAAAAIAAABIADBCgApiCzIgcAAQgIAJgYAJIAAgLQgBgOAOgJQAHgEAKgEQAUgGAOABIAJACIgJgHIAWgBQAiAAAPACIAAACIgCAAIgCAAIgCAAIAAAAIgBAAQgeAAgOAVIAAABQgEAGgDALQgKAEgHAGgApRC7IAAAAgAoXCSIAAAAgApICQIgWABQgdgXgRgJQgagPgTAAQgFAAgUAGQgUAFgMAAIgLgGQAGgLAegcIBbAAIgBgFQALABALAFQAYALAaAbQAVAWALAQIAAAFQgPgCgiAAgApjlPQK5kzJaEyIgmCtIhpgkQigg3imgcQifgdidAkQigAjiOBRIgvAbIAFgKIgLAAIAAANIhkA4g");
	this.shape_21.setTransform(1.2,3.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFB871").s().p("ADZFIQglgsAAg9QAAg+AlgsQAkgrA1AAQAzAAAlArQAlAsAAA+QAAA9glAsQglAsgzAAQg1AAgkgsgAmIFIQglgsAAg9QAAg+AlgsQAlgrA0AAQAzAAAlArQAlAsAAA+QAAA9glAsQglAsgzAAQg0AAglgsgAq5EfQhcg5AAhhQAAg1ACgJIAAgBIACgDIALAGQALAAAVgFQAUgFAFAAQATAAAaAOQAQAKAeAWIAJAHIgJgBQgOgBgUAGQgKADgHAFQgOAJABANIAAALQAYgJAHgIIAcAAIAAARQAIgFAKgEQgGASgFAhQAFghAGgSIAFgCIAMgEQAXAAALAhIAKAAIADBqIgCAEQgBABgCAoQhKAAhFgrgArUC8IAAgGQAAgqArgSQAJgEAKgDQAQgEAUgCQgUACgQAEQgKADgJAEQgrASAAAqIAAAGgAI1ERQgBgdABgdQAUgbALgIQARgNAUAHIAAgPIAIAJIAJAPIgJgPIgIgJQgBgaADgFQACgDARAAQANAAARAFQAUAGARAMIgZguIgFgKQAOgDAPgBQgPABgOADQgJgSAAgRQAAgQAQgIQAKgGAmgOQAsAUAABfQAABkhGAeQgiAPhTAAIgmAAgAJ5CfIAAAAgAplCYIAAAAgApJhlIgvkOIA8EHIAPBBQgHA0ABAYQgCgUgUhygAnZilIAAgMIAMAAIgGAJIgGAHgAmSixIADAAIgDAOIAAgOg");
	this.shape_22.setTransform(3.3,7.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFCC99").s().p("AhKIwQhHgKhwgiQh6glhkhNQhehIgWhzQAJg+AEg9QACgoABgBIACgEIgDhpIgDhBIADgBIABABIAEgCIgEgBIgCAAIgDgFIgEgpIAAAAIgCgcIgBgEQgBgYAHg1IgPhBIBjg5IAAAEIAGgHIAwgbQCOhQCfgkQCdgjCgAcQCmAdCgA2IBoAlQg6ENgEA+QgCAgAAAfIgBAUQAAAcABAdQACBaANBaQgWBzheBIQhkBNh6AlQhwAig8AKIg1AJIg/gJgAhlBVQgXAWAAAgQAAAgAXAXQAWAWAgAAQAgAAAWgWQAWgXAAggQAAgggWgWQgWgXggAAQggAAgWAXgAC8h4QgkAsAAA+QAAA8AkAsQAlAsA0AAQA0AAAlgsQAlgsAAg8QAAg+glgsQglgrg0AAQg0AAglArgAmlh4QgkAsAAA+QAAA8AkAsQAlAsA0AAQA0AAAlgsQAlgsAAg8QAAg+glgsQglgrg0AAQg0AAglArgAmumRIADgOIgDAAIAAAOg");
	this.shape_23.setTransform(6.1,31);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.face_happycopy2, new cjs.Rectangle(-111.1,-151,230.2,240), null);


(lib.face_happycopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(1,1,1).p("AL6msQAIgvAjgiQAtgsA/ABQA/ABAsAsQAsAtgBBAQgBA/gtArQgsAsg/gBQg/AAgsgtQgEgEgDgDQglgrABg6QAAgOACgMQkaBJjpEfQgEAMgDAHIgFgFQgmAwgkA0AERDRQABgFACgEQAehiA6hQQAdgoAYgVIgDDqIAVABQCOgpBfgbQC8g2AHgBQgFAKhBBjQg/BdgRAuQAmAKCdBNQCNBFBYAyIhYACQg0AAhQgXQhQgWgfAAQgBAHAAANQAAAYAjBRQAoBdAKAsIAAAIQgCgDgCgCQhRhvhhhDQhGgxATgIQhGgYhHgZQigg2imgdQifgcidAjQigAkiOBQQiaBXiaBXQAAABgBAAIAAgDQAAgRgIgaQAAgHgChkQAAgBAAgCQgDAAgGADQgIAGhyBbQhwBXgMAAQAAgSAIgRQAIgRAAgeQAAhrAtg+QATgbAzgjQgDAAgygLQgygLgJgGQAYgKCsgwQCdgrADgIQgdgLhWgwQhYgygWgTQAAgBgBAAQgBgBgBgBIAGAAQA3ACAvACIAFANAKWCUIAKgUQAHkJB1ikAD3hEQgHAHgFAHAiTpZQCaALCFDaQAwBPAjBZQAdBLAAAaQAAALgEARQgBADAAAEAEODHQgmgyhJhhQgCgCgBgBQg3hHgOgPQgBAAgBAAQgBgBAAAAQAAgBAAAAQgBAAAAABQgCAUAABPQgBA4gJABQADAAAAACQgBADABABQgGAEAAABQgBAAgGAAQgBAAjvjTIgDAAQgDANAAA5QAAAlASAqQARAjAAABIgBASIAAAAQgMgBgegIQhiieh3hzIAfABQBCAABIjFQAbhIAThMQAIgjAFgYAiTpZQgHANgLALQgJAHgIAGQgZAQgfAAQgqAAgegfQgegeABgqQAAgqAfgeQAfgeAqAAQAqABAdAeQAeAfAAAqQgBAbgMAVgAtimkQgJAMgMAMQgaAZgfAKQgXAHgaAAQg9AAgrgsQgrgsAAg+QABg+AsgrQAsgrA+ABQA+ABArAsQArAsgBA9QgBAtgXAkgAmaihQgCgBgBgBQjFi8kAhFAiKCSIg3giQhAgSiSg2QjmhTgagFQAFAHBWBZQBEBHAAAaQgQAAjjgLAuwlpQDeDygVD9");
	this.shape.setTransform(3.9,-74.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("AKCDvQhHgxATgJIiNgwQigg3imgcQiegdieAjQigAkiOBQIk0CvIgBgDQAAgRgIgZIgChrIAAgEQgDAAgGAEIh6BgQhwBXgMAAQAAgRAIgSQAIgRAAgeQAAhqAtg/QATgaAzgkIg1gLQgygLgJgGQAYgKCsguQCdgsADgIQgdgLhWgwQhYgxgWgUIADgDIBmAFIDzAKQAAgZhEhIQhWhagFgHQAaAFDmBVQCSA2BAARIA3AiIg3giQAeAJAMAAIAAAAIABgSIgRgkQgSgqAAglQAAg6ADgNIADAAQDwDUABAAIAHAAIAFgFQAAAAgBgBQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQAJAAABg4QABhRACgUIAAABIABAAIACAAQAOAPA3BIIADAEIBvCSIAGACIgDAIIADgIQAfhjA5hPQAdgqAYgUIgDDrIAVAAIDthEIgKAUIAKgUIDDg3QgFAKhBBjQg/BdgRAvQAmAJCdBMQCNBGBYAxIhYACQg0AAhQgXQhQgWgfAAIgBAUQAAAYAjBSQAoBdAKArIgEADQhRhuhghDgAspi5IgFgMgADNiDg");
	this.shape_1.setTransform(11,-41.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCFF00").s().p("AgBCYQg/gBgsgtIgGgHQglgqAAg6QABgNACgMQAIgvAjgjQAtgrA+AAQA/ABArAtQAsAtgBA+QAAA/gtAsQgsArg9AAIgCAAg");
	this.shape_2.setTransform(95.1,-114.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CCFF").s().p("AgBCVQg9gBgrgsQgrgsAAg9QABg9AsgrQAsgrA9AAQA+ABArAsQArAsgBA9QgBAtgXAjQgJANgMAMQgZAZggAKQgWAHgYAAIgCAAg");
	this.shape_3.setTransform(-95.3,-125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#99CC00").s().p("AjOE5IhwiTIACgGQAlg1AlgvIAGAEQADgHADgMQDpkdEahKQgDANAAANQAAA7AlAqQh1CkgHEJIjsBEIgVgBIACjrQgXAVgeApQg5BQgfBjg");
	this.shape_4.setTransform(51.7,-86);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0099FF").s().p("AitERQAUj/jejwQAggKAagaQAMgLAIgNQEBBFDEC8IACACQB4ByBiCfQhAgRiTg2QjkhVgbgFQAFAHBWBaQBDBIAAAZIjxgKg");
	this.shape_5.setTransform(-53,-88.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF3333").s().p("AhkCZIgDAAQgCANAAA6QgBAlATAqIARAkIgBASIAAAAQgMAAgegJQhiifh3hzIAfABQBCAABIjEQAahHAUhNQAIgiAEgYQAJgGAJgIQALgLAHgNQCaALCFDaQAwBPAiBYQAdBMAAAZQABALgEASIgBAGIgNAPQglAvglA1IgEADQg3hIgPgPIgBAAIgBAAIAAgCIgBABQgCAUgBBRQAAA4gKAAQABAAABAAQABAAAAABQABAAAAAAQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABABAAIgGAFIgHAAQgBAAjwjUg");
	this.shape_6.setTransform(-4,-98.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF9900").s().p("AjwLKIABACIgBABgAlkCpIADgCIgDADIAAgBgAD+n/QgqAAgegfQgegfAAgpQABgqAegeQAfgeAqAAQAqABAeAeQAeAfgBAqQAAAbgMAVQgIANgLALQgIAHgJAGQgYAQgeAAIgBAAg");
	this.shape_7.setTransform(-45.3,-78.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// brows
	this.instance = new lib.browTween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(2.7,-20.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// lips
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgJAzQhIgBhGgVQhGgSgig8QDkBBEWhFIAEATQgzAyhCATQg0ATg4AAQgUAAgTgDg");
	this.shape_8.setTransform(2.4,62);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#990000").s().p("AiRgEQBMgfBDADIAGAAIAHAAQBHgEAlANQAPAGAMAIIgRAJQg/AihCAAQhGAAhLgmg");
	this.shape_9.setTransform(4.9,74.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("AlihlQAsAUAuAOIAIACQAiA9BFARQBHAVBIABQBLAJBIgZQBBgTA0gyIAcgcIBKgXQhZCLhhA7QgMgIgPgGQglgNhHAEIgHAAIgHAAQhCgDhMAfQhwg6h5iRg");
	this.shape_10.setTransform(2.5,63.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8}]}).wait(1));

	// eyes
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0099FF").s().p("AhMBbQgggmAAg1QAAg0AggnQANgOAPgJQAWgOAaAAQAsAAAgAlQAhAnAAA0QAAApgTAfIgOATQggAmgsAAQgsAAgggmgAAkA8QAGAIAJAAQAJAAAHgIQAGgIAAgLQAAgLgGgHQgGgHgHgBIABgPQAAgbgQgTQgGgHgIgFQgDgKgHgIQgNgPgSgBQgJAAgHAFQgIADgHAIQgMAQAAAVQAAAPAGALIgCANQAAAaAQAVQARASAWAAQARAAAPgMQABAEADADg");
	this.shape_11.setTransform(-20.3,22.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("Ah4COQgwg7AAhTQAAhSAwg7QAyg7BGAAQBGAAAyA7QAyA7gBBSQABBTgyA7QgyA7hGAAQhGAAgyg7gAgyhnQgOAJgNAPQggAmAAA0QAAA1AgAmQAfAmAtAAQAsAAAggmIANgSQAUggAAgpQAAg0ghgmQggglgsAAQgaAAgXANgAAiBHQgDgDgBgDQgCgGAAgGQAAgLAGgIQAHgHAIAAIADAAQAIABAGAGQAGAIAAALQAAAKgGAIQgHAIgKAAQgIAAgHgIgAgwAIQgEgEgDgGQgFgLAAgPQAAgVAMgQQAGgHAJgEQAGgEAKAAQASAAANAPQAHAIADAKQADAJAAAKQAAAWgNAOQgNAQgSAAQgSAAgNgQg");
	this.shape_12.setTransform(-20.1,21.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgmAsQgQgVAAgaIABgNQADAGAEAFQANAPASAAQASAAANgPQAMgPAAgWQAAgKgCgJQAHAFAGAHQAQATAAAbIgBAPIgDAAQgIAAgHAIQgHAHAAALQAAAHADAFQgOAMgSAAQgWAAgQgSg");
	this.shape_13.setTransform(-20.3,22.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgmArQgQgUAAgZQAAgIABgGQADAGAEAFQANAOASAAQASAAANgOQAMgQAAgWQAAgJgCgJQAHAFAGAHQAQATAAAcIgBAOIgDAAQgIAAgHAHQgHAJAAAKQAAAHADAGQgOALgSAAQgWAAgQgTg");
	this.shape_14.setTransform(26.7,22.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0099FF").s().p("AhMBbQgfgmgBg1QABg0AfgmQANgPAPgJQAWgOAaAAQAsAAAhAmQAfAmAAA0QAAApgSAfIgNATQghAmgsAAQgsAAgggmgAAkA8QAHAIAIAAQAKAAAGgIQAHgIgBgLQABgKgHgJQgFgFgIgCIABgPQAAgbgQgTQgGgHgHgFQgEgKgGgIQgNgQgTAAQgJAAgHAEQgIAEgGAIQgNAPAAAWQAAAPAGALQgBAHgBAGQABAbAQATQAQATAWAAQARAAAPgLQABADADADg");
	this.shape_15.setTransform(26.7,22.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ah4COQgwg7gBhTQABhSAwg7QAzg7BFAAQBGAAAxA7QAzA7AABSQAABTgzA7QgxA7hGAAQhFAAgzg7gAgyhnQgPAJgNAPQgfAmAAA0QAAA1AfAmQAgAmAuAAQArAAAggmIAOgTQASgfAAgpQAAg0gggmQgggmgrAAQgbAAgXAOgAAjBHQgEgDgBgDQgCgGAAgHQgBgKAIgIQAGgIAIAAIAEAAQAHACAFAGQAHAIAAAKQAAAMgHAHQgGAIgKAAQgIAAgGgIgAgwAIQgEgFgCgEQgHgMAAgPQAAgWANgPQAGgHAIgEQAIgFAJAAQASAAANAQQAHAIADALQACAIABAKQAAAWgNAOQgNAQgSAAQgSAAgNgQg");
	this.shape_16.setTransform(26.9,21.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	// nose
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFEAD5").s().p("AgRARQgGgHAAgKQAAgJAGgHQAIgHAJAAQAKAAAIAHQAGAHAAAJQAAAKgGAHQgIAHgKAAQgJAAgIgHg");
	this.shape_17.setTransform(4.9,39.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFCC99").s().p("AgpAqQgRgRAAgZQAAgXARgSQASgSAXAAQAZAAARASQARASAAAXQAAAZgRARQgRARgZABQgXgBgSgRgAgEgbQgHAHAAAKQAAAKAHAGQAGAHAKAAQALAAAHgHQAGgGAAgKQAAgKgGgHQgHgHgLgBQgKABgGAHg");
	this.shape_18.setTransform(3.7,40.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FEB367").s().p("AhABBQgbgbAAgmQAAgmAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAmQAAAmgbAbQgbAbgmAAQglAAgbgbgAgRg0QgSARAAAYQAAAYASARQARASAYAAQAYAAASgSQARgRAAgYQAAgYgRgRQgSgSgYAAQgYAAgRASg");
	this.shape_19.setTransform(1.3,42.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]}).wait(1));

	// base
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#993300").ss(0.6,1,1).p("AI4hUQADg+A6kOQARhOAWhfAozi4QgCgUgUhzQgRhjgeirAoshvQgBgUgCgVIAAAAQgCgOgBgNAorhoQAAgBgBgBQACAAABAAQgBgCgCgDQgKgQgWgWQgagbgYgLQgLgFgKgBQAAADAAACIhaAAQgeAcgGALQgBABgBABQAAABAAAAQgCAKAAA1QAABgBcA5QBFAqBKAAApwgNQAFghAGgTQADgLADgGQAPgYAkACIABAAQAAABAAAAQACAhABAhQADBnAAACAojhpQgBgBgDAAQgBAAgBAAQABABAAACQgCgBgBAAAoshqQAAgDAAgCArUgcQgEgvAvgUQAJgEAKgDQAQgEATgBQALgBAMAAQAhAAAPACAoohnQABAAAAABAonBHQgBACgCAnAI2A4QACBZAMBbQgWBzhdBIQhkBNh6AkQhxAig8AKQgxAIgFACQgGgCg4gIQhHgKhwgiQh6gkhkhNQhehIgWhzQAJg+AEg+AI4hUQARgkAggsQALgOAJgNQANACAQgEQAIgCAKgDQAFgCAFgCQATgGAHAAQAOAAAMAFQAsATAABgQAABihGAfQgiAPhTAAQgZAAgMAAQgCgeABgcQAAgKAAgJQAMgYARgTIAfABQAEADAEADQAFAEADAFQAEAFAFAKALQiAQgPAAgOADQghAIgcAUQgHAFgGAFQgNALgKAMAI4hUQgCAfgBAg");
	this.shape_20.setTransform(3.3,29);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF9D3C").s().p("Ag1HTQgWgWAAggQAAggAWgXQAXgWAfAAQAgAAAWAWQAXAXAAAgQAAAggXAWQgWAXggAAQgfAAgXgXgAJJDnQAMgYARgTIAfABIAIAGIgIgGIgfgBQAKgMANgLIANgKQAcgUAhgHQghAHgcAUIgNAKQgNALgKAMQgRATgMAYQABggACgfQARgkAggsIAUgaQANABAQgEIASgFIAKgEQATgGAHAAQAPAAALAFQglAOgKAFQgQAIAAARQAAAQAIATIAFAKIAZAuQgQgNgVgGQgRgFgMAAQgRAAgCADQgDAFAAAbIABAOQgVgGgRANQgLAIgUAaIAAgTgAodDXQgMghgXAAIgMADIgFACQADgLAEgGIAAgBQAOgVAeAAIABAAIAAAAIACAAIACAAIACAAIAAAAIAAABIADBCgApiCzIgcAAQgIAJgYAJIAAgLQgBgOAOgJQAHgEAKgEQAUgGAOABIAJACIgJgHIAWgBQAiAAAPACIAAACIgCAAIgCAAIgCAAIAAAAIgBAAQgeAAgOAVIAAABQgEAGgDALQgKAEgHAGgApRC7IAAAAgAoXCSIAAAAgApICQIgWABQgdgXgRgJQgagPgTAAQgFAAgUAGQgUAFgMAAIgLgGQAGgLAegcIBbAAIgBgFQALABALAFQAYALAaAbQAVAWALAQIAAAFQgPgCgiAAgApjlPQK5kzJaEyIgmCtIhpgkQigg3imgcQifgdidAkQigAjiOBRIgvAbIAFgKIgLAAIAAANIhkA4g");
	this.shape_21.setTransform(1.2,3.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFB871").s().p("ADZFIQglgsAAg9QAAg+AlgsQAkgrA1AAQAzAAAlArQAlAsAAA+QAAA9glAsQglAsgzAAQg1AAgkgsgAmIFIQglgsAAg9QAAg+AlgsQAlgrA0AAQAzAAAlArQAlAsAAA+QAAA9glAsQglAsgzAAQg0AAglgsgAq5EfQhcg5AAhhQAAg1ACgJIAAgBIACgDIALAGQALAAAVgFQAUgFAFAAQATAAAaAOQAQAKAeAWIAJAHIgJgBQgOgBgUAGQgKADgHAFQgOAJABANIAAALQAYgJAHgIIAcAAIAAARQAIgFAKgEQgGASgFAhQAFghAGgSIAFgCIAMgEQAXAAALAhIAKAAIADBqIgCAEQgBABgCAoQhKAAhFgrgArUC8IAAgGQAAgqArgSQAJgEAKgDQAQgEAUgCQgUACgQAEQgKADgJAEQgrASAAAqIAAAGgAI1ERQgBgdABgdQAUgbALgIQARgNAUAHIAAgPIAIAJIAJAPIgJgPIgIgJQgBgaADgFQACgDARAAQANAAARAFQAUAGARAMIgZguIgFgKQAOgDAPgBQgPABgOADQgJgSAAgRQAAgQAQgIQAKgGAmgOQAsAUAABfQAABkhGAeQgiAPhTAAIgmAAgAJ5CfIAAAAgAplCYIAAAAgApJhlIgvkOIA8EHIAPBBQgHA0ABAYQgCgUgUhygAnZilIAAgMIAMAAIgGAJIgGAHgAmSixIADAAIgDAOIAAgOg");
	this.shape_22.setTransform(3.3,7.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFCC99").s().p("AhKIwQhHgKhwgiQh6glhkhNQhehIgWhzQAJg+AEg9QACgoABgBIACgEIgDhpIgDhBIADgBIABABIAEgCIgEgBIgCAAIgDgFIgEgpIAAAAIgCgcIgBgEQgBgYAHg1IgPhBIBjg5IAAAEIAGgHIAwgbQCOhQCfgkQCdgjCgAcQCmAdCgA2IBoAlQg6ENgEA+QgCAgAAAfIgBAUQAAAcABAdQACBaANBaQgWBzheBIQhkBNh6AlQhwAig8AKIg1AJIg/gJgAhlBVQgXAWAAAgQAAAgAXAXQAWAWAgAAQAgAAAWgWQAWgXAAggQAAgggWgWQgWgXggAAQggAAgWAXgAC8h4QgkAsAAA+QAAA8AkAsQAlAsA0AAQA0AAAlgsQAlgsAAg8QAAg+glgsQglgrg0AAQg0AAglArgAmlh4QgkAsAAA+QAAA8AkAsQAlAsA0AAQA0AAAlgsQAlgsAAg8QAAg+glgsQglgrg0AAQg0AAglArgAmumRIADgOIgDAAIAAAOg");
	this.shape_23.setTransform(6.1,31);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.face_happycopy, new cjs.Rectangle(-111.1,-151,230.2,240), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween5copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.jackboxRedcopy3();
	this.instance.parent = this;
	this.instance.setTransform(0.2,92,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_1 = new lib.jackboxRedcopy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(409.7,92,0.792,0.792,0,0,0,7.7,365.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-170.1,-230.2,749.7,460.5);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween5copy4("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,13.9);

	this.instance_1 = new lib.Symbol5copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.1,-6,1,1,0,0,0,603.8,238.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-603.7,-244.1,1207.6,488.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.JackHead_redcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.instance = new lib.face_happycopy2();
	this.instance.parent = this;
	this.instance.setTransform(-1.5,20.2,1.012,1.012);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.JackHead_redcopy2, new cjs.Rectangle(-113.5,-132.2,232.1,241.8), null);


(lib.JackHead_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.instance = new lib.face_happycopy();
	this.instance.parent = this;
	this.instance.setTransform(-1.5,20.2,1.012,1.012);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.JackHead_mccopy2, new cjs.Rectangle(-113.5,-132.2,232.1,241.8), null);


(lib.jackboxRedcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop()*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC00").s().p("A7lKQIiM0eMA7jAAAIiMUeg");
	this.shape.setTransform(10.7,421.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC6600").s().p("A9xAeIBVg7MA47AAAIBTA7g");
	this.shape_1.setTransform(10.7,352.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.jackhead = new lib.JackHead_redcopy2();
	this.jackhead.name = "jackhead";
	this.jackhead.parent = this;
	this.jackhead.setTransform(14.2,140.8,1,1,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.jackhead).wait(1));

	// Layer 3
	this.instance = new lib.springcopy3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(26.2,471.7,2.967,2.654,0,-1.1,-0.2,5.2,48.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,0,0,0.502)").ss(1,1,1,3,true).p("AckggIAGgBIAlAAA9OAhIAnAA");
	this.shape_2.setTransform(13,260.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC3300").s().p("A5dh6MAy7gAfIh5EZMgu7AAag");
	this.shape_3.setTransform(5.7,-22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("A4fziMAu7gAZMADhAn2Mgz5AACg");
	this.shape_4.setTransform(13,118.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("A9OX0MACGgvHMAz6gAgMAEdAukIglAAMgEVguCMgy7AAeMgCBAung");
	this.shape_5.setTransform(13,111.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FF9900","#CC6600"],[0,0.992],-14.7,-15.4,14.7,-15.4).s().p("AAaVPMgDggn2IB5kaMAEUAuCIgGABg");
	this.shape_6.setTransform(176.5,109.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FF9900","#CC6600"],[0,0.992],-13.9,-0.3,13.9,-0.3).s().p("AgC3TICGEVMgBdAneIiqC0g");
	this.shape_7.setTransform(-157,114.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#BCEA04","#87A703"],[0,0.992],-7.1,-301.9,-7.1,0.9).s().p("AgCgBIAFgBIAAAFg");
	this.shape_8.setTransform(196.1,257.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("A5lIDIAAwFMAzLAAAIAAQFg");
	this.shape_9.setTransform(13.6,300.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#FF9900","#993300"],[0,0.992],-35.6,1.6,2.3,1.6).s().p("AhRIDIAAwFICjQFg");
	this.shape_10.setTransform(185.6,300.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FF9900","#993300"],[0,0.992],-39.8,2.4,14.3,2.4).s().p("AhwIDIDhwFIAAQFg");
	this.shape_11.setTransform(-161.5,300.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF9900").s().p("AcnH8IikwFIAhgcIDORMgA5uolIAnAcIjjQFIhHArg");
	this.shape_12.setTransform(10.7,300.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("A5iAOIgngbMA0TAAAIghAbg");
	this.shape_13.setTransform(13.3,247.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(0,0,0,0.502)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_14.setTransform(13.5,453.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.2)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_15.setTransform(13.5,453.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 4
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(255,255,255,0.008)").s().p("EghjAbSMAAAg2jMBDHAAAMAAAA2jg");
	this.shape_16.setTransform(7.5,365.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(1));

}).prototype = getMCSymbolPrototype(lib.jackboxRedcopy2, new cjs.Rectangle(-207.3,-41.1,429.6,581.5), null);


(lib.Tween5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.jackboxRedcopy3();
	this.instance.parent = this;
	this.instance.setTransform(0.2,92,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_1 = new lib.jackboxRedcopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-409.3,92,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_2 = new lib.jackboxRedcopy4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(409.7,92,0.792,0.792,0,0,0,7.7,365.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-579.6,-230.2,1159.2,460.5);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.jackboxRedcopy3();
	this.instance.parent = this;
	this.instance.setTransform(0.2,92,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_1 = new lib.jackboxRedcopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-409.3,92,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_2 = new lib.jackboxRedcopy4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(409.7,92,0.792,0.792,0,0,0,7.7,365.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-579.6,-230.2,1159.2,460.5);


(lib.jackboxRed = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop()*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF66").s().p("Ap7CmQkJhFAAhhQAAhgEJhFQEHhFF0AAQF0AAEIBFQEJBFgBBgQABBhkJBFQkIBFl0gBQl0ABkHhFg");
	this.shape.setTransform(7.6,272.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EFAD00").s().p("AsbDPQlJhWgBh5QABh4FJhXQFKhVHRAAQHRAAFLBVQFJBXAAB4QAAB5lJBWQlLBWnRAAQnRAAlKhWgAp7i8QkJBFAABhQAABgEJBFQEHBFF0AAQF0AAEIhFQEJhFgBhgQABhhkJhFQkIhEl0AAQl0AAkHBEg");
	this.shape_1.setTransform(7.6,275.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("A+XHEIDtuHMA2UAAAICuOHg");
	this.shape_2.setTransform(10.6,306,1,1,0,0,0,-0.1,29.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC6600").s().p("A+XCcIAAk3MA8vAAAIAAE3g");
	this.shape_3.setTransform(10.7,337.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("rgba(0,0,0,0.498)").ss(1,1,1,3,true).p("AdydxMg7jAAAMADog7hMA1QAAAg");
	this.shape_4.setTransform(10.7,276.3,1.02,0.237);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#FD3E8A","#DA035A"],[0,1],190.6,111.2,-190.5,111.2).s().p("A9xdxMADog7hMA1QAAAMACrA7hg");
	this.shape_5.setTransform(10.7,276.3,1.02,0.237);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("rgba(0,0,0,0.498)").ss(1,1,1,3,true).p("AdyKQMg7jAAAIAA0eMA7jAAAg");
	this.shape_6.setTransform(10.7,337.1,1.02,0.237);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FD3E8A","#DA035A"],[0,1],190.6,-13.7,-190.5,-13.7).s().p("A9xKQIAA0eMA7jAAAIAAUeg");
	this.shape_7.setTransform(10.7,337.1,1.02,0.237);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFCC00").s().p("A7lKQIiM0eMA7jAAAIiMUeg");
	this.shape_8.setTransform(10.7,421.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC6600").s().p("A9xAeIBVg7MA47AAAIBTA7g");
	this.shape_9.setTransform(10.7,352.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 2
	this.jackhead = new lib.JackHead_mccopy2();
	this.jackhead.name = "jackhead";
	this.jackhead.parent = this;
	this.jackhead.setTransform(-0.5,362.9,0.426,0.426);

	this.timeline.addTween(cjs.Tween.get(this.jackhead).wait(1));

	// Layer 1
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.502)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_10.setTransform(13.5,453.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("A5lIDIAAwFMAzLAAAIAAQFg");
	this.shape_11.setTransform(13.6,300.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FF9900","#993300"],[0,0.992],-35.6,1.6,2.3,1.6).s().p("AhRIDIAAwFICjQFg");
	this.shape_12.setTransform(185.6,300.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FF9900","#993300"],[0,0.992],-39.8,2.4,14.3,2.4).s().p("AhwIDIDhwFIAAQFg");
	this.shape_13.setTransform(-161.5,300.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF9900").s().p("AcnH8IikwFIAhgcIDORMgA5uolIAnAcIjjQFIhHArg");
	this.shape_14.setTransform(10.7,300.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("A5iAOIgngbMA0TAAAIghAbg");
	this.shape_15.setTransform(13.3,247.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(0,0,0,0.2)").s().p("A/qIHIJWwNMApGAAAIM5QNg");
	this.shape_16.setTransform(13.5,453.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("rgba(0,0,0,0.498)").ss(1,1,1,3,true).p("AakolIghAcICkQFAaDH8IAAwFMgzKAAAIgngcMA0SAAAAdyInIjOxMA5HoJIAAQFA8qH8IDjwFA9xInIEDxM");
	this.shape_17.setTransform(10.7,300.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#BCEA04","#87A703"],[0,0.992],-18.4,0.7,35.7,0.7).s().p("AhwIDIDhwFIAAQFg");
	this.shape_18.setTransform(-161.5,300.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#667E01","#3D4B01"],[0,0.992],5.3,-54.5,5.2,55.8).s().p("A5lIDIAAwFMAzLAAAIAAQFg");
	this.shape_19.setTransform(13.6,300.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#BCEA04","#87A703"],[0,0.992],-22.2,0.7,15.7,0.7).s().p("AhRIDIAAwFICjQFg");
	this.shape_20.setTransform(185.6,300.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#4A5C01").s().p("A5iAOIgngbMA0TAAAIghAbg");
	this.shape_21.setTransform(13.3,247.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#D3FC2E").s().p("AcnH8IikwFIAhgdIDORNgA5uomIAnAdIjjQFIhHArg");
	this.shape_22.setTransform(10.7,300.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]}).wait(1));

	// Layer 4
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(255,255,255,0.008)").s().p("EghjAbSMAAAg2jMBDHAAAMAAAA2jg");
	this.shape_23.setTransform(7.5,365.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

}).prototype = getMCSymbolPrototype(lib.jackboxRed, new cjs.Rectangle(-207.3,191.3,429.6,349.1), null);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.jackboxRed();
	this.instance.parent = this;
	this.instance.setTransform(0.2,0,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_1 = new lib.jackboxRed();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-409.3,0,0.792,0.792,0,0,0,7.7,365.8);

	this.instance_2 = new lib.jackboxRed();
	this.instance_2.parent = this;
	this.instance_2.setTransform(409.7,0,0.792,0.792,0,0,0,7.7,365.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-579.6,-138.2,1159.2,276.5);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(235.8,441.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(217).to({_off:false},0).wait(56).to({startPosition:16},0).to({alpha:0,startPosition:3},7).wait(1));

	// Layer_8
	this.instance_1 = new lib.jackboxRedcopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(230.7,570.1,0.792,0.792,0,0,0,7.7,365.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(217).to({_off:false},0).to({regY:365.9,scaleX:0.84,scaleY:0.84,y:568.1},12).wait(44).to({alpha:0},7).wait(1));

	// Layer_9
	this.instance_2 = new lib.Tween5copy4("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(640,478.1);

	this.instance_3 = new lib.Symbol5copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(640.1,458.2,1,1,0,0,0,603.8,238.1);

	this.instance_4 = new lib.Tween3("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(640.1,464.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3},{t:this.instance_2}]},217).to({state:[{t:this.instance_4}]},56).to({state:[{t:this.instance_4}]},7).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(273).to({_off:false},0).to({alpha:0},7).wait(1));

	// hand
	this.instance_5 = new lib.handanim("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(200.1,643.5,1,1,0,0,0,41,60.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(187).to({_off:false},0).to({_off:true},31).wait(63));

	// arrow
	this.instance_6 = new lib.arrowanim("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(227.1,196.5,1,1,0,0,0,41,60.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(156).to({_off:false},0).to({_off:true},31).wait(94));

	// Layer_4
	this.instance_7 = new lib.Symbol4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(765,110.1,1,1,0,0,0,216,36.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(96).to({_off:false},0).to({_off:true},57).wait(128));

	// Layer_6
	this.instance_8 = new lib.jackboxRedcopy2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(230.7,570.1,0.792,0.792,0,0,0,7.7,365.8);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(96).to({_off:false},0).to({scaleX:0.82,scaleY:0.82},10).to({scaleX:0.79,scaleY:0.79},9).to({scaleX:0.82,scaleY:0.82},9).to({scaleX:0.79,scaleY:0.79},9).to({scaleX:0.82,scaleY:0.82},9).to({scaleX:0.79,scaleY:0.79},10).to({_off:true},1).wait(128));

	// Layer_3
	this.instance_9 = new lib.Tween5("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640,478.1);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween5copy("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,478.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9}]},74).to({state:[{t:this.instance_10}]},7).to({state:[]},138).wait(62));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(74).to({_off:false},0).to({_off:true,alpha:1},7).wait(200));

	// Layer_2
	this.instance_11 = new lib.Tween4("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,570.1);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(45).to({_off:false},0).to({alpha:1},8).to({_off:true},21).wait(207));

	// Layer_1
	this.instance_12 = new lib.Symbol5copy();
	this.instance_12.parent = this;
	this.instance_12.setTransform(640.1,458.2,1,1,0,0,0,603.8,238.1);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(45).to({_off:false},0).to({alpha:1},8).to({_off:true},166).wait(62));

	// QuestionText1
	this.instance_13 = new lib.Tween19("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(637.1,109.7);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.instance_14 = new lib.Tween20("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(637.1,109.7);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(19).to({_off:false},0).to({_off:true,alpha:1},8).wait(254));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(19).to({_off:false},8).wait(187).to({startPosition:0},0).to({alpha:0},4).to({_off:true},1).wait(62));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(565.1,296.3,1447,867);
// library properties:
lib.properties = {
	id: '56AA0165ECEEE544ABAAD2F12A1CA2AF',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['56AA0165ECEEE544ABAAD2F12A1CA2AF'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;