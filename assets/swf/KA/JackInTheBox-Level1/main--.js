///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 200, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 3, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0, n = 0, quesHolderMc, quesMarkMc, clk = -1, correctCnt = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;
var currentX, currentY
var currentObj = []
var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var question1, question2;
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var quesArr = [];
var chposArr = [1, 2, 3, 2, 1, 3, 1, 2, 3, 1, 3, 2];
var choiceArr = []
var attemptCnt
var ansMc = []
var currentPos

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////
function init() {

    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "JackInTheBox-Level1/";
    soundpath = "FA/"
    loadProgressLabel.color = "white"

    var success = createManifest();
    if (success == 1) {
        manifest.push(
            { id: "choice1", src: gameAssetsPath + "ChoiceImages1.png" },
            { id: "choice2", src: gameAssetsPath + "ChoiceImages2.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}
//=====================================================================//
function doneLoading1(event) {
    var event = assets[i];
    var id = event.item.id;

    if (id == "questionText") {
        var questionTextSprisheet = new createjs.SpriteSheet({
            framerate: 60,
            "images": [preload.getResult("questionText")],
            "frames": { "regX": 50, "height": 118, "count": 64, "regY": 50, "width": 849 }
        });

        questionText = new createjs.Sprite(questionTextSprisheet);
        container.parent.addChild(questionText);
        questionText.visible = false;

    }


    if (id == "choice1") {
        //
        var spriteSheet2 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 495, "count": 30, "regY": 50, "width": 401 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        choice1 = new createjs.Sprite(spriteSheet2);
        container.parent.addChild(choice1);
        choice1.visible = false;

    }

    if (id == "choice2") {
        //
        var spriteSheet2 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice2")],
            "frames": { "regX": 50, "height": 188, "count": 30, "regY": 50, "width": 238 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        choice2 = new createjs.Sprite(spriteSheet2);
        container.parent.addChild(choice2);
        choice2.visible = false;

    }

    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("56AA0165ECEEE544ABAAD2F12A1CA2AF");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }
}

function tick(e) {
    stage.update();
}

/////////////////////////////////////////////////////////////////=======GAME START========///////////////////////////////////////////////////////////////////
function handleClick(e) {
    toggleFullScreen()
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Observe the jack and select the box that had jack";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 20

    bg.visible = true;

    container.parent.addChild(questionText);
    questionText.visible = true;
    questionText.x = 300; questionText.y = 130;

    chposArr.sort(randomSort)

    var btnX = [100, 500, 900]
    var btnY = [330, 330, 330]

    for (i = 1; i <= choiceCnt; i++) {

        choiceArr[i] = choice2.clone()
        container.parent.addChild(choiceArr[i])
        choiceArr[i].visible = false;
        choiceArr[i].name = i;
        choiceArr[i].x = btnX[i - 1] + 60
        choiceArr[i].y = btnY[i - 1] + 200
        choiceArr[i].scaleX = .95
        choiceArr[i].scaleY = .9
        choiceArr[i].gotoAndStop(2)
        choiceArr[i].alpha = .2


        quesArr[i] = choice1.clone()
        container.parent.addChild(quesArr[i])
        quesArr[i].visible = true;
        quesArr[i].name = i;
        quesArr[i].x = btnX[i - 1]
        quesArr[i].y = btnY[i - 1]
        quesArr[i].scaleX = quesArr[i].scaleY = .8

    }
    container.parent.addChild(correctImageMc);
    container.parent.addChild(wrongImageMc);
    setTimeout(pickques, 1000);
}

function helpDisable() {
    for (i = 1; i <= choiceCnt; i++) {
        quesArr[i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 1; i <= choiceCnt; i++) {
        quesArr[i].mouseEnabled = true;
    }
}
//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    n = 0
    clk = -1
    currentObj = []
    correctImageMc.visible = false
    wrongImageMc.visible = false
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    //=================================================================================================================================//
    boardMc.boardMc.openMc.mouseEnabled = false;
    boardMc.boardMc.openMc.alpha = .5

    questionText.gotoAndStop(0);
    currentPos = chposArr[cnt]
    for (i = 0; i < choiceCnt; i++) {
        quesArr[i + 1].visible = true;
        quesArr[i + 1].gotoAndStop(30);
        choiceArr[i + 1].visible = true
    }

    questionInterval = setInterval(questionDisplay, 1000)

    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
}

function questionDisplay() {
    helpMc.helpMc.visible = false;
    clearInterval(questionInterval);
    boardMc.boardMc.openMc.mouseEnabled = true;
    boardMc.boardMc.openMc.alpha = 1

    questionText.gotoAndStop(1);
    for (i = 0; i < choiceCnt; i++) {
        quesArr[i + 1].alpha = 1
        if ((i + 1) == currentPos) {
            quesArr[i + 1].gotoAndStop(11);
        } else {
            quesArr[i + 1].gotoAndStop(29)
        }
        quesArr[i + 1].visible = true;
    }

    ans = chposArr[cnt];
    console.log(ans)

    rst = 0;
    gameResponseTimerStart();
    enablechoices();
}

function enablechoices() {
    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].mouseEnabled = true;
        choiceArr[i].cursor = "pointer";
        choiceArr[i].addEventListener("click", answerSelected)

    }
}

function disablechoices() {
    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].mouseEnabled = false;
        choiceArr[i].cursor = "default";
        choiceArr[i].removeEventListener("click", answerSelected)
        quesArr[i].visible = false
        choiceArr[i].visible = false
    }

    boardMc.boardMc.openMc.mouseEnabled = false;
    boardMc.boardMc.openMc.alpha = .5
}

function onRoll_over(e) {
    // e.currentTarget.alpha = .5;
    stage.update();
}

function onRoll_out(e) {
    // e.currentTarget.alpha = 1;
    stage.update();
}

function answerSelected(e) {

    e.preventDefault();
    boardMc.boardMc.openMc.mouseEnabled = false;
    boardMc.boardMc.openMc.alpha = .5;

    uans = e.currentTarget.name;
    gameResponseTimerStop();


    console.log(ans + " =correct= " + uans)
    if (ans == uans) {

        for (i = 1; i <= choiceCnt; i++) {
            choiceArr[i].mouseEnabled = false;
        }
        e.currentTarget.removeEventListener("click", answerSelected)
        currentX = e.currentTarget.x + 20
        currentY = e.currentTarget.y - 50
        starAnimation()
        setTimeout(correct, 800)

    } else {
        wrongImageMc.visible = true;
        getValidation("wrong");
        disablechoices();
    }
}
function correct() {
    getValidation("correct");
    disablechoices();
}


function disableMouse() {

}

function enableMouse() {

}
//===============================================================================================//

