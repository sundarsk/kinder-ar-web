(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ag2BWIAAgyIABgmIAAgNIgsgBIAAglIAuAAQACgUAHgUQAFgSAMgQQALgOAQgJQARgIAYAAQANAAAOAEQAPAEAOAMIgUAiQgLgIgKgDQgJgDgJgBQgIAAgJAAQgIACgFAFQgGAEgEAHQgEAIgDAMQgDALgCASIBSAAIgFAmIhQgBIAAAQIAAA1IAAAYIAAAbIAAAbIAAAbIgoAAIABhJg");
	this.shape.setTransform(220.4,-1.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgoBqQgTgIgOgPQgOgPgIgVQgJgVAAgaQAAgZAJgVQAIgVAOgPQAOgPATgJQATgIAVAAQAVAAAUAJQATAJAOAPQAOAPAJAVQAIAVAAAYQAAAZgIAVQgJAUgOAQQgOAPgTAJQgUAJgVAAQgVAAgTgJgAgchIQgMAHgHALQgIAMgDAOQgEAPAAANQAAAOAEAOQAEAOAIAMQAHALAMAHQAMAIAPAAQAQAAANgIQALgHAIgLQAIgMAEgOQAEgOAAgOQAAgNgEgPQgEgOgHgMQgIgLgMgHQgMgHgRAAQgQAAgMAHg");
	this.shape_1.setTransform(197.7,3.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("Ag2BWIAAgyIABgmIAAgNIgtgBIAAglIAvAAQACgUAGgUQAHgSAKgQQAMgOAQgJQARgIAYAAQANAAAOAEQAOAEAPAMIgUAiQgLgIgKgDQgJgDgJgBQgIAAgIAAQgJACgGAFQgFAEgEAHQgFAIgDAMQgCALgCASIBSAAIgFAmIhQgBIAAAQIAAA1IAAAYIAAAbIAAAbIAAAbIgoAAIABhJg");
	this.shape_2.setTransform(168.3,-1.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AANCqQgMgFgGgGQgJgIgGgJQgFgJgEgJQgJgXgDgcIAFj2IApAAIgCA/IgCA1IgBArIAAAfIgBA1QAAATAEAOIAFAMQADAFAFAFQAEAFAHADQAHACAIAAIgGAoQgNAAgKgFg");
	this.shape_3.setTransform(153.3,-3.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA8BvIAEgbQgZAPgWAHQgXAGgTAAQgOAAgNgDQgOgDgKgIQgKgIgGgLQgHgMABgQQAAgPAEgNQAFgMAJgKQAHgIAMgHQALgIAMgDQANgFAOgCQANgCAPAAIAXABIAUACIgGgTQgEgJgFgIQgHgHgIgFQgIgEgMgBQgHAAgJACQgKADgMAFQgLAHgOAKQgOAKgQAQIgXgcQASgSASgLQAQgMAPgFQAPgHAOgCQAMgDAKAAQASAAAOAGQAOAHALAKQAKAKAIAPQAHAPAFAQQAFARABARQADARAAARQAAATgDAWQgBAWgFAZgAgIgBQgRAEgMAIQgMAHgGAMQgGALADAPQADALAIAFQAIAEALAAQAMAAANgDIAZgJQANgGAMgHIATgLIAAgTQABgJgCgLIgTgEIgWgBQgRgBgPAEg");
	this.shape_4.setTransform(133.4,2.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AA0CVQAGgTACgSIAEggIABgfQgBgVgHgOQgGgOgJgJQgKgKgKgEQgLgEgKAAQgLABgLAGQgKAEgLAKQgLAJgKASIgBCBIgmABIgDksIAtgCIgCB3QALgMAMgGQAMgHALgEQAMgDAKgCQAWAAATAIQARAIANAOQANAPAIAVQAIAUAAAaIAAAdIgBAcIgDAbQgCAMgDAKg");
	this.shape_5.setTransform(108.6,-1.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgqCjIgSgHQgJgEgJgGQgIgGgIgJQgHgJgEgMIAlgRQAEAJAFAGQAGAGAFAEQAHAEAGACQAGADAGABQAPADAOgCQAOgCAKgGQAKgGAHgHQAHgHAEgHIAIgPIADgNIABgJIAAgZQgMALgMAGQgNAGgLACQgMAEgMAAQgWAAgTgIQgUgIgOgOQgPgPgJgUQgIgVAAgbQAAgaAJgVQAKgVAOgOQAPgOAUgIQASgHATAAQANABANAEQALAEAOAHQANAHAMALIAAgmIAmABIgBDfQAAANgEANQgCAMgHAMQgGAMgIAKQgKALgMAIQgMAIgOAFQgPAFgRACIgDAAQgWAAgTgFgAgdh2QgMAFgIAKQgJALgEAOQgFANAAAQQAAAQAFAOQAEANAJAJQAJAKAMAFQAMAGAOAAQAMAAAMgFQANgEAKgIQAJgIAHgLQAHgLACgNIAAgaQgBgOgIgLQgGgMgLgIQgKgIgMgFQgNgEgMAAQgOAAgMAGg");
	this.shape_6.setTransform(70.8,7.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AhbAwIgCgwIgBglIgBgdIgBgpIAtgBIAAA4QAHgLAIgJQAIgKAKgIQAJgIAKgEQALgFAMgBQAPgBALAEQALADAIAGQAIAHAFAIQAFAJADAJQADAJABAJIABAQQACAhgBAiIgBBGIgogBIADg/QABgfgCgfIAAgJIgCgMIgFgNQgDgGgFgEQgEgFgHgCQgHgDgJACQgPACgPAUQgQAUgSAmIABAzIABAcIAAAPIgqAFIgCg9g");
	this.shape_7.setTransform(45.9,3.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgYCTIABgzIABg9IABhTIAngBIAAAyIgBAqIgBAjIAAAbIgBAqgAgKhdQgFgBgDgFQgEgDgDgFQgCgGAAgFQAAgGACgFQADgFAEgEQADgEAFgCQAFgCAFAAQAFAAAFACQAGACADAEQAFAEACAFQACAFAAAGQAAAFgCAGQgCAFgFADQgDAFgGABQgFACgFAAQgFAAgFgCg");
	this.shape_8.setTransform(28.9,-1.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AA1CVQAFgTADgSIADggQABgQAAgPQgCgVgGgOQgGgOgJgJQgJgKgLgEQgLgEgKAAQgLABgLAGQgKAEgLAKQgLAJgKASIgBCBIgmABIgEksIAtgCIgBB3QALgMAMgGQAMgHALgEQAMgDALgCQAVAAATAIQARAIANAOQAOAPAHAVQAIAUABAaIgBAdIgBAcIgDAbQgCAMgDAKg");
	this.shape_9.setTransform(10.7,-1.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgjBpQgVgJgQgOQgQgQgKgUQgKgVABgYQAAgOADgOQAEgOAIgNQAHgNAKgLQALgKANgIQANgIAPgEQAOgFAPAAQASAAAPAFQAPAEANAHQANAIALALQAKALAHAOIABABIghATIgBAAQgEgLgIgHQgHgIgJgGQgJgFgKgEQgLgCgLAAQgPAAgOAGQgNAGgLALQgKAKgHAOQgFAPAAAPQAAAPAFAOQAHAPAKAKQALAKANAHQAOAFAPAAQALAAAKgCQAJgDAJgFQAIgFAIgHQAHgHAFgJIABgBIAiATIAAABQgIANgLAKQgLALgNAHQgOAHgOAEQgPAEgQAAQgUAAgVgJg");
	this.shape_10.setTransform(-14.7,3.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgXgWIhEABIABgkIBEgBIABhWIAmgDIgBBYIBMgCIgDAkIhKACIgBCpIgoABg");
	this.shape_11.setTransform(-36.4,-0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AA8BvIAEgbQgYAPgYAHQgVAGgUAAQgOAAgNgDQgOgDgKgIQgKgIgGgLQgHgMAAgQQABgPAEgNQAFgMAIgKQAJgIALgHQALgIAMgDQANgFAOgCQANgCAOAAIAYABIAUACIgGgTQgEgJgGgIQgFgHgJgFQgIgEgMgBQgIAAgIACQgKADgMAFQgMAHgOAKQgNAKgQAQIgXgcQASgSASgLQARgMAOgFQAPgHAOgCQAMgDAKAAQASAAAOAGQAOAHALAKQAKAKAIAPQAHAPAFAQQAEARACARQACARAAARQAAATgCAWQgCAWgEAZgAgJgBQgQAEgMAIQgMAHgGAMQgGALADAPQAEALAHAFQAIAEALAAQAMAAANgDIAZgJQANgGAMgHIATgLIABgTQAAgJgBgLIgUgEIgWgBQgRgBgQAEg");
	this.shape_12.setTransform(-58.9,2.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("ABeAuIgBgqIAAgfQgBgQgEgHQgFgIgIAAQgEAAgFADIgKAHIgKALIgJAMIgIAMIgHALIAAASIABAXIABAeIAAAkIglACIgBg9IgBgqIgCgfQgBgQgEgHQgFgIgHAAQgGAAgEADQgGADgFAGIgLAMIgKANIgJANIgHAKIACBnIgnACIgFjRIApgEIAAA5IAOgQQAHgJAIgHQAIgHAJgEQAKgEAMAAQAHAAAJACQAHADAFAFQAGAGAEAIQAFAJABAMIANgQQAGgIAJgGQAHgHAKgEQAJgEALAAQAIAAAJADQAIADAHAGQAGAGAEAKQAEAJAAANIACAkIADAvIAABEIgpACIAAg9g");
	this.shape_13.setTransform(-87.1,2.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgXgWIhDABIAAgkIBEgBIAChWIAlgDIgBBYIBLgCIgCAkIhJACIgCCpIgoABg");
	this.shape_14.setTransform(-123.2,-0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgkBpQgUgJgQgOQgQgQgKgUQgKgVAAgYQAAgOAFgOQAEgOAHgNQAHgNALgLQAKgKAMgIQANgIAPgEQAPgFAQAAQAQAAAQAFQAPAEANAHQANAIAKALQALALAHAOIAAABIggATIAAAAQgGgLgHgHQgHgIgJgGQgJgFgKgEQgKgCgLAAQgPAAgOAGQgOAGgLALQgKAKgHAOQgFAPgBAPQABAPAFAOQAHAPAKAKQALAKAOAHQAOAFAPAAQAJAAAKgCQAKgDAJgFQAIgFAIgHQAHgHAFgJIABgBIAiATIgBABQgHANgLAKQgLALgNAHQgNAHgPAEQgPAEgPAAQgVAAgWgJg");
	this.shape_15.setTransform(-144.7,3.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AA8BvIAEgbQgZAPgXAHQgVAGgUAAQgOAAgNgDQgOgDgKgIQgKgIgGgLQgHgMABgQQAAgPAEgNQAFgMAJgKQAHgIAMgHQALgIAMgDQANgFAOgCQANgCAOAAIAYABIAUACIgGgTQgEgJgFgIQgHgHgIgFQgIgEgMgBQgHAAgJACQgKADgMAFQgLAHgOAKQgOAKgQAQIgXgcQASgSASgLQAQgMAPgFQAPgHAOgCQAMgDAKAAQASAAAOAGQAOAHALAKQALAKAHAPQAHAPAFAQQAEARACARQACARABARQgBATgCAWQgCAWgEAZgAgIgBQgRAEgMAIQgMAHgGAMQgGALADAPQAEALAHAFQAIAEALAAQAMAAANgDIAZgJQANgGAMgHIATgLIAAgTQABgJgBgLIgUgEIgWgBQgRgBgPAEg");
	this.shape_16.setTransform(-170,2.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AACAeIg6BLIgpgLIBLheIhOheIApgKIA8BKIA7hLIAlAOIhIBbIBMBcIgkAOg");
	this.shape_17.setTransform(-192.8,3.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AgrBpQgUgIgOgOQgOgOgHgVQgHgUAAgZQAAgXAIgVQAJgVAOgQQAPgPATgKQAUgIAVgBQAVABASAHQASAIANAPQAOANAJATQAJATADAWIipAZQACANAFALQAGALAIAIQAJAHALAFQALADAMAAQALAAAKgCQAKgEAJgGQAIgGAHgKQAGgIADgMIAmAHQgGARgKAPQgKAPgNAKQgNALgQAFQgQAGgSgBQgYAAgUgHgAgQhLQgJADgKAGQgJAIgHALQgHALgDARIB0gOIgBgEQgHgTgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_18.setTransform(-215.2,3.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(4,1,1).p("EgiOgE/MBEdAAAQBrAABMBMQBNBNAABrIAAB3QAABshNBMQhMBMhrAAMhEdAAAQhrAAhNhMQhMhMAAhsIAAh3QAAhrBMhNQBNhMBrAAg");
	this.shape_19.setTransform(1.8,1.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("EgiOAFAQhrAAhNhNQhMhMAAhrIAAh3QAAhrBMhNQBNhMBrAAMBEdAAAQBrAABMBMQBNBNAABrIAAB3QAABrhNBMQhMBNhrAAg");
	this.shape_20.setTransform(1.8,1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-245.2,-33.7,494.2,69);


(lib.quscopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape.setTransform(49.3,75.2,0.965,0.935,0,-31.5,147.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_1.setTransform(49,68.2,0.965,0.935,0,-31.5,147.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_2.setTransform(48,60.9,0.965,0.935,0,-31.5,147.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_3.setTransform(47.5,52.9,0.965,0.935,0,-31.5,147.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_4.setTransform(48,46.4,0.965,0.935,0,-31.5,147.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_5.setTransform(48.5,39.3,0.965,0.935,0,-31.5,147.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_6.setTransform(52.4,33.8,0.965,0.935,0,-31.5,147.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("ABRChIgCAAQgegRgRgpIAAgBQgQg1gIgXIAAgBIgWhJQgPgogTgaQgQgNgHgDIgCgBQgFgFgEgFQgEgIADgGIAAAAQACgJANABQAGABAIAEQALAFALAbQAVAdAOArIAAgBQAEAKATBBIAZBMIAAAAQANAgAWAOIAAAWIgFgDg");
	this.shape_7.setTransform(102,57);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],2.9,-17.5,3.3,13.5).s().p("AADBaIgFgdQgHgjAAgsIgBgXQABgnAHgfIAFAFIAIAFIAAAUIgCAAIgBAAQgDAAgCADQgDAEAAAFIgBAhQgBAiAEAcQACAXAHAYIAAAnIgIgWg");
	this.shape_8.setTransform(109,123.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],0.5,-16.9,0.9,13.9).s().p("AgBAfQgFgdABghIABghQABgGADgDQABgDADAAIABAAIACAAIAACaQgHgYgBgXg");
	this.shape_9.setTransform(109.4,123);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.6,0.2,18.6).s().p("AATCVIgIgQQgbg3gGhAQgEgZACgdQADgVAGgYQALgrAHgHIAIgKIADgCQADgCACABIABAAQABAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAFAEgIAQIgCADQgIAFgKAmQgGAWgDAVQgEAbADAYQADAqAOA0IAMAqIgBAAg");
	this.shape_10.setTransform(106.7,94.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],4.7,-18.7,5.1,17.7).s().p("AAaCqIgHgFQgMgMgJgTIgEgIQgYg2gChMQAAgYACgXIABgOQAGglANgfQAKgXAKgNIAEgEQABAHAEAEIADACIALABIAAFNIgHgEgAAOidIgDACIgIAKQgHAHgLArQgHAXgCAWQgCAdAEAZQAGA/AbA4IAIAPIABABIgMgqQgOg0gDgqQgDgZAEgaQADgVAGgWQAKgmAIgFIACgDQAIgQgFgEQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAABIgBgBIgBAAIgEACg");
	this.shape_11.setTransform(106.7,95.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#D42400","#A30000"],[0,1],-1.9,-1.3,0,-1.9,-1.3,3.3).s().p("AgHATQgFgCAAgHQgBgIAEgGQACgIAHgDIAAAAQAHgEAEABIAAABIACAEIAAAJIgDAJQgDAHgGAFIgBABIgFABIgCAAgAgFAPQgCgDAAgFQAAgGAEgFIADgFIACgDQAGgEADACIgBgBQgDgCgHADIAAAAQgFAEgCAFQgEAGACAFQAAAGAEADIAAAAg");
	this.shape_12.setTransform(108.7,74.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#FFCC00","#FF9900"],[0,1],-0.1,0,0,-0.1,0,1.3).s().p("AgJAHQgCgFAEgGQACgFAFgDIAAgBQAHgDADACIABABQgDgCgGAEIgCADIgDAFQgEAFAAAGQAAAFACADQgEgDAAgGg");
	this.shape_13.setTransform(108.7,74.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],2.3,-3.7,2.2,3.8).s().p("AAAAhIgCgCQgEgEgCgIQgCgGAAgJQAEABAEgCIACgBQAEgEADgHIAEgKIAAA1IgLgBgAAJggIgBgBIACAAIABABIAAAEIgCgEg");
	this.shape_14.setTransform(109,76.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FF6600","#FF3300"],[0,1],-7.6,-6.9,7.7,6.8).s().p("AggB2QgqgHgVgnQgTgoANgxQAOgxAngeIAGgEQgfAZgLAnQgMAnASAgQASAhAkAGQAjAFAhgYQAigYAMgoQALgogSghQgQgcgdgIQAmAHAUAmQAUAogOAwQgOAxgnAfQghAYghAAIgPgBg");
	this.shape_15.setTransform(81.9,135.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-2.3,-9.1,2.4,9.1).s().p("AgaBiQgkgGgSghQgSggAMgnQALgnAfgZIACgCQAigYAjAGIAJABQAdAIAQAdQASAhgLAnQgMApgiAYQgbAUgcAAIgNgBgAgjhCQgdAVgJAiQgKAiAPAcQAPAcAfAEQAeAFAcgVQAcgUAKgjQAKghgPgcQgPgbgfgGIgLgBQgYAAgXARg");
	this.shape_16.setTransform(82.1,132.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#FFCC00","#FF9900"],[0,1],-5.5,-4.9,5.5,4.9).s().p("AgWBTQgfgFgPgcQgPgcAKghQAJgiAdgVQAcgVAeAFQAfAFAPAcQAPAbgKAiQgKAigcAVQgXARgXAAIgMgBgAgQhFQgYAGgOAXQgMAWAHAXQAGAZAVAMQAWAMAXgHQAYgHANgWQANgVgGgZQgHgYgVgNQgOgHgOAAQgIAAgJADg");
	this.shape_17.setTransform(82.1,132.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FF6600","#FF3300"],[0,1],1.5,-6.5,-1.5,6.6).s().p("AglA8QgdgMgJgaQgJgZAQgYQAQgZAggJQAegKAdAMIAFADQgYgLgYAIQgaAHgNAVQgNAWAIAUQAIAXAXAKQAXAKAZgHQAagHANgWQANgUgIgWIgBgBIACAEQAJAYgQAZQgQAZgfAJQgOAEgNAAQgRAAgPgGg");
	this.shape_18.setTransform(65.9,117.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],4.3,-4.3,-4.3,4.4).s().p("AgeAyQgXgKgIgWQgIgVANgVQANgVAagHQAYgIAYALQAWAKAIAVIABABQAIAVgNAVQgNAVgaAIQgKACgJAAQgPAAgOgGgAgSgrQgUAHgKAQQgKASAGARQAGATAVAJQAUAJAVgHQAVgGALgRQALgSgGgSQgHgTgUgJIgKgDQgHgEgJAAQgKABgIAFg");
	this.shape_19.setTransform(67.2,117.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FFCC00","#FF9900"],[0,1],1.1,-4.8,-1,4.7).s().p("AgZAqQgVgJgGgTQgGgRAKgSQAKgQAUgHIgKAHQgLAMgBAQQAAAPALALQALALAQAAQAPgBAMgLQAMgMAAgPQAAgRgLgLQgEgEgGgDIAKADQAUAJAHATQAGASgLASQgLARgVAGQgKADgIAAQgMAAgLgFg");
	this.shape_20.setTransform(67.2,117.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.lf(["#FF6600","#FF3300"],[0,1],14.2,-9.1,-13.6,9.2).s().p("AhDCVQhEgmgbhGQgchHAeg9QAdg9BGgRQBDgQBFAmQBEAmAbBGIAGARIgBgCQgWg8g4gfQg3ggg5AQQgcAHgVARQgLAJgIALQgIALgGANQgaA1AWA6QAVA7A4AgQA3AfA5gQQA6gPAZg1IgDAGQgdA9hFARQgVAFgVAAQgvAAgwgag");
	this.shape_21.setTransform(66.8,61.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.lf(["#FFCC00","#FF9900"],[0,1],10,-6.6,-9.9,6.6).s().p("AgzBtQgvgbgTgzQgSgxAWgsIAJgRQAHgKAJgIQATgRAagHQAwgNAwAbQAvAaASAzQASAygVAtQgWAsgxANQgQAEgQAAQgfAAgggRgAAShfQgnACgcAcQgSASgGAVIgBABQgEANAAAOQAAAnAbAbQAcAbAngBQAogBAcgcQAdgdAAgmQABgmgcgcQgcgbgmAAIgCAAg");
	this.shape_22.setTransform(70.4,62.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],14.8,-0.3,-14.8,0.4).s().p("Ag8CAQg3gggWg7QgVg6AZg1QAGgNAIgLQAJgLAKgJQAVgRAdgHQA4gQA4AgQA4AfAWA8IABACQAQA5gWAzQgZA1g5APQgTAFgTAAQglAAgmgUgAgqh5QgbAHgSAQQgJAIgHALIgJAQQgWAtASAxQASAyAvAbQAwAbAwgNQAwgNAWgtQAVgtgRgxQgTgzgvgbQgggSggAAQgQAAgPAFg");
	this.shape_23.setTransform(70.4,62.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.rf(["#FFCC00","#FF3366"],[0,1],1.6,-0.4,0,1.6,-0.4,50.5).s().p("AkgERQgLgWgOgQQgIgLgDgOQgFgTAGgOIARgkQAGgLgLgXQgLgWANgXQALgUgPgSQgQgTAIgTQAIgNABgHQABgLgMgaQgKgWgZgPQgWgLAAgKQAAgJAXgSQAWgRABgYQACg9AogpQA5g6B0AYQAtAJAtAYIDICjQAzA5A2BKIBCBgQAWAgARAqQgLANgJAWQgOAfgFAmQgVhAgchDQgihKgkg+IgBAAQhRiGhthYQh0hfhiADIgEABQgeACgOAXIAHgKQgYATgJAnQgFAWgDAkQgCARgQAcQgPAYgBAQQgBAOANAWQAPAcAGARQAHAYAAAOQAAAKgDASQgDAPAKAVQAJATgGAPQgGATAIAVQAHAVgCAKQgGAYgBAJQgCANADAPQAEARAHAMIAQAeQAJASAJALQg4gQgbg0g");
	this.shape_24.setTransform(69.6,62.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.rf(["#FF3333","#330066"],[0,1],3.5,-1.2,0,3.5,-1.2,45.8).s().p("AjNFKQgQgQgOgZIgPgfQgHgLgEgRQgDgQABgNIAEgSIAAgCIADgMQADgKgIgWQgHgVAGgRQAFgQgJgTQgKgVADgPQAEgSAAgKQgBgOgHgYQgFgRgQgdQgMgVABgPQAAgPAPgZQAQgaADgSIAGgwQAFggAOgTQANgSAUgJIADAAQBigDB0BfQBtBXBRCHIABAAQAkA9AiBKQAdBDAUBAIgBAOQg7gEhIAKQgrAGg+ANIhEAPIgYAFIgIACQg6AMglAFQgdADgZAAIgdgBgAh5jHQhFARgeA9QgdA9AbBHQAcBGBEAmQBEAmBEgRQBFgRAeg9IACgGQAXgzgRg4IgGgSQgbhGhEgmQgwgbgwAAQgUAAgVAFg");
	this.shape_25.setTransform(72.8,64.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#330066").s().p("AAkGhQgWgMgGgZQgGgYAMgWQANgXAZgGQAYgHAVALQAVANAHAYQAGAZgMAWQgNAWgYAHQgJADgJAAQgOAAgOgIgAhuDtQgLgLAAgQQABgQALgMIAKgHQAIgFAKgBQAKAAAHAEQAGADAEAEQALALAAARQAAAQgMAMQgMALgQABIgBAAQgPAAgLgLgAhjkFQgcgbABgnQAAgOAEgNIAAgBQAHgWARgSQAdgcAogBQAmgBAdAbQAcAcgCAmQAAAngcAdQgdAcgnACQgoAAgbgbg");
	this.shape_26.setTransform(75.3,95.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.rf(["#FF3333","#330066"],[0,1],1.5,0.2,0,1.5,0.2,44.2).s().p("ABCFTQgEgBgKgKQgGgHgOAJQgQAKgGgCQgGgCgFgNQgGgOgKgDQgJgEgXAKQgWAKgLgFQgKgEgKgQQgLgUgKgIQgKgGgSADQgTADgGgFQgGgFgEgPQgDgQgKgGQgIgFgXAEQgUACgIgHQgGgGAJgiQALgpgEgYIgchRQgPgtANg0QAqijC+hFIBDgOQA+gNArgGQBIgKA7ADQgDAXABAYQACBNAYA2IAEAJQgzAygTBmIgQBfQgLAxgVAVQgLALgCAUIgBAaIgKAeIgFAOQgGATgHANQgHAPgJAIQgMAKgMAHQgHADgFAAQgEAAgDgCgAAAADIgDACIgFAEQgoAdgNAyQgOAyAUAnQAUAnApAHQAqAHAogeQAogeANgyQAOgxgUgoQgTglgngHIgIgCIgOgBQgdAAgaATgAiNiAQggAJgQAYQgQAZAJAaQAJAZAdAMQAcALAggIQAfgJAQgZQAQgZgJgZIgCgEQgIgVgWgKIgFgDQgQgGgRAAQgNAAgOAEg");
	this.shape_27.setTransform(78,124.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFCC00").s().p("AAjE9IgDgBIgBAAQgJgLgJgSIgPgeQgHgMgEgQQgDgQACgNQABgJAGgXQACgLgHgVQgIgVAGgSQAGgQgJgTQgKgVADgPQADgSAAgJQAAgOgHgYQgGgSgPgcQgNgVABgPQABgQAPgYQAQgbACgSQADgkAFgVQAJgnAXgUIgHAKQgIARgCAeIgCA2QgDARgQAbQgPAZAAAPQgBAPAMAVQAQAdAFARQAHAYABAOQAAAKgEASQgDAPAJAUQAJAUgFAPQgGASAHAVQAIAWgDAJIgBABIgCALIAAADIgEASQgBANADAQQAEARAHALIAPAeQAOAaAQAPQgPgBgNgEg");
	this.shape_28.setTransform(45.9,65);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#D31D3A").s().p("AhMC4IACgaQABgUALgLQAWgVALgxIAQheQARhnAzgyQAKASAMAMQgHAfAAAnQgPA6gcBEIgcBgQgNAegaAHQgcAGgHAPIgBABIAAgHg");
	this.shape_29.setTransform(101,128.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#4413F3").s().p("ACFFQQgDgCgJgPQgGgIgPALQgQAOgHgDQgFgCgEgRQgFgRgKgGQgJgEgXAQQgYAPgLgFQgJgEgJgUQgKgWgMgIQgKgHgVAHQgVAIgGgEQgHgFgCgSQgCgQgMgGQgJgEgbAKQgaAJgLgFQgHgEANgnQAQgvgEgcQgCgLgdhSQgSg0AOg6QArimC9hIIAZgGQi9BFgqCjQgNA0APAuIAcBQQAEAYgLApQgJAiAGAGQAIAHAUgCQAXgEAIAFQAKAGADAQQAEAPAGAFQAGAFATgDQASgDAKAGQAKAIALAUQAKAQAKAEQAKAFAWgKQAXgKAJAEQAKADAGAOQAGANAGACQAGACAQgKQAOgJAGAHQAKAKAEABQAHAEAMgFQAMgHAMgKQAJgIAHgPIgEALQgUAygaANQgJAEgGAAQgEAAgCgCgADoC4IAAAHIgKAXIAKgeg");
	this.shape_30.setTransform(70.1,128.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy4, new cjs.Rectangle(31.5,28.4,79.1,133.8), null);


(lib.InstructionText_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgjA4IABggIAAgZIAAgIIgdgBIAAgYIAeAAQABgNAFgNQADgMAIgKQAHgJAKgGQALgGAPAAQAJAAAJADQAKADAJAIIgMAWQgIgFgGgCIgLgDQgHAAgEABQgGABgDADQgFACgCAFQgDAFgCAIQgCAHAAALIA0ABIgCAZIg0gBIAAAKIAAAiIAAAQIAAARIAAASIAAARIgaABIAAgwg");
	this.shape.setTransform(216.9,-15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZBFQgNgFgJgKQgJgKgGgOQgFgOAAgQQAAgQAFgOQAGgOAJgJQAJgKANgGQAMgEANgBQAOAAAMAGQANAFAJALQAJAKAGANQAFAOAAAPQAAAQgFANQgGAOgJAKQgJAKgNAGQgMAFgOAAQgNAAgMgFgAgSgvQgIAFgFAIQgEAHgDAKQgCAIAAAJQAAAJACAJQADAKAFAHQAFAHAIAFQAIAFAJgBQAKABAJgFQAHgFAFgHQAGgHACgKQACgJAAgJQAAgJgCgIQgDgKgFgHQgFgIgHgFQgIgEgLAAQgKAAgIAEg");
	this.shape_1.setTransform(202.2,-11.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgjA4IABggIAAgZIAAgIIgdgBIAAgYIAeAAQABgNAFgNQAEgMAHgKQAHgJAKgGQAMgGAPAAQAIAAAKADQAJADAJAIIgMAWQgIgFgGgCIgLgDQgHAAgEABQgGABgDADQgFACgCAFQgCAFgDAIQgCAHAAALIA1ABIgDAZIg0gBIAAAKIAAAiIAAAQIAAARIAAASIAAARIgaABIAAgwg");
	this.shape_2.setTransform(183,-15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAIBvQgHgDgEgFQgFgFgEgFIgGgMQgHgPgBgSIADihIAaAAIgBAqIgBAiIgBAcIAAAVIAAAiQAAALACAKQABAEADAEIAFAHIAHAEQAEACAFAAIgDAZQgJAAgHgCg");
	this.shape_3.setTransform(173.2,-16.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAoBIIABgSQgQAKgPAFQgNAFgNAAQgKAAgIgDQgIgDgHgEQgGgFgFgIQgDgHgBgLQAAgKADgIQAEgIAFgGQAFgFAIgFQAGgFAJgCQAJgDAIgBQAJgCAIABIAQAAIANABIgEgLQgDgHgDgFQgEgFgGgDQgFgDgHAAIgKABQgHACgIAEQgHAEgKAGQgIAHgLAKIgPgSQAMgLALgIQAMgHAJgEQAKgEAIgCIAPgBQALAAAJADQAKAFAHAHQAGAGAFAKQAFAJADALQADALACALQABAKAAAMIgBAaIgFAfgAgGAAQgKACgIAFQgIAFgEAIQgDAHACAJQACAIAFADQAFADAHAAQAIAAAJgCIAQgHIAPgIIANgHIAAgMIAAgNIgOgDIgNgBQgLABgLACg");
	this.shape_4.setTransform(160.4,-12.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAiBhQAEgMABgMIADgVIAAgUQgBgOgEgJQgEgJgGgGQgGgGgHgCQgHgDgHAAQgGAAgHAEQgHADgHAHQgHAFgGAMIgBBUIgZAAIgCjDIAdgBIgBBOQAHgIAIgEIAPgHQAIgDAHAAQAOAAALAFQAMAFAIAJQAJAKAFANQAFANAAARIAAATIgBASIgCARIgDAPg");
	this.shape_5.setTransform(144.2,-15);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgbBqIgMgEQgGgDgGgEIgKgKQgFgGgDgIIAZgKQADAFADAEQADAEAEADIAIAEIAIADQAKACAJgCQAJgBAGgEQAHgEAEgEIAIgKIAFgJIACgJIAAgGIAAgQQgHAHgJAEIgPAGQgIACgHAAQgOAAgNgFQgNgFgKgKQgJgJgFgNQgFgOgBgRQAAgRAHgOQAFgNAKgKQAKgJAMgFQANgFAMAAQAHABAJADIAQAHQAJAEAHAIIAAgZIAaABIgBCQQAAAJgCAIQgDAIgDAIQgEAHgGAHQgHAHgHAFQgIAGgKADQgJADgLABIgDAAQgNAAgMgDgAgShMQgIADgGAHQgFAHgEAJQgDAJAAAKQAAAKADAJQAEAJAFAFQAGAHAIADQAIAEAJAAQAIAAAIgDQAHgDAGgFQAHgFAEgHQAFgHABgJIAAgRQgBgJgFgHQgEgIgHgFQgGgFgIgDQgJgDgGAAQgKAAgHAEg");
	this.shape_6.setTransform(119.7,-9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag7AgIgBggIAAgYIgBgSIgBgbIAdgBIABAlQAEgHAFgHQAFgGAHgFQAGgFAGgDQAHgDAIgBQAJAAAIACQAHACAFAEQAFAFADAFIAFAMIADALIABALIABArIgBAuIgagBIACgpQAAgVgBgTIAAgGIgBgIIgDgIQgCgEgDgDQgDgDgFgCQgEgBgGABQgKABgJAOQgLANgMAYIABAhIABASIAAAKIgbADIgCgng");
	this.shape_7.setTransform(103.5,-12);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPBfIAAggIABgoIABg2IAZgBIgBAhIAAAbIgBAWIAAASIAAAbgAgGg8IgGgDIgEgGIgBgHQAAgEABgEIAEgFIAGgEIAGgCQAEABADABIAGAEIAEAFQABAEAAAEIgBAHIgEAGIgGADIgHABIgGgBg");
	this.shape_8.setTransform(92.4,-15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAiBhQAEgMABgMIADgVIAAgUQgBgOgEgJQgEgJgGgGQgGgGgHgCQgHgDgHAAQgGAAgHAEQgHADgHAHQgHAFgGAMIgBBUIgZAAIgCjDIAdgBIgBBOQAHgIAIgEIAPgHQAIgDAHAAQAOAAALAFQAMAFAIAJQAJAKAFANQAFANAAARIAAATIgBASIgCARIgDAPg");
	this.shape_9.setTransform(80.6,-15);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgXBEQgNgFgLgKQgKgKgGgNQgHgOABgQQgBgIADgJQADgKAEgIQAFgIAHgHQAGgHAJgFQAIgFAKgDQAJgDAKAAQALAAAKADQAKACAJAFQAIAFAHAIQAGAHAFAJIABABIgWAMIAAAAQgEgHgEgFQgFgFgGgEQgFgDgHgCQgHgCgHAAQgJAAgKAEQgJAEgGAHQgHAGgEAKQgEAJAAAJQAAAKAEAKQAEAJAHAHQAGAGAJAEQAKAEAJAAQAHAAAGgBIAMgFQAGgEAEgEQAFgFADgGIABAAIAWAMIAAABQgFAIgIAHQgGAGgJAFQgIAFgKACQgKADgKAAQgNAAgOgGg");
	this.shape_10.setTransform(64.1,-11.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgPgOIgsABIABgXIAsgBIABg4IAYgCIgBA5IAygBIgDAXIgvACIgBBtIgaABg");
	this.shape_11.setTransform(50,-14.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAoBIIABgSQgPAKgQAFQgNAFgNAAQgJAAgJgDQgIgDgHgEQgGgFgEgIQgFgHAAgLQAAgKADgIQADgIAGgGQAFgFAIgFQAGgFAJgCQAJgDAIgBQAJgCAIABIAQAAIANABIgEgLQgDgHgDgFQgEgFgGgDQgFgDgIAAIgJABQgHACgIAEQgHAEgKAGQgIAHgLAKIgPgSQAMgLALgIQAMgHAJgEQAKgEAIgCIAPgBQAMAAAIADQAKAFAHAHQAGAGAGAKQAEAJADALQADALACALQABAKAAAMIgBAaIgFAfgAgFAAQgLACgIAFQgHAFgFAIQgEAHADAJQACAIAFADQAFADAHAAQAHAAAJgCIARgHIAPgIIANgHIAAgMIgBgNIgMgDIgOgBQgMABgJACg");
	this.shape_12.setTransform(35.4,-12.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA9AeIAAgbIAAgUQgBgLgDgEQgDgGgFAAQgDAAgDACIgHAGIgGAHIgGAHIgFAIIgEAHIAAALIABAPIAAATIAAAZIgYAAIAAgnIgBgbIgBgUQgBgLgDgEQgDgGgFAAQgDAAgDACIgHAHIgIAHIgGAJIgFAJIgFAFIABBEIgZAAIgEiHIAbgCIAAAkIAJgKIAKgKQAFgEAHgDQAFgDAIAAIAKABQAFACADAEQAEADADAGQADAFAAAIIAJgKIAJgKQAGgEAGgCQAFgDAHAAQAGAAAFABQAGADAEADQAEAEADAHQADAGAAAJIABAXIACAeIAAAtIgbAAIAAgng");
	this.shape_13.setTransform(17.1,-12.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgOgOIgsABIAAgXIAsgBIABg4IAYgCIgBA5IAxgBIgBAXIgwACIgBBtIgaABg");
	this.shape_14.setTransform(-6.4,-14.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgXBEQgNgFgKgKQgLgKgGgNQgHgOAAgQQABgIACgJQADgKAEgIQAFgIAHgHQAGgHAJgFQAIgFAKgDQAJgDAKAAQALAAAKADQAKACAJAFQAIAFAHAIQAGAHAFAJIABABIgWAMIAAAAQgDgHgFgFQgEgFgHgEQgFgDgHgCQgHgCgHAAQgJAAgKAEQgJAEgGAHQgHAGgEAKQgEAJAAAJQAAAKAEAKQAEAJAHAHQAGAGAJAEQAKAEAJAAQAHAAAGgBIAMgFQAGgEAEgEQAFgFADgGIABAAIAWAMIAAABQgFAIgIAHQgGAGgJAFQgJAFgJACQgKADgKAAQgNAAgOgGg");
	this.shape_15.setTransform(-20.4,-11.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAnBIIACgSQgPAKgPAFQgOAFgNAAQgJAAgJgDQgIgDgHgEQgHgFgDgIQgEgHgBgLQABgKADgIQADgIAFgGQAFgFAHgFQAIgFAIgCQAIgDAKgBQAHgCAKABIAPAAIANABIgEgLQgCgHgEgFQgEgFgFgDQgGgDgIAAIgKABQgGACgHAEQgJAEgIAGQgKAHgJAKIgQgSQAMgLALgIQALgHALgEQAJgEAJgCIAOgBQALAAAKADQAJAFAHAHQAHAGAFAKQAEAJADALQADALABALQACAKAAAMIgCAaIgEAfgAgFAAQgLACgIAFQgHAFgEAIQgFAHADAJQACAIAFADQAFADAIAAQAHAAAIgCIAQgHIAQgIIANgHIAAgMIgBgNIgMgDIgOgBQgLABgKACg");
	this.shape_16.setTransform(-36.8,-12.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AABATIglAyIgbgIIAxg9Igzg9IAbgGIAnAwIAmgxIAYAKIgvA6IAyA8IgYAJg");
	this.shape_17.setTransform(-51.6,-12);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_18.setTransform(-66.2,-11.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_19.setTransform(-88.4,-11.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAiBhQAEgMABgMIADgVIAAgUQgBgOgEgJQgEgJgGgGQgGgGgHgCQgHgDgHAAQgGAAgHAEQgHADgHAHQgHAFgGAMIgBBUIgZAAIgCjDIAdgBIgBBOQAHgIAIgEIAPgHQAIgDAHAAQAOAAALAFQAMAFAIAJQAJAKAFANQAFANAAARIAAATIgBASIgCARIgDAPg");
	this.shape_20.setTransform(-104.6,-15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgOgOIgsABIAAgXIAsgBIABg4IAYgCIgBA5IAygBIgCAXIgwACIgBBtIgaABg");
	this.shape_21.setTransform(-119.4,-14.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgOgOIgsABIAAgXIAsgBIABg4IAYgCIgBA5IAxgBIgBAXIgwACIgBBtIgaABg");
	this.shape_22.setTransform(-138.6,-14.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgXBEQgNgFgKgKQgLgKgGgNQgHgOAAgQQABgIACgJQADgKAEgIQAFgIAHgHQAGgHAJgFQAIgFAKgDQAJgDAKAAQALAAAKADQAKACAJAFQAIAFAHAIQAGAHAFAJIABABIgWAMIAAAAQgEgHgEgFQgEgFgHgEQgFgDgHgCQgHgCgHAAQgJAAgKAEQgIAEgHAHQgHAGgEAKQgEAJAAAJQAAAKAEAKQAEAJAHAHQAHAGAIAEQAKAEAJAAQAHAAAGgBIAMgFQAGgEAEgEQAFgFADgGIABAAIAWAMIAAABQgFAIgIAHQgGAGgJAFQgJAFgJACQgKADgKAAQgOAAgNgGg");
	this.shape_23.setTransform(-152.6,-11.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_24.setTransform(-168.2,-11.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAIBvQgHgDgEgFQgFgFgEgFIgGgMQgHgPgBgSIADihIAaAAIgBAqIgBAiIgBAcIAAAVIAAAiQAAALACAKQABAEADAEIAFAHIAHAEQAEACAFAAIgDAZQgJAAgHgCg");
	this.shape_25.setTransform(-178.8,-16.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_26.setTransform(-190.9,-11.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgQBmQgKgCgJgEQgJgDgJgGIgSgLIATgWQAKAIAJAGIASAIQAJACAHABQAJABAIgCQAIgCAGgEQAGgDADgFQAEgFABgGQAAgEgCgFQgCgDgEgEIgJgFIgMgFIgMgDIgMgCIgPgEIgQgFQgHgEgHgFQgHgEgFgHQgFgIgDgJQgCgKABgMQAAgKAEgIQADgIAGgHQAFgGAIgFQAHgEAIgEIARgEIAQgBQAMAAAMADIALAEIALAEIALAHIALAJIgOAWIgJgIIgJgGIgJgEIgJgDQgKgDgJAAQgKAAgKAEQgJADgGAGQgGAFgDAGQgDAIAAAGQAAAHAEAFQADAHAHAEQAHAGAJADQAKADAJACQAKABAJACQAJABAIAEQAJAEAHAFQAHAFAFAGQAFAHACAIQACAIgBAKQgCAJgDAHQgEAHgGAFQgFAFgHAEQgHADgIACIgPADIgPABQgJAAgJgCg");
	this.shape_27.setTransform(-207,-14.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAjLgDqMhGVAAAQhhAAhFBFQhFBFAABgQAABhBFBFQBFBFBhAAMBGVAAAQBhAABFhFQBFhFAAhhQAAhghFhFQhFhFhhAAg");
	this.shape_28.setTransform(1,-14.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF3333").s().p("EgjKADrQhhgBhFhFQhEhEAAhhQAAhgBEhFQBFhEBhAAMBGVAAAQBhAABFBEQBFBFgBBgQABBhhFBEQhFBFhhABg");
	this.shape_29.setTransform(1,-14.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mc, new cjs.Rectangle(-249.1,-39.5,500.1,49.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.inside_fruitcopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.3,-13.2,0,8.7).s().p("AgFAyQAEgYAAgSQABgTgCgYIgCgVQgBgEACgEQABgCACgCQAFAAABAJIACAYQABAYgCATQgCAWgFAVQgFASgCAAIACgTg");
	this.shape.setTransform(-10.5,29.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.9,-13.2,-0.6,8.8).s().p("AgUBMIAAgaIABADQAFASADAAIgEgUQgEgRgBgPIAAg0IABgWQABgHgCgCIAAgPQAGACAGAAQAMAAAKgLQAHAdAAAlQAAAtgHAhQgJAhgLAAIAAAAQgIAAgGgNgAAEg/QgCAEABADIACAVQACAYgBATQAAASgEAZIgCASQACAAAFgSQAGgVACgVQACgUgBgYIgCgXQgBgJgGAAQgCABgBADg");
	this.shape_1.setTransform(-11.3,29.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.7,-13.4,-0.4,8.5).s().p("AgCAyIgBgDIAAgfQABAPACARIAFAUQgDgBgEgRgAgDhDQABACAAAHIgBAWg");
	this.shape_2.setTransform(-13,30.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-2.5,-13.5,-2.2,12.4).s().p("AghB8IAAjuIAIAAIAQAAQgFAEAIAMQAEAEAKAcQALAigCAgQgBAdgKAlIgHAeQAYgtAEgzQADgggLgmQgJgegGgFIgFgIIgFgCQADgDACgHQAJAJAHASQASAmABA2QABA2gQAmQgJATgJAKQgJALgNAAQgGAAgGgCg");
	this.shape_3.setTransform(-10,9.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.1,-12.5,0.2,13.3).s().p("AgBBMQAKglABgeQABgfgLgiQgJgcgFgEQgIgMAFgEIACAAIACgBIAFACIAGAIQAEAFAJAeQAMAmgEAfQgEA0gXAtg");
	this.shape_4.setTransform(-9.3,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,0.9).s().p("AAGAFQAAgDgCgDQgDgEgDgCIgBgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgBAAAAAAIABgBQACgBAEACIAGAGQACAEAAAEQgBAEgCACQABgCAAgEg");
	this.shape_5.setTransform(-10.8,-5.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#D42400","#A30000"],[0,1],1.3,-0.9,0,1.3,-0.9,2.3).s().p("AABAMQgDgDgDgGIgEgIIACgFIABgCIABAAQADgBADACIAAAAQAFACADAFQACAEgBAFQAAAGgDACIgCAAIgEgBgAgCgJIABAAQADACADAFQACACAAAEQAAAEgBABQACgBABgFQAAgDgCgEIgHgHQgDgBgCABIgBAAIABAAIADACg");
	this.shape_6.setTransform(-10.9,-5.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.8,-2.6,-0.9,2.8).s().p("AAIAZIgPAAIgIAAIAAguIAAABIABABIACAAQABgBACgEIABgBIABAAQACAEABABIACABIACgDIAAgBIACABIgBADIgBAEIADAKQADAFAEACQAEADACgCIAAAMQgCAHgDACIgBACIgCgBg");
	this.shape_7.setTransform(-11.8,-4.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,0,-0.4,0.1).s().p("AAAALQgBgBgBgEIgBgDIAAgDIAAgBIAAgBIAAAAIAAgDIAAgGIADAEIADAIIAAABIABAFIAAACIgCADIgCgBg");
	this.shape_8.setTransform(-12.3,-7.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.3,0,0.4,0.1).s().p("AgBALIgBgBIgBgBIAAgIIADgFIAAgDIACgDIABAFIABACIAAABIAAABIAAABIgBADIgBADQgBAFgBAAIgBAAIAAAAg");
	this.shape_9.setTransform(-13.1,-7.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgoB4QgEgCgDABIAAAAIgDgCIgBgFIAAgBIACgCQAUgKAKgcIAOg3QALgxACgFQAIggAOgVQAJgUAGgCQAFgFAFAAIABAAQAIgBACAGQACAEgDAGQgCAEgEAEIgBAAIgQAMQgMAUgJAdIAAAAIgNA1IAAABIgNA3IgBABQgLAfgWANgAg7BxIABgDIAAADIABADIgBAAIgCAAIABgDgAhCBoIADABIgDAGg");
	this.shape_10.setTransform(-6.8,-18.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_11.setTransform(17.6,21.2,0.277,0.277,45);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_12.setTransform(17.6,22,0.416,0.416,77);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_13.setTransform(17.6,22,0.416,0.416,77);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_14.setTransform(18.5,22.4,0.416,0.416,77);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_15.setTransform(6.9,32.7,0.433,0.433,105);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_16.setTransform(6.9,33.5,0.65,0.65,137);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_17.setTransform(6.9,33.5,0.65,0.65,137);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_18.setTransform(7.1,35.1,0.65,0.65,137);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_19.setTransform(30.6,-8.5,0.677,0.677,31);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_20.setTransform(30.7,-13.5,0.677,0.677,31);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_21.setTransform(31.5,-18.6,0.677,0.677,31);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_22.setTransform(31.8,-24.4,0.677,0.677,31);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_23.setTransform(31.4,-29,0.677,0.677,31);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_24.setTransform(31.1,-34.1,0.677,0.677,31);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_25.setTransform(28.3,-37.9,0.677,0.677,31);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_26.setTransform(14.2,-17,0.677,0.677);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_27.setTransform(15.4,-17.1,1.016,1.016,32);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_28.setTransform(15.4,-17.1,1.016,1.016,32);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_29.setTransform(17.7,-18,1.016,1.016,32);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_30.setTransform(32.9,-15.6,0.677,0.677);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_31.setTransform(14.9,-17.2,0.677,0.677);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_32.setTransform(0.5,-19.8,0.677,0.677,31);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_33.setTransform(11.2,-16.4,0.677,0.677);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_34.setTransform(-6.7,30,0.677,0.677);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_35.setTransform(8.9,27.7,0.677,0.677);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_36.setTransform(9.9,29,0.677,0.677);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy5, new cjs.Rectangle(-13.4,-41.7,56.7,96.2), null);


(lib.inside_fruitcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.3,-13.2,0,8.7).s().p("AgFAyQAEgYAAgSQABgTgCgYIgCgVQgBgEACgEQABgCACgCQAFAAABAJIACAYQABAYgCATQgCAWgFAVQgFASgCAAIACgTg");
	this.shape.setTransform(-10.5,29.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.9,-13.2,-0.6,8.8).s().p("AgUBMIAAgaIABADQAFASADAAIgEgUQgEgRgBgPIAAg0IABgWQABgHgCgCIAAgPQAGACAGAAQAMAAAKgLQAHAdAAAlQAAAtgHAhQgJAhgLAAIAAAAQgIAAgGgNgAAEg/QgCAEABADIACAVQACAYgBATQAAASgEAZIgCASQACAAAFgSQAGgVACgVQACgUgBgYIgCgXQgBgJgGAAQgCABgBADg");
	this.shape_1.setTransform(-11.3,29.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.7,-13.4,-0.4,8.5).s().p("AgCAyIgBgDIAAgfQABAPACARIAFAUQgDgBgEgRgAgDhDQABACAAAHIgBAWg");
	this.shape_2.setTransform(-13,30.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-2.5,-13.5,-2.2,12.4).s().p("AghB8IAAjuIAIAAIAQAAQgFAEAIAMQAEAEAKAcQALAigCAgQgBAdgKAlIgHAeQAYgtAEgzQADgggLgmQgJgegGgFIgFgIIgFgCQADgDACgHQAJAJAHASQASAmABA2QABA2gQAmQgJATgJAKQgJALgNAAQgGAAgGgCg");
	this.shape_3.setTransform(-10,9.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.1,-12.5,0.2,13.3).s().p("AgBBMQAKglABgeQABgfgLgiQgJgcgFgEQgIgMAFgEIACAAIACgBIAFACIAGAIQAEAFAJAeQAMAmgEAfQgEA0gXAtg");
	this.shape_4.setTransform(-9.3,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,0.9).s().p("AAGAFQAAgDgCgDQgDgEgDgCIgBgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgBAAAAAAIABgBQACgBAEACIAGAGQACAEAAAEQgBAEgCACQABgCAAgEg");
	this.shape_5.setTransform(-10.8,-5.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#D42400","#A30000"],[0,1],1.3,-0.9,0,1.3,-0.9,2.3).s().p("AABAMQgDgDgDgGIgEgIIACgFIABgCIABAAQADgBADACIAAAAQAFACADAFQACAEgBAFQAAAGgDACIgCAAIgEgBgAgCgJIABAAQADACADAFQACACAAAEQAAAEgBABQACgBABgFQAAgDgCgEIgHgHQgDgBgCABIgBAAIABAAIADACg");
	this.shape_6.setTransform(-10.9,-5.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.8,-2.6,-0.9,2.8).s().p("AAIAZIgPAAIgIAAIAAguIAAABIABABIACAAQABgBACgEIABgBIABAAQACAEABABIACABIACgDIAAgBIACABIgBADIgBAEIADAKQADAFAEACQAEADACgCIAAAMQgCAHgDACIgBACIgCgBg");
	this.shape_7.setTransform(-11.8,-4.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,0,-0.4,0.1).s().p("AAAALQgBgBgBgEIgBgDIAAgDIAAgBIAAgBIAAAAIAAgDIAAgGIADAEIADAIIAAABIABAFIAAACIgCADIgCgBg");
	this.shape_8.setTransform(-12.3,-7.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.3,0,0.4,0.1).s().p("AgBALIgBgBIgBgBIAAgIIADgFIAAgDIACgDIABAFIABACIAAABIAAABIAAABIgBADIgBADQgBAFgBAAIgBAAIAAAAg");
	this.shape_9.setTransform(-13.1,-7.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgoB4QgEgCgDABIAAAAIgDgCIgBgFIAAgBIACgCQAUgKAKgcIAOg3QALgxACgFQAIggAOgVQAJgUAGgCQAFgFAFAAIABAAQAIgBACAGQACAEgDAGQgCAEgEAEIgBAAIgQAMQgMAUgJAdIAAAAIgNA1IAAABIgNA3IgBABQgLAfgWANgAg7BxIABgDIAAADIABADIgBAAIgCAAIABgDgAhCBoIADABIgDAGg");
	this.shape_10.setTransform(-6.8,-18.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_11.setTransform(17.6,21.2,0.277,0.277,45);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_12.setTransform(17.6,22,0.416,0.416,77);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_13.setTransform(17.6,22,0.416,0.416,77);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_14.setTransform(18.5,22.4,0.416,0.416,77);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_15.setTransform(6.9,32.7,0.433,0.433,105);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_16.setTransform(6.9,33.5,0.65,0.65,137);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_17.setTransform(6.9,33.5,0.65,0.65,137);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_18.setTransform(7.1,35.1,0.65,0.65,137);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_19.setTransform(30.6,-8.5,0.677,0.677,31);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_20.setTransform(30.7,-13.5,0.677,0.677,31);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_21.setTransform(31.5,-18.6,0.677,0.677,31);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_22.setTransform(31.8,-24.4,0.677,0.677,31);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_23.setTransform(31.4,-29,0.677,0.677,31);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_24.setTransform(31.1,-34.1,0.677,0.677,31);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_25.setTransform(28.3,-37.9,0.677,0.677,31);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_26.setTransform(14.2,-17,0.677,0.677);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_27.setTransform(15.4,-17.1,1.016,1.016,32);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_28.setTransform(15.4,-17.1,1.016,1.016,32);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_29.setTransform(17.7,-18,1.016,1.016,32);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_30.setTransform(32.9,-15.6,0.677,0.677);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_31.setTransform(14.9,-17.2,0.677,0.677);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_32.setTransform(0.5,-19.8,0.677,0.677,31);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_33.setTransform(11.2,-16.4,0.677,0.677);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_34.setTransform(-6.7,30,0.677,0.677);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_35.setTransform(8.9,27.7,0.677,0.677);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_36.setTransform(9.9,29,0.677,0.677);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy3, new cjs.Rectangle(-13.4,-41.7,56.7,96.2), null);


(lib.bflyTween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgDDeQgxgTgag/IAAAAQgVhGgLgfIAAgBQgZhTgFgNIAAAAQgTg0gZgkQgVgQgKgEIgCgBQgIgGgEgIIAAAAQgGgKAEgIIAAAAQAEgMAQACQAJAAAJAHQAOAGAQAjQAbAmAUA4IgBAAQAFANAaBWIAAgBIAhBkIAAABQAWAzAlAPIAFAEIABACIABgCIAEgDQAmgTASg0IAahmQAWhcADgIIAAAAQAQg6AagoQAQgkAMgGQAJgHAJgBIAAAAQARgCACALQAFAIgFALQgEAIgJAGIAAABQgKAFgUARQgXAkgQA2IAAAAIgZBkIAAAAQgJAfgRBHIAAABQgWA/gwAWQgEACgDgBQgEgCgCgDIgDgFIAAgBIAAABIgDAFQgBAEgEABIgEABIgDgBg");
	this.shape.setTransform(0,0,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.6,-17.8,37.3,35.7);


(lib.bflyTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgDDeQgxgTgag/IAAAAQgVhGgLgfIAAgBQgZhTgFgNIAAAAQgTg0gZgkQgVgQgKgEIgCgBQgIgGgEgIIAAAAQgGgKAEgIIAAAAQAEgMAQACQAJAAAJAHQAOAGAQAjQAbAmAUA4IgBAAQAFANAaBWIAAgBIAhBkIAAABQAWAzAlAPIAFAEIABACIABgCIAEgDQAmgTASg0IAahmQAWhcADgIIAAAAQAQg6AagoQAQgkAMgGQAJgHAJgBIAAAAQARgCACALQAFAIgFALQgEAIgJAGIAAABQgKAFgUARQgXAkgQA2IAAAAIgZBkIAAAAQgJAfgRBHIAAABQgWA/gwAWQgEACgDgBQgEgCgCgDIgDgFIAAgBIAAABIgDAFQgBAEgEABIgEABIgDgBg");
	this.shape.setTransform(0,0,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.6,-17.8,37.3,35.7);


(lib.bflyTween2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgDDeQgxgTgag/IAAAAQgVhGgLgfIAAgBQgZhTgFgNIAAAAQgTg0gZgkQgVgQgKgEIgCgBQgIgGgEgIIAAAAQgGgKAEgIIAAAAQAEgMAQACQAJAAAJAHQAOAGAQAjQAbAmAUA4IgBAAQAFANAaBWIAAgBIAhBkIAAABQAWAzAlAPIAFAEIABACIABgCIAEgDQAmgTASg0IAahmQAWhcADgIIAAAAQAQg6AagoQAQgkAMgGQAJgHAJgBIAAAAQARgCACALQAFAIgFALQgEAIgJAGIAAABQgKAFgUARQgXAkgQA2IAAAAIgZBkIAAAAQgJAfgRBHIAAABQgWA/gwAWQgEACgDgBQgEgCgCgDIgDgFIAAgBIAAABIgDAFQgBAEgEABIgEABIgDgBg");
	this.shape_1.setTransform(0,0,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.6,-17.8,37.3,35.7);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-245.2,-33.7,494.2,69);


(lib.bfkyantenacopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bflyTween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(18.7,17.9,1,1,0,0,0,0,17.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:17.8,scaleX:0.64,scaleY:0.68},7).to({regY:17.9,scaleX:1,scaleY:1},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-17.8,37.4,35.7);


(lib.bfkyantena = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bflyTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(18.7,17.9,1,1,0,0,0,0,17.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:17.8,scaleX:0.64,scaleY:0.68},7).to({regY:17.9,scaleX:1,scaleY:1},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-17.8,37.4,35.7);


(lib.bfkyantena_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance_1 = new lib.bflyTween2_1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(18.7,17.9,1,1,0,0,0,0,17.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regY:17.8,scaleX:0.64,scaleY:0.68},7).to({regY:17.9,scaleX:1,scaleY:1},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-17.8,37.4,35.7);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.quscopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMK/QkjkjgBmcQABmbEjkjQEhkjGcAAIAAfDQmcAAkhkjg");
	mask.setTransform(60.7,84.7);

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.4,-18.6,0,12.2).s().p("AgHBHQAFgiAAgaQABgbgDgiIgCgdQgCgGACgEQACgEAEgDQAGABADANIABAgQACAigDAbQgDAegHAeQgHAagCAAQgBgBAEgZg");
	this.shape.setTransform(114.4,124.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-18.8,0.2,12).s().p("AADBGQgJgegDgcQgEgcAAgiIABghQABgGADgDQACgDAEAAIAAAAQAHAAgBANIgDAgQgBAiABAaQAAAbAIAgIAFAcQgDgCgIgZg");
	this.shape_1.setTransform(110.1,124.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.6,0.2,12.4).s().p("AgaBQQgNgtgBhAQgBg1AIgoQAQAPARgBQARAAAPgPQAJAoAAA0QABBAgKAuQgNAtgQAAIgBAAQgQAAgMgsgAgbhZQgDAEgBAGIgBAgQAAAjAEAcQADAcAKAeQAIAZADABIgFgcQgIggAAgaQgCgbACghIADggQABgNgIgBIAAABIgBAAQgDAAgCACgAARhZQgCAFACAGIACAdQADAigBAbQAAAZgGAiQgDAaABAAQACAAAHgZQAIgeADgeQADgbgCgiIgCghQgCgNgHAAQgEACgCAEg");
	this.shape_2.setTransform(112,124.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.6,0.2,18.6).s().p("AgWAOQgHgtAOg2QALgrAHgHIAIgKQAFgFAEACQABAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAFAEgKATQgIAFgKAmQgPAyAFAsQADAqAOA0IAMAqQgjg+gHhJg");
	this.shape_3.setTransform(106.7,94.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.5,0.2,18.7).s().p("AgCBqQAOgzABgrQACgrgPgwQgOgogGgEQgLgTAHgFIACAAQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIAGADIAJALQAHAGAMArQAQA2gEAsQgGBIghBAg");
	this.shape_4.setTransform(116.2,94.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.9,0.2,17.5).s().p("AgdCjQgOgOgMgZQgYg2gChMQgBhLAXg2QAMgbANgNQACAJAGAEQAFACAUgBIAVAAQgHAGALASQAHAFAOAnQAQAwgCAtQgCApgOA0IgLAqQAjg/AFhIQAFgtgQg2QgNgqgHgHIgJgLIgHgDQAEgEADgJQANAMALAZQAZA1ABBNQABBLgWA2QgMAagNAOQgPAPgSAAIgCAAQgPAAgPgNgAgcCJIgMgqQgPg0gDgqQgFgsAQgyQAKgmAIgFQAKgTgFgEQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEgCgFAFIgIAKQgIAHgLArQgOA2AHAtQAHBJAkA+g");
	this.shape_5.setTransform(111.6,96);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,1.3).s().p("AAIAIQAAgFgDgEQgEgGgEgDQgGgEgBABIABgBQACgBAHACQAEAEAEAFQADAFAAAGQgBAGgDACQACgCgBgFg");
	this.shape_6.setTransform(114,74.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#D42400","#A30000"],[0,1],1.9,-1.3,0,1.9,-1.3,3.3).s().p("AABAQQgEgEgFgHQgEgHgBgGQABgGAEgDIAAAAQAEgCAFADQAHADAEAHQADAHgBAGQAAAJgEACIgDABQgDAAgDgDgAgCgMQAEACAEAGQAEAEAAAFQAAAGgCACQAEgCAAgGQABgFgDgGQgFgGgFgDQgFgCgDABIAAABIAAAAIAGADg");
	this.shape_7.setTransform(113.8,74.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFCC00","#FF9900"],[0,1],-0.1,0,0,-0.1,0,1.3).s().p("AgJAHQgCgFAEgGQACgFAFgEQAHgDADACIABABQgDgCgGAEQgDAEgCAEQgEAFAAAGQAAAFACADQgEgDAAgGg");
	this.shape_8.setTransform(108.7,74.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#D42400","#A30000"],[0,1],-1.9,-1.3,0,-1.9,-1.3,3.3).s().p("AgHATQgFgCAAgHQgBgIAEgGQACgIAHgDQAHgEAEABIAAABQADADAAAGQAAAGgEAHQgDAHgGAFQgDACgDAAIgCAAgAgFAPQgCgDAAgFQAAgGAEgFQACgEADgEQAGgEADACIgBgBQgDgCgHADQgFAEgCAFQgEAGACAFQAAAGAEADIAAAAg");
	this.shape_9.setTransform(108.7,74.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-3.6,-0.1,3.9).s().p("AAXAkIgWAAQgUAAgFgBQgFgFgDgJQgCgGAAgKQAEACAGgEQAFgDADgHQAEgIAAgHQAAgGgCgDIgBAAIAEgBQABADACACQAEABADgIIABgBIABAAQADAJAEgBQACgCABgDIAEABQgEADgBAHQACAGAEAIQAFAHAEADQAGAEAEgDIgBARQgDAJgDAFQgBAAAAAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgCAAg");
	this.shape_10.setTransform(111.4,76.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,-0.1,-0.6,0.1).s().p("AgEAIIAAgEIgBgEIAAgBIAAgCIAAAAIAAgFIABgHIAEAFQADAHACAGIABAIQgBADgDACIgBAAQgCAAgDgIg");
	this.shape_11.setTransform(111.9,71.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.4,-0.2,0.5,0).s().p("AgBAQQgDgCgBgEQgBgDABgFQADgFADgHIACgFIACAHIABAEIAAAAIAAACIAAACIgBAEIgBAEQgDAIgCAAIAAAAg");
	this.shape_12.setTransform(110.8,71.9);

	this.instance = new lib.bfkyantena_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(111.5,56.6,0.95,0.95,0,0,0,18.9,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_13.setTransform(67,116.2,0.396,0.382,0,-46,134);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_14.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_15.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_16.setTransform(65.9,117.8,0.588,0.579,0,-79,102.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_17.setTransform(81.9,131.6,0.602,0.613,0,-106.9,74.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_18.setTransform(82.1,132.8,0.895,0.928,0,-138,41.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_19.setTransform(82.1,132.7,0.895,0.928,0,-138,41.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_20.setTransform(81.9,135,0.895,0.928,0,-138,41.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_21.setTransform(49.3,75.2,0.965,0.935,0,-31.5,147.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_22.setTransform(49,68.2,0.965,0.935,0,-31.5,147.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_23.setTransform(48,60.9,0.965,0.935,0,-31.5,147.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_24.setTransform(47.5,52.9,0.965,0.935,0,-31.5,147.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_25.setTransform(48,46.4,0.965,0.935,0,-31.5,147.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_26.setTransform(48.5,39.3,0.965,0.935,0,-31.5,147.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_27.setTransform(52.4,33.8,0.965,0.935,0,-31.5,147.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_28.setTransform(72.1,62.3,0.95,0.95,0,0,178);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_29.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_30.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_31.setTransform(67.2,61.2,1.448,1.402,0,-32.6,146.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_32.setTransform(45.9,65.1,0.95,0.95,0,0,178);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_33.setTransform(71.2,62.1,0.95,0.95,0,0,178);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_34.setTransform(60.7,53.9,0.965,0.935,0,-31.5,147.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_35.setTransform(76.3,63,0.95,0.95,0,0,178);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_36.setTransform(101.4,127.2,0.95,0.95,0,0,178);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_37.setTransform(79.5,124.8,0.95,0.95,0,0,178);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_38.setTransform(78.2,126.6,0.95,0.95,0,0,178);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_39.setTransform(155.3,112.8,0.389,0.389,45);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_40.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_41.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_42.setTransform(156.5,114.5,0.583,0.583,77);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_43.setTransform(140.3,128.8,0.608,0.608,105);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_44.setTransform(140.2,130,0.912,0.912,137);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_45.setTransform(140.2,130,0.912,0.912,137);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_46.setTransform(140.4,132.2,0.912,0.912,137);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_47.setTransform(173.1,71.2,0.95,0.95,31);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_48.setTransform(173.4,64.2,0.95,0.95,31);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_49.setTransform(174.4,56.9,0.95,0.95,31);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_50.setTransform(174.9,48.8,0.95,0.95,31);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_51.setTransform(174.3,42.3,0.95,0.95,31);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_52.setTransform(173.8,35.2,0.95,0.95,31);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_53.setTransform(169.9,29.9,0.95,0.95,31);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_54.setTransform(150.2,59,0.95,0.95);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_55.setTransform(151.9,59.1,1.425,1.425,32);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_56.setTransform(151.8,59.1,1.425,1.425,32);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_57.setTransform(155.1,57.9,1.425,1.425,32);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_58.setTransform(176.4,60.9,0.95,0.95);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_59.setTransform(151.1,58.8,0.95,0.95);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_60.setTransform(161.6,50.3,0.95,0.95,31);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_61.setTransform(146,59.9,0.95,0.95);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_62.setTransform(120.9,125,0.95,0.95);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_63.setTransform(142.7,121.8,0.95,0.95);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_64.setTransform(144.1,123.6,0.95,0.95);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.instance,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.instance},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy6, new cjs.Rectangle(31.5,24.4,78.8,137.8), null);


(lib.quscopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMK/QkjkjgBmcQABmbEjkjQEhkjGcAAIAAfDQmcAAkhkjg");
	mask.setTransform(60.7,84.7);

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.4,-18.6,0,12.2).s().p("AgHBHQAFgiAAgaQABgbgDgiIgCgdQgCgGACgEQACgEAEgDQAGABADANIABAgQACAigDAbQgDAegHAeQgHAagCAAQgBgBAEgZg");
	this.shape.setTransform(114.4,124.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-18.8,0.2,12).s().p("AADBGQgJgegDgcQgEgcAAgiIABghQABgGADgDQACgDAEAAIAAAAQAHAAgBANIgDAgQgBAiABAaQAAAbAIAgIAFAcQgDgCgIgZg");
	this.shape_1.setTransform(110.1,124.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.6,0.2,12.4).s().p("AgaBQQgNgtgBhAQgBg1AIgoQAQAPARgBQARAAAPgPQAJAoAAA0QABBAgKAuQgNAtgQAAIgBAAQgQAAgMgsgAgbhZQgDAEgBAGIgBAgQAAAjAEAcQADAcAKAeQAIAZADABIgFgcQgIggAAgaQgCgbACghIADggQABgNgIgBIAAABIgBAAQgDAAgCACgAARhZQgCAFACAGIACAdQADAigBAbQAAAZgGAiQgDAaABAAQACAAAHgZQAIgeADgeQADgbgCgiIgCghQgCgNgHAAQgEACgCAEg");
	this.shape_2.setTransform(112,124.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.6,0.2,18.6).s().p("AgWAOQgHgtAOg2QALgrAHgHIAIgKQAFgFAEACQABAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAFAEgKATQgIAFgKAmQgPAyAFAsQADAqAOA0IAMAqQgjg+gHhJg");
	this.shape_3.setTransform(106.7,94.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.5,0.2,18.7).s().p("AgCBqQAOgzABgrQACgrgPgwQgOgogGgEQgLgTAHgFIACAAQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIAGADIAJALQAHAGAMArQAQA2gEAsQgGBIghBAg");
	this.shape_4.setTransform(116.2,94.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.9,0.2,17.5).s().p("AgdCjQgOgOgMgZQgYg2gChMQgBhLAXg2QAMgbANgNQACAJAGAEQAFACAUgBIAVAAQgHAGALASQAHAFAOAnQAQAwgCAtQgCApgOA0IgLAqQAjg/AFhIQAFgtgQg2QgNgqgHgHIgJgLIgHgDQAEgEADgJQANAMALAZQAZA1ABBNQABBLgWA2QgMAagNAOQgPAPgSAAIgCAAQgPAAgPgNgAgcCJIgMgqQgPg0gDgqQgFgsAQgyQAKgmAIgFQAKgTgFgEQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEgCgFAFIgIAKQgIAHgLArQgOA2AHAtQAHBJAkA+g");
	this.shape_5.setTransform(111.6,96);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,1.3).s().p("AAIAIQAAgFgDgEQgEgGgEgDQgGgEgBABIABgBQACgBAHACQAEAEAEAFQADAFAAAGQgBAGgDACQACgCgBgFg");
	this.shape_6.setTransform(114,74.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#D42400","#A30000"],[0,1],1.9,-1.3,0,1.9,-1.3,3.3).s().p("AABAQQgEgEgFgHQgEgHgBgGQABgGAEgDIAAAAQAEgCAFADQAHADAEAHQADAHgBAGQAAAJgEACIgDABQgDAAgDgDgAgCgMQAEACAEAGQAEAEAAAFQAAAGgCACQAEgCAAgGQABgFgDgGQgFgGgFgDQgFgCgDABIAAABIAAAAIAGADg");
	this.shape_7.setTransform(113.8,74.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFCC00","#FF9900"],[0,1],-0.1,0,0,-0.1,0,1.3).s().p("AgJAHQgCgFAEgGQACgFAFgEQAHgDADACIABABQgDgCgGAEQgDAEgCAEQgEAFAAAGQAAAFACADQgEgDAAgGg");
	this.shape_8.setTransform(108.7,74.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#D42400","#A30000"],[0,1],-1.9,-1.3,0,-1.9,-1.3,3.3).s().p("AgHATQgFgCAAgHQgBgIAEgGQACgIAHgDQAHgEAEABIAAABQADADAAAGQAAAGgEAHQgDAHgGAFQgDACgDAAIgCAAgAgFAPQgCgDAAgFQAAgGAEgFQACgEADgEQAGgEADACIgBgBQgDgCgHADQgFAEgCAFQgEAGACAFQAAAGAEADIAAAAg");
	this.shape_9.setTransform(108.7,74.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-3.6,-0.1,3.9).s().p("AAXAkIgWAAQgUAAgFgBQgFgFgDgJQgCgGAAgKQAEACAGgEQAFgDADgHQAEgIAAgHQAAgGgCgDIgBAAIAEgBQABADACACQAEABADgIIABgBIABAAQADAJAEgBQACgCABgDIAEABQgEADgBAHQACAGAEAIQAFAHAEADQAGAEAEgDIgBARQgDAJgDAFQgBAAAAAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgCAAg");
	this.shape_10.setTransform(111.4,76.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,-0.1,-0.6,0.1).s().p("AgEAIIAAgEIgBgEIAAgBIAAgCIAAAAIAAgFIABgHIAEAFQADAHACAGIABAIQgBADgDACIgBAAQgCAAgDgIg");
	this.shape_11.setTransform(111.9,71.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.4,-0.2,0.5,0).s().p("AgBAQQgDgCgBgEQgBgDABgFQADgFADgHIACgFIACAHIABAEIAAAAIAAACIAAACIgBAEIgBAEQgDAIgCAAIAAAAg");
	this.shape_12.setTransform(110.8,71.9);

	this.instance = new lib.bfkyantena_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(111.5,56.6,0.95,0.95,0,0,0,18.9,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_13.setTransform(67,116.2,0.396,0.382,0,-46,134);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_14.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_15.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_16.setTransform(65.9,117.8,0.588,0.579,0,-79,102.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_17.setTransform(81.9,131.6,0.602,0.613,0,-106.9,74.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_18.setTransform(82.1,132.8,0.895,0.928,0,-138,41.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_19.setTransform(82.1,132.7,0.895,0.928,0,-138,41.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_20.setTransform(81.9,135,0.895,0.928,0,-138,41.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_21.setTransform(49.3,75.2,0.965,0.935,0,-31.5,147.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_22.setTransform(49,68.2,0.965,0.935,0,-31.5,147.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_23.setTransform(48,60.9,0.965,0.935,0,-31.5,147.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_24.setTransform(47.5,52.9,0.965,0.935,0,-31.5,147.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_25.setTransform(48,46.4,0.965,0.935,0,-31.5,147.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_26.setTransform(48.5,39.3,0.965,0.935,0,-31.5,147.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_27.setTransform(52.4,33.8,0.965,0.935,0,-31.5,147.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_28.setTransform(72.1,62.3,0.95,0.95,0,0,178);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_29.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_30.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_31.setTransform(67.2,61.2,1.448,1.402,0,-32.6,146.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_32.setTransform(45.9,65.1,0.95,0.95,0,0,178);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_33.setTransform(71.2,62.1,0.95,0.95,0,0,178);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_34.setTransform(60.7,53.9,0.965,0.935,0,-31.5,147.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_35.setTransform(76.3,63,0.95,0.95,0,0,178);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_36.setTransform(101.4,127.2,0.95,0.95,0,0,178);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_37.setTransform(79.5,124.8,0.95,0.95,0,0,178);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_38.setTransform(78.2,126.6,0.95,0.95,0,0,178);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_39.setTransform(155.3,112.8,0.389,0.389,45);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_40.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_41.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_42.setTransform(156.5,114.5,0.583,0.583,77);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_43.setTransform(140.3,128.8,0.608,0.608,105);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_44.setTransform(140.2,130,0.912,0.912,137);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_45.setTransform(140.2,130,0.912,0.912,137);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_46.setTransform(140.4,132.2,0.912,0.912,137);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_47.setTransform(173.1,71.2,0.95,0.95,31);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_48.setTransform(173.4,64.2,0.95,0.95,31);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_49.setTransform(174.4,56.9,0.95,0.95,31);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_50.setTransform(174.9,48.8,0.95,0.95,31);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_51.setTransform(174.3,42.3,0.95,0.95,31);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_52.setTransform(173.8,35.2,0.95,0.95,31);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_53.setTransform(169.9,29.9,0.95,0.95,31);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_54.setTransform(150.2,59,0.95,0.95);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_55.setTransform(151.9,59.1,1.425,1.425,32);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_56.setTransform(151.8,59.1,1.425,1.425,32);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_57.setTransform(155.1,57.9,1.425,1.425,32);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_58.setTransform(176.4,60.9,0.95,0.95);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_59.setTransform(151.1,58.8,0.95,0.95);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_60.setTransform(161.6,50.3,0.95,0.95,31);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_61.setTransform(146,59.9,0.95,0.95);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_62.setTransform(120.9,125,0.95,0.95);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_63.setTransform(142.7,121.8,0.95,0.95);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_64.setTransform(144.1,123.6,0.95,0.95);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.instance,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.instance},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FF0000").s().p("AgnG2QhAADgmgbQglgbADg2QgCgyAkgbQAjgaA8ACQA/gCAnAZQAlAZgCA1QADA2glAbQggAZgzAAIgNgBgAhxC5IACgQQABgRAEgZQAEgZAJgeQAIgfANgbIAlhKQAVgtAKglQAKgkgCgnQAAgbgDgSQgDgRgFgJQgGgJgJgEQgJgDgLAAQgYgBgcAHQgcAIgZALQgZAJgQAJIgkASIghiNIAvgRQBGgbAygFQAwgFAqAAQA2AAAwAQQAwAQAjAdQAiAcATApQATApAAAyQAABnhjBxIhMBTQgSAUgKAZQgLAZgDAPIgDASg");
	this.shape_65.setTransform(161,94.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Aq7K8QkikiAAmaQAAmZEikiQECkCFfgcQAogDApgBIAJAAQFNAAD8C+QA8AsA3A4QCjCjBIDLQA3CcAACxQAAGakiEiQkhEimbAAQmaAAkhkig");
	this.shape_66.setTransform(109.7,85.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_66},{t:this.shape_65}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy5, new cjs.Rectangle(10.7,-13.1,198,198), null);


(lib.quscopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMK/QkjkjgBmcQABmbEjkjQEhkjGcAAIAAfDQmcAAkhkjg");
	mask.setTransform(60.7,84.7);

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.4,-18.6,0,12.2).s().p("AgHBHQAFgiAAgaQABgbgDgiIgCgdQgCgGACgEQACgEAEgDQAGABADANIABAgQACAigDAbQgDAegHAeQgHAagCAAQgBgBAEgZg");
	this.shape.setTransform(114.4,124.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-18.8,0.2,12).s().p("AADBGQgJgegDgcQgEgcAAgiIABghQABgGADgDQACgDAEAAIAAAAQAHAAgBANIgDAgQgBAiABAaQAAAbAIAgIAFAcQgDgCgIgZg");
	this.shape_1.setTransform(110.1,124.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.6,0.2,12.4).s().p("AgaBQQgNgtgBhAQgBg1AIgoQAQAPARgBQARAAAPgPQAJAoAAA0QABBAgKAuQgNAtgQAAIgBAAQgQAAgMgsgAgbhZQgDAEgBAGIgBAgQAAAjAEAcQADAcAKAeQAIAZADABIgFgcQgIggAAgaQgCgbACghIADggQABgNgIgBIAAABIgBAAQgDAAgCACgAARhZQgCAFACAGIACAdQADAigBAbQAAAZgGAiQgDAaABAAQACAAAHgZQAIgeADgeQADgbgCgiIgCghQgCgNgHAAQgEACgCAEg");
	this.shape_2.setTransform(112,124.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.6,0.2,18.6).s().p("AgWAOQgHgtAOg2QALgrAHgHIAIgKQAFgFAEACQABAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAFAEgKATQgIAFgKAmQgPAyAFAsQADAqAOA0IAMAqQgjg+gHhJg");
	this.shape_3.setTransform(106.7,94.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.5,0.2,18.7).s().p("AgCBqQAOgzABgrQACgrgPgwQgOgogGgEQgLgTAHgFIACAAQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIAGADIAJALQAHAGAMArQAQA2gEAsQgGBIghBAg");
	this.shape_4.setTransform(116.2,94.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.9,0.2,17.5).s().p("AgdCjQgOgOgMgZQgYg2gChMQgBhLAXg2QAMgbANgNQACAJAGAEQAFACAUgBIAVAAQgHAGALASQAHAFAOAnQAQAwgCAtQgCApgOA0IgLAqQAjg/AFhIQAFgtgQg2QgNgqgHgHIgJgLIgHgDQAEgEADgJQANAMALAZQAZA1ABBNQABBLgWA2QgMAagNAOQgPAPgSAAIgCAAQgPAAgPgNgAgcCJIgMgqQgPg0gDgqQgFgsAQgyQAKgmAIgFQAKgTgFgEQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEgCgFAFIgIAKQgIAHgLArQgOA2AHAtQAHBJAkA+g");
	this.shape_5.setTransform(111.6,96);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,1.3).s().p("AAIAIQAAgFgDgEQgEgGgEgDQgGgEgBABIABgBQACgBAHACQAEAEAEAFQADAFAAAGQgBAGgDACQACgCgBgFg");
	this.shape_6.setTransform(114,74.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#D42400","#A30000"],[0,1],1.9,-1.3,0,1.9,-1.3,3.3).s().p("AABAQQgEgEgFgHQgEgHgBgGQABgGAEgDIAAAAQAEgCAFADQAHADAEAHQADAHgBAGQAAAJgEACIgDABQgDAAgDgDgAgCgMQAEACAEAGQAEAEAAAFQAAAGgCACQAEgCAAgGQABgFgDgGQgFgGgFgDQgFgCgDABIAAABIAAAAIAGADg");
	this.shape_7.setTransform(113.8,74.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFCC00","#FF9900"],[0,1],-0.1,0,0,-0.1,0,1.3).s().p("AgJAHQgCgFAEgGQACgFAFgEQAHgDADACIABABQgDgCgGAEQgDAEgCAEQgEAFAAAGQAAAFACADQgEgDAAgGg");
	this.shape_8.setTransform(108.7,74.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#D42400","#A30000"],[0,1],-1.9,-1.3,0,-1.9,-1.3,3.3).s().p("AgHATQgFgCAAgHQgBgIAEgGQACgIAHgDQAHgEAEABIAAABQADADAAAGQAAAGgEAHQgDAHgGAFQgDACgDAAIgCAAgAgFAPQgCgDAAgFQAAgGAEgFQACgEADgEQAGgEADACIgBgBQgDgCgHADQgFAEgCAFQgEAGACAFQAAAGAEADIAAAAg");
	this.shape_9.setTransform(108.7,74.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-3.6,-0.1,3.9).s().p("AAXAkIgWAAQgUAAgFgBQgFgFgDgJQgCgGAAgKQAEACAGgEQAFgDADgHQAEgIAAgHQAAgGgCgDIgBAAIAEgBQABADACACQAEABADgIIABgBIABAAQADAJAEgBQACgCABgDIAEABQgEADgBAHQACAGAEAIQAFAHAEADQAGAEAEgDIgBARQgDAJgDAFQgBAAAAAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgCAAg");
	this.shape_10.setTransform(111.4,76.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,-0.1,-0.6,0.1).s().p("AgEAIIAAgEIgBgEIAAgBIAAgCIAAAAIAAgFIABgHIAEAFQADAHACAGIABAIQgBADgDACIgBAAQgCAAgDgIg");
	this.shape_11.setTransform(111.9,71.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.4,-0.2,0.5,0).s().p("AgBAQQgDgCgBgEQgBgDABgFQADgFADgHIACgFIACAHIABAEIAAAAIAAACIAAACIgBAEIgBAEQgDAIgCAAIAAAAg");
	this.shape_12.setTransform(110.8,71.9);

	this.instance = new lib.bfkyantena_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(111.5,56.6,0.95,0.95,0,0,0,18.9,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_13.setTransform(67,116.2,0.396,0.382,0,-46,134);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_14.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_15.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_16.setTransform(65.9,117.8,0.588,0.579,0,-79,102.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_17.setTransform(81.9,131.6,0.602,0.613,0,-106.9,74.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_18.setTransform(82.1,132.8,0.895,0.928,0,-138,41.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_19.setTransform(82.1,132.7,0.895,0.928,0,-138,41.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_20.setTransform(81.9,135,0.895,0.928,0,-138,41.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_21.setTransform(49.3,75.2,0.965,0.935,0,-31.5,147.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_22.setTransform(49,68.2,0.965,0.935,0,-31.5,147.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_23.setTransform(48,60.9,0.965,0.935,0,-31.5,147.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_24.setTransform(47.5,52.9,0.965,0.935,0,-31.5,147.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_25.setTransform(48,46.4,0.965,0.935,0,-31.5,147.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_26.setTransform(48.5,39.3,0.965,0.935,0,-31.5,147.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_27.setTransform(52.4,33.8,0.965,0.935,0,-31.5,147.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_28.setTransform(72.1,62.3,0.95,0.95,0,0,178);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_29.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_30.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_31.setTransform(67.2,61.2,1.448,1.402,0,-32.6,146.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_32.setTransform(45.9,65.1,0.95,0.95,0,0,178);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_33.setTransform(71.2,62.1,0.95,0.95,0,0,178);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_34.setTransform(60.7,53.9,0.965,0.935,0,-31.5,147.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_35.setTransform(76.3,63,0.95,0.95,0,0,178);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_36.setTransform(101.4,127.2,0.95,0.95,0,0,178);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_37.setTransform(79.5,124.8,0.95,0.95,0,0,178);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_38.setTransform(78.2,126.6,0.95,0.95,0,0,178);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_39.setTransform(155.3,112.8,0.389,0.389,45);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_40.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_41.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_42.setTransform(156.5,114.5,0.583,0.583,77);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_43.setTransform(140.3,128.8,0.608,0.608,105);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_44.setTransform(140.2,130,0.912,0.912,137);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_45.setTransform(140.2,130,0.912,0.912,137);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_46.setTransform(140.4,132.2,0.912,0.912,137);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_47.setTransform(173.1,71.2,0.95,0.95,31);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_48.setTransform(173.4,64.2,0.95,0.95,31);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_49.setTransform(174.4,56.9,0.95,0.95,31);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_50.setTransform(174.9,48.8,0.95,0.95,31);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_51.setTransform(174.3,42.3,0.95,0.95,31);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_52.setTransform(173.8,35.2,0.95,0.95,31);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_53.setTransform(169.9,29.9,0.95,0.95,31);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_54.setTransform(150.2,59,0.95,0.95);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_55.setTransform(151.9,59.1,1.425,1.425,32);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_56.setTransform(151.8,59.1,1.425,1.425,32);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_57.setTransform(155.1,57.9,1.425,1.425,32);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_58.setTransform(176.4,60.9,0.95,0.95);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_59.setTransform(151.1,58.8,0.95,0.95);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_60.setTransform(161.6,50.3,0.95,0.95,31);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_61.setTransform(146,59.9,0.95,0.95);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_62.setTransform(120.9,125,0.95,0.95);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_63.setTransform(142.7,121.8,0.95,0.95);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_64.setTransform(144.1,123.6,0.95,0.95);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.instance,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.instance},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("Aq7K8QkikiAAmaQAAmZEikiQECkCFfgcQAogDApgBIAJAAQFNAAD8C+QA8AsA3A4QCjCjBIDLQA3CcAACxQAAGakiEiQkhEimbAAQmaAAkhkig");
	this.shape_65.setTransform(109.7,85.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_65).wait(1));

	// Layer 1
	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("rgba(0,0,0,0.549)").s().p("Ar3L3Qk6k6AAm9QAAm8E6k6QE7k7G8AAQG9AAE6E7QE7E6AAG8QAAG9k7E6Qk6E7m9AAQm8AAk7k7g");
	this.shape_66.setTransform(109.2,86.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_66).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy3, new cjs.Rectangle(1.8,-20.9,214.8,214.8), null);


(lib.qus = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMK/QkjkjgBmcQABmbEjkjQEhkjGcAAIAAfDQmcAAkhkjg");
	mask.setTransform(60.7,84.7);

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.4,-18.6,0,12.2).s().p("AgHBHQAFgiAAgaQABgbgDgiIgCgdQgCgGACgEQACgEAEgDQAGABADANIABAgQACAigDAbQgDAegHAeQgHAagCAAQgBgBAEgZg");
	this.shape.setTransform(114.4,124.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-18.8,0.2,12).s().p("AADBGQgJgegDgcQgEgcAAgiIABghQABgGADgDQACgDAEAAIAAAAQAHAAgBANIgDAgQgBAiABAaQAAAbAIAgIAFAcQgDgCgIgZg");
	this.shape_1.setTransform(110.1,124.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.6,0.2,12.4).s().p("AgaBQQgNgtgBhAQgBg1AIgoQAQAPARgBQARAAAPgPQAJAoAAA0QABBAgKAuQgNAtgQAAIgBAAQgQAAgMgsgAgbhZQgDAEgBAGIgBAgQAAAjAEAcQADAcAKAeQAIAZADABIgFgcQgIggAAgaQgCgbACghIADggQABgNgIgBIAAABIgBAAQgDAAgCACgAARhZQgCAFACAGIACAdQADAigBAbQAAAZgGAiQgDAaABAAQACAAAHgZQAIgeADgeQADgbgCgiIgCghQgCgNgHAAQgEACgCAEg");
	this.shape_2.setTransform(112,124.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.6,0.2,18.6).s().p("AgWAOQgHgtAOg2QALgrAHgHIAIgKQAFgFAEACQABAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAFAEgKATQgIAFgKAmQgPAyAFAsQADAqAOA0IAMAqQgjg+gHhJg");
	this.shape_3.setTransform(106.7,94.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.9,0.2,17.5).s().p("AgdCjQgOgOgMgZQgYg2gChMQgBhLAXg2QAMgbANgNQACAJAGAEQAFACAUgBIAVAAQgHAGALASQAHAFAOAnQAQAwgCAtQgCApgOA0IgLAqQAjg/AFhIQAFgtgQg2QgNgqgHgHIgJgLIgHgDQAEgEADgJQANAMALAZQAZA1ABBNQABBLgWA2QgMAagNAOQgPAPgSAAIgCAAQgPAAgPgNgAgcCJIgMgqQgPg0gDgqQgFgsAQgyQAKgmAIgFQAKgTgFgEQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEgCgFAFIgIAKQgIAHgLArQgOA2AHAtQAHBJAkA+g");
	this.shape_4.setTransform(111.6,96);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.5,0.2,18.7).s().p("AgCBqQAOgzABgrQACgrgPgwQgOgogGgEQgLgTAHgFIACAAQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIAGADIAJALQAHAGAMArQAQA2gEAsQgGBIghBAg");
	this.shape_5.setTransform(116.2,94.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,1.3).s().p("AAIAIQAAgFgDgEQgEgGgEgDQgGgEgBABIABgBQACgBAHACQAEAEAEAFQADAFAAAGQgBAGgDACQACgCgBgFg");
	this.shape_6.setTransform(114,74.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#D42400","#A30000"],[0,1],1.9,-1.3,0,1.9,-1.3,3.3).s().p("AABAQQgEgEgFgHQgEgHgBgGQABgGAEgDIAAAAQAEgCAFADQAHADAEAHQADAHgBAGQAAAJgEACIgDABQgDAAgDgDgAgCgMQAEACAEAGQAEAEAAAFQAAAGgCACQAEgCAAgGQABgFgDgGQgFgGgFgDQgFgCgDABIAAABIAAAAIAGADg");
	this.shape_7.setTransform(113.8,74.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFCC00","#FF9900"],[0,1],-0.1,0,0,-0.1,0,1.3).s().p("AgJAHQgCgFAEgGQACgFAFgEQAHgDADACIABABQgDgCgGAEQgDAEgCAEQgEAFAAAGQAAAFACADQgEgDAAgGg");
	this.shape_8.setTransform(108.7,74.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#D42400","#A30000"],[0,1],-1.9,-1.3,0,-1.9,-1.3,3.3).s().p("AgHATQgFgCAAgHQgBgIAEgGQACgIAHgDQAHgEAEABIAAABQADADAAAGQAAAGgEAHQgDAHgGAFQgDACgDAAIgCAAgAgFAPQgCgDAAgFQAAgGAEgFQACgEADgEQAGgEADACIgBgBQgDgCgHADQgFAEgCAFQgEAGACAFQAAAGAEADIAAAAg");
	this.shape_9.setTransform(108.7,74.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-3.6,-0.1,3.9).s().p("AAXAkIgWAAQgUAAgFgBQgFgFgDgJQgCgGAAgKQAEACAGgEQAFgDADgHQAEgIAAgHQAAgGgCgDIgBAAIAEgBQABADACACQAEABADgIIABgBIABAAQADAJAEgBQACgCABgDIAEABQgEADgBAHQACAGAEAIQAFAHAEADQAGAEAEgDIgBARQgDAJgDAFQgBAAAAAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgCAAg");
	this.shape_10.setTransform(111.4,76.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,-0.1,-0.6,0.1).s().p("AgEAIIAAgEIgBgEIAAgBIAAgCIAAAAIAAgFIABgHIAEAFQADAHACAGIABAIQgBADgDACIgBAAQgCAAgDgIg");
	this.shape_11.setTransform(111.9,71.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.4,-0.2,0.5,0).s().p("AgBAQQgDgCgBgEQgBgDABgFQADgFADgHIACgFIACAHIABAEIAAAAIAAACIAAACIgBAEIgBAEQgDAIgCAAIAAAAg");
	this.shape_12.setTransform(110.8,71.9);

	this.instance = new lib.bfkyantena_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(111.5,56.6,0.95,0.95,0,0,0,18.9,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_13.setTransform(67,116.2,0.396,0.382,0,-46,134);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_14.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_15.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_16.setTransform(65.9,117.8,0.588,0.579,0,-79,102.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_17.setTransform(81.9,131.6,0.602,0.613,0,-106.9,74.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_18.setTransform(82.1,132.8,0.895,0.928,0,-138,41.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_19.setTransform(82.1,132.7,0.895,0.928,0,-138,41.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_20.setTransform(81.9,135,0.895,0.928,0,-138,41.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_21.setTransform(49.3,75.2,0.965,0.935,0,-31.5,147.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_22.setTransform(49,68.2,0.965,0.935,0,-31.5,147.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_23.setTransform(48,60.9,0.965,0.935,0,-31.5,147.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_24.setTransform(47.5,52.9,0.965,0.935,0,-31.5,147.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_25.setTransform(48,46.4,0.965,0.935,0,-31.5,147.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_26.setTransform(48.5,39.3,0.965,0.935,0,-31.5,147.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_27.setTransform(52.4,33.8,0.965,0.935,0,-31.5,147.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_28.setTransform(72.1,62.3,0.95,0.95,0,0,178);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_29.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_30.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_31.setTransform(67.2,61.2,1.448,1.402,0,-32.6,146.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_32.setTransform(45.9,65.1,0.95,0.95,0,0,178);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_33.setTransform(71.2,62.1,0.95,0.95,0,0,178);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_34.setTransform(60.7,53.9,0.965,0.935,0,-31.5,147.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_35.setTransform(76.3,63,0.95,0.95,0,0,178);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_36.setTransform(101.4,127.2,0.95,0.95,0,0,178);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_37.setTransform(79.5,124.8,0.95,0.95,0,0,178);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_38.setTransform(78.2,126.6,0.95,0.95,0,0,178);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_39.setTransform(155.3,112.8,0.389,0.389,45);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_40.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_41.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_42.setTransform(156.5,114.5,0.583,0.583,77);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_43.setTransform(140.3,128.8,0.608,0.608,105);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_44.setTransform(140.2,130,0.912,0.912,137);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_45.setTransform(140.2,130,0.912,0.912,137);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_46.setTransform(140.4,132.2,0.912,0.912,137);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_47.setTransform(173.1,71.2,0.95,0.95,31);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_48.setTransform(173.4,64.2,0.95,0.95,31);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_49.setTransform(174.4,56.9,0.95,0.95,31);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_50.setTransform(174.9,48.8,0.95,0.95,31);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_51.setTransform(174.3,42.3,0.95,0.95,31);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_52.setTransform(173.8,35.2,0.95,0.95,31);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_53.setTransform(169.9,29.9,0.95,0.95,31);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_54.setTransform(150.2,59,0.95,0.95);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_55.setTransform(151.9,59.1,1.425,1.425,32);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_56.setTransform(151.8,59.1,1.425,1.425,32);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_57.setTransform(155.1,57.9,1.425,1.425,32);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_58.setTransform(176.4,60.9,0.95,0.95);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_59.setTransform(151.1,58.8,0.95,0.95);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_60.setTransform(161.6,50.3,0.95,0.95,31);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_61.setTransform(146,59.9,0.95,0.95);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_62.setTransform(120.9,125,0.95,0.95);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_63.setTransform(142.7,121.8,0.95,0.95);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_64.setTransform(144.1,123.6,0.95,0.95);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.instance,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.instance},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FF0000").s().p("AgnG2QhAADgmgbQglgbADg2QgCgyAkgbQAjgaA8ACQA/gCAnAZQAlAZgCA1QADA2glAbQggAZgzAAIgNgBgAhxC5IACgQQABgRAEgZQAEgZAJgeQAIgfANgbIAlhKQAVgtAKglQAKgkgCgnQAAgbgDgSQgDgRgFgJQgGgJgJgEQgJgDgLAAQgYgBgcAHQgcAIgZALQgZAJgQAJIgkASIghiNIAvgRQBGgbAygFQAwgFAqAAQA2AAAwAQQAwAQAjAdQAiAcATApQATApAAAyQAABnhjBxIhMBTQgSAUgKAZQgLAZgDAPIgDASg");
	this.shape_65.setTransform(161,94.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Aq7K8QkikiAAmaQAAmZEikiQECkCFfgcQAogDApgBIAJAAQFNAAD8C+QA8AsA3A4QCjCjBIDLQA3CcAACxQAAGakiEiQkhEimbAAQmaAAkhkig");
	this.shape_66.setTransform(109.7,85.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_66},{t:this.shape_65}]}).wait(1));

	// Layer 1
	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("rgba(0,0,0,0.549)").s().p("Ar3L3Qk6k6AAm9QAAm8E6k6QE7k7G8AAQG9AAE6E7QE7E6AAG8QAAG9k7E6Qk6E7m9AAQm8AAk7k7g");
	this.shape_67.setTransform(109.2,86.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_67).wait(1));

}).prototype = getMCSymbolPrototype(lib.qus, new cjs.Rectangle(1.8,-20.9,214.8,214.8), null);


(lib.qus_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AjMK/QkjkjgBmcQABmbEjkjQEhkjGcAAIAAfDQmcAAkhkjg");
	mask_1.setTransform(60.7,84.7);

	// Layer_5
	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.4,-18.6,0,12.2).s().p("AgHBHQAFgiAAgaQABgbgDgiIgCgdQgCgGACgEQACgEAEgDQAGABADANIABAgQACAigDAbQgDAegHAeQgHAagCAAQgBgBAEgZg");
	this.shape_68.setTransform(114.4,124.6);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-18.8,0.2,12).s().p("AADBGQgJgegDgcQgEgcAAgiIABghQABgGADgDQACgDAEAAIAAAAQAHAAgBANIgDAgQgBAiABAaQAAAbAIAgIAFAcQgDgCgIgZg");
	this.shape_69.setTransform(110.1,124.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.6,0.2,12.4).s().p("AgaBQQgNgtgBhAQgBg1AIgoQAQAPARgBQARAAAPgPQAJAoAAA0QABBAgKAuQgNAtgQAAIgBAAQgQAAgMgsgAgbhZQgDAEgBAGIgBAgQAAAjAEAcQADAcAKAeQAIAZADABIgFgcQgIggAAgaQgCgbACghIADggQABgNgIgBIAAABIgBAAQgDAAgCACgAARhZQgCAFACAGIACAdQADAigBAbQAAAZgGAiQgDAaABAAQACAAAHgZQAIgeADgeQADgbgCgiIgCghQgCgNgHAAQgEACgCAEg");
	this.shape_70.setTransform(112,124.5);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.6,0.2,18.6).s().p("AgWAOQgHgtAOg2QALgrAHgHIAIgKQAFgFAEACQABAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAFAEgKATQgIAFgKAmQgPAyAFAsQADAqAOA0IAMAqQgjg+gHhJg");
	this.shape_71.setTransform(106.7,94.8);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-17.5,0.2,18.7).s().p("AgCBqQAOgzABgrQACgrgPgwQgOgogGgEQgLgTAHgFIACAAQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIAGADIAJALQAHAGAMArQAQA2gEAsQgGBIghBAg");
	this.shape_72.setTransform(116.2,94.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,-18.9,0.2,17.5).s().p("AgdCjQgOgOgMgZQgYg2gChMQgBhLAXg2QAMgbANgNQACAJAGAEQAFACAUgBIAVAAQgHAGALASQAHAFAOAnQAQAwgCAtQgCApgOA0IgLAqQAjg/AFhIQAFgtgQg2QgNgqgHgHIgJgLIgHgDQAEgEADgJQANAMALAZQAZA1ABBNQABBLgWA2QgMAagNAOQgPAPgSAAIgCAAQgPAAgPgNgAgcCJIgMgqQgPg0gDgqQgFgsAQgyQAKgmAIgFQAKgTgFgEQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEgCgFAFIgIAKQgIAHgLArQgOA2AHAtQAHBJAkA+g");
	this.shape_73.setTransform(111.6,96);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,1.3).s().p("AAIAIQAAgFgDgEQgEgGgEgDQgGgEgBABIABgBQACgBAHACQAEAEAEAFQADAFAAAGQgBAGgDACQACgCgBgFg");
	this.shape_74.setTransform(114,74.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.rf(["#D42400","#A30000"],[0,1],1.9,-1.3,0,1.9,-1.3,3.3).s().p("AABAQQgEgEgFgHQgEgHgBgGQABgGAEgDIAAAAQAEgCAFADQAHADAEAHQADAHgBAGQAAAJgEACIgDABQgDAAgDgDgAgCgMQAEACAEAGQAEAEAAAFQAAAGgCACQAEgCAAgGQABgFgDgGQgFgGgFgDQgFgCgDABIAAABIAAAAIAGADg");
	this.shape_75.setTransform(113.8,74.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.rf(["#FFCC00","#FF9900"],[0,1],-0.1,0,0,-0.1,0,1.3).s().p("AgJAHQgCgFAEgGQACgFAFgEQAHgDADACIABABQgDgCgGAEQgDAEgCAEQgEAFAAAGQAAAFACADQgEgDAAgGg");
	this.shape_76.setTransform(108.7,74.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.rf(["#D42400","#A30000"],[0,1],-1.9,-1.3,0,-1.9,-1.3,3.3).s().p("AgHATQgFgCAAgHQgBgIAEgGQACgIAHgDQAHgEAEABIAAABQADADAAAGQAAAGgEAHQgDAHgGAFQgDACgDAAIgCAAgAgFAPQgCgDAAgFQAAgGAEgFQACgEADgEQAGgEADACIgBgBQgDgCgHADQgFAEgCAFQgEAGACAFQAAAGAEADIAAAAg");
	this.shape_77.setTransform(108.7,74.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-3.6,-0.1,3.9).s().p("AAXAkIgWAAQgUAAgFgBQgFgFgDgJQgCgGAAgKQAEACAGgEQAFgDADgHQAEgIAAgHQAAgGgCgDIgBAAIAEgBQABADACACQAEABADgIIABgBIABAAQADAJAEgBQACgCABgDIAEABQgEADgBAHQACAGAEAIQAFAHAEADQAGAEAEgDIgBARQgDAJgDAFQgBAAAAAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgCAAg");
	this.shape_78.setTransform(111.4,76.3);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,-0.1,-0.6,0.1).s().p("AgEAIIAAgEIgBgEIAAgBIAAgCIAAAAIAAgFIABgHIAEAFQADAHACAGIABAIQgBADgDACIgBAAQgCAAgDgIg");
	this.shape_79.setTransform(111.9,71.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.4,-0.2,0.5,0).s().p("AgBAQQgDgCgBgEQgBgDABgFQADgFADgHIACgFIACAHIABAEIAAAAIAAACIAAACIgBAEIgBAEQgDAIgCAAIAAAAg");
	this.shape_80.setTransform(110.8,71.9);

	this.instance_1 = new lib.bfkyantenacopy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(111.5,56.6,0.95,0.95,0,0,0,18.9,0);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_81.setTransform(67,116.2,0.396,0.382,0,-46,134);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_82.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_83.setTransform(67.2,117.2,0.588,0.579,0,-79,102.9);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_84.setTransform(65.9,117.8,0.588,0.579,0,-79,102.9);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_85.setTransform(81.9,131.6,0.602,0.613,0,-106.9,74.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_86.setTransform(82.1,132.8,0.895,0.928,0,-138,41.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_87.setTransform(82.1,132.7,0.895,0.928,0,-138,41.9);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_88.setTransform(81.9,135,0.895,0.928,0,-138,41.9);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_89.setTransform(49.3,75.2,0.965,0.935,0,-31.5,147.5);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_90.setTransform(49,68.2,0.965,0.935,0,-31.5,147.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_91.setTransform(48,60.9,0.965,0.935,0,-31.5,147.5);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_92.setTransform(47.5,52.9,0.965,0.935,0,-31.5,147.5);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_93.setTransform(48,46.4,0.965,0.935,0,-31.5,147.5);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_94.setTransform(48.5,39.3,0.965,0.935,0,-31.5,147.5);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_95.setTransform(52.4,33.8,0.965,0.935,0,-31.5,147.5);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_96.setTransform(72.1,62.3,0.95,0.95,0,0,178);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_97.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_98.setTransform(70.5,62.3,1.448,1.402,0,-32.6,146.5);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_99.setTransform(67.2,61.2,1.448,1.402,0,-32.6,146.5);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_100.setTransform(45.9,65.1,0.95,0.95,0,0,178);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_101.setTransform(71.2,62.1,0.95,0.95,0,0,178);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_102.setTransform(60.7,53.9,0.965,0.935,0,-31.5,147.5);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_103.setTransform(76.3,63,0.95,0.95,0,0,178);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_104.setTransform(101.4,127.2,0.95,0.95,0,0,178);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_105.setTransform(79.5,124.8,0.95,0.95,0,0,178);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_106.setTransform(78.2,126.6,0.95,0.95,0,0,178);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_107.setTransform(155.3,112.8,0.389,0.389,45);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_108.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_109.setTransform(155.2,113.9,0.583,0.583,77);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_110.setTransform(156.5,114.5,0.583,0.583,77);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_111.setTransform(140.3,128.8,0.608,0.608,105);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_112.setTransform(140.2,130,0.912,0.912,137);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_113.setTransform(140.2,130,0.912,0.912,137);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_114.setTransform(140.4,132.2,0.912,0.912,137);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_115.setTransform(173.1,71.2,0.95,0.95,31);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_116.setTransform(173.4,64.2,0.95,0.95,31);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_117.setTransform(174.4,56.9,0.95,0.95,31);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_118.setTransform(174.9,48.8,0.95,0.95,31);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_119.setTransform(174.3,42.3,0.95,0.95,31);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_120.setTransform(173.8,35.2,0.95,0.95,31);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_121.setTransform(169.9,29.9,0.95,0.95,31);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_122.setTransform(150.2,59,0.95,0.95);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_123.setTransform(151.9,59.1,1.425,1.425,32);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_124.setTransform(151.8,59.1,1.425,1.425,32);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_125.setTransform(155.1,57.9,1.425,1.425,32);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_126.setTransform(176.4,60.9,0.95,0.95);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_127.setTransform(151.1,58.8,0.95,0.95);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_128.setTransform(161.6,50.3,0.95,0.95,31);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_129.setTransform(146,59.9,0.95,0.95);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_130.setTransform(120.9,125,0.95,0.95);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_131.setTransform(142.7,121.8,0.95,0.95);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_132.setTransform(144.1,123.6,0.95,0.95);

	var maskedShapeInstanceList = [this.shape_68,this.shape_69,this.shape_70,this.shape_71,this.shape_72,this.shape_73,this.shape_74,this.shape_75,this.shape_76,this.shape_77,this.shape_78,this.shape_79,this.shape_80,this.instance_1,this.shape_81,this.shape_82,this.shape_83,this.shape_84,this.shape_85,this.shape_86,this.shape_87,this.shape_88,this.shape_89,this.shape_90,this.shape_91,this.shape_92,this.shape_93,this.shape_94,this.shape_95,this.shape_96,this.shape_97,this.shape_98,this.shape_99,this.shape_100,this.shape_101,this.shape_102,this.shape_103,this.shape_104,this.shape_105,this.shape_106,this.shape_107,this.shape_108,this.shape_109,this.shape_110,this.shape_111,this.shape_112,this.shape_113,this.shape_114,this.shape_115,this.shape_116,this.shape_117,this.shape_118,this.shape_119,this.shape_120,this.shape_121,this.shape_122,this.shape_123,this.shape_124,this.shape_125,this.shape_126,this.shape_127,this.shape_128,this.shape_129,this.shape_130,this.shape_131,this.shape_132];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.instance_1},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68}]}).wait(1));

	// Layer 2
	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("Aq7K8QkikiAAmaQAAmZEikiQECkCFfgcQAogDApgBIAJAAQFNAAD8C+QA8AsA3A4QCjCjBIDLQA3CcAACxQAAGakiEiQkhEimbAAQmaAAkhkig");
	this.shape_133.setTransform(109.7,85.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_133).wait(1));

	// Layer 1
	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("rgba(0,0,0,0.549)").s().p("Ar3L3Qk6k6AAm9QAAm8E6k6QE7k7G8AAQG9AAE6E7QE7E6AAG8QAAG9k7E6Qk6E7m9AAQm8AAk7k7g");
	this.shape_134.setTransform(109.2,86.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_134).wait(1));

}).prototype = getMCSymbolPrototype(lib.qus_1, new cjs.Rectangle(1.8,-20.9,214.8,214.8), null);


(lib.inside_fruitcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Albq3QEgAADKDMQDNDMAAEfQAAEgjNDMQjKDLkgABg");
	mask.setTransform(33.7,0.2);

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00259C").s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape.setTransform(-30.3,25.8,0.335,0.33,0,-79,102.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00259C").s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_1.setTransform(-19.1,33.1,0.51,0.529,0,-138,41.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00259C").s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_2.setTransform(-25.2,-5.5,0.826,0.8,0,-32.6,146.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00259C").s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_3.setTransform(26,23.3,0.333,0.333,77);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00259C").s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_4.setTransform(15.9,30.7,0.52,0.52,137);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00259C").s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_5.setTransform(21.5,-6.3,0.813,0.813,32);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6633CC").s().p("AC/ErQgcgNgJgfQgJghhIilQBtBrAigGQAggFAXAUQAXASADAdQAEAcgPAXQgPAYgaAIQgMAEgLAAQgQAAgPgIgAjnEvQgagIgPgYQgOgXADgcQADgdAXgSQAYgUAfAFQAiAGBthrQhHClgKAhQgJAfgcANQgPAIgQAAQgLAAgMgEgAkCiQQggAFgXgUQgXgSgDgdQgEgbAPgYQAPgXAagJQAcgJAaANQAcANAJAfQAJAhBIClQhthrgiAGgADFj+QAKgfAbgNQAagNAcAKQAaAIAPAXQAPAYgDAbQgEAdgWASQgYAUgggFQgigFhtBqQBIilAJghg");
	this.shape_6.setTransform(-1.9,8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-13.3,0.1,8.6).s().p("AACAyQgGgVgCgUQgDgUAAgZIABgXIACgHQABAAAAgBQABAAAAAAQABAAAAAAQABAAAAAAIABgBQAEABAAAJIgCAXQgBAYAAASQABATAFAXIAEAUQgCgBgGgSg");
	this.shape_7.setTransform(-2.4,28.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.1,-12.5,0.2,13.2).s().p("AgQAKQgEggAKgmQAIgfAEgFIAGgHQAEgEADACIACAAQAEADgHAOQgGADgHAcQgKAjADAfQACAeAJAlIAJAeQgYgsgGg0g");
	this.shape_8.setTransform(-4.9,7.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#D42400","#A30000"],[0,1],-1.3,-0.9,0,-1.3,-0.9,2.3).s().p("AgFANQgDgBAAgFQgBgFACgFQACgFAFgDQAFgCACABIABAAQACACAAAEQAAAFgDAFQgCAFgEADQgCACgDAAIgBgBgAgEAKQgBgBAAgEQAAgEADgDQABgEADgDQAEgCABABIAAgBQgCgBgFACQgDADgBADQgDAEAAAEQABAFACABIAAAAg");
	this.shape_9.setTransform(-3.4,-6.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,0.9).s().p("AgGAFQgBgEADgEQABgDAEgDQAEgCADABIAAABQgCgBgEACQgCADgCAEQgDADAAAEQAAAEACABQgCgBgBgFg");
	this.shape_10.setTransform(-3.5,-6.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.2,0,0.4,0.1).s().p("AAAALQgBAAAAAAQgBgBAAAAQAAgBgBAAQAAgBAAgBIAAgFIAEgJIACgDIABAFIAAACIAAABIAAABIAAABIAAADIgBADQgCAFgBAAIAAAAg");
	this.shape_11.setTransform(-2,-9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,0,-0.3,0.1).s().p("AgCAGIAAgDIgBgDIAAgBIAAgBIAAgBIAAgCIABgGIACAEIAEAJIAAAFQAAABAAABQAAAAgBABQAAAAAAABQgBAAAAAAIgBABQgBAAgCgGg");
	this.shape_12.setTransform(-1.2,-9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.1,-13.2,0.2,8.8).s().p("AgSA5QgKggAAgtQgBgmAGgdQALALAMgBQAMAAALgLQAGAdAAAlQABAtgIAhQgIAhgMAAIgBAAQgLAAgIgggAgTg/IgDAHIAAAXQgBAZAEATQABAUAHAWQAGASACAAIgEgUQgFgWAAgTQgCgTACgYIACgWQABgKgFAAIgBAAIgBAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAAAAAABgAAMg/QgBAEABADIACAVQACAYgBATQAAASgEAZIgCASQACAAAFgSQAFgVACgVQADgUgCgYIgBgXQgCgJgFAAIgEAEg");
	this.shape_13.setTransform(-1.1,28.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.3,-13.2,0,8.7).s().p("AgFAyQAEgYABgSQAAgTgCgYIgCgVQgBgEACgEIADgEQAFAAACAJIABAYQABAYgCATQgCAWgFAVQgFASgCAAIACgTg");
	this.shape_14.setTransform(0.6,28.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-2.6,-0.1,2.8).s().p("AAQAZIgPAAQgOABgDgCQgEgCgCgHQgCgEAAgHQADABAEgDQAEgCADgFQACgGAAgEQAAgEgBgDIgBAAIADgBQAAABAAAAQAAABABAAQAAABAAAAQABAAAAABQADABABgGIABgBIABAAQACAGADAAQAAgBABAAQAAgBAAAAQABAAAAgBQAAgBAAAAIADABQgDADAAAEIAEAKQADAFADACQAFADACgCIAAAMQgCAHgDACIgCACIgCgBg");
	this.shape_15.setTransform(-1.6,-5.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.1,-13.5,0.2,12.4).s().p("AgUB0QgKgKgJgSQgRgmgBg2QgBg2ARgmQAIgTAJgKQACAHAEADQADABAPAAIAOAAQgEAEAHAMQAFAEAKAcQALAigBAgQgBAdgKAlIgIAeQAYgtAEgzQAEgggMgmQgIgegGgFIgGgIIgFgCQADgDACgHQAJAJAIASQASAmAAA2QACA2gQAmQgJATgJAKQgLALgNAAIgBAAQgLAAgKgKgAgUBiIgJgfQgKglgCgeQgDgfALgjQAHgbAGgEQAHgNgEgDIgCgBQgDgBgEAEIgGAHQgFAFgIAeQgKAmAEAgQAGA1AZAsg");
	this.shape_16.setTransform(-1.4,8.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.1,-12.5,0.2,13.3).s().p("AgBBMQAJglABgeQACgfgLgiQgJgcgFgEQgIgMAFgEIACAAIACgBIAEACIAHAIQAEAFAJAeQALAmgDAfQgEA0gYAtg");
	this.shape_17.setTransform(1.9,7.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,0.9).s().p("AAGAFQAAgDgCgDQgDgEgDgCQgDgDgCABIABgBQACgBAEACQADACADAEQACAEAAAEQgBAEgCACQABgCAAgEg");
	this.shape_18.setTransform(0.3,-7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#D42400","#A30000"],[0,1],1.3,-0.9,0,1.3,-0.9,2.3).s().p("AAAAMQgCgDgDgGIgEgIQAAgFADgCIABAAQADgBACACQAFACADAFQACAEAAAFQAAAGgDACIgCAAIgFgBgAgBgJQACACADAFQADACAAAEQAAAEgCABQADgBAAgFQABgDgCgEQgDgEgEgDQgEgBgBABIgBAAIAAAAIAFACg");
	this.shape_19.setTransform(0.2,-6.8);

	this.instance = new lib.bfkyantena("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-1.6,-20,0.677,0.677,0,0,0,18.7,-0.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_20.setTransform(-48.5,-13.8,0.677,0.677,0,0,178);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF3399").s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_21.setTransform(-30.4,-16,0.677,0.677,0,0,178);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.lf(["#FF3399","#0033CC"],[0,1],39.9,-2.3,-22.2,5.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_22.setTransform(-26.8,-15.3,0.677,0.677,0,0,178);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.lf(["#FF3399","#0033CC"],[0,1],-30.2,0,30.3,0).s().p("AhGFSQgMgEgMgJQgUgNgLgmQgOgzgFgLQgFgJgWgPQgVgQgNgZQhciygEhzIAAgGQgDhPAIg8IAIgsIBQgEQBigCBUAQQEQAyAzDGQAMAwgNApQgHAZgSAxQgFAUAGAkQAGAfgFAIQgFAJgQAFQgTAFgIAHQgHAHgGAOQgFAPgHAGQgGAFgRACQgRADgJAGQgJAHgOASQgMAQgKAFQgKAFgXgFQgXgEgJAFQgIADgKAKQgJAKgGACQgHACgPgHQgPgFgHAEQgJAHgFABIgHABQgGAAgHgCg");
	this.shape_23.setTransform(-23.5,27.5,0.677,0.677,0,0,178);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_24.setTransform(-24.5,28.7,0.677,0.677,0,0,178);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF3399").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_25.setTransform(-25.4,30,0.677,0.677,0,0,178);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_26.setTransform(44.8,-16.7,0.677,0.677);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF3399").s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_27.setTransform(26.7,-18.3,0.677,0.677);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.lf(["#FF3399","#0033CC"],[0,1],39.9,-2.3,-22.2,5.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_28.setTransform(23.1,-17.5,0.677,0.677);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FF3399","#0033CC"],[0,1],-21.3,0,21.3,0).s().p("AgxDuQgIgDgJgGQgOgJgHgbQgKgkgEgIQgEgGgPgLQgOgKgKgSQhBh9gDhRIAAgEQgBg4AFgqIAGgfIA4gDQBFgBA7ALQC/AjAkCMQAJAhgJAdIgSA0QgDAOAEAZQAEAWgEAGQgDAGgMADQgNAEgGAFQgEAFgEAKQgEAKgFAEQgEAEgMACQgMABgHAFQgGAFgJANQgJAKgHAEQgHAEgRgEQgPgCgHADQgGACgGAHQgHAHgEACQgFABgKgFQgLgEgFADQgGAFgEABIgEABIgKgCg");
	this.shape_29.setTransform(19.9,25.3,0.962,0.962);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_30.setTransform(20.8,26.6,0.677,0.677);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF3399").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_31.setTransform(21.7,27.9,0.677,0.677);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.instance,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.instance},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#ECE9E9").s().p("AFwLnIgIAAQkvgDjWjXQjZjZAAk0QAAkzDZjaQDWjWEvgDIAIAAIAHAAIAAXNIgHAAg");
	this.shape_32.setTransform(-37.6,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF6666").s().p("AAAM3IgHAAQlQgDjujvQjwjwAAlVQAAlUDwjxQDujuFQgDIAHAAIAHAAQFQADDvDuQDxDxgBFUQABFVjxDwQjvDvlQADIgHAAgAooooQjlDlAAFDQAAFEDlDlQDiDiE/ACIAHAAIAHAAQE/gCDijiQDljlABlEQgBlDjljlQjijik/gCIgHAAIgHAAQk/ACjiDig");
	this.shape_33.setTransform(-0.8,0);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFCC00").s().p("AAAMNIgHAAQk/gCjijiQjljlAAlEQAAlDDljlQDijiE/gCIAHAAIAHAAQE/ACDiDiQDlDlABFDQgBFEjlDlQjiDik/ACIgHAAgAoMoNQjZDaAAEzQAAE0DZDZQDWDXEvADIAHAAIAHAAQEvgDDXjXQDajZAAk0QAAkzjajaQjXjWkvgDIgHAAIgHAAQkvADjWDWg");
	this.shape_34.setTransform(-0.8,0);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AlvrmQEvADDWDWQDaDaAAEzQAAE0jaDZQjWDXkvADg");
	this.shape_35.setTransform(36.6,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32}]}).wait(1));

	// Layer 4
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("rgba(0,0,0,0.027)").s().p("Ap/KAQkJkJAAl3QAAl2EJkJQEJkJF2AAQF3AAEJEJQEJEJAAF2QAAF3kJEJQkJEIl3AAQl2AAkJkIg");
	this.shape_36.setTransform(-0.8,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_36).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy2, new cjs.Rectangle(-91.3,-90.4,181,180.9), null);


(lib.inside_fruitcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Albq3QEgAADKDMQDNDMAAEfQAAEgjNDMQjKDLkgABg");
	mask.setTransform(33.7,0.2);

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.2,-13.3,0.1,8.6).s().p("AACAyQgGgVgCgUQgDgUAAgZIABgXQAAgEADgDQAAAAAAgBQABAAAAAAQABAAAAAAQABAAAAAAIABgBQAFABgBAJIgCAXQgBAYAAASQABATAFAXIAEAUQgDgBgFgSg");
	this.shape.setTransform(-2.4,28.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.1,-12.5,0.2,13.2).s().p("AgQAKQgFggAKgmQAJgfAEgFIAGgHQAEgEACACIADAAQAEADgHAOQgGADgIAcQgKAjAEAfQACAeAJAlIAJAeQgZgsgFg0g");
	this.shape_1.setTransform(-4.9,7.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#D42400","#A30000"],[0,1],-1.3,-0.9,0,-1.3,-0.9,2.3).s().p("AgFANQgEgBAAgFQAAgFACgFQACgFAFgDQAFgCACABIABAAQACACAAAEQAAAFgDAFQgCAFgEADQgCACgCAAIgCgBgAgEAKQgBgBAAgEQAAgEADgDQABgEACgDQAEgCACABIAAgBQgCgBgFACQgDADgCADQgDAEABAEQABAFACABIAAAAg");
	this.shape_2.setTransform(-3.4,-6.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,0.9).s().p("AgGAFQgBgEADgEQABgDADgDQAFgCACABIABABQgCgBgEACQgCADgCAEQgDADAAAEQAAAEACABQgDgBAAgFg");
	this.shape_3.setTransform(-3.5,-6.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.3,0,0.4,0.1).s().p("AgBALQAAAAgBAAQAAgBAAAAQAAgBgBAAQAAgBAAgBIAAgFIAEgJIABgDIACAFIAAACIAAABIAAABIAAABIgBADIAAADQgCAFgBAAIgBAAg");
	this.shape_4.setTransform(-2,-9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0.3,0,-0.4,0.1).s().p("AgCAGIgBgDIAAgDIAAgBIAAgBIAAAAIAAgDIAAgGIADAEIADAJIABAFIgCAEIgBABQgBAAgCgGg");
	this.shape_5.setTransform(-1.2,-9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.1,-13.2,0.2,8.8).s().p("AgTA5QgJggAAgtQgBgmAGgdQALALAMgBQAMAAALgLQAGAdAAAlQABAtgIAhQgJAhgLAAIgBAAQgKAAgKgggAgTg/QgCADgBAEIgBAXQAAAZADATQACAUAHAWQAGASACAAIgEgUQgFgWAAgTQgCgTACgYIACgWQABgKgGAAIAAAAIgBAAQgBAAAAAAQAAAAgBABQAAAAgBAAQAAAAAAABgAAMg/QgBAEABADIABAVQADAYgBATQAAASgEAZIgCASQACAAAFgSQAFgVACgVQADgUgCgYIgBgXQgCgJgFAAQgDABgBADg");
	this.shape_6.setTransform(-1.1,28.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.3,-13.2,0,8.7).s().p("AgFAyQAEgYAAgSQABgTgCgYIgCgVQgBgEACgEQABgCACgCQAFAAABAJIACAYQABAYgCATQgCAWgFAVQgFASgCAAIACgTg");
	this.shape_7.setTransform(0.6,28.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#5A759B","#545D95","#464F85","#34396B","#272D51"],[0,0.141,0.38,0.686,1],-0.1,-12.5,0.2,13.3).s().p("AgBBMQAKglABgeQABgfgLgiQgJgcgFgEQgIgMAFgEIACAAIACgBIAFACIAGAIQAEAFAJAeQAMAmgEAfQgEA0gXAtg");
	this.shape_8.setTransform(1.8,7.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],0,-2.6,-0.1,2.8).s().p("AAQAZIgPAAQgOABgEgCQgDgCgCgHQgCgEAAgHQADABAEgDQAEgCACgFQADgGAAgEQAAgEgCgDIAAAAIADgBQAAABAAAAQAAABAAAAQABABAAAAQAAAAABABQACABACgGIABgBIABAAQACAGADAAIACgEIADABQgDADgBAEIAEAKQAEAFADACQAEADADgCIAAAMQgDAHgCACIgCACIgCgBg");
	this.shape_9.setTransform(-1.6,-5.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#2A1010","#000000","#000000"],[0,1,1],-0.1,-13.5,0.2,12.4).s().p("AgUB0QgLgKgIgSQgRgmgBg2QgBg2AQgmQAIgTAKgKQACAHADADQAEABAPAAIAOAAQgFAEAIAMQAFAEAKAcQALAigBAgQgBAdgLAlIgHAeQAYgtAEgzQAEgggMgmQgJgegFgFIgGgIIgFgCQACgDADgHQAJAJAIASQASAmAAA2QABA2gPAmQgJATgJAKQgLALgNAAIgBAAQgLAAgKgKgAgUBiIgJgfQgKglgCgeQgEgfALgjQAIgbAGgEQAHgNgEgDIgDgBQgCgBgEAEIgGAHQgFAFgJAeQgKAmAFAgQAFA1AaAsg");
	this.shape_10.setTransform(-1.4,8.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FFCC00","#FF9900"],[0,1],0,0,0,0,0,0.9).s().p("AAGAFQAAgDgCgDQgDgEgDgCQgEgDgBABIABgBQACgBAEACIAGAGQACAEAAAEQgBAEgCACQABgCAAgEg");
	this.shape_11.setTransform(0.3,-7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#D42400","#A30000"],[0,1],1.3,-0.9,0,1.3,-0.9,2.3).s().p("AABAMQgDgDgDgGIgEgIQABgFACgCIABAAQADgBADACQAFACADAFQACAEgBAFQAAAGgDACIgCAAIgEgBgAgBgJQADACADAFQACACAAAEQAAAEgBABQACgBABgFQAAgDgCgEIgHgHQgDgBgCABIgBAAIABAAIAEACg");
	this.shape_12.setTransform(0.2,-6.8);

	this.instance = new lib.bfkyantena("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-1.6,-20,0.677,0.677,0,0,0,18.7,-0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_13.setTransform(-33.2,22.5,0.282,0.272,0,-46,134);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_14.setTransform(-33.1,23.3,0.419,0.412,0,-79,102.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_15.setTransform(-33.1,23.3,0.419,0.412,0,-79,102.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_16.setTransform(-34,23.7,0.419,0.412,0,-79,102.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_17.setTransform(-22.6,33.6,0.429,0.437,0,-106.9,74.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_18.setTransform(-22.5,34.4,0.638,0.661,0,-138,41.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_19.setTransform(-22.5,34.4,0.638,0.661,0,-138,41.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_20.setTransform(-22.6,36,0.638,0.661,0,-138,41.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_21.setTransform(-46,-6.7,0.688,0.667,0,-31.5,147.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_22.setTransform(-46.2,-11.7,0.688,0.667,0,-31.5,147.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_23.setTransform(-47,-16.9,0.688,0.667,0,-31.5,147.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_24.setTransform(-47.3,-22.6,0.688,0.667,0,-31.5,147.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_25.setTransform(-46.9,-27.2,0.688,0.667,0,-31.5,147.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_26.setTransform(-46.6,-32.3,0.688,0.667,0,-31.5,147.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_27.setTransform(-43.8,-36.2,0.688,0.667,0,-31.5,147.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_28.setTransform(-29.8,-15.8,0.677,0.677,0,0,178);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_29.setTransform(-30.9,-15.9,1.032,1,0,-32.6,146.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_30.setTransform(-30.9,-15.9,1.032,1,0,-32.6,146.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_31.setTransform(-33.3,-16.6,1.032,1,0,-32.6,146.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_32.setTransform(-48.5,-13.8,0.677,0.677,0,0,178);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_33.setTransform(-30.4,-16,0.677,0.677,0,0,178);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_34.setTransform(-37.9,-21.9,0.688,0.667,0,-31.5,147.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_35.setTransform(-26.8,-15.3,0.677,0.677,0,0,178);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_36.setTransform(-8.9,30.4,0.677,0.677,0,0,178);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_37.setTransform(-24.5,28.7,0.677,0.677,0,0,178);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_38.setTransform(-25.4,30,0.677,0.677,0,0,178);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_39.setTransform(29.4,20.1,0.277,0.277,45);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_40.setTransform(29.4,20.9,0.416,0.416,77);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_41.setTransform(29.4,20.9,0.416,0.416,77);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_42.setTransform(30.4,21.3,0.416,0.416,77);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_43.setTransform(18.8,31.6,0.433,0.433,105);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_44.setTransform(18.8,32.4,0.65,0.65,137);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_45.setTransform(18.8,32.4,0.65,0.65,137);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_46.setTransform(18.9,34,0.65,0.65,137);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgMAJQgCgDADgFQACgEAFgEQAFgDAFAAQAFAAACACQACAEgCAEQgDAEgFAEQgFADgFAAIgBAAQgEAAgCgCg");
	this.shape_47.setTransform(42.4,-9.6,0.677,0.677,31);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgPAJQgCgEADgFQADgFAGgEQAGgDAGAAQAGAAACAEQADAEgDAEQgDAGgHADQgFAEgGAAQgGAAgDgEg");
	this.shape_48.setTransform(42.6,-14.6,0.677,0.677,31);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgRAKQgCgEAEgFQAEgGAHgDQAHgFAHAAQAGAAACAEQACAEgEAFQgEAGgIAEQgGADgGABQgHAAgCgEg");
	this.shape_49.setTransform(43.3,-19.7,0.677,0.677,31);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgUALQgCgFAEgGQAFgGAIgEQAIgEAIAAQAHAAADAFQACAEgFAFQgEAHgJAEQgHAEgIAAQgIAAgCgEg");
	this.shape_50.setTransform(43.7,-25.5,0.677,0.677,31);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgSALQgCgFAEgGQAEgFAIgEQAHgFAHAAQAHAAACAEQACAFgEAFQgEAGgIAFQgHAEgHAAQgHAAgCgEg");
	this.shape_51.setTransform(43.3,-30.1,0.677,0.677,31);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgRAQQgDgEADgHQADgHAHgGQAHgHAHgCQAHgBAEADQADAEgDAIQgDAGgHAHQgHAGgHACIgEAAQgEAAgDgCg");
	this.shape_52.setTransform(42.9,-35.2,0.677,0.677,31);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgPAKQgCgEADgFQADgFAGgEQAGgEAGAAQAGAAACADQADAEgDAFQgDAFgGAEQgGAEgGAAIgBAAQgFAAgDgDg");
	this.shape_53.setTransform(40.1,-39,0.677,0.677,31);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#330066").s().p("AhGBHQgegdAAgpQgBgoAdgeQAegdAqAAQApgBAeAdQAdAdABApQAAAogdAeQgdAdgqABQgpAAgegdg");
	this.shape_54.setTransform(26.1,-18.1,0.677,0.677);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.lf(["#FFCC00","#FF9900"],[0,1],-8.2,0,8.3,0).s().p("AgqBTQgdgSgIglQgIglARgiQASgjAhgLQAhgMAdATQAdASAIAlQAIAkgRAjQgSAighAMQgNAEgLAAQgUAAgSgLg");
	this.shape_55.setTransform(27.2,-18.2,1.016,1.016,32);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#FDE77B","#FDF7B6"],[0,1],-8.7,5.6,8.8,-5.6).s().p("AgxBiQgjgWgJgrQgKgrAVgpQAVgoAngOQAmgNAjAVQAiAWAJArQAKArgVApQgVAognAOQgPAFgNAAQgXAAgVgNg");
	this.shape_56.setTransform(27.2,-18.2,1.016,1.016,32);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.lf(["#FF6600","#FF3300"],[0,1],-11.5,0,11.5,0).s().p("Ag9B3QgogZgKg0QgKg0AZgxQAagyAugRQAugRAoAYQAoAZAKA1QAKA0gZAxQgaAyguARQgTAHgRAAQgaAAgYgPg");
	this.shape_57.setTransform(29.6,-19.1,1.016,1.016,32);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC00").s().p("AgiEoIARghQAGgMAFgSQADgQgBgOIgHgiQgDgKAIgYQAHgWgGgSQgGgQAKgVQAKgXgDgPQgEgTAAgKQABgPAHgaQAFgRARgfQANgYgBgPQgBgQgPgZQgRgbgDgTIgCg5QgEgpgPgRQAZASAJApQAFAXAEAmQACASARAcQAQAZABAQQAAAQgNAXQgQAegGATQgHAZgBAQQAAAJAEAUQADAPgLAWQgJAUAGAQQAHAUgJAWQgIAXADAKQAGAZABAJQACAOgDARQgEARgIANIgPAgQgKATgKANQgQAEgRADQARgRAOgbg");
	this.shape_58.setTransform(44.8,-16.7,0.677,0.677);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFCC00","#FF3366"],[0,1],0,0,0,0,0,53.2).s().p("ADoE+IAQggQAHgNAEgSQAEgQgCgOIgHgiQgDgKAIgXQAIgWgGgTQgGgQAKgVQAKgWgDgQQgEgTAAgLQABgNAHgaQAGgSAQgfQANgXgBgQQAAgQgQgZQgRgbgCgTQAAgzgFgcQgKgzgyAAQhnAAh6BoQhzBhhWCQIAAAAQgmBCgkBQQgiBPgXBLQgkAFgiAJQACgeALgqQAWhVAog7IBGhoQA4hPA2g+QCei1CTglQB7gdA8A7QAqAqACBAQABAZAYARQAXASAAAJQABAKgYAOQgaAQgKAXQgNAdABALQABAHAJAOQAIAUgRAUQgQAUALAUQAOAYgLAXQgMAZAGALIATAmQAFAOgEAUQgEAQgIALQgPASgLAXQgkBFhVANQARgQAOgcg");
	this.shape_59.setTransform(26.7,-18.3,0.677,0.677);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.lf(["#FDBB46","#F2683B","#CC5039","#9C3334"],[0.016,0.482,0.702,1],-1,-9.4,11.7,43.9).s().p("AgEATIgRgsQAUgXAYgXQgXBOAMBBQgFgXgLgeg");
	this.shape_60.setTransform(34.2,-24.4,0.677,0.677,31);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,48.3).s().p("ACnFgQgngDg9gLIhqgTQhBgLgtgFQiAgOhWAbQACgfALgqQAWhUAog8IBGhnQA4hQA2g9QB6iOB2g1QCAgjAqA6QAOAUAGAiIAHAyQACASARAcQAQAZAAAQQABAQgNAXQgQAfgGARQgHAagBAOQAAALAEATQADAPgKAXQgKAUAGARQAGASgIAWQgIAYADAKQAGAXABAKQACAOgEARQgEASgHAMIgQAhQgOAbgRARQgVADgYAAQgUAAgXgCg");
	this.shape_61.setTransform(23.1,-17.5,0.677,0.677);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#D31D3A").s().p("ABVDSQgIgQgdgFQgbgGgOgfQhXjHgFiGIAAgdQBRAtAYCKIARBjQAMAzAWAVQALALACAWIACAig");
	this.shape_62.setTransform(5.2,28.9,0.677,0.677);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#FF3333","#330066"],[0,1],0,0,0,0,0,46.6).s().p("AhIFkQgNgGgMgLQgRgQgNgpQgSg0gGgMQgGgMgZgKQgYgLgPgcQhZi8gFh8IAAgFQgChUAIg/IAIguIBUgFQBlgBBZARQEcAzA1DQQAPA2gQAwIgeBWQgEAZAMAqQAJAkgGAHQgJAHgVgBQgYgDgJAFQgKAHgEARQgDAQgHAFQgGAFgUgCQgTgCgLAHQgKAIgMAVQgLASgKAFQgLAFgYgKQgYgJgJAEQgLAEgGAPQgGAOgHACQgGADgQgKQgPgKgGAIQgKALgEACQgEACgEAAQgGAAgHgDg");
	this.shape_63.setTransform(20.8,26.6,0.677,0.677);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#4413F3").s().p("AhLF1QgcgNgVg0QgVg7gNgYQgJgQgcgGQgcgFgNggQhZjJgEiGQgDhYAJhBIAJgxIBXgFQBpgBBeARQEnA2A4DYQAQA8gTA3QgfBXgCAMQgFAeASAxQANAogHAEQgMAHgcgJQgcgKgJAFQgNAGgCASQgCATgHAFQgHAEgWgHQgWgGgKAHQgNAJgLAYQgJAVgJAFQgNAFgZgPQgZgPgKAEQgKAHgFASQgEARgGADQgGAEgRgPQgQgLgGAJQgKAQgCACQgDADgFAAQgGAAgIgEg");
	this.shape_64.setTransform(21.7,27.9,0.677,0.677);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.instance,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.instance},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#ECE9E9").s().p("AFwLnIgIAAQkvgDjWjXQjZjZAAk0QAAkzDZjaQDWjWEvgDIAIAAIAHAAIAAXNIgHAAg");
	this.shape_65.setTransform(-37.6,0);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FF6666").s().p("AAAM3IgHAAQlQgDjujvQjwjwAAlVQAAlUDwjxQDujuFQgDIAHAAIAHAAQFQADDvDuQDxDxgBFUQABFVjxDwQjvDvlQADIgHAAgAooooQjlDlAAFDQAAFEDlDlQDiDiE/ACIAHAAIAHAAQE/gCDijiQDljlABlEQgBlDjljlQjijik/gCIgHAAIgHAAQk/ACjiDig");
	this.shape_66.setTransform(-0.8,0);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFCC00").s().p("AAAMNIgHAAQk/gCjijiQjljlAAlEQAAlDDljlQDijiE/gCIAHAAIAHAAQE/ACDiDiQDlDlABFDQgBFEjlDlQjiDik/ACIgHAAgAoMoNQjZDaAAEzQAAE0DZDZQDWDXEvADIAHAAIAHAAQEvgDDXjXQDajZAAk0QAAkzjajaQjXjWkvgDIgHAAIgHAAQkvADjWDWg");
	this.shape_67.setTransform(-0.8,0);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AlvrmQEvADDWDWQDaDaAAEzQAAE0jaDZQjWDXkvADg");
	this.shape_68.setTransform(36.6,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65}]}).wait(1));

	// Layer 4
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("rgba(0,0,0,0.027)").s().p("Ap/KAQkJkJAAl3QAAl2EJkJQEJkJF2AAQF3AAEJEJQEJEJAAF2QAAF3kJEJQkJEIl3AAQl2AAkJkIg");
	this.shape_69.setTransform(-0.8,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_69).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy, new cjs.Rectangle(-91.3,-90.4,181,180.9), null);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ch1 = new lib.inside_fruitcopy5();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(25.9,-4.4,1.847,1.847,0,0,0,0.1,0.1);

	this.instance = new lib.qus_1();
	this.instance.parent = this;
	this.instance.setTransform(2.1,31.7,1.309,1.309,0,0,0,110.7,110.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.ch1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-140.5,-140.5,281.1,281.1);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ch1 = new lib.inside_fruitcopy2();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(373.6,-23.3,1.847,1.847,0,0,0,0.1,0.1);

	this.ch1_1 = new lib.inside_fruitcopy();
	this.ch1_1.name = "ch1_1";
	this.ch1_1.parent = this;
	this.ch1_1.setTransform(-380.6,-23.3,1.847,1.847,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ch1_1},{t:this.ch1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-549.4,-190.6,1088.4,334.1);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.qus();
	this.instance.parent = this;
	this.instance.setTransform(2.1,31.7,1.309,1.309,0,0,0,110.7,110.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:110.6,regY:110.5,scaleX:1.35,scaleY:1.35,x:2,y:31.6},9).to({regX:110.7,regY:110.6,scaleX:1.31,scaleY:1.31,x:2.1,y:31.7},10).to({regX:110.6,regY:110.5,scaleX:1.35,scaleY:1.35,x:2,y:31.6},10).to({regX:110.7,regY:110.6,scaleX:1.31,scaleY:1.31,x:2.1,y:31.7},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-140.5,-140.5,281.1,281.1);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(642.8,299.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(406).to({_off:false},0).wait(44).to({x:638.8,y:319.6,startPosition:16},0).to({alpha:0,startPosition:7},9).wait(1));

	// Layer_11
	this.instance_1 = new lib.Tween4("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(639.8,278.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(385).to({_off:false},0).to({scaleX:1.1,scaleY:1.1,y:278.3},21).to({startPosition:0},44).to({alpha:0},9).wait(1));

	// Layer_3
	this.ch1 = new lib.inside_fruitcopy3();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(281.1,540.2,1.847,1.847,0,0,0,0.1,0.1);
	this.ch1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.ch1).wait(344).to({_off:false},0).to({x:665.7,y:273.9},31).to({_off:true},10).wait(75));

	// Layer_2
	this.instance_2 = new lib.Symbol1copy_1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(405.6,602.7,1,1,-19.2,0,0,190.5,64.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(309).to({_off:false},0).to({_off:true},35).wait(116));

	// Layer_8
	this.instance_3 = new lib.Symbol1copy("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(261.1,296.5,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(274).to({_off:false},0).to({_off:true},35).wait(151));

	// Layer_1
	this.instance_4 = new lib.quscopy4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_4.alpha = 0.699;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(179).to({_off:false},0).to({x:1012.5,y:576.6},17).wait(38).to({alpha:0.5},0).to({alpha:0},6).to({_off:true},14).wait(206));

	// Layer_10
	this.instance_5 = new lib.quscopy5();
	this.instance_5.parent = this;
	this.instance_5.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);

	this.instance_6 = new lib.quscopy3();
	this.instance_6.parent = this;
	this.instance_6.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_6._off = true;

	this.instance_7 = new lib.quscopy6();
	this.instance_7.parent = this;
	this.instance_7.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6},{t:this.instance_5}]},344).to({state:[{t:this.instance_6},{t:this.instance_7}]},31).to({state:[{t:this.instance_6}]},78).to({state:[{t:this.instance_6}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(344).to({_off:false},0).wait(109).to({alpha:0},6).wait(1));

	// Layer_7
	this.instance_8 = new lib.quscopy4();
	this.instance_8.parent = this;
	this.instance_8.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_8.alpha = 0.699;
	this.instance_8._off = true;

	this.instance_9 = new lib.quscopy3();
	this.instance_9.parent = this;
	this.instance_9.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},179).to({state:[{t:this.instance_8}]},17).to({state:[{t:this.instance_8}]},38).to({state:[{t:this.instance_8}]},6).to({state:[]},14).to({state:[{t:this.instance_9}]},137).to({state:[]},1).wait(68));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(179).to({_off:false},0).to({regY:110.7,scaleX:1.39,scaleY:1.39,x:258.2,y:579.7},17).wait(38).to({alpha:0.5},0).to({alpha:0},6).to({_off:true},14).wait(206));

	// Layer_9
	this.instance_10 = new lib.Symbol4copy("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(639.8,278.4);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(151).to({_off:false},0).to({_off:true},122).wait(187));

	// Layer_7
	this.instance_11 = new lib.Symbol1("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(761.4,96.8);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(151).to({_off:false},0).wait(116).to({startPosition:16},0).to({alpha:0,startPosition:22},6).to({_off:true},1).wait(186));

	// Layer_6
	this.instance_12 = new lib.Tween1("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,566.1);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(107).to({_off:false},0).to({alpha:1},6).to({startPosition:0},185).to({startPosition:0},155).to({alpha:0},6).wait(1));

	// Layer_5
	this.instance_13 = new lib.qus();
	this.instance_13.parent = this;
	this.instance_13.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(57).to({_off:false},0).to({alpha:1},6).wait(116).to({_off:true},165).wait(116));

	// Layer_4
	this.questxt = new lib.InstructionText_mc();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(638.7,119.2,1.539,1.539,0,0,0,0.1,0.1);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(24).to({_off:false},0).to({alpha:1},6).wait(355).to({alpha:0},6).to({_off:true},1).wait(68));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(522.5,235.2,1579,938);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;