(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AAGgmIgwCWIhIAFIgqjoIA0gEIAfC1IA8i1IAwADIAnC3IAai5IA5ACIgsDqIhIACg");
	this.shape.setTransform(166.6,2.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AguB6QgWgJgQgSQgRgRgJgYQgJgYAAgeQAAgdAJgYQAJgYARgRQAQgSAWgJQAVgKAZAAQAZAAAVAKQAXAKARASQAPASAKAYQAKAYgBAbQABAcgKAYQgKAYgPASQgRASgWAKQgWAKgZAAQgZAAgVgKgAgchMQgNAHgIAMQgIAMgEAPQgDAQAAAOQgBAPAFAPQAEAPAIAMQAIAMANAIQALAHAQAAQARAAAMgHQAMgIAJgMQAIgMAEgPQAEgPAAgPQAAgOgEgPQgEgQgIgMQgIgMgMgHQgNgIgRAAQgQAAgMAIg");
	this.shape_1.setTransform(136,2.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgyCfQgXgJgRgRQgQgRgKgYQgJgZAAgeQAAgeAKgXQALgYARgQQARgRAWgIQAWgJARAAQATAAARAEQALADAQAHQAPAIAGAMIAChvIAxAEIAAFHIg2AAIABgaIAAgCIAAACQgBAEgGAEQgHAFgHADIgOAGIgSAEQgIAEgOAAQgaAAgWgJgAgfghQgMAGgJALQgKAKgFAOQgFAPAAASQAAARAFAPQAGAOAJAKQAJALANAFQANAGAPAAQAMAAAMgEQAMgEAKgIQAKgHAIgKQAHgLAEgMIABgtQgEgMgIgKQgIgLgKgIQgLgIgMgEQgMgEgMAAQgPAAgNAGg");
	this.shape_2.setTransform(106.5,-1.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AA/B/IADgeQgVARgaAIQgZAIgWAAQgRAAgPgFQgPgEgMgJQgMgJgHgOQgIgNAAgTQAAgSAGgPQAGgPAJgKQAJgLANgIQANgIAPgFQAOgFARgCQAPgCAPAAIAbABIARADIgHgPQgEgLgFgHQgGgIgIgFQgJgFgMAAQgHAAgKADQgKACgNAGQgMAHgPALQgQALgUATIgfglQAXgWATgNQAUgMAQgIQARgHAQgCQAOgDAMAAQAUAAARAHQARAHAMANQAMAMAIAQQAJARAFATQAGASACAUQACAUAAATQAAAVgCAYQgDAZgFAfgAgJADQgRAEgMAIQgNAIgGAMQgGALADANQADANAIAEQAHAEALAAQAMAAAPgEQANgEAOgGIAbgNIATgLIABgTIgBgTIgTgEQgMgBgLAAQgTAAgRAEg");
	this.shape_3.setTransform(77.6,2.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA1CpQAHgZACgUQADgUABgQIABghQgBgXgHgPQgGgQgKgJQgJgJgMgEQgLgFgKAAQgLABgMAGQgKAFgMALQgLAJgKASIgCCTIgzABIgElWIA7gCIgBCEQAEgMAOgIQAOgHAKgEQAQgFANgCQAaABAUAIQAVAKAPAQQAPASAJAYQAJAXABAfIgBAcIgBAiQgBAQgDANQgCAOgEAPg");
	this.shape_4.setTransform(49.4,-2.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgdB/QgLgBgLgDQgKgDgKgEQgKgEgKgGQgKgFgLgKIAXgmIAcAMIAZAKQANAFANACQAMADAPAAQALAAAIgCIAMgFQAFgDACgEIADgFIABgIIgCgHQgBgDgEgEQgEgEgGgDQgGgDgKgCIgYgDQgSgBgSgDQgTgDgPgHQgOgHgKgLQgKgMgDgRQgBgRAEgOQADgOAIgKQAIgLALgIQALgIANgGQANgFAPgCQAOgDAOAAIAUABQAMABANADQANACANAFQANAFAOALIgSAtQgSgIgNgEIgXgGIgVgEQgfgCgRAJQgRAIAAAPQAAAMAGAFQAGAEALADQAMACAQABIAiAEQAWADAPAHQAPAHAKAIQAKAKAEALQAEALAAAMQAAAWgJAPQgJAPgPAJQgOAKgTAEQgTAEgUABQgTAAgSgEg");
	this.shape_5.setTransform(21.5,3.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgegUIhLACIABgxIBLgCIABhfIA0gDIgCBhIBUgDIgDAxIhSADIgBC8Ig2ABg");
	this.shape_6.setTransform(-13.5,-1.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgpB5QgXgKgTgRQgTgSgLgYQgLgXAAgdQAAgPAFgRQAEgQAJgPQAIgOAMgNQAMgMAPgJQAPgJARgEQARgGARAAQATAAASAFQASAFAPAJQAPAJAMAMQAMAOAIAPIABAFIgrAZIgCgEQgFgLgIgIQgIgIgJgGQgJgGgLgDQgLgDgMAAQgQAAgOAGQgPAGgLAMQgLALgGAPQgHAOAAAQQAAARAHAPQAGAPALALQALALAPAGQAOAHAQAAQALAAAKgDQALgDAJgFQAJgGAHgHQAIgHAFgKIAEgDIAsAYIgBAEQgJAQgNALQgNAMgPAIQgPAJgRAEQgRAFgSAAQgYAAgYgKg");
	this.shape_7.setTransform(-38.1,2.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgyB4QgXgIgQgRQgPgQgIgYQgJgXAAgcQAAgbAKgYQAJgZARgSQARgSAWgKQAXgKAZAAQAXAAAVAJQAVAJAQAQQAQAQAKAWQAKAWADAdIi7AbQACAKAFAMQAGAMAJAHQAJAIALAEQAMAFANAAQAKAAALgEQALgDAJgHQAJgGAGgKQAHgJAEgQIAyAJQgHAZgLARQgMARgPALQgPAMgTAHQgSAGgUAAQgdAAgXgJgAgQhPQgKADgKAHQgJAHgIAMQgHAMgDANIB4gPIgBAAQgIgUgNgLQgOgLgTAAQgIAAgKADg");
	this.shape_8.setTransform(-65.4,2.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AhlA2IgCg2IgCgpIgBgfIgCgzIA7gCIABAkIAAAWIAAAPIAAAGIAAgGQABgGAGgIQAJgMAMgLQAMgLANgIQAOgJAQgCQATgBAQAJQAHAFAGAFQAGAHAGAJQAFAJAEANQADANABAVIgzASQAAgPgCgHQgBgIgDgFQgCgFgDgDQgCgDAAgCQgKgFgHAAQgGAAgGAFQgGADgGAHIgOAOIgOARIgMARIgKAPIAAAeIABAdIABAYIABAWIg3AGIgChHg");
	this.shape_9.setTransform(-90.2,2.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AhmA2IgCg2IgBgpIgBgfIgBgzIA6gCIAAAkIAAAWIABAPIAAAGIAAgGQABgGAGgIQAJgMAMgLQAMgLANgIQAOgJAQgCQAUgBAPAJQAGAFAHAFQAHAHAEAJQAGAJADANQAEANACAVIg0ASQAAgPgCgHQgCgIgCgFQgCgFgDgDQgCgDABgCQgLgFgHAAQgFAAgHAFQgGADgHAHIgNAOIgNARIgMARIgLAPIAAAeIACAdIAAAYIACAWIg4AGIgDhHg");
	this.shape_10.setTransform(-115,2.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AguB6QgWgJgRgSQgQgRgJgYQgKgYABgeQgBgdAKgYQAJgYAQgRQARgSAWgJQAVgKAZAAQAYAAAWAKQAXAKAQASQARASAJAYQAJAYAAAbQAAAcgJAYQgJAYgRASQgQASgWAKQgWAKgZAAQgZAAgVgKgAgchMQgNAHgIAMQgIAMgEAPQgDAQAAAOQgBAPAFAPQAEAPAIAMQAIAMANAIQALAHAQAAQARAAAMgHQAMgIAJgMQAIgMAEgPQAEgPAAgPQAAgOgEgPQgEgQgIgMQgIgMgMgHQgNgIgRAAQgQAAgMAIg");
	this.shape_11.setTransform(-142.2,2.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgpB5QgXgKgTgRQgTgSgLgYQgLgXAAgdQAAgPAFgRQAEgQAJgPQAIgOAMgNQAMgMAPgJQAPgJARgEQARgGARAAQATAAASAFQASAFAPAJQAPAJAMAMQAMAOAIAPIABAFIgrAZIgCgEQgFgLgIgIQgIgIgJgGQgJgGgLgDQgLgDgMAAQgQAAgOAGQgPAGgLAMQgLALgGAPQgHAOAAAQQAAARAHAPQAGAPALALQALALAPAGQAOAHAQAAQALAAAKgDQALgDAJgFQAJgGAHgHQAIgHAFgKIAEgDIAsAYIgBAEQgJAQgNALQgNAMgPAIQgPAJgRAEQgRAFgSAAQgYAAgYgKg");
	this.shape_12.setTransform(-169.4,2.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(3,1,1).p("A7NlpMA2bAAAQCaAAAACZIAAGhQAACZiaAAMg2bAAAQiaAAAAiZIAAmhQAAiZCaAAg");
	this.shape_13.setTransform(0,3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("A7NFqQiaAAAAiZIAAmhQAAiZCaAAMA2bAAAQCaAAAACZIAAGhQAACZiaAAg");
	this.shape_14.setTransform(0,3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191,-39.1,382.2,79.9);


(lib.question_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AADgUIgZBQIgmACIgWh7IAcgBIAQBfIAghfIAYABIAVBhIAOhiIAeABIgXB8IgmAAg");
	this.shape.setTransform(152.2,1.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYBBQgMgFgIgJQgJgKgFgMQgFgNAAgQQAAgPAFgMQAFgNAJgJQAIgKAMgFQAMgFAMAAQANAAALAGQAMAFAJAJQAJAKAFAMQAFANAAAOQAAAPgFANQgFANgJAJQgJAJgMAGQgLAFgNAAQgMAAgMgFgAgPgoQgGAEgEAGQgFAHgCAIQgCAIAAAHQAAAIACAIQADAIAEAGQAEAHAHAEQAGAEAIAAQAJAAAGgEQAHgEAFgHQADgGACgIQADgIAAgIQAAgHgCgIQgCgIgEgHQgFgGgHgEQgGgEgJAAQgIAAgHAEg");
	this.shape_1.setTransform(136,1.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbBUQgLgFgJgIQgJgKgFgNQgFgNAAgPQAAgQAFgMQAGgNAJgJQAKgIALgFQAMgEAJgBQAKABAIACQAHABAHAEQAIAFAEAFIABg6IAaACIAACsIgdAAIABgOIAAAAIAAAAQgBADgDACIgIAFIgHADIgKACQgEACgHAAQgOAAgMgFgAgQgRQgHADgFAGQgEAGgDAGQgCAIgBAJQABAKACAHQADAIAFAGQAFAFAGADQAIADAHAAQAGAAAHgCQAGgCAGgEQAFgEAEgFIAGgMIAAgYQgCgHgEgEQgEgHgFgEQgHgEgGgCQgGgDgGAAQgIAAgHAEg");
	this.shape_2.setTransform(120.4,-0.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAhBDIACgQQgLAKgNAEQgNADgMAAQgJAAgJgCQgHgCgGgFQgHgEgEgIQgEgHAAgKQAAgJADgJQADgHAFgFQAFgGAHgEQAHgEAHgDIARgEIAQgBIANAAIAKACIgDgIQgDgFgDgFQgDgEgEgCQgFgDgGAAIgJACIgMAEQgGAEgJAFIgTARIgQgUQAMgMALgGQAKgIAJgDQAJgEAIgBIANgBQALgBAJAEQAIAEAHAGQAHAHAEAIQAEAJADAKIAFAVIAAAUIAAAYIgFAegAgEACQgKACgGAEQgHAEgDAHQgDAFABAHQACAHAEACQAEACAGAAQAGAAAIgCIAOgFIAOgGIALgHIAAgKIAAgKIgKgCIgNAAQgJAAgJACg");
	this.shape_3.setTransform(105.1,1.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAdBZIAEgYIACgSIABgSQgBgMgEgIQgDgIgFgFQgFgFgGgCQgGgCgFAAQgGAAgGADQgGADgFAFQgHAFgFAKIgBBOIgbAAIgCi1IAfgBIAABGQACgGAHgEIANgGIAPgEQAOAAALAFQALAFAIAJQAHAJAFANQAEALABARIAAAPIgBASIgCAQIgEAPg");
	this.shape_4.setTransform(90.1,-0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPBDIgLgCIgLgEIgKgFIgMgIIANgVIAPAHIAMAGIAPADIANABIAKgBIAGgCIAEgDIACgDIABgEIgBgEIgEgEIgEgDIgJgEIgNgBQgJAAgKgCQgJgBgIgFQgIgDgFgGQgGgFAAgKQgBgJABgHQACgHAFgGQADgGAHgEQAFgFAHgCQAIgDAHgBIAPgBIAKAAIANACIAOAEQAHACAHAGIgJAYIgQgHIgNgDIgLgBQgQgCgJAFQgJAEAAAIQAAAHADACQADACAGACIAPABIASACQAMACAHAEQAIAEAGAEQAEAFADAFQACAGAAAHQAAAMgFAIQgEAIgIAEQgIAFgKADQgKACgKAAQgKAAgKgCg");
	this.shape_5.setTransform(75.3,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPgKIgoABIAAgaIAogBIABgzIAbgBIgBAzIAsgBIgBAaIgrABIgBBjIgcABg");
	this.shape_6.setTransform(56.7,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgVBAQgNgFgKgJQgJgKgGgMQgGgNAAgPQAAgIACgJQADgIAEgIQAFgIAGgGQAGgHAIgEQAIgFAJgDQAJgCAJAAQAKAAAJACQAKADAIAEQAIAFAGAHQAGAHAFAIIAAADIgXANIgBgCIgHgKQgEgFgFgDIgKgFIgMgBQgIAAgIADQgIAEgGAFQgFAGgEAIQgDAIAAAIQAAAJADAIQAEAIAFAGQAGAGAIADQAIADAIAAQAGAAAFgBQAFgCAFgDQAFgCAEgEIAHgJIACgCIAXANIgBACQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgNAAgMgFg");
	this.shape_7.setTransform(43.7,1.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgaBAQgNgFgHgJQgJgIgFgNQgDgMAAgPQAAgOAEgNQAFgNAJgJQAJgKAMgFQAMgGAMAAQANAAALAFQALAFAJAJQAIAIAFAMQAGALABAQIhiAOQAAAFAEAGQADAGAEAEQAFAFAGACQAHACAFAAQAGAAAGgCQAFgCAGgDQAEgDADgFQAEgFACgJIAaAFQgDANgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgOAAgMgFgAgIgqQgGACgEAEQgFADgEAHQgEAGgDAHIBAgIIgBAAQgDgKgIgGQgHgGgLAAIgIABg");
	this.shape_8.setTransform(29.3,1.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag1AdIgCgdIAAgWIgBgQIgBgbIAfgBIABATIAAALIAAAJIAAADIAAgDQAAgDAEgEIALgNQAGgFAHgFQAHgEAJgCQAKAAAIAFIAHAFIAGAIIAFAMQACAHABALIgcAKIgBgNIgCgGIgDgFQAAAAAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAQgFgCgEAAIgGACIgHAGIgGAHIgIAJIgGAJIgGAHIABAQIAAAQIABAMIAAAMIgdAEIgBgmg");
	this.shape_9.setTransform(16.1,1.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag1AdIgCgdIAAgWIgBgQIgBgbIAfgBIABATIAAALIAAAJIAAADIAAgDQAAgDAEgEIALgNQAGgFAHgFQAHgEAJgCQAKAAAIAFIAHAFIAGAIIAFAMQACAHABALIgcAKIgBgNIgCgGIgDgFQAAAAAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAQgFgCgEAAIgGACIgHAGIgGAHIgIAJIgGAJIgGAHIABAQIAAAQIABAMIAAAMIgdAEIgBgmg");
	this.shape_10.setTransform(3,1.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgYBBQgMgFgJgJQgIgKgFgMQgFgNAAgQQAAgPAFgMQAFgNAIgJQAJgKAMgFQALgFANAAQANAAALAGQAMAFAJAJQAJAKAFAMQAFANAAAOQAAAPgFANQgFANgJAJQgJAJgMAGQgLAFgNAAQgNAAgLgFgAgPgoQgGAEgFAGQgEAHgCAIQgCAIAAAHQAAAIADAIQACAIAEAGQAEAHAHAEQAGAEAIAAQAJAAAGgEQAHgEAEgHQAFgGACgIQACgIAAgIQAAgHgCgIQgCgIgFgHQgDgGgIgEQgGgEgJAAQgIAAgHAEg");
	this.shape_11.setTransform(-11.4,1.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgVBAQgNgFgKgJQgJgKgGgMQgGgNAAgPQAAgIACgJQADgIAEgIQAFgIAGgGQAGgHAIgEQAIgFAJgDQAJgCAJAAQAKAAAJACQAKADAIAEQAIAFAGAHQAGAHAFAIIAAADIgXANIgBgCIgHgKQgEgFgFgDIgKgFIgMgBQgIAAgIADQgIAEgGAFQgFAGgEAIQgDAIAAAIQAAAJADAIQAEAIAFAGQAGAGAIADQAIADAIAAQAGAAAFgBQAFgCAFgDQAFgCAEgEIAHgJIACgCIAXANIgBACQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgNAAgMgFg");
	this.shape_12.setTransform(-25.9,1.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgaBAQgNgFgIgJQgIgIgFgNQgDgMAAgPQgBgOAFgNQAFgNAJgJQAJgKAMgFQAMgGANAAQAMAAALAFQAMAFAIAJQAIAIAFAMQAGALACAQIhjAOQAAAFADAGQADAGAFAEQAFAFAGACQAGACAHAAQAFAAAGgCQAGgCAEgDQAFgDADgFQAEgFACgJIAbAFQgEANgGAJQgGAJgIAGQgIAHgKADQgJAEgLAAQgPAAgMgFgAgIgqQgFACgFAEQgGADgDAHQgFAGgBAHIA/gIIgBAAQgEgKgGgGQgIgGgKAAIgJABg");
	this.shape_13.setTransform(-46.6,1.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAcBZIAFgYIACgSIAAgSQAAgMgDgIQgEgIgFgFQgFgFgGgCQgGgCgGAAQgFAAgGADQgFADgHAFQgGAFgFAKIgBBOIgbAAIgCi1IAfgBIgBBGQADgGAHgEIANgGIAPgEQANAAALAFQAMAFAHAJQAJAJAEANQAFALAAARIAAAPIgBASIgCAQIgDAPg");
	this.shape_14.setTransform(-61.6,-0.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgPgKIgoABIAAgaIAogBIABgzIAbgBIgBAzIAsgBIgBAaIgrABIgBBjIgcABg");
	this.shape_15.setTransform(-75.3,-0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgPgKIgoABIAAgaIAogBIABgzIAbgBIgBAzIAsgBIgBAaIgrABIgBBjIgcABg");
	this.shape_16.setTransform(-93.1,-0.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgVBAQgNgFgKgJQgJgKgGgMQgGgNAAgPQAAgIACgJQADgIAEgIQAFgIAGgGQAGgHAIgEQAIgFAJgDQAJgCAJAAQAKAAAJACQAKADAIAEQAIAFAGAHQAGAHAFAIIAAADIgXANIgBgCIgHgKQgEgFgFgDIgKgFIgMgBQgIAAgIADQgIAEgGAFQgFAGgEAIQgDAIAAAIQAAAJADAIQAEAIAFAGQAGAGAIADQAIADAIAAQAGAAAFgBQAFgCAFgDQAFgCAEgEIAHgJIACgCIAXANIgBACQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgNAAgMgFg");
	this.shape_17.setTransform(-106.1,1.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgaBAQgMgFgJgJQgIgIgEgNQgEgMgBgPQAAgOAFgNQAFgNAJgJQAJgKAMgFQAMgGANAAQAMAAALAFQAMAFAHAJQAJAIAFAMQAGALACAQIhjAOQAAAFADAGQAEAGAEAEQAFAFAGACQAGACAHAAQAFAAAGgCQAGgCAEgDQAFgDADgFQAEgFACgJIAbAFQgEANgGAJQgGAJgIAGQgIAHgKADQgKAEgKAAQgPAAgMgFgAgIgqQgFACgGAEQgEADgFAHQgEAGgBAHIA/gIIAAAAQgEgKgHgGQgIgGgKAAIgJABg");
	this.shape_18.setTransform(-120.5,1.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAGBmQgGgCgEgFQgGgEgDgGQgDgFgDgKQgFgKgCgRIADiTIAcAAIgBAoIgBAfIgBAZIAAATIgBAeQABALACAHIADAIIAEAFIAGAEQADABAHABIgDAbQgLAAgHgDg");
	this.shape_19.setTransform(-130.3,-2.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgaBAQgNgFgHgJQgJgIgFgNQgEgMAAgPQABgOAEgNQAFgNAJgJQAJgKAMgFQAMgGAMAAQANAAALAFQALAFAIAJQAJAIAGAMQAFALABAQIhiAOQAAAFAEAGQACAGAGAEQAEAFAGACQAHACAFAAQAHAAAFgCQAFgCAGgDQAEgDAEgFQADgFACgJIAaAFQgDANgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgOAAgMgFgAgIgqQgGACgFAEQgEADgFAHQgEAGgCAHIBAgIIgBAAQgDgKgIgGQgHgGgLAAIgIABg");
	this.shape_20.setTransform(-141.6,1.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgPBeIgRgEQgJgEgIgFQgIgEgJgIIATgXQALAJAIAEIARAHIANAEQAHAAAHgCQAHgCAFgCQAFgEADgEQADgEAAgFIgBgGIgFgFIgIgFIgKgEIgLgDIgLgDIgNgCIgPgGQgHgCgGgGQgHgEgEgGQgFgHgDgJQgCgJABgMQAAgJAEgIQADgIAFgFQAFgHAHgEQAHgEAIgDIAPgFIAPgBQAMABALADIAJADIALAEIALAGIAKAJIgPAYIgJgIIgIgGIgIgDIgJgDQgIgCgIgBQgJAAgHADQgIAEgFAEQgGAFgCAFQgDAHAAAFQAAAFADAGQADAFAGAEIAOAHQAIADAJABIARADQAIACAIACQAIAEAHAEQAGAFAFAGQAEAHACAIQADAIgCAJQgBAIgEAHQgDAGgFAGIgMAIQgHADgHACIgOACIgOACQgIgBgJgCg");
	this.shape_21.setTransform(-156.6,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#660000").ss(3,1,1).p("A4IjLMAwRAAAQBBAAAvAvQAuAuAABBIAABbQAABBguAvQgvAuhBAAMgwRAAAQhBAAgvguQgugvAAhBIAAhbQAAhBAuguQAvgvBBAAg");

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF3333").s().p("A4IDLQhBAAgvgtQgugvAAhBIAAhbQAAhAAugvQAvgvBBABMAwRAAAQBBgBAvAvQAuAvAABAIAABbQAABBguAvQgvAthBAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.question_mc, new cjs.Rectangle(-173.6,-21.8,345.4,43.7), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("AibhbIBUmWIA4ABIA8ACIBbGWAibhbIC6AEIBpACIBDACICIACIleJDIg3hkIkQnsg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("AkOgsIC3ADIC6AEIBoACIBEACIkOHhgABjglgAhXgpIBTmWIA4ABIAvGZg");
	this.shape_1.setTransform(-6.7,-5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF99").s().p("AjKGNIENngICIACIleJCgAhohYIgvmYIA9ABIBaGXIAAACg");
	this.shape_2.setTransform(13.6,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-35.3,-51.2,70.8,102.5);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("Aj9E+IgVk0IgGhPIgEhCIACgUQARg5BAguQBXg7B2AAQB6AABVA7QBWA7ABBUIAAACQAAARgEAOIgFBWIgZE6gAi3joQg2AmgOAwQgDAFAAAIIAfGkIHMAAIAemUQgDhEhGgvQhPg2hvAAQhtAAhOA2gAF2DnIgQgbQAAgnBFAVQBFAWAAATQAAAdgrAAQg0AAgbgZgAosDZQgRgCgIgMQgFgIAAgIQAAgGAFgIQAIgNARgBICsAAQASABAFANQAEAFAAAJQAAAcgbACgAAKCdIgEgGIAAlAQAWAGAFA8QABASAABQIgBCiQgIADgFABQgIgBgCgDgAGeB5IgEgOQAAgcA+AAQA/AAAAAcQAAANgHAEQgMAMgsAAQgvAAgLgPgAm3BzIgEgMQAAgeAoAAQApAAAAAeQAAAbgpAAQgbAAgJgPgAqhBcIgEgOQAAgbA/AAQA+AAAAAbQAAANgEAFQgOALgsAAQgvAAgMgPgAJfBLIgCgOQAAgcAkAAQAlAAAAAcQAAAeglAAQgaAAgIgQgABdAzIgCgIQAGgKAEgCQAHgFAZAHQAAABAAAAQABAAAAAAQABABAAAAQABAAABAAIAAAUQgJAEgOAAQgNAAgIgIgAg8AvQgCAEAAgNQAAgIANgEQAPgEAaAEQAAABAAAAQAAABAAAAQABABAAAAQABABABAAIAAAIQAAAKgIADQgGAEgNAAQgVAAgHgIg");
	this.shape.setTransform(-9.5,67.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000099").s().p("AF6I9QANg3ADhEQABgLAAgrIBPgXQgeBSgxB2gAEmI9QgMgigbhbIgHgNIAMABIBqggIAAAxQgKAYgFAbIgBAzQAAAJAGAJgAnCnQQgRgQgGgRIABAAQASAIADAAIAgAAIAqAAQASgGAfgcIAsgmQANgKALgBQAIARAAASQAAAqgjAfQghAegwAAQgwAAgigeg");
	this.shape_1.setTransform(2.5,-4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AC+GcQAAgNADgMIAAgBIABgIQBRgtAmgJQAJgDAYgCIAACiIgcAIQhGATgZAWQgghpgBgNgAF3G9QAAhDgBgKQgDgcgEgVQAVgCAbAAQA7AAAPAEIAQAFQgRA7gaBMQguAGgpAJIAAgfgAnrmnIgNAAIAAgCQAAgrAigfQAigeAwAAQAvAAAiAeQAMALAHALQgfgCgqAdQg0AjglAAQgcAAgNgIg");
	this.shape_2.setTransform(4.5,-15.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9900").s().p("AmfGgIgblyIABgDQAMggAlgZQBDguBeAAQBeAABFAuQAtAfAGAsIgbFjgAjlEeIADAFQACAEAIABQAGgBAIgEIABijQAAhQgBgSQgGg6gVgHgAiHClQgDADgHAKIACAIQAIAIAOAAQANAAAJgFIAAgTQgBAAAAgBQgBAAAAAAQgBAAAAgBQAAAAAAAAQgQgFgIAAQgGAAgDACgAkNC9QAOAAAGgDQAHgEAAgKIAAgIQAAAAgBAAQgBgBAAAAQAAgBgBAAQAAgBAAAAQgagFgOAFQgOADAAAIQAAAOACgFQAHAIAVAAgACcAMQgHgMgqiFQgtiMAAgRQAAgQAEgPQADgPAFgHQAAgBAAAAQAAAAAAgBQAAAAAAAAQABAAAAAAQBdg2ApgJQAVgHBYAAQBBAAAQAEQAXAFAVAQQgQBAgfBXQADACAAAHIgFACIgHANQgfBag5CJgAEWjCQAAArgBALQgDBEgNA3IARAAQAxh2AehSIhPAXgACKiOQAbBbAMAiIA+AAQgGgJAAgJIABgzQAFgbAKgYIAAgxIhqAgIgMgBIAHANgADYl6QgmAJhRAtIgBAIIAAABQgDAMAAANQACANAfBpQAZgWBGgTIAcgIIAAiiQgYACgJADgAEOl/QAFAVACAcQABAKAABDIAAAfQAqgJAtgGQAbhMAQg7IgQgFQgPgEg7AAQgaAAgWACg");
	this.shape_3.setTransform(14.2,54.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DFAD37").s().p("AgUBsQg4gwgbgmQAMgeAFgnQAOhFAZAAQARAAA0ANQA1APAdAAQhNBPghB/IgOgKg");
	this.shape_4.setTransform(64.6,-21);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("ArBLOIgKAAIhKndQA2gbBygxQCOg9CAgxQBUggCohmQB+hNBBAAQAoAACsBfQBrA6BvBFQBcA2CwBFIhRILQgxAOgzAAQj9gFg9ABIAGhWQADgPAAgRIAAgCQgBhUhVg7QhWg7h5ABQh3gBhWA7QhAAugRA6IgDAUQgCAKAAALQAAAWAHAWIAGBQIi3ACQiMAAhzgMgAHECuQgpAJhdA2QAAAAgBABQAAAAAAAAQAAAAAAABQAAAAAAABQgFAHgDAPQgEAOAAARQAAAQAtCNQAqCEAHAOICPAAQA5iKAfhaIAHgOIAFgBQAAgIgDgBQAfhYAQhAQgVgQgXgEQgQgFhBABQhYAAgVAGgAn9oeQC2i7BsAAQAHAABsA0QCLBDBxAtQC3BKChAoIgbEhIkXieQjhhdi0B8IkECRgAlgqAQgtAoAAA5QAAA4AtAnQAtAqBAAAQA/AAAtgqQAtgnAAg4QAAg5gtgoQgtgqg/AAQhAAAgtAqg");
	this.shape_5.setTransform(-10.1,-4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC6600").s().p("AESOWIAZk6QA9gBD9AFQAzAAAxgOIglDxQgNBTh5AAgAFxMlIAQAbQAbAYAzAAQArAAAAgdQAAgThEgVQgZgIgPAAQgdAAAAAagAGlLEIADANQALAPAwAAQArAAANgLQAHgFAAgMQAAgcg/AAQg+AAAAAcgAJnKWIADANQAHAQAaAAQAlAAAAgdQAAgcglAAQgkAAAAAcgAodOWQh6AAgMhTIgkjrIAKAAQBzAMCMAAIC3gCIAVE0gAo7MFQgEAIAAAHQAAAHAEAIQAIAMARACICsAAQAbgCAAgbQAAgKgDgFQgGgMgSgBIisAAQgRABgIAMgAmwK/IADANQAJAOAbAAQAqAAAAgbQAAgegqAAQgnAAAAAegAqaKnIADANQANAQAvAAQAsgBANgKQAFgGAAgMQAAgbg+AAQg/AAAAAbgAseA0IgBgKIJIlIQC0h7DhBdIJiFbQAAAKgCALIgEATQiwhFhcg1QhvhGhrg6QishfgoAAQhBAAh+BNQioBnhUAgQiAAxiOA8QhyAxg2AbgAJnjFQASgcAOgoQAbAlA4AwIAOAKIgJArgAlco2QgtgnAAg4QAAg5AtgoQAtgqBAAAQA/AAAtAqQAtAoAAA5QAAA4gtAnQgtAqg/AAQhAAAgtgqgAlApNQAiAeAwAAQAwAAAhgeQAjgfAAgrQAAgRgIgRQgLABgNAKIgsAmQgfAcgSAGIgqAAIggAAQgDAAgSgIIgBAAQAGAQARARgAlArgQghAeAAArIAAADIAMAAQANAHAdABQAlgBAzgiQAqgdAgACQgIgLgMgLQghgegwAAQgwAAgiAegACYqrQhxgtiLhDQhsg0gHAAQhsAAi2C7IgDgtIAthAQA4hOAWgYQAqguAjAAQAEAAD1BeIEdBqQA3AUDMAoQAKADAJACIgIBTQihgoi3hKg");
	this.shape_6.setTransform(-10.5,7.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("Ak/OhIgemlQAAgIACgFQAPgwA2gmQBNg2BuAAQBvAABPA2QBFAvAEBEIgfGVgAj7HtQglAagMAfIgBAEIAbFyIFyAAIAblkQgHgsgsgfQhFguheAAQhdAAhDAugAK2BIIpilaQjhhei0B8IpIFHIg7AiIgEAAQAAgTgBgHIgFgEIKNlwQC0h7DhBcIN7H8IgDAEQgLAPgHAGgArWpEQAsg9BNhuQB6ixAmAAQA+AACTBHQDIBhBIAYQCXA2EHBCQAyAOAlAQQg7gIhQgRQgKgBgJgDQjMgpg3gTIkdhrQj1hdgFAAQgiAAgqAuQgWAYg4BNIgtBAQgVAZgQAPQgkAkgiAIQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAABg");
	this.shape_7.setTransform(0.1,3.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("APQJlIt7n8Qjhhci0B6IqNFxQgIgFgQAAQgBgOATgjQAKgXAFgOIGAjZIEEiRQC0h7DhBcIEXCeICSBTIB3BGIFAC2QAFAMAMAIQAQAOAQAAIAAAoIgUAdgAsMibIAAgvQAkgfCMjNQB6ixAmABQA+gBCTBJQDIBgBIAZQCXA1EHBBQC5A4AAA9QAAARgDAGIAAADQgngBg4gIQglgPgygPQkHhBiXg2QhIgZjIhhQiThHg+AAQgmAAh6CxQhNBugsA9IgBACQgGADAAADg");
	this.shape_8.setTransform(0,-38.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-99.8,-99.6,199.7,199.3), null);


(lib.ch1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D2A25").s().p("AADOrQgahAgWg8IgDASIgEACIgHAUQhpA2hjAAQgkAAg5gWQhDgbg8gsQigh1gbirQAKAOATADQAHACAYAAQAfAABngbQBmgbAiAAQBqAABVBLQAeAaAWAhQgJgpgHgnQgUiDAli1QhlBfiBgvQjNhKBfjRQAPggASgeQgSAFgQABQgLABgmAAQheAAhRhNQhbhWAAiDQAAhtBJgoQBFgmCpgCQgggagdgfQg4g4gVhHQgUhHAhhNQAhhNBRgTQBRgUBDAUQBEATAtA2IANAPQAFhMAmg+QBChtCaAAQBQAAA5BHQAoAyAXBMQBYg/BxAfQCeAsgSCuQgPCYhpBXQgOAMgRAKQgVAOgTALIAHABIADABQAvAGB5A7QB5A6APBjQAOBkgvBIQgxBHhjAPQhjAPgpgIIgPgEIAAAnQAAB5hRA0QhBArh+AAQhpAAglhIIgBgDQgkCMAbClQAWCKAfBjIAGgSQAKiGBchRQBUhLBrAAQAiAABmAbQBmAaAeAAQAaAAAGgBQATgDAMgOQgcCqigB1Qg8AthCAaQg7AXgjAAQhjAAhog2QATAyAWAnQBIB5BLA6QgJAihLASIgMABQhGAAhQi7gACVA7IAAgDIgBAAIABADgAF9qZIACgGIAAgDIgCAJgAAjtCIABgBIAAgFIgBAGg");
	this.shape.setTransform(-1.1,2.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvQPRQmVmVAAo8QAAo7GVmVQGVmVI7AAQI8AAGVGVQGVGVgBI7QABI8mVGVQmVGVo8gBQo7ABmVmVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AwBQCQmompgBpZQABpYGompQGpmoJYgBQJZABGpGoQGoGpABJYQgBJZmoGpQmpGopZABQpYgBmpmog");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy2, new cjs.Rectangle(-145,-145,290.1,290.1), null);


(lib.ch1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D2A25").s().p("ApkQLIh1glQhngog/hYIgBgDIAAgBQgDgDgCAAQhviHASi5IA1hYQBHhHB8AiQgKgHgLgEIDWAAIAKAEQgZgWgOgUQggg2gKg8QgQhZBhiHIABgBIgLgDQBnhpBvhBIAAgVQANgIAZgDQgBAAgBAAQgBAAAAAAQgBgBAAAAQAAAAAAgBQibiWAAjSIAAgPQgoAZgwALQijgDgmieQgShRgHhSQgIi/C+AKQCgAHCLBIIAeAJIABAEQCLhuC7AAQCSAAByA9IALAKQAbAMAaAVIAPgHQCLhICigHQC9gKgIC/QgGBSgRBRQgoCeijADQglgKgfgPIAAAEQAADKiOCPQAuAgAnAdQAPAOARANQBDA5AuA5QAAABAAAAQAAABAAAAQABABAAAAQAAABABAAIAAACIACACQCcDUixCoIBuAAQgLAJgKAEQB6gfBGBDQAoAoAMA0QATC3hrCGIgKAHQhABZhlAnQgzAahEAOQhhANg9gnIABAFIhyg6IANAPIgxghIiRhLIk0AAIAAgFIgCAAIACAFIizBdIgrAeQABgFAHgGIhdAtIAAgDQgrAbg9AAQgZAAgdgFg");
	this.shape.setTransform(-4.3,1.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvQPRQmVmVAAo8QAAo7GVmVQGVmVI7AAQI8AAGVGVQGVGVgBI7QABI8mVGVQmVGVo8gBQo7ABmVmVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AwBQCQmompgBpZQABpYGompQGpmoJYgBQJZABGpGoQGoGpABJYQgBJZmoGpQmpGopZABQpYgBmpmog");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy, new cjs.Rectangle(-145,-145,290.1,290.1), null);


(lib.ch1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D2A25").s().p("AqHPkQh5AAgNhTIh6sPIgBgKIg7AiIgEAAQAAgTgCgHIgEgFQgIgEgRgBQABgOARgjQAMgXAEgOIGAjYIghm7QgVAZgQAOQgkAkgiAIQAAAAgBAAQAAABAAAAQgBAAAAAAQAAABAAAAIgEgCIgBACQgFADAAADIgxAAIAAgvQAmgfCLjMQB5ixAnAAQA+AACTBIQDJBgBGAZQCZA1EFBCQC6A3AAA+QAAAQgEAGIAAADQglgBg5gIQg7gHhRgRIgiF0ICSBTQASgcANgoQAMgfAFgnQAOhGAaAAQAQAAA1AOQA1AOAdAAQhNBPghCAIgJArIE/C1QAEAMAMAJQASANAQAAIAAApIgVAcIgCgDIgEAFQgKAPgIAFIkEiUQAAAKgCALIh5MPQgOBTh4AAg");
	this.shape.setTransform(2.3,-3.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvQPRQmVmVAAo8QAAo7GVmVQGVmVI7AAQI8AAGVGVQGVGVgBI7QABI8mVGVQmVGVo8gBQo7ABmVmVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AwBQCQmompgBpZQABpYGompQGpmoJYgBQJZABGpGoQGoGpABJYQgBJZmoGpQmpGopZABQpYgBmpmog");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1, new cjs.Rectangle(-145,-145,290.1,290.1), null);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1copy2();
	this.instance.parent = this;
	this.instance.setTransform(-154.9,0,0.912,0.912);

	this.instance_1 = new lib.ch1copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(155,0,0.912,0.912);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-287.2,-132.3,574.5,264.6);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("AgeCUQgKgEgKgEQg5gbgWg6QgVg6Abg4QAag6A7gVQA6gWA5AbQAoATAWAj");
	this.shape.setTransform(-22.6,-16.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AglDgQgWgGgVgKQhUgoghhYQgghXAohWQAohUBYggQAOgFAOgDQAxgMAwAMQAXAFAXALQBEAfAiA9");
	this.shape_1.setTransform(-22.9,-16.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhhjDQAJgOAKgNQASgWAYgSQANgLAQgKIAAAAQAHgHAJgFQAKgGAMgFQBNggBgAOQBiAOB9FGQhQChhPAUQhhAZgwAEQgSABgWAKQgVAJgzAmQgvAmggAjQgeAhgQgKQgwgjAChBQABg4AigwQAigyArgOQANgGAegIQAOgFAOgEQhkhQhlgqQgagLgYgPQgQgJgOgLQgegXgdgcQgWgXg1hCQg0hEAag/QAag9BQA5QBRA5A7A4QACADADACQAXAVAWAQQAKAHAKAGQADACADACQAcAQAcALQBBAWAMACAgHkbQBBgRBUBB");
	this.shape_2.setTransform(15.9,14.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AhyF8QgwgjAChBQABg4AigwQAigyArgOQANgGAegIIAcgJQhkhQhlgqQgagLgYgPQgQgJgOgLQgegXgdgcQgWgXg1hCQg0hEAag/QAag9BQA5QBRA5A7A4IAFAFQAXAVAWAQIAUANIAGAEQAcAQAcALQBBAWAMACQgMgChBgWQgcgLgcgQIgGgEIAFgJQAJgOAKgNQASgWAYgSQANgLAQgKIAAAAIAQgMIAWgLQBNggBgAOQBiAOB9FGQhQChhPAUQhhAZgwAEQgSABgWAKQgVAJgzAmQgvAmggAjQgXAZgOAAQgFAAgEgCgACOjrIgBgBIgDgCIgDgCIgBgBIgBgBIgBgBQg9grgzgBIAAAAIgBAAQgOAAgMAEQAMgEAOAAIABAAIAAAAQAzABA9ArIABABIABABIABABIADACIADACIABABIAAAAg");
	this.shape_3.setTransform(15.9,14.7);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-22.9,-17.5,0.708,0.708,0,0,0,0.4,0);
	this.instance.alpha = 0.852;
	this.instance.filters = [new cjs.BlurFilter(53, 53, 1)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.6,-79.9,145.8,134.4);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween19("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(189.6,39.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:1.06,scaleY:1.06},9).to({regX:0,scaleX:1,scaleY:1},10).to({regX:0.1,scaleX:1.06,scaleY:1.06},10).to({regX:0,scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,382.2,79.9);


(lib.Symbol4copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1();
	this.instance.parent = this;
	this.instance.setTransform(132.3,132.3,0.912,0.912);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy3, new cjs.Rectangle(0,0,264.6,264.6), null);


(lib.Symbol4copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1();
	this.instance.parent = this;
	this.instance.setTransform(132.3,132.3,0.912,0.912);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy2, new cjs.Rectangle(0,0,264.6,264.6), null);


(lib.Symbol2copy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(79.9,79.7,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy7, new cjs.Rectangle(0,0,159.8,159.4), null);


(lib.Symbol2copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(79.9,79.7,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy5, new cjs.Rectangle(0,0,159.8,159.4), null);


(lib.Symbol2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(79.9,79.7,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy3, new cjs.Rectangle(0,0,159.8,159.4), null);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(79.9,79.7,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy2, new cjs.Rectangle(0,0,159.8,159.4), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(271.2,90.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:150.9},7).to({y:90.9},7).to({y:150.9},6).to({y:90.9},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(235.9,39.6,70.8,102.5);


(lib.quecopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2copy2();
	this.instance.parent = this;
	this.instance.setTransform(-23.2,-34.3,1,1,0,0,0,79.9,79.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AsNMNQlDlDAAnKQAAnJFDlDQFElEHJAAQHKAAFDFEQFEFDAAHJQAAHKlEFDQlDFEnKAAQnJAAlElEg");
	this.shape.setTransform(-25,-31.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("AtdNeQllllABn5QgBn4FlllQFllkH4gBQH5ABFlFkQFlFlAAH4QAAH5llFlQllFkn5ABQn4gBlllkg");
	this.shape_1.setTransform(-25,-31.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,51,0.698)").s().p("As7M9QlYlYAAnlQAAnkFYlYQFXlXHkAAQHlAAFYFXQFXFYAAHkQAAHllXFYQlYFXnlAAQnkAAlXlXg");
	this.shape_2.setTransform(-25.1,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.quecopy2, new cjs.Rectangle(-146.9,-153.4,243.7,243.7), null);


(lib.quecopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2copy2();
	this.instance.parent = this;
	this.instance.setTransform(-23.2,-34.3,1,1,0,0,0,79.9,79.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AsNMNQlDlDAAnKQAAnJFDlDQFElEHJAAQHKAAFDFEQFEFDAAHJQAAHKlEFDQlDFEnKAAQnJAAlElEg");
	this.shape.setTransform(-25,-31.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("AtdNeQllllABn5QgBn4FlllQFllkH4gBQH5ABFlFkQFlFlAAH4QAAH5llFlQllFkn5ABQn4gBlllkg");
	this.shape_1.setTransform(-25,-31.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,51,0.698)").s().p("As7M9QlYlYAAnlQAAnkFYlYQFXlXHkAAQHlAAFYFXQFXFYAAHkQAAHllXFYQlYFXnlAAQnkAAlXlXg");
	this.shape_2.setTransform(-25.1,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.quecopy, new cjs.Rectangle(-146.9,-153.4,243.7,243.7), null);


(lib.que = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2copy2();
	this.instance.parent = this;
	this.instance.setTransform(-23.2,-34.3,1,1,0,0,0,79.9,79.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AsNMNQlDlDAAnKQAAnJFDlDQFElEHJAAQHKAAFDFEQFEFDAAHJQAAHKlEFDQlDFEnKAAQnJAAlElEg");
	this.shape.setTransform(-25,-31.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("AtdNeQllllABn5QgBn4FlllQFllkH4gBQH5ABFlFkQFlFlAAH4QAAH5llFlQllFkn5ABQn4gBlllkg");
	this.shape_1.setTransform(-25,-31.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,51,0.698)").s().p("As7M9QlYlYAAnlQAAnkFYlYQFXlXHkAAQHlAAFYFXQFXFYAAHkQAAHllXFYQlYFXnlAAQnkAAlXlXg");
	this.shape_2.setTransform(-25.1,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.que, new cjs.Rectangle(-146.9,-153.4,243.7,243.7), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;
	this.instance.alpha = 0.398;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},6).to({scaleX:1.1,scaleY:1.1},8).to({scaleX:1,scaleY:1},9).to({startPosition:0},26).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.6,-79.9,145.8,134.4);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4copy3();
	this.instance.parent = this;
	this.instance.setTransform(-309.8,0,1,1,0,0,0,132.3,132.3);

	this.instance_1 = new lib.ch1copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.912,0.912);

	this.instance_2 = new lib.ch1copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(309.9,0,0.912,0.912);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-442.1,-132.3,884.4,264.6);


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4copy3();
	this.instance.parent = this;
	this.instance.setTransform(-309.8,0,1,1,0,0,0,132.3,132.3);

	this.instance_1 = new lib.ch1copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,0.912,0.912);

	this.instance_2 = new lib.ch1copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(309.9,0,0.912,0.912);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-442.1,-132.3,884.4,264.6);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(326.2,581.4,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(287).to({_off:false},0).wait(65).to({startPosition:0},0).to({alpha:0,startPosition:5},5).to({_off:true},1).wait(65));

	// HAND
	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(414.2,649.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(237).to({_off:false},0).to({_off:true},32).wait(154));

	// ARROW
	this.instance_2 = new lib.Symbol1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(321.7,406.6,1,1,0,0,0,271.2,90.9);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(205).to({_off:false},0).to({_off:true},32).wait(186));

	// Layer_4
	this.instance_3 = new lib.Symbol2copy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(642.2,282.1,1.147,1.147,0,0,0,79.9,79.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(269).to({_off:false},0).to({x:329.1,y:572.2},18).to({regX:80,scaleX:1.26,scaleY:1.26,x:329.2},19).wait(46).to({regX:79.9,scaleX:1.15,scaleY:1.15,x:329.1},0).to({alpha:0.02},5).to({_off:true},1).wait(65));

	// Layer_7
	this.instance_4 = new lib.Symbol2copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(642,281.9,1.133,1.133,0,0,0,80.2,79.9);
	this.instance_4._off = true;

	this.instance_5 = new lib.Symbol2copy7();
	this.instance_5.parent = this;
	this.instance_5.setTransform(946.1,576.1,1.133,1.133,0,0,0,80.2,79.9);
	this.instance_5.alpha = 0.5;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(133).to({_off:false},0).to({_off:true,x:946.1,y:576.1,alpha:0.5},20).wait(270));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(133).to({_off:false},20).wait(45).to({alpha:0},5).to({_off:true},1).wait(219));

	// Layer_8
	this.instance_6 = new lib.Symbol2copy2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(642,281.9,1.133,1.133,0,0,0,80.2,79.9);
	this.instance_6._off = true;

	this.instance_7 = new lib.Symbol2copy5();
	this.instance_7.parent = this;
	this.instance_7.setTransform(636,576,1.133,1.133,0,0,0,80.2,79.9);
	this.instance_7.alpha = 0.5;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(133).to({_off:false},0).to({_off:true,x:636,y:576,alpha:0.5},20).wait(270));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(133).to({_off:false},20).wait(45).to({alpha:0},5).to({_off:true},1).wait(219));

	// Layer_6
	this.instance_8 = new lib.Symbol2copy2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(642,281.9,1.133,1.133,0,0,0,80.2,79.9);
	this.instance_8._off = true;

	this.instance_9 = new lib.Symbol2copy3();
	this.instance_9.parent = this;
	this.instance_9.setTransform(329.9,571.9,1.156,1.156,0,0,0,80.2,79.9);
	this.instance_9.alpha = 0.5;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(133).to({_off:false},0).to({_off:true,scaleX:1.16,scaleY:1.16,x:329.9,y:571.9,alpha:0.5},20).wait(270));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(133).to({_off:false},20).wait(45).to({alpha:0},5).to({_off:true},1).wait(219));

	// Layer_5
	this.instance_10 = new lib.Symbol5("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(766.7,104.9,1,1,0,0,0,189.6,39.9);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(88).to({_off:false},0).wait(110).to({startPosition:28},0).to({alpha:0,startPosition:33},5).to({_off:true},1).wait(219));

	// Layer_10
	this.instance_11 = new lib.Symbol4copy2();
	this.instance_11.parent = this;
	this.instance_11.setTransform(327.5,575.6,1,1,0,0,0,132.3,132.3);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(287).to({_off:false},0).to({scaleX:1.1,scaleY:1.1},19).wait(46).to({alpha:0},5).to({_off:true},1).wait(65));

	// Layer_3
	this.instance_12 = new lib.Tween21("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(637.3,575.6);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_13 = new lib.Tween22("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(637.3,575.6);

	this.instance_14 = new lib.ch1copy2();
	this.instance_14.parent = this;
	this.instance_14.setTransform(637.3,575.6,0.912,0.912);

	this.instance_15 = new lib.ch1copy();
	this.instance_15.parent = this;
	this.instance_15.setTransform(947.2,575.6,0.912,0.912);

	this.instance_16 = new lib.Tween20("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(792.3,575.6);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},73).to({state:[{t:this.instance_13}]},7).to({state:[{t:this.instance_15},{t:this.instance_14}]},226).to({state:[{t:this.instance_16}]},46).to({state:[{t:this.instance_16}]},5).to({state:[]},1).wait(65));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(73).to({_off:false},0).to({_off:true,alpha:1},7).wait(343));
	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(352).to({_off:false},0).to({alpha:0},5).to({_off:true},1).wait(65));

	// Layer_2
	this.instance_17 = new lib.que();
	this.instance_17.parent = this;
	this.instance_17.setTransform(667.8,321.9,1.14,1.14,0,0,0,0.1,0);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.instance_18 = new lib.quecopy();
	this.instance_18.parent = this;
	this.instance_18.setTransform(667.8,321.9,1.14,1.14,0,0,0,0.1,0);

	this.instance_19 = new lib.quecopy2();
	this.instance_19.parent = this;
	this.instance_19.setTransform(667.8,321.9,1.14,1.14,0,0,0,0.1,0);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_17}]},43).to({state:[{t:this.instance_18}]},7).to({state:[{t:this.instance_19}]},38).to({state:[{t:this.instance_19}]},9).to({state:[{t:this.instance_19}]},9).to({state:[{t:this.instance_19}]},11).to({state:[{t:this.instance_19}]},11).to({state:[{t:this.instance_19}]},10).to({state:[{t:this.instance_19}]},9).to({state:[{t:this.instance_19}]},11).to({state:[{t:this.instance_19}]},11).to({state:[{t:this.instance_19}]},10).to({state:[{t:this.instance_19}]},10).to({state:[{t:this.instance_19}]},7).to({state:[{t:this.instance_19}]},8).to({state:[{t:this.instance_17}]},148).to({state:[{t:this.instance_17}]},5).to({state:[]},1).wait(65));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(43).to({_off:false},0).to({_off:true,alpha:1},7).wait(154).to({_off:false},148).to({alpha:0.02},5).to({_off:true},1).wait(65));
	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(88).to({_off:false},0).to({scaleX:1.17,scaleY:1.17,x:667.7},9).to({scaleX:1.14,scaleY:1.14,x:667.8},9).to({scaleX:1.17,scaleY:1.17,x:667.7},11).to({scaleX:1.14,scaleY:1.14,x:667.8},11).to({scaleX:1.17,scaleY:1.17,x:667.7},10).to({scaleX:1.14,scaleY:1.14,x:667.8},9).to({scaleX:1.17,scaleY:1.17,x:667.7},11).to({scaleX:1.14,scaleY:1.14,x:667.8},11).to({scaleX:1.17,scaleY:1.17,x:667.7},10).to({scaleX:1.14,scaleY:1.14,x:667.8},10).to({scaleX:1.17,scaleY:1.17,x:667.7},7).to({scaleX:1.14,scaleY:1.14,x:667.8},8).to({_off:true},148).wait(71));

	// Layer_1
	this.questiontxt = new lib.question_mc();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(640.1,107.1,1.966,1.966,0,0,0,0,0.1);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(20).to({_off:false},0).to({alpha:1},7).wait(325).to({alpha:0.02},5).to({_off:true},1).wait(65));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(566.9,295.7,1430,864);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#333333",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;