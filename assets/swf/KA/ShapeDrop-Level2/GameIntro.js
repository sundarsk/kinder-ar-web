(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0000FF").s().p("AgyCeQgWgJgQgRQgRgQgJgZQgJgXAAgeQAAgeAKgWQAKgZARgQQARgPAWgJQAVgJAWAAQAOABAPADQANAEAPAGQAPAIANANIAChxIApADIAAFFIguAAIABgcIgOALIgOAIIgOAHIgMADQgOADgNABQgaAAgWgJgAghgmQgOAHgKALQgKAMgGAPQgFAQAAATQAAASAGAQQAFAPAKAMQAKAKAPAHQAOAHAQgBQANABANgFQANgEALgJQALgIAIgLQAIgLAFgOIAAgvQgEgOgIgLQgJgMgLgJQgLgHgOgFQgNgFgNAAQgRAAgNAHg");
	this.shape.setTransform(172.9,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0000FF").s().p("AgxB3QgXgJgPgQQgQgRgIgWQgIgXAAgcQAAgbAKgXQAJgZAQgSQARgRAWgLQAWgKAZABQAXgBAUAKQAUAJAQAPQAQAQAKAWQAKAVADAZIi/AbQACAQAGANQAGAMAKAJQAJAIANAFQANAFAOAAQALAAAMgEQALgEAKgGQAKgIAHgKQAHgKAEgOIAqAIQgGAVgLAQQgLAQgPAMQgQAMgRAHQgTAFgTAAQgdAAgWgIgAgShVQgLADgKAIQgLAHgIANQgIANgEAUICFgQIgCgFQgIgWgPgMQgOgNgWAAQgJABgLADg");
	this.shape_1.setTransform(145.6,5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0000FF").s().p("Ah5izIAxgCIgBAgQANgMANgHQAOgHAMgDQANgEAMgBQAPAAAPAEQAPAEANAIQANAIAMALQALALAIAOQAIANAEAQQAFAQgBARQgBATgFARQgFARgIANQgJAOgLALQgLAMgMAHQgNAIgNAEQgOAEgNAAQgMAAgOgEQgMgDgPgGQgPgHgOgMIgBCWIgpABgAAAiKQgNAAgMAFQgMAEgKAIQgKAIgGALQgHAKgEALIAAAqQADAPAHALQAHALAKAHQAKAHALAEQALAEAMAAQAOAAAOgGQAOgGAKgJQAKgLAGgOQAGgOABgRQABgQgFgPQgFgPgKgLQgKgLgNgHQgNgGgPAAIgCAAg");
	this.shape_2.setTransform(118.1,11.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0000FF").s().p("Ah5izIAxgCIgBAgQANgMANgHQAOgHAMgDQANgEAMgBQAPAAAPAEQAPAEANAIQANAIAMALQALALAIAOQAIANAEAQQAFAQgBARQgBATgFARQgFARgIANQgJAOgLALQgLAMgMAHQgNAIgNAEQgOAEgNAAQgMAAgOgEQgMgDgPgGQgPgHgOgMIgBCWIgpABgAAAiKQgNAAgMAFQgMAEgKAIQgKAIgGALQgHAKgEALIAAAqQADAPAHALQAHALAKAHQAKAHALAEQALAEAMAAQAOAAAOgGQAOgGAKgJQAKgLAGgOQAGgOABgRQABgQgFgPQgFgPgKgLQgKgLgNgHQgNgGgPAAIgCAAg");
	this.shape_3.setTransform(88.7,11.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0000FF").s().p("AguB5QgVgJgQgRQgQgSgKgYQgIgYgBgdQABgcAIgZQAKgXAQgRQAQgRAVgKQAWgJAYAAQAYAAAWAKQAVAJARASQAQARAJAZQAKAXAAAbQAAAcgKAXQgJAZgQARQgRASgVAKQgWAJgYAAQgYAAgWgJgAgghSQgOAIgIAOQgJAMgDARQgEAPgBAQQAAAQAFAQQAEAQAJANQAJANAOAIQANAIARAAQATAAANgIQAOgIAIgNQAKgNAEgQQADgQAAgQQABgQgEgPQgEgRgJgMQgJgOgNgIQgOgIgTAAQgSAAgOAIg");
	this.shape_4.setTransform(59.8,5.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0000FF").s().p("AhjA3IgCg3IgCgrIgBggIgBgvIAzgBIAAAcIAAAaIAAAbQAGgLAKgMQAJgMAMgLQALgLANgIQAOgIAQgCQARgBAPAJQAGADAHAGQAFAGAFAJQAGAJADANQAEANABARIgrARQgBgLgBgIIgFgOIgFgKIgHgGQgHgFgKAAQgHABgHAEQgHAEgGAHQgHAHgIAJIgNARIgOASIgLAPIABAhIAAAeIABAZIABASIgvAGIgChFg");
	this.shape_5.setTransform(35.1,5.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0000FF").s().p("AgyCeQgWgJgQgRQgRgQgJgZQgJgXAAgeQAAgeAKgWQAKgZARgQQARgPAWgJQAVgJAWAAQAOABAPADQANAEAPAGQAPAIANANIAChxIApADIAAFFIguAAIABgcIgOALIgOAIIgOAHIgMADQgOADgNABQgaAAgWgJgAghgmQgOAHgKALQgKAMgGAPQgFAQAAATQAAASAGAQQAFAPAKAMQAKAKAPAHQAOAHAQgBQANABANgFQANgEALgJQALgIAIgLQAIgLAFgOIAAgvQgEgOgIgLQgJgMgLgJQgLgHgOgFQgNgFgNAAQgRAAgNAHg");
	this.shape_6.setTransform(5.6,1.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0000FF").s().p("AggB+IgSgFIgUgHQgLgEgKgFIgTgNIAVgiIAZAMIAaAMIAbAGQANADAPAAQAMAAAJgCQAIgCAGgEQAFgEADgEQADgEABgEIABgIQAAgEgCgFIgGgJQgFgEgHgDQgHgEgKgCQgKgCgOgBQgTgBgTgCQgSgEgPgHQgOgHgKgKQgJgLgCgRQgCgQAEgNQAEgNAHgKQAIgLAKgIQALgIANgFQANgFAOgCQAPgEANAAIAVABIAYAEQANADANAFQANAEALAJIgPApQgPgIgNgFIgXgHIgWgDQgigCgTAKQgUAJAAATQAAANAHAGQAIAGAMACQANADARABQAQABATADQAVAEAPAGQAPAFAJAKQAJAIAEALQAEALAAALQAAAWgIAOQgJAOgOAJQgPAJgSAEQgSAFgUABQgUgBgVgDg");
	this.shape_7.setTransform(-33.2,6.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0000FF").s().p("AgxB3QgXgJgPgQQgQgRgIgWQgIgXAAgcQAAgbAKgXQAJgZAQgSQARgRAWgLQAWgKAZABQAXgBAUAKQAUAJAQAPQAQAQAKAWQAKAVADAZIi/AbQACAQAGANQAGAMAKAJQAJAIANAFQANAFAOAAQALAAAMgEQALgEAKgGQAKgIAHgKQAHgKAEgOIAqAIQgGAVgLAQQgLAQgPAMQgQAMgRAHQgTAFgTAAQgdAAgWgIgAgShVQgLADgKAIQgLAHgIANQgIANgEAUICFgQIgCgFQgIgWgPgMQgOgNgWAAQgJABgLADg");
	this.shape_8.setTransform(-58.8,5.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0000FF").s().p("Ah5izIAxgCIgBAgQANgMANgHQAOgHAMgDQANgEAMgBQAPAAAPAEQAPAEANAIQANAIAMALQALALAIAOQAIANAEAQQAFAQgBARQgBATgFARQgFARgIANQgJAOgLALQgLAMgMAHQgNAIgNAEQgOAEgNAAQgMAAgOgEQgMgDgPgGQgPgHgOgMIgBCWIgpABgAAAiKQgNAAgMAFQgMAEgKAIQgKAIgGALQgHAKgEALIAAAqQADAPAHALQAHALAKAHQAKAHALAEQALAEAMAAQAOAAAOgGQAOgGAKgJQAKgLAGgOQAGgOABgRQABgQgFgPQgFgPgKgLQgKgLgNgHQgNgGgPAAIgCAAg");
	this.shape_9.setTransform(-86.3,11.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0000FF").s().p("ABFB+IACgfQgaARgaAIQgZAIgXAAQgQAAgPgEQgPgEgLgJQgLgIgIgOQgGgNAAgSQgBgSAGgOQAFgOAKgLQAIgKANgIQANgHAOgFQAPgFAQgCQAPgDAPAAIAcABIAWADQgCgLgFgKQgEgLgGgJQgHgIgJgGQgKgFgOAAQgIAAgKADQgMACgNAHQgNAGgQAMQgQAMgRASIgbghQAVgUATgMQAUgNARgHQARgHAQgDQANgDAMAAQAUAAAQAIQAQAGAMANQAMAMAIAQQAJAQAFATQAFASADAVQACATAAAUQAAAVgDAYQgCAZgFAdgAgKgBQgSAEgOAJQgNAJgIANQgGAMAEARQADANAJAFQAJAGAMAAQANAAAQgEQANgFAPgGQAOgGAOgIIAWgMIAAgWIgBgXQgKgCgMgCQgMgCgNAAQgTAAgSAEg");
	this.shape_10.setTransform(-116.5,5.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0000FF").s().p("AA7CpQAHgWADgUIAEgkIAAgkQgBgXgHgQQgHgQgKgKQgLgMgMgEQgMgFgMAAQgMACgNAFQgLAGgNALQgMAKgLAVIgBCSIgsABIgElUIAzgCIgBCHQAMgNAOgIQAOgIAMgEQANgFANgBQAZAAAUAJQAUAJAPAQQAPARAIAYQAJAXABAeIAAAfIgCAhIgDAdQgCAPgEALg");
	this.shape_11.setTransform(-144.6,0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0000FF").s().p("AggB+IgSgFIgUgHQgLgEgKgFIgTgNIAVgiIAZAMIAaAMIAbAGQANADAPAAQAMAAAJgCQAIgCAGgEQAFgEADgEQADgEABgEIABgIQAAgEgCgFIgGgJQgFgEgHgDQgHgEgKgCQgKgCgOgBQgTgBgTgCQgSgEgPgHQgOgHgKgKQgJgLgCgRQgCgQAEgNQAEgNAHgKQAIgLAKgIQALgIANgFQANgFAOgCQAPgEANAAIAVABIAYAEQANADANAFQANAEALAJIgPApQgPgIgNgFIgXgHIgWgDQgigCgTAKQgUAJAAATQAAANAHAGQAIAGAMACQANADARABQAQABATADQAVAEAPAGQAPAFAJAKQAJAIAEALQAEALAAALQAAAWgIAOQgJAOgOAJQgPAJgSAEQgSAFgUABQgUgBgVgDg");
	this.shape_12.setTransform(-172.6,6.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(3,1,1).p("A7MkcMA2ZAAAQBrAABMBMQBNBNAABrIAAAxQAABshNBMQhMBMhrAAMg2ZAAAQhrAAhNhMQhMhMAAhsIAAgxQAAhrBMhNQBNhMBrAAg");
	this.shape_13.setTransform(6,3.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("A7LEdQhsAAhNhNQhLhLAAhsIAAgxQAAhrBLhNQBNhMBsAAMA2YAAAQBrAABMBMQBMBNAABrIAAAxQAABshMBLQhMBNhrAAg");
	this.shape_14.setTransform(6,3.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-206,-35.5,413.5,71.1);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.1,1,1).p("AAAuoIQ7dRMgh1AAAg");
	this.shape.setTransform(0,0.1,0.712,0.712);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6600CC").s().p("Aw6OpIQ69RIQ7dRg");
	this.shape_1.setTransform(0,0.1,0.712,0.712);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-78.1,-67.6,156.2,135.5);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFCC00").ss(5,1,1).p("AREAAQAAHFlAFAQk/E/nFAAQnEAAlAk/Qk/lAAAnFQAAnEE/lAQFAk/HEAAQHFAAE/E/QFAFAAAHEg");
	this.shape.setTransform(0,13.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AsEMEQk/k/AAnFQAAnEE/k/QFAlAHEAAQHFAAE/FAQFAE/AAHEQAAHFlAE/Qk/FAnFAAQnEAAlAlAg");
	this.shape_1.setTransform(0,13.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.7,-98.2,223.5,223.4);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgpEjQgLgFgIgIQgIgIgFgLQgFgLAAgNQAAgMAFgLQAFgMAIgIQAIgHALgFQAMgFAMAAQAMAAALAFQALAFAIAHQAIAIAEAMQAGALAAAMQAAANgGALQgEALgIAIQgIAIgLAFQgLAEgMAAQgMAAgMgEgAgvBWIgHhBIAAgEIAAgDQAAgRAIgNQAJgNANgLQANgMAOgLQAQgKANgNQANgNAJgQQAJgQgBgVQAAgPgFgLQgGgLgIgJQgLgIgNgEQgOgFgOAAQgXAAgPAFQgRAFgKAHIgSALQgIAFgHAAQgOAAgGgMIgagpQANgNAQgKQARgLASgIQAUgIAWgFQAWgEAZAAQAiAAAcAJQAdAKAUASQAVARALAaQALAZAAAfQAAAegJAXQgIAWgOAQQgNARgPAMIgfAXQgOAKgJAJQgLAKgCANIgJA7g");
	this.shape.setTransform(0.1,38);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.9,-14,37.8,100);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(2,1,1).p("Ap2pQQgYAaADAmQAEAnAeAcIRbQaQAeAcAnABQAnACAZgaQAYgbgDgnQgEgmgegcIxbwaQgegcgngCQgngBgZAbg");
	this.shape.setTransform(162.2,3.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCCCC").s().p("AIrI+QgQgCgMgLIgBgBIxcwaQgNgMgCgRIAAgCQgDgWAOgOIABgCQAOgPAUABQAFAAAFABQANADAKAKIABAAIRcQaQAPAOAAATQACATgNAPIgCACQgJAMgTABIgGABIgEAAg");
	this.shape_1.setTransform(162.2,3.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999999").s().p("AI3JqQgngBgegcIxbwaQgegcgEgnQgDgmAYgaQAZgbAnABQAnACAeAcIRbQaQAeAcAEAmQADAngYAbQgXAZglAAIgEgBgApRovIgBACQgOAOADAWIAAACQACARANAMIRcQaIABABQAMALAQACQAGAAAEgBQATgBAJgMIACgCQANgPgCgTQAAgTgPgOIxcwaIgBAAQgKgKgNgDQgFgBgFAAIgCAAQgSAAgOAOg");
	this.shape_2.setTransform(162.2,3.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(2,1,1).p("Ah/AAQAAA1AlAmQAmAlA0AAQA1AAAmglQAlgmAAg1QAAg0glgmQgmglg1AAQg0AAgmAlQglAmAAA0gAjHAAQAABTA7A7QA6A6BSAAQBTAAA7g6QA6g7AAhTQAAhSg6g7Qg7g6hTAAQhSAAg6A6Qg7A7AABSg");
	this.shape_3.setTransform(104.9,-51);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AhaBbQglgmAAg1QAAg1AlglQAmglA0AAQA1AAAmAlQAlAlAAA1QAAA1glAmQgmAlg1AAQg0AAgmglg");
	this.shape_4.setTransform(104.8,-51);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQglAmAAA0QAAA1AlAmQAmAmA0AAQA1AAAmgmQAkgmABg1QgBg0gkgmQgmglg1AAQg0AAgmAlg");
	this.shape_5.setTransform(104.9,-51);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#330000").ss(2,1,1).p("Ah/AAQAAA1AlAmQAmAlA0AAQA1AAAmglQAlgmAAg1QAAg0glgmQgmglg1AAQg0AAgmAlQglAmAAA0gAjHAAQAABTA7A7QA6A6BSAAQBTAAA7g6QA6g7AAhTQAAhSg6g7Qg7g6hTAAQhSAAg6A6Qg7A7AABSg");
	this.shape_6.setTransform(104.9,-51);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQglAmAAA0QAAA1AlAmQAmAmA0AAQA1AAAmgmQAkgmABg1QgBg0gkgmQgmglg1AAQg0AAgmAlg");
	this.shape_7.setTransform(104.9,-51);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#666666").ss(2,1,1).p("AJ3pQQAYAagDAmQgEAngeAcIxbQaQgeAcgnABQgnACgZgaQgYgbADgnQAEgmAegcIRbwaQAegcAngCQAngBAZAbg");
	this.shape_8.setTransform(-161.3,3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CCCCCC").s().p("Ao1I9QgSgBgKgMIgBgBQgNgPACgUQAAgSAPgOIRcwaIAAgBQALgKANgCQAFgCAFAAQAUAAAOAPIABABQAOAOgDAWIAAACQgCARgNANIxcQaIgBAAQgMALgQACIgEAAIgHgBg");
	this.shape_9.setTransform(-161.3,3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#999999").s().p("Ap2JSQgYgbADgnQAEgmAegcIRbwaQAegcAngCQAngBAZAbQAYAagDAmQgEAngeAcIxbQaQgeAcgnABIgEABQglAAgXgZgAIwo9QgFAAgFABQgNADgKAKIgBAAIxcQaQgPAPAAASQgCAUANAOIABACQAKAMASABQAFABAGAAQAQgCAMgLIABgBIRcwaQANgMACgRIAAgCQADgWgOgOIgBgCQgOgOgTAAIgBAAg");
	this.shape_10.setTransform(-161.3,3.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#330000").ss(2,1,1).p("ACAAAQAAA1glAmQglAlg2AAQg0AAglglQgmgmAAg1QAAg0AmgmQAlglA0AAQA2AAAlAlQAlAmAAA0gADIAAQAABTg6A7Qg7A6hTAAQhSAAg6g6Qg7g7AAhTQAAhSA7g7QA6g6BSAAQBTAAA7A6QA6A7AABSg");
	this.shape_11.setTransform(-103.9,-51);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AhaBbQglgmAAg1QAAg1AlglQAmglA0AAQA1AAAmAlQAlAlAAA1QAAA1glAmQgmAlg1AAQg0AAgmglg");
	this.shape_12.setTransform(-103.9,-51);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQgkAmgBA0QABA1AkAmQAmAmA1AAQA0AAAmgmQAlgmAAg1QAAg0glgmQgmglg0AAQg1AAgmAlg");
	this.shape_13.setTransform(-103.9,-51);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#330000").ss(2,1,1).p("ACAAAQAAA1glAmQglAlg2AAQg0AAglglQgmgmAAg1QAAg0AmgmQAlglA0AAQA2AAAlAlQAlAmAAA0gADIAAQAABTg6A7Qg7A6hTAAQhSAAg6g6Qg7g7AAhTQAAhSA7g7QA6g6BSAAQBTAAA7A6QA6A7AABSg");
	this.shape_14.setTransform(-103.9,-51);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#940907").s().p("AiMCNQg7g6AAhTQAAhSA7g6QA6g7BSAAQBTAAA6A7QA7A6AABSQAABTg7A6Qg6A7hTAAQhSAAg6g7gAhahaQgkAmgBA0QABA1AkAmQAmAmA1AAQA0AAAmgmQAlgmAAg1QAAg0glgmQgmglg0AAQg1AAgmAlg");
	this.shape_15.setTransform(-103.9,-51);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#660000").ss(2,1,1).p("EAgCgJ/IABAAQBGgaBBg7QAUgTAVgXQAOgQANgRQA5hKAhhVQAphsgRhOQgThVhXgUMhGrAAAQhWAUgTBVQgRBOAoBsQAhBVA5BKQANARAPAQQAUAXAVATQBAA7BGAaIABAAMBADAAAIjEX0QgfEkkmBJMgvxAAAQklhJggkkIjE30EAgDgKJIgBAKATGmJQBLAAAyA1QAxA1gFBLIhIOlQgFBDg2AuQg1AvhGAAQhHAAgvgvQgZgZgLgeQgJgbACgfIA1ulQAEhLA4g1QApgnAzgKQAUgEAVAAgAJ2mJQBMAAAzA1QAzA1gDBLIgpOlQgDBDg1AuQgzAvhHAAQhGAAgxgvQgqgogEg4QgBgIAAgJIAXulQAChLA2g1QAvguA+gGQALgBALAAgAABmJQBMAAA1A1QA1A1gBBLIgJOlQgBBDgyAuQgyAvhHAAQhGAAgygvQgygugBhDIgJulQgBhLA1g1QA1g1BLAAgEggCgKJIABAKApumJQALAAALABQA+AGAvAuQA2A1ACBLIAXOlQAAAIAAAIQgEA4grApQgxAvhGAAQhHAAg0gvQgzgugDhDIgqulQgDhLAzg1QAzg1BMAAgAy9mJQAUAAAUAEQAzAKApAnQA4A1AFBLIA1OlQABAfgJAbQgLAegZAZQgvAvhGAAQhHAAg1gvQg2gugFhDIhHulQgGhLAxg1QAyg1BMAAg");
	this.shape_16.setTransform(-0.1,50);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#B60B09").s().p("A3xPoQj+hCgcj+IgBgBIi828MA+RAAAIi9W8IAAABQgcD+j+BCgA7BKdIAAACQAXDHDCA3MAvQAAAQDDg3AXjHIAAgCICy1nMg7nAAAgEgiJgOcQgUgTgUgXIgcghMBGbAAAIgcAhQgUAXgUATg");
	this.shape_17.setTransform(0,70);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#940907").s().p("A34PcQklhKggkkIjE3zMBADAAAIjEXzQgfEkkmBKgA8MJoIABABQAcD+D+BCMAvjAAAQD+hCAcj+IAAgBIC928Mg+RAAAgAP7I8QgZgZgLgeQAnAcA1AAQBDAAAygsQAygsAEg/IA7txQAEhFgwgyQgigkgugKQAUgEAVAAQBLAAAyA1QAxA2gGBKIhHOlQgFBDg2AvQg1AuhHAAQhGAAgvgugAHTI8QgqgpgEg3QAIANANAMQAuAsBDAAQBDAAAwgsQAxgsACg/IAitxQAChFgvgyQgrgrg7gGIAWgBQBMAAAzA1QAzA2gDBKIgpOlQgDBDg1AvQgzAuhHAAQhGAAgxgugAh3I8QgygvgBhDIgJulQgBhKA1g2QA1g1BLAAQBMAAA0A1QA2A2gBBKIgJOlQgBBDgzAvQgxAuhHAAQhGAAgygugAh3peQgzAyACBFIAHNxQABA/AvAsQAwAsBCAAQBDAAAvgsQAwgsAAg/IAItxQABhFgzgyQgxgxhIgBQhFABgyAxgAq9I8Qg0gvgChDIgqulQgDhKAzg2QAzg1BMAAIAVABQg7AGgqArQgwAyADBFIAhNxQACA/AxAsQAwAsBDAAQBDAAAvgsQAMgNAJgNQgDA4grApQgxAuhGAAQhHAAg0gugAzkI8Qg2gvgFhDIhIulQgFhKAxg2QAyg1BLAAQAVAAAUAEQgvAKgjAkQguAyAFBFIA5NxQAEA/AyAsQAyAsBDAAQA2gBAngcQgLAfgZAZQgvAuhHAAQhGAAg1gugEAgCgOFIABgKIgBAKMhADAAAIgBgKIABAKIgBAAQhGgahBg8MBETAAAQhBA8hGAagEggBgOFg");
	this.shape_18.setTransform(0,76.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#D70503").s().p("A3oSjQjCg3gXjGIAAgCIiy1nMA7nAAAIiyVnIAAACQgXDGjDA3gASdlHQgzALgpAmQg4A2gEBKIg1OlQgCAgAJAbQALAeAZAZQAvAuBHAAQBGAAA1guQA2gvAFhDIBIulQAFhKgxg2Qgyg1hLAAQgVAAgUAEgAJglKQg+AHgvAtQg2A2gCBKIgXOlIABASQAEA3AqApQAxAuBGAAQBHAAAzguQA1gvADhDIApulQADhKgzg2Qgzg1hMAAIgWABgAh/kWQg1A2ABBKIAJOlQABBDAyAvQAyAuBGAAQBHAAAyguQAygvABhDIAJulQABhKg1g2Qg1g1hMAAIAAAAQhLAAg1A1gArtkWQgzA2ADBKIAqOlQADBDAzAvQA0AuBHAAQBGAAAxguQArgpAEg4IAAgRIgXulQgChKg2g2Qgvgtg+gHIgWgBIAAAAQhMAAgzA1gA07kWQgxA2AGBKIBHOlQAFBDA2AvQA1AuBHAAQBGAAAvguQAZgZALgfQAJgagBggIg1ulQgFhKg4g2QgpgmgzgLQgUgEgUAAIgBAAQhLAAgyA1gEgjNgLhQg5hJghhWQgohrARhOQAThVBWgVMBGrAAAQBXAVATBVQARBOgpBrQghBWg5BJg");
	this.shape_19.setTransform(-0.1,43.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-238.3,-76,476.5,252), null);


(lib.InstructionText_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbBsIgQgDIgSgGIgSgIIgQgMIATgdIAVALQALAFAMAEIAXAHQAMACAMAAQALAAAHgCQAHgCAFgDQAFgDACgEIAEgHIABgHQAAgDgCgFQgCgEgDgDQgFgEgFgDQgHgDgIgCIgVgCQgRgBgQgDQgQgCgMgHQgNgFgIgJQgJgKgBgPQgBgNADgMQADgLAHgJQAFgKAKgGQAKgHALgEQALgFAMgCQAMgDAMABIASAAQAKABALACQAMACALAFQALAFAKAGIgNAjQgNgGgMgEIgTgGIgUgDQgegCgQAJQgRAHAAARQAAALAGAGQAHAFAKACIAaAEIAfADQATADAMAGQANAEAIAIQAIAHAEAKQACAJAAAKQABATgIAMQgHANgMAHQgNAIgQAEQgQADgSACQgQgBgSgEg");
	this.shape.setTransform(275.7,42.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgrCKQgTgIgOgPQgOgPgJgUQgIgVAAgaQAAgaAJgTQAJgVAPgOQAPgOASgHQATgIASAAQANAAANADQALAEANAFQANAHALALIAChiIAkADIgBEbIgnAAIAAgZIgLAJIgNAHIgMAGIgKADQgNADgLAAQgWAAgTgHgAgdghQgLAGgJAKQgJALgFAMQgEAOAAAQQAAAQAEAOQAFANAJAKQAJAKAMAGQAMAFAOAAQALAAAMgEQALgEAKgHQAJgHAHgJQAHgLAEgLIAAgpQgDgNgIgIQgHgLgKgHQgJgHgMgFQgMgDgLAAQgOAAgMAFg");
	this.shape_1.setTransform(251.1,37.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAMCoQgKgFgHgHQgIgHgGgJQgGgJgDgJQgKgWgCgcIAFjzIAnAAIgBA/IgCA0IgBApIAAAgIgBA1QAAARAEAOIAFAMQADAGAFAFQAEAEAHADQAGACAIABIgFAmQgNAAgLgEg");
	this.shape_2.setTransform(234.5,34.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgoBpQgSgIgOgPQgOgPgIgUQgIgWAAgYQAAgZAIgVQAIgVAOgPQAOgOASgJQATgIAVAAQAVAAATAJQATAIAOAQQAOAOAIAWQAIAUAAAYQAAAYgIAUQgIAVgOAPQgOAPgTAIQgTAJgVAAQgVAAgTgIgAgchHQgLAIgIALQgHALgEAOQgDAOAAANQAAAOAEAOQADAOAIALQAIALALAIQAMAGAPAAQAQAAAMgGQALgIAJgLQAHgLADgOQAEgOAAgOQAAgNgDgOQgEgOgHgLQgHgLgMgIQgMgHgRAAQgQAAgMAHg");
	this.shape_3.setTransform(216,41.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AA0CSQAFgSADgSIADgfIAAgfQAAgUgHgPQgGgNgJgJQgJgJgKgFQgLgEgLAAQgKABgLAGQgJAEgLAKQgLAJgKARIgBCAIglABIgEkoIAsgBIgBB0QALgLAMgHQAMgGAKgEQAMgEALgBQAVAAASAIQASAHAMAPQANAOAIAVQAHAUABAaIAAAbIgCAcIgDAaQgCANgDAJg");
	this.shape_4.setTransform(191.6,37.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgXgWIhCACIAAgjIBDgCIABhVIAmgCIgCBWIBLgDIgDAkIhIACIgCCnIgnABg");
	this.shape_5.setTransform(158.9,37.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AA7BtIAEgbQgYAQgWAGQgWAHgUAAQgOAAgNgEQgNgDgKgIQgJgHgGgLQgHgMABgPQAAgQAEgMQAEgMAJgKQAIgIAKgHQAMgHANgEQAMgEAOgCQANgCANgBIAYACIATACIgGgTQgDgJgGgHQgGgIgIgEQgJgFgLAAQgIAAgIACQgKACgLAGQgMAGgNAKQgOAKgPAQIgYgcQATgSAQgLQARgLAQgGQAOgGANgCQAMgDAKAAQASAAAOAGQAOAGAKALQALAKAGAPQAHANAGARQAEAQACASQACAQAAARQAAASgCAWIgGAugAgIgBQgQADgMAIQgMAIgGAMQgGAKADAOQAEAMAHAFQAIAFALAAQALAAANgFQAMgDANgGIAYgLIATgLIAAgTQABgKgCgKQgJgCgKgBQgKgCgLAAQgRAAgPADg");
	this.shape_6.setTransform(136.7,41.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AA0CSQAFgSADgSIADgfIAAgfQAAgUgHgPQgGgNgJgJQgJgJgKgFQgLgEgLAAQgKABgLAGQgJAEgLAKQgLAJgKARIgBCAIglABIgEkoIAsgBIgBB0QALgLAMgHQAMgGAKgEQAMgEALgBQAVAAASAIQASAHAMAPQANAOAIAVQAHAUABAaIAAAbIgCAcIgDAaQgCANgDAJg");
	this.shape_7.setTransform(112.2,37.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXgWIhCACIAAgjIBDgCIABhVIAmgCIgCBWIBLgDIgDAkIhIACIgCCnIgnABg");
	this.shape_8.setTransform(89.9,37.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgXgWIhCACIAAgjIBDgCIABhVIAmgCIgCBWIBLgDIgDAkIhIACIgCCnIgnABg");
	this.shape_9.setTransform(60.9,37.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_10.setTransform(40,41.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgRAyIggAgIgCA5IgpADIAJknIAsAAIgIC1IBjhjIAeAXIhIBGIBTBrIghAZg");
	this.shape_11.setTransform(18.3,37.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgbBsIgQgDIgRgGIgSgIIgSgMIATgdIAWALQALAFALAEIAYAHQALACANAAQALAAAHgCQAHgCAFgDQAFgDADgEIADgHIABgHQAAgDgCgFQgCgEgEgDQgDgEgHgDQgFgDgKgCIgVgCQgQgBgQgDQgQgCgNgHQgMgFgIgJQgJgKgBgPQgCgNAEgMQADgLAGgJQAHgKAJgGQAJgHALgEQAMgFANgCQAMgDAMABIARAAQAKABAMACQALACALAFQALAFAKAGIgNAjQgNgGgLgEIgVgGIgTgDQgegCgQAJQgRAHAAARQAAALAHAGQAGAFALACIAaAEIAdADQATADANAGQANAEAIAIQAIAHADAKQADAJAAAKQAAATgGAMQgIANgNAHQgMAIgQAEQgQADgRACQgRgBgSgEg");
	this.shape_12.setTransform(-4.7,42.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA8BtIACgbQgXAQgWAGQgXAHgTAAQgOAAgNgEQgNgDgJgIQgLgHgFgLQgHgMAAgPQABgQAEgMQAFgMAHgKQAJgIAKgHQAMgHANgEQAMgEAOgCQAMgCAOgBIAYACIAUACIgHgTQgDgJgGgHQgGgIgIgEQgIgFgMAAQgIAAgIACQgKACgLAGQgMAGgOAKQgNAKgPAQIgYgcQASgSARgLQARgLAQgGQAOgGANgCQAMgDAKAAQARAAAPAGQANAGALALQAKAKAIAPQAHANAFARQAEAQACASQACAQAAARQAAASgCAWIgGAugAgJgBQgPADgMAIQgMAIgGAMQgGAKAEAOQADAMAHAFQAIAFALAAQALAAANgFQAMgDANgGIAYgLIATgLIAAgTQABgKgCgKQgJgCgKgBQgKgCgLAAQgRAAgQADg");
	this.shape_13.setTransform(-28.3,41.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgRCTQgLgDgNgGQgNgFgMgMIAAAZIgjABIgEknIAqgCIgBByQALgLAMgGQAMgGALgCQALgDAKgBQANAAANAEQANAEAMAGQALAHAKAKQAKAKAHALQAHAMADAMQAEAOAAAPQgBARgEAPQgFAOgHANQgHAMgKAJQgJAKgLAGQgLAHgMAEQgMAEgKAAQgMgBgMgDgAgWgZQgLAEgIAHQgIAHgGAHQgHAJgDAKIAAAtQADAMAHAKQAGAJAIAGQAJAGAJADQAKADAKABQANAAANgFQALgGAKgJQAJgJAGgNQAFgOAAgOQACgPgFgNQgEgNgJgJQgJgKgNgHQgMgFgOAAQgLAAgLADg");
	this.shape_14.setTransform(-52.1,37.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_15.setTransform(-87.2,41.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AA0CSQAFgSADgSIADgfIAAgfQAAgUgHgPQgGgNgJgJQgJgJgKgFQgLgEgLAAQgKABgLAGQgJAEgLAKQgLAJgKARIgBCAIglABIgEkoIAsgBIgBB0QALgLAMgHQAMgGAKgEQAMgEALgBQAVAAASAIQASAHAMAPQANAOAIAVQAHAUABAaIAAAbIgCAcIgDAaQgCANgDAJg");
	this.shape_16.setTransform(-111.7,37.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgXgWIhCACIAAgjIBDgCIABhVIAmgCIgCBWIBLgDIgDAkIhIACIgCCnIgnABg");
	this.shape_17.setTransform(-134.1,37.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgXgWIhCACIAAgjIBDgCIABhVIAmgCIgCBWIBLgDIgDAkIhIACIgCCnIgnABg");
	this.shape_18.setTransform(-163.1,37.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgiBoQgVgJgQgPQgPgPgKgVQgKgTAAgZQAAgNAFgOQADgOAIgNQAHgMAKgKQAKgLANgHQAMgIAPgFQAPgEAPAAQAQAAAPAEQAPAEANAIQANAHAKALQALALAGAOIABABIggATIAAgBQgFgKgIgHQgHgIgIgGQgJgFgKgDQgLgDgKAAQgPAAgOAGQgOAGgKAKQgKAKgGAOQgGAOAAAPQAAAQAGAOQAGAOAKAKQAKALAOAFQAOAGAPAAQAKAAAJgCQAKgDAJgFQAIgFAHgHQAHgHAFgIIABgCIAhATIAAACQgHAMgLAKQgLAKgNAHQgNAHgOAEQgPAEgPAAQgVAAgUgIg");
	this.shape_19.setTransform(-184.3,41.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_20.setTransform(-207.8,41.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAMCoQgKgFgHgHQgIgHgGgJQgGgJgDgJQgKgWgCgcIAFjzIAnAAIgBA/IgCA0IgBApIAAAgIgBA1QAAARAEAOIAFAMQADAGAFAFQAEAEAHADQAGACAIABIgFAmQgNAAgLgEg");
	this.shape_21.setTransform(-224,34.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_22.setTransform(-242.2,41.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgZCbQgOgEgOgFQgOgGgOgIIgbgRIAcghQAPANAPAIQAPAHAMAEQANAFAMABQAOABALgDQAMgDAJgFQAJgGAGgHQAFgIABgIQABgIgDgFQgEgGgGgFQgFgFgJgEQgIgEgJgCQgJgEgKgCIgRgDIgXgFQgNgEgLgFQgMgFgKgIQgLgHgHgKQgIgLgEgPQgEgOACgTQABgPAFgMQAFgNAJgKQAIgKALgGQALgIAMgEQAMgFAOgCQANgCAMAAQASABASAFIAQAFIASAHIARAKQAIAFAIAIIgWAiQgGgHgHgFQgHgGgHgDIgOgHIgNgEQgPgEgOgBQgQAAgOAFQgOAGgJAIQgJAIgFALQgFAKAAAKQAAAKAGAJQAGAJAKAIQAKAHAOAGQAOAFAQACQAOABAOAEQAOACAMAGQANAFALAIQAKAHAIAKQAHAKADANQAEAMgDAPQgBANgGALQgGAKgIAIQgJAIgKAFQgLAFgLAEIgXAEIgXACQgOAAgOgDg");
	this.shape_23.setTransform(-266.6,37.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAr+gEkMhX7AAAQh5AAhXBVQhVBXAAB4QAAB5BVBWQBXBWB5AAMBX7AAAQB5AABWhWQBWhWAAh5QAAh4hWhXQhWhVh5AAg");
	this.shape_24.setTransform(0.9,35.3,1.05,1.05);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF3333").s().p("Egr9AElQh5AAhXhWQhVhWAAh5QAAh4BVhXQBXhVB5AAMBX7AAAQB5AABWBVQBWBXAAB4QAAB5hWBWQhWBWh5AAg");
	this.shape_25.setTransform(0.9,35.3,1.05,1.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mccopy2, new cjs.Rectangle(-326.8,3,655.5,64.6), null);


(lib.InstructionText_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrCKQgTgIgOgPQgOgPgJgUQgIgVAAgaQAAgaAJgTQAJgVAPgOQAPgOASgHQASgIAUAAQAMAAANADQALAEANAFQANAHAMALIABhiIAkADIgBEbIgnAAIABgZIgNAJIgMAHIgLAGIgLADQgMADgLAAQgXAAgTgHgAgdghQgMAGgIAKQgJALgFAMQgFAOABAQQAAAQAEAOQAFANAJAKQAJAKAMAGQAMAFAPAAQALAAALgEQALgEAKgHQAJgHAIgKQAGgKAEgLIABgpQgEgNgHgIQgHgLgKgHQgKgHgMgFQgLgDgLAAQgPAAgMAFg");
	this.shape.setTransform(281,35.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_1.setTransform(257.2,40);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhpibIAqgCIAAAcQALgLAMgFQAMgGAKgDQALgEALgBQANAAANAEQAMAEAMAHQAMAGAJAKQAKAKAHALQAHAMAEAOQADANAAAPQgBARgEAPQgFAOgHAMQgHAMgKAJQgJAKgLAHQgLAHgLADQgMAEgMAAQgKgBgNgDQgKgCgNgGQgNgGgMgKIgBCCIgkABgAAAh4QgLAAgLAFQgKAEgJAHQgIAGgGAJQgGAJgDAKIAAAlQADAMAGAKQAGAKAJAFQAIAGAKAEQAJADAKAAQANABAMgFQAMgGAIgIQAJgJAGgMQAFgNABgOQAAgOgEgNQgEgNgJgKQgIgJgMgGQgLgGgNAAIgCAAg");
	this.shape_2.setTransform(233.3,44.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhpibIAqgCIAAAcQALgLAMgFQAMgGAKgDQALgEALgBQANAAANAEQAMAEAMAHQAMAGAJAKQAKAKAHALQAHAMAEAOQADANAAAPQgBARgEAPQgFAOgHAMQgHAMgKAJQgJAKgLAHQgLAHgLADQgMAEgMAAQgKgBgNgDQgKgCgNgGQgNgGgMgKIgBCCIgkABgAAAh4QgLAAgLAFQgKAEgJAHQgIAGgGAJQgGAJgDAKIAAAlQADAMAGAKQAGAKAJAFQAIAGAKAEQAJADAKAAQANABAMgFQAMgGAIgIQAJgJAGgMQAFgNABgOQAAgOgEgNQgEgNgJgKQgIgJgMgGQgLgGgNAAIgCAAg");
	this.shape_3.setTransform(207.9,44.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgoBpQgSgIgOgPQgOgPgIgUQgIgWAAgYQAAgZAIgVQAIgVAOgPQAOgOASgJQATgIAVAAQAVAAATAJQASAIAOAQQAOAOAJAWQAIAUAAAYQAAAYgIAUQgJAVgOAPQgOAPgSAIQgTAJgVAAQgVAAgTgIgAgbhHQgMAIgHALQgIALgEAOQgDAOAAANQAAAOAEAOQAEAOAHALQAIALAMAIQALAGAPAAQAQAAAMgGQALgIAIgLQAIgLADgOQAFgOAAgOQgBgNgDgOQgEgOgHgLQgIgLgMgIQgMgHgQAAQgPAAgMAHg");
	this.shape_4.setTransform(182.7,39.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhWAwIgCgwIgBglIgBgcIgBgoIAsgBIAAAYIAAAWIABAYIANgUQAIgKAKgKQAKgJAMgHQALgHAOgCQAPgBANAHIALAJQAFAFAEAIQAFAIADALQADALABAPIgmAOIgBgQQgCgHgCgFQgCgFgDgEIgGgFQgGgFgJAAQgFABgGAEQgGAEgGAGIgMANIgNAPIgLAQIgKANIABAcIAAAaIABAWIABAPIgpAFIgCg7g");
	this.shape_5.setTransform(161.3,39.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgrCKQgTgIgPgPQgOgPgHgUQgJgVAAgaQAAgaAJgTQAJgVAPgOQAOgOATgHQATgIASAAQAMAAANADQAMAEANAFQANAHAMALIABhiIAkADIAAEbIgoAAIAAgZIgLAJIgMAHIgNAGIgLADQgMADgLAAQgWAAgTgHgAgdghQgLAGgKAKQgIALgFAMQgEAOgBAQQAAAQAGAOQAEANAJAKQAIAKANAGQAMAFAOAAQALAAAMgEQALgEAKgHQAJgHAHgKQAIgKADgLIAAgpQgDgNgIgIQgGgLgLgHQgJgHgMgFQgLgDgMAAQgOAAgMAFg");
	this.shape_6.setTransform(135.7,35.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgbBsIgQgDIgSgGIgSgIIgRgMIATgcIAWAKQALAFALAEIAYAHQALACANAAQALAAAHgCQAIgCAEgDQAFgDADgEIADgHIABgHQAAgDgCgFQgCgEgEgDQgDgEgHgDQgGgDgIgCIgWgCQgQgBgQgDQgQgCgNgHQgMgFgIgJQgIgKgCgPQgCgNAEgMQADgLAGgJQAHgKAJgGQAJgHALgEQAMgFANgCQAMgDAMABIARABQAKAAAMACQALACALAFQALAFAKAGIgNAjQgNgGgLgEIgVgGIgTgDQgdgCgRAJQgRAHAAARQAAALAHAGQAGAFALACIAaAEIAdADQAUADAMAGQANAEAIAIQAIAHADAKQADAJABAKQgBATgGAMQgIANgMAHQgNAIgQAEQgQADgRACQgRgBgSgEg");
	this.shape_7.setTransform(102,40.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_8.setTransform(79.8,40);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhpibIAqgCIAAAcQALgLAMgFQAMgGAKgDQALgEALgBQANAAANAEQAMAEAMAHQAMAGAJAKQAKAKAHALQAHAMAEAOQADANAAAPQgBARgEAPQgFAOgHAMQgHAMgKAJQgJAKgLAHQgLAHgLADQgMAEgMAAQgKgBgNgDQgKgCgNgGQgNgGgMgKIgBCCIgkABgAAAh4QgLAAgLAFQgKAEgJAHQgIAGgGAJQgGAJgDAKIAAAlQADAMAGAKQAGAKAJAFQAIAGAKAEQAJADAKAAQANABAMgFQAMgGAIgIQAJgJAGgMQAFgNABgOQAAgOgEgNQgEgNgJgKQgIgJgMgGQgLgGgNAAIgCAAg");
	this.shape_9.setTransform(55.9,44.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA7BtIADgbQgXAQgXAGQgVAHgUAAQgOAAgMgEQgNgDgLgIQgKgHgGgLQgFgMAAgPQgBgQAFgMQAEgMAJgKQAIgIALgHQAKgHANgEQANgEANgCQANgCAOgBIAYACIATACIgGgTQgEgJgFgHQgGgIgIgEQgIgFgMAAQgHAAgJACQgJACgMAGQgMAGgNAKQgOAKgQAQIgXgcQASgSASgLQAQgLAPgGQAPgGANgCQAMgDAKAAQASAAANAGQAOAGALALQAKAKAIAPQAHANAEARQAFAQACASQACAQAAARQAAASgCAWIgHAugAgJgBQgQADgMAIQgLAIgGAMQgGAKADAOQADAMAIAFQAIAFALAAQALAAANgFQAMgDANgGIAYgLIATgLIABgTQAAgKgBgKQgKgCgKgBQgLgCgKAAQgRAAgQADg");
	this.shape_10.setTransform(29.7,39.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AA0CSQAFgSADgSIADgfIAAgfQAAgUgHgPQgGgNgJgJQgJgJgKgFQgLgEgLAAQgKABgLAGQgJAEgLAKQgLAJgKARIgBCAIglABIgEkoIAsgBIgBB0QALgLAMgHQAMgGAKgEQAMgEALgBQAVAAASAIQASAHAMAPQANAOAIAVQAHAUABAaIAAAbIgCAcIgDAaQgCANgDAJg");
	this.shape_11.setTransform(5.3,35.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgbBsIgQgDIgRgGIgSgIIgRgMIATgcIAVAKQALAFAMAEIAXAHQAMACAMAAQALAAAHgCQAIgCAEgDQAFgDACgEIAEgHIABgHQAAgDgCgFQgCgEgDgDQgEgEgGgDQgHgDgJgCIgUgCQgRgBgQgDQgQgCgMgHQgNgFgIgJQgJgKgBgPQgBgNADgMQADgLAHgJQAFgKAKgGQAKgHALgEQALgFAMgCQAMgDAMABIASABQAKAAAMACQAKACAMAFQALAFAKAGIgNAjQgNgGgMgEIgUgGIgTgDQgegCgQAJQgRAHAAARQAAALAGAGQAHAFAKACIAaAEIAfADQASADANAGQANAEAIAIQAIAHAEAKQACAJAAAKQABATgIAMQgHANgNAHQgMAIgQAEQgQADgSACQgQgBgSgEg");
	this.shape_12.setTransform(-19,40.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_13.setTransform(-51.5,40);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AA0CSQAFgSADgSIADgfIAAgfQAAgUgHgPQgGgNgJgJQgJgJgKgFQgLgEgLAAQgKABgLAGQgJAEgLAKQgLAJgKARIgBCAIglABIgEkoIAsgBIgBB0QALgLAMgHQAMgGAKgEQAMgEALgBQAVAAASAIQASAHAMAPQANAOAIAVQAHAUABAaIAAAbIgCAcIgDAaQgCANgDAJg");
	this.shape_14.setTransform(-76,35.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgXgWIhCACIAAgjIBDgCIABhVIAmgCIgCBWIBLgDIgDAkIhIACIgCCnIgnABg");
	this.shape_15.setTransform(-98.4,36);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_16.setTransform(-129.6,40);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeBnIhEjBIAogGIA5CkIA8irIAoAFIhMDKg");
	this.shape_17.setTransform(-152.2,39.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AhWAwIgCgwIgBglIgBgcIgBgoIAsgBIAAAYIAAAWIABAYIANgUQAIgKAKgKQAKgJAMgHQALgHAOgCQAPgBANAHIALAJQAFAFAFAIQAEAIADALQADALABAPIgmAOIgBgQQgCgHgCgFQgCgFgDgEIgGgFQgGgFgJAAQgFABgGAEQgHAEgFAGIgNANIgMAPIgLAQIgKANIABAcIABAaIAAAWIABAPIgpAFIgCg7g");
	this.shape_18.setTransform(-172.8,39.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgrBnQgTgIgOgOQgNgOgHgTQgHgUAAgYQAAgYAIgUQAIgWAOgPQAPgPATgJQATgJAVAAQAUAAASAIQASAHANAOQAOAOAJATQAJATACAVIilAYQABAOAFALQAGALAIAHQAIAIAMADQALAFALAAQALAAAKgEQAKgDAIgGQAJgGAGgJQAGgJADgMIAlAHQgFASgKAOQgKAPgNAJQgNALgQAFQgPAGgSAAQgYAAgUgIgAgPhKQgKADgJAGQgJAIgHAKQgHAMgDAQIBygNIgBgEQgHgTgNgLQgNgLgTAAQgHAAgJADg");
	this.shape_19.setTransform(-196.1,40);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgbBsIgQgDIgRgGIgSgIIgRgMIASgcIAWAKQALAFAMAEIAXAHQAMACAMAAQALAAAHgCQAHgCAFgDQAFgDACgEIAEgHIABgHQAAgDgCgFQgCgEgEgDQgEgEgGgDQgFgDgKgCIgVgCQgQgBgQgDQgQgCgNgHQgMgFgIgJQgIgKgCgPQgCgNAEgMQADgLAGgJQAHgKAJgGQAJgHALgEQAMgFANgCQALgDAMABIASABQAKAAAMACQALACALAFQALAFAKAGIgNAjQgNgGgMgEIgUgGIgTgDQgdgCgRAJQgRAHAAARQAAALAHAGQAGAFALACIAZAEIAfADQASADANAGQANAEAIAIQAIAHADAKQADAJAAAKQAAATgGAMQgIANgNAHQgMAIgQAEQgQADgSACQgQgBgSgEg");
	this.shape_20.setTransform(-219,40.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgRCTQgKgDgNgGQgNgFgNgMIAAAZIgjABIgEknIAqgCIgBByQALgLAMgGQAMgGAKgCQAMgDALgBQANAAANAEQANAEALAGQAMAHAKAKQAJAKAHALQAHAMAEAMQADAOAAAPQgBARgEAPQgEAOgIANQgHAMgKAJQgJAKgLAGQgLAHgLAEQgMAEgMAAQgKgBgNgDgAgWgZQgLAEgHAHQgJAHgGAHQgGAJgEAKIAAAtQADAMAGAKQAHAJAJAGQAHAGALADQAJADAKABQANAAANgFQAMgGAJgJQAJgJAGgNQAFgOAAgOQACgPgFgNQgFgNgIgKQgJgJgNgHQgMgFgOAAQgMAAgKADg");
	this.shape_21.setTransform(-241.8,35.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgmCPQgSgFgRgKQgQgJgNgOQgNgOgKgQQgJgRgFgTQgFgTAAgUQAAgUAFgTQAFgTAJgQQAKgRANgNQANgOAQgKQARgJASgGQATgFATAAQAUAAATAFQASAGAQAJQAQAKAOAOQANANAKARQAJAQAFATQAFATAAAUQAAAUgFATQgFATgJARQgKAQgNAOQgOAOgQAJQgQAKgSAFQgTAGgUAAQgTAAgTgGgAgphlQgTAJgPAPQgPAPgIAUQgIAUAAAWQAAAXAIAUQAIAUAPAPQAPAPATAJQAUAJAVAAQAPAAAOgEQANgEAMgHQAMgIAKgKQAKgKAHgMQAHgNADgOQAEgOAAgPQAAgOgEgOQgDgPgHgMQgHgMgKgKQgKgKgMgIQgMgHgNgEQgOgEgPAAQgVAAgUAJg");
	this.shape_22.setTransform(-271.3,35.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAuKgEzMhcUAAAQh/AAhbBaQhaBbAAB+QAACABaBZQBbBbB/AAMBcUAAAQCAAABahbQBbhZAAiAQAAh+hbhbQhahaiAAAg");
	this.shape_23.setTransform(1,35.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#0066FF").s().p("EguJAE0QiAAAhahbQhahaAAh/QAAh/BahaQBahaCAAAMBcTAAAQCAAABZBaQBcBaAAB/QAAB/hcBaQhZBbiAAAg");
	this.shape_24.setTransform(1,35.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mccopy, new cjs.Rectangle(-326.8,3.3,655.5,64.6), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgdB0IgRgEIgTgGIgTgJQgJgFgJgHIAUgfIAXALIAYAKQAMAEANADQAMACAOAAQALAAAIgCQAIgCAFgDIAIgHIAEgIQABgEgBgDQAAgFgCgEQgCgEgEgEQgEgEgGgDQgHgDgJgCQgKgCgNgBQgRgBgRgCQgRgDgNgHQgOgGgJgJQgIgLgCgPQgCgPAEgMQADgMAHgKQAHgKAKgHQAKgHAMgFQAMgFANgCQANgDANAAIASABIAXAEQAMACAMAFQAMAEALAIIgPAlQgNgHgMgEIgWgGQgLgDgKAAQgfgCgRAIQgSAJAAARQAAANAGAFQAHAGAMACQALADAQABQAOAAASADQAUADAOAGQAOAFAIAJQAIAIAEAKQAEAKAAAKQAAAUgIANQgIANgNAJQgNAIgRAEQgSAEgSABQgSgBgTgDg");
	this.shape.setTransform(175.6,8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AguCSQgVgIgPgPQgOgQgJgWQgJgWAAgcQAAgcAKgVQAJgVAQgPQAPgPAVgIQATgIAUAAQANABAOADQAMADAOAGQAOAHAMAMIAChoIAmACIgBEuIgqAAIABgaIgNAKIgMAHIgOAFIgLAEQgNADgMAAQgYABgUgJgAgfgjQgNAGgIALQgKALgFAOQgFAPAAAQQAAARAFAPQAFAPAKAJQAJALANAGQANAGAPAAQAMAAAMgFQAMgDAKgIQAKgHAIgLQAHgKAFgMIAAgsQgEgOgIgJQgHgLgLgIQgKgIgMgEQgNgEgMgBQgPABgNAGg");
	this.shape_1.setTransform(149.4,3.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AANCyQgLgFgIgHQgIgIgGgJIgLgTQgJgYgDgdIAGkDIApAAIgBBDIgCA4IgBAsIAAAiIgBA3QAAATAEAOQACAHAEAGQADAHAFAEQAFAFAGADQAHADAJAAIgGApQgOAAgLgFg");
	this.shape_2.setTransform(131.8,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgqBwQgTgJgPgQQgQgQgIgWQgJgWAAgbQAAgaAJgWQAIgXAQgPQAPgQATgJQAUgIAWAAQAXAAATAJQAVAJAOAQQAQAQAIAWQAJAWAAAZQAAAagJAWQgIAWgQAQQgOAQgVAJQgTAJgXAAQgWAAgUgIgAgdhMQgNAIgHAMQgJAMgDAPQgEAPAAAOQAAAOAEAPQAEAPAIAMQAJAMAMAIQAMAHAQAAQARAAANgHQANgIAHgMQAJgMADgPQAFgPAAgOQgBgOgDgPQgEgPgIgMQgIgMgMgIQgNgHgSAAQgQAAgNAHg");
	this.shape_3.setTransform(112.1,7.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA2CcQAGgUADgTIAEghIABghQgBgWgHgPQgHgOgJgKQgKgKgLgEQgLgEgMAAQgLABgLAFQgKAFgMALQgMAJgKATIgBCHIgpABIgCk6IAvgCIgCB8QALgMANgHQANgHALgEQANgEALgBQAXAAATAIQATAIANAQQAOAPAIAWQAHAVACAbIgBAeIgCAeIgCAbQgCAOgEAKg");
	this.shape_4.setTransform(86.1,2.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIAChaIAngCIgBBbIBPgDIgDAnIhNACIgCCxIgqABg");
	this.shape_5.setTransform(51.2,3.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AA/B1IADgdQgZAQgYAHQgXAHgVAAQgOAAgOgEQgOgDgLgIQgKgIgHgMQgGgNAAgQQAAgRAFgNQAFgNAIgKQAJgJAMgHQALgHAOgFQANgEAPgCQAOgDAOAAIAZABIAVADQgDgKgEgKQgDgKgGgIQgHgIgIgFQgJgFgMAAQgIAAgKADQgKACgMAGQgMAGgPALQgPALgQARIgZgeQAUgTASgMQASgLAQgHQAQgHANgCQANgCALAAQASAAAPAGQAPAGALAMQALALAIAPQAIAPAEARQAFARADATQACASAAASQAAAUgDAWQgCAXgFAagAgJAAQgSADgMAIQgMAIgHAMQgGAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVQgKgCgLgCQgLgCgMAAQgSAAgQAFg");
	this.shape_6.setTransform(27.6,6.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA2CcQAHgUACgTIAEghIABghQgBgWgHgPQgHgOgJgKQgKgKgLgEQgLgEgMAAQgKABgMAFQgKAFgMALQgMAJgKATIgBCHIgpABIgCk6IAugCIgBB8QALgMANgHQAMgHAMgEQANgEALgBQAXAAATAIQASAIAOAQQANAPAJAWQAHAVACAbIgBAeIgCAeIgDAbQgBAOgDAKg");
	this.shape_7.setTransform(1.6,2.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIAChaIAngCIgBBbIBPgDIgDAnIhNACIgBCxIgrABg");
	this.shape_8.setTransform(-22.2,3.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIAChaIAngCIgBBbIBPgDIgDAnIhMACIgCCxIgqABg");
	this.shape_9.setTransform(-53,3.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgtBuQgVgIgOgQQgPgOgHgWQgIgUAAgbQAAgYAJgWQAJgWAPgQQAPgRAUgJQAVgKAXAAQAVAAATAJQATAHAOAPQAPAPAJAUQAJAUADAXIiwAZQABAOAGANQAGALAJAIQAIAIAMAEQAMAEANAAQALAAAKgDQALgDAJgHQAJgGAHgKQAGgKADgMIAoAIQgGASgLAQQgKAPgOAKQgOALgQAGQgRAGgSAAQgaAAgVgIgAgQhPQgKADgKAHQgKAIgHALQgIAMgDATIB6gPIgBgFQgIgUgNgLQgOgLgUAAQgIAAgKACg");
	this.shape_10.setTransform(-75.3,7.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgSA2IgiAhIgCA9IgsADIAKk6IAuAAIgIDBIBqhqIAfAZIhMBLIBYByIgkAag");
	this.shape_11.setTransform(-98.5,2.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgdB0IgRgEIgTgGIgTgJQgJgFgJgHIAUgfIAXALIAYAKQAMAEANADQAMACAOAAQALAAAIgCQAIgCAFgDIAIgHIAEgIQABgEgBgDQAAgFgCgEQgCgEgEgEQgEgEgGgDQgHgDgJgCQgKgCgNgBQgRgBgRgCQgRgDgNgHQgOgGgJgJQgIgLgCgPQgCgPAEgMQADgMAHgKQAHgKAKgHQAKgHAMgFQAMgFANgCQANgDANAAIASABIAXAEQAMACAMAFQAMAEALAIIgPAlQgNgHgMgEIgWgGQgLgDgKAAQgfgCgRAIQgSAJAAARQAAANAGAFQAHAGAMACQALADAQABQAOAAASADQAUADAOAGQAOAFAIAJQAIAIAEAKQAEAKAAAKQAAAUgIANQgIANgNAJQgNAIgRAEQgSAEgSABQgSgBgTgDg");
	this.shape_12.setTransform(-122.9,8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AA/B1IADgdQgZAQgYAHQgXAHgVAAQgOAAgOgEQgOgDgLgIQgKgIgHgMQgGgNAAgQQAAgRAFgNQAFgNAIgKQAJgJAMgHQALgHAOgFQANgEAPgCQAOgDAOAAIAZABIAVADQgDgKgEgKQgDgKgGgIQgHgIgIgFQgJgFgMAAQgIAAgKADQgKACgMAGQgMAGgPALQgPALgQARIgZgeQAUgTASgMQASgLAQgHQAQgHANgCQANgCALAAQASAAAPAGQAPAGALAMQALALAIAPQAIAPAEARQAFARADATQACASAAASQAAAUgDAWQgCAXgFAagAgJAAQgSADgMAIQgMAIgHAMQgGAMAEAPQADAMAIAFQAIAFAMAAQAMAAAOgEIAagJIAagNIAUgMIABgUIgBgVQgKgCgLgCQgLgCgMAAQgSAAgQAFg");
	this.shape_13.setTransform(-148,6.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgSCcQgMgCgNgHQgOgGgNgMIAAAbIgnABIgDk7IAtgCIgBB6QALgMANgGQANgGALgDQANgEALAAQAOAAANAEQAOAEAMAHQANAIAKAKQAKAKAIAMQAHANAEANQAEAPAAAQQgBASgFAPQgEAPgIAOQgIANgKAKQgKAKgMAHQgLAIgNADQgMAFgMAAQgMgBgNgEgAAAgeQgMAAgLAEQgMAFgIAGQgJAHgHAJQgHAJgDAKIgBAxQAEAMAHAKQAHAKAJAHQAIAGALAEQAKADALAAQAOABANgGQANgGAJgKQAKgKAGgOQAGgOABgPQABgPgFgOQgFgPgJgKQgKgLgNgGQgMgGgPAAIgBAAg");
	this.shape_14.setTransform(-173.4,2.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(3,1,1).p("A9ikIMA7FAAAQBrAABMBMQBNBMAABsIAAAJQAABrhNBMQhMBNhrAAMg7FAAAQhrAAhNhNQhMhMAAhrIAAgJQAAhsBMhMQBNhMBrAAg");
	this.shape_15.setTransform(-4.2,1.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("A9iEJQhrAAhNhMQhMhMAAhsIAAgJQAAhrBMhNQBNhMBrAAMA7FAAAQBrAABMBMQBNBNAABrIAAAJQAABshNBMQhMBMhrAAg");
	this.shape_16.setTransform(-4.2,1.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.7,-30.8,433.2,65.8);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.inside_fruitcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AAAltIDTFtIjTFuIjSlug");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0000").s().p("AjSAAIDSltIDTFtIjTFug");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.027)").s().p("AlHFIQiIiIAAjAQAAi/CIiIQCIiIC/AAQDAAACJCIQCHCIAAC/QAADAiHCIQiJCIjAABQi/gBiIiIg");
	this.shape_2.setTransform(-0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy2, new cjs.Rectangle(-46.5,-46.6,92.9,92.9), null);


(lib.inside_fruitcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AAAuoIQ7dRMgh1AAAg");
	this.shape.setTransform(0,0,0.289,0.289);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6600CC").s().p("Aw6OpIQ69RIQ7dRg");
	this.shape_1.setTransform(0,0,0.289,0.289);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.027)").s().p("AlHFIQiIiIAAjAQAAi/CIiIQCIiIC/AAQDAAACJCIQCHCIAAC/QAADAiHCIQiJCIjAABQi/gBiIiIg");
	this.shape_2.setTransform(-0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy, new cjs.Rectangle(-46.5,-46.6,92.9,92.9), null);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.question = new lib.inside_fruitcopy2();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(214.3,0.5,2.333,2.333);

	this.question_1 = new lib.inside_fruitcopy();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(-213.7,0.5,2.333,2.333);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.question_1},{t:this.question}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-322.3,-108.3,644.8,216.7);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween17("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(12.1,0,1.008,1.008);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1.01,scaleY:1.01},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1.01,scaleY:1.01},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-195.6,-35.8,417,71.8);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.25,scaleY:1.25},14).to({scaleX:1,scaleY:1},15).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.9,-14,37.8,100);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.08,scaleY:1.08},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.08,scaleY:1.08},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-78.1,-67.7,156.2,135.5);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-78.1,-67.7,156.2,135.5), null);


(lib.Nbbaskt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0.1,-49.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Nbbaskt, new cjs.Rectangle(-238.2,-126,476.5,252), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03,y:-2},6).to({scaleX:1,scaleY:1,y:0},7).to({scaleX:1.03,scaleY:1.03,y:-2},7).to({scaleX:1,scaleY:1,y:0},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.7,-30.8,433.2,65.8);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween6("synched",0);
	this.instance.parent = this;

	this.question = new lib.inside_fruitcopy();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(-213.7,0.5,2.333,2.333);
	this.question.alpha = 0.68;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.question},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-322.3,-108.3,644.8,216.7);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy("synched",23);
	this.instance.parent = this;

	this.instance_1 = new lib.Symbol1copy2();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-80.6,-69.9,161.3,139.9);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.RedBasket = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.Nbbaskt();
	this.instance.parent = this;
	this.instance.setTransform(-2.5,3.7,0.383,0.383,0,0,0,-0.1,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.4)").s().p("ApHA2QjxgWAAggQAAgeDxgXQDygWFVAAQFWAADxAWQDyAXAAAeQAAAgjyAWQjxAWlWAAQlVAAjygWg");
	this.shape.setTransform(-2.5,49.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 8
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.027)").s().p("AsuHpIAAvRIZdAAIAAPRg");
	this.shape_1.setTransform(-5.4,4.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.RedBasket, new cjs.Rectangle(-93.6,-44.8,182.4,101.9), null);


(lib.Tween11copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc3 = new lib.RedBasket();
	this.mc3.name = "mc3";
	this.mc3.parent = this;
	this.mc3.setTransform(218.5,29.3,1.728,1.728,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.mc3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(56.5,-48.4,315.1,176.2);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.mc3 = new lib.RedBasket();
	this.mc3.name = "mc3";
	this.mc3.parent = this;
	this.mc3.setTransform(218.5,29.3,1.728,1.728,0,0,0,0.1,0.1);

	this.mc2 = new lib.RedBasket();
	this.mc2.name = "mc2";
	this.mc2.parent = this;
	this.mc2.setTransform(-209.6,29.3,1.728,1.728,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc2},{t:this.mc3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-371.5,-48.4,743.1,176.2);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(427.9,610.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(412).to({_off:false},0).wait(35).to({startPosition:15},0).to({alpha:0,startPosition:3},8).wait(1));

	// Layer_11
	this.mc2 = new lib.RedBasket();
	this.mc2.name = "mc2";
	this.mc2.parent = this;
	this.mc2.setTransform(430.4,587.3,1.728,1.728,0,0,0,0.1,0.1);
	this.mc2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc2).wait(390).to({_off:false},0).to({scaleX:1.9,scaleY:1.9,y:587.4},6).wait(51).to({alpha:0},8).wait(1));

	// question
	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(854.1,395.4,1.5,1.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(136).to({_off:false},0).to({x:854,y:395.3,alpha:1},14).to({x:854.2,y:395.5},29).to({x:854.1,y:395.4,alpha:0},6).to({_off:true},1).wait(270));

	// question
	this.instance_2 = new lib.Symbol5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(426,395.4,1.5,1.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(136).to({_off:false},0).to({y:395.3,alpha:1},14).to({y:395.5},29).to({y:395.4,alpha:0},6).to({_off:true},1).wait(270));

	// Layer_9
	this.instance_3 = new lib.Symbol1copy("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(537.7,650.7,1,1,0,0,0,190.5,64.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(355).to({_off:false},0).to({_off:true},35).wait(66));

	// Layer_8
	this.instance_4 = new lib.Symbol1copy3("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(429.1,458.5,1,1,0,0,0,41,60.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(321).to({_off:false},0).to({_off:true},34).wait(101));

	// Layer_6
	this.instance_5 = new lib.Tween6("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(640,589.5);

	this.question = new lib.inside_fruitcopy();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(426.3,589.9,2.333,2.333);
	this.question.alpha = 0.68;

	this.instance_6 = new lib.Tween18("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(640,589.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.question},{t:this.instance_5}]},270).to({state:[{t:this.instance_6}]},35).to({state:[{t:this.instance_6}]},6).to({state:[]},1).wait(144));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(305).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(144));

	// Layer_12
	this.instance_7 = new lib.Tween14("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(778.4,118.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(242).to({_off:false},0).wait(63).to({startPosition:7},0).to({startPosition:13},6).to({_off:true},1).wait(144));

	// Layer_1
	this.instance_8 = new lib.Symbol2copy("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,253.9);

	this.instance_9 = new lib.Symbol1copy2();
	this.instance_9.parent = this;
	this.instance_9.setTransform(640,253.9);

	this.instance_10 = new lib.Tween4("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,254);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9},{t:this.instance_8}]},242).to({state:[{t:this.instance_10}]},63).to({state:[{t:this.instance_10}]},6).to({state:[]},1).wait(144));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(305).to({_off:false},0).to({startPosition:0},6).to({_off:true},1).wait(144));

	// fruits
	this.instance_11 = new lib.Tween13("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,255.2);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(198).to({_off:false},0).to({y:253.8,alpha:1},14).to({startPosition:0},29).to({startPosition:0},1).to({startPosition:0},1).to({startPosition:0},10).to({startPosition:0},16).to({_off:true},1).wait(42).to({_off:false},0).wait(137).to({startPosition:0},0).to({alpha:0},6).wait(1));

	// Layer_4
	this.instance_12 = new lib.Tween12("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,253.8);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(192).to({_off:false},0).to({alpha:1},14).to({startPosition:0},243).to({alpha:0},6).wait(1));

	// Layer_3
	this.instance_13 = new lib.Tween11("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,558.1);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.instance_14 = new lib.Tween11copy("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,558.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_13}]},39).to({state:[{t:this.instance_13}]},7).to({state:[{t:this.instance_13}]},280).to({state:[{t:this.instance_14}]},64).to({state:[{t:this.instance_13}]},59).to({state:[{t:this.instance_13}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(39).to({_off:false},0).to({alpha:1},7).to({startPosition:0},280).to({_off:true},64).wait(59).to({_off:false},0).to({alpha:0},6).wait(1));

	// choices
	this.instance_15 = new lib.Tween6("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(640,249.5);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(60).to({_off:false},0).to({alpha:1},5).to({startPosition:0},29).to({y:589.5},16).to({alpha:0},13).to({_off:true},69).wait(264));

	// Layer_5
	this.questxt = new lib.InstructionText_mccopy2();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(639.1,78.1,1.152,1.152,0,0,0,0.1,0);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(192).to({_off:false},0).to({regY:0.1,x:639,y:78.2,alpha:1},6).to({regY:0,x:639.1,y:78.1},251).to({alpha:0},6).wait(1));

	// Layer_10
	this.instance_16 = new lib.Symbol9();
	this.instance_16.parent = this;
	this.instance_16.setTransform(785.1,116.3);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(123).to({_off:false},0).wait(62).to({y:118.3},0).to({alpha:0},6).to({_off:true},1).wait(264));

	// Layer_2
	this.questxt_1 = new lib.InstructionText_mccopy();
	this.questxt_1.name = "questxt_1";
	this.questxt_1.parent = this;
	this.questxt_1.setTransform(639.1,78.1,1.152,1.152,0,0,0,0.1,0);
	this.questxt_1.alpha = 0;
	this.questxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt_1).wait(20).to({_off:false},0).to({alpha:1},6).to({_off:true},166).wait(264));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(111.8,274.7,1919,907);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;