(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween34 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("Aw6OpIQ69RIQ7dRg");
	this.shape.setTransform(170.4,0,0.509,0.509);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("Ao1CmQjqjqAAlLIY/AAQAAFLjqDqQjrDqlLAAQlKAAjrjqg");
	this.shape_1.setTransform(-168.6,0,0.71,0.71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#46B0F7").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape_2.setTransform(57.4,0,0.37,0.37);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_3.setTransform(-55.6,0,0.527,0.527);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_4.setTransform(0,0,1.08,1.08);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_5.setTransform(0,0,1.08,1.08);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_6.setTransform(0,0,1.08,1.08);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_7.setTransform(0,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-249.8,-98.2,499.7,196.4);


(lib.Tween32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("AhWAuIgBguIgBgjIgBgbIgBgnIAqgBIAAA2QAGgLAIgJQAIgKAJgHQAJgHAJgEQALgFALgBQAOgBAKAEQALADAHAGQAHAGAFAIQAFAIADAJQADAIABAJIABAPQABAfAAAgIgBBDIgmgCIADg7QABgegCgdIAAgJIgCgLQgCgGgDgFQgCgGgFgFQgEgEgHgCQgGgCgIABQgPACgOATQgPATgSAkIACAvIABAbIAAAOIgoAFIgCg5g");
	this.shape.setTransform(143.3,4.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AhTAtIgCgtIgBgjIAAgbIgBgnIApgBIAAAXIABAWIAAAWIANgTQAIgKAKgJQAJgJALgHQAMgGANgCQAOgBANAHQAEADAFAFQAFAFAFAHQAFAJACAKQADAKABAPIgkAOQAAgJgCgHIgDgLQgCgGgDgCIgFgGQgHgEgIAAQgFABgGADIgLAJIgMAOIgMAOIgLAPIgJAMIABAbIAAAZIABAVIABAPIgoAFIgCg6g");
	this.shape_1.setTransform(122.5,4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("AgpBjQgTgHgMgOQgOgNgGgTQgHgTAAgXQAAgXAIgTQAIgVAOgPQANgOASgJQATgJAVAAQASABASAHQAQAIANANQAOANAIASQAJASACAVIifAWQABANAGALQAEAKAJAIQAIAHAKAEQALAEAMgBQAJAAAKgCQAJgEAIgFQAJgGAGgJQAFgJAEgLIAjAHQgFARgJANQgKAPgNAJQgMAKgPAFQgPAFgQAAQgYAAgTgHgAgPhHQgJADgIAGQgJAHgHAKQgHAMgDAPIBvgNIgCgEQgHgSgMgKQgNgLgRAAQgHAAgKADg");
	this.shape_2.setTransform(100.1,4.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("AgWgUIhAABIABgiIBAgCIAChRIAjgCIgBBSIBIgCIgDAjIhFABIgCCgIgmABg");
	this.shape_3.setTransform(79.8,0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AgWgUIhAABIABgiIBAgCIAChRIAjgCIgBBSIBIgCIgDAjIhFABIgCCgIgmABg");
	this.shape_4.setTransform(61.9,0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("AA5BpIADgaQgXAPgVAGQgVAHgTAAQgNAAgMgEQgNgDgJgHQgKgHgGgLQgGgLAAgPQAAgPAFgMQAEgMAIgJQAIgIAKgGQALgHAMgEQAMgEANgCQAMgCAOAAIAWABIATACIgGgSQgDgJgGgHQgFgHgIgEQgIgFgLAAQgIAAgIACQgJACgLAGQgLAGgNAJQgNAKgPAPIgXgbQASgRAQgKQARgLAOgGQAOgGANgCQALgCAKAAQARAAANAGQANAFAKALQAKAKAHANQAHAOAFAPQAEAQACARQACAQAAAQQAAASgCAUIgGAsgAgIAAQgQADgLAHQgLAIgGAKQgFALADAOQACALAIAEQAHAFALAAQALAAAMgEIAYgJIAXgLIASgLIABgSIgBgTQgJgCgKgBQgKgCgKAAQgQAAgPAEg");
	this.shape_5.setTransform(40.6,4.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("AhliVIApgCIAAAbQAKgKAMgGQALgFAKgDQALgDAKgBQAMAAANADQAMAEALAGQALAHAJAJQAKAKAHAKQAGAMAEANQADANAAAOQgBARgEAOQgEAOgHAKQgHAMgJAKQgKAJgKAHQgKAGgLADQgMADgLAAQgKAAgMgDQgKgDgMgEQgMgGgMgLIgBB+IgiABgAgVhvQgKAEgIAHQgIAGgFAJQgGAJgDAJIAAAjQACAMAGAKQAGAIAIAGQAIAGAKADQAJAEAKAAQAMAAALgFQALgFAJgIQAIgIAGgNQAFgLAAgOQABgNgEgMQgEgNgIgKQgJgJgLgFQgMgGgNAAQgLAAgKAEg");
	this.shape_6.setTransform(17.6,9.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgWgUIhAABIABgiIBAgCIAChRIAjgCIgBBSIBIgCIgDAjIhFABIgCCgIgmABg");
	this.shape_7.setTransform(-14,0.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("AghBjQgTgHgPgPQgQgPgJgTQgJgUAAgXQAAgNADgNQAEgNAHgMQAHgMAKgLQAKgJAMgIQAMgHAOgEQAOgFAOAAQARAAAOAFQAOADAMAIQANAHAKAKQAKALAGAOIABAAIgfATIgBgBQgEgKgHgHQgHgIgJgFQgIgGgKgCQgKgDgKAAQgOAAgNAGQgOAGgJAKQgKAJgGAOQgFAOAAANQAAAPAFANQAGAOAKAKQAJAKAOAGQANAFAOAAQAKAAAJgCQAJgDAJgFQAIgEAHgHQAHgGAEgJIABgBIAgASIAAABQgIANgJAJQgLAJgNAIQgMAGgOAEQgOAEgPgBQgUABgTgJg");
	this.shape_8.setTransform(-34.4,4.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgpBjQgTgHgNgOQgNgNgGgTQgHgTAAgXQAAgXAIgTQAHgVAPgPQANgOASgJQATgJAVAAQATABARAHQAQAIANANQAOANAIASQAJASACAVIifAWQACANAFALQAEAKAJAIQAIAHAKAEQALAEAMgBQAJAAAKgCQAJgEAJgFQAHgGAGgJQAHgJACgLIAkAHQgFARgJANQgKAPgNAJQgMAKgPAFQgPAFgQAAQgYAAgTgHgAgOhHQgKADgIAGQgJAHgHAKQgHAMgDAPIBugNIgBgEQgHgSgMgKQgNgLgRAAQgHAAgJADg");
	this.shape_9.setTransform(-57,4.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("AhTAtIgBgtIgBgjIgBgbIgCgnIAqgBIAAAXIABAWIAAAWIANgTQAIgKAKgJQAJgJALgHQAMgGANgCQAOgBANAHQAEADAGAFQAEAFAFAHQAEAJAEAKQACAKABAPIgkAOQAAgJgBgHIgEgLQgCgGgDgCIgGgGQgGgEgIAAQgFABgGADIgLAJIgMAOIgMAOIgLAPIgJAMIAAAbIABAZIABAVIABAPIgoAFIgCg6g");
	this.shape_10.setTransform(-77.6,4.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("AhTAtIgCgtIgBgjIgBgbIgBgnIArgBIAAAXIAAAWIAAAWIANgTQAIgKAJgJQAKgJALgHQALgGANgCQAPgBAMAHQAGADAEAFQAGAFAEAHQAEAJADAKQADAKACAPIglAOQAAgJgBgHIgEgLQgCgGgDgCIgGgGQgGgEgIAAQgFABgGADIgLAJIgMAOIgMAOIgLAPIgJAMIAAAbIABAZIABAVIABAPIgoAFIgCg6g");
	this.shape_11.setTransform(-98.1,4.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgmBkQgSgHgNgOQgNgPgIgUQgIgUAAgYQAAgXAIgVQAIgTANgPQANgOASgIQASgHAUgBQAUAAASAIQASAJAOAPQANAOAIAUQAIATAAAXQAAAXgIAUQgIAUgNAPQgOAOgSAIQgSAIgUAAQgUABgSgJgAgahFQgLAIgIAKQgHALgDAOQgEAOAAAMQAAANAEANQAEAOAHALQAHAKAMAIQALAGAOAAQAPAAAMgGQALgIAIgKQAHgLADgOQAEgNAAgNQAAgMgDgOQgEgOgHgLQgHgKgMgIQgLgGgQAAQgPAAgLAGg");
	this.shape_12.setTransform(-120.8,4.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AghBjQgTgHgQgPQgPgPgJgTQgJgUAAgXQAAgNADgNQAEgNAHgMQAHgMAKgLQAKgJAMgIQAMgHAOgEQAOgFAOAAQAQAAAPAFQAOADAMAIQANAHAKAKQAJALAHAOIAAAAIgeATIgBgBQgEgKgHgHQgHgIgJgFQgIgGgKgCQgKgDgKAAQgOAAgNAGQgOAGgJAKQgKAJgGAOQgGAOABANQgBAPAGANQAGAOAKAKQAJAKAOAGQANAFAOAAQAKAAAJgCQAKgDAIgFQAIgEAGgHQAHgGAGgJIAAgBIAgASIAAABQgIANgKAJQgKAJgMAIQgNAGgOAEQgOAEgPgBQgTABgUgJg");
	this.shape_13.setTransform(-143.5,4.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(3,1,1).p("A2Lj0MAsXAAAQCgAAAACgIAACpQAACgigAAMgsXAAAQigAAAAigIAAipQAAigCgAAg");
	this.shape_14.setTransform(-0.3,6.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("A2LD1QigAAAAigIAAipQAAigCgAAMAsXAAAQCgAAAACgIAACpQAACgigAAg");
	this.shape_15.setTransform(-0.3,6.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159.8,-30.6,319,62.7);


(lib.Tween28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(0,0,0.234,0.234);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.5,-36.8,85.1,73.7);


(lib.Tween27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.484,0.293,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.9,-23.5,77.9,47.2);


(lib.Tween26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(0,0,0.288,0.288);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.4,-39.4,82.9,78.9);


(lib.Tween25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.396,0.396);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.9,63.9,63.9);


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(0,0,0.285,0.285);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.7,-44.8,103.5,89.6);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(0,0,0.234,0.234);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.5,-36.8,85.1,73.7);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.592,0.358,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.7,-28.8,95.4,57.7);


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.484,0.293,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.9,-23.5,77.9,47.2);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(0,0,0.349,0.349);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.2,-47.7,100.4,95.6);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(0,0,0.288,0.288);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.4,-39.4,82.9,78.9);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.606,0.606);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-48.7,-48.7,97.6,97.6);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.396,0.396);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.9,63.9,63.9);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(38.3,38.3,0.476,0.476);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,76.6,76.6), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(49.7,47.3,0.346,0.346);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,99.5,94.7), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(46.7,28.3,0.581,0.351,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,93.5,56.6), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(51.1,44.2,0.281,0.281);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,102.1,88.4), null);


(lib.Symbol7copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AnkHlIAAvJIPJAAIAAPJg");
	this.shape.setTransform(49.8,49.8,1.026,1.026);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7copy, new cjs.Rectangle(0,0,99.6,99.6), null);


(lib.Symbol6copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(52.3,49.8,0.364,0.364);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6copy, new cjs.Rectangle(0,0,104.7,99.6), null);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(49.2,29.8,0.611,0.37,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy, new cjs.Rectangle(0,0,98.4,59.6), null);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(53.7,46.5,0.296,0.296);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy, new cjs.Rectangle(0,0,107.5,93.1), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("Ag/AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAAnQAFgHAGgHQAFgHAHgFQAHgGAGgDQAIgDAIgBQAKgBAIADQAIACAFAFQAGAEADAGQAEAGACAGIADANIABALQABAXgBAXIgBAxIgcgBIACgrQABgWgBgWIAAgGIgCgIIgDgJQgCgEgDgDIgIgFQgFgCgGABQgLACgKAOQgLAOgNAaIABAjIABAUIAAAKIgdAEIgCgqg");
	this.shape.setTransform(179.9,21.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_1.setTransform(164.6,21.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("AgeBJQgOgGgJgJQgKgKgFgOQgEgOAAgSQAAgQAFgOQAGgPAKgLQAKgLAOgGQANgHAPAAQAOAAAMAGQANAFAKALQAJAJAGANQAHANABAQIh0AQQABAKAEAHQADAIAHAGQAFAFAIADQAIACAIABQAHgBAHgBQAHgDAGgEQAGgFAEgGQAFgGACgJIAbAGQgFAMgGAKQgIAKgIAHQgKAHgLAEQgLAFgMAAQgRgBgOgFgAgKg0QgIACgGAFQgGAEgFAIQgGAIgCAMIBRgJIgBgEQgFgNgIgHQgJgIgOAAQgFAAgGACg");
	this.shape_2.setTransform(148.1,21.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_3.setTransform(133.2,18.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_4.setTransform(120,18.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("AAqBNIACgTQgRALgQAFQgOAFgOgBQgKAAgJgCQgJgDgHgFQgIgFgDgIQgFgIAAgLQAAgLADgJQAEgJAFgGQAGgGAHgEQAJgGAIgCQAJgEAKgBQAIgBAKAAIARAAIAOACIgFgNQgCgHgEgFQgEgFgGgDQgGgEgIAAIgLACQgHACgIADQgIAEgKAIQgJAHgMALIgQgUQANgMAMgIQAMgIALgEQAKgEAJgCQAIgCAIAAQAMAAAJAEQAKAEAIAIQAHAIAGAJQAEAKAEAMQADALABANQACALAAAMIgCAcQgBAPgDASgAgGAAQgLACgJAGQgIAFgEAIQgEAHACAKQACAJAGADQAFAEAIAAQAIAAAJgDIASgGIARgJIANgIIAAgOIAAgNIgPgDIgOgBQgNAAgKADg");
	this.shape_5.setTransform(104.4,21.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("AhKhtIAegBIAAAUQAIgIAIgFIAQgGIAPgDQAJAAAJADQAJACAIAGQAIAEAHAHQAHAGAFAJQAFAJADAJQACAJAAALQAAAMgEAKQgDALgFAHQgFAJgHAHQgGAGgIAFQgIAFgIADQgIACgIAAQgIAAgIgCIgRgGQgJgEgIgIIgBBcIgZABgAgPhRQgHADgGAFQgGAEgEAHQgFAGgCAHIAAAaQACAJAFAGQAEAHAGAEQAGAEAHADIAOACQAIABAJgEQAIgDAGgGQAGgGAEgKQAEgIAAgKQABgKgDgJQgDgJgGgIQgGgGgJgEQgIgEgKgBQgIABgHADg");
	this.shape_6.setTransform(87.4,25.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_7.setTransform(64.1,18.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("AgYBJQgOgGgMgLQgLgKgHgOQgGgPAAgRQAAgJACgKQADgJAGgJQAFgKAGgHQAIgIAJgEQAJgGAKgDQAKgDALAAQALAAALADQALADAIAFQAKAGAHAHQAHAIAFAJIAAABIgWAOIAAgBQgEgHgFgGQgFgFgGgEQgHgDgHgDQgHgCgHAAQgKAAgLAFQgJAEgIAHQgGAHgFALQgEAJAAAKQAAALAEAKQAFAKAGAHQAIAHAJAFQALAEAKAAQAHAAAGgCIAOgGQAFgDAGgFQAEgFAEgGIABgBIAXAOIAAABQgGAIgHAHQgIAHgJAGQgJAEgKAEQgLACgKAAQgPAAgOgGg");
	this.shape_8.setTransform(49.1,21.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgeBJQgOgGgJgJQgKgKgFgOQgEgOAAgSQAAgQAFgOQAGgPAKgLQAKgLAOgGQANgHAPAAQAOAAAMAGQANAFAKALQAJAJAGANQAHANABAQIh0AQQABAKAEAHQADAIAHAGQAFAFAIADQAIACAIABQAHgBAHgBQAHgDAGgEQAGgFAEgGQAFgGACgJIAaAGQgEAMgGAKQgIAKgJAHQgJAHgLAEQgLAFgMAAQgRgBgOgFgAgKg0QgIACgGAFQgGAEgFAIQgGAIgCAMIBRgJIgBgEQgFgNgIgHQgJgIgOAAQgFAAgGACg");
	this.shape_9.setTransform(32.5,21.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_10.setTransform(17.3,21.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_11.setTransform(2.2,21.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgbBKQgOgGgJgKQgKgLgGgOQgGgPAAgSQAAgRAGgPQAGgPAKgKQAJgKAOgGQANgGAOAAQAPAAANAHQAOAFAKALQAJAKAGAQQAGAOAAAQQAAARgGAPQgGAOgJALQgKALgOAGQgNAGgPAAQgOAAgNgGgAgTgyQgIAFgGAIQgFAIgCAKQgDAKAAAJQAAAJADAKQADALAFAHQAFAIAJAGQAIAEAKAAQALAAAJgEQAIgGAGgIQAFgHADgLQACgKAAgJQAAgJgCgKQgDgKgFgIQgFgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_12.setTransform(-14.5,21.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgYBJQgOgGgLgLQgMgKgGgOQgIgPAAgRQAAgJADgKQADgJAFgJQAGgKAHgHQAHgIAIgEQAJgGALgDQAKgDALAAQAMAAAKADQALADAJAFQAJAGAHAHQAHAIAFAJIABABIgXAOIgBgBQgDgHgFgGQgFgFgHgEQgFgDgHgDQgIgCgHAAQgLAAgJAFQgKAEgHAHQgIAHgEALQgEAJAAAKQAAALAEAKQAEAKAIAHQAHAHAKAFQAJAEALAAQAGAAAHgCIAOgGQAFgDAFgFQAFgFAEgGIAAgBIAYAOIAAABQgGAIgHAHQgHAHgKAGQgJAEgLAEQgKACgKAAQgPAAgOgGg");
	this.shape_13.setTransform(-31.1,21.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("AgeBJQgNgGgKgJQgKgKgFgOQgEgOAAgSQgBgQAGgOQAGgPAKgLQAKgLAOgGQANgHAPAAQAOAAANAGQAMAFAJALQALAJAFANQAHANACAQIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHgBAHgBQAHgDAGgEQAGgFAEgGQAFgGACgJIAbAGQgFAMgHAKQgHAKgIAHQgKAHgLAEQgLAFgMAAQgRgBgOgFgAgLg0QgGACgHAFQgGAEgFAIQgFAIgCAMIBQgJIAAgEQgFgNgKgHQgIgIgOAAQgFAAgHACg");
	this.shape_14.setTransform(-55.1,21.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#330000").s().p("AAkBnQAEgMACgNIADgWIAAgWQgBgPgEgJQgEgKgHgGQgGgHgIgCQgHgEgIAAQgHABgIAEQgGADgIAHQgHAGgIAMIAABaIgbABIgCjQIAfgBIgBBSQAIgIAJgFQAHgFAIgCQAJgDAGgBQAQABANAFQAMAFAJALQAJAJAFAPQAFAOABASIAAAUIgBATIgCATIgEAPg");
	this.shape_15.setTransform(-72.5,18.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#330000").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_16.setTransform(-88.3,18.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330000").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_17.setTransform(-108.8,18.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#330000").s().p("AgYBJQgOgGgLgLQgMgKgGgOQgIgPAAgRQAAgJADgKQADgJAFgJQAGgKAHgHQAHgIAIgEQAJgGALgDQAKgDALAAQAMAAAKADQALADAJAFQAJAGAHAHQAHAIAFAJIABABIgXAOIgBgBQgDgHgFgGQgFgFgHgEQgFgDgHgDQgIgCgHAAQgLAAgJAFQgKAEgHAHQgIAHgEALQgEAJAAAKQAAALAEAKQAEAKAIAHQAHAHAKAFQAJAEALAAQAGAAAHgCIAOgGQAFgDAFgFQAFgFAEgGIAAgBIAYAOIAAABQgGAIgHAHQgHAHgKAGQgJAEgLAEQgKACgKAAQgPAAgOgGg");
	this.shape_18.setTransform(-123.8,21.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#330000").s().p("AgeBJQgNgGgKgJQgKgKgEgOQgGgOAAgSQABgQAFgOQAGgPAKgLQAKgLANgGQAOgHAPAAQAOAAAMAGQANAFAKALQAJAJAHANQAGANACAQIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHgBAHgBQAIgDAFgEQAGgFAFgGQAEgGACgJIAaAGQgDAMgIAKQgGAKgKAHQgJAHgLAEQgLAFgMAAQgRgBgOgFgAgLg0QgGACgHAFQgGAEgFAIQgGAIgBAMIBRgJIgBgEQgGgNgJgHQgJgIgNAAQgFAAgHACg");
	this.shape_19.setTransform(-140.5,21.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#330000").s().p("AAJB3QgIgDgFgFQgFgFgEgHIgHgNQgHgPgBgUIAEisIAbAAIgBAtIgBAlIgBAdIAAAXIAAAkQAAAMACALIAEAIIAFAHIAIAGQAEABAGAAIgDAbQgKAAgHgCg");
	this.shape_20.setTransform(-151.9,16.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#330000").s().p("AgeBJQgNgGgKgJQgKgKgFgOQgEgOAAgSQgBgQAGgOQAGgPAKgLQAKgLAOgGQANgHAPAAQAOAAANAGQAMAFAJALQALAJAFANQAHANABAQIh0AQQABAKADAHQAFAIAGAGQAFAFAIADQAIACAIABQAHgBAHgBQAIgDAFgEQAGgFAEgGQAFgGACgJIAbAGQgFAMgGAKQgIAKgIAHQgKAHgLAEQgLAFgMAAQgRgBgOgFgAgKg0QgIACgGAFQgGAEgFAIQgFAIgDAMIBRgJIgBgEQgEgNgKgHQgIgIgOAAQgFAAgGACg");
	this.shape_21.setTransform(-164.8,21.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#330000").s().p("AgTBNIgLgCIgNgFIgNgFIgLgIIANgVIAPAHIAQAHIARAFQAIABAJAAQAHAAAFgBIAJgEIAFgFIADgFIAAgEQAAgDgBgDQgBgDgDgDIgIgEQgEgCgGgCIgOgCQgMAAgLgCQgMgCgIgEQgKgFgFgFQgGgHgBgLQgBgJACgIQACgJAFgGQAFgGAGgFQAHgFAHgDQAIgDAJgBQAJgCAIAAIAMAAIAPACIARAFQAHADAHAFIgJAZIgRgIIgOgDIgOgCQgVgCgLAGQgMAFAAAMQAAAIAFAEQAEADAHACIASACIAWADQANACAJAEQAJADAGAFQAFAGADAHQACAGAAAHQAAANgFAJQgGAJgIAFQgJAFgLADQgMADgMAAQgLAAgNgCg");
	this.shape_22.setTransform(-181,22.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#330000").s().p("AgeBhQgNgFgLgLQgKgKgFgPQgHgOABgTQAAgSAGgOQAHgOAKgKQAKgKANgFQANgFANAAQAJAAAJACQAIACAJAEQAJAFAJAIIABhGIAaACIgBDIIgcAAIAAgRIgIAGIgIAFIgJAEIgIACIgQADQgPAAgOgGgAgUgXQgIAEgHAHQgGAIgDAIQgDAKgBALQABAMADAJQADAKAGAHQAGAHAJAEQAJAEAKAAQAHAAAIgDQAJgDAHgFQAGgFAFgHQAFgHADgIIAAgdQgDgJgFgGQgFgHgGgFQgIgFgIgDQgIgDgIAAQgKAAgIAEg");
	this.shape_23.setTransform(152.5,-13.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#330000").s().p("Ag/AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAAnQAFgHAGgHQAFgHAHgFQAHgGAGgDQAIgDAIgBQAKgBAIADQAIACAFAFQAGAEADAGQAEAGACAGIADANIABALQABAXgBAXIgBAxIgcgBIACgrQABgWgBgWIAAgGIgCgIIgDgJQgCgEgDgDIgIgFQgFgCgGABQgLACgKAOQgLAOgNAaIABAjIABAUIAAAKIgdAEIgCgqg");
	this.shape_24.setTransform(135.2,-10.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#330000").s().p("AAqBNIACgTQgQALgRAFQgPAFgOgBQgJABgKgDQgJgDgHgFQgGgFgEgIQgFgIAAgLQAAgLADgJQAEgIAFgHQAGgGAHgFQAJgEAIgEQAJgDAKgBQAIgBAKAAIARABIAOABIgFgNQgCgGgEgGQgEgFgGgEQgGgDgIAAIgLACQgHABgIAFQgIADgKAIQgKAHgKALIgRgUQANgMAMgIQAMgHAKgFQALgFAJgBQAIgCAHAAQANAAAKAFQAJADAIAIQAHAHAGAKQAFALADALQADAMABAMQACALAAANIgCAbQgBAQgDARgAgGAAQgLACgJAFQgIAGgEAIQgEAIADAKQABAIAGADQAFADAIAAQAIAAAJgCIASgHIARgIIANgIIAAgOIgBgNIgNgDIgPgBQgMABgLACg");
	this.shape_25.setTransform(117.6,-11.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#330000").s().p("AgeBJQgNgGgKgKQgKgJgFgOQgEgPAAgRQgBgPAGgPQAGgPAKgLQAKgLAOgGQANgGAPgBQAOABANAFQAMAGAJAKQALAJAFANQAHAOACAPIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAFgHQAEgGACgJIAbAGQgFAMgHAKQgHAKgIAHQgKAIgLADQgLAFgMAAQgRAAgOgGgAgLg0QgGACgHAFQgGAFgFAHQgFAJgCALIBRgKIgBgDQgFgNgKgHQgIgIgOAAQgFAAgHACg");
	this.shape_26.setTransform(93.8,-10.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#330000").s().p("AgdBxIgMgEIgNgHQgGgFgFgGQgFgGgDgJIAagLQACAGAEAEIAIAHIAJAFIAJACQAKACAJgBQAKgCAHgEQAHgEAEgFQAGgEACgGIAGgKIACgJIAAgGIAAgSQgIAIgJAEQgIAEgIACQgIACgIABQgPAAgOgGQgNgFgLgLQgKgKgFgOQgHgOAAgTQABgSAGgPQAGgOALgKQAKgKANgFQANgFANAAQAJABAJADQAJACAIAFQAKAFAHAIIABgbIAaABIAACbQAAAJgCAIQgCAJgFAIQgFAJgFAHQgHAHgIAGQgJAFgKAEQgKAEgMABIgDAAQgOAAgNgEgAgUhSQgIAEgGAHQgGAHgDAKQgEAJAAALQAAAMAEAJQADAJAHAGQAFAHAJAEQAIADAKAAQAIAAAIgDQAJgDAHgFQAGgFAFgIQAFgIACgJIAAgSQgCgKgFgHQgEgIgHgGQgIgGgIgDQgJgDgIAAQgJAAgJAEg");
	this.shape_27.setTransform(75.7,-7.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330000").s().p("AAqBNIACgTQgRALgQAFQgOAFgOgBQgKABgJgDQgJgDgIgFQgHgFgDgIQgFgIAAgLQAAgLADgJQADgIAGgHQAGgGAHgFQAIgEAJgEQAJgDAKgBQAIgBAKAAIARABIAOABIgFgNQgCgGgEgGQgEgFgGgEQgGgDgIAAIgLACQgHABgIAFQgIADgKAIQgJAHgMALIgQgUQANgMAMgIQAMgHALgFQAKgFAJgBQAIgCAIAAQAMAAAJAFQAKADAIAIQAHAHAGAKQAEALAEALQADAMABAMQACALAAANIgCAbQgBAQgDARgAgGAAQgLACgJAFQgIAGgEAIQgEAIACAKQACAIAGADQAFADAIAAQAIAAAJgCIASgHIARgIIANgIIAAgOIgBgNIgOgDIgOgBQgNABgKACg");
	this.shape_28.setTransform(58,-11.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#330000").s().p("ABCAgIgBgdIAAgWQgBgLgDgFQgDgGgGABQgDgBgDADIgHAFIgHAHIgHAJIgFAIIgFAHIABANIAAAQIAAAVIAAAZIgYACIgBgrIgBgdIgCgWQAAgLgDgFQgEgGgEABQgEgBgEADIgHAGIgIAJIgGAJIgHAJIgFAHIACBHIgbACIgEiSIAcgDIABAoIAJgMIALgKQAGgFAGgDQAHgDAIAAQAFAAAGACQAFACADAEQAEADAEAHQADAGAAAHIAJgLIAKgKQAGgEAHgDQAGgDAHAAQAGAAAGACQAGACAFAFQADAEADAGQAEAIAAAJIABAZIACAfIAAAwIgcACIAAgrg");
	this.shape_29.setTransform(38.4,-11.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#330000").s().p("AgRBmIABgjIABgrIABg5IAbgBIgBAjIAAAdIgBAZIAAASIgBAdgAgHhAIgGgEIgEgGQgBgDAAgEQAAgFABgDQACgEACgDIAGgDQAEgCADAAQAEAAADACIAHADQACADACAEQABADAAAFQAAAEgBADIgEAGIgHAEQgDACgEAAQgDAAgEgCg");
	this.shape_30.setTransform(23.8,-14.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#330000").s().p("AgeBJQgNgGgKgKQgKgJgFgOQgEgPAAgRQgBgPAGgPQAGgPAKgLQAKgLAOgGQANgGAPgBQAOABANAFQAMAGAJAKQALAJAGANQAGAOACAPIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAFgHQAEgGACgJIAbAGQgFAMgHAKQgHAKgIAHQgKAIgLADQgLAFgMAAQgRAAgOgGgAgLg0QgGACgHAFQgGAFgFAHQgFAJgCALIBRgKIgBgDQgFgNgKgHQgIgIgOAAQgFAAgHACg");
	this.shape_31.setTransform(4.7,-10.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#330000").s().p("AAkBoQAEgOACgMIADgXIAAgVQgBgPgEgKQgEgJgHgHQgGgGgIgDQgHgDgIAAQgHABgIAEQgGADgIAHQgHAGgIANIAABaIgbAAIgCjQIAfgBIgBBSQAIgIAJgFQAHgFAIgCQAJgDAGgBQAQAAANAGQAMAFAJAKQAJALAFAOQAFAOABASIAAAUIgBATIgCATIgEAPg");
	this.shape_32.setTransform(-12.6,-14.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#330000").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_33.setTransform(-28.5,-13.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#330000").s().p("AgeBJQgOgGgJgKQgKgJgEgOQgGgPAAgRQABgPAFgPQAGgPAKgLQAKgLANgGQAOgGAPgBQAOABAMAFQANAGAKAKQAKAJAGANQAGAOABAPIh0AQQABAKAEAHQAEAIAFAGQAGAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAFgHQAEgGACgJIAaAGQgDAMgIAKQgHAKgJAHQgJAIgLADQgLAFgMAAQgRAAgOgGgAgLg0QgHACgGAFQgGAFgFAHQgFAJgDALIBSgKIgCgDQgEgNgJgHQgKgIgNAAQgFAAgHACg");
	this.shape_34.setTransform(-50.6,-10.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#330000").s().p("AgVBJIgviJIAbgEIAoB0IAqh5IAcAEIg1CPg");
	this.shape_35.setTransform(-66.6,-11.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#330000").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_36.setTransform(-81.1,-10.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#330000").s().p("AgeBJQgNgGgKgKQgKgJgEgOQgFgPgBgRQAAgPAGgPQAGgPAKgLQAKgLANgGQAOgGAPgBQAOABANAFQAMAGAJAKQAKAJAHANQAGAOACAPIh1AQQABAKADAHQAEAIAGAGQAGAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAFgHQAEgGACgJIAaAGQgDAMgIAKQgGAKgKAHQgJAIgLADQgLAFgMAAQgRAAgOgGgAgLg0QgGACgHAFQgGAFgFAHQgFAJgCALIBRgKIgBgDQgGgNgJgHQgJgIgNAAQgFAAgHACg");
	this.shape_37.setTransform(-97.6,-10.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#330000").s().p("AgTBNIgLgCIgNgFIgMgGIgMgHIANgVIAQAHIAPAHIARAEQAIACAIAAQAIAAAFgCIAJgDIAFgFIACgFIABgFQAAgDgCgCQAAgDgDgDIgIgEQgDgDgHgBIgPgCQgLAAgLgCQgMgCgJgEQgIgFgGgFQgGgHgBgKQgBgKACgIQADgJAEgGQAFgGAGgFQAHgFAIgDQAHgDAKgBQAIgCAIgBIAMABIAQADIAPAEQAJADAGAFIgJAZIgRgIIgOgDIgOgDQgUgBgMAGQgMAFAAAMQAAAIAFAEQAEAEAIACIASABIAUADQAOACAJAEQAJADAFAFQAHAGACAGQACAHAAAHQAAANgFAJQgFAIgJAHQgJAFgLADQgLACgMAAQgMABgNgDg");
	this.shape_38.setTransform(-113.8,-10.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#330000").s().p("AgMBoQgHgCgJgEQgJgEgJgIIAAASIgaAAIgCjQIAegBIgBBQQAIgIAJgEQAIgEAHgCIAQgDQAJAAAJADQAJADAIAFQAIAEAHAHQAHAHAFAIQAFAJADAIQACAKAAAKQAAAMgEALQgDAKgFAJQgFAIgHAHIgOAMQgIAEgIADQgIACgIAAQgIAAgJgCgAgPgRQgHADgGAEQgGAFgFAFQgEAGgCAHIgBAhQADAIAEAHQAFAGAGAFQAGAEAGACIAOADQAJAAAJgEQAJgEAGgGQAGgHAEgJQAEgJABgLQAAgKgDgJQgDgKgGgGQgGgHgJgFQgJgEgKAAQgIAAgHADg");
	this.shape_39.setTransform(-130,-14.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#330000").s().p("AgaBmQgNgFgMgGQgLgHgKgJQgJgKgHgMQgGgMgEgNQgEgOAAgOQAAgNAEgOQAEgNAGgMQAHgMAJgKQAKgJALgHQAMgHANgEQANgDANAAQAOAAANADQANAEAMAHQALAHAKAJQAJAKAHAMQAHAMADANQAEAOAAANQAAAOgEAOQgDANgHAMQgHAMgJAKQgKAJgLAHQgMAGgNAFQgNADgOAAQgNAAgNgDgAgchHQgOAGgKALQgLALgGANQgGAPAAAPQAAAQAGAPQAGANALALQAKALAOAGQANAGAPAAQALAAAJgCQAKgEAIgFQAJgEAHgHQAHgIAFgIQAFgJACgKQADgKAAgLQAAgKgDgKQgCgKgFgJQgFgIgHgHQgHgIgJgFQgIgEgKgEQgJgCgLAAQgPAAgNAGg");
	this.shape_40.setTransform(-150.9,-13.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#CC9900").ss(3,1,1).p("A9gmNMA7BAAAQCkAAAACkIAAHTQAACkikAAMg7BAAAQikAAAAikIAAnTQAAikCkAAg");
	this.shape_41.setTransform(0.4,4.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFCC00").s().p("A9gGOQikgBAAijIAAnTQAAikCkAAMA7BAAAQCkAAAACkIAAHTQAACjikABg");
	this.shape_42.setTransform(0.4,4.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-206.4,-36.9,413.6,82.8), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy5, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(597.4,371.4,0.705,0.705);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape_1.setTransform(423.8,362.7,0.416,0.416);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape_2.setTransform(573.1,172.7,0.512,0.512);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_3.setTransform(394.7,200.9,0.86,0.521,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_4.setTransform(495.3,276.1,0.866,0.866);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B8B2B2").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_5.setTransform(495.2,276.1,0.883,0.883);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333366").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_6.setTransform(495.2,276.1,0.95,0.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]}).wait(1));

	// Layer_1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_7.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(597.4,371.4,0.705,0.705);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape_1.setTransform(423.8,362.7,0.416,0.416);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape_2.setTransform(573.1,172.7,0.512,0.512);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_3.setTransform(394.7,200.9,0.86,0.521,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_4.setTransform(495.3,276.1,0.866,0.866);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B8B2B2").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_5.setTransform(495.2,276.1,0.883,0.883);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333366").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_6.setTransform(495.2,276.1,0.95,0.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]}).wait(1));

	// Layer_1
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("A8ScTQrvruAAwlQAAwkLvrvQLurtQkAAQQlAALuLtQLuLvABQkQgBQlruLuQruLvwlgBQwkABrurvg");
	this.shape_7.setTransform(495.3,276.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(239.1,19.9,512.3,512.3), null);


(lib.Tween33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(172.5,0,1,1,0,0,0,49.8,49.8);

	this.instance_1 = new lib.Symbol6copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(58.7,0,1,1,0,0,0,52.3,49.8);

	this.instance_2 = new lib.Symbol5copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-54.9,0,1,1,0,0,0,49.2,29.8);

	this.instance_3 = new lib.Symbol4copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-168.5,0,1,1,0,0,0,53.7,46.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape.setTransform(0,0,1.08,1.08);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_1.setTransform(0,0,1.08,1.08);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_2.setTransform(0,0,1.08,1.08);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_3.setTransform(0,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-249.8,-98.2,499.7,196.4);


(lib.Tween30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(-130.9,0,1,1,0,0,0,49.8,49.8);

	this.instance_1 = new lib.Symbol6copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-244.7,0,1,1,0,0,0,52.3,49.8);

	this.instance_2 = new lib.Symbol5copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-358.3,0,1,1,0,0,0,49.2,29.8);

	this.instance_3 = new lib.Symbol4copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-471.9,0,1,1,0,0,0,53.7,46.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("Aw6OpIQ69RIQ7dRg");
	this.shape.setTransform(473.8,0,0.509,0.509);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("Ao1CmQjqjqAAlLIY/AAQAAFLjqDqQjrDqlLAAQlKAAjrjqg");
	this.shape_1.setTransform(134.8,0,0.71,0.71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#46B0F7").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape_2.setTransform(360.9,0,0.37,0.37);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_3.setTransform(247.8,0,0.527,0.527);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_4.setTransform(303.5,0,1.08,1.08);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_5.setTransform(303.5,0,1.08,1.08);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_6.setTransform(303.5,0,1.08,1.08);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_7.setTransform(303.5,0,1.091,1.091);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_8.setTransform(-303.4,0,1.08,1.08);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_9.setTransform(-303.4,0,1.08,1.08);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_10.setTransform(-303.4,0,1.08,1.08);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_11.setTransform(-303.4,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-553.2,-98.2,1106.6,196.4);


(lib.Tween29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy();
	this.instance.parent = this;
	this.instance.setTransform(-130.9,0,1,1,0,0,0,49.8,49.8);

	this.instance_1 = new lib.Symbol6copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-244.7,0,1,1,0,0,0,52.3,49.8);

	this.instance_2 = new lib.Symbol5copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-358.3,0,1,1,0,0,0,49.2,29.8);

	this.instance_3 = new lib.Symbol4copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-471.9,0,1,1,0,0,0,53.7,46.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("Aw6OpIQ69RIQ7dRg");
	this.shape.setTransform(473.8,0,0.509,0.509);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("Ao1CmQjqjqAAlLIY/AAQAAFLjqDqQjrDqlLAAQlKAAjrjqg");
	this.shape_1.setTransform(134.8,0,0.71,0.71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#46B0F7").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape_2.setTransform(360.9,0,0.37,0.37);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_3.setTransform(247.8,0,0.527,0.527);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_4.setTransform(303.5,0,1.08,1.08);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_5.setTransform(303.5,0,1.08,1.08);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_6.setTransform(303.5,0,1.08,1.08);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_7.setTransform(303.5,0,1.091,1.091);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_8.setTransform(-303.4,0,1.08,1.08);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_9.setTransform(-303.4,0,1.08,1.08);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_10.setTransform(-303.4,0,1.08,1.08);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_11.setTransform(-303.4,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-553.2,-98.2,1106.6,196.4);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween32("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(158.5,30.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,0,319,62.7);


(lib.Symbol8copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(146.7,145,0.563,0.563,0,0,0,499.9,277.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy, new cjs.Rectangle(0,0,288.2,288.2), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy5();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy3();
	this.instance.parent = this;
	this.instance.setTransform(146.7,145,0.563,0.563,0,0,0,499.9,277.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(0,0,288.2,288.2), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(144.1,144.1,1,1,0,0,0,144.1,144.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0,0,288.2,288.2), null);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.Symbol8copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.Symbol14();
	this.instance_1.parent = this;
	this.instance_1.setTransform(144.1,144.1,1,1,0,0,0,144.1,144.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1.05,scaleY:1.05},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,288.2,288.2);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_21
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(335.1,588.5,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(285).to({_off:false},0).wait(47).to({startPosition:7},0).to({alpha:0,startPosition:16},9).wait(1));

	// Layer_23
	this.instance_1 = new lib.Tween33("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(337.2,588.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(265).to({_off:false},0).to({regY:0.1,scaleX:1.16,scaleY:1.16},20).to({startPosition:0},47).to({alpha:0},9).wait(1));

	// Layer_7
	this.instance_2 = new lib.Symbol7copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(509.7,588.1,1,1,0,0,0,49.8,49.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(242).to({_off:false},0).to({regX:49.9,regY:49.9,scaleX:0.66,scaleY:0.66,x:698,y:378.9,alpha:0},23).to({_off:true},1).wait(76));

	// Layer_6
	this.instance_3 = new lib.Symbol6copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(395.9,588.1,1,1,0,0,0,52.3,49.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(242).to({_off:false},0).to({regX:52.4,regY:49.9,scaleX:0.81,scaleY:0.81,x:683.1,y:267,alpha:0},23).to({_off:true},1).wait(76));

	// Layer_5
	this.instance_4 = new lib.Symbol5copy();
	this.instance_4.parent = this;
	this.instance_4.setTransform(282.3,588.1,1,1,0,0,0,49.2,29.8);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(242).to({_off:false},0).to({regX:49.3,regY:29.9,scaleX:0.81,scaleY:0.81,x:583.4,y:284,alpha:0},23).to({_off:true},1).wait(76));

	// Layer_4
	this.instance_5 = new lib.Symbol4copy();
	this.instance_5.parent = this;
	this.instance_5.setTransform(168.7,588.1,1,1,0,0,0,53.7,46.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(242).to({_off:false},0).to({regY:46.6,scaleX:0.81,scaleY:0.81,x:600.5,y:375.5,alpha:0},23).to({_off:true},1).wait(76));

	// Layer_18
	this.instance_6 = new lib.handanim("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(354.2,657.7,1,1,0,0,0,41,60.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(212).to({_off:false},0).to({_off:true},30).wait(100));

	// Layer_17
	this.instance_7 = new lib.arrowanim("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(341.2,437.7,1,1,0,0,0,41,60.1);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(181).to({_off:false},0).to({_off:true},31).wait(130));

	// Layer_16
	this.instance_8 = new lib.Tween25("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(697.5,379.7);
	this.instance_8._off = true;

	this.instance_9 = new lib.Symbol12();
	this.instance_9.parent = this;
	this.instance_9.setTransform(1117.5,597.2,1,1,0,0,0,38.3,38.3);
	this.instance_9.alpha = 0.5;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(115).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,x:1113.6,y:593.1,alpha:0.801},19).to({_off:true},40).wait(168));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(174).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// VAL 2
	this.instance_10 = new lib.Tween26("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(683.8,267.9);
	this.instance_10._off = true;

	this.instance_11 = new lib.Symbol11();
	this.instance_11.parent = this;
	this.instance_11.setTransform(999.8,580.1,1,1,0,0,0,49.7,47.3);
	this.instance_11.alpha = 0.5;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(115).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,x:995.9,y:579.9,alpha:0.801},19).to({_off:true},40).wait(168));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(174).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_14
	this.instance_12 = new lib.Tween27("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(583.5,283.8);
	this.instance_12._off = true;

	this.instance_13 = new lib.Symbol10();
	this.instance_13.parent = this;
	this.instance_13.setTransform(887.6,590.3,1,1,0,0,0,46.8,28.3);
	this.instance_13.alpha = 0.5;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(115).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,x:889.6,y:589.1,alpha:0.801},19).to({_off:true},40).wait(168));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(174).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_15
	this.instance_14 = new lib.Tween28("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(599.9,374.8);
	this.instance_14._off = true;

	this.instance_15 = new lib.Symbol9();
	this.instance_15.parent = this;
	this.instance_15.setTransform(771.9,585,1,1,0,0,0,51.1,44.2);
	this.instance_15.alpha = 0.5;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(115).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,x:771.9,y:580.9,alpha:0.801},19).to({_off:true},40).wait(168));
	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(174).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_13
	this.instance_16 = new lib.Tween17("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(697.5,379.7);
	this.instance_16._off = true;

	this.instance_17 = new lib.Tween18("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(509.2,588);
	this.instance_17.alpha = 0.801;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(115).to({_off:false},0).to({_off:true,x:509.2,y:588,alpha:0.801},19).wait(208));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(115).to({_off:false},19).wait(40).to({alpha:0.5},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_11
	this.instance_18 = new lib.Tween19("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(683.8,267.9);
	this.instance_18._off = true;

	this.instance_19 = new lib.Tween20("synched",0);
	this.instance_19.parent = this;
	this.instance_19.setTransform(395.7,588.2);
	this.instance_19.alpha = 0.801;
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(115).to({_off:false},0).to({_off:true,x:395.7,y:588.2,alpha:0.801},19).wait(208));
	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(115).to({_off:false},19).wait(40).to({alpha:0.5},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// VAL 1
	this.instance_20 = new lib.Tween21("synched",0);
	this.instance_20.parent = this;
	this.instance_20.setTransform(583.5,283.8);
	this.instance_20._off = true;

	this.instance_21 = new lib.Tween22("synched",0);
	this.instance_21.parent = this;
	this.instance_21.setTransform(281.4,587.8);
	this.instance_21.alpha = 0.801;
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(115).to({_off:false},0).to({_off:true,x:281.4,y:587.8,alpha:0.801},19).wait(208));
	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(115).to({_off:false},19).wait(40).to({alpha:0.5},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_12
	this.instance_22 = new lib.Tween23("synched",0);
	this.instance_22.parent = this;
	this.instance_22.setTransform(599.9,374.8);
	this.instance_22._off = true;

	this.instance_23 = new lib.Tween24("synched",0);
	this.instance_23.parent = this;
	this.instance_23.setTransform(167.6,588.9);
	this.instance_23.alpha = 0.801;
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(115).to({_off:false},0).to({_off:true,x:167.6,y:588.9,alpha:0.801},19).wait(208));
	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(115).to({_off:false},19).wait(40).to({alpha:0.5},0).to({alpha:0},6).to({_off:true},1).wait(161));

	// Layer_8
	this.instance_24 = new lib.Symbol13("synched",0);
	this.instance_24.parent = this;
	this.instance_24.setTransform(740,141.2,1,1,0,0,0,158.1,31.4);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(84).to({_off:false},0).wait(90).to({startPosition:3},0).to({alpha:0,startPosition:10},6).to({_off:true},1).wait(161));

	// Layer_3
	this.instance_25 = new lib.Tween29("synched",0);
	this.instance_25.parent = this;
	this.instance_25.setTransform(640.6,588.1);
	this.instance_25.alpha = 0;
	this.instance_25._off = true;

	this.instance_26 = new lib.Tween30("synched",0);
	this.instance_26.parent = this;
	this.instance_26.setTransform(640.6,588.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("Aw6OpIQ69RIQ7dRg");
	this.shape.setTransform(1114.4,588,0.509,0.509);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("Ao1CmQjqjqAAlLIY/AAQAAFLjqDqQjrDqlLAAQlKAAjrjqg");
	this.shape_1.setTransform(775.4,588,0.71,0.71);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#46B0F7").s().p("AlpFhIw/gcINfqUIk2wSIN/JoIOApoIk2QSINfKUIw/AcIlqQBg");
	this.shape_2.setTransform(1001.5,588,0.37,0.37);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#46B0F7").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_3.setTransform(888.4,588,0.527,0.527);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_4.setTransform(944.1,588.1,1.08,1.08);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_5.setTransform(944.1,588.1,1.08,1.08);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_6.setTransform(944.1,588.1,1.08,1.08);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_7.setTransform(944.1,588,1.091,1.091);

	this.instance_27 = new lib.Tween34("synched",0);
	this.instance_27.parent = this;
	this.instance_27.setTransform(944.1,588.1);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_25}]},71).to({state:[{t:this.instance_26}]},7).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},186).to({state:[{t:this.instance_27}]},68).to({state:[{t:this.instance_27}]},9).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(71).to({_off:false},0).to({_off:true,alpha:1},7).wait(264));
	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(332).to({_off:false},0).to({alpha:0},9).wait(1));

	// Layer_22
	this.instance_28 = new lib.Symbol8copy_1("synched",0);
	this.instance_28.parent = this;
	this.instance_28.setTransform(640.1,326.2,1.05,1.05,0,0,0,144.2,144.2);
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(84).to({_off:false},0).to({_off:true},97).wait(161));

	// Layer_1
	this.instance_29 = new lib.Symbol8copy();
	this.instance_29.parent = this;
	this.instance_29.setTransform(640.1,326.1,1,1,0,0,0,144.1,144.1);
	this.instance_29.alpha = 0;
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(44).to({_off:false},0).to({alpha:1},7).wait(281).to({alpha:0},9).wait(1));

	// Layer_2
	this.instance_30 = new lib.Symbol1();
	this.instance_30.parent = this;
	this.instance_30.setTransform(639.5,117.6,1.315,1.315);
	this.instance_30.alpha = 0;
	this.instance_30._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(19).to({_off:false},0).to({alpha:1},6).wait(307).to({alpha:0},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(495,271.3,1576,904);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;