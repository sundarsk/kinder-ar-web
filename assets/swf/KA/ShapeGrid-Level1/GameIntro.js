(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000066").s().p("Ag0B/QgYgKgRgRQgRgRgIgZQgJgYAAgeQAAgdAKgZQAKgaASgSQARgUAYgKQAXgLAbAAQAYAAAWAJQAWAKARARQAQAQALAYQALAWADAbIjMAeQACARAHANQAGANALAKQAKAIAOAFQANAFAPABQAMAAANgFQAMgDAKgIQALgHAIgLQAHgLAEgPIAtAJQgGAWgMARQgNARgQANQgQAMgTAIQgTAGgVAAQgeAAgYgJgAgThbQgMADgLAJQgLAIgJAOQgJAOgDAUICNgRIgBgFQgJgXgQgOQgQgNgXABQgJAAgMADg");
	this.shape.setTransform(162.4,6.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000066").s().p("AiBi/IA0gCIgBAiQAOgNAPgHQAOgHAMgEQAPgEANgBQAQAAAPAEQAQAFAOAIQAPAJAMALQAMAMAIAPQAJAOAEARQAFARgBASQAAAVgGASQgFARgJAPQgJAOgMAMQgMANgNAIQgNAIgPAFQgOAEgOAAQgOgBgOgDQgNgEgQgHQgQgHgPgNIgBCgIgsABgAAAiTQgOAAgNAFQgNAFgKAIQgKAJgHALQgIALgDAMIgBAtQAEAPAHAMQAIAMAKAHQAKAIANAEQAMAEAMAAQAPABAPgHQAOgGALgKQALgMAHgPQAGgPABgRQABgSgFgPQgGgQgKgMQgLgMgOgIQgOgGgQAAIgCAAg");
	this.shape_1.setTransform(133,12.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("ABJCHIAEgiQgeATgbAIQgbAIgYAAQgRAAgPgEQgRgEgMgJQgMgKgHgOQgIgOAAgTQAAgTAFgPQAHgPAJgMQAKgKAOgJQANgIAQgFQAPgFARgDQAQgCARAAIAdABIAXADIgHgXQgFgMgGgJQgHgJgLgGQgKgFgOAAQgJAAgLADQgMACgOAHQgOAHgQANQgRAMgTAUIgdgjQAXgVAUgOQAVgNATgIQARgIARgDQAOgCANAAQAVAAARAHQARAHANANQANANAJASQAJARAFAUQAGAUADAVQACAVAAAVQAAAXgDAaQgCAagGAegAgKgBQgUAEgPAKQgPAJgGAOQgIAOAEARQADAOALAGQAJAGANAAQAOAAAQgFQAPgFAQgGQAPgHAOgIIAYgNIABgYQAAgLgBgNQgMgCgNgCQgNgDgNAAQgVAAgSAFg");
	this.shape_2.setTransform(100.9,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000066").s().p("ABAC0QAGgXADgVQADgWABgRQACgUgBgSQgCgZgHgSQgIgQgKgMQgLgLgNgFQgOgFgMAAQgNABgNAHQgNAGgNAMQgNALgMAVIgBCdIguABIgFlsIA2gCIgBCQQANgOAPgIQAOgIANgFQAPgFANgBQAaAAAXAKQAVAJAPASQAQARAKAaQAJAYAAAgIAAAiIgBAiIgEAgQgCAPgEAMg");
	this.shape_3.setTransform(70.9,0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000066").s().p("AgiCFIgTgEIgWgIQgLgEgLgFQgLgGgKgIIAYgkIAaANQANAHAOAEIAdAIQAOADARAAQAMAAAJgDQAKgCAFgEQAGgEADgEQADgFABgEQACgEgBgEQAAgFgCgEQgCgGgFgEQgEgFgIgDQgHgEgLgCQgLgCgPgBQgVgBgTgEQgTgDgQgHQgPgIgKgKQgLgMgCgTQgCgRAFgOQADgNAJgMQAHgLALgIQAMgJAOgFQAOgFAPgDQAPgDAPAAIAVABIAbAEQAOADAOAFQANAFANAJIgRArQgQgIgNgEQgOgFgLgDQgOgDgLgBQgjgCgVAKQgVAKAAAUQAAAOAIAHQAIAGANADQANADASAAQASACAUADQAXADAQAHQAQAHAKAJQAJAKAEALQAFAMAAAMQgBAWgIAPQgKAPgPALQgPAJgUAEQgTAFgWABQgUAAgXgFg");
	this.shape_4.setTransform(41.1,6.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000066").s().p("AgzDFQgLgDgLgFQgLgFgLgHQgKgIgJgLQgJgLgFgOIAugUQAEAKAGAHQAHAIAHAFQAHAFAIADIAPAEQARAEASgCQARgEAMgHQAMgGAIgJQAJgIAGgJIAIgSQADgJABgHIABgLIAAgfQgOAOgQAHQgPAHgNAEQgPAEgNAAQgcAAgYgJQgXgKgRgSQgSgSgKgYQgKgaAAggQAAggALgZQALgZATgRQARgRAXgJQAXgKAXAAQAPACAQAFQAOAFAQAIQARAIANAPIABgvIAuABIgBEOQgBAQgEAPQgDAPgIAPQgHAOgLANQgLAMgPAKQgOAKgRAGQgSAGgVACIgGAAQgZAAgWgGgAgkiPQgNAHgLAMQgKAMgGARQgGARAAATQAAAUAGAQQAGAQAKALQALAMAOAGQAPAHASAAQAOAAAPgGQAOgFAMgKQANgJAHgNQAJgOADgQIAAgfQgCgRgJgOQgJgNgMgKQgMgKgPgGQgQgFgOAAQgRAAgPAHg");
	this.shape_5.setTransform(-1.6,11.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000066").s().p("AhuA6IgCg6IgBguIgBgiIgCgyIA2gBIABBEQAIgNAKgMQAKgMALgJQAMgKAMgFQANgGAOgBQASgBAOAEQANAEAJAIQAKAIAGAKQAGAKAEALQADALACALIACAUQABAngBAqIgBBVIgxgCIAEhMQABgmgCgmIAAgLIgDgOIgFgPQgEgHgGgGQgFgFgIgDQgIgDgLACQgTACgSAZQgTAYgXAuIACA9IABAiIABASIgzAHIgDhKg");
	this.shape_6.setTransform(-31.8,6.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000066").s().p("AgeCyIACg9IAChKIABhlIAvgBIgBA9IgBAzIgBApIAAAgIgBA0gAgMhwQgGgCgFgFQgEgEgDgHQgCgGgBgHQABgHACgGQADgGAEgEQAFgFAGgDQAGgDAGAAQAHAAAGADQAGADAFAFQAEAEADAGQADAGAAAHQAAAHgDAGQgDAHgEAEQgFAFgGACQgGADgHAAQgGAAgGgDg");
	this.shape_7.setTransform(-52.3,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000066").s().p("AgiCFIgTgEIgWgIQgLgEgLgFQgLgGgKgIIAXgkIAbANQANAHAOAEIAdAIQAOADARAAQAMAAAJgDQAKgCAFgEQAGgEADgEQADgFABgEQACgEgBgEQAAgFgCgEQgCgGgFgEQgEgFgIgDQgHgEgLgCQgLgCgPgBQgVgBgTgEQgTgDgQgHQgPgIgKgKQgLgMgCgTQgCgRAFgOQADgNAIgMQAIgLALgIQAMgJAOgFQAOgFAPgDQAPgDAPAAIAVABIAbAEQAOADANAFQAOAFANAJIgRArQgQgIgNgEQgOgFgMgDQgNgDgLgBQgjgCgVAKQgVAKAAAUQAAAOAIAHQAIAGANADQANADASAAQASACAUADQAXADAQAHQAQAHAKAJQAJAKAEALQAFAMAAAMQgBAWgIAPQgKAPgPALQgPAJgUAEQgTAFgWABQgUAAgXgFg");
	this.shape_8.setTransform(-72.3,6.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000066").s().p("AgiCFIgUgEIgVgIQgLgEgLgFQgKgGgLgIIAYgkIAaANQAOAHAOAEIAdAIQANADAQAAQANAAAKgDQAIgCAGgEQAGgEADgEQADgFACgEQABgEgBgEQAAgFgBgEQgDgGgFgEQgEgFgIgDQgIgEgKgCQgLgCgPgBQgUgBgUgEQgUgDgPgHQgPgIgLgKQgJgMgDgTQgCgRAEgOQAFgNAIgMQAHgLAMgIQAMgJANgFQANgFAQgDQAPgDAOAAIAXABIAaAEQAOADAOAFQANAFANAJIgRArQgQgIgOgEQgOgFgKgDQgNgDgMgBQgjgCgVAKQgVAKAAAUQAAAOAIAHQAHAGAOADQAOADARAAQARACAVADQAXADAQAHQAQAHAJAJQAKAKAEALQAEAMAAAMQAAAWgJAPQgIAPgQALQgPAJgTAEQgVAFgVABQgUAAgXgFg");
	this.shape_9.setTransform(-99.2,6.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000066").s().p("AgeCyIACg9IAChKIABhlIAvgBIgBA9IgBAzIAAApIgBAgIgBA0gAgMhwQgGgCgFgFQgEgEgDgHQgCgGgBgHQABgHACgGQADgGAEgEQAFgFAGgDQAGgDAGAAQAHAAAGADQAGADAFAFQAEAEADAGQACAGAAAHQAAAHgCAGQgDAHgEAEQgFAFgGACQgGADgHAAQgGAAgGgDg");
	this.shape_10.setTransform(-118.1,0.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000066").s().p("ABxA4IAAgzIgBglQgBgUgFgJQgGgKgIAAQgGAAgHAEQgGADgFAGIgMANIgMAPIgKAPIgIAMIABAWIACAcIAAAkIAAAsIgtADIgBhKIgCgzIgCglQgBgUgFgJQgGgKgJAAQgGAAgHAEQgFAEgHAHIgNAOIgMARIgKAPIgJANIACB8IgvADIgGj9IAxgGIABBFIAQgTQAJgLAKgIQAJgIALgFQAMgGAOAAQAKAAAJAEQAJADAHAGQAIAHAEAKQAGAKABAPIAQgTQAIgKAKgIQAJgIALgFQALgFAOAAQAKAAAKAEQAKADAIAIQAHAHAFAMQAFAMABAQIACArIACA4IABBTIgyADIAAhKg");
	this.shape_11.setTransform(-144.1,5.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(3,1,1).p("A4xknMAxjAAAQCgAAAACgIAAEPQAACgigAAMgxjAAAQigAAAAigIAAkPQAAigCgAAg");
	this.shape_12.setTransform(5.2,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("A4xEoQigAAAAigIAAkPQAAigCgAAMAxjAAAQCgAAAACgIAAEPQAACgigAAg");
	this.shape_13.setTransform(5.2,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-181.7,-37.9,363.5,76.6);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("Ag0B/QgYgJgRgSQgRgRgIgZQgJgYAAgeQAAgdAKgZQAKgZASgUQARgTAYgKQAXgLAbAAQAYAAAWAKQAWAJARARQAQAQALAYQALAXADAaIjMAeQACARAHANQAGANALAKQAKAIAOAFQANAGAPAAQAMAAANgFQAMgEAKgHQALgHAIgMQAHgLAEgOIAtAJQgGAVgMASQgNARgQANQgQAMgTAHQgTAHgVAAQgeAAgYgJgAgThbQgMADgLAJQgLAIgJAOQgJAOgDAUICNgRIgBgEQgJgYgQgOQgQgNgXAAQgJABgMADg");
	this.shape.setTransform(218.5,5.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AiBi/IA0gCIgBAiQAOgNAPgHQAOgHAMgEQAPgEANgBQAQAAAPAEQAQAFAOAIQAPAJAMALQAMAMAIAPQAJAOAEARQAFARgBASQAAAVgGASQgFARgJAPQgJAOgMAMQgMANgNAIQgNAIgPAFQgOAEgOAAQgOgBgOgDQgNgEgQgHQgQgHgPgNIgBCgIgsABgAAAiTQgOAAgNAFQgNAFgKAIQgKAJgHALQgIALgDAMIgBAtQAEAPAHAMQAIAMAKAHQAKAIANAEQAMAEAMAAQAPABAPgHQAOgGALgKQALgMAHgPQAGgPABgRQABgSgFgPQgGgQgKgMQgLgMgOgIQgOgGgQAAIgCAAg");
	this.shape_1.setTransform(189.2,11.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("ABJCHIADgiQgdATgbAIQgbAIgYAAQgRAAgQgEQgQgEgMgJQgMgKgHgOQgIgOAAgTQAAgTAGgPQAFgPALgMQAJgKANgJQAOgIAPgFQAQgFARgDQAQgCARAAIAdABIAYADIgIgXQgFgMgGgJQgHgJgKgGQgKgFgOAAQgKAAgKADQgMACgOAHQgPAHgRANQgQAMgUAUIgcgjQAWgVAWgOQAUgNASgIQATgIAPgDQAPgCAMAAQAXAAAQAHQASAHANANQAMANAJASQAJARAGAUQAFAUADAVQACAVAAAVQAAAXgCAaQgDAagFAegAgLgBQgTAEgPAKQgOAJgHAOQgIAOAFARQACAOAKAGQAKAGAOAAQANAAAQgFQAPgFAPgGQARgHANgIIAYgNIAAgYQABgLgCgNQgLgCgNgCQgMgDgNAAQgVAAgUAFg");
	this.shape_2.setTransform(157.1,4.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("AA/C0QAHgXADgVQADgWABgRQABgUAAgSQgBgZgIgRQgHgSgLgLQgMgLgNgFQgNgFgNAAQgMABgOAHQgMAGgNAMQgNAKgMAWIgCCdIguABIgDlrIA2gDIgCCQQANgOAPgIQAOgIANgFQAPgFANgBQAbAAAVAKQAWAJAQASQAQARAIAaQAJAYABAgIAAAiIgBAjIgDAfQgDAPgEAMg");
	this.shape_3.setTransform(127.1,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AgiCGIgUgFIgVgIQgLgDgLgHQgKgFgKgIIAWgkIAbANQANAGAOAFIAeAIQAOADAPAAQANAAAKgDQAIgCAGgDQAGgFADgEQAEgFABgEQABgDAAgFQAAgFgCgEQgDgGgEgEQgGgFgHgDQgIgEgLgCQgKgDgPAAQgVgBgTgEQgTgDgQgIQgQgHgKgKQgJgNgDgRQgBgSADgOQAEgOAIgLQAIgLAMgIQALgIAOgGQAOgFAPgDQAPgDAOAAIAWABIAaAEQAOADAOAFQAOAFAMAJIgQArQgQgIgOgEQgOgFgLgDQgNgDgKgBQglgCgUAKQgVAKAAAUQAAAOAIAHQAIAGANADQANADATAAQAQACAVADQAXAEAQAGQAPAHAKAJQAKAJAFAMQADAMAAAMQABAWgKAPQgIAPgQALQgPAJgTAEQgVAFgVABQgVAAgWgEg");
	this.shape_4.setTransform(97.3,6.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("Ag0B/QgYgJgRgSQgRgRgIgZQgJgYAAgeQAAgdAKgZQAKgZASgUQARgTAYgKQAXgLAbAAQAYAAAWAKQAWAJARARQAQAQALAYQALAXADAaIjMAeQACARAHANQAGANALAKQAKAIAOAFQANAGAPAAQAMAAANgFQAMgEAKgHQALgHAIgMQAHgLAEgOIAtAJQgGAVgMASQgNARgQANQgQAMgTAHQgTAHgVAAQgeAAgYgJgAgThbQgMADgLAJQgLAIgJAOQgJAOgDAUICNgRIgBgEQgJgYgQgOQgQgNgXAAQgJABgMADg");
	this.shape_5.setTransform(57.2,5.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("AA/C0QAHgXADgVQADgWABgRQABgUAAgSQgBgZgIgRQgHgSgLgLQgMgLgNgFQgNgFgNAAQgMABgOAHQgMAGgNAMQgNAKgMAWIgCCdIguABIgDlrIA2gDIgCCQQANgOAPgIQAOgIANgFQAPgFANgBQAbAAAVAKQAWAJAQASQAQARAIAaQAJAYABAgIAAAiIgBAjIgDAfQgDAPgEAMg");
	this.shape_6.setTransform(27.2,-0.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgcgbIhSACIABgrIBSgDIAChnIAugDIgCBpIBcgCIgEAsIhYACIgCDNIgxABg");
	this.shape_7.setTransform(-0.4,0.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("Ag0B/QgYgJgRgSQgRgRgIgZQgJgYAAgeQAAgdAKgZQAKgZASgUQARgTAYgKQAXgLAbAAQAYAAAWAKQAWAJARARQAQAQALAYQALAXADAaIjMAeQACARAHANQAGANALAKQAKAIAOAFQANAGAPAAQAMAAANgFQAMgEAKgHQALgHAIgMQAHgLAEgOIAtAJQgGAVgMASQgNARgQANQgQAMgTAHQgTAHgVAAQgeAAgYgJgAgThbQgMADgLAJQgLAIgJAOQgJAOgDAUICNgRIgBgEQgJgYgQgOQgQgNgXAAQgJABgMADg");
	this.shape_8.setTransform(-38.7,5.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AglB/IhTjuIAwgHIBGDKIBLjTIAwAHIhcD4g");
	this.shape_9.setTransform(-66.4,5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("AhqA6IgCg6IgCguIgBgiIgBgyIA1gBIABAeIAAAbIAAAdIARgYQAKgNAMgMQANgLAOgJQAOgJARgCQATgBAPAJQAHAEAHAGQAGAHAGAKQAFAJAEAOQAEANABATIguARIgCgUIgFgOIgGgLIgHgGQgIgGgLAAQgGABgIAFQgHAEgHAIIgQAQIgPATIgOASIgMARIABAjIABAgIABAaIABATIgzAHIgChKg");
	this.shape_10.setTransform(-91.7,5.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("Ag0B/QgYgJgRgSQgRgRgIgZQgJgYAAgeQAAgdAKgZQAKgZASgUQARgTAYgKQAXgLAbAAQAYAAAWAKQAWAJARARQAQAQALAYQALAXADAaIjMAeQACARAHANQAGANALAKQAKAIAOAFQANAGAPAAQAMAAANgFQAMgEAKgHQALgHAIgMQAHgLAEgOIAtAJQgGAVgMASQgNARgQANQgQAMgTAHQgTAHgVAAQgeAAgYgJgAgThbQgMADgLAJQgLAIgJAOQgJAOgDAUICNgRIgBgEQgJgYgQgOQgQgNgXAAQgJABgMADg");
	this.shape_11.setTransform(-120.4,5.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgiCGIgUgFIgVgIQgLgDgLgHQgKgFgKgIIAWgkIAbANQANAGAOAFIAeAIQAOADAPAAQANAAAKgDQAIgCAGgDQAGgFADgEQAEgFABgEQABgDAAgFQAAgFgCgEQgDgGgEgEQgGgFgHgDQgIgEgLgCQgKgDgPAAQgVgBgTgEQgTgDgQgIQgQgHgKgKQgJgNgDgRQgBgSADgOQAEgOAIgLQAIgLAMgIQALgIAOgGQAOgFAPgDQAPgDAOAAIAWABIAaAEQAOADAOAFQAOAFAMAJIgQArQgQgIgOgEQgOgFgLgDQgNgDgKgBQglgCgUAKQgVAKAAAUQAAAOAIAHQAIAGANADQANADATAAQAQACAVADQAXAEAQAGQAPAHAKAJQAKAJAFAMQADAMAAAMQABAWgKAPQgIAPgQALQgPAJgTAEQgVAFgVABQgVAAgWgEg");
	this.shape_12.setTransform(-148.4,6.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgVC0QgOgDgPgHQgQgIgQgNIAAAfIgrABIgElrIAzgCIgBCLQAOgNAOgHQAPgIANgCQAOgEANgBQAQAAAQAFQAQAEAOAIQAOAJAMAMQAMALAJAPQAIAPAFAQQAEAQAAATQgBAUgFASQgGARgJAQQgJAOgLANQgMAMgNAIQgOAIgOAFQgPAEgOAAQgNAAgPgFgAgbgeQgNAFgKAHQgLAJgHAJQgIALgEAMIgBA4QAFAPAHALQAIAMALAHQAKAIAMAEQAMADAMABQAQAAAPgGQAPgHALgMQAMgLAGgQQAHgQABgSQABgRgFgRQgGgQgLgMQgKgNgQgHQgPgHgRAAQgOAAgNAFg");
	this.shape_13.setTransform(-176.5,-0.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("AgvCwQgWgGgUgMQgUgMgQgRQgRgQgMgVQgLgUgHgYQgFgXAAgZQAAgYAFgYQAHgXALgVQAMgTARgRQAQgRAUgMQAUgMAWgGQAXgHAYAAQAYAAAYAHQAWAGAUAMQAUAMARARQAQARAMATQALAVAHAXQAFAYAAAYQAAAZgFAXQgHAYgLAUQgMAVgQAQQgRARgUAMQgUAMgWAGQgYAHgYAAQgYAAgXgHgAgyh8QgYALgSASQgSATgKAZQgLAYAAAbQAAAcALAZQAKAYASATQASASAYAMQAYAKAaAAQASAAARgFQARgFAOgIQAPgKAMgMQAMgMAJgPQAIgPAFgRQAFgSAAgTQAAgSgFgRQgFgRgIgQQgJgPgMgMQgMgMgPgJQgOgJgRgFQgRgFgSAAQgaAAgYALg");
	this.shape_14.setTransform(-212.7,0.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(3,1,1).p("EgiAgF3MBECAAAQCeAAAACfIAAGxQAACfieAAMhECAAAQifAAAAifIAAmxQAAifCfAAg");
	this.shape_15.setTransform(0,1.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("EgiAAF4QifAAAAifIAAmxQAAifCfAAMBECAAAQCeAAAACfIAAGxQAACfieAAg");
	this.shape_16.setTransform(0,1.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-235.9,-38.7,471.9,79);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#350000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape.setTransform(19.2,25.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(0,0,38.3,51), null);


(lib.Shapesqustcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhIAAQiIwiAAIwhAAIAAwiIAAwhIQhAAIAAQhIAAQiAAAAAIQiAAAwhAAIQhAA");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#350000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape_1.setTransform(-49.4,-43.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("An8H9IAAv5IP5AAIAAP5g");
	this.shape_2.setTransform(-53.2,-52.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 6
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhIAAQiIwiAAIwhAAIAAwiIEFAAIMcAAIMUAAIEOAAAAAwhIAAESIAAMPIAAMhIAAEBAwhAAIAAwhIQhAA");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3333").s().p("AAFMYIAAsgIAAMgIscAAIAAsgIAAsPIMcAAIMTAAIAAMPIsTAAIscAAIMcAAIMTAAIAAMggAAFgIIAAsPgAMYgIg");
	this.shape_4.setTransform(-0.5,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#003333").ss(4,1,1).p("AwhwhMAhDAAAMAAAAhDMghDAAAg");

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AwhQiMAAAghDMAhDAAAMAAAAhDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// Layer 5
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.027)").s().p("AyLSLMAAAgkVMAkXAAAMAAAAkVg");
	this.shape_7.setTransform(0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy2, new cjs.Rectangle(-116.4,-116.3,232.8,232.7), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Shapesqustcopy2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhAAAAAIQiAAIAAQiIwiAAIwhAAIAAwiIAAwhIQhAAIAAQhIAAQiAwhAAIQhAA");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

	// Layer 3
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("An8H9IAAv5IP5AAIAAP5g");
	this.shape_9.setTransform(-53.2,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

	// Layer 6
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIAAESIAAMPIAAMhIAAEBIwhAAIAAwiIEFAAIMcAAIMUAAIEOAAIAAQiIwiAAAAAwhIQiAAIAAQhAwhAAIAAwhIQhAA");

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF3333").s().p("AAFMYIAAsgIMTAAIAAMggAAFMYIscAAIAAsgIAAsPIMcAAIMTAAIAAMPIsTAAIAAMggAAFgIIscAAgAAFgIIAAsPgAMYgIg");
	this.shape_11.setTransform(-0.5,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10}]}).wait(1));

	// Layer 2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#003333").ss(4,1,1).p("AwhwhMAhDAAAMAAAAhDMghDAAAg");

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AwhQiMAAAghDMAhDAAAMAAAAhDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy2_1, new cjs.Rectangle(-107.8,-107.7,215.6,215.6), null);


(lib.Shapesqustcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhIAAQiIwiAAIwhAAIAAwiIAAwhIQhAAIAAQhIAAQiAAAAAIQiAAAwhAAIQhAA");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("An8H9IAAv5IP5AAIAAP5g");
	this.shape_1.setTransform(-53.2,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhIAAQiIwiAAIwhAAIAAwiIEFAAIMcAAIMUAAIEOAAAAAwhIAAESIAAMPIAAMhIAAEBAwhAAIAAwhIQhAA");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AAFMYIAAsgIAAMgIscAAIAAsgIAAsPIMcAAIMTAAIAAMPIsTAAIscAAIMcAAIMTAAIAAMggAAFgIIAAsPgAMYgIg");
	this.shape_3.setTransform(-0.5,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#003333").ss(4,1,1).p("AwhwhMAhDAAAMAAAAhDMghDAAAg");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AwhQiMAAAghDMAhDAAAMAAAAhDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy, new cjs.Rectangle(-107.8,-107.7,215.6,215.6), null);


(lib.InstructionText_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000066").s().p("AggBOQgPgFgLgLQgJgLgGgPQgFgPgBgSQABgSAGgPQAGgQALgMQALgMAPgHQAOgGAQgBQAQAAANAHQANAFALALQAKALAHANQAHAOACARIh+ASQABALADAIQAFAIAGAGQAGAFAJADQAIAEAJAAQAIAAAIgDQAHgDAGgEQAHgEAFgIQAEgHACgIIAdAFQgEANgIAMQgHALgKAHQgKAIgMAEQgMAEgNABQgTAAgOgHgAgLg4QgHACgIAFQgGAFgGAJQgFAJgCAMIBXgKIgCgEQgFgOgJgIQgKgIgPAAQgFgBgHADg");
	this.shape.setTransform(181.3,-9.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000066").s().p("AhQh2IAhgBIgBAVQAJgIAJgFIAQgHQAJgCAIgBQAKAAAKADQAJADAJAFQAJAFAHAIQAIAHAFAJQAFAJADAKQADALAAALQgBANgDALQgDALgGAIQgFAKgIAHQgHAIgIAFQgJAFgIADQgJACgJAAIgRgCQgIgCgKgFQgKgEgJgIIgBBjIgbABgAgQhYQgIADgHAGQgGAFgEAHQgFAGgCAIIgBAcQADAJAEAIQAFAHAHAEQAGAFAHADQAIACAHAAQAKAAAJgDQAJgEAGgGQAHgHAEgKQAEgJABgLQAAgLgDgKQgDgKgHgHQgGgHgJgFQgJgEgLAAQgIAAgIADg");
	this.shape_1.setTransform(163.1,-5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_2.setTransform(143.1,-10);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000066").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_3.setTransform(124.5,-13.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000066").s().p("AgVBTIgMgDIgNgEIgOgHQgGgDgGgFIANgXIARAJIARAHQAJADAJABQAIACAKAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgCgGQgBgDgDgDIgHgFIgLgDIgRgCQgLgBgNgCQgMgCgJgFQgKgEgHgHQgGgHgBgLQgBgLACgIQADgJAEgHQAGgHAHgFQAHgFAIgEQAJgDAJgCQAKgCAIAAIAOABIAQACIASAFQAIAEAHAFIgKAbQgJgFgJgDIgPgFIgPgCQgXgBgMAGQgNAGAAANQAAAIAFAEQAEAEAJACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQACAHABAIQgBAOgFAJQgGAJgJAGQgJAGgNADQgMADgOABQgMAAgOgDg");
	this.shape_4.setTransform(106.1,-9.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000066").s().p("AgfB6IgOgFIgNgIQgHgFgGgGQgFgHgDgJIAcgMQADAGAEAFQADAEAFADIAJAFIAKADQALACALgBQAJgCAJgEQAHgFAFgFQAFgFADgGIAGgLIACgKIABgGIAAgTQgJAIgJAEQgJAFgJACQgJACgIABQgRAAgPgGQgOgGgLgLQgLgLgGgPQgHgQAAgUQAAgUAIgPQAGgQALgLQAMgKAOgGQAOgGAOAAIATAFIATAIQAKAFAIAJIAAgdIAeAAIgCCoQAAAJgCAKQgDAJgEAJQgEAJgIAIQgGAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgOgEgAgVhZQgJAFgHAHQgGAIgEAKQgDALAAAMQAAAMADAKQAEAKAGAGQAHAHAJAEQAJAFALAAQAJAAAIgEQAKgDAHgGQAHgFAFgJQAGgIABgKIAAgTQgBgLgGgIQgFgJgHgGQgIgGgJgEQgKgDgIAAQgKAAgJAEg");
	this.shape_5.setTransform(79.6,-6.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000066").s().p("AhDAkIgCgkIgBgcIAAgVIgBgfIAhgBIAAAqQAGgIAGgHQAFgIAIgGQAHgFAIgEQAHgDAJgBQALgBAJADQAIADAGAEQAFAFAEAGQAFAHABAHIADANIACAMIABAyIgBA1IgfgBIACgvQACgYgCgXIAAgHIgCgIQgBgFgCgFQgCgEgEgEQgEgDgFgCQgFgCgGABQgMACgLAPQgMAPgOAcIACAmIAAAVIABAMIggAEIgBgug");
	this.shape_6.setTransform(60.9,-9.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000066").s().p("AgSBuIABglIABguIAAg+IAegBIgBAlIgBAgIAAAaIgBATIAAAggAgHhFIgHgEQgDgDgBgEQgCgEAAgEQAAgEACgEQABgEADgDQADgDAEgBQAEgCADAAQAEAAAEACQAEABADADIAEAHIACAIQAAAEgCAEIgEAHIgHAEIgIACQgDAAgEgCg");
	this.shape_7.setTransform(48.1,-13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000066").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIAOgXIAQAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgBgDgEgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgKgEgGgHQgGgHgBgLQgBgLADgIQABgJAGgHQAEgHAIgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgVgBgNAGQgNAGAAANQAAAIAFAEQAFAEAIACIATADIAXACQAPACAJAFQAKADAGAGQAGAGADAHQACAHAAAIQABAOgGAJQgGAJgJAGQgKAGgMADQgMADgNABQgNAAgNgDg");
	this.shape_8.setTransform(35.7,-9.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000066").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgGgFIAPgXIAQAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgBgDgEgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgKgEgGgHQgGgHgBgLQgBgLADgIQABgJAGgHQAEgHAIgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIARAFQAJAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgVgBgNAGQgNAGAAANQAAAIAFAEQAFAEAIACIATADIAXACQAPACAJAFQAKADAGAGQAGAGADAHQACAHAAAIQABAOgGAJQgGAJgJAGQgKAGgMADQgMADgNABQgNAAgNgDg");
	this.shape_9.setTransform(19.1,-9.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000066").s().p("AgSBuIABglIABguIABg+IAdgBIgBAlIAAAgIgBAaIAAATIAAAggAgHhFIgHgEQgCgDgCgEQgCgEAAgEQAAgEACgEQACgEACgDQADgDAEgBQADgCAEAAQAEAAAEACQAEABADADIAFAHIABAIQAAAEgBAEIgFAHIgHAEIgIACQgEAAgDgCg");
	this.shape_10.setTransform(7.3,-13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000066").s().p("ABGAjIAAgfIAAgYQgBgMgDgGQgEgFgFgBQgEAAgEADIgHAFIgHAIIgHAJIgGAKIgFAHIAAAOIABASIAAAVIAAAcIgbABIgBgtIgBgfIgBgYQgBgMgDgGQgEgFgFgBQgEAAgEADIgIAGIgIAJIgHAKIgHALIgFAGIABBOIgdABIgEicIAfgEIAAArIAKgMIALgMQAGgEAIgDQAHgEAIAAQAGAAAGACQAGACADAEQAFAEADAGQAEAHAAAJIAKgMIALgLQAGgEAHgEQAHgDAIAAQAHAAAGACQAGACAFAFQAEAEADAIQAEAHAAAKIABAbIACAiIAAA0IgeABIgBgtg");
	this.shape_11.setTransform(-8.8,-10.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000066").s().p("AggBOQgPgFgKgLQgLgLgFgPQgGgPAAgSQAAgSAHgPQAGgQALgMQALgMAPgHQAOgGAQgBQAPAAAOAHQANAFAKALQALALAHANQAGAOACARIh+ASQABALAFAIQAEAIAGAGQAHAFAIADQAIAEAJAAQAIAAAHgDQAIgDAHgEQAGgEAFgIQAFgHACgIIAcAFQgEANgHAMQgIALgKAHQgKAIgMAEQgMAEgNABQgTAAgOgHgAgLg4QgIACgHAFQgGAFgGAJQgGAJgCAMIBYgKIgBgEQgGgOgKgIQgKgIgOAAQgFgBgHADg");
	this.shape_12.setTransform(-37.5,-9.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000066").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_13.setTransform(-56.2,-13.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000066").s().p("AgRgQIgzABIABgbIAygBIAChBIAcgCIgBBCIA5gCIgDAbIg3ACIgBB/IgeABg");
	this.shape_14.setTransform(-73.3,-12.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000066").s().p("AgRgQIgzABIABgbIAygBIAChBIAcgCIgBBCIA5gCIgDAbIg3ACIgBB/IgeABg");
	this.shape_15.setTransform(-95.3,-12.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000066").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_16.setTransform(-111.5,-9.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000066").s().p("AggBOQgPgFgKgLQgKgLgGgPQgGgPAAgSQAAgSAHgPQAGgQALgMQALgMAPgHQAOgGAQgBQAPAAAOAHQANAFAKALQALALAHANQAGAOACARIh9ASQAAALAEAIQAFAIAGAGQAHAFAIADQAIAEAJAAQAIAAAHgDQAIgDAHgEQAGgEAFgIQAEgHADgIIAcAFQgEANgIAMQgHALgKAHQgKAIgMAEQgMAEgNABQgTAAgOgHgAgLg4QgIACgHAFQgGAFgGAJQgFAJgDAMIBYgKIgBgEQgGgOgKgIQgKgIgOAAQgFgBgHADg");
	this.shape_17.setTransform(-129.4,-9.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000066").s().p("AAJB/QgIgDgFgFQgGgGgEgHQgFgGgDgHQgHgRgBgVIAEi5IAdAAIgBAwIgBAnIgBAgIAAAYIgBAoQABANACALIAEAJIAGAIQADADAFADQAFACAGAAIgEAdQgKAAgIgEg");
	this.shape_18.setTransform(-141.7,-14.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000066").s().p("AggBOQgPgFgLgLQgKgLgFgPQgGgPABgSQAAgSAGgPQAGgQALgMQALgMAOgHQAPgGAQgBQAPAAAOAHQANAFALALQAKALAHANQAGAOACARIh+ASQACALAEAIQADAIAHAGQAHAFAIADQAIAEAJAAQAIAAAHgDQAIgDAGgEQAHgEAFgIQAEgHACgIIAdAFQgEANgHAMQgIALgKAHQgKAIgMAEQgMAEgNABQgSAAgPgHgAgMg4QgGACgHAFQgIAFgFAJQgFAJgDAMIBXgKIgBgEQgFgOgKgIQgJgIgPAAQgFgBgIADg");
	this.shape_19.setTransform(-155.6,-9.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000066").s().p("AgTB2QgLgCgKgFQgLgEgKgGIgVgNIAWgaQALALALAFQALAGAJADQALAEAIABQALAAAJgCQAJgCAHgEQAHgEAEgGQAEgGABgGQAAgGgCgEQgDgFgEgDQgFgEgGgDIgNgFIgOgEIgOgDIgRgEIgSgGQgJgEgIgGQgIgFgGgIQgGgIgDgLQgDgLABgPQABgLAEgKQAEgJAHgIQAGgHAIgFQAIgGAKgDQAJgEAKgBQAKgCAJAAQAOABAOADIAMAEIANAGQAHADAGAEIANAKIgRAaIgKgJIgKgHIgLgFIgKgEQgLgDgLAAQgMAAgLAEQgKAEgHAGQgHAHgEAIQgEAHAAAIQAAAHAFAHQAEAHAIAGQAIAGALAEQAKAEAMACIAVADQALACAJAEQAKAEAIAGQAIAGAGAHQAFAIADAJQADAKgCALQgCAKgEAIQgEAIgHAGQgGAGgIAEIgRAHQgIACgJABIgSABQgKAAgLgCg");
	this.shape_20.setTransform(-174.2,-12.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAjLgDqMhGVAAAQhhAAhFBFQhFBFAABgQAABhBFBFQBFBFBhAAMBGVAAAQBhAABFhFQBFhFAAhhQAAhghFhFQhFhFhhAAg");
	this.shape_21.setTransform(1,-14.5,0.912,1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#00CCFF").s().p("EgjKADrQhhgBhFhFQhEhEAAhhQAAhgBEhFQBFhEBhAAMBGVAAAQBhAABFBEQBFBFgBBgQABBhhFBEQhFBFhhABg");
	this.shape_22.setTransform(1,-14.5,0.912,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mccopy, new cjs.Rectangle(-227.1,-39.5,456.3,49.9), null);


(lib.InstructionText_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("AggBPQgPgHgLgKQgJgKgGgQQgFgPgBgTQABgRAGgPQAGgRALgLQALgMAPgHQAOgGAQAAQAQAAANAFQANAHALAKQAKALAHANQAHAPACAQIh+ASQABAKADAJQAFAIAGAGQAGAFAJADQAIADAJAAQAIAAAIgCQAHgDAGgEQAHgEAFgIQAEgGACgKIAdAGQgEAOgIALQgHAKgKAIQgKAIgMAEQgMAFgNAAQgTgBgOgFgAgLg4QgHACgIAFQgGAFgGAJQgFAIgCANIBXgLIgCgDQgFgOgJgIQgKgJgPABQgFAAgHACg");
	this.shape.setTransform(138.6,-11.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AhQh2IAhgBIgBAVQAJgIAJgFIAQgHQAJgCAIgBQAKAAAKADQAJADAJAFQAJAFAHAIQAIAHAFAJQAFAJADAKQADALAAALQgBANgDALQgDALgGAIQgFAKgIAHQgHAIgIAFQgJAFgIADQgJACgJAAIgRgCQgIgCgKgFQgKgEgJgIIgBBjIgbABgAgQhYQgIADgHAGQgGAFgEAHQgFAGgCAIIgBAcQADAJAEAIQAFAHAHAEQAGAFAHADQAIACAHAAQAKAAAJgDQAJgEAGgGQAHgHAEgKQAEgJABgLQAAgLgDgKQgDgKgHgHQgGgHgJgFQgJgEgLAAQgIAAgIADg");
	this.shape_1.setTransform(120.3,-7.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_2.setTransform(100.4,-11.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_3.setTransform(81.8,-14.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AgVBTIgMgDIgNgEIgOgHQgGgDgGgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgCgGQgBgDgDgDIgHgFIgLgDIgRgCQgLgBgNgCQgMgCgJgFQgKgEgHgHQgGgHgBgLQgBgLACgIQADgJAEgHQAGgHAHgFQAHgFAIgEQAJgDAJgCQAKgCAIAAIAOABIAQACIASAFQAIAEAHAFIgKAbQgJgFgJgDIgPgFIgPgCQgXgBgMAGQgNAGAAANQAAAIAFAEQAEAEAJACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQACAHABAIQgBAOgFAJQgGAJgJAGQgJAGgNADQgMADgOABQgMAAgOgDg");
	this.shape_4.setTransform(63.3,-10.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("AggBPQgPgHgKgKQgLgKgFgQQgGgPAAgTQAAgRAHgPQAGgRALgLQALgMAPgHQAOgGAQAAQAPAAAOAFQANAHAKAKQALALAHANQAGAPACAQIh+ASQABAKAFAJQAEAIAGAGQAHAFAIADQAIADAJAAQAIAAAHgCQAIgDAHgEQAGgEAFgIQAFgGACgKIAcAGQgEAOgHALQgIAKgKAIQgKAIgMAEQgMAFgNAAQgTgBgOgFgAgLg4QgIACgHAFQgGAFgGAJQgGAIgCANIBYgLIgBgDQgGgOgKgIQgKgJgOABQgFAAgHACg");
	this.shape_5.setTransform(38.5,-11.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_6.setTransform(19.8,-14.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgCIgBBCIA5gCIgDAbIg3ACIgBB+IgeACg");
	this.shape_7.setTransform(2.8,-14.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("AggBPQgPgHgKgKQgKgKgGgQQgGgPAAgTQAAgRAHgPQAGgRALgLQALgMAPgHQAOgGAQAAQAPAAAOAFQANAHAKAKQALALAHANQAGAPACAQIh9ASQAAAKAEAJQAFAIAGAGQAHAFAIADQAIADAJAAQAIAAAHgCQAIgDAHgEQAGgEAFgIQAEgGADgKIAcAGQgEAOgIALQgHAKgKAIQgKAIgMAEQgMAFgNAAQgTgBgOgFgAgLg4QgIACgHAFQgGAFgGAJQgFAIgDANIBYgLIgBgDQgGgOgKgIQgKgJgOABQgFAAgHACg");
	this.shape_8.setTransform(-21,-11.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgXBOIgziSIAdgFIAtB9IAtiCIAeAEIg6CZg");
	this.shape_9.setTransform(-38.2,-11.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_10.setTransform(-53.9,-11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("AggBPQgPgHgKgKQgLgKgFgQQgGgPABgTQgBgRAHgPQAGgRALgLQALgMAOgHQAPgGAQAAQAPAAAOAFQANAHAKAKQALALAHANQAGAPACAQIh+ASQABAKAFAJQAEAIAGAGQAGAFAJADQAIADAJAAQAIAAAHgCQAIgDAHgEQAGgEAFgIQAFgGACgKIAcAGQgEAOgHALQgIAKgKAIQgKAIgMAEQgMAFgNAAQgSgBgPgFgAgMg4QgHACgHAFQgGAFgGAJQgGAIgCANIBXgLIAAgDQgGgOgKgIQgKgJgOABQgFAAgIACg");
	this.shape_11.setTransform(-71.6,-11.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgVBTIgMgDIgNgEIgNgHQgIgDgGgFIAPgXIAQAJIASAHQAIADAKABQAHACALAAQAHAAAGgBQAGgCADgCQAEgCACgDIACgGIABgFIgCgGQgBgDgCgDIgIgFIgLgDIgQgCQgNgBgMgCQgMgCgJgFQgLgEgFgHQgHgHgBgLQgBgLACgIQADgJAFgHQAFgHAGgFQAIgFAIgEQAJgDAJgCQAJgCAJAAIAOABIAQACIARAFQAJAEAHAFIgKAbQgKgFgIgDIgQgFIgOgCQgXgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIATADIAXACQAPACAKAFQAJADAGAGQAGAGADAHQADAHAAAIQAAAOgGAJQgGAJgJAGQgKAGgMADQgMADgOABQgMAAgOgDg");
	this.shape_12.setTransform(-89.1,-10.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgNBwQgIgCgKgFQgKgEgJgJIAAAUIgbAAIgDjhIAggBIgBBXQAJgJAJgEQAJgFAIgCQAJgCAIAAQAKAAAJADQAKACAJAGQAJAFAHAHQAIAHAFAJQAFAJADAKQADAKAAAMQgBAMgDALQgEALgFAKQgGAJgHAIQgHAHgIAFQgJAFgJADQgJADgIAAQgIgBgKgCgAAAgVQgIAAgIADQgIADgHAFQgGAFgFAFQgFAHgCAHIAAAjQACAJAFAHQAFAHAGAFQAHAFAHACQAHADAIAAQAKAAAJgEQAJgEAHgHQAHgIAEgJQAFgKAAgMQABgLgEgKQgDgKgHgHQgGgHgKgFQgIgEgKAAIgCAAg");
	this.shape_13.setTransform(-106.5,-14.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("AgcBtQgPgDgMgIQgMgHgKgLQgKgKgIgNQgHgMgEgPQgEgOAAgQQAAgPAEgOQAEgOAHgNQAIgNAKgKQAKgLAMgHQAMgIAPgDQAOgFAOAAQAPAAAOAFQAPADAMAIQAMAHAKALQAKAKAIANQAHANAEAOQAEAOAAAPQAAAQgEAOQgEAPgHAMQgIANgKAKQgKALgMAHQgMAIgPADQgOAEgPABQgOgBgOgEgAgfhNQgPAHgLALQgLAMgGAPQgHAPAAARQAAARAHAQQAGAPALAMQALALAPAHQAPAGAQAAQALAAALgDQAKgCAJgGQAJgGAIgHQAHgIAGgJQAFgKADgLQADgKAAgMQAAgKgDgMQgDgKgFgKQgGgJgHgIQgIgHgJgFQgJgGgKgDQgLgDgLAAQgQAAgPAGg");
	this.shape_14.setTransform(-129,-14.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAjLgDqMhGVAAAQhhAAhFBFQhFBFAABgQAABhBFBFQBFBFBhAAMBGVAAAQBhAABFhFQBFhFAAhhQAAhghFhFQhFhFhhAAg");
	this.shape_15.setTransform(1,-14.5,0.814,1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFCC00").s().p("EgjKADrQhhgBhFhFQhEhEAAhhQAAhgBEhFQBFhEBhAAMBGVAAAQBhAABFBEQBFBFgBBgQABBhhFBEQhFBFhhABg");
	this.shape_16.setTransform(1,-14.5,0.814,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mc, new cjs.Rectangle(-202.8,-39.5,407.7,49.9), null);


(lib.ch1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("At/NxIAA7iIcAAAIAAbig");
	this.shape.setTransform(0.2,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy4, new cjs.Rectangle(-89.5,-88.3,179.3,176.3), null);


(lib.ch1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("At/NxIAA7iIcAAAIAAbig");
	this.shape.setTransform(0.2,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy3, new cjs.Rectangle(-89.5,-88.3,179.3,176.3), null);


(lib.ch1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("At/NxIAA7iIcAAAIAAbig");
	this.shape.setTransform(0.2,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy2, new cjs.Rectangle(-89.5,-88.3,179.3,176.3), null);


(lib.ch1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF9900").s().p("AsKMNIAA4ZIAFAAIAcAAIBIAEQCqAKCdAsQCBAiB5A8QA2AbA0AfQANAHAMAKQCYBeCJCHQAfAfAeAhQCXCmBcC+QAOAdAMAcQBqDwAQEVIADAtIAAA6IAAAIg");
	this.shape.setTransform(0.2,-2.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape_1.setTransform(-1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_2.setTransform(-1,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_3.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_4.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.ch1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("At/NxIAA7iIcAAAIAAbig");
	this.shape.setTransform(0.2,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape_1.setTransform(-1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_2.setTransform(-1,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_3.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_4.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(5.8,13.9,1.5,1.5,0,0,0,19.2,25.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ar6L7IAA31IX1AAIAAX1g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-76.3,-76.3,152.6,152.6);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween17("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-181.7,-37.9,363.5,76.6);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-235.9,-38.7,471.9,79);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1copy();
	this.instance.parent = this;
	this.instance.setTransform(381.5,0.3,1.08,1.08,0,0,0,-0.9,-0.4);

	this.instance_1 = new lib.ch1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-380.8,0.3,1.08,1.08,0,0,0,-0.9,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-523.5,-138.3,1047.1,276.8);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween19("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.1,scaleY:1.1},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-76.3,-76.3,152.6,152.6);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ar6L7IAA31IX1AAIAAX1g");
	this.shape.setTransform(79.2,79.4);

	this.instance = new lib.Shapesqustcopy();
	this.instance.parent = this;
	this.instance.setTransform(158.7,158.6,1.5,1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy, new cjs.Rectangle(-3,-3,323.4,323.3), null);


(lib.Shapesqustcopy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhAAAAAIQiAAIAAQiIwiAAIwhAAIAAwiIAAwhIQhAAIAAQhIAAQiAwhAAIQhAA");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(-49.4,-43.4,1,1,0,0,0,19.2,25.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("An8H9IAAv5IP5AAIAAP5g");
	this.shape_1.setTransform(-53.2,-52.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.instance}]}).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIAAESIAAMPIAAMhIAAEBIwhAAIAAwiIEFAAIMcAAIMUAAIEOAAIAAQiIwiAAAAAwhIQiAAIAAQhAwhAAIAAwhIQhAA");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AAFMYIAAsgIMTAAIAAMggAAFMYIscAAIAAsgIAAsPIMcAAIMTAAIAAMPIsTAAIAAMggAAFgIIscAAgAAFgIIAAsPgAMYgIg");
	this.shape_3.setTransform(-0.5,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#003333").ss(4,1,1).p("AwhwhMAhDAAAMAAAAhDMghDAAAg");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AwhQiMAAAghDMAhDAAAMAAAAhDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy9, new cjs.Rectangle(-107.8,-107.7,215.6,215.6), null);


(lib.Shapesqustcopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhIAAQiIwiAAIwhAAIAAwiIAAwhIQhAAIAAQhIAAQiAAAAAIQiAAAwhAAIQhAA");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(-49.4,-43.4,1,1,0,0,0,19.2,25.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("An8H9IAAv5IP5AAIAAP5g");
	this.shape_1.setTransform(-53.2,-52.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.instance}]}).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhIAAQiIwiAAIwhAAIAAwiIEFAAIMcAAIMUAAIEOAAAAAwhIAAESIAAMPIAAMhIAAEBAwhAAIAAwhIQhAA");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AAFMYIAAsgIAAMgIscAAIAAsgIAAsPIMcAAIMTAAIAAMPIsTAAIscAAIMcAAIMTAAIAAMggAAFgIIAAsPgAMYgIg");
	this.shape_3.setTransform(-0.5,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#003333").ss(4,1,1).p("AwhwhMAhDAAAMAAAAhDMghDAAAg");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AwhQiMAAAghDMAhDAAAMAAAAhDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy6, new cjs.Rectangle(-107.8,-107.7,215.6,215.6), null);


(lib.Shapesqustcopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIQiAAIAAQhAAAAAIQiAAIAAQiIwiAAIwhAAIAAwiIAAwhIQhAAIAAQhIAAQiAwhAAIQhAA");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(-49.4,-43.4,1,1,0,0,0,19.2,25.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("An8H9IAAv5IP5AAIAAP5g");
	this.shape_1.setTransform(-53.2,-52.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.instance}]}).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(4,1,1).p("AAAwhIAAESIAAMPIAAMhIAAEBIwhAAIAAwiIEFAAIMcAAIMUAAIEOAAIAAQiIwiAAAAAwhIQiAAIAAQhAwhAAIAAwhIQhAA");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AAFMYIAAsgIMTAAIAAMggAAFMYIscAAIAAsgIAAsPIMcAAIMTAAIAAMPIsTAAIAAMggAAFgIIscAAgAAFgIIAAsPgAMYgIg");
	this.shape_3.setTransform(-0.5,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#003333").ss(4,1,1).p("AwhwhMAhDAAAMAAAAhDMghDAAAg");

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AwhQiMAAAghDMAhDAAAMAAAAhDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy5, new cjs.Rectangle(-107.8,-107.7,215.6,215.6), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(645.9,325.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(354).to({_off:false},0).wait(52).to({startPosition:9},0).to({alpha:0,startPosition:13},7).wait(1));

	// hand
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(230.1,649.5,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(288).to({_off:false},0).to({_off:true},31).wait(95));

	// arrow
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(269.1,380.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(255).to({_off:false},0).to({_off:true},33).wait(126));

	// Layer_5
	this.instance_3 = new lib.ch1copy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(264.2,568,1.08,1.08,0,0,0,-0.9,-0.4);
	this.instance_3._off = true;

	this.instance_4 = new lib.ch1copy3();
	this.instance_4.parent = this;
	this.instance_4.setTransform(583.4,259.2,0.648,0.648,0,0,0,-0.8,-0.3);

	this.instance_5 = new lib.ch1copy4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(583.4,259.2,0.648,0.648,0,0,0,-0.8,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},319).to({state:[{t:this.instance_4}]},20).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},26).to({state:[{t:this.instance_5}]},38).to({state:[{t:this.instance_5}]},7).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(319).to({_off:false},0).to({_off:true,regX:-0.8,regY:-0.3,scaleX:0.65,scaleY:0.65,x:583.4,y:259.2},20).wait(75));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(342).to({_off:false},0).to({regX:-0.7,scaleX:0.78,scaleY:0.78,x:570.6,y:246.6},26).wait(38).to({alpha:0},7).wait(1));

	// Layer_8
	this.instance_6 = new lib.ch1copy2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(583.4,259.1,0.648,0.648,0,0,0,-0.8,-0.3);
	this.instance_6.alpha = 0.801;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(193).to({_off:false},0).wait(54).to({alpha:0},7).to({_off:true},1).wait(159));

	// Layer_10
	this.instance_7 = new lib.Symbol8("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(565.1,239.9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(140).to({_off:false},0).wait(107).to({startPosition:27},0).to({alpha:0,startPosition:34},7).to({_off:true},1).wait(159));

	// Layer_7
	this.instance_8 = new lib.Tween14("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(775.6,88);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(140).to({_off:false},0).wait(107).to({startPosition:25},0).to({alpha:0,startPosition:32},7).to({_off:true},1).wait(159));

	// Layer_4
	this.questxt = new lib.InstructionText_mccopy();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(643.5,116,1.612,1.612);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(127).to({_off:false},0).to({alpha:1},6).wait(206).to({alpha:0},5).to({_off:true},1).wait(69));

	// Layer_3
	this.instance_9 = new lib.Tween2("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(645,567.7);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(127).to({_off:false},0).to({alpha:1},6).to({startPosition:0},206).to({alpha:0},5).to({_off:true},1).wait(69));

	// Layer_6
	this.instance_10 = new lib.Tween12("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(644.5,91.5);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(63).to({_off:false},0).wait(57).to({startPosition:0},0).to({regX:0.1,regY:0.1,scaleX:1.02,scaleY:1.02,alpha:0},6).to({_off:true},1).wait(287));

	// Layer_2
	this.instance_11 = new lib.Shapesqustcopy2();
	this.instance_11.parent = this;
	this.instance_11.setTransform(645,318.8,1.5,1.5);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.instance_12 = new lib.Shapesqustcopy6();
	this.instance_12.parent = this;
	this.instance_12.setTransform(645,318.8,1.5,1.5);

	this.instance_13 = new lib.Shapesqustcopy9();
	this.instance_13.parent = this;
	this.instance_13.setTransform(645,318.8,1.5,1.5);

	this.instance_14 = new lib.Shapesqustcopy();
	this.instance_14.parent = this;
	this.instance_14.setTransform(644.7,319.1,1.5,1.5);

	this.instance_15 = new lib.Shapesqustcopy5();
	this.instance_15.parent = this;
	this.instance_15.setTransform(644.5,318.8,1.5,1.5);

	this.instance_16 = new lib.Shapesqustcopy5();
	this.instance_16.parent = this;
	this.instance_16.setTransform(644.5,318.8,1.5,1.5);

	this.instance_17 = new lib.Shapesqustcopy2_1();
	this.instance_17.parent = this;
	this.instance_17.setTransform(644.7,319.1,1.5,1.5);

	this.instance_18 = new lib.Symbol4copy();
	this.instance_18.parent = this;
	this.instance_18.setTransform(644.7,319.2,1,1,0,0,0,158.7,158.7);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11}]},44).to({state:[{t:this.instance_12}]},8).to({state:[{t:this.instance_13}]},75).to({state:[{t:this.instance_15,p:{x:644.5}},{t:this.instance_14}]},66).to({state:[{t:this.instance_16},{t:this.instance_14},{t:this.instance_15,p:{x:645}}]},62).to({state:[{t:this.instance_17}]},84).to({state:[{t:this.instance_18}]},3).to({state:[{t:this.instance_18}]},26).to({state:[{t:this.instance_18}]},38).to({state:[{t:this.instance_18}]},7).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(44).to({_off:false},0).to({_off:true,alpha:1},8).wait(362));
	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(342).to({_off:false},0).to({scaleX:1.21,scaleY:1.21,x:644.6,y:319.1},26).wait(38).to({alpha:0},7).wait(1));

	// Layer_1
	this.questxt_1 = new lib.InstructionText_mc();
	this.questxt_1.name = "questxt_1";
	this.questxt_1.parent = this;
	this.questxt_1.setTransform(643.5,116,1.612,1.612);
	this.questxt_1.alpha = 0;
	this.questxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt_1).wait(20).to({_off:false},0).to({alpha:1},6).to({regX:0.1,regY:0.1,x:643.6,y:116.2},94).to({y:116.1,alpha:0},6).to({_off:true},1).wait(287));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(573,307,1420,832);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;