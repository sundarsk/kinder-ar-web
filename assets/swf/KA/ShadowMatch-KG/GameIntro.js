(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AAGgmIgvCVIhHAFIgrjmIA1gEIAeC0IA8i0IAuADIAoC2IAZi4IA5ACIgsDoIhHACg");
	this.shape.setTransform(166,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AguB5QgWgJgQgSQgQgRgJgYQgKgYAAgdQAAgdAKgXQAJgYAQgSQAQgRAWgKQAWgJAYAAQAYAAAWAKQAWAKARARQAQASAJAYQAKAYAAAbQAAAbgKAYQgJAZgQARQgRASgWAKQgVAKgZgBQgYAAgWgJgAgchMQgNAHgIAMQgHANgEAOQgEAPAAAPQAAAPAEAPQAEAPAIAMQAIALANAIQALAHAQAAQARAAAMgHQAMgIAIgLQAIgMAEgPQAEgPAAgPQAAgOgDgQQgEgOgIgNQgIgMgMgHQgNgHgRAAQgQAAgMAHg");
	this.shape_1.setTransform(135.7,5.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgyCdQgWgJgRgQQgQgSgKgXQgKgYAAgeQAAgeALgXQALgYARgQQAQgQAWgJQAWgIARgBQATABAQAEQAMADAPAHQAPAHAGAMIAChuIAxAEIgBFFIg1AAIABgaIAAgCIAAACQgBAEgFAEIgPAIIgNAGQgHACgLACQgIADgOAAQgaABgWgKgAgeghQgNAHgJAKQgJAKgFAOQgGAPAAARQAAASAGAOQAFAOAKALQAJAKAMAFQANAGAPAAQAMAAAMgEQAMgEAKgHQAKgIAHgKQAIgKADgNIABgrQgDgNgJgKQgHgLgLgHQgKgJgMgEQgNgEgLAAQgPAAgMAGg");
	this.shape_2.setTransform(106.6,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AA+B+IADgdQgUAQgZAIQgZAHgXAAQgQAAgPgEQgQgEgLgJQgMgIgHgOQgHgOAAgSQAAgTAFgOQAFgPAKgKQAJgLANgIQANgHAOgFQAPgFAQgDQAPgCAPAAIAaABQANABAFACIgHgPQgEgKgGgIQgFgHgJgFQgIgFgMAAQgHAAgKADQgKACgNAGQgMAHgPALQgPALgVASIgegkQAWgWAUgNQATgMARgHQARgIAPgCQAOgDALAAQAVAAAQAHQARAHAMANQAMAMAIAQQAJAQAFATQAFATADAUQACASAAAUQAAAUgDAYQgCAZgGAfgAgIADQgSAEgMAIQgMAIgGALQgHAMAEAMQADANAHAEQAIAFAKAAQAMAAAOgEQAOgEANgGIAbgNIATgLIABgTQAAgKgBgJIgTgEQgLgBgLAAQgTAAgQAEg");
	this.shape_3.setTransform(77.8,4.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA1CnQAGgZADgUIAEgiIAAghQgBgXgGgPQgHgPgJgJQgJgKgMgEQgLgEgKAAQgLABgLAFQgKAFgMAKQgMAKgKASIgBCSIgzABIgElUIA7gBIgBCCQAEgMAOgHQAOgIAJgDQAQgFANgCQAZAAAVAJQAVAJAPARQAOARAJAYQAIAWACAgIgBAcIgBAhQgBAQgDAOQgCANgEAPg");
	this.shape_4.setTransform(49.9,0.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgcB+IgWgEIgUgHIgUgKQgKgFgLgKIAYgmIAbANIAZAKIAaAGQALADAPAAQALAAAIgCQAHgCAFgDQAEgDADgEIADgFIABgIIgCgGIgFgIQgEgDgGgDQgGgEgKgCIgYgCQgSgBgSgDQgSgDgPgHQgOgHgKgLQgKgMgCgRQgCgRAEgNQAEgOAHgLQAIgKALgIQALgIANgGQANgFAOgCQAOgDAOAAIAUABIAZAEQAMACANAFQAOAFANAKIgRAuIgfgMIgXgHIgUgDQgggCgQAIQgSAIAAAQQAAALAGAFQAGAFAMACQALADARAAQAPABASADQAWADAPAHQAPAHAKAIQAJAJAEAMQAEALAAAMQAAAVgJAPQgIAPgPAJQgOAJgTAFQgSAEgUABQgUAAgRgEg");
	this.shape_5.setTransform(22.2,6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgegUIhKACIABgwIBKgCIACheIAygDIgBBgIBTgDIgDAxIhRACIgCC6Ig1ABg");
	this.shape_6.setTransform(-12.5,1.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgpB3QgXgJgSgSQgTgRgLgXQgLgYAAgcQAAgQAFgQQAEgQAJgOQAIgOAMgNQAMgMAOgJQAPgIARgGQAQgEASAAQATgBASAFQARAFAPAJQAPAIAMANQAMANAIAQIABAEIgrAZIgCgEQgFgKgIgJQgHgIgKgGQgJgFgLgDQgLgEgLAAQgQAAgOAHQgPAFgLAMQgLALgGAPQgGAOAAAQQAAARAGAPQAGAOALALQALALAPAGQAOAHAQgBQALABAKgDQAKgDAJgFQAJgGAIgGQAHgIAFgJIAEgEIArAYIgBAEQgJAQgMALQgNALgPAJQgOAIgSAFQgRAFgRAAQgZAAgXgLg");
	this.shape_7.setTransform(-36.8,5.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgyB3QgWgIgQgRQgQgQgIgXQgIgXAAgcQAAgbAJgYQAKgYAQgSQARgSAWgKQAXgKAYAAQAYAAAUAJQAVAJAQAQQAQAQAKAVQAKAWADAdIi5AbQABAKAGAMQAFALAJAIQAJAHALAFQAMAEAMAAQALAAAKgEQALgDAJgGQAJgHAGgJQAHgJAEgQIAxAJQgGAZgLAQQgMARgPAMQgPALgSAHQgTAGgUAAQgcAAgXgJgAgQhOQgKACgJAHQgKAHgIAMQgHAMgDANIB3gPIgBAAQgIgTgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_8.setTransform(-63.8,5.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AhlA1IgCg1IgBgpIgCgfIgBgyIA6gBIABAjIAAAVIABAOIAAAHIAAgHQABgFAFgHQAKgMALgLQAMgLANgIQAOgJAPgCQAUgBAPAJQAGAEAHAGQAGAGAGAKQAFAJAEANQADAMABAVIgzARQAAgOgCgHIgDgNQgCgFgEgDQgCgDAAgCQgKgFgHAAQgFABgGAEQgGAEgGAGIgOAPIgOAQIgMARIgKAOIAAAeIABAdIABAYIABAWIg3AGIgChHg");
	this.shape_9.setTransform(-88.4,5.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AhlA1IgCg1IgBgpIgBgfIgBgyIA5gBIAAAjIABAVIAAAOIAAAHIAAgHQACgFAFgHQAKgMALgLQAMgLAMgIQAOgJAQgCQAUgBAPAJQAHAEAGAGQAGAGAGAKQAFAJAEANQADAMABAVIgzARQAAgOgCgHIgEgNQgBgFgDgDQgDgDAAgCQgJgFgIAAQgFABgGAEQgGAEgGAGIgOAPIgNAQIgNARIgKAOIAAAeIABAdIABAYIACAWIg4AGIgChHg");
	this.shape_10.setTransform(-112.9,5.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AguB5QgWgJgQgSQgQgRgJgYQgKgYAAgdQAAgdAKgXQAJgYAQgSQAQgRAWgKQAWgJAYAAQAYAAAWAKQAWAKARARQAQASAJAYQAKAYAAAbQAAAbgKAYQgJAZgQARQgRASgWAKQgVAKgZgBQgYAAgWgJgAgchMQgNAHgIAMQgHANgEAOQgEAPAAAPQAAAPAEAPQAEAPAIAMQAIALANAIQALAHAQAAQARAAAMgHQAMgIAIgLQAIgMAEgPQAEgPAAgPQAAgOgDgQQgEgOgIgNQgIgMgMgHQgNgHgRAAQgQAAgMAHg");
	this.shape_11.setTransform(-140,5.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgpB3QgXgJgSgSQgTgRgLgXQgLgYAAgcQAAgQAFgQQAEgQAJgOQAIgOAMgNQAMgMAOgJQAPgIARgGQAQgEASAAQATgBASAFQARAFAPAJQAPAIAMANQAMANAIAQIABAEIgrAZIgCgEQgFgKgIgJQgHgIgKgGQgJgFgLgDQgLgEgLAAQgQAAgOAHQgPAFgLAMQgLALgGAPQgGAOAAAQQAAARAGAPQAGAOALALQALALAPAGQAOAHAQgBQALABAKgDQAKgDAJgFQAJgGAIgGQAHgIAFgJIAEgEIArAYIgBAEQgJAQgMALQgNALgPAJQgOAIgSAFQgRAFgRAAQgZAAgXgLg");
	this.shape_12.setTransform(-166.9,5.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("A8IFeQhnABhKhKQhIhJAAhnIAAjJQAAhnBIhKQBKhIBngBMA4QAAAQBoABBIBIQBKBKAABnIAADJQAABnhKBJQhIBKhogBg");
	this.shape_13.setTransform(0,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-205,-36,410.1,72);


(lib.question_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AADgUIgZBQIgmACIgWh7IAcgBIAQBfIAghfIAYABIAVBhIAOhiIAeABIgXB8IgmAAg");
	this.shape.setTransform(152.2,1.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYBBQgMgFgIgJQgJgKgFgMQgFgNAAgQQAAgPAFgMQAFgNAJgJQAIgKAMgFQAMgFAMAAQANAAALAGQAMAFAJAJQAJAKAFAMQAFANAAAOQAAAPgFANQgFANgJAJQgJAJgMAGQgLAFgNAAQgMAAgMgFgAgPgoQgGAEgEAGQgFAHgCAIQgCAIAAAHQAAAIACAIQADAIAEAGQAEAHAHAEQAGAEAIAAQAJAAAGgEQAHgEAFgHQADgGACgIQADgIAAgIQAAgHgCgIQgCgIgEgHQgFgGgHgEQgGgEgJAAQgIAAgHAEg");
	this.shape_1.setTransform(136,1.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbBUQgLgFgJgIQgJgKgFgNQgFgNAAgPQAAgQAFgMQAGgNAJgJQAKgIALgFQAMgEAJgBQAKABAIACQAHABAHAEQAIAFAEAFIABg6IAaACIAACsIgdAAIABgOIAAAAIAAAAQgBADgDACIgIAFIgHADIgKACQgEACgHAAQgOAAgMgFgAgQgRQgHADgFAGQgEAGgDAGQgCAIgBAJQABAKACAHQADAIAFAGQAFAFAGADQAIADAHAAQAGAAAHgCQAGgCAGgEQAFgEAEgFIAGgMIAAgYQgCgHgEgEQgEgHgFgEQgHgEgGgCQgGgDgGAAQgIAAgHAEg");
	this.shape_2.setTransform(120.4,-0.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAhBDIACgQQgLAKgNAEQgNADgMAAQgJAAgJgCQgHgCgGgFQgHgEgEgIQgEgHAAgKQAAgJADgJQADgHAFgFQAFgGAHgEQAHgEAHgDIARgEIAQgBIANAAIAKACIgDgIQgDgFgDgFQgDgEgEgCQgFgDgGAAIgJACIgMAEQgGAEgJAFIgTARIgQgUQAMgMALgGQAKgIAJgDQAJgEAIgBIANgBQALgBAJAEQAIAEAHAGQAHAHAEAIQAEAJADAKIAFAVIAAAUIAAAYIgFAegAgEACQgKACgGAEQgHAEgDAHQgDAFABAHQACAHAEACQAEACAGAAQAGAAAIgCIAOgFIAOgGIALgHIAAgKIAAgKIgKgCIgNAAQgJAAgJACg");
	this.shape_3.setTransform(105.1,1.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAdBZIAEgYIACgSIABgSQgBgMgEgIQgDgIgFgFQgFgFgGgCQgGgCgFAAQgGAAgGADQgGADgFAFQgHAFgFAKIgBBOIgbAAIgCi1IAfgBIAABGQACgGAHgEIANgGIAPgEQAOAAALAFQALAFAIAJQAHAJAFANQAEALABARIAAAPIgBASIgCAQIgEAPg");
	this.shape_4.setTransform(90.1,-0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPBDIgLgCIgLgEIgKgFIgMgIIANgVIAPAHIAMAGIAPADIANABIAKgBIAGgCIAEgDIACgDIABgEIgBgEIgEgEIgEgDIgJgEIgNgBQgJAAgKgCQgJgBgIgFQgIgDgFgGQgGgFAAgKQgBgJABgHQACgHAFgGQADgGAHgEQAFgFAHgCQAIgDAHgBIAPgBIAKAAIANACIAOAEQAHACAHAGIgJAYIgQgHIgNgDIgLgBQgQgCgJAFQgJAEAAAIQAAAHADACQADACAGACIAPABIASACQAMACAHAEQAIAEAGAEQAEAFADAFQACAGAAAHQAAAMgFAIQgEAIgIAEQgIAFgKADQgKACgKAAQgKAAgKgCg");
	this.shape_5.setTransform(75.3,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPgKIgoABIAAgaIAogBIABgzIAbgBIgBAzIAsgBIgBAaIgrABIgBBjIgcABg");
	this.shape_6.setTransform(56.7,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgVBAQgNgFgKgJQgJgKgGgMQgGgNAAgPQAAgIACgJQADgIAEgIQAFgIAGgGQAGgHAIgEQAIgFAJgDQAJgCAJAAQAKAAAJACQAKADAIAEQAIAFAGAHQAGAHAFAIIAAADIgXANIgBgCIgHgKQgEgFgFgDIgKgFIgMgBQgIAAgIADQgIAEgGAFQgFAGgEAIQgDAIAAAIQAAAJADAIQAEAIAFAGQAGAGAIADQAIADAIAAQAGAAAFgBQAFgCAFgDQAFgCAEgEIAHgJIACgCIAXANIgBACQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgNAAgMgFg");
	this.shape_7.setTransform(43.7,1.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgaBAQgNgFgHgJQgJgIgFgNQgDgMAAgPQAAgOAEgNQAFgNAJgJQAJgKAMgFQAMgGAMAAQANAAALAFQALAFAJAJQAIAIAFAMQAGALABAQIhiAOQAAAFAEAGQADAGAEAEQAFAFAGACQAHACAFAAQAGAAAGgCQAFgCAGgDQAEgDADgFQAEgFACgJIAaAFQgDANgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgOAAgMgFgAgIgqQgGACgEAEQgFADgEAHQgEAGgDAHIBAgIIgBAAQgDgKgIgGQgHgGgLAAIgIABg");
	this.shape_8.setTransform(29.3,1.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag1AdIgCgdIAAgWIgBgQIgBgbIAfgBIABATIAAALIAAAJIAAADIAAgDQAAgDAEgEIALgNQAGgFAHgFQAHgEAJgCQAKAAAIAFIAHAFIAGAIIAFAMQACAHABALIgcAKIgBgNIgCgGIgDgFQAAAAAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAQgFgCgEAAIgGACIgHAGIgGAHIgIAJIgGAJIgGAHIABAQIAAAQIABAMIAAAMIgdAEIgBgmg");
	this.shape_9.setTransform(16.1,1.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag1AdIgCgdIAAgWIgBgQIgBgbIAfgBIABATIAAALIAAAJIAAADIAAgDQAAgDAEgEIALgNQAGgFAHgFQAHgEAJgCQAKAAAIAFIAHAFIAGAIIAFAMQACAHABALIgcAKIgBgNIgCgGIgDgFQAAAAAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAQgFgCgEAAIgGACIgHAGIgGAHIgIAJIgGAJIgGAHIABAQIAAAQIABAMIAAAMIgdAEIgBgmg");
	this.shape_10.setTransform(3,1.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgYBBQgMgFgJgJQgIgKgFgMQgFgNAAgQQAAgPAFgMQAFgNAIgJQAJgKAMgFQALgFANAAQANAAALAGQAMAFAJAJQAJAKAFAMQAFANAAAOQAAAPgFANQgFANgJAJQgJAJgMAGQgLAFgNAAQgNAAgLgFgAgPgoQgGAEgFAGQgEAHgCAIQgCAIAAAHQAAAIADAIQACAIAEAGQAEAHAHAEQAGAEAIAAQAJAAAGgEQAHgEAEgHQAFgGACgIQACgIAAgIQAAgHgCgIQgCgIgFgHQgDgGgIgEQgGgEgJAAQgIAAgHAEg");
	this.shape_11.setTransform(-11.4,1.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgVBAQgNgFgKgJQgJgKgGgMQgGgNAAgPQAAgIACgJQADgIAEgIQAFgIAGgGQAGgHAIgEQAIgFAJgDQAJgCAJAAQAKAAAJACQAKADAIAEQAIAFAGAHQAGAHAFAIIAAADIgXANIgBgCIgHgKQgEgFgFgDIgKgFIgMgBQgIAAgIADQgIAEgGAFQgFAGgEAIQgDAIAAAIQAAAJADAIQAEAIAFAGQAGAGAIADQAIADAIAAQAGAAAFgBQAFgCAFgDQAFgCAEgEIAHgJIACgCIAXANIgBACQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgNAAgMgFg");
	this.shape_12.setTransform(-25.9,1.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgaBAQgNgFgIgJQgIgIgFgNQgDgMAAgPQgBgOAFgNQAFgNAJgJQAJgKAMgFQAMgGANAAQAMAAALAFQAMAFAIAJQAIAIAFAMQAGALACAQIhjAOQAAAFADAGQADAGAFAEQAFAFAGACQAGACAHAAQAFAAAGgCQAGgCAEgDQAFgDADgFQAEgFACgJIAbAFQgEANgGAJQgGAJgIAGQgIAHgKADQgJAEgLAAQgPAAgMgFgAgIgqQgFACgFAEQgGADgDAHQgFAGgBAHIA/gIIgBAAQgEgKgGgGQgIgGgKAAIgJABg");
	this.shape_13.setTransform(-46.6,1.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAcBZIAFgYIACgSIAAgSQAAgMgDgIQgEgIgFgFQgFgFgGgCQgGgCgGAAQgFAAgGADQgFADgHAFQgGAFgFAKIgBBOIgbAAIgCi1IAfgBIgBBGQADgGAHgEIANgGIAPgEQANAAALAFQAMAFAHAJQAJAJAEANQAFALAAARIAAAPIgBASIgCAQIgDAPg");
	this.shape_14.setTransform(-61.6,-0.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgPgKIgoABIAAgaIAogBIABgzIAbgBIgBAzIAsgBIgBAaIgrABIgBBjIgcABg");
	this.shape_15.setTransform(-75.3,-0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgPgKIgoABIAAgaIAogBIABgzIAbgBIgBAzIAsgBIgBAaIgrABIgBBjIgcABg");
	this.shape_16.setTransform(-93.1,-0.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgVBAQgNgFgKgJQgJgKgGgMQgGgNAAgPQAAgIACgJQADgIAEgIQAFgIAGgGQAGgHAIgEQAIgFAJgDQAJgCAJAAQAKAAAJACQAKADAIAEQAIAFAGAHQAGAHAFAIIAAADIgXANIgBgCIgHgKQgEgFgFgDIgKgFIgMgBQgIAAgIADQgIAEgGAFQgFAGgEAIQgDAIAAAIQAAAJADAIQAEAIAFAGQAGAGAIADQAIADAIAAQAGAAAFgBQAFgCAFgDQAFgCAEgEIAHgJIACgCIAXANIgBACQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgNAAgMgFg");
	this.shape_17.setTransform(-106.1,1.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgaBAQgMgFgJgJQgIgIgEgNQgEgMgBgPQAAgOAFgNQAFgNAJgJQAJgKAMgFQAMgGANAAQAMAAALAFQAMAFAHAJQAJAIAFAMQAGALACAQIhjAOQAAAFADAGQAEAGAEAEQAFAFAGACQAGACAHAAQAFAAAGgCQAGgCAEgDQAFgDADgFQAEgFACgJIAbAFQgEANgGAJQgGAJgIAGQgIAHgKADQgKAEgKAAQgPAAgMgFgAgIgqQgFACgGAEQgEADgFAHQgEAGgBAHIA/gIIAAAAQgEgKgHgGQgIgGgKAAIgJABg");
	this.shape_18.setTransform(-120.5,1.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAGBmQgGgCgEgFQgGgEgDgGQgDgFgDgKQgFgKgCgRIADiTIAcAAIgBAoIgBAfIgBAZIAAATIgBAeQABALACAHIADAIIAEAFIAGAEQADABAHABIgDAbQgLAAgHgDg");
	this.shape_19.setTransform(-130.3,-2.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgaBAQgNgFgHgJQgJgIgFgNQgEgMAAgPQABgOAEgNQAFgNAJgJQAJgKAMgFQAMgGAMAAQANAAALAFQALAFAIAJQAJAIAGAMQAFALABAQIhiAOQAAAFAEAGQACAGAGAEQAEAFAGACQAHACAFAAQAHAAAFgCQAFgCAGgDQAEgDAEgFQADgFACgJIAaAFQgDANgGAJQgGAJgIAGQgIAHgKADQgKAEgLAAQgOAAgMgFgAgIgqQgGACgFAEQgEADgFAHQgEAGgCAHIBAgIIgBAAQgDgKgIgGQgHgGgLAAIgIABg");
	this.shape_20.setTransform(-141.6,1.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgPBeIgRgEQgJgEgIgFQgIgEgJgIIATgXQALAJAIAEIARAHIANAEQAHAAAHgCQAHgCAFgCQAFgEADgEQADgEAAgFIgBgGIgFgFIgIgFIgKgEIgLgDIgLgDIgNgCIgPgGQgHgCgGgGQgHgEgEgGQgFgHgDgJQgCgJABgMQAAgJAEgIQADgIAFgFQAFgHAHgEQAHgEAIgDIAPgFIAPgBQAMABALADIAJADIALAEIALAGIAKAJIgPAYIgJgIIgIgGIgIgDIgJgDQgIgCgIgBQgJAAgHADQgIAEgFAEQgGAFgCAFQgDAHAAAFQAAAFADAGQADAFAGAEIAOAHQAIADAJABIARADQAIACAIACQAIAEAHAEQAGAFAFAGQAEAHACAIQADAIgCAJQgBAIgEAHQgDAGgFAGIgMAIQgHADgHACIgOACIgOACQgIgBgJgCg");
	this.shape_21.setTransform(-156.6,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#660000").ss(3,1,1).p("A4IjLMAwRAAAQBBAAAvAvQAuAuAABBIAABbQAABBguAvQgvAuhBAAMgwRAAAQhBAAgvguQgugvAAhBIAAhbQAAhBAuguQAvgvBBAAg");

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF3333").s().p("A4IDLQhBAAgvgtQgugvAAhBIAAhbQAAhAAugvQAvgvBBABMAwRAAAQBBgBAvAvQAuAvAABAIAABbQAABBguAvQgvAthBAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.question_mc, new cjs.Rectangle(-173.6,-21.8,345.4,43.7), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993300").s().p("Aj9E+IgVk0IgGhPIgEhCIACgUQARg5BAguQBXg7B2AAQB6AABVA7QBWA7ABBUIAAACQAAARgEAOIgFBWIgZE6gAi3joQg2AmgOAwQgDAFAAAIIAfGkIHMAAIAemUQgDhEhGgvQhPg2hvAAQhtAAhOA2gAF2DnIgQgbQAAgnBFAVQBFAWAAATQAAAdgrAAQg0AAgbgZgAosDZQgRgCgIgMQgFgIAAgIQAAgGAFgIQAIgNARgBICsAAQASABAFANQAEAFAAAJQAAAcgbACgAAKCdIgEgGIAAlAQAWAGAFA8QABASAABQIgBCiQgIADgFABQgIgBgCgDgAGeB5IgEgOQAAgcA+AAQA/AAAAAcQAAANgHAEQgMAMgsAAQgvAAgLgPgAm3BzIgEgMQAAgeAoAAQApAAAAAeQAAAbgpAAQgbAAgJgPgAqhBcIgEgOQAAgbA/AAQA+AAAAAbQAAANgEAFQgOALgsAAQgvAAgMgPgAJfBLIgCgOQAAgcAkAAQAlAAAAAcQAAAeglAAQgaAAgIgQgABdAzIgCgIQAGgKAEgCQAHgFAZAHQAAABAAAAQABAAAAAAQABABAAAAQABAAABAAIAAAUQgJAEgOAAQgNAAgIgIgAg8AvQgCAEAAgNQAAgIANgEQAPgEAaAEQAAABAAAAQAAABAAAAQABABAAAAQABABABAAIAAAIQAAAKgIADQgGAEgNAAQgVAAgHgIg");
	this.shape.setTransform(-9.5,67.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000099").s().p("AF6I9QANg3ADhEQABgLAAgrIBPgXQgeBSgxB2gAEmI9QgMgigbhbIgHgNIAMABIBqggIAAAxQgKAYgFAbIgBAzQAAAJAGAJgAnCnQQgRgQgGgRIABAAQASAIADAAIAgAAIAqAAQASgGAfgcIAsgmQANgKALgBQAIARAAASQAAAqgjAfQghAegwAAQgwAAgigeg");
	this.shape_1.setTransform(2.5,-4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AC+GcQAAgNADgMIAAgBIABgIQBRgtAmgJQAJgDAYgCIAACiIgcAIQhGATgZAWQgghpgBgNgAF3G9QAAhDgBgKQgDgcgEgVQAVgCAbAAQA7AAAPAEIAQAFQgRA7gaBMQguAGgpAJIAAgfgAnrmnIgNAAIAAgCQAAgrAigfQAigeAwAAQAvAAAiAeQAMALAHALQgfgCgqAdQg0AjglAAQgcAAgNgIg");
	this.shape_2.setTransform(4.5,-15.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9900").s().p("AmfGgIgblyIABgDQAMggAlgZQBDguBeAAQBeAABFAuQAtAfAGAsIgbFjgAjlEeIADAFQACAEAIABQAGgBAIgEIABijQAAhQgBgSQgGg6gVgHgAiHClQgDADgHAKIACAIQAIAIAOAAQANAAAJgFIAAgTQgBAAAAgBQgBAAAAAAQgBAAAAgBQAAAAAAAAQgQgFgIAAQgGAAgDACgAkNC9QAOAAAGgDQAHgEAAgKIAAgIQAAAAgBAAQgBgBAAAAQAAgBgBAAQAAgBAAAAQgagFgOAFQgOADAAAIQAAAOACgFQAHAIAVAAgACcAMQgHgMgqiFQgtiMAAgRQAAgQAEgPQADgPAFgHQAAgBAAAAQAAAAAAgBQAAAAAAAAQABAAAAAAQBdg2ApgJQAVgHBYAAQBBAAAQAEQAXAFAVAQQgQBAgfBXQADACAAAHIgFACIgHANQgfBag5CJgAEWjCQAAArgBALQgDBEgNA3IARAAQAxh2AehSIhPAXgACKiOQAbBbAMAiIA+AAQgGgJAAgJIABgzQAFgbAKgYIAAgxIhqAgIgMgBIAHANgADYl6QgmAJhRAtIgBAIIAAABQgDAMAAANQACANAfBpQAZgWBGgTIAcgIIAAiiQgYACgJADgAEOl/QAFAVACAcQABAKAABDIAAAfQAqgJAtgGQAbhMAQg7IgQgFQgPgEg7AAQgaAAgWACg");
	this.shape_3.setTransform(14.2,54.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DFAD37").s().p("AgUBsQg4gwgbgmQAMgeAFgnQAOhFAZAAQARAAA0ANQA1APAdAAQhNBPghB/IgOgKg");
	this.shape_4.setTransform(64.6,-21);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("ArBLOIgKAAIhKndQA2gbBygxQCOg9CAgxQBUggCohmQB+hNBBAAQAoAACsBfQBrA6BvBFQBcA2CwBFIhRILQgxAOgzAAQj9gFg9ABIAGhWQADgPAAgRIAAgCQgBhUhVg7QhWg7h5ABQh3gBhWA7QhAAugRA6IgDAUQgCAKAAALQAAAWAHAWIAGBQIi3ACQiMAAhzgMgAHECuQgpAJhdA2QAAAAgBABQAAAAAAAAQAAAAAAABQAAAAAAABQgFAHgDAPQgEAOAAARQAAAQAtCNQAqCEAHAOICPAAQA5iKAfhaIAHgOIAFgBQAAgIgDgBQAfhYAQhAQgVgQgXgEQgQgFhBABQhYAAgVAGgAn9oeQC2i7BsAAQAHAABsA0QCLBDBxAtQC3BKChAoIgbEhIkXieQjhhdi0B8IkECRgAlgqAQgtAoAAA5QAAA4AtAnQAtAqBAAAQA/AAAtgqQAtgnAAg4QAAg5gtgoQgtgqg/AAQhAAAgtAqg");
	this.shape_5.setTransform(-10.1,-4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC6600").s().p("AESOWIAZk6QA9gBD9AFQAzAAAxgOIglDxQgNBTh5AAgAFxMlIAQAbQAbAYAzAAQArAAAAgdQAAgThEgVQgZgIgPAAQgdAAAAAagAGlLEIADANQALAPAwAAQArAAANgLQAHgFAAgMQAAgcg/AAQg+AAAAAcgAJnKWIADANQAHAQAaAAQAlAAAAgdQAAgcglAAQgkAAAAAcgAodOWQh6AAgMhTIgkjrIAKAAQBzAMCMAAIC3gCIAVE0gAo7MFQgEAIAAAHQAAAHAEAIQAIAMARACICsAAQAbgCAAgbQAAgKgDgFQgGgMgSgBIisAAQgRABgIAMgAmwK/IADANQAJAOAbAAQAqAAAAgbQAAgegqAAQgnAAAAAegAqaKnIADANQANAQAvAAQAsgBANgKQAFgGAAgMQAAgbg+AAQg/AAAAAbgAseA0IgBgKIJIlIQC0h7DhBdIJiFbQAAAKgCALIgEATQiwhFhcg1QhvhGhrg6QishfgoAAQhBAAh+BNQioBnhUAgQiAAxiOA8QhyAxg2AbgAJnjFQASgcAOgoQAbAlA4AwIAOAKIgJArgAlco2QgtgnAAg4QAAg5AtgoQAtgqBAAAQA/AAAtAqQAtAoAAA5QAAA4gtAnQgtAqg/AAQhAAAgtgqgAlApNQAiAeAwAAQAwAAAhgeQAjgfAAgrQAAgRgIgRQgLABgNAKIgsAmQgfAcgSAGIgqAAIggAAQgDAAgSgIIgBAAQAGAQARARgAlArgQghAeAAArIAAADIAMAAQANAHAdABQAlgBAzgiQAqgdAgACQgIgLgMgLQghgegwAAQgwAAgiAegACYqrQhxgtiLhDQhsg0gHAAQhsAAi2C7IgDgtIAthAQA4hOAWgYQAqguAjAAQAEAAD1BeIEdBqQA3AUDMAoQAKADAJACIgIBTQihgoi3hKg");
	this.shape_6.setTransform(-10.5,7.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("Ak/OhIgemlQAAgIACgFQAPgwA2gmQBNg2BuAAQBvAABPA2QBFAvAEBEIgfGVgAj7HtQglAagMAfIgBAEIAbFyIFyAAIAblkQgHgsgsgfQhFguheAAQhdAAhDAugAK2BIIpilaQjhhei0B8IpIFHIg7AiIgEAAQAAgTgBgHIgFgEIKNlwQC0h7DhBcIN7H8IgDAEQgLAPgHAGgArWpEQAsg9BNhuQB6ixAmAAQA+AACTBHQDIBhBIAYQCXA2EHBCQAyAOAlAQQg7gIhQgRQgKgBgJgDQjMgpg3gTIkdhrQj1hdgFAAQgiAAgqAuQgWAYg4BNIgtBAQgVAZgQAPQgkAkgiAIQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAABg");
	this.shape_7.setTransform(0.1,3.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("APQJlIt7n8Qjhhci0B6IqNFxQgIgFgQAAQgBgOATgjQAKgXAFgOIGAjZIEEiRQC0h7DhBcIEXCeICSBTIB3BGIFAC2QAFAMAMAIQAQAOAQAAIAAAoIgUAdgAsMibIAAgvQAkgfCMjNQB6ixAmABQA+gBCTBJQDIBgBIAZQCXA1EHBBQC5A4AAA9QAAARgDAGIAAADQgngBg4gIQglgPgygPQkHhBiXg2QhIgZjIhhQiThHg+AAQgmAAh6CxQhNBugsA9IgBACQgGADAAADg");
	this.shape_8.setTransform(0,-38.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-99.8,-99.6,199.7,199.3), null);


(lib.ch1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D2A25").s().p("ApkQLIh1glQhngog/hYIgBgDIAAgBQgDgDgCAAQhviHASi5IA1hYQBHhHB8AiQgKgHgLgEIDWAAIAKAEQgZgWgOgUQggg2gKg8QgQhZBhiHIABgBIgLgDQBnhpBvhBIAAgVQANgIAZgDQgBAAgBAAQgBAAAAAAQgBgBAAAAQAAAAAAgBQibiWAAjSIAAgPQgoAZgwALQijgDgmieQgShRgHhSQgIi/C+AKQCgAHCLBIIAeAJIABAEQCLhuC7AAQCSAAByA9IALAKQAbAMAaAVIAPgHQCLhICigHQC9gKgIC/QgGBSgRBRQgoCeijADQglgKgfgPIAAAEQAADKiOCPQAuAgAnAdQAPAOARANQBDA5AuA5QAAABAAAAQAAABAAAAQABABAAAAQAAABABAAIAAACIACACQCcDUixCoIBuAAQgLAJgKAEQB6gfBGBDQAoAoAMA0QATC3hrCGIgKAHQhABZhlAnQgzAahEAOQhhANg9gnIABAFIhyg6IANAPIgxghIiRhLIk0AAIAAgFIgCAAIACAFIizBdIgrAeQABgFAHgGIhdAtIAAgDQgrAbg9AAQgZAAgdgFg");
	this.shape.setTransform(-4.3,1.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvQPRQmVmVAAo8QAAo7GVmVQGVmVI7AAQI8AAGVGVQGVGVgBI7QABI8mVGVQmVGVo8gBQo7ABmVmVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AwBQCQmompgBpZQABpYGompQGpmoJYgBQJZABGpGoQGoGpABJYQgBJZmoGpQmpGopZABQpYgBmpmog");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy, new cjs.Rectangle(-145,-145,290.1,290.1), null);


(lib.ch1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D2A25").s().p("AqHPkQh5AAgNhTIh6sPIgBgKIg7AiIgEAAQAAgTgCgHIgEgFQgIgEgRgBQABgOARgjQAMgXAEgOIGAjYIghm7QgVAZgQAOQgkAkgiAIQAAAAgBAAQAAABAAAAQgBAAAAAAQAAABAAAAIgEgCIgBACQgFADAAADIgxAAIAAgvQAmgfCLjMQB5ixAnAAQA+AACTBIQDJBgBGAZQCZA1EFBCQC6A3AAA+QAAAQgEAGIAAADQglgBg5gIQg7gHhRgRIgiF0ICSBTQASgcANgoQAMgfAFgnQAOhGAaAAQAQAAA1AOQA1AOAdAAQhNBPghCAIgJArIE/C1QAEAMAMAJQASANAQAAIAAApIgVAcIgCgDIgEAFQgKAPgIAFIkEiUQAAAKgCALIh5MPQgOBTh4AAg");
	this.shape.setTransform(2.3,-3.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvQPRQmVmVAAo8QAAo7GVmVQGVmVI7AAQI8AAGVGVQGVGVgBI7QABI8mVGVQmVGVo8gBQo7ABmVmVg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AwBQCQmompgBpZQABpYGompQGpmoJYgBQJZABGpGoQGoGpABJYQgBJZmoGpQmpGopZABQpYgBmpmog");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1, new cjs.Rectangle(-145,-145,290.1,290.1), null);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1copy();
	this.instance.parent = this;
	this.instance.setTransform(374.4,0,1.14,1.14);

	this.instance_1 = new lib.ch1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-400.3,0,1.14,1.14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-565.7,-165.3,1105.5,330.7);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(2.5,-3.4,1.14,1.14);

	this.instance_1 = new lib.ch1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,1.14,1.14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy, new cjs.Rectangle(-165.3,-165.3,330.7,330.7), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-2,0.1,0.97,0.97,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1},9).to({scaleX:0.97,scaleY:0.97},10).to({scaleX:1,scaleY:1},10).to({scaleX:0.97,scaleY:0.97},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200.8,-34.9,397.9,69.9);


(lib.quecopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(2.3,-3.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AvQPRQmVmVAAo8QAAo7GVmVQGVmVI7AAQI8AAGVGVQGVGVgBI7QABI8mVGVQmVGVo8gBQo7ABmVmVg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("Aw0Q1Qm+m+AAp3QAAp2G+m+QG+m+J2AAQJ3AAG+G+QG+G+AAJ2QAAJ3m+G+Qm+G+p3AAQp2AAm+m+g");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,51,0.698)").s().p("AwLQMQmtmtAApfQAApeGtmtQGumtJdAAQJfAAGtGtQGtGtAAJeQAAJfmtGtQmtGtpfAAQpdAAmumtg");
	this.shape_2.setTransform(0,0.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.quecopy3, new cjs.Rectangle(-152.3,-152.3,304.6,304.6), null);


(lib.que = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-17.9,-3.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AvQPRQmUmVAAo8QAAo7GUmVQGUmVI8AAQI8AAGVGVQGVGVAAI7QAAI8mVGVQmVGVo8gBQo8ABmUmVg");
	this.shape.setTransform(-20.2,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("Aw1Q1Qm9m+AAp3QAAp2G9m+QG/m+J2AAQJ3AAG/G+QG9G+AAJ2QAAJ3m9G+Qm/G+p3AAQp2AAm/m+g");
	this.shape_1.setTransform(-20.2,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_4
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,51,0.698)").s().p("AwLQMQmtmtAApfQAApeGtmtQGumtJdAAQJfAAGtGtQGtGtAAJeQAAJfmtGtQmtGtpfAAQpdAAmumtg");
	this.shape_2.setTransform(-20.2,0.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.que, new cjs.Rectangle(-172.5,-152.3,304.6,304.6), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy3();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.quecopy3();
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,1.14,1.14,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.17,scaleY:1.17},9).to({scaleX:1.14,scaleY:1.14},10).to({scaleX:1.17,scaleY:1.17},10).to({scaleX:1.14,scaleY:1.14},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-173.6,-173.6,347.2,347.2);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4("synched",25);
	this.instance.parent = this;
	this.instance.setTransform(-61.7,37);

	this.instance_1 = new lib.Symbol2("synched",25);
	this.instance_1.parent = this;
	this.instance_1.setTransform(63.9,-178.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238.4,-213.6,502.9,427.3);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(260.7,549.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(337).to({_off:false},0).wait(33).to({startPosition:13},0).to({alpha:0,startPosition:0},7).wait(4));

	// Layer_9
	this.instance_1 = new lib.Symbol1copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(413.6,616.8,1,1,-17.2,0,0,190.5,64.3);

	this.instance_2 = new lib.Symbol5copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(256.7,536.7);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1}]},245).to({state:[]},34).to({state:[{t:this.instance_2}]},41).to({state:[{t:this.instance_2}]},20).to({state:[{t:this.instance_2}]},24).to({state:[{t:this.instance_2}]},7).wait(10));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(320).to({_off:false},0).to({scaleX:1.1,scaleY:1.1,x:269.7},20).wait(24).to({alpha:0},7).wait(10));

	// Layer_8
	this.instance_3 = new lib.Symbol1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(252.7,323.2,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(207).to({_off:false},0).to({_off:true},40).wait(134));

	// Layer_7
	this.instance_4 = new lib.Symbol3();
	this.instance_4.parent = this;
	this.instance_4.setTransform(670.3,318,1.14,1.14);
	this.instance_4.alpha = 0.699;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(157).to({_off:false},0).to({x:1045.4,y:532.1},21).wait(34).to({x:1045.3,alpha:0.5},0).to({_off:true},1).wait(168));

	// Layer_5
	this.instance_5 = new lib.Symbol3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(670.3,318,1.14,1.14);
	this.instance_5.alpha = 0.699;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(157).to({_off:false},0).to({x:259.2,y:532.1},21).wait(34).to({x:259.1,alpha:0.5},0).to({_off:true},1).wait(168));

	// Layer_6
	this.instance_6 = new lib.Symbol3();
	this.instance_6.parent = this;
	this.instance_6.setTransform(258.7,533.8,1.14,1.14);
	this.instance_6._off = true;
	this.instance_6.filters = [new cjs.ColorFilter(0, 0, 0, 1, 51, 51, 51, 0)];
	this.instance_6.cache(-102,-102,204,203);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(279).to({_off:false},0).to({x:648.2,y:318},35).wait(6).to({alpha:0},7).to({_off:true},53).wait(1));

	// Layer_4
	this.instance_7 = new lib.Symbol4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(643,321.9);

	this.instance_8 = new lib.Symbol2("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(766,106.9);

	this.instance_9 = new lib.Tween4("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(703.4,285);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8},{t:this.instance_7}]},114).to({state:[{t:this.instance_9}]},98).to({state:[]},1).wait(168));

	// Layer_3
	this.instance_10 = new lib.Tween2("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(657,536.7);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(79).to({_off:false},0).to({alpha:1},5).to({startPosition:0},221).to({startPosition:0},15).to({alpha:0},8).wait(53));

	// Layer_2
	this.instance_11 = new lib.que();
	this.instance_11.parent = this;
	this.instance_11.setTransform(667.8,321.9,1.14,1.14,0,0,0,0.1,0);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(38).to({_off:false},0).to({alpha:1},5).to({regY:0.1,y:322},57).wait(57).to({regY:0,y:321.9},163).to({alpha:0},8).wait(53));

	// Layer_1
	this.questiontxt = new lib.question_mc();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(640.1,107.1,1.966,1.966,0,0,0,0,0.1);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(14).to({_off:false},0).to({alpha:1},6).wait(300).to({alpha:0},8).wait(53));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(554.5,294.2,1442,876);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;