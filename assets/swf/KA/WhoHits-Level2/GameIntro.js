(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgRChQgEgBgDgDIgFgHQgCgEABgFQgBgEACgEQACgFADgDIAHgEQAEgCAEAAQAFAAAEACIAGAEQADADACAFQABAEABAEQgBAFgBAEIgFAHQgDADgDABQgEADgFAAQgEAAgEgDgAgeBdQgCgJAAgHQAAgNAEgJQAEgKAGgHQAHgJAIgGIAQgOIARgNQAIgHAGgJQAHgIADgKQAFgKAAgNQgBgNgEgMQgEgMgIgKQgIgJgLgGQgMgGgNAAQgNAAgLAGQgKAFgIAIQgHAJgEAMQgDAMAAAMIAAAJIABALIgjAFIgDgNIAAgNQAAgTAGgSQAIgSAMgNQANgNARgIQASgIAVAAQAUAAATAIQARAHANANQANANAHATQAIASgBAVIgBAVQgBAKgFAKQgEAKgHAKQgHAIgLAJIgTAPQgLAHgJAJQgJAJgFALQgFAMADAQg");
	this.shape.setTransform(225.4,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AA1CWQAGgTACgSIAEggIAAgfQgBgWgGgOQgHgOgJgKQgJgJgLgEQgLgFgLAAQgKABgMAGQgJAFgMAKQgLAJgKASIgBCDIgnABIgDkwIAugCIgCB4QALgLAMgHQANgHAKgEQANgEALgBQAWAAASAIQASAIANAPQANAPAIAVQAIAUABAbIgBAcIgBAdIgDAbQgCANgDAKg");
	this.shape_1.setTransform(201.2,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgqClQgKgCgJgEQgJgEgJgHQgJgGgHgJQgHgJgFgNIAmgQQAEAIAFAGQAFAHAHAEQAGAEAGACIANAEQAOADAPgCQAOgCAKgGQAKgGAIgHQAHgHAEgIIAHgPIAEgNIAAgJIAAgaQgLAMgNAGQgNAGgLADQgMADgMAAQgXAAgTgIQgUgIgPgPQgPgPgIgUQgIgVAAgbQAAgbAJgVQAJgVAQgOQAPgPATgHQATgIATAAQANABANAFQAMAEANAGQAOAHALAMIABgnIAmABIgBDiQAAANgDANQgDANgGAMQgHAMgJAKQgJALgMAIQgMAIgPAGQgPAFgRABIgDAAQgWAAgTgFgAgdh3QgMAFgJALQgIAKgFAOQgFAOAAAQQAAAQAFAOQAFAOAJAJQAIAJANAGQAMAFAOAAQAMAAANgEQAMgFAKgIQAKgHAHgLQAHgMACgOIAAgZQgCgPgHgLQgHgMgKgIQgLgIgMgFQgNgEgMAAQgOAAgMAGg");
	this.shape_2.setTransform(173.5,9.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgVBuQgRgBgOgGQgMgGgJgKQgIgKgGgNQgFgOgDgPQgCgPgBgPIgBgdQAAgVABgVQACgVAEgWIAoABQgDAZgBATIgBAjIAAARIACAVQABALACALQAEAMAFAJQAEAIAIAFQAIAFALgCQAOgCARgTQARgTASglIgBgzIgBgcIgBgRIAqgEIADA9IACAwIABAnIAAAcIABAqIgtABIgBg5QgGAMgIAKQgIALgKAIQgKAHgLAFQgJAEgLAAIgCAAg");
	this.shape_3.setTransform(148,5.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgpBsQgTgIgOgPQgPgQgIgVQgIgWAAgaQAAgZAIgVQAIgWAPgPQAOgPATgJQATgIAWAAQAWAAATAJQAUAJANAPQAPAQAJAVQAIAVAAAYQAAAZgIAVQgJAWgPAPQgNAQgUAJQgTAIgWAAQgWAAgTgIgAgchJQgMAHgIAMQgIAMgEAOQgDAPAAANQAAAOAEAPQAEAOAIAMQAHALANAIQAMAHAPAAQAQAAAMgHQANgIAHgLQAIgMAFgOQADgPAAgOQAAgNgDgPQgEgOgIgMQgIgMgMgHQgMgHgRAAQgQAAgMAHg");
	this.shape_4.setTransform(123.4,5.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AhZAxIgCgxIgBgmIgBgdIgBgqIAtAAIAAAYIABAXIAAAYIAOgUQAIgLALgJQAKgKALgHQAMgHAOgCQAQgBANAHIAMAJQAFAFAFAIQAEAJAEALQADALABAQIgnAOIgBgQIgFgNIgEgIIgHgGQgHgFgIABQgGAAgGAEIgMAKIgNAOIgMAPIgNAQIgJAOIAAAdIABAbIABAWIABAQIgqAFIgDg9g");
	this.shape_5.setTransform(101.4,5.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AA1CWQAGgTACgSIAEggIAAgfQgBgWgGgOQgHgOgJgKQgJgJgLgEQgLgFgLAAQgKABgMAGQgJAFgMAKQgLAJgKASIgBCDIgnABIgDkwIAugCIgCB4QALgLAMgHQANgHAKgEQANgEALgBQAWAAASAIQASAIANAPQANAPAIAVQAIAUABAbIgBAcIgBAdIgDAbQgCANgDAKg");
	this.shape_6.setTransform(76.2,0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgXgWIhFABIABgkIBFgCIABhXIAmgCIgBBYIBNgCIgDAlIhKACIgCCrIgpABg");
	this.shape_7.setTransform(53.1,1.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgIQgJgGgIgGIATgeIAWALIAYAJQALAFANABQALADAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgCgBgFIgBgIQgCgEgEgDQgEgEgHgDQgGgDgJgCQgJgCgNAAQgQgCgRgCQgQgDgNgHQgNgGgJgIQgIgLgCgOQgBgOADgNQADgLAHgJQAHgKAJgHQAKgHALgEQAMgFANgCQAMgCAMAAIATAAIAWADQALADAMAEQALAFALAHIgOAkQgNgHgMgDIgVgHIgUgDQgegCgRAJQgRAIAAAQQAAAMAGAGQAHAFALADIAaADIAfAEQAUADANAFQANAFAIAIQAIAIAEAJQADAKAAALQAAASgHANQgIANgMAIQgNAIgRAEQgQADgSABQgRABgTgEg");
	this.shape_8.setTransform(21.4,5.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgsBqQgUgIgOgOQgOgPgHgUQgHgUAAgZQAAgYAIgWQAJgVAOgQQAPgQAUgJQATgJAWAAQAVAAASAIQASAIAOAOQAOAOAKAUQAJASACAXIiqAYQABAOAGALQAFAMAJAHQAIAIAMAEQALAFAMAAQALgBAKgDQAKgDAJgHQAJgFAGgKQAHgJADgNIAmAIQgGASgKAPQgKAOgNALQgOAKgQAGQgQAGgSAAQgZAAgUgIgAgQhMQgKADgJAHQgJAHgIALQgHALgDASIB2gOIgBgFQgIgTgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_9.setTransform(-1.5,5.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgIQgJgGgIgGIATgeIAWALIAYAJQALAFANABQALADAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgCgBgFIgBgIQgCgEgEgDQgEgEgHgDQgGgDgJgCQgJgCgNAAQgQgCgRgCQgQgDgNgHQgNgGgJgIQgIgLgCgOQgBgOADgNQADgLAHgJQAHgKAJgHQAKgHALgEQAMgFANgCQAMgCAMAAIATAAIAWADQALADAMAEQALAFALAHIgOAkQgNgHgMgDIgVgHIgUgDQgegCgRAJQgRAIAAAQQAAAMAGAGQAHAFALADIAaADIAfAEQAUADANAFQANAFAIAIQAIAIAEAJQADAKAAALQAAASgHANQgIANgMAIQgNAIgRAEQgQADgSABQgRABgTgEg");
	this.shape_10.setTransform(-25.1,5.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgIQgJgGgIgGIATgeIAWALIAYAJQALAFANABQALADAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgCgBgFIgBgIQgCgEgEgDQgEgEgHgDQgGgDgJgCQgJgCgNAAQgQgCgRgCQgQgDgNgHQgNgGgJgIQgIgLgCgOQgBgOADgNQADgLAHgJQAHgKAJgHQAKgHALgEQAMgFANgCQAMgCAMAAIATAAIAWADQALADAMAEQALAFALAHIgOAkQgNgHgMgDIgVgHIgUgDQgegCgRAJQgRAIAAAQQAAAMAGAGQAHAFALADIAaADIAfAEQAUADANAFQANAFAIAIQAIAIAEAJQADAKAAALQAAASgHANQgIANgMAIQgNAIgRAEQgQADgSABQgRABgTgEg");
	this.shape_11.setTransform(-47.5,5.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AA9BxIADgcQgYAPgXAHQgWAHgVAAQgOAAgNgDQgNgEgLgIQgKgHgGgMQgGgMgBgQQABgQAEgNQAFgMAIgKQAJgJALgHQALgHANgEQANgEAOgCQANgCAOAAIAZABIAUACQgDgKgEgJQgDgKgGgHQgGgIgIgFQgJgFgMAAQgIAAgJADQgJACgMAGQgMAGgOAKQgOAKgQARIgYgdQATgSARgLQASgMAPgGQAPgHANgCQAMgCALAAQASAAAOAGQAPAGALALQAKALAIAOQAIAPAEARQAFAQACASQACARAAASQAAATgCAWIgHAvgAgJAAQgQADgNAIQgMAIgGALQgFALADAPQACAMAJAFQAHAFAMAAQALAAAOgEIAagKQANgFALgHIAUgLIABgUIgBgUQgKgCgLgCQgKgCgMAAQgRAAgQAFg");
	this.shape_12.setTransform(-71.7,4.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AhsifIArgDIAAAeQALgMAMgFQANgHAKgDQAMgDALgCQANAAANAFQAOADALAIQAMAGAKAKQAKAKAIAMQAHANAEANQADAPAAAPQgBARgEAPQgFAPgHAMQgIAMgKAKQgJAKgMAHQgLAHgMAEQgMADgMABQgLgBgMgDQgLgDgNgFQgNgHgNgKIgBCGIglABgAgXh3QgKAFgJAGQgJAHgGAJQgGAKgDAKIAAAlQADANAGAKQAHALAIAFQAJAHAKADQAKADAKABQANABAMgGQAMgGAKgIQAJgKAFgMQAGgNAAgOQABgPgEgNQgFgNgIgLQgJgJgNgHQgMgFgOAAQgMAAgLAEg");
	this.shape_13.setTransform(-96.3,10.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AAGgnIgvCJIg7AFIgmjMIAogDIAfClIA6ilIAlACIAoCnIAbipIAsACIgnDOIg7ABg");
	this.shape_14.setTransform(-135.9,5.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgpBsQgTgIgOgPQgOgQgJgVQgIgWAAgaQAAgZAIgVQAJgWAOgPQAOgPATgJQATgIAWAAQAVAAAUAJQATAJAPAPQAOAQAIAVQAJAVAAAYQAAAZgJAVQgIAWgOAPQgPAQgTAJQgUAIgVAAQgWAAgTgIgAgdhJQgMAHgHAMQgIAMgDAOQgEAPAAANQAAAOAEAPQAEAOAHAMQAJALALAIQAMAHAQAAQARAAAMgHQALgIAIgLQAJgMADgOQAEgPAAgOQAAgNgEgPQgDgOgIgMQgIgMgMgHQgMgHgRAAQgQAAgNAHg");
	this.shape_15.setTransform(-163.2,5.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AhZAxIgCgxIgBgmIgBgdIgBgqIAtAAIAAAYIABAXIAAAYIAOgUQAIgLALgJQAKgKALgHQAMgHAOgCQAQgBANAHIAMAJQAFAFAFAIQAEAJAEALQADALABAQIgnAOIgBgQIgFgNIgEgIIgHgGQgHgFgIABQgGAAgGAEIgMAKIgNAOIgMAPIgNAQIgJAOIAAAdIABAbIABAWIABAQIgqAFIgDg9g");
	this.shape_16.setTransform(-185.3,5.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AhZAxIgBgxIgBgmIgBgdIgCgqIAtAAIABAYIAAAXIAAAYIAOgUQAIgLAKgJQALgKAMgHQAMgHAOgCQAQgBANAHIAKAJQAGAFAEAIQAGAJACALQADALACAQIgnAOIgCgQIgDgNIgGgIIgGgGQgGgFgJABQgGAAgGAEIgMAKIgNAOIgNAPIgLAQIgKAOIABAdIAAAbIABAWIABAQIgrAFIgCg9g");
	this.shape_17.setTransform(-207.4,5.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AA+BxIACgcQgYAPgXAHQgWAHgVAAQgOAAgNgDQgNgEgLgIQgKgHgGgMQgGgMAAgQQgBgQAFgNQAFgMAJgKQAIgJALgHQALgHANgEQANgEAPgCQANgCAOAAIAYABIAUACQgDgKgEgJQgDgKgGgHQgGgIgJgFQgIgFgMAAQgHAAgKADQgJACgMAGQgMAGgOAKQgOAKgQARIgYgdQATgSASgLQARgMAPgGQAQgHANgCQAMgCAKAAQASAAAPAGQAOAGALALQALALAHAOQAHAPAFARQAFAQACASQACARAAASQAAATgCAWIgHAvgAgIAAQgRADgMAIQgNAIgFALQgHALAEAPQACAMAJAFQAHAFAMAAQALAAAOgEIAZgKQANgFAMgHIAUgLIAAgUIgBgUQgJgCgLgCQgLgCgKAAQgSAAgPAFg");
	this.shape_18.setTransform(-232.7,4.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(3,1,1).p("EgkwgEPMBJhAAAQCgAAAACgIAADfQAACgigAAMhJhAAAQigAAAAigIAAjfQAAigCgAAg");
	this.shape_19.setTransform(0,2.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("EgkwAEPQigAAAAigIAAjdQAAigCgAAMBJhAAAQCgAAAACgIAADdQAACgigAAg");
	this.shape_20.setTransform(0,2.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-252.7,-31.7,505.5,63.6);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.setscopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhtCIQgugxAAhUQAAhYAvgxQAugyBJAAQBDAAAoAnQAZAXAMArIhIARQgGgcgUgQQgUgQgdAAQgnAAgYAcQgZAdAABAQgBBCAZAdQAZAdAmABQAcgBAVgSQAVgSAJgoIBGAXQgQA6gmAdQglAcg5AAQhHAAgugxg");
	this.shape.setTransform(-54.1,-295.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AiVCzIAAllICPAAQApAAAWAEQAUADARAMQAQAKALATQALATAAAXQAAAZgNAWQgOAUgXALQAhAJARAXQASAWAAAfQAAAYgMAYQgLAXgTANQgUAOgdADQgSAChEABgAhNB3IBDAAQAnAAAKgDQAQgCAKgLQAKgMAAgTQAAgQgIgMQgHgLgPgFQgPgFgxAAIg6AAgAhNgkIAwAAQApAAAKgBQATgCAKgKQALgLAAgRQAAgRgJgKQgJgLgTgCQgKgBgygBIgqAAg");
	this.shape_1.setTransform(-224.4,-111.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABlCzIgfhSIiPAAIgdBSIhNAAICMllIBLAAICPFlgAgyAmIBgAAIgwiEg");
	this.shape_2.setTransform(-237.3,179.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#793F06").s().p("AjrhWILABvIupA+g");
	this.shape_3.setTransform(304.7,102.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF9900").s().p("AkHAXIACgBIgnkCIJZHZg");
	this.shape_4.setTransform(321.5,81.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FBAA30").s().p("AE5FiIpZnaIgjjsIKHLIIgCABg");
	this.shape_5.setTransform(320.3,69.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9E5104").s().p("AlkgCICNhqIIzDVIAJADIgJABg");
	this.shape_6.setTransform(316.8,94.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AuSkTIAAiJIclLCIgsB3g");
	this.shape_7.setTransform(239.9,59.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#A17446").s().p("Ag6IPQg0gGgDgMQgFgMAFgLQADgMgBAAIAAgjQABhKAHgSQAGgQAMgbIATgpQA/ibAJhSQAIhPgehmQgIgjg1iNQgYg/AIgXQAEgQAKgKQAQgPAdgKQBLgtAWgIQAdgMAKAMQALAIAAAbQAAAWACAVQAMGrhEEkQgKApgMAoQgVBGgOAhQgPAgAEATQACARgiAAIgRgBg");
	this.shape_8.setTransform(102.1,-99.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_9.setTransform(172,-137.5,4.09,4.09,0,-48.2,131.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_10.setTransform(169.7,-142.9,4.09,4.09,0,-48.2,131.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#A8D64A").s().p("ApPIvQkwiNismBQhDiSgxjGIAAgHQAdiXA/hwQAQgbATgZQAGBnAaBmQA+DvCXCmQDRDiFfAzQFAAvFMhuIBiglQDZhcB9iSQCdi2AGkKQACg2gEg1QCTDEAjEhQkAEgk+DSQg4Alg3AjQjuCNjuA3Qh+Ach1AAQjHAAishRg");
	this.shape_11.setTransform(93.3,4.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#D1EC93").s().p("AgGD6QhcgQhEhMQg9hMgfgkQA+gZAvg4QAUgXAhgzQAggzAVgWQAggpArgVQAxgUAnAOQA7AVAnBYQAsBygDBJQgLBqhNA/Qg9Aqg/AAQgbAAgagHg");
	this.shape_12.setTransform(133.1,-26.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#94CD05").s().p("AC0PeQgtgFhagQQhZgQgvgEQgxgEhoAGQhhAGgxgGQjEgNiuh6Qihh1hkizQiikhgTnRQgKjhAliwIAAAGQAxDHBDCTQCsF/ExCNQEQCBFVhMQDvg3DtiNQA3gjA4glQE+jREAkhIAAAFQAxFoiNFWQiNFakeDfQhUA9hSArQimBSiaAAIgHAAgAhhsMQhmgZh1heQA7AOBEACQCAALDrgfQBNgKArgPQBAgUAjgpQhGB/iLA7QhXAmhWAAQg2AAg2gPg");
	this.shape_13.setTransform(92.5,35.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C6E57E").s().p("AjhJPQlfgzjRjjQiXimg9juQgbhmgGhnQBWh2CBhEQBhg1BhgIQBIgIBFAQQA5AOBOAlQAtATBUArQBMAYBHgCQArgSAugZQD4iDBbgSQBSgSBUAIQBOAGBNAcQCcA8B2B6QAnAnAjAtQAEA1gCA2QgGEJidC2Qh9CTjZBcIhiAlQjoBNjjAAQhhAAhggOgAHIjUQgsAVggApQgVAWggAzQghAzgUAXQgvA4g+AZQAfAkA9BMQBEBMBcAQQBcAZBWg8QBNg/ALhqQADhKgshxQgnhYg7gVQgQgGgSAAQgZAAgdAMgAhPh+QCMAnCNg9QCLg8BGh/QgjAphAAVQgrAOhNAKQjrAfiAgKQhEgCg7gOQB1BeBmAYg");
	this.shape_14.setTransform(90.7,-29.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF9900").s().p("AkLkvIEUkNIiLG6IDAifIhJFcICwiqIh6GXIDhjGIiuHag");
	this.shape_15.setTransform(-14.5,-226.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC6600").s().p("AhXDgIEpgRIl0jNID0gCIkpjEID4gXImZjVIGEgGIADgCIFqNrIgCAGg");
	this.shape_16.setTransform(-42.8,-213.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_17.setTransform(25.1,-92.7,0.95,0.95,46.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF9900").s().p("Ao7gIIG6CLIigjAIFcBJIiqiwIGWB6IjFjhIHaCuItrFpg");
	this.shape_18.setTransform(-172.4,142.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC6600").s().p("Am2gLIgCgDINrlqIAGACIjYHOIgRkpIjNF0IgDj0IjDEpIgYj4IjWGZg");
	this.shape_19.setTransform(-158.7,171.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_20.setTransform(-38.2,103.4,0.95,0.95,-43.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF9900").s().p("Am4AaIgGmCIDiGUIARj5IDLElIgDj3IDVFwIAKkrIDjHCg");
	this.shape_21.setTransform(-135.7,-103.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC6600").s().p("AElAkImUCFICmi1IlbBSICZjEIm1CYIEHkfIAAgDIN3FOIACAGInbC9g");
	this.shape_22.setTransform(-148.3,-74.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_23.setTransform(-14.3,-36.5,0.95,0.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 3
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("EgpDApEQxBxAAA4EQAA4CRBxBQRBxBYCAAQYEAARARBQRBRBAAYCQAAYExBRAQxARB4EAAQ4CAAxBxBg");
	this.shape_24.setTransform(36.2,20);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF0000").s().p("EgqgAqhQxnxoAA45QAA44RnxnQRoxoY4ABQY6gBRnRoQRmRnAAY4QAAY5xmRoQxnRm46ABQ44gBxoxmgEgpDgpDQxBRBAAYCQAAYERBRAQRBRBYCAAQYEAARAxBQRBxAAA4EQAA4CxBxBQxAxB4EAAQ4CAAxBRBg");
	this.shape_25.setTransform(36.2,20);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

	// Layer 4
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,153,0,0.02)").s().p("EgNOA8wQxVjktVtUQyKyLAA5tQAA5sSKyLQFMlMF0jtQOipTSWABQSYgBOiJTQFyDtFMFMQGvGvEPHxQHNNNAAQKQAAQMnNNMQkPHxmvGvQtUNUxVDkQmXBTm4AAQm4AAmWhTg");
	this.shape_26.setTransform(36.2,20);

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy5, new cjs.Rectangle(-360.9,-377.2,794.3,794.3), null);


(lib.setscopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#793F06").s().p("AjrhWILABvIupA+g");
	this.shape.setTransform(304.7,102.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("AkGAXIAAgBIgmkCIJZHZg");
	this.shape_1.setTransform(321.5,81.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FBAA30").s().p("AE5FiIpZnaIgjjsIKHLIIgCABg");
	this.shape_2.setTransform(320.2,69.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9E5104").s().p("AlkgCICNhqIIzDVIAJADIgJABg");
	this.shape_3.setTransform(316.7,94.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AuSkTIAAiJIclLCIgsB3g");
	this.shape_4.setTransform(239.9,59.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF9900").s().p("Am4AaIgGmCIDiGUIARj5IDLElIgDj3IDVFwIAKkrIDjHCg");
	this.shape_5.setTransform(-135.7,-103.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC6600").s().p("AElAkImVCFICni1IlbBSICZjEIm0CYIEGkfIAAgDIN2FOIAEAGInbC9g");
	this.shape_6.setTransform(-148.4,-74.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_7.setTransform(-14.4,-36.5,0.95,0.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhtCIQgugxAAhUQAAhYAugxQAvgyBKAAQBBAAApAnQAZAXAMArIhHARQgHgcgUgQQgUgQgdAAQgnAAgZAcQgZAdAABAQABBCAYAdQAYAdAmABQAdgBAVgSQAVgSAJgoIBGAXQgQA6glAdQgmAcg6AAQhGAAgugxg");
	this.shape_8.setTransform(-54.2,-295.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AiVCzIAAllICPAAQAqAAAUAEQAWADAQAMQAQAKAMATQAKATABAXQAAAZgOAWQgOAUgXALQAhAJASAXQARAWAAAfQAAAYgMAYQgLAXgUANQgTAOgdADQgSAChEABgAhNB3IBEAAQAmAAAKgDQAQgCAKgLQAKgMAAgTQAAgQgHgMQgIgLgPgFQgPgFgwAAIg7AAgAhNgkIAwAAQApAAAKgBQATgCALgKQAKgLAAgRQAAgRgJgKQgJgLgSgCQgLgBgygBIgqAAg");
	this.shape_9.setTransform(-224.5,-111.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("ABlCzIgfhSIiOAAIgeBSIhMAAICLllIBMAAICPFlgAgyAmIBhAAIgxiEg");
	this.shape_10.setTransform(-237.4,179.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8}]}).wait(1));

	// Layer 2
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#793F06").s().p("AjrhWILABvIupA+g");
	this.shape_11.setTransform(304.7,102.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF9900").s().p("AkGAXIAAgBIgmkCIJZHZg");
	this.shape_12.setTransform(321.5,81.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FBAA30").s().p("AE5FiIpZnaIgjjsIKHLIIgCABg");
	this.shape_13.setTransform(320.2,69.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#9E5104").s().p("AlkgCICNhqIIzDVIAJADIgJABg");
	this.shape_14.setTransform(316.7,94.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#330000").s().p("AuSkTIAAiJIclLCIgsB3g");
	this.shape_15.setTransform(239.9,59.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#A17446").s().p("Ag6IPQg0gGgDgMQgFgMAFgLQADgMgBAAIAAgjQABhKAHgSQAGgQAMgbIATgpQA/ibAJhSQAIhPgehmQgIgjg1iNQgYg/AIgXQAEgQAKgKQAQgPAdgKQBLgtAWgIQAdgMAKAMQALAIAAAbQAAAWACAVQAMGrhEEkQgKApgMAoQgVBGgOAhQgPAgAEATQACARgiAAIgRgBg");
	this.shape_16.setTransform(102,-99.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_17.setTransform(172,-137.5,4.09,4.09,0,-48.2,131.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_18.setTransform(169.6,-142.9,4.09,4.09,0,-48.2,131.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#A8D64A").s().p("ApOIvQkxiNismBQhDiSgxjGIAAgHQAdiXA/hwQAQgbASgZQAHBnAbBmQA8DvCYCmQDRDiFfAzQFAAvFMhuIBhglQDahcB9iSQCdi2AGkKQACg2gEg1QCTDEAjEhQkAEgk/DSQg3Alg4AjQjtCNjvA3Qh9Ach1AAQjHAAirhRg");
	this.shape_19.setTransform(93.3,4.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#D1EC93").s().p("AgGD6QhcgQhEhMQg9hMgfgkQA+gZAvg4QAUgXAhgzQAggzAVgWQAggpArgVQAxgUAnAOQA7AVAnBYQAsBygDBJQgLBqhNA/Qg9Aqg/AAQgbAAgagHg");
	this.shape_20.setTransform(133,-26.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#94CD05").s().p("AC0PeQgtgFhagQQhZgQgvgEQgxgEhoAGQhhAGgxgGQjEgNiuh6Qihh1hkizQiikhgTnRQgKjhAliwIAAAGQAxDHBDCTQCsF/ExCNQEQCBFVhMQDvg3DtiNQA3gjA4glQE+jREAkhIAAAFQAxFoiNFWQiNFakeDfQhUA9hSArQimBSiaAAIgHAAgAhhsMQhmgZh1heQA7AOBEACQCAALDrgfQBNgKArgPQBAgUAjgpQhGB/iLA7QhXAmhWAAQg2AAg2gPg");
	this.shape_21.setTransform(92.4,35.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#C6E57E").s().p("AjhJPQlfgzjRjjQiXimg9juQgbhmgGhnQBWh2CBhEQBhg1BhgIQBIgIBFAQQA5AOBOAlQAtATBUArQBMAYBHgCQArgSAugZQD4iDBbgSQBSgSBUAIQBOAGBNAcQCcA8B2B6QAnAnAjAtQAEA1gCA2QgGEJidC2Qh9CTjZBcIhiAlQjoBNjjAAQhhAAhggOgAHIjUQgsAVggApQgVAWggAzQghAzgUAXQgvA4g+AZQAfAkA9BMQBEBMBcAQQBcAZBWg8QBNg/ALhqQADhKgshxQgnhYg7gVQgQgGgSAAQgZAAgdAMgAhPh+QCMAnCNg9QCLg8BGh/QgjAphAAVQgrAOhNAKQjrAfiAgKQhEgCg7gOQB1BeBmAYg");
	this.shape_22.setTransform(90.6,-29.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF9900").s().p("AkLkvIEUkNIiLG6IDAifIhJFcICwiqIh6GXIDhjGIiuHag");
	this.shape_23.setTransform(-14.5,-226.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CC6600").s().p("AhXDgIEpgRIl0jNID0gCIkpjEID4gXImZjVIGEgGIADgCIFqNrIgCAGg");
	this.shape_24.setTransform(-42.8,-213.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_25.setTransform(25.1,-92.7,0.95,0.95,46.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF9900").s().p("Ao8gIIG6CLIifjAIFcBJIiqiwIGXB6IjGjhIHaCuItrFpg");
	this.shape_26.setTransform(-172.5,142.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#CC6600").s().p("Am2gLIgCgDINrlqIAGACIjZHOIgRkpIjNF0IgCj0IjEEpIgXj4IjVGZg");
	this.shape_27.setTransform(-158.8,171.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_28.setTransform(-38.2,103.4,0.95,0.95,-43.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF9900").s().p("Am4AaIgGmCIDiGUIARj5IDLElIgDj3IDVFwIAKkrIDjHCg");
	this.shape_29.setTransform(-135.7,-103.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#CC6600").s().p("AElAkImVCFICni1IlbBSICZjEIm0CYIEGkfIAAgDIN2FOIAEAGInbC9g");
	this.shape_30.setTransform(-148.4,-74.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_31.setTransform(-14.4,-36.5,0.95,0.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	// Layer 3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("EgpEApEQxAxAAA4EQAA4CRAxBQRCxBYCAAQYDAARBRBQRBRBAAYCQAAYExBRAQxBRB4DAAQ4CAAxCxBg");
	this.shape_32.setTransform(36.2,20);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF0000").s().p("EgqfAqhQxoxoAB45QgB44RoxnQRnxoY4ABQY6gBRmRoQRoRnAAY4QAAY5xoRoQxmRm46ABQ44gBxnxmgEgpEgpDQxARBAAYCQAAYERARAQRCRBYCAAQYDAARBxBQRBxAAA4EQAA4CxBxBQxBxB4DAAQ4CAAxCRBg");
	this.shape_33.setTransform(36.2,20);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32}]}).wait(1));

	// Layer 4
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("rgba(255,153,0,0.02)").s().p("EgNOA8wQxVjktVtUQyKyLAA5tQAA5sSKyLQFMlMF0jtQOipTSWABQSYgBOhJTQFzDtFMFMQGvGvEPHxQHNNNAAQKQAAQMnNNMQkPHxmvGvQtUNUxVDkQmXBTm4AAQm4AAmWhTg");
	this.shape_34.setTransform(36.1,20);

	this.timeline.addTween(cjs.Tween.get(this.shape_34).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy4, new cjs.Rectangle(-361,-377.2,794.3,794.3), null);


(lib.setscopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhtCIQgugxAAhUQAAhYAvgxQAugyBJAAQBDAAAoAnQAZAXAMArIhIARQgGgcgUgQQgUgQgdAAQgnAAgYAcQgZAdAABAQgBBCAZAdQAZAdAmABQAcgBAVgSQAVgSAJgoIBGAXQgQA6gmAdQglAcg5AAQhHAAgugxg");
	this.shape.setTransform(-54.1,-295.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AiVCzIAAllICPAAQApAAAWAEQAUADARAMQAQAKALATQALATAAAXQAAAZgNAWQgOAUgXALQAhAJARAXQASAWAAAfQAAAYgMAYQgLAXgTANQgUAOgdADQgSAChEABgAhNB3IBDAAQAnAAAKgDQAQgCAKgLQAKgMAAgTQAAgQgIgMQgHgLgPgFQgPgFgxAAIg6AAgAhNgkIAwAAQApAAAKgBQATgCAKgKQALgLAAgRQAAgRgJgKQgJgLgTgCQgKgBgygBIgqAAg");
	this.shape_1.setTransform(-224.4,-111.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABlCzIgfhSIiPAAIgdBSIhNAAICMllIBLAAICPFlgAgyAmIBgAAIgwiEg");
	this.shape_2.setTransform(-237.3,179.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#793F06").s().p("AjrhWILABvIupA+g");
	this.shape_3.setTransform(304.7,102.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF9900").s().p("AkHAXIACgBIgnkCIJZHZg");
	this.shape_4.setTransform(321.5,81.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FBAA30").s().p("AE5FiIpZnaIgjjsIKHLIIgCABg");
	this.shape_5.setTransform(320.3,69.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9E5104").s().p("AlkgCICNhqIIzDVIAJADIgJABg");
	this.shape_6.setTransform(316.8,94.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AuSkTIAAiJIclLCIgsB3g");
	this.shape_7.setTransform(239.9,59.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#A17446").s().p("Ag6IPQg0gGgDgMQgFgMAFgLQADgMgBAAIAAgjQABhKAHgSQAGgQAMgbIATgpQA/ibAJhSQAIhPgehmQgIgjg1iNQgYg/AIgXQAEgQAKgKQAQgPAdgKQBLgtAWgIQAdgMAKAMQALAIAAAbQAAAWACAVQAMGrhEEkQgKApgMAoQgVBGgOAhQgPAgAEATQACARgiAAIgRgBg");
	this.shape_8.setTransform(102.1,-99.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_9.setTransform(172,-137.5,4.09,4.09,0,-48.2,131.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_10.setTransform(169.7,-142.9,4.09,4.09,0,-48.2,131.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#A8D64A").s().p("ApPIvQkwiNismBQhDiSgxjGIAAgHQAdiXA/hwQAQgbATgZQAGBnAaBmQA+DvCXCmQDRDiFfAzQFAAvFMhuIBiglQDZhcB9iSQCdi2AGkKQACg2gEg1QCTDEAjEhQkAEgk+DSQg4Alg3AjQjuCNjuA3Qh+Ach1AAQjHAAishRg");
	this.shape_11.setTransform(93.3,4.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#D1EC93").s().p("AgGD6QhcgQhEhMQg9hMgfgkQA+gZAvg4QAUgXAhgzQAggzAVgWQAggpArgVQAxgUAnAOQA7AVAnBYQAsBygDBJQgLBqhNA/Qg9Aqg/AAQgbAAgagHg");
	this.shape_12.setTransform(133.1,-26.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#94CD05").s().p("AC0PeQgtgFhagQQhZgQgvgEQgxgEhoAGQhhAGgxgGQjEgNiuh6Qihh1hkizQiikhgTnRQgKjhAliwIAAAGQAxDHBDCTQCsF/ExCNQEQCBFVhMQDvg3DtiNQA3gjA4glQE+jREAkhIAAAFQAxFoiNFWQiNFakeDfQhUA9hSArQimBSiaAAIgHAAgAhhsMQhmgZh1heQA7AOBEACQCAALDrgfQBNgKArgPQBAgUAjgpQhGB/iLA7QhXAmhWAAQg2AAg2gPg");
	this.shape_13.setTransform(92.5,35.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C6E57E").s().p("AjhJPQlfgzjRjjQiXimg9juQgbhmgGhnQBWh2CBhEQBhg1BhgIQBIgIBFAQQA5AOBOAlQAtATBUArQBMAYBHgCQArgSAugZQD4iDBbgSQBSgSBUAIQBOAGBNAcQCcA8B2B6QAnAnAjAtQAEA1gCA2QgGEJidC2Qh9CTjZBcIhiAlQjoBNjjAAQhhAAhggOgAHIjUQgsAVggApQgVAWggAzQghAzgUAXQgvA4g+AZQAfAkA9BMQBEBMBcAQQBcAZBWg8QBNg/ALhqQADhKgshxQgnhYg7gVQgQgGgSAAQgZAAgdAMgAhPh+QCMAnCNg9QCLg8BGh/QgjAphAAVQgrAOhNAKQjrAfiAgKQhEgCg7gOQB1BeBmAYg");
	this.shape_14.setTransform(90.7,-29.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF9900").s().p("AkLkvIEUkNIiLG6IDAifIhJFcICwiqIh6GXIDhjGIiuHag");
	this.shape_15.setTransform(-14.5,-226.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC6600").s().p("AhXDgIEpgRIl0jNID0gCIkpjEID4gXImZjVIGEgGIADgCIFqNrIgCAGg");
	this.shape_16.setTransform(-42.8,-213.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_17.setTransform(25.1,-92.7,0.95,0.95,46.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF9900").s().p("Ao7gIIG6CLIigjAIFcBJIiqiwIGWB6IjFjhIHaCuItrFpg");
	this.shape_18.setTransform(-172.4,142.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC6600").s().p("Am2gLIgCgDINrlqIAGACIjYHOIgRkpIjNF0IgDj0IjDEpIgYj4IjWGZg");
	this.shape_19.setTransform(-158.7,171.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_20.setTransform(-38.2,103.4,0.95,0.95,-43.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF9900").s().p("Am4AaIgGmCIDiGUIARj5IDLElIgDj3IDVFwIAKkrIDjHCg");
	this.shape_21.setTransform(-135.7,-103.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC6600").s().p("AElAkImUCFICmi1IlbBSICZjEIm1CYIEHkfIAAgDIN3FOIACAGInbC9g");
	this.shape_22.setTransform(-148.3,-74.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_23.setTransform(-14.3,-36.5,0.95,0.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 3
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("EgpDApEQxBxAAA4EQAA4CRBxBQRBxBYCAAQYEAARARBQRBRBAAYCQAAYExBRAQxARB4EAAQ4CAAxBxBg");
	this.shape_24.setTransform(36.2,20);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF0000").s().p("EgqgAqhQxnxoAA45QAA44RnxnQRoxoY4ABQY6gBRnRoQRmRnAAY4QAAY5xmRoQxnRm46ABQ44gBxoxmgEgpDgpDQxBRBAAYCQAAYERBRAQRBRBYCAAQYEAARAxBQRBxAAA4EQAA4CxBxBQxAxB4EAAQ4CAAxBRBg");
	this.shape_25.setTransform(36.2,20);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

	// Layer 4
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,153,0,0.02)").s().p("EgNOA8wQxVjktVtUQyKyLAA5tQAA5sSKyLQFMlMF0jtQOipTSWABQSYgBOiJTQFyDtFMFMQGvGvEPHxQHNNNAAQKQAAQMnNNMQkPHxmvGvQtUNUxVDkQmXBTm4AAQm4AAmWhTg");
	this.shape_26.setTransform(36.2,20);

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy3, new cjs.Rectangle(-360.9,-377.2,794.3,794.3), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMB4IgFgDQgDgCgBgEQgCgDAAgDQAAgDACgDQABgEADgCQABgCAEgBQADgCADABIAGABQACABADACIADAGIABAGIgBAGIgDAGIgFADIgGABIgGgBgAgWBFIgCgMQAAgJAEgHQADgHAEgGIALgLIALgKIANgJQAGgGAFgGQAEgGAEgHQADgIAAgJQgBgKgDgJQgDgJgGgIQgGgHgIgDQgJgFgJAAQgKAAgIAEQgIADgFAIQgGAGgDAJQgCAIAAAKIAAAGIABAIIgaAFIgCgKIgBgKQAAgPAGgNQAFgNAJgJQAJgLAOgFQANgGAPAAQAPAAANAFQAOAGAJAKQAJAKAGANQAFANABARIgBAOIgFAPQgDAIgFAHQgFAGgJAHIgOALQgIAFgGAHQgHAGgEAIQgDAJABAMg");
	this.shape.setTransform(237.7,-16.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_1.setTransform(219.7,-15.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgfB6IgOgFIgNgIQgHgFgGgGQgFgHgDgJIAcgMQADAGAEAFQADAEAFADIAJAFIAKADQALACALgBQAKgCAIgEQAHgFAFgFQAFgFADgGIAGgLIACgKIABgGIAAgTQgJAIgJAEQgJAFgJACQgJACgIABQgRAAgPgGQgOgGgLgLQgLgLgGgPQgHgQAAgUQAAgUAHgPQAHgQALgLQAMgKAOgGQAOgGAOAAIATAFIATAIQAKAFAIAJIAAgdIAeAAIgCCoQAAAJgCAKQgDAJgEAJQgEAJgIAIQgGAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgOgEgAgVhZQgJAFgHAHQgGAIgEAKQgDALAAAMQAAAMADAKQAEAKAGAGQAHAHAJAEQAJAFALAAQAJAAAIgEQAKgDAHgGQAHgFAFgJQAGgIABgKIAAgTQgBgLgGgIQgFgJgHgGQgIgGgJgEQgKgDgIAAQgKAAgJAEg");
	this.shape_2.setTransform(199.3,-9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPBRQgNAAgKgFQgKgEgGgIQgGgHgEgKQgEgKgCgLQgCgLAAgLIgBgWQAAgPABgQIADggIAfABIgEAhIAAAZIAAANIABAPIADARQACAIAEAHQAEAHAFADQAGAEAJgCQAKgBAMgOQAMgOANgcIAAglIgBgVIAAgMIAggEIABAuIABAjIABAdIAAAVIABAfIghAAIgBgqIgLARQgFAHgIAGQgHAGgIADQgGAEgIAAIgCgBg");
	this.shape_3.setTransform(180.4,-12.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgeBQQgOgHgLgKQgKgMgHgQQgFgPgBgUQABgSAFgQQAHgQAKgLQALgMAOgGQAOgGAQAAQAQAAAPAHQAOAGAKAMQALALAGAQQAGAQABARQgBATgGAPQgGAQgLAMQgKALgOAHQgPAGgQAAQgQAAgOgGgAgUg2QgJAGgHAIQgFAJgDAKQgCALAAAKQAAAKACALQADAKAGAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJACgKQADgLAAgKQAAgKgCgLQgDgKgGgJQgGgIgIgGQgKgFgMgBQgMABgIAFg");
	this.shape_4.setTransform(162.2,-12.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_5.setTransform(145.8,-12.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_6.setTransform(127.2,-15.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygCIACg/IAcgCIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_7.setTransform(110.1,-15.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgNgHQgHgDgHgFIAPgXIAQAJIARAHQAJADAJABQAJACAKAAQAHAAAGgBQAFgCAEgCQAEgCACgDIADgGIAAgFIgBgGQgBgDgEgDIgHgFIgMgDIgPgCQgMgBgNgCQgMgCgKgFQgJgEgGgHQgHgHgBgLQgBgLADgIQABgJAGgHQAEgHAIgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIARAFQAJAEAIAFIgKAbQgKgFgJgDIgQgFIgPgCQgVgBgNAGQgNAGAAANQAAAIAFAEQAEAEAJACIATADIAXACQAPACAJAFQAKADAGAGQAGAGADAHQACAHAAAIQABAOgGAJQgGAJgJAGQgKAGgMADQgMADgNABQgNAAgNgDg");
	this.shape_8.setTransform(86.7,-12);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AggBPQgPgHgLgKQgJgLgGgPQgFgPgBgTQABgRAGgQQAGgQALgMQALgLAPgHQAOgHAQAAQAQABANAFQANAHALAKQAKALAHAOQAHANACARIh+ASQABAKADAJQAFAIAGAGQAGAGAJADQAIACAJAAQAIABAIgDQAHgDAGgEQAHgFAFgGQAEgIACgJIAdAHQgEANgIAKQgHALgKAIQgKAIgMAEQgMAFgNgBQgTAAgOgFgAgLg4QgHACgIAFQgGAFgGAJQgFAJgCAMIBXgLIgCgCQgFgPgJgIQgKgJgPAAQgFABgHACg");
	this.shape_9.setTransform(69.7,-12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgCgDgDgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgJgEgHgHQgGgHgBgLQgBgLADgIQACgJAEgHQAGgHAHgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgWgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQADAHgBAIQABAOgGAJQgGAJgJAGQgJAGgNADQgMADgNABQgNAAgNgDg");
	this.shape_10.setTransform(52.3,-12);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgCgDgDgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgKgEgGgHQgGgHgBgLQgBgLADgIQACgJAEgHQAGgHAHgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgWgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQADAHgBAIQABAOgGAJQgGAJgJAGQgJAGgNADQgMADgNABQgNAAgNgDg");
	this.shape_11.setTransform(35.6,-12);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_12.setTransform(17.6,-12.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhQh2IAhgBIgBAVQAJgIAJgFIAQgHQAJgCAIgBQAKAAAKADQAJADAJAFQAJAFAHAIQAIAHAFAJQAFAJADAKQADALAAALQgBANgDALQgDALgGAIQgFAKgIAHQgHAIgIAFQgJAFgIADQgJACgJAAQgIAAgJgCQgIgCgKgFQgKgEgJgIIgBBjIgbABgAgQhYQgIADgHAGQgGAFgEAHQgFAGgCAIIgBAcQADAJAEAIQAFAHAHAEQAGAFAHADQAIACAHAAQAKAAAJgDQAJgEAGgGQAHgHAEgKQAEgJABgLQAAgLgDgKQgDgKgHgHQgGgHgJgFQgJgEgLAAQgIAAgIADg");
	this.shape_13.setTransform(-0.5,-8.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAEgdIgiBlIgrAEIgdiXIAegCIAWB6IAsh6IAbACIAeB7IAUh8IAgABIgdCYIgsABg");
	this.shape_14.setTransform(-29.8,-12.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgeBQQgOgHgKgKQgMgMgFgQQgHgPAAgUQAAgSAHgQQAFgQAMgLQAKgMAOgGQAPgGAPAAQAQAAAOAHQAOAGALAMQALALAGAQQAHAQAAARQAAATgHAPQgGAQgLAMQgLALgOAHQgOAGgQAAQgPAAgPgGgAgVg2QgIAGgGAIQgGAJgCAKQgDALAAAKQAAAKADALQADAKAFAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJADgKQACgLAAgKQABgKgDgLQgDgKgFgJQgHgIgJgGQgIgFgNgBQgMABgJAFg");
	this.shape_15.setTransform(-50.1,-12.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_16.setTransform(-66.4,-12.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_17.setTransform(-82.8,-12.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_18.setTransform(-101.5,-12.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_19.setTransform(-128,-15.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_20.setTransform(-147,-12.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgSBuIABglIABguIABg+IAdgBIgBAlIAAAgIgBAaIAAATIAAAggAgHhFIgGgEQgDgDgCgEQgCgEAAgEQAAgEACgEQACgEADgDQACgDAEgBQADgCAEAAQAEAAAEACQAEABADADIAFAHIABAIQAAAEgBAEIgFAHIgHAEIgIACQgEAAgDgCg");
	this.shape_21.setTransform(-159.7,-15.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_22.setTransform(-173.4,-15.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AhXBwIgojUIAegEIAbCnIA3iaIAjAEIAwCfIAei3IAeAGIgnDYIgjAAIg1inIg6Cog");
	this.shape_23.setTransform(-197.2,-16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(6,1,1).p("EglAgEnMBKBAAAQCcAAAACcIAAEXQAACcicAAMhKBAAAQicAAAAicIAAkXQAAicCcAAg");
	this.shape_24.setTransform(16.3,-15.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF3333").s().p("EglAAEoQicAAAAicIAAkXQAAicCcAAMBKBAAAQCcAAAACcIAAEXQAACcicAAg");
	this.shape_25.setTransform(16.3,-15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-239.2,-47.8,511.1,65.3), null);


(lib.C = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgvClQgpgSgdgkQgTgZgKgdQgMgdAAgeQAAgiAOgfQANgeAXgYQAWgYAegQQAegPAggFIAFgBIAGAAQANAAASADIAkAHIAkAKIAgAKIggBZQgPgHgVgHQgVgGgRgBIgNACQgQADgPAGQgNAGgKAKQgLAKgGANQgHANAAATQABAQAEAPQAFAOAJALQAKAKANAHQAMAGASAAQALAAALgCIAUgGIAUgEQAJgDAKAAQAKAAAJADQAJADAHAHQAHAGAEAIQAFAJAAAKQAAAVgOAOQgMAOgSAKQgTAIgVAEQgUAEgRAAQgsAAgpgSg");
	this.shape.setTransform(-2.6,-6.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgvClQgpgSgdglQgSgYgMgdQgLgeABgdQgBgjANgeQAOgfAWgYQAXgXAegQQAegQAggEIAGgBIAFAAQAOABASADIAkAHIAkAJIAeAKIgeBaQgQgJgVgGQgVgHgRAAIgNACQgRADgOAGQgNAGgLAJQgKAKgHAOQgFAOgBASQAAAQAFAPQAFAOAKALQAIALAOAGQANAGARAAQAMAAAKgCIAUgGIATgEQAKgDAKAAQAKAAAJADQAJAEAHAGQAHAGAFAJQADAIAAALQABAUgNAPQgNAOgTAIQgSAJgVAEQgUADgQAAQguAAgogRg");
	this.shape_1.setTransform(1.4,-3.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AiIBZIgCgOQAAgvBJgjQAegOAUACQAWACAAAVQAAgFg7AyIg/A0QgRAAgEgMgABNgsIgKgTQAAgKALgLQALgLAOgEQAkgKAAAvQAAASgEAIQgGAOgUAAQgRAAgPgWg");
	this.shape_2.setTransform(-23.3,-24.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#BF0F0F").s().p("AgpEIQhXgChMgbQh5gshHhhQBJBNB/ASQCqAZCihfQClheA+ifQAbhDADg+QATCfhHCMQhUCpivAvQgwAMg6AAIgRAAg");
	this.shape_3.setTransform(3.8,18.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3333").s().p("AiXFwQh/gShJhNQgdgngVgyQgLgXgHgaIgHgXQgGgagEgaQgBhAA5hWQA6hVCkhgQCihgCVgFQCUgGBNBpQAnBEAPBYIAFAhQgCA+gbBDQg+CfilBfQiBBLiEAAQgkAAgjgGg");
	this.shape_4.setTransform(-0.7,0.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FC6868").s().p("AkehwQA5g3BNgoQAvgXAggJQDfg8CWBqQBFAwArBJQhNhpiUAGQiVAFijBgQikBfg5BVQg5BWAABBQgojfCdiWg");
	this.shape_5.setTransform(-4.1,-19.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BFBFBF").s().p("AAFHXQhZgChPgdQh+gthKhlQgfgqgVgzQgKgYgJgbIgHgYQgHgbgEgaQgqjoCkieQA7g5BQgpQAxgYAhgIQDohACdBvQBHAyAsBMQAoBGARBcIAFAjQAUCkhJCTQhYCvi2AxQgyANg+AAIgRAAg");
	this.shape_6.setTransform(-0.7,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AmJFXQg6hJglheQgYg6gHglQgvkMCLilQB7iSDagTQDTgTCvBrQC8B1AlDSQAMBAgKBRQgLBlgoBVQhsDpkRAxQg9AKg3AAQjjAAiRiyg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.C, new cjs.Rectangle(-53.1,-52.1,106.3,104.3), null);


(lib.Bcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiWh4QAAgMAFgLQAEgLAIgIQAIgIALgEQALgFAMAAIBTAAQAXAAAXAHQAXAHASANQASAOALAUQAMAUAAAaQAAAQgFAQQgEAQgKANIADADIAFADIAGAEIAEACQAQANAIATQAIASAAAVQAAAZgKAUQgKAUgQAOQgRAOgVAHQgWAIgXAAIhbAAIhbAAgAgiBQIA1AAQAHAAAFgEQAEgFAAgHQAAgHgEgFQgFgFgHAAIg1AAgAgigwIAbAAQAHAAAEgFQAGgEAAgHQAAgHgGgFQgFgEgHAAIgaAAg");
	this.shape.setTransform(-2.2,-6.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AiWh4QAAgMAFgLQAEgLAIgIQAIgIALgEQALgFAMAAIBTAAQAXAAAXAHQAXAHASANQASAOALAUQAMAUAAAaQAAAQgFAQQgEAQgKANIADADIAFADIAGAEIAEACQAQANAIATQAIASAAAVQAAAZgKAUQgKAUgQAOQgRAOgVAHQgWAIgXAAIhbAAIhbAAgAgiBQIA1AAQAHAAAFgEQAEgFAAgHQAAgHgEgFQgFgFgHAAIg1AAgAgigwIAbAAQAHAAAEgFQAGgEAAgHQAAgHgGgFQgFgEgHAAIgaAAg");
	this.shape_1.setTransform(1.8,-3.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AiIBZIgCgOQAAgvBJgjQAegOAUACQAWACAAAVQAAgFg7AyIg/A0QgRAAgEgMgABNgsIgKgTQAAgKALgLQALgLAOgEQAkgKAAAvQAAASgEAIQgGAOgUAAQgRAAgPgWg");
	this.shape_2.setTransform(-23.3,-24.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#BF0F0F").s().p("AgpEIQhXgChMgbQh5gshHhhQBJBNB/ASQCqAZCihfQClheA+ifQAbhDADg+QATCfhHCMQhUCpivAvQgwAMg6AAIgRAAg");
	this.shape_3.setTransform(3.8,18.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3333").s().p("AiXFwQh/gShJhNQgdgngVgyQgLgXgHgaIgHgXQgGgagEgaQgBhAA5hWQA6hVCkhgQCihgCVgFQCUgGBNBpQAnBEAPBYIAFAhQgCA+gbBDQg+CfilBfQiBBLiEAAQgkAAgjgGg");
	this.shape_4.setTransform(-0.7,0.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FC6868").s().p("AkehwQA5g3BNgoQAvgXAggJQDfg8CWBqQBFAwArBJQhNhpiUAGQiVAFijBgQikBfg5BVQg5BWAABBQgojfCdiWg");
	this.shape_5.setTransform(-4.1,-19.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BFBFBF").s().p("AAFHXQhZgChPgdQh+gthKhlQgfgqgVgzQgKgYgJgbIgHgYQgHgbgEgaQgqjoCkieQA7g5BQgpQAxgYAhgIQDohACdBvQBHAyAsBMQAoBGARBcIAFAjQAUCkhJCTQhYCvi2AxQgyANg+AAIgRAAg");
	this.shape_6.setTransform(-0.7,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AmJFXQg6hJglheQgYg6gHglQgvkMCLilQB7iSDagTQDTgTCvBrQC8B1AlDSQAMBAgKBRQgLBlgoBVQhsDpkRAxQg9AKg3AAQjjAAiRiyg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Bcopy, new cjs.Rectangle(-53.1,-52.1,106.3,104.3), null);


(lib.A = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AA3B8Ih4AAIgJAVQgFALgGAIQgHAIgKAEQgKAFgNAAQgMAAgLgEQgKgEgIgHQgJgHgEgLQgFgKAAgMQAAgPAGgPIBWjbQAMgdAWgRQAWgRAfAAQAfAAAWARQAXARAKAdIBoEaIhxAcgAAVAYIgahfIgbBfIA1AAg");
	this.shape.setTransform(-2.3,-6.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AA3B8Ih4AAIgJAVQgFALgGAIQgHAIgKAEQgKAFgNAAQgMAAgLgEQgKgEgIgHQgJgHgEgLQgFgKAAgMQAAgPAGgPIBWjbQAMgdAWgRQAWgRAfAAQAfAAAWARQAXARAKAdIBoEaIhxAcgAAVAYIgahfIgbBfIA1AAg");
	this.shape_1.setTransform(1.7,-3.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AiIBZIgCgOQAAgvBJgjQAegOAUACQAWACAAAVQAAgFg7AyIg/A0QgRAAgEgMgABNgsIgKgTQAAgKALgLQALgLAOgEQAkgKAAAvQAAASgEAIQgGAOgUAAQgRAAgPgWg");
	this.shape_2.setTransform(-23.3,-24.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#BF0F0F").s().p("AgpEIQhXgChMgbQh5gshHhhQBJBNB/ASQCqAZCihfQClheA+ifQAbhDADg+QATCfhHCMQhUCpivAvQgwAMg6AAIgRAAg");
	this.shape_3.setTransform(3.8,18.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3333").s().p("AiXFwQh/gShJhNQgdgngVgyQgLgXgHgaIgHgXQgGgagEgaQgBhAA5hWQA6hVCkhgQCihgCVgFQCUgGBNBpQAnBEAPBYIAFAhQgCA+gbBDQg+CfilBfQiBBLiEAAQgkAAgjgGg");
	this.shape_4.setTransform(-0.7,0.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FC6868").s().p("AkehwQA5g3BNgoQAvgXAggJQDfg8CWBqQBFAwArBJQhNhpiUAGQiVAFijBgQikBfg5BVQg5BWAABBQgojfCdiWg");
	this.shape_5.setTransform(-4.1,-19.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BFBFBF").s().p("AAFHXQhZgChPgdQh+gthKhlQgfgqgVgzQgKgYgJgbIgHgYQgHgbgEgaQgqjoCkieQA7g5BQgpQAxgYAhgIQDohACdBvQBHAyAsBMQAoBGARBcIAFAjQAUCkhJCTQhYCvi2AxQgyANg+AAIgRAAg");
	this.shape_6.setTransform(-0.7,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AmJFXQg6hJglheQgYg6gHglQgvkMCLilQB7iSDagTQDTgTCvBrQC8B1AlDSQAMBAgKBRQgLBlgoBVQhsDpkRAxQg9AKg3AAQjjAAiRiyg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.A, new cjs.Rectangle(-53.1,-52.1,106.3,104.3), null);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.C();
	this.instance.parent = this;
	this.instance.setTransform(-41.8,174.1,1.555,1.555,0,0,0,0.1,0.1);
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, 149))];
	this.instance.cache(-55,-54,110,108);

	this.instance_1 = new lib.Bcopy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.8,0.1,1.555,1.555,0,0,0,0.1,0.1);
	this.instance_1.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, -108))];
	this.instance_1.cache(-55,-54,110,108);

	this.instance_2 = new lib.A();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-41.8,-173.8,1.555,1.555,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-124.6,-254.9,168,514.8);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween12("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(251.3,31.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02,y:31.7},9).to({scaleX:1,scaleY:1,y:31.8},10).to({scaleX:1.02,scaleY:1.02,y:31.7},10).to({scaleX:1,scaleY:1,y:31.8},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,505.5,63.6);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(948,425.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(243).to({_off:false},0).wait(58).to({startPosition:12},0).to({alpha:0,startPosition:19},7).wait(1));

	// Layer_4
	this.question = new lib.setscopy4();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(494.7,420,0.669,0.669,0,0,0,2.8,0.1);
	this.question._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question).wait(230).to({_off:false},0).wait(71).to({_off:true},1).wait(7));

	// Layer_8
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(892.3,520.9,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(202).to({_off:false},0).to({_off:true},28).wait(79));

	// Layer_7
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(1095.3,424.6,1,1,89,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(172).to({_off:false},0).to({_off:true},30).wait(107));

	// Layer_11
	this.question_1 = new lib.setscopy5();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(494.7,420,0.669,0.669,0,0,0,2.8,0.1);
	this.question_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question_1).wait(92).to({_off:false},0).to({regX:2.9,regY:0.2,scaleX:0.7,scaleY:0.7,x:494.8,y:420.1},10).to({regX:2.8,regY:0.1,scaleX:0.67,scaleY:0.67,x:494.7,y:420},10).to({regX:2.9,regY:0.2,scaleX:0.7,scaleY:0.7,x:494.8,y:420.1},10).to({regX:2.8,regY:0.1,scaleX:0.67,scaleY:0.67,x:494.7,y:420},10).to({regX:2.9,regY:0.2,scaleX:0.7,scaleY:0.7,x:494.8,y:420.1},10).to({regX:2.8,regY:0.1,scaleX:0.67,scaleY:0.67,x:494.7,y:420},10).to({regX:2.9,regY:0.2,scaleX:0.7,scaleY:0.7,x:494.8,y:420.1},9).to({regX:2.8,regY:0.1,scaleX:0.67,scaleY:0.67,x:494.7,y:420},7).to({_off:true},1).wait(140));

	// Layer_6
	this.question_2 = new lib.setscopy4();
	this.question_2.name = "question_2";
	this.question_2.parent = this;
	this.question_2.setTransform(494.7,420,0.669,0.669,0,0,0,2.8,0.1);
	this.question_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question_2).wait(126).to({_off:false},0).to({_off:true},43).wait(140));

	// Layer_5
	this.instance_3 = new lib.Symbol6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(713.5,113.9,1,1,0,0,0,251.3,31.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(92).to({_off:false},0).wait(70).to({startPosition:29},0).to({alpha:0,startPosition:35},6).to({_off:true},1).wait(140));

	// Layer_9
	this.instance_4 = new lib.Bcopy();
	this.instance_4.parent = this;
	this.instance_4.setTransform(952.4,434.2,1.555,1.555,0,0,0,0.1,0.1);
	this.instance_4._off = true;
	this.instance_4.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, -108))];
	this.instance_4.cache(-55,-54,110,108);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(230).to({_off:false},0).to({scaleX:1.71,scaleY:1.71},27).wait(44).to({alpha:0},7).wait(1));

	// Layer_3
	this.instance_5 = new lib.Tween6("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(994.9,433.3);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(71).to({_off:false},0).to({alpha:1},7).to({startPosition:0},223).to({alpha:0},7).wait(1));

	// Layer_1
	this.question_3 = new lib.setscopy3();
	this.question_3.name = "question_3";
	this.question_3.parent = this;
	this.question_3.setTransform(494.7,420,0.669,0.669,0,0,0,2.8,0.1);
	this.question_3.alpha = 0;
	this.question_3._off = true;

	this.question_4 = new lib.setscopy5();
	this.question_4.name = "question_4";
	this.question_4.parent = this;
	this.question_4.setTransform(494.7,420,0.669,0.669,0,0,0,2.8,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.question_3}]},42).to({state:[{t:this.question_4}]},8).to({state:[]},180).wait(79));
	this.timeline.addTween(cjs.Tween.get(this.question_3).wait(42).to({_off:false},0).to({_off:true,alpha:1},8).wait(259));

	// Layer_2
	this.questiontxt = new lib.questiontext_mccopy();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(620.9,122.3,1.351,1.351,0,0,0,2.2,-9.3);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(20).to({_off:false},0).to({alpha:1},7).wait(274).to({alpha:0},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(260.9,112.9,2025,1284);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;