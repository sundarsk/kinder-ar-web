(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgIQgJgGgIgGIATgeIAWALIAYAJQALAFANABQALADAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgCgBgFIgBgIQgCgEgEgDQgEgEgHgDQgGgDgJgCQgJgCgNAAQgQgCgRgCQgQgDgNgHQgNgGgJgIQgIgLgCgOQgBgOADgNQADgLAHgJQAHgKAJgHQAKgHALgEQAMgFANgCQAMgCAMAAIATAAIAWADQALADAMAEQALAFALAHIgOAkQgNgHgMgDIgVgHIgUgDQgegCgRAJQgRAIAAAQQAAAMAGAGQAHAFALADIAaADIAfAEQAUADANAFQANAFAIAIQAIAIAEAJQADAKAAALQAAASgHANQgIANgMAIQgNAIgRAEQgQADgSABQgRABgTgEg");
	this.shape.setTransform(124,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgsBqQgUgIgOgOQgOgPgHgUQgHgUAAgZQAAgYAIgWQAJgVAOgQQAPgQAUgJQATgJAWAAQAVAAASAIQASAIAOAOQAOAOAKAUQAJASACAXIiqAYQABAOAGALQAFAMAJAHQAIAIAMAEQALAFAMAAQALgBAKgDQAKgDAJgHQAJgFAGgKQAHgJADgNIAmAIQgGASgKAPQgKAOgNALQgOAKgQAGQgQAGgSAAQgZAAgUgIgAgQhMQgKADgJAHQgJAHgIALQgHALgDASIB2gOIgBgFQgIgTgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_1.setTransform(101.1,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgIQgJgGgIgGIATgeIAWALIAYAJQALAFANABQALADAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgCgBgFIgBgIQgCgEgEgDQgEgEgHgDQgGgDgJgCQgJgCgNAAQgQgCgRgCQgQgDgNgHQgNgGgJgIQgIgLgCgOQgBgOADgNQADgLAHgJQAHgKAJgHQAKgHALgEQAMgFANgCQAMgCAMAAIATAAIAWADQALADAMAEQALAFALAHIgOAkQgNgHgMgDIgVgHIgUgDQgegCgRAJQgRAIAAAQQAAAMAGAGQAHAFALADIAaADIAfAEQAUADANAFQANAFAIAIQAIAIAEAJQADAKAAALQAAASgHANQgIANgMAIQgNAIgRAEQgQADgSABQgRABgTgEg");
	this.shape_2.setTransform(77.5,5.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgIQgJgGgIgGIATgeIAWALIAYAJQALAFANABQALADAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgCgBgFIgBgIQgCgEgEgDQgEgEgHgDQgGgDgJgCQgJgCgNAAQgQgCgRgCQgQgDgNgHQgNgGgJgIQgIgLgCgOQgBgOADgNQADgLAHgJQAHgKAJgHQAKgHALgEQAMgFANgCQAMgCAMAAIATAAIAWADQALADAMAEQALAFALAHIgOAkQgNgHgMgDIgVgHIgUgDQgegCgRAJQgRAIAAAQQAAAMAGAGQAHAFALADIAaADIAfAEQAUADANAFQANAFAIAIQAIAIAEAJQADAKAAALQAAASgHANQgIANgMAIQgNAIgRAEQgQADgSABQgRABgTgEg");
	this.shape_3.setTransform(55.1,5.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA9BxIADgcQgYAPgXAHQgWAHgVAAQgOAAgNgDQgNgEgLgIQgKgHgGgMQgGgMgBgQQABgQAEgNQAFgMAIgKQAJgJALgHQALgHANgEQANgEAOgCQANgCAOAAIAZABIAUACQgDgKgEgJQgDgKgGgHQgGgIgIgFQgJgFgMAAQgIAAgJADQgJACgMAGQgMAGgOAKQgOAKgQARIgYgdQATgSARgLQASgMAPgGQAPgHANgCQAMgCALAAQASAAAOAGQAPAGALALQAKALAIAOQAIAPAEARQAFAQACASQACARAAASQAAATgCAWIgHAvgAgJAAQgQADgNAIQgMAIgGALQgFALADAPQACAMAJAFQAHAFAMAAQALAAAOgEIAagKQANgFALgHIAUgLIABgUIgBgUQgKgCgLgCQgKgCgMAAQgRAAgQAFg");
	this.shape_4.setTransform(30.9,4.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AhsifIArgDIAAAeQALgMAMgFQANgHAKgDQAMgDALgCQANAAANAFQAOADALAIQAMAGAKAKQAKAKAIAMQAHANAEANQADAPAAAPQgBARgEAPQgFAPgHAMQgIAMgKAKQgJAKgMAHQgLAHgMAEQgMADgMABQgLgBgMgDQgLgDgNgFQgNgHgNgKIgBCGIglABgAgXh3QgKAFgJAGQgJAHgGAJQgGAKgDAKIAAAlQADANAGAKQAHALAIAFQAJAHAKADQAKADAKABQANABAMgGQAMgGAKgIQAJgKAFgMQAGgNAAgOQABgPgEgNQgFgNgIgLQgJgJgNgHQgMgFgOAAQgMAAgLAEg");
	this.shape_5.setTransform(6.3,10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AAGgnIgvCJIg7AFIgmjMIAogDIAfClIA6ilIAlACIAoCnIAbipIAsACIgnDOIg7ABg");
	this.shape_6.setTransform(-33.3,5.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgpBsQgTgIgOgPQgOgQgJgVQgIgWAAgaQAAgZAIgVQAJgWAOgPQAOgPATgJQATgIAWAAQAVAAAUAJQATAJAPAPQAOAQAIAVQAJAVAAAYQAAAZgJAVQgIAWgOAPQgPAQgTAJQgUAIgVAAQgWAAgTgIgAgdhJQgMAHgHAMQgIAMgDAOQgEAPAAANQAAAOAEAPQAEAOAHAMQAJALALAIQAMAHAQAAQARAAAMgHQALgIAIgLQAJgMADgOQAEgPAAgOQAAgNgEgPQgDgOgIgMQgIgMgMgHQgMgHgRAAQgQAAgNAHg");
	this.shape_7.setTransform(-60.6,5.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AhZAxIgCgxIgBgmIgBgdIgBgqIAtAAIAAAYIABAXIAAAYIAOgUQAIgLALgJQAKgKALgHQAMgHAOgCQAQgBANAHIAMAJQAFAFAFAIQAEAJAEALQADALABAQIgnAOIgBgQIgFgNIgEgIIgHgGQgHgFgIABQgGAAgGAEIgMAKIgNAOIgMAPIgNAQIgJAOIAAAdIABAbIABAWIABAQIgqAFIgDg9g");
	this.shape_8.setTransform(-82.7,5.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AhZAxIgBgxIgBgmIgBgdIgCgqIAtAAIABAYIAAAXIAAAYIAOgUQAIgLAKgJQALgKAMgHQAMgHAOgCQAQgBANAHIAKAJQAGAFAEAIQAGAJACALQADALACAQIgnAOIgCgQIgDgNIgGgIIgGgGQgGgFgJABQgGAAgGAEIgMAKIgNAOIgNAPIgLAQIgKAOIABAdIAAAbIABAWIABAQIgrAFIgCg9g");
	this.shape_9.setTransform(-104.8,5.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AA+BxIACgcQgYAPgXAHQgWAHgVAAQgOAAgNgDQgNgEgLgIQgKgHgGgMQgGgMAAgQQgBgQAFgNQAFgMAJgKQAIgJALgHQALgHANgEQANgEAPgCQANgCAOAAIAYABIAUACQgDgKgEgJQgDgKgGgHQgGgIgJgFQgIgFgMAAQgHAAgKADQgJACgMAGQgMAGgOAKQgOAKgQARIgYgdQATgSASgLQARgMAPgGQAQgHANgCQAMgCAKAAQASAAAPAGQAOAGALALQALALAHAOQAHAPAFARQAFAQACASQACARAAASQAAATgCAWIgHAvgAgIAAQgRADgMAIQgNAIgFALQgHALAEAPQACAMAJAFQAHAFAMAAQALAAAOgEIAZgKQANgFAMgHIAUgLIAAgUIgBgUQgJgCgLgCQgLgCgKAAQgSAAgPAFg");
	this.shape_10.setTransform(-130.1,4.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(3,1,1).p("A0TkNMAonAAAQCgAAAACgIAADbQAACgigAAMgonAAAQigAAAAigIAAjbQAAigCgAAg");
	this.shape_11.setTransform(0,4.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("A0TEOQigAAAAigIAAjbQAAigCgAAMAonAAAQCgAAAACgIAADbQAACgigAAg");
	this.shape_12.setTransform(0,4.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-147.5,-31.7,295.1,65.1);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4BA921").s().p("ACRB4QhYiEgkgqQhJhbhagiQhNgThOAaQhPhBhWg8QgMgOgTgKQBFgVBAgIQCMgdBfAGQEFASDWCwQAJAXAIAbQA3C1A7EIQAEAfAJAcQjCgsibjTg");
	this.shape.setTransform(-85.6,-142.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#43870F").s().p("AE/HsQgWhEgag8QiUlCkUjsQEIBkDFCBQAUhviBh0QgsghjKhVQlnh8jPhPQBlgNBwAQQDdAbDIB5QB1BCBzBgQC8CiCsDyQgFAkgKAoQgZCChFAjIiIA5IgTASQgMgOgSgOg");
	this.shape_1.setTransform(-82.5,-209.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#39740C").s().p("AAiAsQgRgNgNgNQjji9kJhbIgKAAQAFgEAFAAIBAgHQDOBPFnB8QDKBUAsAhQCCBzgVBxQjEiCkKhlg");
	this.shape_2.setTransform(-101.4,-233.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4F9525").s().p("ABmD0QAFg7AChoIAUhAIAAgIQAnBaAQBbQg1Cvh+CvQBQh0ARi0gAiXi6QACimgUhpIgIhSQAvByAoBWQgLErgiDJQgCBpguA0QAckWAEjig");
	this.shape_3.setTransform(-0.1,-196.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5AA42D").s().p("AhBMAQgbAAgbgKQgfgJgagTQiHhchSgQQg0gOhXADQhgAHgtgBQgtgBgsgJQgJgcgEgfQg7kIg3i2QAEgNAJgJIATgSICIg5QBFgiAYiCQALgoAFgkQA2kqgNjsQAUBpgCCnQgEDjgcEVQAugzADhpQAijKAKkrIBPDEQBIDfgEC+IA2i9QgDBpgFA6QgRCzhPB1QB9ivA2iuQAdCLgZCRQB7g4AniQQAAAJAEAOIAAAKQgBAHAFAKIgCBfQA6ASAygoQAzgnABg7QA2AOA2AhQAgg+Aig/QCmgcC2AIQB+gCB+AUQiQArhuAqQhlAnhEAnQkpCrgyEhQgeCigqAoQgWASgYAJQgWAJgZAAIgUgCg");
	this.shape_4.setTransform(40.1,-165.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#5AA42D","#418515","#E5CD87"],[0,0.722,1],1.3,-143.7,-8.6,85.9).s().p("AnIHsQh6guhciOIgfgpQgbgqgbgkQjllAknj1QBOgaBNATQBaAiBLBbQAkAqBXCFQCcDSDBAsQAsAJAtABQAuABBggHQBXgDA0AOQBSAQCFBcQAbATAfAJQAbAKAbAAQAkAFAggMQAYgJAVgSQAqgoAfiiQAykgEpirQBEgnBlgoQBugqCQgrQAKAAAIAFIAXAAQAlAFAkAJQg0AcgtAeQgEAAgFAEQiTBUh3BhQizCEiACYQiUC2g1B+QghBDhFBMQkIBCjNAAQigAAh9gog");
	this.shape_5.setTransform(12.5,-121.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#39782E").s().p("AFaF1IhOjEQgohWguhxIAHBQQANDsg2EqQirjzi8iiIgHhSQgZjKhAi7QgviEhBh8QA7AUA0AcQBXAlBlBGICXBrIAbASQB+BHA2ABQgRgpgfgvQg9hNhxhYQi1iLlDjNQC5BHBrA4QDgB7B7CWIAkApQAWAhAaAkIAoBGQA9BxAvCRQAsCJAsD1QAEAyAOAxIgUA/Ig2C9QAEi+hJjfg");
	this.shape_6.setTransform(-36,-248.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#315D32").s().p("AHENoQgih7gIhbIAAgSQgDhoALhRQAggSAggJQAkgRAhgMQCohGCWgZQAggIAgAAQggASghAXQj7C0iAEtQgTAsgGAtQgIgRgEgSgAnlCQIgbgSIiYhrQhmhFhWgkQg1gdg6gUQhTiah0iPQBEAYA+AcQFCDNC3CLQBxBXA9BOQAfAuARApQg2AAh+hIgAGHABQA4ikBHh5QC/hPB2ghQCsgvCagHIAKAAQgFABgFAFQljCFj3DVQiBBlhCBqQAPg1AUg3gAkGiVQgWgygMgpIAAgcQARjKghi+QgViAgrh3QAJAOAOAJQAeBuAdByQBMFHAVF6QgjhrgehXg");
	this.shape_7.setTransform(18.4,-288.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3E713F").s().p("AnNNXQgRhagmhbIAAAIQgOgxgEgyQgsj0gsiJQgviSg9hwIgohGQgagkgVgiQAriTAQiaQAMAqAXAxQAdBXAjBtQgVl8hMlHQgchxgfhuQBQBqA9B1QBxDhApERQARBMAHBRQA8g0BSgxQBgg1DAhOIB6gvQhFB6g5ClQgUA2gPA2QBChrCAhlQD4jWFiiFQAFgFAFAAIAAAAQA7gDA6AFQhAAag/AeQl/C8kaFcQgKAFgEAIQiTC8hfDIQBqg5BRgiQgKBRADBpIAAASQAHBbAiB7QAEASAHARQAGguATgrQCAkuD8i0QAhgWAfgSQBDgNA/ABQhpCDhdCFQjMEeiaE1QgiA/gfA/Qg2ghg2gPQgBA7gzAoQgyAog7gSIAChfQgFgKABgJIAAgJQgEgPAAgIQgnCRh7A4QAZiRgdiNg");
	this.shape_8.setTransform(64.6,-263);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CB9A0E").s().p("AhTIEIgNgKIgugnIgXAMIgWgFIgCgBIAigRIgugqIg4AeIgPgMIA7geIgsgqIglAUIgQgLIApgWIgqgsIgnAWIgIgOIAkgUIgFgGIgagdIgKgLIgCgDIgFgGIABgEIABgHIACgBIADgCIAlgUIASgKIgSgWIgNgRIgGgHIgNAHIgSALIACAMIgDgPQgCgGAAgFQgCgEABgEIABAFIAYgOIgfgoIAAgeIA6glIgjgyIgSANIABgUIAJgGIgHgKIACgWQALAMAHALIAyglIgjg2QgKAIgRAKIAHgxQgBAMAAAQIANgJIgMgTIAFgYIATAhIAwgoIgMgTIgUghIAAgCQgCgCgHgIIAAAAIAKgTIAHgMIAPgbIACgCIAOgMIgFgJIABgCQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAABAAQAAgBAAAAQABAAAAAAQABAAAAAAQABAAAAAAIADACIADgGIAEAHIAvgtIgOgbIAMgNIAPAbIApgqIgJgQIAOgKIAHAMIAAABIAeghIAEgEIAHgIIABgCIANgFIAOgHIgBABIgJALIAdAzIAPAXIAUgXIAUgZIgUggIgBgBQAFgFAGgFIACAAIABAAIASAdIAXgfIAAAAIACABIAGAEIACABIgBABIgbAjIASAcIAYAjIAdgjIAJgNIADgDIgDgDIgdgrIAHgEIAGgFIAAABIAIALIAAAAIAAAAIAGAIIACAEIANASIAOgUIAIAIIgQAWIAuA9IAkgwIAIAIIglAxIAvA7QAYgeAWgeIAIAIIguA9IAxA3IAqg1IAGALIgoAzIAfAhIAXgfIADAOIgSAZIA0A0IAHgJIADAIIABAFIgCAEIgIAKIgtA2IA2AyIAJAIIgIAJIgwA0IA2AuIAKAIIgJAJQgZAagaAZIA1AsIABAAIALAJIgIAIIgKAJIgrAqIAoAfIAAABIgCAMIgvgkIghAeIA9ArIgGAIIg/gsIgKAJIgwAoQAjAbAlAZIgCACIgBAJQgogbgmgdIg9AvQAqAhAsAeIgJAHQgsgfgqggQgfAXggAVIA8AuIgDABIgJAEIg6gsIg7AmIAqAhQgIAAgJgBIgDgBIgggZIgrAaIgNAHIgLAGIgNgLgAiDHNIA6AxIA5ggIg6gxIg5AggAg/GnQAcAZAeAXIA7gmIg4gwIg9AmgAi9GYIAtApIA5gfIgugqIg4AggAAHF7IA5AwIA+gsIg5guIg+AqgAh6FxIAuArIA9glIgvgrIg8AlgAj1FhIArArIA4gfIgsgsIg3AggABPFKIA5AuIA9guIg5gtIg9AtgAgzFFIAvArIA9gpIgxgrIg7ApgAizE6IAtAsIA9glIgugtIg8AmgAkqEoIApAtIA3ggIgqgtIg2AggACVEWIA5AtIA7gxIg6grIg6AvgAATEUIAwAsIA9gsIgzgsIg6AsgAhsEMIAuAtIA8goIgvgtIg7AogAjpEBIArAtIA8glIgrgvIg8AngAlGDdIgaAOIASAWIAIAJIARASIA2ggIgggkIgKgMIgdARgABXDgIAzAsIA6gvIg0grIg5AugADkDVIgMAKIA5AsIAhgdIg3gtIgXAUgAgnDcIAuAuIA7gsIgxguIg4AsgAiiDSIArAvIA7goIgugvIg4AogAkdDFIARAUIAZAcIA7gmIgYgdIgQgSIg9AlgAEEC5IA3AtIA1g0Ig3grIg1AygAAbCoIAxAuIA5guIgzgtIg3AtgACdCnIgEADIA0AsIAYgUIALgKIgzguIggAdgAhfCiIAtAvIA5grIgugvIg4ArgAjUCYIAXAbIARAUIA4goIgngsIg5AlgAlMCLIAlAuIA9glIgmgvIg8AmgADHCCIAzAuIA1gyIg1gtIgzAxgABcBzIAyAuIAPgNIAWgTIgygwIglAigAANBLIgqAkIAuAvIA3gtIgxgvIgKAJgAiQBrIABAAIAmAsIA4gqIgngqIg4AogAkEBdIAmAvIA4glIgogwIg2AmgAl6BNIAkAwIA8glIglgxIg7AmgACLBIIAyAwIAzgwIgzgwIgyAwgAEDBIIA1AuIAzgzIg2guIgyAzgAAhA5IAwAwIAFgDIAigfIgwgxIgnAjgAhNA6IAmArIA1gtIglgpIg2ArgAjCAvIAoAwIA3goIgRgUIgYgdIg2ApgAkyAeIAkAyIA3glIgngxIg0AkgADHAOIAzAxIAygyIg1gwIgwAxgABSAMIAwAyIAygvIgxgyIgxAvgAgNAHIAlAoIAngjIgBgBIgjgmIgoAigAiBgBIAgAlIAKALIA2gqIglgpIgHgIIg0ArgAjygPIAnAxIA1gmIgogzIg0AogAleghIAjAyIA0gjIglg0IgyAlgACMgsIAxAxIAxgxIgygyIgwAygAEAgsIA0AwIAwg0Ig1gxIgvA1gAA8g5IgWAUIAjAoIAxgvIgQgSIgTgUIgbAZgAhCg1IAPASIAcAgIApgjIgqg0IgqAlgAizhAIAoAzIA1gqIgqg0IgzArgAkfhQIAkA1IA0goIgng1IgxAogAgMhlIgBABIAqA0IAfgcIASgRIgtg0IgtAsgADFhoIAyAzIAvg0Ig0gzIgtA0gABhhcIAcAeIAHAIIAvgxIgdggIgFgEIgwAvgAhViRIggAdIApA0IArglIgpg2IgLAKgAjiiBIAnA1IAygqIgog2IgxArgAlLiTIAjA3IAygnIglg3IgwAngAAsibIAsA1IAwgvIgug1IguAvgAg/ilIApA2IAKgJIAlgjIgqg3IguAtgACbiVIAOAOIATAWIAug0IghgiIguAygAD7inIA0A0IAsg2Ig1gzIgrA1gAiei9IgIAHIAoA3IApglIACgCIgng4IgkAhgAjmjpIgpAkIAlA4IAwgqIgmg4IgGAGgABljVIAtA2IAvgyIgwg1IgsAxgAgGjdIAqA3IAuguIgsg4IgsAvgADTjSIACACIAeAhIArg1IgfghIgsAzgAhvjqIAoA6IAugtIgpg5IgtAsgAjVj5IAnA4IAQgPIAcgaIgmg6IgtArgAkXlBIgOANIgUARIgFAPIAAABIACAEIAXAmIAOAYIAvgqQgXgjgWglIgCACgACbkRIAvA2IAtgzIgJgKIgogtIgrA0gAAwkXIAsA4IAtgxIgug4IgrAxgAg3kiIApA6IAsguIgqg6IgrAugAickxIAmA8IAsgsIgng8IgrAsgAkJlOQAVAlAYAkIAtgsQgWgkgVgmIgvAtgABllUIAuA5IArgzIgrg0IgEgFIgqAzgAgBlcIApA6IArgwIgsg8IgoAygAhmlpIAnA8IArguIgog9IgqAvgAjOmHQAVAmAWAkIArgsQgXgkgVglIgqArgAAxmaIAsA8IAqgzIgtg8IgpAzgAgVnHIgcAiIAoA+IApgxIgggvIgJgPIgMAPgAiRnHIgHAIQAUAmAXAkIAqguIgXgkIgWglIghAlg");
	this.shape_9.setTransform(1.2,141.2,4.523,4.523,1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F3D24B").s().p("AmHSHQhMgtg4iXQhikWg7kQQhHlUA2jqQCsgqCLicQB9iKA5izQAOgvAKgsQAwjGALgWQArh/BVgrQBOgxBsAeQBaATBdBGQCrB4BbDFQBbC/gIDVQgHCMg5C0QgFAShtEeQglBjhRC8QgOApgTAkQg0Bwg4BRQhYCKh3BeQiABkiNAeQgtALgoAAQhAAAgygdgAhEtxIgBgGIgEgXQgEgNgJgFQAHAYALAXg");
	this.shape_10.setTransform(-61.5,115.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFCC00").s().p("EgUvAg9QgfgogagqQgEgJAAgOQAEgJAKgEQAKgJAIAFQAkAFAoAKIAKABQAAgFgEgFQglgOgggXQgjgTgkgcQhLg8g+hXQgXgbgRgcQgWgggOggQgdg7gOg8QgIgWgEgXQgMgugEgtQAAgIAEgFIAAgFQAKgIAJgFQAJAAAJAGIARAIIABgIQgEgOAAgKIgJgJIgXggIAAgNIAKggQAAgEAFAAIASgJQgNgOgJgOIAAgJQgDgNAAgKQgIgogFgpQAAgFgEgEIgIg6IgIhGQgNgbAAgWQgEgWAFgOQgFgJAAgKQAAgJgEgJIgHhHQgIghgFgcQgEgbAKgfIADiDIAAgSQAGh+ASjDQAEgtAGgyIAKhDIALhnQAFgyAHg1IBLnAIAAgEIAJgkQAKgkATgpIAAgEIAJgSQAKgbASgkQAUg6Acg6IAEgFIAvhRQAPgWASgjIBChxQAAgJAEgEQAGgOAJgNQAOgjARghQAFgEABgFQAFgIAJgEQAJgFAJAFQAEAAAJAEIAJgaQAqhVAdg3QAuhVA9g+QAXgkAggVQAJgOAKgJIAEAAQAFgJAIABQAKAAAIAEQAKAFAEAEIAAAEQBFhGBOg5QAbgXAmgWIANgJQAJgEAFgEQAagSAdgNQAAgEAEAAQAugbAugWIAJAAIAbgjQAFgFAFgDQARgKAlgNIBAgZQAEAAAFAEIBMA4IAuhIQAFgJAFgEIASAAQAEAAAJAFIA+A3IAFAAIAJgJQAAgFgEAAQAWgZAcgTIAJAAQAFgEAFAAQAJgJAOgJIAIgJQAFgEAJgDQAJAAAIADQAJAKAAAEIAWA3IAlg1QAAgJANgFIAOAAIAFAAQAIAFAFAJIAIAOIAAgEQAFAEAAAFQAJAJAXAKQAEAAAEAFQgEAAAAADQAJAFANAFIASABQASAEASgEIAFAAIAKAAQAIgFAFAAIAFAAQANgEASgNQAPgNANgJQAFAAADAFQAPAFAAAIIANAkQAMAUAJASIAXgOQCiEyB7EAQCYFIBfDxQFBMvAOK5QAMJoj6FTQl9IcwVAkQiCAFiEAAQlVAAlogkgApTx/QhWArgrB+QgLAWgwDGQgKAtgOAuQg5C0h9CKQiLCcisAqQg2DpBHFVQA7EQBiEVQA4CXBMAuQBRAvB2gdQCNgeCBhlQB3heBYiJQA4hRA0hxQANAKARAJQgHgJgOgFQgEgFgFAAQATgkAOgoQBRi8AlhjQBskeAFgTQA5izAHiNQAIjVhbi/QhajEirh5QhdhFhagUQgpgLglAAQg8AAgwAfgALN2uQAEAEAEAFQgIgMABgLIgBAOgAbLJSQAEAAAAgDIAAAIQAAgFgEAAgAawBBQAFgFAIgFIAKAhQgKgPgNgIg");
	this.shape_11.setTransform(-1.2,115.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E9BC06").s().p("EgJvAj6Ig1gtQgSgFgRgKQgpgOhbgmQhQglgogOQgJgFgNgFQgqgOg+gPIgJAAQhNgXgogPQhjgmhag8Ig+g4QgEAAAAgFQgtglgfguQH1AyHOgTQQVgkF9ocQD6lTgMpoQgOq5lBsvQhfjxiYlIQh7kAiikyIAlgRQAEgFAJABQAJAAAFAFIBqBfQAOgCAWADIAEAAQAlAPAfAPQAkASAgASIAFAAIAAAFIANAJIAEAAQAoAcAkAlQARAOASAXQAFAAAAAFQAfAgAaAgQBIAdBIBJQAVAXAXAcIAjAlQAaAcAYAbQA0BAAZB2QAlgIASAbIAAAFQAJAJAIATQANAWANAcQAeBNAQBoQAJAgAEAbQAEAlAIAaIANBSQAOgJAJgDQAIgKAKAFQANAAAFAKIAJAJQARAbASAgQARAkAVA2QAFAPAAAJIAVA2IAOAlQgBANAEAKIAJAkQgOgKgIgIQgTAVgSAXIA+A4QgGA2gTBRQgTBRgGAyQAeA6AWA3IggAgQAWATASASIgkAsQAoAhAoAkQAMBJgCBIIAAAIIgLCjQgcAfghAfIAtAlQgSATgTAVQANAJANAPIAAADQAkBBgLBjQgLBRgmCeIAFAAIAwAqQgSAMgTAXQgWASgXAWQgPBEgOAoIgaBfQAKAEAEAJQAEAKAFAIIAAAFQANApgFAxIgBAFQAAAbgFAXQgKAggOAfQgXAtggAxQgFAIgGAFQgNAOgNAXIghAnQh+CPhEBHQgEAFgFADQgBASgEAXIAEAFQAEAFAFAJQAEAFgEANQAAAFgKAIQgJAFgKAIIgDAAQguAphOA9QgzAogyAkQgYAMgRAOQhlA9hSAVQgkAJhXAMQg/AIglAJQgIAEgFAAQgTAEgWANQgkAMg0AcQhVArguANQg/AQg7AEQgpAAgogGQgEgEgFAAQg1gKgqgTQhQAUhXAEQgtAEgogBQgdANgbARIgyAbQgggcgfgYg");
	this.shape_12.setTransform(20.7,144.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-175.4,-379.4,350.9,758.8), null);


(lib.sets2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// question
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#793F06").s().p("An0AFISqnzI1rPdg");
	this.shape.setTransform(457.1,98.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("Al/C0Ik0lqIVnCeIwzDPg");
	this.shape_1.setTransform(457.1,50.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9E5104").s().p("AnmgpIQyjQIAQgFIgNAKIyqHzg");
	this.shape_2.setTransform(467.4,73.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FBAA30").s().p("AowBXIkXlLIaPHjIgCABIgQAFg");
	this.shape_3.setTransform(444,23.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("A7sDfMA2rgKQIAuDiMg1WAKBg");
	this.shape_4.setTransform(307.3,86.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#4BA921").s().p("ACRB4QhYiEgjgqQhLhbhZgiQhOgThNAaQhPhBhWg8QgMgOgTgKQBFgVA/gIQCNgdBeAGQEGASDVCwQAKAXAIAbQA4C1A6EIQAEAfAJAcQjCgsibjTg");
	this.shape_5.setTransform(-71.5,-125.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#43870F").s().p("AE+HsQgUhEgbg8QiUlCkUjsQEIBkDFCBQAUhviBh0QgtghjIhVQloh8jOhPQBjgNBwAQQDfAbDHB5QB1BCBzBgQC8CiCsDyQgEAkgMAoQgXCChGAjIiIA5IgSASQgNgOgTgOg");
	this.shape_6.setTransform(-68.4,-193);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#39740C").s().p("AAiAsQgRgNgNgNQjji9kJhbIgKAAQAFgEAFAAIBAgHQDOBPFnB8QDKBUAsAhQCCB0gVBwQjEiCkKhlg");
	this.shape_7.setTransform(-87.3,-217);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#4F9525").s().p("ABmD0QAFg7AChoIAUhAIAAgIQAnBbAQBaQg1Cvh+CvQBQh0ARi0gAiXi6QACingUhoIgIhSQAvByAoBWQgLErgiDJQgCBpguA0QAckWAEjig");
	this.shape_8.setTransform(14,-180.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#5AA42D").s().p("AhBMAQgbAAgbgKQgfgJgagTQiHhchSgQQg0gOhXADQhgAHgtgBQgtgBgsgJQgJgcgEgfQg7kIg3i2QAEgNAJgJIATgSICIg5QBFgiAYiCQALgoAFgkQA2kqgNjsQAUBpgCCnQgEDjgcEVQAugzADhpQAijKAKkrIBPDEQBIDfgEC+IA2i9QgDBpgFA6QgRCzhPB1QB9ivA2iuQAdCLgZCRQB7g4AniQQAAAJAEAOIAAAKQgBAHAFAKIgCBfQA6ASAygoQAzgnABg7QA2AOA2AhQAgg+Aig/QCmgcC2AIQB+gCB+AUQiQArhuAqQhlAnhEAnQkpCrgyEhQgeCigqAoQgWASgYAJQgWAJgZAAIgUgCg");
	this.shape_9.setTransform(54.2,-149);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#5AA42D","#418515","#E5CD87"],[0,0.722,1],1.3,-143.7,-8.6,85.9).s().p("AnJHsQh5guhdiOIgegpQgbgqgbgkQjllAkmj1QBNgaBNATQBaAiBLBbQAkAqBXCFQCcDSDCAsQArAJAuABQAtABBggHQBXgDA0AOQBSAQCGBcQAaATAfAJQAaAKAbAAQAlAFAggMQAXgJAXgSQApgoAfiiQAxkgEqirQBEgnBlgoQBtgqCRgrQAKAAAIAFIAXAAQAlAFAjAJQgzAcguAeQgDAAgFAEQiSBUh4BhQiyCEiBCYQiVC2g1B+QgfBDhGBMQkIBCjNAAQigAAh+gog");
	this.shape_10.setTransform(26.6,-105);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#39782E").s().p("AFbF1IhPjEQgnhWgwhxIAIBQQANDsg2EqQisjzi7iiIgGhSQgZjKhBi7QgwiEhAh8QA6AUA2AcQBVAlBnBGICXBrIAbASQB9BHA3ABQgSgpgfgvQg9hNhxhYQi2iLlBjNQC4BHBrA4QDfB7B8CWIAkApQAWAhAaAkIAoBGQA9BxAvCRQArCJAtD1QADAyAOAxIgUA/Ig1C9QAEi+hIjfg");
	this.shape_11.setTransform(-21.9,-231.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3E713F").s().p("AnNNXQgRhagmhbIAAAIQgOgwgEgzQgsj1gsiIQgviSg9hxIgohFQgaglgVggQAriUAQiaQAMAqAXAxQAdBXAjBtQgVl8hMlHQgchxgfhuQBQBqA9B1QBxDhApERQARBMAHBRQA8g0BSgxQBgg0DAhPIB6gvQhFB5g5CmQgUA3gPA1QBChrCAhlQD4jWFiiFQAFgFAFAAIAAAAQA7gDA6AFQhAAag/AeQl/C9kaFbQgKAFgEAIQiTC8hfDJQBqg6BRgiQgKBRADBoIAAATQAHBaAiB8QAEARAHASQAGgtATgsQCAkuD8i0QAhgWAfgSQBDgNA/ABQhpCChdCGQjMEfiaE0QgiA/gfA/Qg2ghg2gPQgBA8gzAnQgyAog7gTIACheQgFgKABgJIAAgJQgEgPAAgIQgnCRh7A4QAZiRgdiNg");
	this.shape_12.setTransform(78.7,-246.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#315D32").s().p("AHENoQgih7gIhaIAAgTQgDhoALhRQAggSAggJQAkgRAhgMQCohGCWgZQAggIAgAAQggASghAXQj7CziAEuQgTAsgGAtQgIgRgEgSgAnlCQIgbgSIiYhrQhmhFhWgkQg1gcg6gVQhTiah0iPQBEAYA+AcQFCDNC3CLQBxBXA9BOQAfAuARApQg2AAh+hIgAGHACQA4ilBHh5QC/hPB2ghQCsgvCagHIAKAAQgFABgFAFQljCFj3DVQiBBkhCBsQAPg2AUg2gAkGiVQgWgxgMgqIAAgcQARjKghi+QgViAgrh2QAJANAOAJQAeBvAdBxQBMFHAVF6QgjhrgehXg");
	this.shape_13.setTransform(32.5,-272.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CB9A0E").s().p("AhTIEIgNgKIgugnIgXAMIgWgFIgCgBIAigRIgugqIg4AeIgPgMIA7geIgsgqIglAUIgQgLIApgWIgqgsIgnAWIgIgOIAkgUIgFgGIgagdIgKgLIgCgDIgFgGIABgEIABgHIACgBIADgCIAlgUIASgKIgSgWIgNgRIgGgHIgNAHIgSALIACAMIgDgPQgCgGAAgFQgCgEABgEIABAFIAYgOIgfgoIAAgeIA6glIgjgyIgSANIABgUIAJgGIgHgKIACgWQALAMAHALIAyglIgjg2QgKAIgRAKIAHgxQgBAMAAAQIANgJIgMgTIAFgYIATAhIAwgoIgMgTIgUghIAAgCQgCgCgHgIIAAAAIAKgTIAHgMIAPgbIACgCIAOgMIgFgJIABgCQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAABAAQAAgBAAAAQABAAAAAAQABAAAAAAQABAAAAAAIADACIADgGIAEAHIAvgtIgOgbIAMgNIAPAbIApgqIgJgQIAOgKIAHAMIAAABIAeghIAEgEIAHgIIABgCIANgFIAOgHIgBABIgJALIAdAzIAPAXIAUgXIAUgZIgUggIgBgBQAFgFAGgFIACAAIABAAIASAdIAXgfIAAAAIACABIAGAEIACABIgBABIgbAjIASAcIAYAjIAdgjIAJgNIADgDIgDgDIgdgrIAHgEIAGgFIAAABIAIALIAAAAIAAAAIAGAIIACAEIANASIAOgUIAIAIIgQAWIAuA9IAkgwIAIAIIglAxIAvA7QAYgeAWgeIAIAIIguA9IAxA3IAqg1IAGALIgoAzIAfAhIAXgfIADAOIgSAZIA0A0IAHgJIADAIIABAFIgCAEIgIAKIgtA2IA2AyIAJAIIgIAJIgwA0IA2AuIAKAIIgJAJQgZAagaAZIA1AsIABAAIALAJIgIAIIgKAJIgrAqIAoAfIAAABIgCAMIgvgkIghAeIA9ArIgGAIIg/gsIgKAJIgwAoQAjAbAlAZIgCACIgBAJQgogbgmgdIg9AvQAqAhAsAeIgJAHQgsgfgqggQgfAXggAVIA8AuIgDABIgJAEIg6gsIg7AmIAqAhQgIAAgJgBIgDgBIgggZIgrAaIgNAHIgLAGIgNgLgAiDHNIA6AxIA5ggIg6gxIg5AggAg/GnQAcAZAeAXIA7gmIg4gwIg9AmgAi9GYIAtApIA5gfIgugqIg4AggAAHF7IA5AwIA+gsIg5guIg+AqgAh6FxIAuArIA9glIgvgrIg8AlgAj1FhIArArIA4gfIgsgsIg3AggABPFKIA5AuIA9guIg5gtIg9AtgAgzFFIAvArIA9gpIgxgrIg7ApgAizE6IAtAsIA9glIgugtIg8AmgAkqEoIApAtIA3ggIgqgtIg2AggACVEWIA5AtIA7gxIg6grIg6AvgAATEUIAwAsIA9gsIgzgsIg6AsgAhsEMIAuAtIA8goIgvgtIg7AogAjpEBIArAtIA8glIgrgvIg8AngAlGDdIgaAOIASAWIAIAJIARASIA2ggIgggkIgKgMIgdARgABXDgIAzAsIA6gvIg0grIg5AugADkDVIgMAKIA5AsIAhgdIg3gtIgXAUgAgnDcIAuAuIA7gsIgxguIg4AsgAiiDSIArAvIA7goIgugvIg4AogAkdDFIARAUIAZAcIA7gmIgYgdIgQgSIg9AlgAEEC5IA3AtIA1g0Ig3grIg1AygAAbCoIAxAuIA5guIgzgtIg3AtgACdCnIgEADIA0AsIAYgUIALgKIgzguIggAdgAhfCiIAtAvIA5grIgugvIg4ArgAjUCYIAXAbIARAUIA4goIgngsIg5AlgAlMCLIAlAuIA9glIgmgvIg8AmgADHCCIAzAuIA1gyIg1gtIgzAxgABcBzIAyAuIAPgNIAWgTIgygwIglAigAANBLIgqAkIAuAvIA3gtIgxgvIgKAJgAiQBrIABAAIAmAsIA4gqIgngqIg4AogAkEBdIAmAvIA4glIgogwIg2AmgAl6BNIAkAwIA8glIglgxIg7AmgACLBIIAyAwIAzgwIgzgwIgyAwgAEDBIIA1AuIAzgzIg2guIgyAzgAAhA5IAwAwIAFgDIAigfIgwgxIgnAjgAhNA6IAmArIA1gtIglgpIg2ArgAjCAvIAoAwIA3goIgRgUIgYgdIg2ApgAkyAeIAkAyIA3glIgngxIg0AkgADHAOIAzAxIAygyIg1gwIgwAxgABSAMIAwAyIAygvIgxgyIgxAvgAgNAHIAlAoIAngjIgBgBIgjgmIgoAigAiBgBIAgAlIAKALIA2gqIglgpIgHgIIg0ArgAjygPIAnAxIA1gmIgogzIg0AogAleghIAjAyIA0gjIglg0IgyAlgACMgsIAxAxIAxgxIgygyIgwAygAEAgsIA0AwIAwg0Ig1gxIgvA1gAA8g5IgWAUIAjAoIAxgvIgQgSIgTgUIgbAZgAhCg1IAPASIAcAgIApgjIgqg0IgqAlgAizhAIAoAzIA1gqIgqg0IgzArgAkfhQIAkA1IA0goIgng1IgxAogAgMhlIgBABIAqA0IAfgcIASgRIgtg0IgtAsgADFhoIAyAzIAvg0Ig0gzIgtA0gABhhcIAcAeIAHAIIAvgxIgdggIgFgEIgwAvgAhViRIggAdIApA0IArglIgpg2IgLAKgAjiiBIAnA1IAygqIgog2IgxArgAlLiTIAjA3IAygnIglg3IgwAngAAsibIAsA1IAwgvIgug1IguAvgAg/ilIApA2IAKgJIAlgjIgqg3IguAtgACbiVIAOAOIATAWIAug0IghgiIguAygAD7inIA0A0IAsg2Ig1gzIgrA1gAiei9IgIAHIAoA3IApglIACgCIgng4IgkAhgAjmjpIgpAkIAlA4IAwgqIgmg4IgGAGgABljVIAtA2IAvgyIgwg1IgsAxgAgGjdIAqA3IAuguIgsg4IgsAvgADTjSIACACIAeAhIArg1IgfghIgsAzgAhvjqIAoA6IAugtIgpg5IgtAsgAjVj5IAnA4IAQgPIAcgaIgmg6IgtArgAkXlBIgOANIgUARIgFAPIAAABIACAEIAXAmIAOAYIAvgqQgXgjgWglIgCACgACbkRIAvA2IAtgzIgJgKIgogtIgrA0gAAwkXIAsA4IAtgxIgug4IgrAxgAg3kiIApA6IAsguIgqg6IgrAugAickxIAmA8IAsgsIgng8IgrAsgAkJlOQAVAlAYAkIAtgsQgWgkgVgmIgvAtgABllUIAuA5IArgzIgrg0IgEgFIgqAzgAgBlcIApA6IArgwIgsg8IgoAygAhmlpIAnA8IArguIgog9IgqAvgAjOmHQAVAmAWAkIArgsQgXgkgVglIgqArgAAxmaIAsA8IAqgzIgtg8IgpAzgAgVnHIgcAiIAoA+IApgxIgggvIgJgPIgMAPgAiRnHIgHAIQAUAmAXAkIAqguIgXgkIgWglIghAlg");
	this.shape_14.setTransform(15.3,157.5,4.523,4.523,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3D24B").s().p("AmHSHQhMgtg4iXQhikWg7kQQhHlUA2jqQCsgqCLicQB9iKA5izQAOgvAKgsQAwjGALgWQArh/BVgrQBOgxBsAeQBaATBdBGQCrB4BbDFQBbC/gIDVQgHCMg5C0QgFAShtEeQglBjhRC8QgOApgTAkQg0Bwg4BRQhYCKh3BeQiABkiNAeQgtALgoAAQhAAAgygdgAhEtxIgBgGIgEgXQgEgNgJgFQAHAYALAXg");
	this.shape_15.setTransform(-47.4,132);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E9BC06").s().p("EgJvAj6Ig1gtQgSgFgRgKQgpgOhbgmQhQglgogOQgJgFgNgFQgqgOg+gPIgJAAQhNgXgogPQhjgmhag8Ig+g4QgEAAAAgFQgtglgfguQH1AyHOgTQQVgkF9ocQD6lTgMpoQgOq5lBsvQhfjxiYlIQh7kAiikyIAlgRQAEgFAJABQAJAAAFAFIBqBfQAOgCAWADIAEAAQAlAPAfAPQAkASAgASIAFAAIAAAFIANAJIAEAAQAoAcAkAlQARAOASAXQAFAAAAAFQAfAgAaAgQBIAdBIBJQAVAXAXAcIAjAlQAaAcAYAbQA0BAAZB2QAlgIASAbIAAAFQAJAJAIATQANAWANAcQAeBNAQBoQAJAgAEAbQAEAlAIAaIANBSQAOgJAJgDQAIgKAKAFQANAAAFAKIAJAJQARAbASAgQARAkAVA2QAFAPAAAJIAVA2IAOAlQgBANAEAKIAJAkQgOgKgIgIQgTAVgSAXIA+A4QgGA2gTBRQgTBRgGAyQAeA6AWA3IggAgQAWATASASIgkAsQAoAhAoAkQAMBJgCBIIAAAIIgLCjQgcAfghAfIAtAlQgSATgTAVQANAJANAPIAAADQAkBBgLBjQgLBRgmCeIAFAAIAwAqQgSAMgTAXQgWASgXAWQgPBEgOAoIgaBfQAKAEAEAJQAEAKAFAIIAAAFQANApgFAxIgBAFQAAAbgFAXQgKAggOAfQgXAtggAxQgFAIgGAFQgNAOgNAXIghAnQh+CPhEBHQgEAFgFADQgBASgEAXIAEAFQAEAFAFAJQAEAFgEANQAAAFgKAIQgJAFgKAIIgDAAQguAphOA9QgzAogyAkQgYAMgRAOQhlA9hSAVQgkAJhXAMQg/AIglAJQgIAEgFAAQgTAEgWANQgkAMg0AcQhVArguANQg/AQg7AEQgpAAgogGQgEgEgFAAQg1gKgqgTQhQAUhXAEQgtAEgogBQgdANgbARIgyAbQgggcgfgYg");
	this.shape_16.setTransform(34.8,160.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFCC00").s().p("EgUvAg9QgfgogagqQgEgJAAgOQAEgJAKgEQAKgJAIAFQAkAFAoAKIAKABQAAgFgEgFQglgOgggXQgjgTgkgcQhLg8g+hXQgXgbgRgcQgWgggOggQgdg7gOg8QgIgWgEgXQgMgugEgtQAAgIAEgFIAAgFQAKgIAJgFQAJAAAJAGIARAIIABgIQgEgOAAgKIgJgJIgXggIAAgNIAKggQAAgEAFAAIASgJQgNgOgJgOIAAgJQgDgNAAgKQgIgogFgpQAAgFgEgEIgIg6IgIhGQgNgbAAgWQgEgWAFgOQgFgJAAgKQAAgJgEgJQgDgogEgfQgIghgFgcQgEgbAKgfIADiDIAAgSQAGh+ASjDQAEgtAGgyIAKhDIALhnQAFgyAHg1IBLnAIAAgEIAJgkQAKgkATgpIAAgEIAJgSQAKgbASgkQAUg6Acg6IAEgFIAvhRQAPgWASgjIBChxQAAgJAEgEQAGgOAJgNQAOgjARghQAFgEABgFQAFgIAJgEQAJgFAJAFQAEAAAJAEIAJgaQAqhVAdg3QAuhVA9g+QAXgkAggVQAJgOAKgJIAEAAQAFgJAIABQAKAAAIAEQAKAFAEAEIAAAEQBFhGBOg5QAbgXAmgWIANgJQAJgEAFgEQAagSAdgNQAAgEAEAAQAugbAugWIAJAAIAbgjQAFgFAFgDQARgKAlgNIBAgZQAEAAAFAEIBMA4IAuhIQAFgJAFgEIASAAQAEAAAJAFIA+A3IAFAAIAJgJQAAgFgEAAQAWgZAcgTIAJAAQAFgEAFAAQAJgJAOgJIAIgJQAFgEAJgDQAJAAAIADQAJAKAAAEIAWA3IAlg1QAAgJANgFIAOAAIAFAAQAIAFAFAJIAIAOIAAgEQAFAEAAAFQAJAJAXAKQAEAAAEAFQgBAAgBAAQgBAAAAABQgBAAAAAAQAAABAAABQAJAFANAFIASABQASAEASgEIAFAAIAKAAQAIgFAFAAIAFAAQANgEASgNQAPgNANgJQAFAAADAFQAPAFAAAIIANAkQAMAUAJASIAXgOQCiEyB7EAQCYFIBfDxQFBMvAOK5QAMJoj6FTQl9IcwVAkQiCAFiEAAQlVAAlogkgApTx/QhWArgrB+QgLAWgwDGQgKAtgOAuQg5C0h9CKQiLCcisAqQg2DpBHFVQA7EQBiEVQA4CXBMAuQBRAvB2gdQCNgeCBhlQB3heBYiJQA4hRA0hxQANAKARAJQgHgJgOgFQgEgFgFAAQATgkAOgoQBRi8AlhjQBskeAFgTQA5izAHiNQAIjVhbi/QhajEirh5QhdhFhagUQgpgLglAAQg8AAgwAfgALN2uQAFAEADAFQgIgMABgMIgBAPgAbLJSQAEAAAAgDIAAAIQAAgFgEAAgAawBBQAFgFAIgFIAKAhQgKgPgNgIg");
	this.shape_17.setTransform(12.9,131.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF9900").s().p("AqqBzIhKpYIGlJNIgQmEIFtGlIgumBIGKIaIginTIGtKXg");
	this.shape_18.setTransform(-301,-91.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC6600").s().p("AHFgLIphESIDlk1IoOC7IDNlOIqNE5IFmnrIAAgGIWfFzIAGAJIrEF3g");
	this.shape_19.setTransform(-314.9,-41.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#330000").s().p("AhTUHMAAigoMICFgBMgAiAoNg");
	this.shape_20.setTransform(-176.3,-29,1.489,1.489,-75.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_8
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#660000").ss(10,1,1).p("EhLvg+eMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAMiXeAAAQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAg");
	this.shape_21.setTransform(-12.8,15.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CCCCCC").s().p("EhLvA+fQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAgEhVAg4SQj2D2AAFbMAAABeCQAAFcD2D2QD2D2FbAAMCXeAAAQFcAAD2j2QD2j2AAlcMAAAheCQAAlbj2j2Qj2j2lcAAMiXeAAAIAAAAQlbAAj2D2g");
	this.shape_22.setTransform(-12.8,15.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("EhLvA8JQlbAAj2j2Qj2j2AAlcMAAAheCQAAlbD2j2QD2j2FbAAMCXeAAAQFcAAD2D2QD2D2AAFbMAAABeCQAAFcj2D2Qj2D2lcAAg");
	this.shape_23.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21}]}).wait(1));

	// Layer_11
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(255,255,255,0.02)").s().p("EhNQBCIQngAAlVlVQlVlVAAnhMAAAhf6QAAngFVlVQFVlVHgAAMCagAAAQHiAAFUFVQFUFVAAHgMAAABf6QAAHhlUFVQlUFVniAAg");
	this.shape_24.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_24).wait(1));

}).prototype = getMCSymbolPrototype(lib.sets2, new cjs.Rectangle(-623.5,-407.7,1221.3,846.4), null);


(lib.setscopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// question
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#793F06").s().p("An0AFISqnzI1rPdg");
	this.shape.setTransform(457.1,98.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("Al/C0Ik0lqIVnCeIwzDPg");
	this.shape_1.setTransform(457.1,50.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9E5104").s().p("AnmgpIQyjQIAQgFIgNAKIyqHzg");
	this.shape_2.setTransform(467.4,73.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FBAA30").s().p("AowBXIkXlLIaPHjIgCABIgQAFg");
	this.shape_3.setTransform(444,23.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("A7sDfMA2rgKQIAuDiMg1WAKBg");
	this.shape_4.setTransform(307.3,86.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#4BA921").s().p("ACRB4QhYiEgjgqQhLhbhZgiQhOgThNAaQhPhBhWg8QgMgOgTgKQBFgVA/gIQCNgdBeAGQEGASDVCwQAKAXAIAbQA4C1A6EIQAEAfAJAcQjCgsibjTg");
	this.shape_5.setTransform(-71.5,-125.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#43870F").s().p("AE+HsQgUhEgbg8QiUlCkUjsQEIBkDFCBQAUhviBh0QgtghjIhVQloh8jOhPQBjgNBwAQQDfAbDHB5QB1BCBzBgQC8CiCsDyQgEAkgMAoQgXCChGAjIiIA5IgSASQgNgOgTgOg");
	this.shape_6.setTransform(-68.4,-193);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#39740C").s().p("AAiAsQgRgNgNgNQjji9kJhbIgKAAQAFgEAFAAIBAgHQDOBPFnB8QDKBUAsAhQCCB0gVBwQjEiCkKhlg");
	this.shape_7.setTransform(-87.3,-217);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#4F9525").s().p("ABmD0QAFg7AChoIAUhAIAAgIQAnBbAQBaQg1Cvh+CvQBQh0ARi0gAiXi6QACingUhoIgIhSQAvByAoBWQgLErgiDJQgCBpguA0QAckWAEjig");
	this.shape_8.setTransform(14,-180.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#5AA42D").s().p("AhBMAQgbAAgbgKQgfgJgagTQiHhchSgQQg0gOhXADQhgAHgtgBQgtgBgsgJQgJgcgEgfQg7kIg3i2QAEgNAJgJIATgSICIg5QBFgiAYiCQALgoAFgkQA2kqgNjsQAUBpgCCnQgEDjgcEVQAugzADhpQAijKAKkrIBPDEQBIDfgEC+IA2i9QgDBpgFA6QgRCzhPB1QB9ivA2iuQAdCLgZCRQB7g4AniQQAAAJAEAOIAAAKQgBAHAFAKIgCBfQA6ASAygoQAzgnABg7QA2AOA2AhQAgg+Aig/QCmgcC2AIQB+gCB+AUQiQArhuAqQhlAnhEAnQkpCrgyEhQgeCigqAoQgWASgYAJQgWAJgZAAIgUgCg");
	this.shape_9.setTransform(54.2,-149);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#5AA42D","#418515","#E5CD87"],[0,0.722,1],1.3,-143.7,-8.6,85.9).s().p("AnJHsQh5guhdiOIgegpQgbgqgbgkQjllAkmj1QBNgaBNATQBaAiBLBbQAkAqBXCFQCcDSDCAsQArAJAuABQAtABBggHQBXgDA0AOQBSAQCGBcQAaATAfAJQAaAKAbAAQAlAFAggMQAXgJAXgSQApgoAfiiQAxkgEqirQBEgnBlgoQBtgqCRgrQAKAAAIAFIAXAAQAlAFAjAJQgzAcguAeQgDAAgFAEQiSBUh4BhQiyCEiBCYQiVC2g1B+QgfBDhGBMQkIBCjNAAQigAAh+gog");
	this.shape_10.setTransform(26.6,-105);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#39782E").s().p("AFbF1IhPjEQgnhWgwhxIAIBQQANDsg2EqQisjzi7iiIgGhSQgZjKhBi7QgwiEhAh8QA6AUA2AcQBVAlBnBGICXBrIAbASQB9BHA3ABQgSgpgfgvQg9hNhxhYQi2iLlBjNQC4BHBrA4QDfB7B8CWIAkApQAWAhAaAkIAoBGQA9BxAvCRQArCJAtD1QADAyAOAxIgUA/Ig1C9QAEi+hIjfg");
	this.shape_11.setTransform(-21.9,-231.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3E713F").s().p("AnNNXQgRhagmhbIAAAIQgOgwgEgzQgsj1gsiIQgviSg9hxIgohFQgaglgVggQAriUAQiaQAMAqAXAxQAdBXAjBtQgVl8hMlHQgchxgfhuQBQBqA9B1QBxDhApERQARBMAHBRQA8g0BSgxQBgg0DAhPIB6gvQhFB5g5CmQgUA3gPA1QBChrCAhlQD4jWFiiFQAFgFAFAAIAAAAQA7gDA6AFQhAAag/AeQl/C9kaFbQgKAFgEAIQiTC8hfDJQBqg6BRgiQgKBRADBoIAAATQAHBaAiB8QAEARAHASQAGgtATgsQCAkuD8i0QAhgWAfgSQBDgNA/ABQhpCChdCGQjMEfiaE0QgiA/gfA/Qg2ghg2gPQgBA8gzAnQgyAog7gTIACheQgFgKABgJIAAgJQgEgPAAgIQgnCRh7A4QAZiRgdiNg");
	this.shape_12.setTransform(78.7,-246.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#315D32").s().p("AHENoQgih7gIhaIAAgTQgDhoALhRQAggSAggJQAkgRAhgMQCohGCWgZQAggIAgAAQggASghAXQj7CziAEuQgTAsgGAtQgIgRgEgSgAnlCQIgbgSIiYhrQhmhFhWgkQg1gcg6gVQhTiah0iPQBEAYA+AcQFCDNC3CLQBxBXA9BOQAfAuARApQg2AAh+hIgAGHACQA4ilBHh5QC/hPB2ghQCsgvCagHIAKAAQgFABgFAFQljCFj3DVQiBBkhCBsQAPg2AUg2gAkGiVQgWgxgMgqIAAgcQARjKghi+QgViAgrh2QAJANAOAJQAeBvAdBxQBMFHAVF6QgjhrgehXg");
	this.shape_13.setTransform(32.5,-272.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CB9A0E").s().p("AhTIEIgNgKIgugnIgXAMIgWgFIgCgBIAigRIgugqIg4AeIgPgMIA7geIgsgqIglAUIgQgLIApgWIgqgsIgnAWIgIgOIAkgUIgFgGIgagdIgKgLIgCgDIgFgGIABgEIABgHIACgBIADgCIAlgUIASgKIgSgWIgNgRIgGgHIgNAHIgSALIACAMIgDgPQgCgGAAgFQgCgEABgEIABAFIAYgOIgfgoIAAgeIA6glIgjgyIgSANIABgUIAJgGIgHgKIACgWQALAMAHALIAyglIgjg2QgKAIgRAKIAHgxQgBAMAAAQIANgJIgMgTIAFgYIATAhIAwgoIgMgTIgUghIAAgCQgCgCgHgIIAAAAIAKgTIAHgMIAPgbIACgCIAOgMIgFgJIABgCQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAABAAQAAgBAAAAQABAAAAAAQABAAAAAAQABAAAAAAIADACIADgGIAEAHIAvgtIgOgbIAMgNIAPAbIApgqIgJgQIAOgKIAHAMIAAABIAeghIAEgEIAHgIIABgCIANgFIAOgHIgBABIgJALIAdAzIAPAXIAUgXIAUgZIgUggIgBgBQAFgFAGgFIACAAIABAAIASAdIAXgfIAAAAIACABIAGAEIACABIgBABIgbAjIASAcIAYAjIAdgjIAJgNIADgDIgDgDIgdgrIAHgEIAGgFIAAABIAIALIAAAAIAAAAIAGAIIACAEIANASIAOgUIAIAIIgQAWIAuA9IAkgwIAIAIIglAxIAvA7QAYgeAWgeIAIAIIguA9IAxA3IAqg1IAGALIgoAzIAfAhIAXgfIADAOIgSAZIA0A0IAHgJIADAIIABAFIgCAEIgIAKIgtA2IA2AyIAJAIIgIAJIgwA0IA2AuIAKAIIgJAJQgZAagaAZIA1AsIABAAIALAJIgIAIIgKAJIgrAqIAoAfIAAABIgCAMIgvgkIghAeIA9ArIgGAIIg/gsIgKAJIgwAoQAjAbAlAZIgCACIgBAJQgogbgmgdIg9AvQAqAhAsAeIgJAHQgsgfgqggQgfAXggAVIA8AuIgDABIgJAEIg6gsIg7AmIAqAhQgIAAgJgBIgDgBIgggZIgrAaIgNAHIgLAGIgNgLgAiDHNIA6AxIA5ggIg6gxIg5AggAg/GnQAcAZAeAXIA7gmIg4gwIg9AmgAi9GYIAtApIA5gfIgugqIg4AggAAHF7IA5AwIA+gsIg5guIg+AqgAh6FxIAuArIA9glIgvgrIg8AlgAj1FhIArArIA4gfIgsgsIg3AggABPFKIA5AuIA9guIg5gtIg9AtgAgzFFIAvArIA9gpIgxgrIg7ApgAizE6IAtAsIA9glIgugtIg8AmgAkqEoIApAtIA3ggIgqgtIg2AggACVEWIA5AtIA7gxIg6grIg6AvgAATEUIAwAsIA9gsIgzgsIg6AsgAhsEMIAuAtIA8goIgvgtIg7AogAjpEBIArAtIA8glIgrgvIg8AngAlGDdIgaAOIASAWIAIAJIARASIA2ggIgggkIgKgMIgdARgABXDgIAzAsIA6gvIg0grIg5AugADkDVIgMAKIA5AsIAhgdIg3gtIgXAUgAgnDcIAuAuIA7gsIgxguIg4AsgAiiDSIArAvIA7goIgugvIg4AogAkdDFIARAUIAZAcIA7gmIgYgdIgQgSIg9AlgAEEC5IA3AtIA1g0Ig3grIg1AygAAbCoIAxAuIA5guIgzgtIg3AtgACdCnIgEADIA0AsIAYgUIALgKIgzguIggAdgAhfCiIAtAvIA5grIgugvIg4ArgAjUCYIAXAbIARAUIA4goIgngsIg5AlgAlMCLIAlAuIA9glIgmgvIg8AmgADHCCIAzAuIA1gyIg1gtIgzAxgABcBzIAyAuIAPgNIAWgTIgygwIglAigAANBLIgqAkIAuAvIA3gtIgxgvIgKAJgAiQBrIABAAIAmAsIA4gqIgngqIg4AogAkEBdIAmAvIA4glIgogwIg2AmgAl6BNIAkAwIA8glIglgxIg7AmgACLBIIAyAwIAzgwIgzgwIgyAwgAEDBIIA1AuIAzgzIg2guIgyAzgAAhA5IAwAwIAFgDIAigfIgwgxIgnAjgAhNA6IAmArIA1gtIglgpIg2ArgAjCAvIAoAwIA3goIgRgUIgYgdIg2ApgAkyAeIAkAyIA3glIgngxIg0AkgADHAOIAzAxIAygyIg1gwIgwAxgABSAMIAwAyIAygvIgxgyIgxAvgAgNAHIAlAoIAngjIgBgBIgjgmIgoAigAiBgBIAgAlIAKALIA2gqIglgpIgHgIIg0ArgAjygPIAnAxIA1gmIgogzIg0AogAleghIAjAyIA0gjIglg0IgyAlgACMgsIAxAxIAxgxIgygyIgwAygAEAgsIA0AwIAwg0Ig1gxIgvA1gAA8g5IgWAUIAjAoIAxgvIgQgSIgTgUIgbAZgAhCg1IAPASIAcAgIApgjIgqg0IgqAlgAizhAIAoAzIA1gqIgqg0IgzArgAkfhQIAkA1IA0goIgng1IgxAogAgMhlIgBABIAqA0IAfgcIASgRIgtg0IgtAsgADFhoIAyAzIAvg0Ig0gzIgtA0gABhhcIAcAeIAHAIIAvgxIgdggIgFgEIgwAvgAhViRIggAdIApA0IArglIgpg2IgLAKgAjiiBIAnA1IAygqIgog2IgxArgAlLiTIAjA3IAygnIglg3IgwAngAAsibIAsA1IAwgvIgug1IguAvgAg/ilIApA2IAKgJIAlgjIgqg3IguAtgACbiVIAOAOIATAWIAug0IghgiIguAygAD7inIA0A0IAsg2Ig1gzIgrA1gAiei9IgIAHIAoA3IApglIACgCIgng4IgkAhgAjmjpIgpAkIAlA4IAwgqIgmg4IgGAGgABljVIAtA2IAvgyIgwg1IgsAxgAgGjdIAqA3IAuguIgsg4IgsAvgADTjSIACACIAeAhIArg1IgfghIgsAzgAhvjqIAoA6IAugtIgpg5IgtAsgAjVj5IAnA4IAQgPIAcgaIgmg6IgtArgAkXlBIgOANIgUARIgFAPIAAABIACAEIAXAmIAOAYIAvgqQgXgjgWglIgCACgACbkRIAvA2IAtgzIgJgKIgogtIgrA0gAAwkXIAsA4IAtgxIgug4IgrAxgAg3kiIApA6IAsguIgqg6IgrAugAickxIAmA8IAsgsIgng8IgrAsgAkJlOQAVAlAYAkIAtgsQgWgkgVgmIgvAtgABllUIAuA5IArgzIgrg0IgEgFIgqAzgAgBlcIApA6IArgwIgsg8IgoAygAhmlpIAnA8IArguIgog9IgqAvgAjOmHQAVAmAWAkIArgsQgXgkgVglIgqArgAAxmaIAsA8IAqgzIgtg8IgpAzgAgVnHIgcAiIAoA+IApgxIgggvIgJgPIgMAPgAiRnHIgHAIQAUAmAXAkIAqguIgXgkIgWglIghAlg");
	this.shape_14.setTransform(15.3,157.5,4.523,4.523,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3D24B").s().p("AmHSHQhMgtg4iXQhikWg7kQQhHlUA2jqQCsgqCLicQB9iKA5izQAOgvAKgsQAwjGALgWQArh/BVgrQBOgxBsAeQBaATBdBGQCrB4BbDFQBbC/gIDVQgHCMg5C0QgFAShtEeQglBjhRC8QgOApgTAkQg0Bwg4BRQhYCKh3BeQiABkiNAeQgtALgoAAQhAAAgygdgAhEtxIgBgGIgEgXQgEgNgJgFQAHAYALAXg");
	this.shape_15.setTransform(-47.4,132);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E9BC06").s().p("EgJvAj6Ig1gtQgSgFgRgKQgpgOhbgmQhQglgogOQgJgFgNgFQgqgOg+gPIgJAAQhNgXgogPQhjgmhag8Ig+g4QgEAAAAgFQgtglgfguQH1AyHOgTQQVgkF9ocQD6lTgMpoQgOq5lBsvQhfjxiYlIQh7kAiikyIAlgRQAEgFAJABQAJAAAFAFIBqBfQAOgCAWADIAEAAQAlAPAfAPQAkASAgASIAFAAIAAAFIANAJIAEAAQAoAcAkAlQARAOASAXQAFAAAAAFQAfAgAaAgQBIAdBIBJQAVAXAXAcIAjAlQAaAcAYAbQA0BAAZB2QAlgIASAbIAAAFQAJAJAIATQANAWANAcQAeBNAQBoQAJAgAEAbQAEAlAIAaIANBSQAOgJAJgDQAIgKAKAFQANAAAFAKIAJAJQARAbASAgQARAkAVA2QAFAPAAAJIAVA2IAOAlQgBANAEAKIAJAkQgOgKgIgIQgTAVgSAXIA+A4QgGA2gTBRQgTBRgGAyQAeA6AWA3IggAgQAWATASASIgkAsQAoAhAoAkQAMBJgCBIIAAAIIgLCjQgcAfghAfIAtAlQgSATgTAVQANAJANAPIAAADQAkBBgLBjQgLBRgmCeIAFAAIAwAqQgSAMgTAXQgWASgXAWQgPBEgOAoIgaBfQAKAEAEAJQAEAKAFAIIAAAFQANApgFAxIgBAFQAAAbgFAXQgKAggOAfQgXAtggAxQgFAIgGAFQgNAOgNAXIghAnQh+CPhEBHQgEAFgFADQgBASgEAXIAEAFQAEAFAFAJQAEAFgEANQAAAFgKAIQgJAFgKAIIgDAAQguAphOA9QgzAogyAkQgYAMgRAOQhlA9hSAVQgkAJhXAMQg/AIglAJQgIAEgFAAQgTAEgWANQgkAMg0AcQhVArguANQg/AQg7AEQgpAAgogGQgEgEgFAAQg1gKgqgTQhQAUhXAEQgtAEgogBQgdANgbARIgyAbQgggcgfgYg");
	this.shape_16.setTransform(34.8,160.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFCC00").s().p("EgUvAg9QgfgogagqQgEgJAAgOQAEgJAKgEQAKgJAIAFQAkAFAoAKIAKABQAAgFgEgFQglgOgggXQgjgTgkgcQhLg8g+hXQgXgbgRgcQgWgggOggQgdg7gOg8QgIgWgEgXQgMgugEgtQAAgIAEgFIAAgFQAKgIAJgFQAJAAAJAGIARAIIABgIQgEgOAAgKIgJgJIgXggIAAgNIAKggQAAgEAFAAIASgJQgNgOgJgOIAAgJQgDgNAAgKQgIgogFgpQAAgFgEgEIgIg6IgIhGQgNgbAAgWQgEgWAFgOQgFgJAAgKQAAgJgEgJQgDgogEgfQgIghgFgcQgEgbAKgfIADiDIAAgSQAGh+ASjDQAEgtAGgyIAKhDIALhnQAFgyAHg1IBLnAIAAgEIAJgkQAKgkATgpIAAgEIAJgSQAKgbASgkQAUg6Acg6IAEgFIAvhRQAPgWASgjIBChxQAAgJAEgEQAGgOAJgNQAOgjARghQAFgEABgFQAFgIAJgEQAJgFAJAFQAEAAAJAEIAJgaQAqhVAdg3QAuhVA9g+QAXgkAggVQAJgOAKgJIAEAAQAFgJAIABQAKAAAIAEQAKAFAEAEIAAAEQBFhGBOg5QAbgXAmgWIANgJQAJgEAFgEQAagSAdgNQAAgEAEAAQAugbAugWIAJAAIAbgjQAFgFAFgDQARgKAlgNIBAgZQAEAAAFAEIBMA4IAuhIQAFgJAFgEIASAAQAEAAAJAFIA+A3IAFAAIAJgJQAAgFgEAAQAWgZAcgTIAJAAQAFgEAFAAQAJgJAOgJIAIgJQAFgEAJgDQAJAAAIADQAJAKAAAEIAWA3IAlg1QAAgJANgFIAOAAIAFAAQAIAFAFAJIAIAOIAAgEQAFAEAAAFQAJAJAXAKQAEAAAEAFQgBAAgBAAQgBAAAAABQgBAAAAAAQAAABAAABQAJAFANAFIASABQASAEASgEIAFAAIAKAAQAIgFAFAAIAFAAQANgEASgNQAPgNANgJQAFAAADAFQAPAFAAAIIANAkQAMAUAJASIAXgOQCiEyB7EAQCYFIBfDxQFBMvAOK5QAMJoj6FTQl9IcwVAkQiCAFiEAAQlVAAlogkgApTx/QhWArgrB+QgLAWgwDGQgKAtgOAuQg5C0h9CKQiLCcisAqQg2DpBHFVQA7EQBiEVQA4CXBMAuQBRAvB2gdQCNgeCBhlQB3heBYiJQA4hRA0hxQANAKARAJQgHgJgOgFQgEgFgFAAQATgkAOgoQBRi8AlhjQBskeAFgTQA5izAHiNQAIjVhbi/QhajEirh5QhdhFhagUQgpgLglAAQg8AAgwAfgALN2uQAFAEADAFQgIgMABgMIgBAPgAbLJSQAEAAAAgDIAAAIQAAgFgEAAgAawBBQAFgFAIgFIAKAhQgKgPgNgIg");
	this.shape_17.setTransform(12.9,131.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF9900").s().p("AwJi3ILeGWIjTmQIJRECIjwl5IKpFsIkQnZIMOHhI6ZFJg");
	this.shape_18.setTransform(-424.9,197.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC6600").s().p("AsPizIgCgFIaZlKIAKAHIooLnIBLoXInyJPIBWm2InKHKIAvnBIoPKMg");
	this.shape_19.setTransform(-411.9,256.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_20.setTransform(-173.8,179.1,1.724,1.724,-31.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_8
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#660000").ss(10,1,1).p("EhLvg+eMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAMiXeAAAQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAg");
	this.shape_21.setTransform(-12.8,15.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CCCCCC").s().p("EhLvA+fQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAgEhVAg4SQj2D2AAFbMAAABeCQAAFcD2D2QD2D2FbAAMCXeAAAQFcAAD2j2QD2j2AAlcMAAAheCQAAlbj2j2Qj2j2lcAAMiXeAAAIAAAAQlbAAj2D2g");
	this.shape_22.setTransform(-12.8,15.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("EhLvA8JQlbAAj2j2Qj2j2AAlcMAAAheCQAAlbD2j2QD2j2FbAAMCXeAAAQFcAAD2D2QD2D2AAFbMAAABeCQAAFcj2D2Qj2D2lcAAg");
	this.shape_23.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21}]}).wait(1));

	// Layer_11
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(255,255,255,0.02)").s().p("EhNQBCIQngAAlVlVQlVlVAAnhMAAAhf6QAAngFVlVQFVlVHgAAMCagAAAQHiAAFUFVQFUFVAAHgMAAABf6QAAHhlUFVQlUFVniAAg");
	this.shape_24.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_24).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy5, new cjs.Rectangle(-623.5,-407.7,1221.3,846.4), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMB4IgFgDQgDgCgBgEQgCgDAAgDQAAgDACgDQABgEADgCQABgCAEgBQADgCADABIAGABQACABADACIADAGIABAGIgBAGIgDAGIgFADIgGABIgGgBgAgWBFIgCgMQAAgJAEgHQADgHAEgGIALgLIALgKIANgJQAGgGAFgGQAEgGAEgHQADgIAAgJQgBgKgDgJQgDgJgGgIQgGgHgIgDQgJgFgJAAQgKAAgIAEQgIADgFAIQgGAGgDAJQgCAIAAAKIAAAGIABAIIgaAFIgCgKIgBgKQAAgPAGgNQAFgNAJgJQAJgLAOgFQANgGAPAAQAPAAANAFQAOAGAJAKQAJAKAGANQAFANABARIgBAOIgFAPQgDAIgFAHQgFAGgJAHIgOALQgIAFgGAHQgHAGgEAIQgDAJABAMg");
	this.shape.setTransform(237.7,-16.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_1.setTransform(219.7,-15.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgfB6IgOgFIgNgIQgHgFgGgGQgFgHgDgJIAcgMQADAGAEAFQADAEAFADIAJAFIAKADQALACALgBQAKgCAIgEQAHgFAFgFQAFgFADgGIAGgLIACgKIABgGIAAgTQgJAIgJAEQgJAFgJACQgJACgIABQgRAAgPgGQgOgGgLgLQgLgLgGgPQgHgQAAgUQAAgUAHgPQAHgQALgLQAMgKAOgGQAOgGAOAAIATAFIATAIQAKAFAIAJIAAgdIAeAAIgCCoQAAAJgCAKQgDAJgEAJQgEAJgIAIQgGAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgOgEgAgVhZQgJAFgHAHQgGAIgEAKQgDALAAAMQAAAMADAKQAEAKAGAGQAHAHAJAEQAJAFALAAQAJAAAIgEQAKgDAHgGQAHgFAFgJQAGgIABgKIAAgTQgBgLgGgIQgFgJgHgGQgIgGgJgEQgKgDgIAAQgKAAgJAEg");
	this.shape_2.setTransform(199.3,-9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPBRQgNAAgKgFQgKgEgGgIQgGgHgEgKQgEgKgCgLQgCgLAAgLIgBgWQAAgPABgQIADggIAfABIgEAhIAAAZIAAANIABAPIADARQACAIAEAHQAEAHAFADQAGAEAJgCQAKgBAMgOQAMgOANgcIAAglIgBgVIAAgMIAggEIABAuIABAjIABAdIAAAVIABAfIghAAIgBgqIgLARQgFAHgIAGQgHAGgIADQgGAEgIAAIgCgBg");
	this.shape_3.setTransform(180.4,-12.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgeBQQgOgHgLgKQgKgMgHgQQgFgPgBgUQABgSAFgQQAHgQAKgLQALgMAOgGQAOgGAQAAQAQAAAPAHQAOAGAKAMQALALAGAQQAGAQABARQgBATgGAPQgGAQgLAMQgKALgOAHQgPAGgQAAQgQAAgOgGgAgUg2QgJAGgHAIQgFAJgDAKQgCALAAAKQAAAKACALQADAKAGAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJACgKQADgLAAgKQAAgKgCgLQgDgKgGgJQgGgIgIgGQgKgFgMgBQgMABgIAFg");
	this.shape_4.setTransform(162.2,-12.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_5.setTransform(145.8,-12.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_6.setTransform(127.2,-15.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygCIACg/IAcgCIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_7.setTransform(110.1,-15.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgNgHQgHgDgHgFIAPgXIAQAJIARAHQAJADAJABQAJACAKAAQAHAAAGgBQAFgCAEgCQAEgCACgDIADgGIAAgFIgBgGQgBgDgEgDIgHgFIgMgDIgPgCQgMgBgNgCQgMgCgKgFQgJgEgGgHQgHgHgBgLQgBgLADgIQABgJAGgHQAEgHAIgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIARAFQAJAEAIAFIgKAbQgKgFgJgDIgQgFIgPgCQgVgBgNAGQgNAGAAANQAAAIAFAEQAEAEAJACIATADIAXACQAPACAJAFQAKADAGAGQAGAGADAHQACAHAAAIQABAOgGAJQgGAJgJAGQgKAGgMADQgMADgNABQgNAAgNgDg");
	this.shape_8.setTransform(86.7,-12);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AggBPQgPgHgLgKQgJgLgGgPQgFgPgBgTQABgRAGgQQAGgQALgMQALgLAPgHQAOgHAQAAQAQABANAFQANAHALAKQAKALAHAOQAHANACARIh+ASQABAKADAJQAFAIAGAGQAGAGAJADQAIACAJAAQAIABAIgDQAHgDAGgEQAHgFAFgGQAEgIACgJIAdAHQgEANgIAKQgHALgKAIQgKAIgMAEQgMAFgNgBQgTAAgOgFgAgLg4QgHACgIAFQgGAFgGAJQgFAJgCAMIBXgLIgCgCQgFgPgJgIQgKgJgPAAQgFABgHACg");
	this.shape_9.setTransform(69.7,-12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgCgDgDgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgJgEgHgHQgGgHgBgLQgBgLADgIQACgJAEgHQAGgHAHgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgWgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQADAHgBAIQABAOgGAJQgGAJgJAGQgJAGgNADQgMADgNABQgNAAgNgDg");
	this.shape_10.setTransform(52.3,-12);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgCgDgDgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgKgEgGgHQgGgHgBgLQgBgLADgIQACgJAEgHQAGgHAHgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgWgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQADAHgBAIQABAOgGAJQgGAJgJAGQgJAGgNADQgMADgNABQgNAAgNgDg");
	this.shape_11.setTransform(35.6,-12);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_12.setTransform(17.6,-12.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhQh2IAhgBIgBAVQAJgIAJgFIAQgHQAJgCAIgBQAKAAAKADQAJADAJAFQAJAFAHAIQAIAHAFAJQAFAJADAKQADALAAALQgBANgDALQgDALgGAIQgFAKgIAHQgHAIgIAFQgJAFgIADQgJACgJAAQgIAAgJgCQgIgCgKgFQgKgEgJgIIgBBjIgbABgAgQhYQgIADgHAGQgGAFgEAHQgFAGgCAIIgBAcQADAJAEAIQAFAHAHAEQAGAFAHADQAIACAHAAQAKAAAJgDQAJgEAGgGQAHgHAEgKQAEgJABgLQAAgLgDgKQgDgKgHgHQgGgHgJgFQgJgEgLAAQgIAAgIADg");
	this.shape_13.setTransform(-0.5,-8.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAEgdIgiBlIgrAEIgdiXIAegCIAWB6IAsh6IAbACIAeB7IAUh8IAgABIgdCYIgsABg");
	this.shape_14.setTransform(-29.8,-12.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgeBQQgOgHgKgKQgMgMgFgQQgHgPAAgUQAAgSAHgQQAFgQAMgLQAKgMAOgGQAPgGAPAAQAQAAAOAHQAOAGALAMQALALAGAQQAHAQAAARQAAATgHAPQgGAQgLAMQgLALgOAHQgOAGgQAAQgPAAgPgGgAgVg2QgIAGgGAIQgGAJgCAKQgDALAAAKQAAAKADALQADAKAFAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJADgKQACgLAAgKQABgKgDgLQgDgKgFgJQgHgIgJgGQgIgFgNgBQgMABgJAFg");
	this.shape_15.setTransform(-50.1,-12.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_16.setTransform(-66.4,-12.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_17.setTransform(-82.8,-12.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_18.setTransform(-101.5,-12.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_19.setTransform(-128,-15.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_20.setTransform(-147,-12.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgSBuIABglIABguIABg+IAdgBIgBAlIAAAgIgBAaIAAATIAAAggAgHhFIgGgEQgDgDgCgEQgCgEAAgEQAAgEACgEQACgEADgDQACgDAEgBQADgCAEAAQAEAAAEACQAEABADADIAFAHIABAIQAAAEgBAEIgFAHIgHAEIgIACQgEAAgDgCg");
	this.shape_21.setTransform(-159.7,-15.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_22.setTransform(-173.4,-15.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AhXBwIgojUIAegEIAbCnIA3iaIAjAEIAwCfIAei3IAeAGIgnDYIgjAAIg1inIg6Cog");
	this.shape_23.setTransform(-197.2,-16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(6,1,1).p("EglAgEnMBKBAAAQCcAAAACcIAAEXQAACcicAAMhKBAAAQicAAAAicIAAkXQAAicCcAAg");
	this.shape_24.setTransform(16.3,-15.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF3333").s().p("EglAAEoQicAAAAicIAAkXQAAicCcAAMBKBAAAQCcAAAACcIAAEXQAACcicAAg");
	this.shape_25.setTransform(16.3,-15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-239.2,-47.8,511.1,65.3), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(146,31.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.97,scaleY:0.97,y:31.7},9).to({scaleX:1,scaleY:1,y:31.8},10).to({scaleX:0.97,scaleY:0.97,y:31.7},10).to({scaleX:1,scaleY:1,y:31.8},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,295.1,65.1);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy3();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.setscopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// validation
	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(14.1,16.3);
	this.instance.alpha = 0.699;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#793F06").s().p("An0AFISqnzI1rPdg");
	this.shape.setTransform(457.1,98.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("Al/C0Ik0lqIVnCeIwzDPg");
	this.shape_1.setTransform(457.1,50.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9E5104").s().p("AnmgpIQyjQIAQgFIgNAKIyqHzg");
	this.shape_2.setTransform(467.4,73.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FBAA30").s().p("AowBXIkXlLIaPHjIgCABIgQAFg");
	this.shape_3.setTransform(444,23.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("A7sDfMA2rgKQIAuDiMg1WAKBg");
	this.shape_4.setTransform(307.3,86.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF9900").s().p("AwJi3ILeGWIjTmQIJRECIjwl5IKpFsIkQnZIMOHhI6ZFJg");
	this.shape_5.setTransform(-424.9,197.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC6600").s().p("AsPizIgCgFIaZlKIAKAHIooLnIBLoXInyJPIBWm2InKHKIAvnBIoPKMg");
	this.shape_6.setTransform(-411.9,256.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_7.setTransform(-173.8,179.1,1.724,1.724,-31.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

	// question
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#793F06").s().p("An0AFISqnzI1rPdg");
	this.shape_8.setTransform(457.1,98.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("Al/C0Ik0lqIVnCeIwzDPg");
	this.shape_9.setTransform(457.1,50.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#9E5104").s().p("AnmgpIQyjQIAQgFIgNAKIyqHzg");
	this.shape_10.setTransform(467.4,73.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FBAA30").s().p("AowBXIkXlLIaPHjIgCABIgQAFg");
	this.shape_11.setTransform(444,23.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("A7sDfMA2rgKQIAuDiMg1WAKBg");
	this.shape_12.setTransform(307.3,86.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#4BA921").s().p("ACRB4QhYiEgjgqQhLhbhZgiQhOgThNAaQhPhBhWg8QgMgOgTgKQBFgVA/gIQCNgdBeAGQEGASDVCwQAKAXAIAbQA4C1A6EIQAEAfAJAcQjCgsibjTg");
	this.shape_13.setTransform(-71.5,-125.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#43870F").s().p("AE+HsQgUhEgbg8QiUlCkUjsQEIBkDFCBQAUhviBh0QgtghjIhVQloh8jOhPQBjgNBwAQQDfAbDHB5QB1BCBzBgQC8CiCsDyQgEAkgMAoQgXCChGAjIiIA5IgSASQgNgOgTgOg");
	this.shape_14.setTransform(-68.4,-193);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#39740C").s().p("AAiAsQgRgNgNgNQjji9kJhbIgKAAQAFgEAFAAIBAgHQDOBPFnB8QDKBUAsAhQCCB0gVBwQjEiCkKhlg");
	this.shape_15.setTransform(-87.3,-217);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#4F9525").s().p("ABmD0QAFg7AChoIAUhAIAAgIQAnBbAQBaQg1Cvh+CvQBQh0ARi0gAiXi6QACingUhoIgIhSQAvByAoBWQgLErgiDJQgCBpguA0QAckWAEjig");
	this.shape_16.setTransform(14,-180.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#5AA42D").s().p("AhBMAQgbAAgbgKQgfgJgagTQiHhchSgQQg0gOhXADQhgAHgtgBQgtgBgsgJQgJgcgEgfQg7kIg3i2QAEgNAJgJIATgSICIg5QBFgiAYiCQALgoAFgkQA2kqgNjsQAUBpgCCnQgEDjgcEVQAugzADhpQAijKAKkrIBPDEQBIDfgEC+IA2i9QgDBpgFA6QgRCzhPB1QB9ivA2iuQAdCLgZCRQB7g4AniQQAAAJAEAOIAAAKQgBAHAFAKIgCBfQA6ASAygoQAzgnABg7QA2AOA2AhQAgg+Aig/QCmgcC2AIQB+gCB+AUQiQArhuAqQhlAnhEAnQkpCrgyEhQgeCigqAoQgWASgYAJQgWAJgZAAIgUgCg");
	this.shape_17.setTransform(54.2,-149);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#5AA42D","#418515","#E5CD87"],[0,0.722,1],1.3,-143.7,-8.6,85.9).s().p("AnJHsQh5guhdiOIgegpQgbgqgbgkQjllAkmj1QBNgaBNATQBaAiBLBbQAkAqBXCFQCcDSDCAsQArAJAuABQAtABBggHQBXgDA0AOQBSAQCGBcQAaATAfAJQAaAKAbAAQAlAFAggMQAXgJAXgSQApgoAfiiQAxkgEqirQBEgnBlgoQBtgqCRgrQAKAAAIAFIAXAAQAlAFAjAJQgzAcguAeQgDAAgFAEQiSBUh4BhQiyCEiBCYQiVC2g1B+QgfBDhGBMQkIBCjNAAQigAAh+gog");
	this.shape_18.setTransform(26.6,-105);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#39782E").s().p("AFbF1IhPjEQgnhWgwhxIAIBQQANDsg2EqQisjzi7iiIgGhSQgZjKhBi7QgwiEhAh8QA6AUA2AcQBVAlBnBGICXBrIAbASQB9BHA3ABQgSgpgfgvQg9hNhxhYQi2iLlBjNQC4BHBrA4QDfB7B8CWIAkApQAWAhAaAkIAoBGQA9BxAvCRQArCJAtD1QADAyAOAxIgUA/Ig1C9QAEi+hIjfg");
	this.shape_19.setTransform(-21.9,-231.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3E713F").s().p("AnNNXQgRhagmhbIAAAIQgOgwgEgzQgsj1gsiIQgviSg9hxIgohFQgaglgVggQAriUAQiaQAMAqAXAxQAdBXAjBtQgVl8hMlHQgchxgfhuQBQBqA9B1QBxDhApERQARBMAHBRQA8g0BSgxQBgg0DAhPIB6gvQhFB5g5CmQgUA3gPA1QBChrCAhlQD4jWFiiFQAFgFAFAAIAAAAQA7gDA6AFQhAAag/AeQl/C9kaFbQgKAFgEAIQiTC8hfDJQBqg6BRgiQgKBRADBoIAAATQAHBaAiB8QAEARAHASQAGgtATgsQCAkuD8i0QAhgWAfgSQBDgNA/ABQhpCChdCGQjMEfiaE0QgiA/gfA/Qg2ghg2gPQgBA8gzAnQgyAog7gTIACheQgFgKABgJIAAgJQgEgPAAgIQgnCRh7A4QAZiRgdiNg");
	this.shape_20.setTransform(78.7,-246.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#315D32").s().p("AHENoQgih7gIhaIAAgTQgDhoALhRQAggSAggJQAkgRAhgMQCohGCWgZQAggIAgAAQggASghAXQj7CziAEuQgTAsgGAtQgIgRgEgSgAnlCQIgbgSIiYhrQhmhFhWgkQg1gcg6gVQhTiah0iPQBEAYA+AcQFCDNC3CLQBxBXA9BOQAfAuARApQg2AAh+hIgAGHACQA4ilBHh5QC/hPB2ghQCsgvCagHIAKAAQgFABgFAFQljCFj3DVQiBBkhCBsQAPg2AUg2gAkGiVQgWgxgMgqIAAgcQARjKghi+QgViAgrh2QAJANAOAJQAeBvAdBxQBMFHAVF6QgjhrgehXg");
	this.shape_21.setTransform(32.5,-272.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CB9A0E").s().p("AhTIEIgNgKIgugnIgXAMIgWgFIgCgBIAigRIgugqIg4AeIgPgMIA7geIgsgqIglAUIgQgLIApgWIgqgsIgnAWIgIgOIAkgUIgFgGIgagdIgKgLIgCgDIgFgGIABgEIABgHIACgBIADgCIAlgUIASgKIgSgWIgNgRIgGgHIgNAHIgSALIACAMIgDgPQgCgGAAgFQgCgEABgEIABAFIAYgOIgfgoIAAgeIA6glIgjgyIgSANIABgUIAJgGIgHgKIACgWQALAMAHALIAyglIgjg2QgKAIgRAKIAHgxQgBAMAAAQIANgJIgMgTIAFgYIATAhIAwgoIgMgTIgUghIAAgCQgCgCgHgIIAAAAIAKgTIAHgMIAPgbIACgCIAOgMIgFgJIABgCQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAABAAQAAgBAAAAQABAAAAAAQABAAAAAAQABAAAAAAIADACIADgGIAEAHIAvgtIgOgbIAMgNIAPAbIApgqIgJgQIAOgKIAHAMIAAABIAeghIAEgEIAHgIIABgCIANgFIAOgHIgBABIgJALIAdAzIAPAXIAUgXIAUgZIgUggIgBgBQAFgFAGgFIACAAIABAAIASAdIAXgfIAAAAIACABIAGAEIACABIgBABIgbAjIASAcIAYAjIAdgjIAJgNIADgDIgDgDIgdgrIAHgEIAGgFIAAABIAIALIAAAAIAAAAIAGAIIACAEIANASIAOgUIAIAIIgQAWIAuA9IAkgwIAIAIIglAxIAvA7QAYgeAWgeIAIAIIguA9IAxA3IAqg1IAGALIgoAzIAfAhIAXgfIADAOIgSAZIA0A0IAHgJIADAIIABAFIgCAEIgIAKIgtA2IA2AyIAJAIIgIAJIgwA0IA2AuIAKAIIgJAJQgZAagaAZIA1AsIABAAIALAJIgIAIIgKAJIgrAqIAoAfIAAABIgCAMIgvgkIghAeIA9ArIgGAIIg/gsIgKAJIgwAoQAjAbAlAZIgCACIgBAJQgogbgmgdIg9AvQAqAhAsAeIgJAHQgsgfgqggQgfAXggAVIA8AuIgDABIgJAEIg6gsIg7AmIAqAhQgIAAgJgBIgDgBIgggZIgrAaIgNAHIgLAGIgNgLgAiDHNIA6AxIA5ggIg6gxIg5AggAg/GnQAcAZAeAXIA7gmIg4gwIg9AmgAi9GYIAtApIA5gfIgugqIg4AggAAHF7IA5AwIA+gsIg5guIg+AqgAh6FxIAuArIA9glIgvgrIg8AlgAj1FhIArArIA4gfIgsgsIg3AggABPFKIA5AuIA9guIg5gtIg9AtgAgzFFIAvArIA9gpIgxgrIg7ApgAizE6IAtAsIA9glIgugtIg8AmgAkqEoIApAtIA3ggIgqgtIg2AggACVEWIA5AtIA7gxIg6grIg6AvgAATEUIAwAsIA9gsIgzgsIg6AsgAhsEMIAuAtIA8goIgvgtIg7AogAjpEBIArAtIA8glIgrgvIg8AngAlGDdIgaAOIASAWIAIAJIARASIA2ggIgggkIgKgMIgdARgABXDgIAzAsIA6gvIg0grIg5AugADkDVIgMAKIA5AsIAhgdIg3gtIgXAUgAgnDcIAuAuIA7gsIgxguIg4AsgAiiDSIArAvIA7goIgugvIg4AogAkdDFIARAUIAZAcIA7gmIgYgdIgQgSIg9AlgAEEC5IA3AtIA1g0Ig3grIg1AygAAbCoIAxAuIA5guIgzgtIg3AtgACdCnIgEADIA0AsIAYgUIALgKIgzguIggAdgAhfCiIAtAvIA5grIgugvIg4ArgAjUCYIAXAbIARAUIA4goIgngsIg5AlgAlMCLIAlAuIA9glIgmgvIg8AmgADHCCIAzAuIA1gyIg1gtIgzAxgABcBzIAyAuIAPgNIAWgTIgygwIglAigAANBLIgqAkIAuAvIA3gtIgxgvIgKAJgAiQBrIABAAIAmAsIA4gqIgngqIg4AogAkEBdIAmAvIA4glIgogwIg2AmgAl6BNIAkAwIA8glIglgxIg7AmgACLBIIAyAwIAzgwIgzgwIgyAwgAEDBIIA1AuIAzgzIg2guIgyAzgAAhA5IAwAwIAFgDIAigfIgwgxIgnAjgAhNA6IAmArIA1gtIglgpIg2ArgAjCAvIAoAwIA3goIgRgUIgYgdIg2ApgAkyAeIAkAyIA3glIgngxIg0AkgADHAOIAzAxIAygyIg1gwIgwAxgABSAMIAwAyIAygvIgxgyIgxAvgAgNAHIAlAoIAngjIgBgBIgjgmIgoAigAiBgBIAgAlIAKALIA2gqIglgpIgHgIIg0ArgAjygPIAnAxIA1gmIgogzIg0AogAleghIAjAyIA0gjIglg0IgyAlgACMgsIAxAxIAxgxIgygyIgwAygAEAgsIA0AwIAwg0Ig1gxIgvA1gAA8g5IgWAUIAjAoIAxgvIgQgSIgTgUIgbAZgAhCg1IAPASIAcAgIApgjIgqg0IgqAlgAizhAIAoAzIA1gqIgqg0IgzArgAkfhQIAkA1IA0goIgng1IgxAogAgMhlIgBABIAqA0IAfgcIASgRIgtg0IgtAsgADFhoIAyAzIAvg0Ig0gzIgtA0gABhhcIAcAeIAHAIIAvgxIgdggIgFgEIgwAvgAhViRIggAdIApA0IArglIgpg2IgLAKgAjiiBIAnA1IAygqIgog2IgxArgAlLiTIAjA3IAygnIglg3IgwAngAAsibIAsA1IAwgvIgug1IguAvgAg/ilIApA2IAKgJIAlgjIgqg3IguAtgACbiVIAOAOIATAWIAug0IghgiIguAygAD7inIA0A0IAsg2Ig1gzIgrA1gAiei9IgIAHIAoA3IApglIACgCIgng4IgkAhgAjmjpIgpAkIAlA4IAwgqIgmg4IgGAGgABljVIAtA2IAvgyIgwg1IgsAxgAgGjdIAqA3IAuguIgsg4IgsAvgADTjSIACACIAeAhIArg1IgfghIgsAzgAhvjqIAoA6IAugtIgpg5IgtAsgAjVj5IAnA4IAQgPIAcgaIgmg6IgtArgAkXlBIgOANIgUARIgFAPIAAABIACAEIAXAmIAOAYIAvgqQgXgjgWglIgCACgACbkRIAvA2IAtgzIgJgKIgogtIgrA0gAAwkXIAsA4IAtgxIgug4IgrAxgAg3kiIApA6IAsguIgqg6IgrAugAickxIAmA8IAsgsIgng8IgrAsgAkJlOQAVAlAYAkIAtgsQgWgkgVgmIgvAtgABllUIAuA5IArgzIgrg0IgEgFIgqAzgAgBlcIApA6IArgwIgsg8IgoAygAhmlpIAnA8IArguIgog9IgqAvgAjOmHQAVAmAWAkIArgsQgXgkgVglIgqArgAAxmaIAsA8IAqgzIgtg8IgpAzgAgVnHIgcAiIAoA+IApgxIgggvIgJgPIgMAPgAiRnHIgHAIQAUAmAXAkIAqguIgXgkIgWglIghAlg");
	this.shape_22.setTransform(15.3,157.5,4.523,4.523,1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#F3D24B").s().p("AmHSHQhMgtg4iXQhikWg7kQQhHlUA2jqQCsgqCLicQB9iKA5izQAOgvAKgsQAwjGALgWQArh/BVgrQBOgxBsAeQBaATBdBGQCrB4BbDFQBbC/gIDVQgHCMg5C0QgFAShtEeQglBjhRC8QgOApgTAkQg0Bwg4BRQhYCKh3BeQiABkiNAeQgtALgoAAQhAAAgygdgAhEtxIgBgGIgEgXQgEgNgJgFQAHAYALAXg");
	this.shape_23.setTransform(-47.4,132);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E9BC06").s().p("EgJvAj6Ig1gtQgSgFgRgKQgpgOhbgmQhQglgogOQgJgFgNgFQgqgOg+gPIgJAAQhNgXgogPQhjgmhag8Ig+g4QgEAAAAgFQgtglgfguQH1AyHOgTQQVgkF9ocQD6lTgMpoQgOq5lBsvQhfjxiYlIQh7kAiikyIAlgRQAEgFAJABQAJAAAFAFIBqBfQAOgCAWADIAEAAQAlAPAfAPQAkASAgASIAFAAIAAAFIANAJIAEAAQAoAcAkAlQARAOASAXQAFAAAAAFQAfAgAaAgQBIAdBIBJQAVAXAXAcIAjAlQAaAcAYAbQA0BAAZB2QAlgIASAbIAAAFQAJAJAIATQANAWANAcQAeBNAQBoQAJAgAEAbQAEAlAIAaIANBSQAOgJAJgDQAIgKAKAFQANAAAFAKIAJAJQARAbASAgQARAkAVA2QAFAPAAAJIAVA2IAOAlQgBANAEAKIAJAkQgOgKgIgIQgTAVgSAXIA+A4QgGA2gTBRQgTBRgGAyQAeA6AWA3IggAgQAWATASASIgkAsQAoAhAoAkQAMBJgCBIIAAAIIgLCjQgcAfghAfIAtAlQgSATgTAVQANAJANAPIAAADQAkBBgLBjQgLBRgmCeIAFAAIAwAqQgSAMgTAXQgWASgXAWQgPBEgOAoIgaBfQAKAEAEAJQAEAKAFAIIAAAFQANApgFAxIgBAFQAAAbgFAXQgKAggOAfQgXAtggAxQgFAIgGAFQgNAOgNAXIghAnQh+CPhEBHQgEAFgFADQgBASgEAXIAEAFQAEAFAFAJQAEAFgEANQAAAFgKAIQgJAFgKAIIgDAAQguAphOA9QgzAogyAkQgYAMgRAOQhlA9hSAVQgkAJhXAMQg/AIglAJQgIAEgFAAQgTAEgWANQgkAMg0AcQhVArguANQg/AQg7AEQgpAAgogGQgEgEgFAAQg1gKgqgTQhQAUhXAEQgtAEgogBQgdANgbARIgyAbQgggcgfgYg");
	this.shape_24.setTransform(34.8,160.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFCC00").s().p("EgUvAg9QgfgogagqQgEgJAAgOQAEgJAKgEQAKgJAIAFQAkAFAoAKIAKABQAAgFgEgFQglgOgggXQgjgTgkgcQhLg8g+hXQgXgbgRgcQgWgggOggQgdg7gOg8QgIgWgEgXQgMgugEgtQAAgIAEgFIAAgFQAKgIAJgFQAJAAAJAGIARAIIABgIQgEgOAAgKIgJgJIgXggIAAgNIAKggQAAgEAFAAIASgJQgNgOgJgOIAAgJQgDgNAAgKQgIgogFgpQAAgFgEgEIgIg6IgIhGQgNgbAAgWQgEgWAFgOQgFgJAAgKQAAgJgEgJQgDgogEgfQgIghgFgcQgEgbAKgfIADiDIAAgSQAGh+ASjDQAEgtAGgyIAKhDIALhnQAFgyAHg1IBLnAIAAgEIAJgkQAKgkATgpIAAgEIAJgSQAKgbASgkQAUg6Acg6IAEgFIAvhRQAPgWASgjIBChxQAAgJAEgEQAGgOAJgNQAOgjARghQAFgEABgFQAFgIAJgEQAJgFAJAFQAEAAAJAEIAJgaQAqhVAdg3QAuhVA9g+QAXgkAggVQAJgOAKgJIAEAAQAFgJAIABQAKAAAIAEQAKAFAEAEIAAAEQBFhGBOg5QAbgXAmgWIANgJQAJgEAFgEQAagSAdgNQAAgEAEAAQAugbAugWIAJAAIAbgjQAFgFAFgDQARgKAlgNIBAgZQAEAAAFAEIBMA4IAuhIQAFgJAFgEIASAAQAEAAAJAFIA+A3IAFAAIAJgJQAAgFgEAAQAWgZAcgTIAJAAQAFgEAFAAQAJgJAOgJIAIgJQAFgEAJgDQAJAAAIADQAJAKAAAEIAWA3IAlg1QAAgJANgFIAOAAIAFAAQAIAFAFAJIAIAOIAAgEQAFAEAAAFQAJAJAXAKQAEAAAEAFQgBAAgBAAQgBAAAAABQgBAAAAAAQAAABAAABQAJAFANAFIASABQASAEASgEIAFAAIAKAAQAIgFAFAAIAFAAQANgEASgNQAPgNANgJQAFAAADAFQAPAFAAAIIANAkQAMAUAJASIAXgOQCiEyB7EAQCYFIBfDxQFBMvAOK5QAMJoj6FTQl9IcwVAkQiCAFiEAAQlVAAlogkgApTx/QhWArgrB+QgLAWgwDGQgKAtgOAuQg5C0h9CKQiLCcisAqQg2DpBHFVQA7EQBiEVQA4CXBMAuQBRAvB2gdQCNgeCBhlQB3heBYiJQA4hRA0hxQANAKARAJQgHgJgOgFQgEgFgFAAQATgkAOgoQBRi8AlhjQBskeAFgTQA5izAHiNQAIjVhbi/QhajEirh5QhdhFhagUQgpgLglAAQg8AAgwAfgALN2uQAFAEADAFQgIgMABgMIgBAPgAbLJSQAEAAAAgDIAAAIQAAgFgEAAgAawBBQAFgFAIgFIAKAhQgKgPgNgIg");
	this.shape_25.setTransform(12.9,131.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF9900").s().p("AwJi3ILeGWIjTmQIJRECIjwl5IKpFsIkQnZIMOHhI6ZFJg");
	this.shape_26.setTransform(-424.9,197.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#CC6600").s().p("AsPizIgCgFIaZlKIAKAHIooLnIBLoXInyJPIBWm2InKHKIAvnBIoPKMg");
	this.shape_27.setTransform(-411.9,256.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_28.setTransform(-173.8,179.1,1.724,1.724,-31.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8}]}).wait(1));

	// Layer_8
	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#660000").ss(10,1,1).p("EhLvg+eMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAMiXeAAAQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAg");
	this.shape_29.setTransform(-12.8,15.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#CCCCCC").s().p("EhLvA+fQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAgEhVAg4SQj2D2AAFbMAAABeCQAAFcD2D2QD2D2FbAAMCXeAAAQFcAAD2j2QD2j2AAlcMAAAheCQAAlbj2j2Qj2j2lcAAMiXeAAAIAAAAQlbAAj2D2g");
	this.shape_30.setTransform(-12.8,15.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("EhLvA8JQlbAAj2j2Qj2j2AAlcMAAAheCQAAlbD2j2QD2j2FbAAMCXeAAAQFcAAD2D2QD2D2AAFbMAAABeCQAAFcj2D2Qj2D2lcAAg");
	this.shape_31.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29}]}).wait(1));

	// Layer_11
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("rgba(255,255,255,0.02)").s().p("EhNQBCIQngAAlVlVQlVlVAAnhMAAAhf6QAAngFVlVQFVlVHgAAMCagAAAQHiAAFUFVQFUFVAAHgMAAABf6QAAHhlUFVQlUFVniAAg");
	this.shape_32.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_32).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy6, new cjs.Rectangle(-623.5,-407.7,1221.3,846.4), null);


(lib.setscopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.question = new lib.sets2();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(499.6,56.7,0.763,0.763,0,0,0,2.9,0.4);

	this.question_1 = new lib.setscopy5();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(-513.2,56.7,0.763,0.763,0,0,0,2.8,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.question_1},{t:this.question}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy, new cjs.Rectangle(-991.1,-254.7,1944.6,645.8), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(328.4,440.9,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(205).to({_off:false},0).wait(59).to({startPosition:19},0).to({alpha:0,startPosition:3},4).wait(1));

	// hand
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(322.2,623.5,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(170).to({_off:false},0).to({_off:true},31).wait(68));

	// arrow
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(351.1,190.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(139).to({_off:false},0).to({_off:true},31).wait(99));

	// Layer_4
	this.question = new lib.setscopy6();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(341.2,426,0.462,0.462,0,0,0,3.1,0.5);
	this.question._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question).wait(201).to({_off:false},0).wait(63).to({alpha:0},4).wait(1));

	// Layer_5
	this.question_1 = new lib.setscopy6();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(341.2,426,0.462,0.462,0,0,0,3.1,0.5);
	this.question_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question_1).wait(91).to({_off:false},0).to({_off:true},46).wait(132));

	// Layer_1
	this.instance_3 = new lib.Symbol17("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(606.2,111.1,1,1,0,0,0,146,32.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(58).to({_off:false},0).wait(72).to({startPosition:31},0).to({alpha:0,startPosition:37},6).to({_off:true},1).wait(132));

	// Layer_3
	this.question_2 = new lib.setscopy();
	this.question_2.name = "question_2";
	this.question_2.parent = this;
	this.question_2.setTransform(654.7,392.4,0.608,0.608,0,0,0,2.7,0.1);
	this.question_2.alpha = 0.352;
	this.question_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question_2).wait(43).to({_off:false},0).to({alpha:1},5).wait(153).to({alpha:0},7).to({_off:true},1).wait(60));

	// Layer_2
	this.questiontxt = new lib.questiontext_mccopy();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(620.9,122.3,1.351,1.351,0,0,0,2.2,-9.3);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(20).to({_off:false},0).to({alpha:1},5).wait(176).to({alpha:0},7).to({_off:true},1).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(260.9,112.9,2025,1284);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;