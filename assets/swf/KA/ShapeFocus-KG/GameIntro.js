(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgbgZIhOABIABgpIBOgDIAChjIAsgDIgCBmIBYgDIgDAqIhVACIgCDFIgvABg");
	this.shape.setTransform(121.1,0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AA+A9QgHANgJAMQgKAMgLAJQgLAJgMAFQgMAFgOAAQgUgBgPgHQgOgHgLgMQgKgLgGgQQgFgPgEgQQgCgSgBgRIgCgiQAAgXADgYIAFgyIAvABQgEAdgBAWIgCAnIAAAUIACAYIAFAaQADANAGAKQAGAKAIAFQAJAGANgCQAQgDAUgVQASgVAVgsIgBg6IgBggIgBgTIAxgFIADBGIACA3IABAsIABAhIABAwIgzABg");
	this.shape_1.setTransform(95.4,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AguB7QgWgJgRgSQgQgRgKgYQgJgZAAgdQAAgeAJgZQAKgYAQgRQARgSAWgJQAWgKAYAAQAZAAAWALQAWAKARARQAQASAKAZQAJAXAAAdQAAAcgJAYQgKAYgQASQgRASgWAKQgWAKgZAAQgYAAgWgKgAgghUQgOAJgJANQgJAOgEAQQgEARAAAPQAAAQAFAQQAEARAJANQAJAOAOAIQANAJASAAQATAAAOgJQAOgIAJgOQAJgNAEgRQAEgQAAgQQAAgPgEgRQgEgQgJgOQgIgNgOgJQgOgIgUAAQgTAAgNAIg");
	this.shape_2.setTransform(67.2,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgyB5QgXgJgQgQQgQgRgIgXQgJgXAAgdQAAgbAKgZQAKgYAQgTQARgSAXgKQAWgKAaAAQAYAAAUAJQAVAJAQAQQAQAQALAWQAKAWACAaIjCAcQABAQAGANQAHANAKAIQAJAJAOAFQAMAEAPAAQAMAAAMgDQALgEAKgHQALgHAHgLQAHgLADgOIAsAJQgGAVgMARQgLAQgQAMQgPAMgTAHQgSAGgUAAQgdAAgXgJgAgShXQgLADgLAIQgLAIgIANQgJANgDAUICIgQIgCgFQgIgWgQgNQgOgNgXAAQgIAAgMAEg");
	this.shape_3.setTransform(27.7,5.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AhpA4IgCg4IgBgrIgBghIgCgwIA0gBIABBBQAHgNAKgLQAJgMALgIQALgJAMgGQAMgFAOgBQARgBANAEQANAEAJAHQAJAHAGAKQAGAKADALQAEAKABALIACATQABAmAAAnIgCBSIgugCIADhJQACgkgCgkIgBgLIgCgOIgGgOQgDgHgFgFQgGgGgIgCQgHgDgLABQgSADgRAYQgTAXgVAsIACA6IABAhIAAARIgwAGIgDhGg");
	this.shape_4.setTransform(-0.7,5.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AguB7QgWgJgRgSQgQgRgKgYQgJgZAAgdQAAgeAJgZQAKgYAQgRQARgSAWgJQAWgKAYAAQAZAAAWALQAWAKARARQAQASAKAZQAJAXAAAdQAAAcgJAYQgKAYgQASQgRASgWAKQgWAKgZAAQgYAAgWgKgAgghUQgOAJgJANQgJAOgEAQQgEARAAAPQAAAQAFAQQAEARAJANQAJAOAOAIQANAJASAAQATAAAOgJQAOgIAJgOQAJgNAEgRQAEgQAAgQQAAgPgEgRQgEgQgJgOQgIgNgOgJQgOgIgUAAQgTAAgNAIg");
	this.shape_5.setTransform(-28.8,5.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgzCiQgWgKgRgRQgRgRgJgYQgKgZAAgeQAAgfALgXQAKgYASgRQARgQAWgJQAVgJAXAAQAOAAAPAEQAOADAPAIQAPAHAOAOIACh0IAqADIgBFNIguAAIAAgdQgGAHgHAEIgPAJIgOAGIgNADQgOAEgNABQgaAAgXgJgAgignQgOAHgKAMQgKAMgGAPQgGAQAAATQAAATAGAQQAGAQAKALQAKAMAPAHQAOAGARAAQANAAANgFQAOgEALgIQALgJAJgLQAIgMAEgOIABgwQgEgOgJgLQgJgMgLgJQgMgJgNgEQgOgFgNAAQgRAAgOAHg");
	this.shape_6.setTransform(-71,0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgzCiQgWgKgRgRQgRgRgJgYQgKgZAAgeQAAgfALgXQAKgYASgRQARgQAWgJQAVgJAXAAQAOAAAPAEQAOADAPAIQAPAHAOAOIACh0IAqADIgBFNIguAAIAAgdQgGAHgHAEIgPAJIgOAGIgNADQgOAEgNABQgaAAgXgJgAgignQgOAHgKAMQgKAMgGAPQgGAQAAATQAAATAGAQQAGAQAKALQAKAMAPAHQAOAGARAAQANAAANgFQAOgEALgIQALgJAJgLQAIgMAEgOIABgwQgEgOgJgLQgJgMgLgJQgMgJgNgEQgOgFgNAAQgRAAgOAHg");
	this.shape_7.setTransform(-101.6,0.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AguB7QgWgJgRgSQgQgRgKgYQgJgZAAgdQAAgeAJgZQAKgYAQgRQARgSAWgJQAWgKAYAAQAZAAAWALQAWAKARARQAQASAKAZQAJAXAAAdQAAAcgJAYQgKAYgQASQgRASgWAKQgWAKgZAAQgYAAgWgKgAgghUQgOAJgJANQgJAOgEAQQgEARAAAPQAAAQAFAQQAEARAJANQAJAOAOAIQANAJASAAQATAAAOgJQAOgIAJgOQAJgNAEgRQAEgQAAgQQAAgPgEgRQgEgQgJgOQgIgNgOgJQgOgIgUAAQgTAAgNAIg");
	this.shape_8.setTransform(-129.9,5.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(3,1,1).p("AzXkSMAmvAAAQBsAABMBMQBMBMAABsIAAAdQAABrhMBMQhMBNhsAAMgmvAAAQhsAAhMhNQhMhMAAhrIAAgdQAAhsBMhMQBMhMBsAAg");
	this.shape_9.setTransform(-2.5,0.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AzXETQhsAAhMhNQhMhLAAhsIAAgdQAAhsBMhMQBMhMBsAAMAmvAAAQBsAABMBMQBMBMAABsIAAAdQAABshMBLQhMBNhsAAg");
	this.shape_10.setTransform(-2.5,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159.4,-37,308.4,73.1);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQgPIgvABIAAgZIAwgBIABg8IAagCIgBA9IA1gCIgCAaIgzABIgBB2IgcABg");
	this.shape.setTransform(152.9,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAlAlIgKAPIgMANQgHAFgIADQgGADgJAAQgLAAgKgFQgJgEgGgHQgGgHgDgJQgDgJgCgLQgCgKgBgLIgBgTIACgdIADgeIAcABIgCAeIgBAYIAAAMIABAOIACAQQACAIAEAGQADAGAGADQAFADAIgBQAJgBAMgNQALgNAMgaIAAgjIgBgUIAAgLIAdgDIACAqIABAhIABAbIABAUIABAdIggAAg");
	this.shape_1.setTransform(137.4,3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgcBKQgNgFgKgLQgKgKgFgPQgGgPAAgSQAAgRAGgPQAFgPAKgKQAKgLANgGQAOgFAOAAQAPAAANAGQAOAGAKALQAKAKAGAPQAFAPAAAQQAAARgFAPQgGAOgKALQgKALgOAGQgNAGgPAAQgOAAgOgGgAgTgyQgIAFgGAIQgFAIgDAKQgCAKAAAJQAAAKADAJQACAKAGAJQAFAHAJAGQAIAFAKAAQAMAAAIgFQAIgGAGgHQAFgJADgKQACgJAAgKQAAgJgCgKQgCgKgGgIQgFgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_2.setTransform(120.3,3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeBJQgOgFgKgKQgJgKgFgOQgFgOAAgSQAAgQAGgPQAGgPAJgKQALgLANgHQAOgGAPAAQAOAAAMAGQAOAFAJAKQAKAKAGANQAGANACAQIh2AQQABAKAFAIQADAHAGAGQAGAFAIADQAIADAIAAQAHAAAIgDQAGgCAGgEQAHgEAEgHQAEgGACgJIAbAGQgEAMgHAKQgHAKgKAHQgJAIgKAEQgMAEgMAAQgRAAgOgGgAgKg0QgIABgGAFQgHAFgFAIQgFAIgCAMIBRgKIAAgDQgGgNgIgIQgKgHgNAAQgFAAgGACg");
	this.shape_3.setTransform(96.5,3.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag/AiIgBgiIgBgaIgBgUIgBgdIAgAAIAAAnQAFgIAFgGIANgNQAHgFAGgDQAIgEAIAAQALgBAHADQAIACAFAEQAGAFADAGIAGAMIADANIABALIABAuIgBAyIgcgBIACgsIgBgsIAAgGIgBgIIgEgJQgCgEgDgDQgDgEgFgBQgFgCgGABQgLACgKAOQgLAOgNAaIABAjIABAUIAAALIgdADIgCgqg");
	this.shape_4.setTransform(79.3,3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgcBKQgNgFgKgLQgKgKgFgPQgGgPAAgSQAAgRAGgPQAFgPAKgKQAKgLANgGQAOgFAOAAQAPAAANAGQAOAGAKALQAKAKAGAPQAFAPAAAQQAAARgFAPQgGAOgKALQgKALgOAGQgNAGgPAAQgOAAgOgGgAgTgyQgIAFgGAIQgFAIgDAKQgCAKAAAJQAAAKADAJQACAKAGAJQAFAHAJAGQAIAFAKAAQAMAAAIgFQAIgGAGgHQAFgJADgKQACgJAAgKQAAgJgCgKQgCgKgGgIQgFgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_5.setTransform(62.3,3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgfBiQgNgGgKgKQgKgLgGgOQgGgPAAgTQAAgSAGgOQAHgOALgKQAKgKANgFQAOgGANAAQAIAAAKADQAHACAKAEQAJAEAIAJIABhGIAaACIgBDIIgcAAIAAgRIgIAHIgJAFIgIADIgHADIgRACQgQAAgOgFgAgUgXQgJAEgGAHQgGAHgDAJQgEAKAAAMQAAALAEAKQADAJAGAHQAGAHAKAEQAIAEAKAAQAHAAAJgDQAIgDAGgEQAHgGAGgGQAFgHACgJIAAgdQgCgJgFgGQgGgHgHgFQgGgGgIgDQgJgCgHAAQgKAAgJAEg");
	this.shape_6.setTransform(36.9,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeBiQgOgGgKgKQgKgLgGgOQgGgPAAgTQAAgSAHgOQAGgOAKgKQALgKANgFQAOgGAMAAQAJAAAJADQAIACAKAEQAJAEAIAJIABhGIAaACIAADIIgcAAIAAgRIgJAHIgIAFIgJADIgIADIgQACQgPAAgOgFgAgUgXQgJAEgGAHQgGAHgEAJQgDAKAAAMQAAALADAKQAEAJAGAHQAGAHAJAEQAJAEAKAAQAHAAAJgDQAIgDAHgEQAHgGAEgGQAFgHADgJIABgdQgDgJgGgGQgEgHgHgFQgHgGgJgDQgIgCgIAAQgKAAgIAEg");
	this.shape_7.setTransform(18.4,0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgcBKQgNgFgKgLQgKgKgFgPQgGgPAAgSQAAgRAGgPQAFgPAKgKQAKgLANgGQAOgFAOAAQAPAAANAGQAOAGAKALQAKAKAGAPQAFAPAAAQQAAARgFAPQgGAOgKALQgKALgOAGQgNAGgPAAQgOAAgOgGgAgTgyQgIAFgGAIQgFAIgDAKQgCAKAAAJQAAAKADAJQACAKAGAJQAFAHAJAGQAIAFAKAAQAMAAAIgFQAIgGAGgHQAFgJADgKQACgJAAgKQAAgJgCgKQgCgKgGgIQgFgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_8.setTransform(1.2,3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgeBJQgOgFgJgKQgKgKgFgOQgFgOAAgSQAAgQAGgPQAFgPAKgKQALgLAOgHQANgGAPAAQAOAAANAGQAMAFAKAKQAKAKAGANQAGANACAQIh1AQQAAAKAEAIQAEAHAGAGQAGAFAIADQAIADAIAAQAHAAAIgDQAHgCAFgEQAGgEAFgHQAFgGACgJIAaAGQgEAMgHAKQgHAKgJAHQgKAIgKAEQgMAEgMAAQgRAAgOgGgAgKg0QgIABgGAFQgHAFgEAIQgGAIgCAMIBRgKIAAgDQgGgNgJgIQgIgHgOAAQgFAAgGACg");
	this.shape_9.setTransform(-22.6,3.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAkBoQAFgNACgMIACgXIAAgWQgBgOgEgLQgEgJgHgGQgGgHgHgDQgIgDgIAAQgHABgIAEQgGADgIAHQgIAGgHANIAABaIgbABIgCjSIAfgBIgBBTQAIgIAIgFQAJgFAHgCIAQgEQAPAAANAGQAMAFAJAKQAJALAGAOQAFAOAAASIAAAUIgBAUIgBATIgEAPg");
	this.shape_10.setTransform(-40.1,-0.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgQgPIgvABIAAgZIAwgBIABg8IAagCIgBA9IA1gCIgCAaIgzABIgBB2IgcABg");
	this.shape_11.setTransform(-56.1,0.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgQgPIgvABIAAgZIAwgBIABg8IAagCIgBA9IA1gCIgCAaIgzABIgBB2IgcABg");
	this.shape_12.setTransform(-76.7,0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgYBKQgPgHgLgKQgMgKgGgPQgHgOAAgSQAAgJADgKQACgKAGgJQAFgJAIgHQAHgHAIgGQAJgFALgDQAKgDAKAAQAMAAALADQALACAJAGQAJAFAIAIQAHAHAFAKIAAABIgXAOIAAgBQgDgHgGgGIgLgJQgHgEgGgCQgHgCgJAAQgKAAgJAEQgLAFgGAHQgIAHgEAKQgEAKgBAKQABAMAEAJQAEAKAIAIQAGAHALAEQAJAFAKgBQAIAAAHgCQAHgCAFgDQAGgDAFgFIAJgMIABgBIAYAOIAAABQgGAJgIAHQgIAHgIAFQgKAFgKADQgKADgMgBQgOAAgOgFg");
	this.shape_13.setTransform(-91.8,3.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeBJQgOgFgKgKQgJgKgFgOQgFgOAAgSQAAgQAGgPQAGgPAKgKQAJgLAOgHQAOgGAPAAQAOAAAMAGQAOAFAJAKQAKAKAGANQAHANABAQIh2AQQACAKADAIQAEAHAGAGQAGAFAIADQAIADAIAAQAHAAAHgDQAIgCAGgEQAGgEAEgHQAFgGABgJIAbAGQgEAMgHAKQgHAKgKAHQgIAIgMAEQgLAEgMAAQgRAAgOgGgAgLg0QgGABgHAFQgGAFgGAIQgEAIgCAMIBRgKIgBgDQgFgNgKgIQgJgHgNAAQgFAAgHACg");
	this.shape_14.setTransform(-108.5,3.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAJB3QgIgDgEgFQgGgFgEgGIgHgNQgHgQgBgUIADisIAcAAIgBAtIgBAlIgBAdIAAAWIAAAlQAAANACAKIAEAIIAFAIQAEADAEACQAEACAGAAIgDAbQgKAAgHgDg");
	this.shape_15.setTransform(-120,-2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgeBJQgOgFgJgKQgKgKgFgOQgFgOAAgSQAAgQAGgPQAFgPALgKQAKgLAOgHQANgGAPAAQAOAAANAGQANAFAJAKQAJAKAHANQAGANACAQIh1AQQABAKADAIQAEAHAGAGQAGAFAIADQAIADAIAAQAHAAAHgDQAHgCAHgEQAFgEAFgHQAFgGACgJIAaAGQgEAMgHAKQgHAKgJAHQgKAIgLAEQgLAEgMAAQgRAAgOgGgAgLg0QgGABgHAFQgGAFgFAIQgGAIgBAMIBQgKIAAgDQgFgNgKgIQgIgHgOAAQgFAAgHACg");
	this.shape_16.setTransform(-133,3.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgRBuQgKgCgKgEQgKgEgKgGIgTgMIAUgYQALAKAKAFIATAIQAKADAIABQAKABAIgCQAIgCAHgEQAGgEAEgFQAEgGABgGQAAgFgCgEQgCgEgFgEQgEgDgGgDIgMgEIgNgEIgNgDIgQgDIgRgGQgIgEgIgGQgHgEgFgIQgGgIgDgKQgCgKAAgNQABgLAEgJQAEgJAGgHQAGgHAHgFQAIgFAJgDQAJgDAJgCQAJgBAJAAQANABANADIALADIAMAFIAMAHIAMAKIgPAYIgKgJIgJgGIgKgFIgKgDQgKgDgKAAQgMAAgKAEQgJADgHAGQgHAGgDAHQgEAIAAAHQAAAHAFAGQAEAHAHAFQAIAGAKADQAKAEAKACQAKABAKACIATAFQAJAEAIAGQAHAFAFAHQAFAHADAJQACAJgBAKQgCAKgEAIQgEAHgGAFQgGAGgHAEIgQAGQgIACgIABIgQABQgKAAgKgCg");
	this.shape_17.setTransform(-150.4,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#990000").ss(4,1,1).p("A5IjaMAyRAAAQBbAABABAQBABAAABaIAAAAQAABbhABAQhABAhbAAMgyRAAAQhbAAhAhAQhAhAAAhbIAAAAQAAhaBAhAQBAhABbAAg");

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF3333").s().p("A5IDbQhbAAhAhAQhAhAAAhbIAAAAQAAhaBAhAQBAhABbAAMAyRAAAQBbAABABAQBABAAABaIAAAAQAABbhABAQhABAhbAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-184.8,-23.9,369.7,47.8), null);


(lib.Symbol1copy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(8,1,1).p("AdNAAIunZSI9LAAIun5SIOn5RIdLAAg");
	this.shape.setTransform(495.2,276.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AulZSIun5SIOn5RIdLAAIOnZRIunZSg");
	this.shape_1.setTransform(495.2,276.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy10, new cjs.Rectangle(304.4,110.3,381.8,331.6), null);


(lib.Symbol1copy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(8,1,1).p("AdNAAIunZSI9LAAIun5SIOn5RIdLAAg");
	this.shape.setTransform(495.2,276.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AulZSIun5SIOn5RIdLAAIOnZRIunZSg");
	this.shape_1.setTransform(495.2,276.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy9, new cjs.Rectangle(304.4,110.3,381.8,331.6), null);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween4("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159.4,-37,308.4,73.1);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy10();
	this.instance.parent = this;
	this.instance.setTransform(457.9,1.2,0.683,0.683,0,0,0,500.3,277.8);

	this.instance_1 = new lib.Symbol1copy10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-148.2,1.2,0.683,0.683,0,0,0,500.2,277.8);

	this.instance_2 = new lib.Symbol1copy10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-451.2,1.2,0.683,0.683,0,0,0,499.9,277.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,51,102,0.4)").s().p("EhMaAWgQjPAAiTiSQiSiTAAjOIAA9YQAAjQCSiSQCTiTDPAAMCY1AAAQDPAACTCTQCSCSAADQIAAdYQAADOiSCTQiTCSjPAAg");
	this.shape.setTransform(0.1,0,1.158,1.158);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-624,-166.7,1248.2,333.5);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy10();
	this.instance.parent = this;
	this.instance.setTransform(457.9,1.2,0.683,0.683,0,0,0,500.3,277.8);

	this.instance_1 = new lib.Symbol1copy10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-148.2,1.2,0.683,0.683,0,0,0,500.2,277.8);

	this.instance_2 = new lib.Symbol1copy9();
	this.instance_2.parent = this;
	this.instance_2.setTransform(154.8,1.2,0.683,0.683,0,0,0,500.3,277.8);

	this.instance_3 = new lib.Symbol1copy10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-451.2,1.2,0.683,0.683,0,0,0,499.9,277.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,51,102,0.4)").s().p("EhMaAWgQjPAAiTiSQiSiTAAjOIAA9YQAAjQCSiSQCTiTDPAAMCY1AAAQDPAACTCTQCSCSAADQIAAdYQAADOiSCTQiTCSjPAAg");
	this.shape.setTransform(0.1,0,1.158,1.158);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-624,-166.7,1248.2,333.5);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy9();
	this.instance.parent = this;
	this.instance.setTransform(3.5,1.2,0.683,0.683,0,0,0,500.3,277.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:500.2,regY:277.7,scaleX:0.75,scaleY:0.75},12).to({regX:500.3,regY:277.8,scaleX:0.68,scaleY:0.68},12).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-130.4,-113.3,260.9,226.6);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape_1.setTransform(-12,-29.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_2.setTransform(-12,-28.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_3.setTransform(22.2,15.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_4.setTransform(22.2,15.8);

	this.instance_3 = new lib.Symbol3copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance_3.alpha = 0.801;
	this.instance_3.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance_3.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-0.2,2.9,0.91,0.91,0,0,0,0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-168.5,-19.1,336.5,43.6), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(790.9,449.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(233).to({_off:false},0).wait(32).to({startPosition:11},0).to({alpha:0,startPosition:0},9).wait(8));

	// Layer_8
	this.instance_1 = new lib.Symbol1copy9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(795,447.3,0.683,0.683,0,0,0,500.3,277.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(218).to({_off:false},0).to({regX:500.4,regY:277.9,scaleX:0.79,scaleY:0.79},21).wait(19).to({alpha:0},9).wait(15));

	// Layer_4
	this.instance_2 = new lib.Symbol1copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(847.8,550.7,1,1,0,0,0,190.5,64.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(182).to({_off:false},0).to({_off:true},35).wait(65));

	// arrow
	this.instance_3 = new lib.Symbol1copy3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(795.2,246.5,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(149).to({_off:false},0).to({_off:true},35).wait(98));

	// Layer_7
	this.instance_4 = new lib.Symbol2_1("synched",4);
	this.instance_4.parent = this;
	this.instance_4.setTransform(791.5,446.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(89).to({_off:false},0).wait(55).to({startPosition:12},0).to({_off:true},1).wait(137));

	// Layer_5
	this.instance_5 = new lib.Tween3("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(769.7,125);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(65).to({_off:false},0).wait(79).to({startPosition:13},0).to({_off:true},1).wait(137));

	// Layer_1
	this.instance_6 = new lib.Tween2("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(640,446.1);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween2copy("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(640,446.1);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(44).to({_off:false},0).to({alpha:1},7).to({_off:true},187).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(238).to({_off:false},0).to({alpha:0},9).wait(35));

	// Layer_3
	this.questxt = new lib.questiontext_mc();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(638.3,121.3,1.818,1.818);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(20).to({_off:false},0).to({alpha:1},7).wait(211).to({alpha:0},9).wait(35));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(562.3,289.3,1438,874);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#333333",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;