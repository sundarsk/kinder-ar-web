(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ag2BWIAAgyIABgmIAAgNIgsgBIAAglIAuAAQACgUAHgUQAFgSAMgQQALgOAQgJQARgIAXAAQANAAAPAEQAPAEAOAMIgUAiQgLgIgKgDQgKgDgIgBQgIAAgJAAQgHACgGAFQgHAEgDAHQgEAIgDAMQgDALgCASIBSAAIgFAmIhPgBIAAAQIAAA1IAAAYIgBAbIAAAbIAAAbIgpAAIAChJg");
	this.shape.setTransform(210.4,-1.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgoBqQgTgIgOgPQgOgPgIgVQgJgVAAgaQAAgZAJgVQAIgVAOgPQAOgPATgJQATgIAVAAQAVAAAUAJQATAJAOAPQAOAPAJAVQAIAVAAAYQAAAZgIAVQgJAUgOAQQgOAPgTAJQgUAJgVAAQgVAAgTgJgAgchIQgMAHgHALQgIAMgDAOQgEAPAAANQAAAOAEAOQAEAOAIAMQAHALAMAHQAMAIAPAAQAQAAANgIQALgHAIgLQAIgMAEgOQAEgOAAgOQAAgNgEgPQgEgOgHgMQgIgLgMgHQgMgHgRAAQgQAAgMAHg");
	this.shape_1.setTransform(187.7,3.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("Ag2BWIAAgyIABgmIAAgNIgsgBIAAglIAuAAQACgUAGgUQAGgSAMgQQALgOAQgJQARgIAYAAQANAAAOAEQAPAEAOAMIgUAiQgLgIgKgDQgJgDgJgBQgIAAgJAAQgIACgFAFQgGAEgEAHQgEAIgDAMQgDALgCASIBSAAIgFAmIhQgBIAAAQIAAA1IAAAYIAAAbIAAAbIAAAbIgoAAIABhJg");
	this.shape_2.setTransform(158.3,-1.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AANCqQgLgFgIgGQgIgIgFgJQgGgJgEgJQgJgXgDgcIAFj2IApAAIgDA/IgBA1IgBArIAAAfIgBA1QAAATAEAOIAFAMQADAFAFAFQAFAFAGADQAHACAIAAIgGAoQgNAAgKgFg");
	this.shape_3.setTransform(143.3,-3.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AA8BvIAEgbQgYAPgYAHQgVAGgUAAQgOAAgNgDQgOgDgKgIQgKgIgGgLQgHgMABgQQAAgPAEgNQAFgMAJgKQAHgIAMgHQALgIAMgDQANgFAOgCQANgCAOAAIAYABIAUACIgGgTQgEgJgFgIQgHgHgIgFQgIgEgMgBQgHAAgJACQgKADgMAFQgLAHgOAKQgOAKgQAQIgXgcQASgSASgLQAQgMAPgFQAPgHAOgCQAMgDAKAAQASAAAOAGQAOAHALAKQALAKAHAPQAHAPAFAQQAEARACARQACARABARQgBATgCAWQgCAWgEAZgAgIgBQgRAEgMAIQgMAHgGAMQgGALADAPQAEALAHAFQAIAEALAAQAMAAANgDIAZgJQANgGAMgHIATgLIABgTQAAgJgBgLIgUgEIgWgBQgRgBgPAEg");
	this.shape_4.setTransform(123.4,2.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AA0CVQAGgTACgSIAEggIABgfQgBgVgHgOQgGgOgJgJQgKgKgKgEQgLgEgKAAQgLABgLAGQgKAEgLAKQgLAJgKASIgBCBIgmABIgDksIAtgCIgCB3QALgMAMgGQAMgHALgEQAMgDAKgCQAWAAATAIQARAIANAOQANAPAIAVQAHAUABAaIAAAdIgBAcIgDAbQgCAMgDAKg");
	this.shape_5.setTransform(98.6,-1.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgqCjIgSgHQgJgEgJgGQgIgGgIgJQgHgJgEgMIAlgRQAEAJAFAGQAGAGAFAEQAHAEAGACQAGADAGABQAOADAPgCQAOgCAKgGQAKgGAHgHQAHgHAEgHIAIgPIADgNIABgJIAAgZQgMALgMAGQgNAGgLACQgMAEgMAAQgWAAgTgIQgUgIgOgOQgPgPgJgUQgIgVAAgbQAAgaAJgVQAKgVAOgOQAPgOAUgIQASgHATAAQAMABAOAEQALAEAOAHQANAHALALIABgmIAmABIgBDfQAAANgEANQgCAMgHAMQgGAMgIAKQgKALgMAIQgMAIgOAFQgPAFgRACIgDAAQgWAAgTgFgAgdh2QgMAFgIAKQgIALgGAOQgEANAAAQQAAAQAEAOQAFANAJAJQAJAKAMAFQAMAGAOAAQAMAAANgFQAMgEAJgIQAKgIAHgLQAHgLACgNIAAgaQgBgOgIgLQgGgMgLgIQgKgIgMgFQgNgEgMAAQgOAAgMAGg");
	this.shape_6.setTransform(60.8,7.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AhbAwIgCgwIgBglIgBgdIgBgpIAtgBIAAA4QAHgLAIgJQAIgKAKgIQAJgIAKgEQALgFAMgBQAPgBALAEQALADAIAGQAIAHAFAIQAFAJADAJQADAJABAJIABAQQACAhgBAiIgBBGIgogBIADg/QABgfgCgfIAAgJIgCgMIgFgNQgDgGgFgEQgEgFgHgCQgHgDgJACQgPACgPAUQgQAUgSAmIABAzIABAcIAAAPIgqAFIgCg9g");
	this.shape_7.setTransform(35.9,3.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgYCTIABgzIABg9IABhTIAngBIAAAyIgBAqIgBAjIAAAbIgBAqgAgKhdQgFgBgDgFQgFgDgCgFQgCgGAAgFQAAgGACgFQACgFAFgEQADgEAFgCQAFgCAFAAQAFAAAFACQAFACAEAEQAEAEADAFQACAFAAAGQAAAFgCAGQgDAFgEADQgEAFgFABQgFACgFAAQgFAAgFgCg");
	this.shape_8.setTransform(18.9,-1.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AA1CVQAFgTACgSIAEggIABgfQgCgVgGgOQgGgOgJgJQgKgKgKgEQgLgEgKAAQgLABgLAGQgKAEgLAKQgLAJgKASIgBCBIgmABIgDksIAtgCIgCB3QALgMAMgGQAMgHALgEQAMgDAKgCQAXAAASAIQARAIANAOQAOAPAHAVQAHAUACAaIgBAdIgBAcIgDAbQgCAMgDAKg");
	this.shape_9.setTransform(0.7,-1.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgkBpQgUgJgQgOQgQgQgKgUQgKgVAAgYQABgOAEgOQADgOAIgNQAHgNAKgLQALgKANgIQANgIAPgEQAOgFAPAAQARAAAQAFQAPAEANAHQANAIALALQAKALAHAOIABABIghATIAAAAQgFgLgIgHQgHgIgJgGQgJgFgKgEQgLgCgLAAQgOAAgOAGQgPAGgKALQgKAKgHAOQgFAPAAAPQAAAPAFAOQAHAPAKAKQAKAKAPAHQAOAFAOAAQALAAAJgCQAKgDAJgFQAJgFAHgHQAHgHAFgJIABgBIAiATIgBABQgHANgLAKQgLALgNAHQgOAHgOAEQgPAEgQAAQgUAAgWgJg");
	this.shape_10.setTransform(-24.7,3.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgXgWIhEABIABgkIBEgBIABhWIAmgDIgBBYIBMgCIgDAkIhKACIgBCpIgoABg");
	this.shape_11.setTransform(-46.4,-0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AA8BvIADgbQgXAPgYAHQgVAGgVAAQgOAAgNgDQgNgDgKgIQgKgIgGgLQgHgMAAgQQABgPAFgNQAEgMAIgKQAJgIAKgHQAMgIAMgDQANgFAOgCQANgCAOAAIAZABIATACIgGgTQgEgJgGgIQgFgHgJgFQgIgEgMgBQgIAAgIACQgKADgMAFQgMAHgOAKQgNAKgQAQIgYgcQATgSARgLQASgMAOgFQAQgHANgCQALgDALAAQASAAAOAGQAOAHALAKQAKAKAIAPQAHAPAFAQQAEARACARQACARAAARQAAATgCAWQgCAWgEAZgAgJgBQgQAEgMAIQgMAHgGAMQgGALAEAPQADALAHAFQAIAEALAAQAMAAANgDIAZgJQANgGAMgHIATgLIABgTQAAgJgBgLIgVgEIgVgBQgRgBgQAEg");
	this.shape_12.setTransform(-68.9,2.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("ABeAuIgBgqIAAgfQgBgQgEgHQgFgIgIAAQgEAAgFADIgKAHIgKALIgJAMIgIAMIgHALIAAASIABAXIABAeIAAAkIglACIgBg9IgBgqIgCgfQgBgQgEgHQgFgIgHAAQgGAAgEADQgGADgFAGIgLAMIgKANIgJANIgHAKIACBnIgnACIgFjRIApgEIAAA5IAOgQQAHgJAIgHQAIgHAJgEQAKgEAMAAQAHAAAIACQAIADAFAFQAHAGADAIQAFAJABAMIANgQQAGgIAJgGQAHgHAKgEQAJgEALAAQAJAAAIADQAIADAGAGQAHAGAEAKQAEAJAAANIACAkIADAvIAABEIgpACIAAg9g");
	this.shape_13.setTransform(-97.1,2.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgXgWIhDABIABgkIBDgBIAChWIAmgDIgCBYIBLgCIgCAkIhJACIgCCpIgoABg");
	this.shape_14.setTransform(-133.2,-0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgkBpQgUgJgQgOQgQgQgKgUQgKgVAAgYQAAgOAEgOQAFgOAHgNQAHgNALgLQAKgKAMgIQANgIAPgEQAPgFAQAAQARAAAPAFQAPAEANAHQANAIAKALQALALAHAOIAAABIggATIgBAAQgFgLgHgHQgHgIgJgGQgJgFgKgEQgKgCgLAAQgQAAgOAGQgOAGgKALQgLAKgFAOQgHAPAAAPQAAAPAHAOQAFAPALAKQAKAKAOAHQAOAFAQAAQAKAAAJgCQAKgDAJgFQAIgFAIgHQAHgHAFgJIABgBIAiATIgBABQgHANgLAKQgLALgNAHQgNAHgPAEQgPAEgPAAQgVAAgWgJg");
	this.shape_15.setTransform(-154.7,3.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AA8BvIAEgbQgYAPgYAHQgVAGgUAAQgOAAgNgDQgOgDgKgIQgKgIgGgLQgHgMAAgQQABgPAEgNQAFgMAIgKQAJgIALgHQALgIAMgDQANgFAOgCQANgCAOAAIAYABIAUACIgGgTQgEgJgGgIQgFgHgJgFQgIgEgMgBQgIAAgIACQgKADgMAFQgMAHgOAKQgNAKgQAQIgXgcQASgSASgLQARgMAOgFQAPgHAOgCQAMgDAKAAQASAAAOAGQAOAHALAKQAKAKAIAPQAHAPAFAQQAEARACARQACARAAARQAAATgCAWQgCAWgEAZgAgJgBQgQAEgMAIQgMAHgGAMQgGALADAPQAEALAHAFQAIAEALAAQAMAAANgDIAZgJQANgGAMgHIATgLIABgTQAAgJgBgLIgUgEIgWgBQgRgBgQAEg");
	this.shape_16.setTransform(-180,2.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AACAeIg6BLIgpgLIBLheIhOheIApgKIA8BKIA7hLIAlAOIhIBbIBMBcIgkAOg");
	this.shape_17.setTransform(-202.8,3.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AgrBpQgUgIgOgOQgOgOgHgVQgHgUAAgZQAAgXAIgVQAJgVAOgQQAPgPATgKQAUgIAVgBQAVABASAHQASAIANAPQAOANAJATQAJATADAWIipAZQACANAFALQAGALAIAIQAJAHALAFQALADAMAAQALAAAKgCQAKgEAJgGQAIgGAHgKQAGgIADgMIAmAHQgGARgKAPQgKAPgNAKQgNALgQAFQgQAGgSgBQgYAAgUgHgAgQhLQgJADgKAGQgJAIgHALQgHALgDARIB0gOIgBgEQgHgTgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_18.setTransform(-225.2,3.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(2,1,1).p("EggqgESMBBVAAAQBrAABMBMQBNBMAABsIAAAdQAABshNBLQhMBNhrAAMhBVAAAQhrAAhNhNQhMhLAAhsIAAgdQAAhsBMhMQBNhMBrAAg");
	this.shape_19.setTransform(-7.2,-0.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("EggqAETQhrAAhNhNQhMhLAAhsIAAgdQAAhsBMhMQBNhMBrAAMBBVAAAQBrAABMBMQBNBMAABsIAAAdQAABshNBLQhMBNhrAAg");
	this.shape_20.setTransform(-7.2,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-243.2,-33.7,472.2,63.6);


(lib.quscopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjjMNQlElDAAnKQAAnJFElDQFClDHJgBMAAAAihQnJgBlClDg");
	mask.setTransform(55.3,110.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(153.3,92.6,0.536,0.536,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(143.5,89.1,0.536,0.536,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(106.6,103.6,0.536,0.536,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(129.5,92.6,0.536,0.536,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(123.8,82.2,0.536,0.536,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(129.6,87.3,0.536,0.536,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(120.3,127.1,0.536,0.536,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(129.7,128.8,0.536,0.536,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(126.3,121.2,0.536,0.536,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(137.9,115.3,0.536,0.536,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(139.6,129,0.536,0.536,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(113.5,147.5,0.536,0.536,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(133.5,150.9,0.536,0.536,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(162.6,120.1,0.536,0.536,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(160.3,107.9,0.536,0.536,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(144.5,102.4,0.536,0.536,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(147.6,111.1,0.536,0.536,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(153.2,132.7,0.536,0.536,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(150.5,143.1,0.536,0.536,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(123.6,146.9,0.536,0.536,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(105.8,137.5,0.536,0.536,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(103.8,117.2,0.536,0.536,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(153.7,170,0.536,0.536,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(135.1,177.4,0.536,0.536,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(96.4,172.2,0.536,0.536,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(109.1,176.4,0.536,0.536,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(124.1,178.5,0.536,0.536,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(111.2,171.9,0.536,0.536,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(91.7,165.6,0.536,0.536,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(80.4,157.7,0.536,0.536,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(72.5,143.8,0.536,0.536,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(74.9,151.6,0.536,0.536,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(70.6,133.2,0.536,0.536,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(80.2,151.2,0.536,0.536,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(73.5,138.8,0.536,0.536,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(70.9,128.3,0.536,0.536,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(125.3,159.7,0.536,0.536,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(122.3,167.2,0.536,0.536,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(114,161.5,0.536,0.536,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(136.3,166.7,0.536,0.536,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(146.6,165.9,0.536,0.536,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(148.7,156.2,0.536,0.536,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(156.3,158.4,0.536,0.536,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(157.8,150.5,0.536,0.536,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(167.7,148.2,0.536,0.536,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(171.9,139.1,0.536,0.536,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(102.1,156.2,0.536,0.536,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(93.4,153.3,0.536,0.536,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(87,147.8,0.536,0.536,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(90,139,0.536,0.536,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(81.2,129.5,0.536,0.536,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(90.4,127.5,0.536,0.536,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(87.5,111.1,0.536,0.536,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(78.9,109.2,0.536,0.536,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(93.6,107.5,0.536,0.536,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(83.9,100.4,0.536,0.536,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(97.1,98.3,0.536,0.536,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(91.9,92.8,0.536,0.536,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F3964B").s().p("AhaCMQgbgHgSgOQgKgKgHgLQgGgKgDgKQgIgYAAgbQABgZAKgXQALgYATgNQANgJAUgEQAKgCAZgDQAVgEANgEQATgFALgLQAOgMADgSQABgIgBgGQAUAGALAFIABAAQAcALAUAMQAXAOARAUQATAWAHAYQAJAjgQAnQgSAmggAXQg5AnhdAAQgdAAgVgEg");
	this.shape_58.setTransform(132.1,122.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#5D2408").s().p("AgKAoIgWgHIgSgDQgKgDgHgEQgIgEgGgIQgFgIAAgHQAAgKAHgHQAFgEAUgFQAWgFAXgFIADALIACAIIABAGIADAPIABAEQADACAGgEQAGgCACgGQACgEADgDQACgBAIAAIAAAAQAEgEgLgZIAfABQARAAAFAJIAIANQAAAGgBAFIgDANQgBAKgFAHQgIAJgUAHQgVAHgPAAQgKAAgNgEg");
	this.shape_59.setTransform(110.4,92.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#A33D0C").s().p("AgQBDIgmgLIgegHQgQgFgMgGQgNgHgJgMQgKgNAAgOQAAgQALgLQAJgIASgEQAtgOAugGQAFASAEAUIgDgLQgXAEgWAGQgUAFgFAEQgHAHAAAKQAAAHAFAIQAGAIAIAEQAHAEAKACIASAEIAWAHQANAEAKAAQAPAAAVgIQAUgGAIgKQAFgGABgKIADgNQABgGAAgFIgIgNQgFgJgRAAIgfgBIgBgHIgIgVQAmgDAoADQAcAAAJAPQAKARACAGQACAHgDAKIgFAXQgCAQgJALQgNARggALQgkALgZAAQgQAAgVgGg");
	this.shape_60.setTransform(110.4,92.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#692B0E").s().p("ABxDDIgwiJQgmhsg5g4Qg7g4gPggQgJgSgdgbQAKgRABgHQBHApA0BbIAKAUQAeA4AiBgIARAvIAbBMIAQArIAIAWIABAGQALAagEADQgPgfgOgmg");
	this.shape_61.setTransform(99.5,64.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#4A1D08").s().p("AB2EGIgBgGIgEgPIAAgGIgCgIQgEgTgGgTQgFgUgHgUQgQgsgLgYIgFgLQgKgWgLgUQgLgYgPgVQg4hbhOhIQgMgLgEgFQgIgKgBgKQgCgOAMgVIAGgKQAdAaAJATQAPAgA6A4QA6A4AlBsIAwCIQAPAnAOAfIAAAAQgHAAgDABQgDADgBAFQgCAGgGADQgFACgDAAIgCAAg");
	this.shape_62.setTransform(98.7,66.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#CD4A0B").s().p("AjTFoQhSgkhEhCQhlheglh5QgGgVgFgWQgJgtgBgvIAAgMQAAisBoiEQgrBeAABtQAAA7AMA2IAKAkQAJAcANAcQAoBWBLBIQBFBABRAmQBqAwB/AAQDhAACeiWQAegcAZggQgnBYhNBIQifCXjhAAQh/AAhpgxg");
	this.shape_63.setTransform(119.3,141.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EC5913").s().p("AjKGgQhQgmhFhAQhMhJgohXQgNgbgJgdIgKgkQgMg2AAg6QABhtAqhdQAZgfAegdQAwgtA1ggQAggSAhgNQAmgQApgKQAHAUAFAUQguAGgtAOQgSAEgJAIQgLAMAAAQQAAAPAKANQAJALAOAIQALAFARAFIAdAIIAmAKQAWAHARAAQAYAAAkgMQAggLANgRQAJgLACgPIAEgYQAEgLgDgGQgBgHgJgQQgKgQgbAAQgogCgnACIgQgrQA1gKA4AAQBUAABKAXQB9AiBkBeQAOANAMAOQCFCOAADCQAABtgrBeQgYAfgeAdQifCWjhAAQh+AAhrgwgAB9lTQgIArghAcQgbAXgsAOQgdAJgyAIQg4AHgZAGQgtAKgeAVQguAfgaA5QgVAzgCA9IAAAgQADArAOAoIAEAJQAUA0AkAiQApAiA/APQAxAMBEAAQDbABCChdQBNg3AlhWQAphagXhWQgPg4gqgyIgagbQgegcgngYQgtgchBgYIhIgZIgBAAQACAOgEARg");
	this.shape_64.setTransform(124.5,126.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE831F").s().p("AhfFQQhEAAgxgMQg/gPgpgiQgkgigVg0IgDgJQgOgogDgrIAAggQACg+AVgyQAag5AugfQAegVAtgKQAYgGA5gHQAygIAegJQAsgOAagXQAhgcAIgrQAEgRgCgOIABAAIBIAZQBBAYAtAcQAmAYAeAcIAaAbQArAyAPA4QAXBVgpBbQglBWhNA3Qh/BcjXAAIgHAAgAAViDQgDASgOANQgLAKgTAFQgNAEgVAEQgZADgKACQgUAEgNAJQgTANgLAZQgKAWgBAaQAAAbAIAXQADAKAGAKQAHALAKAKQASAOAbAHQAVAFAdAAQBdAAA5goQAggXASgmQAQgmgJgkQgHgYgTgWQgRgUgXgOQgUgMgcgLIgBAAQgLgFgUgFQABAFgBAIg");
	this.shape_65.setTransform(132.1,122.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AEMA0IAAgBQAIgSgKgqQgIgsgagcQgZgcglgSQgdgOgpgLQhMgUhMgDQgCAAgCAAQgfAAgeABQgxADguAKQgSADgpALADaA0QgDgIgGgIQgNgPgkgJQgogLgNgKQgJgHgFgJQgOgVgEgbADaA0QgeAAgggEQgPgBgPAQQgEAEgHAJQgGAIgGAFQgNAKgZAFQgUAFgyAHAEKA4QgRAtgtAhQg2AphBABQg+AEg8geQgHgDgIgDQgjgTgegbQgNgKgLgNQgpgqgig8QgcgxgZhDQgBgCgBgCAEAA1QgTAAgTgBAA4AYQgBgdgfgXQgKgIgQgKQgXgMgFgEQgOgJgIgKQgFgGAAgFAgXAFQgMgCgRAAQgBAAgCAAQgXAAgNAFQgEAEgIAEQgIAEgGABIAAABQgMAAgQgGAgXAFQgPgFgOgFQgigPgfgRQgQgIgQgKQADgRgBgOQgDgQgLgHQgCgCgFgDQgFgCgCgCQgGgEgDgHQgCgCgEgKACcAwQhWgKhOgcQgIgDgHgCAiVg3Qg/gng7g1");
	this.shape_66.setTransform(72.4,68.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#159335").s().p("ADaByQgDgIgGgIQgNgPgkgKQgogLgNgKQgJgHgFgKQgOgVgEgZQAEAZAOAVQAFAKAJAHQANAKAoALQAkAKANAPQAGAIADAIQgegBgggEQhWgKhOgcIgPgEQgPgFgOgHQgigOgfgSQgQgIgQgKQACgKAAgKIAAgJQgDgQgLgHIgHgFIgHgEQgGgEgDgHIgGgMIAGAMQADAHAGAEIAHAEIAHAFQALAHADAQIAAAJQAAAKgCAKQg/gmg7g0QApgLASgEQAugJAxgEIA9gBIAEAAQBMAEBMATQApAMAdANQAlATAZAcQAaAbAIArQAKArgIASIAAABIgHAAIgDAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAIgmgBgAg0gTQAIAKAOAJIAcAOQAQALAKAHQAfAYABAdQgBgdgfgYQgKgHgQgLIgcgOQgOgJgIgKQgFgHAAgEQAAAEAFAHg");
	this.shape_67.setTransform(72.4,62.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0E802B").s().p("AhICIIgPgHQgjgTgegaQgNgLgLgMQgpgrgig7QgcgxgZhDIgCgEQA7A0A/AnQAQAKAQAIQAfASAiAOQAOAHAPAFQgMgDgRAAIgDAAQgXAAgNAGQgEADgIADQgIAFgGABIAAABQgMAAgQgHQAQAHAMAAIAAgBQAGgBAIgFQAIgDAEgDQANgGAXAAIADAAQARAAAMADIAPAEQBOAbBWAKIgBAAIAAAAIAAAAIgBAAQgOABgNANIAAAAIgBABIgLANQgGAJgGAFQgNAJgZAFQgUAFgyAHQAygHAUgFQAZgFANgJQAGgFAGgJIALgNIABgBIAAAAQANgNAOgBIABAAIAAAAIAAAAIABAAQAgAEAeABIAmABQAAAAAAgBQAAAAAAAAQABAAAAAAQAAgBABAAIADAAIAHAAIARAAIAFgBIAFgBIAOAAQAGAAAFgCIAQgFIADgBQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAAAQgBABAAAAIgCABQgHAFgKABQgJACgKAAIgGAAIgGABIgFABIgUAAQgRAtgtAhQg2AohBACIgNAAQg2AAg3gagAB5AhIAAAAg");
	this.shape_68.setTransform(75.9,70.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FF0000").s().p("AhZHPQgfgBgQgKQgRgLgGgPQgGgPAAgNQAAgSAJgWQAJgWAXgQQAWgPApgBQAtABAUASQAUARAAAaQAAAXgJAWQgIAWgYAPQgYAOgvABIgBAAgAiSDkIgEhHIgFhCQgCghAAgnQACgjAVgYQAWgYAggTQAhgTAhgVQAggVAXgcQAXgbADgqQgFgggYgSQgagTgjgHQgkgHgnAAQgdAAgbADQgcABgTADIACgOIACgQIAFgdIAEgfIAGgqQAdgGAegDQAegEAeAAQBEgBA9AXQA8AVAmA0QAlAzABBYQgBA5gXAoQgXAogiAbQgiAcgiATQgiATgWARQgXARgBASQAAAOACAgIAGBCIACA+g");
	this.shape_69.setTransform(158.7,121.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AsJMJQlClBAAnHQAAnIFClCQEekeGHggQAsgDAtgBIAMAAQFwAAEaDTQBCAxA9A+QC2C2BPDgQA9CuAADGQAAHHlCFBQlBFDnIAAQnIAAlClDg");
	this.shape_70.setTransform(109.8,111.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70},{t:this.shape_69}]}).wait(1));

	// Layer 1
	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("rgba(51,51,51,0.859)").s().p("AtLNMQldleAAnuQAAntFdleQFeldHtAAQHvAAFdFdQFdFeAAHtQAAHuldFeQldFdnvAAQntAAleldg");
	this.shape_71.setTransform(109.2,112.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_71).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy5, new cjs.Rectangle(-10.1,-6.9,238.6,238.7), null);


(lib.quscopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjjMNQlElDAAnKQAAnJFElDQFClDHJgBMAAAAihQnJgBlClDg");
	mask.setTransform(55.3,110.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(153.3,92.6,0.536,0.536,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(143.5,89.1,0.536,0.536,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(106.6,103.6,0.536,0.536,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(129.5,92.6,0.536,0.536,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(123.8,82.2,0.536,0.536,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(129.6,87.3,0.536,0.536,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(120.3,127.1,0.536,0.536,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(129.7,128.8,0.536,0.536,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(126.3,121.2,0.536,0.536,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(137.9,115.3,0.536,0.536,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(139.6,129,0.536,0.536,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(113.5,147.5,0.536,0.536,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(133.5,150.9,0.536,0.536,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(162.6,120.1,0.536,0.536,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(160.3,107.9,0.536,0.536,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(144.5,102.4,0.536,0.536,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(147.6,111.1,0.536,0.536,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(153.2,132.7,0.536,0.536,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(150.5,143.1,0.536,0.536,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(123.6,146.9,0.536,0.536,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(105.8,137.5,0.536,0.536,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(103.8,117.2,0.536,0.536,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(153.7,170,0.536,0.536,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(135.1,177.4,0.536,0.536,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(96.4,172.2,0.536,0.536,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(109.1,176.4,0.536,0.536,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(124.1,178.5,0.536,0.536,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(111.2,171.9,0.536,0.536,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(91.7,165.6,0.536,0.536,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(80.4,157.7,0.536,0.536,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(72.5,143.8,0.536,0.536,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(74.9,151.6,0.536,0.536,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(70.6,133.2,0.536,0.536,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(80.2,151.2,0.536,0.536,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(73.5,138.8,0.536,0.536,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(70.9,128.3,0.536,0.536,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(125.3,159.7,0.536,0.536,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(122.3,167.2,0.536,0.536,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(114,161.5,0.536,0.536,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(136.3,166.7,0.536,0.536,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(146.6,165.9,0.536,0.536,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(148.7,156.2,0.536,0.536,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(156.3,158.4,0.536,0.536,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(157.8,150.5,0.536,0.536,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(167.7,148.2,0.536,0.536,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(171.9,139.1,0.536,0.536,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(102.1,156.2,0.536,0.536,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(93.4,153.3,0.536,0.536,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(87,147.8,0.536,0.536,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(90,139,0.536,0.536,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(81.2,129.5,0.536,0.536,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(90.4,127.5,0.536,0.536,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(87.5,111.1,0.536,0.536,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(78.9,109.2,0.536,0.536,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(93.6,107.5,0.536,0.536,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(83.9,100.4,0.536,0.536,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(97.1,98.3,0.536,0.536,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(91.9,92.8,0.536,0.536,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F3964B").s().p("AhaCMQgbgHgSgOQgKgKgHgLQgGgKgDgKQgIgYAAgbQABgZAKgXQALgYATgNQANgJAUgEQAKgCAZgDQAVgEANgEQATgFALgLQAOgMADgSQABgIgBgGQAUAGALAFIABAAQAcALAUAMQAXAOARAUQATAWAHAYQAJAjgQAnQgSAmggAXQg5AnhdAAQgdAAgVgEg");
	this.shape_58.setTransform(132.1,122.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#5D2408").s().p("AgKAoIgWgHIgSgDQgKgDgHgEQgIgEgGgIQgFgIAAgHQAAgKAHgHQAFgEAUgFQAWgFAXgFIADALIACAIIABAGIADAPIABAEQADACAGgEQAGgCACgGQACgEADgDQACgBAIAAIAAAAQAEgEgLgZIAfABQARAAAFAJIAIANQAAAGgBAFIgDANQgBAKgFAHQgIAJgUAHQgVAHgPAAQgKAAgNgEg");
	this.shape_59.setTransform(110.4,92.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#A33D0C").s().p("AgQBDIgmgLIgegHQgQgFgMgGQgNgHgJgMQgKgNAAgOQAAgQALgLQAJgIASgEQAtgOAugGQAFASAEAUIgDgLQgXAEgWAGQgUAFgFAEQgHAHAAAKQAAAHAFAIQAGAIAIAEQAHAEAKACIASAEIAWAHQANAEAKAAQAPAAAVgIQAUgGAIgKQAFgGABgKIADgNQABgGAAgFIgIgNQgFgJgRAAIgfgBIgBgHIgIgVQAmgDAoADQAcAAAJAPQAKARACAGQACAHgDAKIgFAXQgCAQgJALQgNARggALQgkALgZAAQgQAAgVgGg");
	this.shape_60.setTransform(110.4,92.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#692B0E").s().p("ABxDDIgwiJQgmhsg5g4Qg7g4gPggQgJgSgdgbQAKgRABgHQBHApA0BbIAKAUQAeA4AiBgIARAvIAbBMIAQArIAIAWIABAGQALAagEADQgPgfgOgmg");
	this.shape_61.setTransform(99.5,64.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#4A1D08").s().p("AB2EGIgBgGIgEgPIAAgGIgCgIQgEgTgGgTQgFgUgHgUQgQgsgLgYIgFgLQgKgWgLgUQgLgYgPgVQg4hbhOhIQgMgLgEgFQgIgKgBgKQgCgOAMgVIAGgKQAdAaAJATQAPAgA6A4QA6A4AlBsIAwCIQAPAnAOAfIAAAAQgHAAgDABQgDADgBAFQgCAGgGADQgFACgDAAIgCAAg");
	this.shape_62.setTransform(98.7,66.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#CD4A0B").s().p("AjTFoQhSgkhEhCQhlheglh5QgGgVgFgWQgJgtgBgvIAAgMQAAisBoiEQgrBeAABtQAAA7AMA2IAKAkQAJAcANAcQAoBWBLBIQBFBABRAmQBqAwB/AAQDhAACeiWQAegcAZggQgnBYhNBIQifCXjhAAQh/AAhpgxg");
	this.shape_63.setTransform(119.3,141.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EC5913").s().p("AjKGgQhQgmhFhAQhMhJgohXQgNgbgJgdIgKgkQgMg2AAg6QABhtAqhdQAZgfAegdQAwgtA1ggQAggSAhgNQAmgQApgKQAHAUAFAUQguAGgtAOQgSAEgJAIQgLAMAAAQQAAAPAKANQAJALAOAIQALAFARAFIAdAIIAmAKQAWAHARAAQAYAAAkgMQAggLANgRQAJgLACgPIAEgYQAEgLgDgGQgBgHgJgQQgKgQgbAAQgogCgnACIgQgrQA1gKA4AAQBUAABKAXQB9AiBkBeQAOANAMAOQCFCOAADCQAABtgrBeQgYAfgeAdQifCWjhAAQh+AAhrgwgAB9lTQgIArghAcQgbAXgsAOQgdAJgyAIQg4AHgZAGQgtAKgeAVQguAfgaA5QgVAzgCA9IAAAgQADArAOAoIAEAJQAUA0AkAiQApAiA/APQAxAMBEAAQDbABCChdQBNg3AlhWQAphagXhWQgPg4gqgyIgagbQgegcgngYQgtgchBgYIhIgZIgBAAQACAOgEARg");
	this.shape_64.setTransform(124.5,126.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE831F").s().p("AhfFQQhEAAgxgMQg/gPgpgiQgkgigVg0IgDgJQgOgogDgrIAAggQACg+AVgyQAag5AugfQAegVAtgKQAYgGA5gHQAygIAegJQAsgOAagXQAhgcAIgrQAEgRgCgOIABAAIBIAZQBBAYAtAcQAmAYAeAcIAaAbQArAyAPA4QAXBVgpBbQglBWhNA3Qh/BcjXAAIgHAAgAAViDQgDASgOANQgLAKgTAFQgNAEgVAEQgZADgKACQgUAEgNAJQgTANgLAZQgKAWgBAaQAAAbAIAXQADAKAGAKQAHALAKAKQASAOAbAHQAVAFAdAAQBdAAA5goQAggXASgmQAQgmgJgkQgHgYgTgWQgRgUgXgOQgUgMgcgLIgBAAQgLgFgUgFQABAFgBAIg");
	this.shape_65.setTransform(132.1,122.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AEMA0IAAgBQAIgSgKgqQgIgsgagcQgZgcglgSQgdgOgpgLQhMgUhMgDQgCAAgCAAQgfAAgeABQgxADguAKQgSADgpALADaA0QgDgIgGgIQgNgPgkgJQgogLgNgKQgJgHgFgJQgOgVgEgbADaA0QgeAAgggEQgPgBgPAQQgEAEgHAJQgGAIgGAFQgNAKgZAFQgUAFgyAHAEKA4QgRAtgtAhQg2AphBABQg+AEg8geQgHgDgIgDQgjgTgegbQgNgKgLgNQgpgqgig8QgcgxgZhDQgBgCgBgCAEAA1QgTAAgTgBAA4AYQgBgdgfgXQgKgIgQgKQgXgMgFgEQgOgJgIgKQgFgGAAgFAgXAFQgMgCgRAAQgBAAgCAAQgXAAgNAFQgEAEgIAEQgIAEgGABIAAABQgMAAgQgGAgXAFQgPgFgOgFQgigPgfgRQgQgIgQgKQADgRgBgOQgDgQgLgHQgCgCgFgDQgFgCgCgCQgGgEgDgHQgCgCgEgKACcAwQhWgKhOgcQgIgDgHgCAiVg3Qg/gng7g1");
	this.shape_66.setTransform(72.4,68.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#159335").s().p("ADaByQgDgIgGgIQgNgPgkgKQgogLgNgKQgJgHgFgKQgOgVgEgZQAEAZAOAVQAFAKAJAHQANAKAoALQAkAKANAPQAGAIADAIQgegBgggEQhWgKhOgcIgPgEQgPgFgOgHQgigOgfgSQgQgIgQgKQACgKAAgKIAAgJQgDgQgLgHIgHgFIgHgEQgGgEgDgHIgGgMIAGAMQADAHAGAEIAHAEIAHAFQALAHADAQIAAAJQAAAKgCAKQg/gmg7g0QApgLASgEQAugJAxgEIA9gBIAEAAQBMAEBMATQApAMAdANQAlATAZAcQAaAbAIArQAKArgIASIAAABIgHAAIgDAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAIgmgBgAg0gTQAIAKAOAJIAcAOQAQALAKAHQAfAYABAdQgBgdgfgYQgKgHgQgLIgcgOQgOgJgIgKQgFgHAAgEQAAAEAFAHg");
	this.shape_67.setTransform(72.4,62.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0E802B").s().p("AhICIIgPgHQgjgTgegaQgNgLgLgMQgpgrgig7QgcgxgZhDIgCgEQA7A0A/AnQAQAKAQAIQAfASAiAOQAOAHAPAFQgMgDgRAAIgDAAQgXAAgNAGQgEADgIADQgIAFgGABIAAABQgMAAgQgHQAQAHAMAAIAAgBQAGgBAIgFQAIgDAEgDQANgGAXAAIADAAQARAAAMADIAPAEQBOAbBWAKIgBAAIAAAAIAAAAIgBAAQgOABgNANIAAAAIgBABIgLANQgGAJgGAFQgNAJgZAFQgUAFgyAHQAygHAUgFQAZgFANgJQAGgFAGgJIALgNIABgBIAAAAQANgNAOgBIABAAIAAAAIAAAAIABAAQAgAEAeABIAmABQAAAAAAgBQAAAAAAAAQABAAAAAAQAAgBABAAIADAAIAHAAIARAAIAFgBIAFgBIAOAAQAGAAAFgCIAQgFIADgBQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAAAQgBABAAAAIgCABQgHAFgKABQgJACgKAAIgGAAIgGABIgFABIgUAAQgRAtgtAhQg2AohBACIgNAAQg2AAg3gagAB5AhIAAAAg");
	this.shape_68.setTransform(75.9,70.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AsJMJQlClBAAnHQAAnIFClCQEekeGHggQAsgDAtgBIAMAAQFwAAEaDTQBCAxA9A+QC2C2BPDgQA9CuAADGQAAHHlCFBQlBFDnIAAQnIAAlClDg");
	this.shape_69.setTransform(109.8,111.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_69).wait(1));

	// Layer 1
	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("rgba(51,51,51,0.859)").s().p("AtLNMQldleAAnuQAAntFdleQFeldHtAAQHvAAFdFdQFdFeAAHtQAAHuldFeQldFdnvAAQntAAleldg");
	this.shape_70.setTransform(109.2,112.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_70).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy4, new cjs.Rectangle(-10.1,-6.9,238.6,238.7), null);


(lib.quscopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjjMNQlElDAAnKQAAnJFElDQFClDHJgBMAAAAihQnJgBlClDg");
	mask.setTransform(55.3,110.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(153.3,92.6,0.536,0.536,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(143.5,89.1,0.536,0.536,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(106.6,103.6,0.536,0.536,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(129.5,92.6,0.536,0.536,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(123.8,82.2,0.536,0.536,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(129.6,87.3,0.536,0.536,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(120.3,127.1,0.536,0.536,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(129.7,128.8,0.536,0.536,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(126.3,121.2,0.536,0.536,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(137.9,115.3,0.536,0.536,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(139.6,129,0.536,0.536,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(113.5,147.5,0.536,0.536,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(133.5,150.9,0.536,0.536,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(162.6,120.1,0.536,0.536,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(160.3,107.9,0.536,0.536,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(144.5,102.4,0.536,0.536,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(147.6,111.1,0.536,0.536,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(153.2,132.7,0.536,0.536,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(150.5,143.1,0.536,0.536,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(123.6,146.9,0.536,0.536,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(105.8,137.5,0.536,0.536,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(103.8,117.2,0.536,0.536,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(153.7,170,0.536,0.536,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(135.1,177.4,0.536,0.536,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(96.4,172.2,0.536,0.536,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(109.1,176.4,0.536,0.536,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(124.1,178.5,0.536,0.536,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(111.2,171.9,0.536,0.536,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(91.7,165.6,0.536,0.536,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(80.4,157.7,0.536,0.536,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(72.5,143.8,0.536,0.536,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(74.9,151.6,0.536,0.536,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(70.6,133.2,0.536,0.536,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(80.2,151.2,0.536,0.536,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(73.5,138.8,0.536,0.536,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(70.9,128.3,0.536,0.536,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(125.3,159.7,0.536,0.536,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(122.3,167.2,0.536,0.536,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(114,161.5,0.536,0.536,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(136.3,166.7,0.536,0.536,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(146.6,165.9,0.536,0.536,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(148.7,156.2,0.536,0.536,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(156.3,158.4,0.536,0.536,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(157.8,150.5,0.536,0.536,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(167.7,148.2,0.536,0.536,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(171.9,139.1,0.536,0.536,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(102.1,156.2,0.536,0.536,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(93.4,153.3,0.536,0.536,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(87,147.8,0.536,0.536,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(90,139,0.536,0.536,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(81.2,129.5,0.536,0.536,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(90.4,127.5,0.536,0.536,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(87.5,111.1,0.536,0.536,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(78.9,109.2,0.536,0.536,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(93.6,107.5,0.536,0.536,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(83.9,100.4,0.536,0.536,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(97.1,98.3,0.536,0.536,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(91.9,92.8,0.536,0.536,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F3964B").s().p("AhaCMQgbgHgSgOQgKgKgHgLQgGgKgDgKQgIgYAAgbQABgZAKgXQALgYATgNQANgJAUgEQAKgCAZgDQAVgEANgEQATgFALgLQAOgMADgSQABgIgBgGQAUAGALAFIABAAQAcALAUAMQAXAOARAUQATAWAHAYQAJAjgQAnQgSAmggAXQg5AnhdAAQgdAAgVgEg");
	this.shape_58.setTransform(132.1,122.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#5D2408").s().p("AgKAoIgWgHIgSgDQgKgDgHgEQgIgEgGgIQgFgIAAgHQAAgKAHgHQAFgEAUgFQAWgFAXgFIADALIACAIIABAGIADAPIABAEQADACAGgEQAGgCACgGQACgEADgDQACgBAIAAIAAAAQAEgEgLgZIAfABQARAAAFAJIAIANQAAAGgBAFIgDANQgBAKgFAHQgIAJgUAHQgVAHgPAAQgKAAgNgEg");
	this.shape_59.setTransform(110.4,92.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#A33D0C").s().p("AgQBDIgmgLIgegHQgQgFgMgGQgNgHgJgMQgKgNAAgOQAAgQALgLQAJgIASgEQAtgOAugGQAFASAEAUIgDgLQgXAEgWAGQgUAFgFAEQgHAHAAAKQAAAHAFAIQAGAIAIAEQAHAEAKACIASAEIAWAHQANAEAKAAQAPAAAVgIQAUgGAIgKQAFgGABgKIADgNQABgGAAgFIgIgNQgFgJgRAAIgfgBIgBgHIgIgVQAmgDAoADQAcAAAJAPQAKARACAGQACAHgDAKIgFAXQgCAQgJALQgNARggALQgkALgZAAQgQAAgVgGg");
	this.shape_60.setTransform(110.4,92.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#692B0E").s().p("ABxDDIgwiJQgmhsg5g4Qg7g4gPggQgJgSgdgbQAKgRABgHQBHApA0BbIAKAUQAeA4AiBgIARAvIAbBMIAQArIAIAWIABAGQALAagEADQgPgfgOgmg");
	this.shape_61.setTransform(99.5,64.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#4A1D08").s().p("AB2EGIgBgGIgEgPIAAgGIgCgIQgEgTgGgTQgFgUgHgUQgQgsgLgYIgFgLQgKgWgLgUQgLgYgPgVQg4hbhOhIQgMgLgEgFQgIgKgBgKQgCgOAMgVIAGgKQAdAaAJATQAPAgA6A4QA6A4AlBsIAwCIQAPAnAOAfIAAAAQgHAAgDABQgDADgBAFQgCAGgGADQgFACgDAAIgCAAg");
	this.shape_62.setTransform(98.7,66.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#CD4A0B").s().p("AjTFoQhSgkhEhCQhlheglh5QgGgVgFgWQgJgtgBgvIAAgMQAAisBoiEQgrBeAABtQAAA7AMA2IAKAkQAJAcANAcQAoBWBLBIQBFBABRAmQBqAwB/AAQDhAACeiWQAegcAZggQgnBYhNBIQifCXjhAAQh/AAhpgxg");
	this.shape_63.setTransform(119.3,141.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EC5913").s().p("AjKGgQhQgmhFhAQhMhJgohXQgNgbgJgdIgKgkQgMg2AAg6QABhtAqhdQAZgfAegdQAwgtA1ggQAggSAhgNQAmgQApgKQAHAUAFAUQguAGgtAOQgSAEgJAIQgLAMAAAQQAAAPAKANQAJALAOAIQALAFARAFIAdAIIAmAKQAWAHARAAQAYAAAkgMQAggLANgRQAJgLACgPIAEgYQAEgLgDgGQgBgHgJgQQgKgQgbAAQgogCgnACIgQgrQA1gKA4AAQBUAABKAXQB9AiBkBeQAOANAMAOQCFCOAADCQAABtgrBeQgYAfgeAdQifCWjhAAQh+AAhrgwgAB9lTQgIArghAcQgbAXgsAOQgdAJgyAIQg4AHgZAGQgtAKgeAVQguAfgaA5QgVAzgCA9IAAAgQADArAOAoIAEAJQAUA0AkAiQApAiA/APQAxAMBEAAQDbABCChdQBNg3AlhWQAphagXhWQgPg4gqgyIgagbQgegcgngYQgtgchBgYIhIgZIgBAAQACAOgEARg");
	this.shape_64.setTransform(124.5,126.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE831F").s().p("AhfFQQhEAAgxgMQg/gPgpgiQgkgigVg0IgDgJQgOgogDgrIAAggQACg+AVgyQAag5AugfQAegVAtgKQAYgGA5gHQAygIAegJQAsgOAagXQAhgcAIgrQAEgRgCgOIABAAIBIAZQBBAYAtAcQAmAYAeAcIAaAbQArAyAPA4QAXBVgpBbQglBWhNA3Qh/BcjXAAIgHAAgAAViDQgDASgOANQgLAKgTAFQgNAEgVAEQgZADgKACQgUAEgNAJQgTANgLAZQgKAWgBAaQAAAbAIAXQADAKAGAKQAHALAKAKQASAOAbAHQAVAFAdAAQBdAAA5goQAggXASgmQAQgmgJgkQgHgYgTgWQgRgUgXgOQgUgMgcgLIgBAAQgLgFgUgFQABAFgBAIg");
	this.shape_65.setTransform(132.1,122.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AEMA0IAAgBQAIgSgKgqQgIgsgagcQgZgcglgSQgdgOgpgLQhMgUhMgDQgCAAgCAAQgfAAgeABQgxADguAKQgSADgpALADaA0QgDgIgGgIQgNgPgkgJQgogLgNgKQgJgHgFgJQgOgVgEgbADaA0QgeAAgggEQgPgBgPAQQgEAEgHAJQgGAIgGAFQgNAKgZAFQgUAFgyAHAEKA4QgRAtgtAhQg2AphBABQg+AEg8geQgHgDgIgDQgjgTgegbQgNgKgLgNQgpgqgig8QgcgxgZhDQgBgCgBgCAEAA1QgTAAgTgBAA4AYQgBgdgfgXQgKgIgQgKQgXgMgFgEQgOgJgIgKQgFgGAAgFAgXAFQgMgCgRAAQgBAAgCAAQgXAAgNAFQgEAEgIAEQgIAEgGABIAAABQgMAAgQgGAgXAFQgPgFgOgFQgigPgfgRQgQgIgQgKQADgRgBgOQgDgQgLgHQgCgCgFgDQgFgCgCgCQgGgEgDgHQgCgCgEgKACcAwQhWgKhOgcQgIgDgHgCAiVg3Qg/gng7g1");
	this.shape_66.setTransform(72.4,68.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#159335").s().p("ADaByQgDgIgGgIQgNgPgkgKQgogLgNgKQgJgHgFgKQgOgVgEgZQAEAZAOAVQAFAKAJAHQANAKAoALQAkAKANAPQAGAIADAIQgegBgggEQhWgKhOgcIgPgEQgPgFgOgHQgigOgfgSQgQgIgQgKQACgKAAgKIAAgJQgDgQgLgHIgHgFIgHgEQgGgEgDgHIgGgMIAGAMQADAHAGAEIAHAEIAHAFQALAHADAQIAAAJQAAAKgCAKQg/gmg7g0QApgLASgEQAugJAxgEIA9gBIAEAAQBMAEBMATQApAMAdANQAlATAZAcQAaAbAIArQAKArgIASIAAABIgHAAIgDAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAIgmgBgAg0gTQAIAKAOAJIAcAOQAQALAKAHQAfAYABAdQgBgdgfgYQgKgHgQgLIgcgOQgOgJgIgKQgFgHAAgEQAAAEAFAHg");
	this.shape_67.setTransform(72.4,62.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0E802B").s().p("AhICIIgPgHQgjgTgegaQgNgLgLgMQgpgrgig7QgcgxgZhDIgCgEQA7A0A/AnQAQAKAQAIQAfASAiAOQAOAHAPAFQgMgDgRAAIgDAAQgXAAgNAGQgEADgIADQgIAFgGABIAAABQgMAAgQgHQAQAHAMAAIAAgBQAGgBAIgFQAIgDAEgDQANgGAXAAIADAAQARAAAMADIAPAEQBOAbBWAKIgBAAIAAAAIAAAAIgBAAQgOABgNANIAAAAIgBABIgLANQgGAJgGAFQgNAJgZAFQgUAFgyAHQAygHAUgFQAZgFANgJQAGgFAGgJIALgNIABgBIAAAAQANgNAOgBIABAAIAAAAIAAAAIABAAQAgAEAeABIAmABQAAAAAAgBQAAAAAAAAQABAAAAAAQAAgBABAAIADAAIAHAAIARAAIAFgBIAFgBIAOAAQAGAAAFgCIAQgFIADgBQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAAAQgBABAAAAIgCABQgHAFgKABQgJACgKAAIgGAAIgGABIgFABIgUAAQgRAtgtAhQg2AohBACIgNAAQg2AAg3gagAB5AhIAAAAg");
	this.shape_68.setTransform(75.9,70.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy3, new cjs.Rectangle(44.2,38.1,66.3,144.7), null);


(lib.quscopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjjMNQlElDAAnKQAAnJFElDQFClDHJgBMAAAAihQnJgBlClDg");
	mask.setTransform(55.3,110.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(153.3,92.6,0.536,0.536,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(143.5,89.1,0.536,0.536,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(106.6,103.6,0.536,0.536,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(129.5,92.6,0.536,0.536,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(123.8,82.2,0.536,0.536,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(129.6,87.3,0.536,0.536,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(120.3,127.1,0.536,0.536,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(129.7,128.8,0.536,0.536,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(126.3,121.2,0.536,0.536,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(137.9,115.3,0.536,0.536,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(139.6,129,0.536,0.536,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(113.5,147.5,0.536,0.536,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(133.5,150.9,0.536,0.536,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(162.6,120.1,0.536,0.536,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(160.3,107.9,0.536,0.536,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(144.5,102.4,0.536,0.536,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(147.6,111.1,0.536,0.536,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(153.2,132.7,0.536,0.536,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(150.5,143.1,0.536,0.536,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(123.6,146.9,0.536,0.536,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(105.8,137.5,0.536,0.536,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(103.8,117.2,0.536,0.536,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(153.7,170,0.536,0.536,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(135.1,177.4,0.536,0.536,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(96.4,172.2,0.536,0.536,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(109.1,176.4,0.536,0.536,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(124.1,178.5,0.536,0.536,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(111.2,171.9,0.536,0.536,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(91.7,165.6,0.536,0.536,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(80.4,157.7,0.536,0.536,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(72.5,143.8,0.536,0.536,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(74.9,151.6,0.536,0.536,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(70.6,133.2,0.536,0.536,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(80.2,151.2,0.536,0.536,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(73.5,138.8,0.536,0.536,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(70.9,128.3,0.536,0.536,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(125.3,159.7,0.536,0.536,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(122.3,167.2,0.536,0.536,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(114,161.5,0.536,0.536,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(136.3,166.7,0.536,0.536,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(146.6,165.9,0.536,0.536,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(148.7,156.2,0.536,0.536,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(156.3,158.4,0.536,0.536,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(157.8,150.5,0.536,0.536,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(167.7,148.2,0.536,0.536,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(171.9,139.1,0.536,0.536,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(102.1,156.2,0.536,0.536,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(93.4,153.3,0.536,0.536,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(87,147.8,0.536,0.536,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(90,139,0.536,0.536,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(81.2,129.5,0.536,0.536,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(90.4,127.5,0.536,0.536,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(87.5,111.1,0.536,0.536,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(78.9,109.2,0.536,0.536,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(93.6,107.5,0.536,0.536,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(83.9,100.4,0.536,0.536,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(97.1,98.3,0.536,0.536,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(91.9,92.8,0.536,0.536,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F3964B").s().p("AhaCMQgbgHgSgOQgKgKgHgLQgGgKgDgKQgIgYAAgbQABgZAKgXQALgYATgNQANgJAUgEQAKgCAZgDQAVgEANgEQATgFALgLQAOgMADgSQABgIgBgGQAUAGALAFIABAAQAcALAUAMQAXAOARAUQATAWAHAYQAJAjgQAnQgSAmggAXQg5AnhdAAQgdAAgVgEg");
	this.shape_58.setTransform(132.1,122.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#5D2408").s().p("AgKAoIgWgHIgSgDQgKgDgHgEQgIgEgGgIQgFgIAAgHQAAgKAHgHQAFgEAUgFQAWgFAXgFIADALIACAIIABAGIADAPIABAEQADACAGgEQAGgCACgGQACgEADgDQACgBAIAAIAAAAQAEgEgLgZIAfABQARAAAFAJIAIANQAAAGgBAFIgDANQgBAKgFAHQgIAJgUAHQgVAHgPAAQgKAAgNgEg");
	this.shape_59.setTransform(110.4,92.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#A33D0C").s().p("AgQBDIgmgLIgegHQgQgFgMgGQgNgHgJgMQgKgNAAgOQAAgQALgLQAJgIASgEQAtgOAugGQAFASAEAUIgDgLQgXAEgWAGQgUAFgFAEQgHAHAAAKQAAAHAFAIQAGAIAIAEQAHAEAKACIASAEIAWAHQANAEAKAAQAPAAAVgIQAUgGAIgKQAFgGABgKIADgNQABgGAAgFIgIgNQgFgJgRAAIgfgBIgBgHIgIgVQAmgDAoADQAcAAAJAPQAKARACAGQACAHgDAKIgFAXQgCAQgJALQgNARggALQgkALgZAAQgQAAgVgGg");
	this.shape_60.setTransform(110.4,92.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#692B0E").s().p("ABxDDIgwiJQgmhsg5g4Qg7g4gPggQgJgSgdgbQAKgRABgHQBHApA0BbIAKAUQAeA4AiBgIARAvIAbBMIAQArIAIAWIABAGQALAagEADQgPgfgOgmg");
	this.shape_61.setTransform(99.5,64.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#4A1D08").s().p("AB2EGIgBgGIgEgPIAAgGIgCgIQgEgTgGgTQgFgUgHgUQgQgsgLgYIgFgLQgKgWgLgUQgLgYgPgVQg4hbhOhIQgMgLgEgFQgIgKgBgKQgCgOAMgVIAGgKQAdAaAJATQAPAgA6A4QA6A4AlBsIAwCIQAPAnAOAfIAAAAQgHAAgDABQgDADgBAFQgCAGgGADQgFACgDAAIgCAAg");
	this.shape_62.setTransform(98.7,66.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#CD4A0B").s().p("AjTFoQhSgkhEhCQhlheglh5QgGgVgFgWQgJgtgBgvIAAgMQAAisBoiEQgrBeAABtQAAA7AMA2IAKAkQAJAcANAcQAoBWBLBIQBFBABRAmQBqAwB/AAQDhAACeiWQAegcAZggQgnBYhNBIQifCXjhAAQh/AAhpgxg");
	this.shape_63.setTransform(119.3,141.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EC5913").s().p("AjKGgQhQgmhFhAQhMhJgohXQgNgbgJgdIgKgkQgMg2AAg6QABhtAqhdQAZgfAegdQAwgtA1ggQAggSAhgNQAmgQApgKQAHAUAFAUQguAGgtAOQgSAEgJAIQgLAMAAAQQAAAPAKANQAJALAOAIQALAFARAFIAdAIIAmAKQAWAHARAAQAYAAAkgMQAggLANgRQAJgLACgPIAEgYQAEgLgDgGQgBgHgJgQQgKgQgbAAQgogCgnACIgQgrQA1gKA4AAQBUAABKAXQB9AiBkBeQAOANAMAOQCFCOAADCQAABtgrBeQgYAfgeAdQifCWjhAAQh+AAhrgwgAB9lTQgIArghAcQgbAXgsAOQgdAJgyAIQg4AHgZAGQgtAKgeAVQguAfgaA5QgVAzgCA9IAAAgQADArAOAoIAEAJQAUA0AkAiQApAiA/APQAxAMBEAAQDbABCChdQBNg3AlhWQAphagXhWQgPg4gqgyIgagbQgegcgngYQgtgchBgYIhIgZIgBAAQACAOgEARg");
	this.shape_64.setTransform(124.5,126.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE831F").s().p("AhfFQQhEAAgxgMQg/gPgpgiQgkgigVg0IgDgJQgOgogDgrIAAggQACg+AVgyQAag5AugfQAegVAtgKQAYgGA5gHQAygIAegJQAsgOAagXQAhgcAIgrQAEgRgCgOIABAAIBIAZQBBAYAtAcQAmAYAeAcIAaAbQArAyAPA4QAXBVgpBbQglBWhNA3Qh/BcjXAAIgHAAgAAViDQgDASgOANQgLAKgTAFQgNAEgVAEQgZADgKACQgUAEgNAJQgTANgLAZQgKAWgBAaQAAAbAIAXQADAKAGAKQAHALAKAKQASAOAbAHQAVAFAdAAQBdAAA5goQAggXASgmQAQgmgJgkQgHgYgTgWQgRgUgXgOQgUgMgcgLIgBAAQgLgFgUgFQABAFgBAIg");
	this.shape_65.setTransform(132.1,122.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AEMA0IAAgBQAIgSgKgqQgIgsgagcQgZgcglgSQgdgOgpgLQhMgUhMgDQgCAAgCAAQgfAAgeABQgxADguAKQgSADgpALADaA0QgDgIgGgIQgNgPgkgJQgogLgNgKQgJgHgFgJQgOgVgEgbADaA0QgeAAgggEQgPgBgPAQQgEAEgHAJQgGAIgGAFQgNAKgZAFQgUAFgyAHAEKA4QgRAtgtAhQg2AphBABQg+AEg8geQgHgDgIgDQgjgTgegbQgNgKgLgNQgpgqgig8QgcgxgZhDQgBgCgBgCAEAA1QgTAAgTgBAA4AYQgBgdgfgXQgKgIgQgKQgXgMgFgEQgOgJgIgKQgFgGAAgFAgXAFQgMgCgRAAQgBAAgCAAQgXAAgNAFQgEAEgIAEQgIAEgGABIAAABQgMAAgQgGAgXAFQgPgFgOgFQgigPgfgRQgQgIgQgKQADgRgBgOQgDgQgLgHQgCgCgFgDQgFgCgCgCQgGgEgDgHQgCgCgEgKACcAwQhWgKhOgcQgIgDgHgCAiVg3Qg/gng7g1");
	this.shape_66.setTransform(72.4,68.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#159335").s().p("ADaByQgDgIgGgIQgNgPgkgKQgogLgNgKQgJgHgFgKQgOgVgEgZQAEAZAOAVQAFAKAJAHQANAKAoALQAkAKANAPQAGAIADAIQgegBgggEQhWgKhOgcIgPgEQgPgFgOgHQgigOgfgSQgQgIgQgKQACgKAAgKIAAgJQgDgQgLgHIgHgFIgHgEQgGgEgDgHIgGgMIAGAMQADAHAGAEIAHAEIAHAFQALAHADAQIAAAJQAAAKgCAKQg/gmg7g0QApgLASgEQAugJAxgEIA9gBIAEAAQBMAEBMATQApAMAdANQAlATAZAcQAaAbAIArQAKArgIASIAAABIgHAAIgDAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAIgmgBgAg0gTQAIAKAOAJIAcAOQAQALAKAHQAfAYABAdQgBgdgfgYQgKgHgQgLIgcgOQgOgJgIgKQgFgHAAgEQAAAEAFAHg");
	this.shape_67.setTransform(72.4,62.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0E802B").s().p("AhICIIgPgHQgjgTgegaQgNgLgLgMQgpgrgig7QgcgxgZhDIgCgEQA7A0A/AnQAQAKAQAIQAfASAiAOQAOAHAPAFQgMgDgRAAIgDAAQgXAAgNAGQgEADgIADQgIAFgGABIAAABQgMAAgQgHQAQAHAMAAIAAgBQAGgBAIgFQAIgDAEgDQANgGAXAAIADAAQARAAAMADIAPAEQBOAbBWAKIgBAAIAAAAIAAAAIgBAAQgOABgNANIAAAAIgBABIgLANQgGAJgGAFQgNAJgZAFQgUAFgyAHQAygHAUgFQAZgFANgJQAGgFAGgJIALgNIABgBIAAAAQANgNAOgBIABAAIAAAAIAAAAIABAAQAgAEAeABIAmABQAAAAAAgBQAAAAAAAAQABAAAAAAQAAgBABAAIADAAIAHAAIARAAIAFgBIAFgBIAOAAQAGAAAFgCIAQgFIADgBQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAAAQgBABAAAAIgCABQgHAFgKABQgJACgKAAIgGAAIgGABIgFABIgUAAQgRAtgtAhQg2AohBACIgNAAQg2AAg3gagAB5AhIAAAAg");
	this.shape_68.setTransform(75.9,70.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FF0000").s().p("AhZHPQgfgBgQgKQgRgLgGgPQgGgPAAgNQAAgSAJgWQAJgWAXgQQAWgPApgBQAtABAUASQAUARAAAaQAAAXgJAWQgIAWgYAPQgYAOgvABIgBAAgAiSDkIgEhHIgFhCQgCghAAgnQACgjAVgYQAWgYAggTQAhgTAhgVQAggVAXgcQAXgbADgqQgFgggYgSQgagTgjgHQgkgHgnAAQgdAAgbADQgcABgTADIACgOIACgQIAFgdIAEgfIAGgqQAdgGAegDQAegEAeAAQBEgBA9AXQA8AVAmA0QAlAzABBYQgBA5gXAoQgXAogiAbQgiAcgiATQgiATgWARQgXARgBASQAAAOACAgIAGBCIACA+g");
	this.shape_69.setTransform(158.7,121.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AsJMJQlClBAAnHQAAnIFClCQEekeGHggQAsgDAtgBIAMAAQFwAAEaDTQBCAxA9A+QC2C2BPDgQA9CuAADGQAAHHlCFBQlBFDnIAAQnIAAlClDg");
	this.shape_70.setTransform(109.8,111.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70},{t:this.shape_69}]}).wait(1));

	// Layer 1
	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("rgba(51,51,51,0.859)").s().p("AtLNMQldleAAnuQAAntFdleQFeldHtAAQHvAAFdFdQFdFeAAHtQAAHuldFeQldFdnvAAQntAAleldg");
	this.shape_71.setTransform(109.2,112.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_71).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy2, new cjs.Rectangle(-10.1,-6.9,238.6,238.7), null);


(lib.quscopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjjMNQlElDAAnKQAAnJFElDQFClDHJgBMAAAAihQnJgBlClDg");
	mask.setTransform(55.3,110.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(153.3,92.6,0.536,0.536,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(143.5,89.1,0.536,0.536,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(106.6,103.6,0.536,0.536,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(129.5,92.6,0.536,0.536,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(123.8,82.2,0.536,0.536,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(129.6,87.3,0.536,0.536,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(120.3,127.1,0.536,0.536,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(129.7,128.8,0.536,0.536,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(126.3,121.2,0.536,0.536,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(137.9,115.3,0.536,0.536,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(139.6,129,0.536,0.536,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(113.5,147.5,0.536,0.536,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(133.5,150.9,0.536,0.536,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(162.6,120.1,0.536,0.536,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(160.3,107.9,0.536,0.536,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(144.5,102.4,0.536,0.536,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(147.6,111.1,0.536,0.536,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(153.2,132.7,0.536,0.536,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(150.5,143.1,0.536,0.536,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(123.6,146.9,0.536,0.536,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(105.8,137.5,0.536,0.536,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(103.8,117.2,0.536,0.536,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(153.7,170,0.536,0.536,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(135.1,177.4,0.536,0.536,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(96.4,172.2,0.536,0.536,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(109.1,176.4,0.536,0.536,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(124.1,178.5,0.536,0.536,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(111.2,171.9,0.536,0.536,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(91.7,165.6,0.536,0.536,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(80.4,157.7,0.536,0.536,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(72.5,143.8,0.536,0.536,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(74.9,151.6,0.536,0.536,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(70.6,133.2,0.536,0.536,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(80.2,151.2,0.536,0.536,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(73.5,138.8,0.536,0.536,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(70.9,128.3,0.536,0.536,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(125.3,159.7,0.536,0.536,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(122.3,167.2,0.536,0.536,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(114,161.5,0.536,0.536,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(136.3,166.7,0.536,0.536,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(146.6,165.9,0.536,0.536,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(148.7,156.2,0.536,0.536,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(156.3,158.4,0.536,0.536,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(157.8,150.5,0.536,0.536,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(167.7,148.2,0.536,0.536,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(171.9,139.1,0.536,0.536,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(102.1,156.2,0.536,0.536,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(93.4,153.3,0.536,0.536,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(87,147.8,0.536,0.536,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(90,139,0.536,0.536,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(81.2,129.5,0.536,0.536,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(90.4,127.5,0.536,0.536,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(87.5,111.1,0.536,0.536,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(78.9,109.2,0.536,0.536,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(93.6,107.5,0.536,0.536,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(83.9,100.4,0.536,0.536,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(97.1,98.3,0.536,0.536,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(91.9,92.8,0.536,0.536,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F3964B").s().p("AhaCMQgbgHgSgOQgKgKgHgLQgGgKgDgKQgIgYAAgbQABgZAKgXQALgYATgNQANgJAUgEQAKgCAZgDQAVgEANgEQATgFALgLQAOgMADgSQABgIgBgGQAUAGALAFIABAAQAcALAUAMQAXAOARAUQATAWAHAYQAJAjgQAnQgSAmggAXQg5AnhdAAQgdAAgVgEg");
	this.shape_58.setTransform(132.1,122.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#5D2408").s().p("AgKAoIgWgHIgSgDQgKgDgHgEQgIgEgGgIQgFgIAAgHQAAgKAHgHQAFgEAUgFQAWgFAXgFIADALIACAIIABAGIADAPIABAEQADACAGgEQAGgCACgGQACgEADgDQACgBAIAAIAAAAQAEgEgLgZIAfABQARAAAFAJIAIANQAAAGgBAFIgDANQgBAKgFAHQgIAJgUAHQgVAHgPAAQgKAAgNgEg");
	this.shape_59.setTransform(110.4,92.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#A33D0C").s().p("AgQBDIgmgLIgegHQgQgFgMgGQgNgHgJgMQgKgNAAgOQAAgQALgLQAJgIASgEQAtgOAugGQAFASAEAUIgDgLQgXAEgWAGQgUAFgFAEQgHAHAAAKQAAAHAFAIQAGAIAIAEQAHAEAKACIASAEIAWAHQANAEAKAAQAPAAAVgIQAUgGAIgKQAFgGABgKIADgNQABgGAAgFIgIgNQgFgJgRAAIgfgBIgBgHIgIgVQAmgDAoADQAcAAAJAPQAKARACAGQACAHgDAKIgFAXQgCAQgJALQgNARggALQgkALgZAAQgQAAgVgGg");
	this.shape_60.setTransform(110.4,92.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#692B0E").s().p("ABxDDIgwiJQgmhsg5g4Qg7g4gPggQgJgSgdgbQAKgRABgHQBHApA0BbIAKAUQAeA4AiBgIARAvIAbBMIAQArIAIAWIABAGQALAagEADQgPgfgOgmg");
	this.shape_61.setTransform(99.5,64.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#4A1D08").s().p("AB2EGIgBgGIgEgPIAAgGIgCgIQgEgTgGgTQgFgUgHgUQgQgsgLgYIgFgLQgKgWgLgUQgLgYgPgVQg4hbhOhIQgMgLgEgFQgIgKgBgKQgCgOAMgVIAGgKQAdAaAJATQAPAgA6A4QA6A4AlBsIAwCIQAPAnAOAfIAAAAQgHAAgDABQgDADgBAFQgCAGgGADQgFACgDAAIgCAAg");
	this.shape_62.setTransform(98.7,66.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#CD4A0B").s().p("AjTFoQhSgkhEhCQhlheglh5QgGgVgFgWQgJgtgBgvIAAgMQAAisBoiEQgrBeAABtQAAA7AMA2IAKAkQAJAcANAcQAoBWBLBIQBFBABRAmQBqAwB/AAQDhAACeiWQAegcAZggQgnBYhNBIQifCXjhAAQh/AAhpgxg");
	this.shape_63.setTransform(119.3,141.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EC5913").s().p("AjKGgQhQgmhFhAQhMhJgohXQgNgbgJgdIgKgkQgMg2AAg6QABhtAqhdQAZgfAegdQAwgtA1ggQAggSAhgNQAmgQApgKQAHAUAFAUQguAGgtAOQgSAEgJAIQgLAMAAAQQAAAPAKANQAJALAOAIQALAFARAFIAdAIIAmAKQAWAHARAAQAYAAAkgMQAggLANgRQAJgLACgPIAEgYQAEgLgDgGQgBgHgJgQQgKgQgbAAQgogCgnACIgQgrQA1gKA4AAQBUAABKAXQB9AiBkBeQAOANAMAOQCFCOAADCQAABtgrBeQgYAfgeAdQifCWjhAAQh+AAhrgwgAB9lTQgIArghAcQgbAXgsAOQgdAJgyAIQg4AHgZAGQgtAKgeAVQguAfgaA5QgVAzgCA9IAAAgQADArAOAoIAEAJQAUA0AkAiQApAiA/APQAxAMBEAAQDbABCChdQBNg3AlhWQAphagXhWQgPg4gqgyIgagbQgegcgngYQgtgchBgYIhIgZIgBAAQACAOgEARg");
	this.shape_64.setTransform(124.5,126.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE831F").s().p("AhfFQQhEAAgxgMQg/gPgpgiQgkgigVg0IgDgJQgOgogDgrIAAggQACg+AVgyQAag5AugfQAegVAtgKQAYgGA5gHQAygIAegJQAsgOAagXQAhgcAIgrQAEgRgCgOIABAAIBIAZQBBAYAtAcQAmAYAeAcIAaAbQArAyAPA4QAXBVgpBbQglBWhNA3Qh/BcjXAAIgHAAgAAViDQgDASgOANQgLAKgTAFQgNAEgVAEQgZADgKACQgUAEgNAJQgTANgLAZQgKAWgBAaQAAAbAIAXQADAKAGAKQAHALAKAKQASAOAbAHQAVAFAdAAQBdAAA5goQAggXASgmQAQgmgJgkQgHgYgTgWQgRgUgXgOQgUgMgcgLIgBAAQgLgFgUgFQABAFgBAIg");
	this.shape_65.setTransform(132.1,122.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AEMA0IAAgBQAIgSgKgqQgIgsgagcQgZgcglgSQgdgOgpgLQhMgUhMgDQgCAAgCAAQgfAAgeABQgxADguAKQgSADgpALADaA0QgDgIgGgIQgNgPgkgJQgogLgNgKQgJgHgFgJQgOgVgEgbADaA0QgeAAgggEQgPgBgPAQQgEAEgHAJQgGAIgGAFQgNAKgZAFQgUAFgyAHAEKA4QgRAtgtAhQg2AphBABQg+AEg8geQgHgDgIgDQgjgTgegbQgNgKgLgNQgpgqgig8QgcgxgZhDQgBgCgBgCAEAA1QgTAAgTgBAA4AYQgBgdgfgXQgKgIgQgKQgXgMgFgEQgOgJgIgKQgFgGAAgFAgXAFQgMgCgRAAQgBAAgCAAQgXAAgNAFQgEAEgIAEQgIAEgGABIAAABQgMAAgQgGAgXAFQgPgFgOgFQgigPgfgRQgQgIgQgKQADgRgBgOQgDgQgLgHQgCgCgFgDQgFgCgCgCQgGgEgDgHQgCgCgEgKACcAwQhWgKhOgcQgIgDgHgCAiVg3Qg/gng7g1");
	this.shape_66.setTransform(72.4,68.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#159335").s().p("ADaByQgDgIgGgIQgNgPgkgKQgogLgNgKQgJgHgFgKQgOgVgEgZQAEAZAOAVQAFAKAJAHQANAKAoALQAkAKANAPQAGAIADAIQgegBgggEQhWgKhOgcIgPgEQgPgFgOgHQgigOgfgSQgQgIgQgKQACgKAAgKIAAgJQgDgQgLgHIgHgFIgHgEQgGgEgDgHIgGgMIAGAMQADAHAGAEIAHAEIAHAFQALAHADAQIAAAJQAAAKgCAKQg/gmg7g0QApgLASgEQAugJAxgEIA9gBIAEAAQBMAEBMATQApAMAdANQAlATAZAcQAaAbAIArQAKArgIASIAAABIgHAAIgDAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAIgmgBgAg0gTQAIAKAOAJIAcAOQAQALAKAHQAfAYABAdQgBgdgfgYQgKgHgQgLIgcgOQgOgJgIgKQgFgHAAgEQAAAEAFAHg");
	this.shape_67.setTransform(72.4,62.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0E802B").s().p("AhICIIgPgHQgjgTgegaQgNgLgLgMQgpgrgig7QgcgxgZhDIgCgEQA7A0A/AnQAQAKAQAIQAfASAiAOQAOAHAPAFQgMgDgRAAIgDAAQgXAAgNAGQgEADgIADQgIAFgGABIAAABQgMAAgQgHQAQAHAMAAIAAgBQAGgBAIgFQAIgDAEgDQANgGAXAAIADAAQARAAAMADIAPAEQBOAbBWAKIgBAAIAAAAIAAAAIgBAAQgOABgNANIAAAAIgBABIgLANQgGAJgGAFQgNAJgZAFQgUAFgyAHQAygHAUgFQAZgFANgJQAGgFAGgJIALgNIABgBIAAAAQANgNAOgBIABAAIAAAAIAAAAIABAAQAgAEAeABIAmABQAAAAAAgBQAAAAAAAAQABAAAAAAQAAgBABAAIADAAIAHAAIARAAIAFgBIAFgBIAOAAQAGAAAFgCIAQgFIADgBQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAAAQgBABAAAAIgCABQgHAFgKABQgJACgKAAIgGAAIgGABIgFABIgUAAQgRAtgtAhQg2AohBACIgNAAQg2AAg3gagAB5AhIAAAAg");
	this.shape_68.setTransform(75.9,70.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AsJMJQlClBAAnHQAAnIFClCQEekeGHggQAsgDAtgBIAMAAQFwAAEaDTQBCAxA9A+QC2C2BPDgQA9CuAADGQAAHHlCFBQlBFDnIAAQnIAAlClDg");
	this.shape_69.setTransform(109.8,111.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_69).wait(1));

	// Layer 1
	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("rgba(51,51,51,0.859)").s().p("AtLNMQldleAAnuQAAntFdleQFeldHtAAQHvAAFdFdQFdFeAAHtQAAHuldFeQldFdnvAAQntAAleldg");
	this.shape_70.setTransform(109.2,112.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_70).wait(1));

}).prototype = getMCSymbolPrototype(lib.quscopy, new cjs.Rectangle(-10.1,-6.9,238.6,238.7), null);


(lib.qus = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjjMNQlElDAAnKQAAnJFElDQFClDHJgBMAAAAihQnJgBlClDg");
	mask.setTransform(55.3,110.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(153.3,92.6,0.536,0.536,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(143.5,89.1,0.536,0.536,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(106.6,103.6,0.536,0.536,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(129.5,92.6,0.536,0.536,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(123.8,82.2,0.536,0.536,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(129.6,87.3,0.536,0.536,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(120.3,127.1,0.536,0.536,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(129.7,128.8,0.536,0.536,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(126.3,121.2,0.536,0.536,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(137.9,115.3,0.536,0.536,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(139.6,129,0.536,0.536,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(113.5,147.5,0.536,0.536,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(133.5,150.9,0.536,0.536,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(162.6,120.1,0.536,0.536,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(160.3,107.9,0.536,0.536,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(144.5,102.4,0.536,0.536,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(147.6,111.1,0.536,0.536,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(153.2,132.7,0.536,0.536,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(150.5,143.1,0.536,0.536,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(123.6,146.9,0.536,0.536,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(105.8,137.5,0.536,0.536,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(103.8,117.2,0.536,0.536,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(153.7,170,0.536,0.536,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(135.1,177.4,0.536,0.536,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(96.4,172.2,0.536,0.536,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(109.1,176.4,0.536,0.536,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(124.1,178.5,0.536,0.536,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(111.2,171.9,0.536,0.536,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(91.7,165.6,0.536,0.536,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(80.4,157.7,0.536,0.536,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(72.5,143.8,0.536,0.536,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(74.9,151.6,0.536,0.536,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(70.6,133.2,0.536,0.536,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(80.2,151.2,0.536,0.536,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(73.5,138.8,0.536,0.536,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(70.9,128.3,0.536,0.536,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(125.3,159.7,0.536,0.536,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(122.3,167.2,0.536,0.536,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(114,161.5,0.536,0.536,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(136.3,166.7,0.536,0.536,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(146.6,165.9,0.536,0.536,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(148.7,156.2,0.536,0.536,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(156.3,158.4,0.536,0.536,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(157.8,150.5,0.536,0.536,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(167.7,148.2,0.536,0.536,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(171.9,139.1,0.536,0.536,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(102.1,156.2,0.536,0.536,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(93.4,153.3,0.536,0.536,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(87,147.8,0.536,0.536,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(90,139,0.536,0.536,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(81.2,129.5,0.536,0.536,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(90.4,127.5,0.536,0.536,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(87.5,111.1,0.536,0.536,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(78.9,109.2,0.536,0.536,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(93.6,107.5,0.536,0.536,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(83.9,100.4,0.536,0.536,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(97.1,98.3,0.536,0.536,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(91.9,92.8,0.536,0.536,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F3964B").s().p("AhaCMQgbgHgSgOQgKgKgHgLQgGgKgDgKQgIgYAAgbQABgZAKgXQALgYATgNQANgJAUgEQAKgCAZgDQAVgEANgEQATgFALgLQAOgMADgSQABgIgBgGQAUAGALAFIABAAQAcALAUAMQAXAOARAUQATAWAHAYQAJAjgQAnQgSAmggAXQg5AnhdAAQgdAAgVgEg");
	this.shape_58.setTransform(132.1,122.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#5D2408").s().p("AgKAoIgWgHIgSgDQgKgDgHgEQgIgEgGgIQgFgIAAgHQAAgKAHgHQAFgEAUgFQAWgFAXgFIADALIACAIIABAGIADAPIABAEQADACAGgEQAGgCACgGQACgEADgDQACgBAIAAIAAAAQAEgEgLgZIAfABQARAAAFAJIAIANQAAAGgBAFIgDANQgBAKgFAHQgIAJgUAHQgVAHgPAAQgKAAgNgEg");
	this.shape_59.setTransform(110.4,92.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#A33D0C").s().p("AgQBDIgmgLIgegHQgQgFgMgGQgNgHgJgMQgKgNAAgOQAAgQALgLQAJgIASgEQAtgOAugGQAFASAEAUIgDgLQgXAEgWAGQgUAFgFAEQgHAHAAAKQAAAHAFAIQAGAIAIAEQAHAEAKACIASAEIAWAHQANAEAKAAQAPAAAVgIQAUgGAIgKQAFgGABgKIADgNQABgGAAgFIgIgNQgFgJgRAAIgfgBIgBgHIgIgVQAmgDAoADQAcAAAJAPQAKARACAGQACAHgDAKIgFAXQgCAQgJALQgNARggALQgkALgZAAQgQAAgVgGg");
	this.shape_60.setTransform(110.4,92.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#692B0E").s().p("ABxDDIgwiJQgmhsg5g4Qg7g4gPggQgJgSgdgbQAKgRABgHQBHApA0BbIAKAUQAeA4AiBgIARAvIAbBMIAQArIAIAWIABAGQALAagEADQgPgfgOgmg");
	this.shape_61.setTransform(99.5,64.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#4A1D08").s().p("AB2EGIgBgGIgEgPIAAgGIgCgIQgEgTgGgTQgFgUgHgUQgQgsgLgYIgFgLQgKgWgLgUQgLgYgPgVQg4hbhOhIQgMgLgEgFQgIgKgBgKQgCgOAMgVIAGgKQAdAaAJATQAPAgA6A4QA6A4AlBsIAwCIQAPAnAOAfIAAAAQgHAAgDABQgDADgBAFQgCAGgGADQgFACgDAAIgCAAg");
	this.shape_62.setTransform(98.7,66.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#CD4A0B").s().p("AjTFoQhSgkhEhCQhlheglh5QgGgVgFgWQgJgtgBgvIAAgMQAAisBoiEQgrBeAABtQAAA7AMA2IAKAkQAJAcANAcQAoBWBLBIQBFBABRAmQBqAwB/AAQDhAACeiWQAegcAZggQgnBYhNBIQifCXjhAAQh/AAhpgxg");
	this.shape_63.setTransform(119.3,141.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#EC5913").s().p("AjKGgQhQgmhFhAQhMhJgohXQgNgbgJgdIgKgkQgMg2AAg6QABhtAqhdQAZgfAegdQAwgtA1ggQAggSAhgNQAmgQApgKQAHAUAFAUQguAGgtAOQgSAEgJAIQgLAMAAAQQAAAPAKANQAJALAOAIQALAFARAFIAdAIIAmAKQAWAHARAAQAYAAAkgMQAggLANgRQAJgLACgPIAEgYQAEgLgDgGQgBgHgJgQQgKgQgbAAQgogCgnACIgQgrQA1gKA4AAQBUAABKAXQB9AiBkBeQAOANAMAOQCFCOAADCQAABtgrBeQgYAfgeAdQifCWjhAAQh+AAhrgwgAB9lTQgIArghAcQgbAXgsAOQgdAJgyAIQg4AHgZAGQgtAKgeAVQguAfgaA5QgVAzgCA9IAAAgQADArAOAoIAEAJQAUA0AkAiQApAiA/APQAxAMBEAAQDbABCChdQBNg3AlhWQAphagXhWQgPg4gqgyIgagbQgegcgngYQgtgchBgYIhIgZIgBAAQACAOgEARg");
	this.shape_64.setTransform(124.5,126.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE831F").s().p("AhfFQQhEAAgxgMQg/gPgpgiQgkgigVg0IgDgJQgOgogDgrIAAggQACg+AVgyQAag5AugfQAegVAtgKQAYgGA5gHQAygIAegJQAsgOAagXQAhgcAIgrQAEgRgCgOIABAAIBIAZQBBAYAtAcQAmAYAeAcIAaAbQArAyAPA4QAXBVgpBbQglBWhNA3Qh/BcjXAAIgHAAgAAViDQgDASgOANQgLAKgTAFQgNAEgVAEQgZADgKACQgUAEgNAJQgTANgLAZQgKAWgBAaQAAAbAIAXQADAKAGAKQAHALAKAKQASAOAbAHQAVAFAdAAQBdAAA5goQAggXASgmQAQgmgJgkQgHgYgTgWQgRgUgXgOQgUgMgcgLIgBAAQgLgFgUgFQABAFgBAIg");
	this.shape_65.setTransform(132.1,122.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AEMA0IAAgBQAIgSgKgqQgIgsgagcQgZgcglgSQgdgOgpgLQhMgUhMgDQgCAAgCAAQgfAAgeABQgxADguAKQgSADgpALADaA0QgDgIgGgIQgNgPgkgJQgogLgNgKQgJgHgFgJQgOgVgEgbADaA0QgeAAgggEQgPgBgPAQQgEAEgHAJQgGAIgGAFQgNAKgZAFQgUAFgyAHAEKA4QgRAtgtAhQg2AphBABQg+AEg8geQgHgDgIgDQgjgTgegbQgNgKgLgNQgpgqgig8QgcgxgZhDQgBgCgBgCAEAA1QgTAAgTgBAA4AYQgBgdgfgXQgKgIgQgKQgXgMgFgEQgOgJgIgKQgFgGAAgFAgXAFQgMgCgRAAQgBAAgCAAQgXAAgNAFQgEAEgIAEQgIAEgGABIAAABQgMAAgQgGAgXAFQgPgFgOgFQgigPgfgRQgQgIgQgKQADgRgBgOQgDgQgLgHQgCgCgFgDQgFgCgCgCQgGgEgDgHQgCgCgEgKACcAwQhWgKhOgcQgIgDgHgCAiVg3Qg/gng7g1");
	this.shape_66.setTransform(72.4,68.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#159335").s().p("ADaByQgDgIgGgIQgNgPgkgKQgogLgNgKQgJgHgFgKQgOgVgEgZQAEAZAOAVQAFAKAJAHQANAKAoALQAkAKANAPQAGAIADAIQgegBgggEQhWgKhOgcIgPgEQgPgFgOgHQgigOgfgSQgQgIgQgKQACgKAAgKIAAgJQgDgQgLgHIgHgFIgHgEQgGgEgDgHIgGgMIAGAMQADAHAGAEIAHAEIAHAFQALAHADAQIAAAJQAAAKgCAKQg/gmg7g0QApgLASgEQAugJAxgEIA9gBIAEAAQBMAEBMATQApAMAdANQAlATAZAcQAaAbAIArQAKArgIASIAAABIgHAAIgDAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAIgmgBgAg0gTQAIAKAOAJIAcAOQAQALAKAHQAfAYABAdQgBgdgfgYQgKgHgQgLIgcgOQgOgJgIgKQgFgHAAgEQAAAEAFAHg");
	this.shape_67.setTransform(72.4,62.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0E802B").s().p("AhICIIgPgHQgjgTgegaQgNgLgLgMQgpgrgig7QgcgxgZhDIgCgEQA7A0A/AnQAQAKAQAIQAfASAiAOQAOAHAPAFQgMgDgRAAIgDAAQgXAAgNAGQgEADgIADQgIAFgGABIAAABQgMAAgQgHQAQAHAMAAIAAgBQAGgBAIgFQAIgDAEgDQANgGAXAAIADAAQARAAAMADIAPAEQBOAbBWAKIgBAAIAAAAIAAAAIgBAAQgOABgNANIAAAAIgBABIgLANQgGAJgGAFQgNAJgZAFQgUAFgyAHQAygHAUgFQAZgFANgJQAGgFAGgJIALgNIABgBIAAAAQANgNAOgBIABAAIAAAAIAAAAIABAAQAgAEAeABIAmABQAAAAAAgBQAAAAAAAAQABAAAAAAQAAgBABAAIADAAIAHAAIARAAIAFgBIAFgBIAOAAQAGAAAFgCIAQgFIADgBQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAAAQgBABAAAAIgCABQgHAFgKABQgJACgKAAIgGAAIgGABIgFABIgUAAQgRAtgtAhQg2AohBACIgNAAQg2AAg3gagAB5AhIAAAAg");
	this.shape_68.setTransform(75.9,70.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FF0000").s().p("AhZHPQgfgBgQgKQgRgLgGgPQgGgPAAgNQAAgSAJgWQAJgWAXgQQAWgPApgBQAtABAUASQAUARAAAaQAAAXgJAWQgIAWgYAPQgYAOgvABIgBAAgAiSDkIgEhHIgFhCQgCghAAgnQACgjAVgYQAWgYAggTQAhgTAhgVQAggVAXgcQAXgbADgqQgFgggYgSQgagTgjgHQgkgHgnAAQgdAAgbADQgcABgTADIACgOIACgQIAFgdIAEgfIAGgqQAdgGAegDQAegEAeAAQBEgBA9AXQA8AVAmA0QAlAzABBYQgBA5gXAoQgXAogiAbQgiAcgiATQgiATgWARQgXARgBASQAAAOACAgIAGBCIACA+g");
	this.shape_69.setTransform(158.7,121.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AsJMJQlClBAAnHQAAnIFClCQEekeGHggQAsgDAtgBIAMAAQFwAAEaDTQBCAxA9A+QC2C2BPDgQA9CuAADGQAAHHlCFBQlBFDnIAAQnIAAlClDg");
	this.shape_70.setTransform(109.8,111.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70},{t:this.shape_69}]}).wait(1));

	// Layer 1
	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("rgba(51,51,51,0.859)").s().p("AtLNMQldleAAnuQAAntFdleQFeldHtAAQHvAAFdFdQFdFeAAHtQAAHuldFeQldFdnvAAQntAAleldg");
	this.shape_71.setTransform(109.2,112.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_71).wait(1));

}).prototype = getMCSymbolPrototype(lib.qus, new cjs.Rectangle(-10.1,-6.9,238.6,238.7), null);


(lib.InstructionText_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgjA4IABggIAAgZIAAgIIgdgBIAAgYIAeAAQABgNAFgNQADgMAIgKQAHgJAKgGQALgGAPAAQAJAAAJADQAKADAJAIIgMAWQgIgFgGgCIgLgDQgHAAgEABQgGABgDADQgFACgCAFQgDAFgCAIQgCAHAAALIA0ABIgCAZIg0gBIAAAKIAAAiIAAAQIAAARIAAASIAAARIgaABIAAgwg");
	this.shape.setTransform(216.9,-15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZBFQgNgFgJgKQgJgKgGgOQgFgOAAgQQAAgQAFgOQAGgOAJgJQAJgKANgGQAMgEANgBQAOAAAMAGQANAFAJALQAJAKAGANQAFAOAAAPQAAAQgFANQgGAOgJAKQgJAKgNAGQgMAFgOAAQgNAAgMgFgAgSgvQgIAFgFAIQgEAHgDAKQgCAIAAAJQAAAJACAJQADAKAFAHQAFAHAIAFQAIAFAJgBQAKABAJgFQAHgFAFgHQAGgHACgKQACgJAAgJQAAgJgCgIQgDgKgFgHQgFgIgHgFQgIgEgLAAQgKAAgIAEg");
	this.shape_1.setTransform(202.2,-11.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgjA4IABggIAAgZIAAgIIgdgBIAAgYIAeAAQABgNAFgNQAEgMAHgKQAHgJAKgGQAMgGAPAAQAIAAAKADQAJADAJAIIgMAWQgIgFgGgCIgLgDQgHAAgEABQgGABgDADQgFACgCAFQgCAFgDAIQgCAHAAALIA1ABIgDAZIg0gBIAAAKIAAAiIAAAQIAAARIAAASIAAARIgaABIAAgwg");
	this.shape_2.setTransform(183,-15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAIBvQgHgDgEgFQgFgFgEgFIgGgMQgHgPgBgSIADihIAaAAIgBAqIgBAiIgBAcIAAAVIAAAiQAAALACAKQABAEADAEIAFAHIAHAEQAEACAFAAIgDAZQgJAAgHgCg");
	this.shape_3.setTransform(173.2,-16.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAoBIIABgSQgQAKgPAFQgNAFgNAAQgKAAgIgDQgIgDgHgEQgGgFgFgIQgDgHgBgLQAAgKADgIQAEgIAFgGQAFgFAIgFQAGgFAJgCQAJgDAIgBQAJgCAIABIAQAAIANABIgEgLQgDgHgDgFQgEgFgGgDQgFgDgHAAIgKABQgHACgIAEQgHAEgKAGQgIAHgLAKIgPgSQAMgLALgIQAMgHAJgEQAKgEAIgCIAPgBQALAAAJADQAKAFAHAHQAGAGAFAKQAFAJADALQADALACALQABAKAAAMIgBAaIgFAfgAgGAAQgKACgIAFQgIAFgEAIQgDAHACAJQACAIAFADQAFADAHAAQAIAAAJgCIAQgHIAPgIIANgHIAAgMIAAgNIgOgDIgNgBQgLABgLACg");
	this.shape_4.setTransform(160.4,-12.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAiBhQAEgMABgMIADgVIAAgUQgBgOgEgJQgEgJgGgGQgGgGgHgCQgHgDgHAAQgGAAgHAEQgHADgHAHQgHAFgGAMIgBBUIgZAAIgCjDIAdgBIgBBOQAHgIAIgEIAPgHQAIgDAHAAQAOAAALAFQAMAFAIAJQAJAKAFANQAFANAAARIAAATIgBASIgCARIgDAPg");
	this.shape_5.setTransform(144.2,-15);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgbBqIgMgEQgGgDgGgEIgKgKQgFgGgDgIIAZgKQADAFADAEQADAEAEADIAIAEIAIADQAKACAJgCQAJgBAGgEQAHgEAEgEIAIgKIAFgJIACgJIAAgGIAAgQQgHAHgJAEIgPAGQgIACgHAAQgOAAgNgFQgNgFgKgKQgJgJgFgNQgFgOgBgRQAAgRAHgOQAFgNAKgKQAKgJAMgFQANgFAMAAQAHABAJADIAQAHQAJAEAHAIIAAgZIAaABIgBCQQAAAJgCAIQgDAIgDAIQgEAHgGAHQgHAHgHAFQgIAGgKADQgJADgLABIgDAAQgNAAgMgDgAgShMQgIADgGAHQgFAHgEAJQgDAJAAAKQAAAKADAJQAEAJAFAFQAGAHAIADQAIAEAJAAQAIAAAIgDQAHgDAGgFQAHgFAEgHQAFgHABgJIAAgRQgBgJgFgHQgEgIgHgFQgGgFgIgDQgJgDgGAAQgKAAgHAEg");
	this.shape_6.setTransform(119.7,-9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag7AgIgBggIAAgYIgBgSIgBgbIAdgBIABAlQAEgHAFgHQAFgGAHgFQAGgFAGgDQAHgDAIgBQAJAAAIACQAHACAFAEQAFAFADAFIAFAMIADALIABALIABArIgBAuIgagBIACgpQAAgVgBgTIAAgGIgBgIIgDgIQgCgEgDgDQgDgDgFgCQgEgBgGABQgKABgJAOQgLANgMAYIABAhIABASIAAAKIgbADIgCgng");
	this.shape_7.setTransform(103.5,-12);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPBfIAAggIABgoIABg2IAZgBIgBAhIAAAbIgBAWIAAASIAAAbgAgGg8IgGgDIgEgGIgBgHQAAgEABgEIAEgFIAGgEIAGgCQAEABADABIAGAEIAEAFQABAEAAAEIgBAHIgEAGIgGADIgHABIgGgBg");
	this.shape_8.setTransform(92.4,-15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAiBhQAEgMABgMIADgVIAAgUQgBgOgEgJQgEgJgGgGQgGgGgHgCQgHgDgHAAQgGAAgHAEQgHADgHAHQgHAFgGAMIgBBUIgZAAIgCjDIAdgBIgBBOQAHgIAIgEIAPgHQAIgDAHAAQAOAAALAFQAMAFAIAJQAJAKAFANQAFANAAARIAAATIgBASIgCARIgDAPg");
	this.shape_9.setTransform(80.6,-15);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgXBEQgNgFgLgKQgKgKgGgNQgHgOABgQQgBgIADgJQADgKAEgIQAFgIAHgHQAGgHAJgFQAIgFAKgDQAJgDAKAAQALAAAKADQAKACAJAFQAIAFAHAIQAGAHAFAJIABABIgWAMIAAAAQgEgHgEgFQgFgFgGgEQgFgDgHgCQgHgCgHAAQgJAAgKAEQgJAEgGAHQgHAGgEAKQgEAJAAAJQAAAKAEAKQAEAJAHAHQAGAGAJAEQAKAEAJAAQAHAAAGgBIAMgFQAGgEAEgEQAFgFADgGIABAAIAWAMIAAABQgFAIgIAHQgGAGgJAFQgIAFgKACQgKADgKAAQgNAAgOgGg");
	this.shape_10.setTransform(64.1,-11.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgPgOIgsABIABgXIAsgBIABg4IAYgCIgBA5IAygBIgDAXIgvACIgBBtIgaABg");
	this.shape_11.setTransform(50,-14.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAoBIIABgSQgPAKgQAFQgNAFgNAAQgJAAgJgDQgIgDgHgEQgGgFgEgIQgFgHAAgLQAAgKADgIQADgIAGgGQAFgFAIgFQAGgFAJgCQAJgDAIgBQAJgCAIABIAQAAIANABIgEgLQgDgHgDgFQgEgFgGgDQgFgDgIAAIgJABQgHACgIAEQgHAEgKAGQgIAHgLAKIgPgSQAMgLALgIQAMgHAJgEQAKgEAIgCIAPgBQAMAAAIADQAKAFAHAHQAGAGAGAKQAEAJADALQADALACALQABAKAAAMIgBAaIgFAfgAgFAAQgLACgIAFQgHAFgFAIQgEAHADAJQACAIAFADQAFADAHAAQAHAAAJgCIARgHIAPgIIANgHIAAgMIgBgNIgMgDIgOgBQgMABgJACg");
	this.shape_12.setTransform(35.4,-12.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA9AeIAAgbIAAgUQgBgLgDgEQgDgGgFAAQgDAAgDACIgHAGIgGAHIgGAHIgFAIIgEAHIAAALIABAPIAAATIAAAZIgYAAIAAgnIgBgbIgBgUQgBgLgDgEQgDgGgFAAQgDAAgDACIgHAHIgIAHIgGAJIgFAJIgFAFIABBEIgZAAIgEiHIAbgCIAAAkIAJgKIAKgKQAFgEAHgDQAFgDAIAAIAKABQAFACADAEQAEADADAGQADAFAAAIIAJgKIAJgKQAGgEAGgCQAFgDAHAAQAGAAAFABQAGADAEADQAEAEADAHQADAGAAAJIABAXIACAeIAAAtIgbAAIAAgng");
	this.shape_13.setTransform(17.1,-12.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgOgOIgsABIAAgXIAsgBIABg4IAYgCIgBA5IAxgBIgBAXIgwACIgBBtIgaABg");
	this.shape_14.setTransform(-6.4,-14.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgXBEQgNgFgKgKQgLgKgGgNQgHgOAAgQQABgIACgJQADgKAEgIQAFgIAHgHQAGgHAJgFQAIgFAKgDQAJgDAKAAQALAAAKADQAKACAJAFQAIAFAHAIQAGAHAFAJIABABIgWAMIAAAAQgDgHgFgFQgEgFgHgEQgFgDgHgCQgHgCgHAAQgJAAgKAEQgJAEgGAHQgHAGgEAKQgEAJAAAJQAAAKAEAKQAEAJAHAHQAGAGAJAEQAKAEAJAAQAHAAAGgBIAMgFQAGgEAEgEQAFgFADgGIABAAIAWAMIAAABQgFAIgIAHQgGAGgJAFQgJAFgJACQgKADgKAAQgNAAgOgGg");
	this.shape_15.setTransform(-20.4,-11.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAnBIIACgSQgPAKgPAFQgOAFgNAAQgJAAgJgDQgIgDgHgEQgHgFgDgIQgEgHgBgLQABgKADgIQADgIAFgGQAFgFAHgFQAIgFAIgCQAIgDAKgBQAHgCAKABIAPAAIANABIgEgLQgCgHgEgFQgEgFgFgDQgGgDgIAAIgKABQgGACgHAEQgJAEgIAGQgKAHgJAKIgQgSQAMgLALgIQALgHALgEQAJgEAJgCIAOgBQALAAAKADQAJAFAHAHQAHAGAFAKQAEAJADALQADALABALQACAKAAAMIgCAaIgEAfgAgFAAQgLACgIAFQgHAFgEAIQgFAHADAJQACAIAFADQAFADAIAAQAHAAAIgCIAQgHIAQgIIANgHIAAgMIgBgNIgMgDIgOgBQgLABgKACg");
	this.shape_16.setTransform(-36.8,-12.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AABATIglAyIgbgIIAxg9Igzg9IAbgGIAnAwIAmgxIAYAKIgvA6IAyA8IgYAJg");
	this.shape_17.setTransform(-51.6,-12);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_18.setTransform(-66.2,-11.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_19.setTransform(-88.4,-11.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAiBhQAEgMABgMIADgVIAAgUQgBgOgEgJQgEgJgGgGQgGgGgHgCQgHgDgHAAQgGAAgHAEQgHADgHAHQgHAFgGAMIgBBUIgZAAIgCjDIAdgBIgBBOQAHgIAIgEIAPgHQAIgDAHAAQAOAAALAFQAMAFAIAJQAJAKAFANQAFANAAARIAAATIgBASIgCARIgDAPg");
	this.shape_20.setTransform(-104.6,-15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgOgOIgsABIAAgXIAsgBIABg4IAYgCIgBA5IAygBIgCAXIgwACIgBBtIgaABg");
	this.shape_21.setTransform(-119.4,-14.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgOgOIgsABIAAgXIAsgBIABg4IAYgCIgBA5IAxgBIgBAXIgwACIgBBtIgaABg");
	this.shape_22.setTransform(-138.6,-14.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgXBEQgNgFgKgKQgLgKgGgNQgHgOAAgQQABgIACgJQADgKAEgIQAFgIAHgHQAGgHAJgFQAIgFAKgDQAJgDAKAAQALAAAKADQAKACAJAFQAIAFAHAIQAGAHAFAJIABABIgWAMIAAAAQgEgHgEgFQgEgFgHgEQgFgDgHgCQgHgCgHAAQgJAAgKAEQgIAEgHAHQgHAGgEAKQgEAJAAAJQAAAKAEAKQAEAJAHAHQAHAGAIAEQAKAEAJAAQAHAAAGgBIAMgFQAGgEAEgEQAFgFADgGIABAAIAWAMIAAABQgFAIgIAHQgGAGgJAFQgJAFgJACQgKADgKAAQgOAAgNgGg");
	this.shape_23.setTransform(-152.6,-11.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_24.setTransform(-168.2,-11.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAIBvQgHgDgEgFQgFgFgEgFIgGgMQgHgPgBgSIADihIAaAAIgBAqIgBAiIgBAcIAAAVIAAAiQAAALACAKQABAEADAEIAFAHIAHAEQAEACAFAAIgDAZQgJAAgHgCg");
	this.shape_25.setTransform(-178.8,-16.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgcBEQgNgFgIgJQgJgJgFgOQgFgNAAgQQAAgPAGgNQAFgOAKgKQAJgLANgFQAMgGAOAAQANAAAMAFQAMAFAJAJQAJAJAGANQAFAMACAOIhtAQQABAJADAHQAEAHAGAFQAFAFAHADQAIACAHAAQAHAAAGgCQAHgCAGgEQAFgEAEgGQAEgGACgIIAZAFQgEAMgGAJQgHAJgIAHQgJAHgKAEQgLADgLAAQgQAAgNgFgAgKgwQgGABgGAFQgGAEgFAIQgEAHgCALIBLgJIgBgDQgEgMgJgHQgIgHgNAAQgEAAgHACg");
	this.shape_26.setTransform(-190.9,-11.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgQBmQgKgCgJgEQgJgDgJgGIgSgLIATgWQAKAIAJAGIASAIQAJACAHABQAJABAIgCQAIgCAGgEQAGgDADgFQAEgFABgGQAAgEgCgFQgCgDgEgEIgJgFIgMgFIgMgDIgMgCIgPgEIgQgFQgHgEgHgFQgHgEgFgHQgFgIgDgJQgCgKABgMQAAgKAEgIQADgIAGgHQAFgGAIgFQAHgEAIgEIARgEIAQgBQAMAAAMADIALAEIALAEIALAHIALAJIgOAWIgJgIIgJgGIgJgEIgJgDQgKgDgJAAQgKAAgKAEQgJADgGAGQgGAFgDAGQgDAIAAAGQAAAHAEAFQADAHAHAEQAHAGAJADQAKADAJACQAKABAJACQAJABAIAEQAJAEAHAFQAHAFAFAGQAFAHACAIQACAIgBAKQgCAJgDAHQgEAHgGAFQgFAFgHAEQgHADgIACIgPADIgPABQgJAAgJgCg");
	this.shape_27.setTransform(-207,-14.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAjLgDqMhGVAAAQhhAAhFBFQhFBFAABgQAABhBFBFQBFBFBhAAMBGVAAAQBhAABFhFQBFhFAAhhQAAhghFhFQhFhFhhAAg");
	this.shape_28.setTransform(1,-14.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF3333").s().p("EgjKADrQhhgBhFhFQhEhEAAhhQAAhgBEhFQBFhEBhAAMBGVAAAQBhAABFBEQBFBFgBBgQABBhhFBEQhFBFhhABg");
	this.shape_29.setTransform(1,-14.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mc, new cjs.Rectangle(-249.1,-39.5,500.1,49.9), null);


(lib.inside_fruitcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgnERQAAgBAAAAQAAgBgBAAQAAAAAAgBQABAAAAAAIABgCIAFgCIAFAAIAAAAIABAAIABADIgBABIAAAAIgDAAIgEABQAAAAgBAAQAAABAAAAQgBAAAAABQAAAAAAAAgAjCD5IAAgCQABgGACgCQACgDAFAAIABABIABACIgCABIgCAAIgBABQgBAAAAABQAAAAgBABQAAAAAAABQAAABAAABIAAABIgBACIgCABIgCgBgAh5D0QAAAAAAgBQAAAAAAAAQAAgBAAAAQABAAAAAAIABgBIAHgEQAFgDAEAAQABgBABAAQAAAAABAAQAAAAABAAQAAAAABABIABABIgBABIgEABIgBABIgEABIgEACIgGAEIgCAAIgCgBgABdDTQAAAAAAgBQAAAAAAAAQAAAAAAgBQAAAAAAAAIADgCIAHgBIAAABIABAAIABABIgBABIgBABIgEAAIgDABIgBABIgCgBgABwCDQgBgCACgDIABgBIABAAIALAAIABAAIABACIAAABIgBAAIgLABIgBABIgCABIgBAAgAhFBqQAAAAAAgBQAAAAAAAAQAAAAAAgBQAAAAAAgBIACgBIADgBIABgCIAAgBIADgEIABgCQAAgBAAAAQAAAAAAgBQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAAAAAQABAAAAAAQAAABAAAAIAAACIAAACIgBADIgCACIAAACQAAABAAAAQAAAAgBAAQAAAAAAAAQgBAAAAAAIgEADIgCAAIgCAAgAADBmQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAgBABAAQABgCADgBIAEgBIALAAQAAAAAAAAQAAABAAAAQABAAAAAAQAAAAAAgBIABAEIgCAAIgLABIgEAAIgCACIgBAAIgCgBgAiIBXIgBgBIgBgBIAAgBIABAAIAAAAIABgBIACAAIAAABIABgBIABACIAAAAIgBABIgCABgAhbAxIgBgIIABgFIABgEIABgCQAAAAAAgBQAAAAAAAAQAAAAAAAAQABAAAAAAQAAgBAAAAQABAAAAAAQAAAAABAAQAAABAAAAIABACIgCAGIAAAFIAAAFIgBADgAC/AvIAAgDIAAgWIAAgEQgBgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQABAAAAAAQABAAAAAAQAAAAABABQAAAAABAAQABADAAAFIAAAYIgBACIgBAAIgCgBgAAAgEIgBgBIAAgBIAAAAIAAgBIABAAIAAAAIABgBIAAABIACAAIAAABIAAABIgBABgABIglIgCgBQAAAAAAAAQAAAAAAAAQAAAAAAgBQAAAAAAAAIAAgBIACgBIAIAAIAAAAIABAAIABABIABACQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAgBAAgACwg4QgBAAAAAAQAAgBAAAAQAAAAgBgBQAAAAAAgBIAAgGIAAgBIgBgBIgBgBQAAAAgBgBQAAAAAAAAQAAAAAAgBQABAAAAAAIABgBIACABIADACIAAADIAAAGIAAACIgBABIgBAAgAA1hoIgBgBIgCgBIAAgBIAAgCIACAAIABABIACACIAAABIgBABgAhIivIAAgCQABgDAFgDQAFgDACgDIABgCIABAAIABACIAAADIgFAEIgGAFIgCACIgCABIgBgBgAB5ixIAAAAIgBgBIgBgCIAAgDIgBgDIAAAAIACgBIACABQACACgBAFIAAACIgCAAgAAtjPIgCgCIgBgBIgCgCIABgCIACAAIAAAAIACABIADADIAAACIAAACIgCAAIgBgBgAg/jbIgBgBIgBgEIABgFIAAAAIADAAIAAACIAAAFIgBADgAhtkAIgBgCIAAgLQAAgBABgBQAAAAAAgBQAAAAABAAQAAgBAAAAIACABIAAACIABAMIgBACIgBACQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAgBg");
	this.shape.setTransform(20.6,4.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8E3105").s().p("AhJAkIgBgBIABgBIAHgHIABgBIAPAAQAAABAAAAQAAABAAAAQAAABAAAAQAAAAAAAAIgCABIgMABIgEAEIgBABIgBABgAARAaQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBIABgBIALAAIAAABIAAACIgBABIgHAAIgBACgAitAOIAAAAIABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABgBAAIAAAAgAijgQIgBgDIABgCIAAgBIABAAIACAAIAAABIABADIgBACgACkggIgBgBIAAgBIAAgBIABgBIAIAAIABAAIABAAIAAACIAAABIgBABg");
	this.shape_1.setTransform(16.2,50);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CD4A0B").s().p("AjvBwIAAgoIAAgBQAAAAABAAQAAAAAAAAQAAAAAAgBQAAAAAAAAIgBgCIAAAAIAAgvQBBAWBLAAQCuAAB7h0QAYgWASgZQgeBEg8A4Qh7B1iuAAQgwAAgsgJgAiEBSIgHAGIgBACIABABIADABIABgBIABgBIAEgEIAMgBIACgBQAAAAAAAAQAAgBAAAAQAAAAAAgBQAAAAgBgBIgOAAgAgwBMQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABIADAAIABgCIAGgBIABAAIABgCIgBgBIgKAAgAjlAgIAAAAIgBACIABAEIACAAIABgDIAAgDIgBAAIgBAAIgBAAIAAAAgABiASIgCABIAAABIAAABIABABIAJAAIABgBIABgBIgBgCIgBAAIAAAAg");
	this.shape_2.setTransform(22.8,44.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F3964B").s().p("AhGBtQgVgGgOgLQgHgHgGgJQgFgHgCgJQgGgSAAgUQABgVAHgRQAJgTAPgKQAKgHAPgDIAbgEQARgDAKgDQAPgEAIgIQALgKADgOIAAgKIAXAIIABAAQAWAIAQAKQASAKANAQQAPARAFATQAHAbgNAeQgOAdgZATQgsAehIAAQgXAAgQgDgAgPAmQAAAAAAAAQgBAAAAAAQAAAAAAABQAAAAAAAAIgBADIgDADIAAACIgBABIgDACIgCABQAAABAAAAQgBAAAAABQAAAAABAAQAAABAAAAQABAAAAAAQAAAAABAAQAAAAABAAQABAAAAAAIAEgDQAAAAABAAQAAAAAAAAQAAAAABAAQAAgBAAAAIAAgCIACgDIABgCIAAgCIAAgCIgBgBIgBAAgAA3AsIgEABQgDABgBACQgBAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIADABIACgCIAEgBIALAAIACgBIgBgDQAAAAAAAAQAAAAgBAAQAAAAAAAAQAAAAAAAAgAhfAgIAAABIABABIABABIABAAIACgBIABgBIAAAAIgBgCIgBAAIAAAAIgCAAIgBAAIAAAAgAgtgXQgBAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIgBACIgBAEIgBAFIABAIIADABIABgDIAAgFIAAgFIACgGIgBgCIgBgBIgBAAgAApg7IAAABIAAAAIAAABIABABIADAAIABgBIAAgBIAAgCIgCAAIAAAAIgCAAIAAAAg");
	this.shape_3.setTransform(16.3,9.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FE831F").s().p("AiXEuIAAgBQAAAAAAAAQAAgBAAAAQAAAAABAAQAAAAAAAAIABgBIAEAAIABABIABABIAAACIgCAAIgFAAgAgoEsIgBgBIAAgBIABgCIABAAIADAAIAAAAIABAAIABACIgBABIgBABgAAmElIAAgCIABgBIABgBIADAAIAAABIABAAIABABIAAABIgBABIgBAAIgEAAgAjeEFQgBAAAAgBQAAAAAAAAQAAgBAAAAQAAAAABgBIADAAIANgBIACgBIABgCQAAAAABgBQAAAAAAAAQABAAAAAAQABAAAAAAQAAAAAAAAQABABAAAAQAAAAAAABQAAAAgBAAIgBADIgCADIgDAAIgOABIgCgBgAiBD0IAAgBIAAgCIABAAIAJAAIAAAAIAAAAIABACQAAAAAAAAQAAABAAAAQAAAAAAAAQAAAAgBAAIAAABIgIAAIgBAAIgBgBgABuDxQAAAAAAgBQgBAAAAAAQAAgBABAAQAAAAAAAAIACgCIAFgFIACgCIACgEIABgBQABAAAAAAQAAAAABAAQAAAAAAAAQAAAAAAAAIAAACIgBADIAAACIgCACIgGAGIgCABIgCAAIgBAAgAiQDbQg1AAgmgJIgJgDIAAl7IAAAAQAUgEAsgGQAngFAXgHQAigLAWgTQAZgVAHghQACgOgBgKIAAAAQAjALAVAIQAyATAkAVQAdATAXAWIAUAVQAiAnALArQATBDggBFQgdBDg8ArQhkBIinAAIgFAAgAg8CrIgFACIgBABQAAABAAAAQAAAAAAABQAAAAAAABQAAAAAAAAIADABQAAAAAAgBQABAAAAAAQAAgBABAAQAAAAABAAIADgBIADAAIAAgBIABgBIgBgCIgBAAIAAAAgAjaCQQgCACAAAGIgBACIADABIABgBIABgCIAAgCQAAAAAAgBQAAgBABAAQAAgBAAAAQABgBAAAAIABgBIACAAIACgBIgBgDIgBAAIgBAAQgEAAgCADgAiBCKQgEABgGADIgHAEIgBABQAAAAAAAAQgBAAAAAAQAAABAAAAQAAAAABABIABABIACgBIAGgDIAEgCIAEgBIABgBIAEgBIABgCIgBgBIgCAAIgCAAgABFBwIgCABQgBABAAAAQAAAAAAABQAAAAAAAAQAAABABAAQAAAAAAAAQAAAAABAAQAAAAAAAAQABAAAAAAIADgBIAFAAIAAgBIABgBIgBgCIAAAAIgBAAIgHABgAg0iQQgCAOgMAKQgJAIgPAEQgJADgSADIgbAEQgPADgKAHQgPAKgIATQgIASgBAVQAAAUAGARQADAJAEAHQAGAJAIAHQANALAWAGQAPADAXAAQBKAAAqgeQAZgTAOgcQANgegHgcQgFgTgPgRQgNgQgSgKQgPgKgWgIIgBAAIgXgIIAAAKgABXAeIgBABQgCADABACQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAgBIABgBIALgBIABAAIABgCIgBgCIgCAAIgLAAgACkhRQgBAAAAABQAAAAAAABQAAAAAAABQABABAAABIAAAEIAAAWIABADQAAABAAAAQABAAAAAAQAAAAABAAQAAAAAAAAIABgDIAAgXQABgFgCgDIgCgBIgBAAgAAriHIAAABQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIACABIALABQAAAAAAAAQAAgBAAAAQAAAAAAAAQABgBAAAAIgBgCIgCgBIgBAAIAAAAIgIAAIgCABgACRilQAAAAAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIACABIABABIAAABIAAAGQAAAAAAABQAAAAAAABQAAAAAAAAQABABAAAAQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAAAgBIAAgCIAAgGIAAgDIgDgDIgBAAIgCABgAAXjLIAAACIACABIABABIABAAIABgBIAAgBIgCgDIgBAAIgCAAgAA3DaIgBgCIACgBIABgBIADAAIAAABIAAAAIABABIAAABIgBABIgEAAgAB9CuQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABgBQAAAAABAAQAAgBABAAQAAAAABAAIADgBIABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABgBAAIgEADIgCABIgBgBgADECiQAAgBAAAAQAAAAABgBQAAAAAAAAQABgBAAAAQAGgEADgGIADgDIAEgCQABACgCADQgDAFgIAHIgDACIgBAAQAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAAAAAgADnBWIABgDIACgCIAHgCQAAAAABAAQAAAAABABQAAAAAAAAQABABAAAAIABACIgBABIgEAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAAAAAABIgDABIgCAAg");
	this.shape_4.setTransform(23.3,14.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#4A1D08").s().p("AgIAwIAAgEIgEgMIgBgEIgBgHIgBgGIAAg/IAJAcQAKAdAMAYIAAAAQgGAAgBABQgDACgCAEQgBAFgEACIgFACIgCgBg");
	this.shape_5.setTransform(0.4,-18.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5D2408").s().p("AgkAfIAAg7IACAFIABAHIAAAEIAEAMIAAADQADABAEgCQAFgCACgEQABgEADgCQABgBAGAAIAAAAQADgDgIgTIAXAAQAOAAADAIIAGAKQABAEgCAEIgCAKQgBAIgEAFQgGAHgPAFQgQAGgLAAQgHAAgKgDg");
	this.shape_6.setTransform(2.5,-13.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#692B0E").s().p("AgGgEIgJgbIAAgRIAHAVIAMAgIAFAQIACAGQAIAUgDACQgMgYgKgdg");
	this.shape_7.setTransform(0.4,-19.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#A33D0C").s().p("Ag6A2IAAgXQAJADAHAAQAMAAAQgGQAPgFAGgHQAEgFABgIIACgKQABgEgBgEIgFgKQgEgIgNAAIgXAAIgCgGIgFgQQAdgCAeACQAWAAAHAMQAIANABAFQACAFgDAIIgEASQgBAMgHAIQgKAOgZAIQgbAJgTAAQgLAAgMgDgAg6gcIAAgBIABAGIgBgFg");
	this.shape_8.setTransform(4.7,-13.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EC5913").s().p("AkAFTIAAh1IAJADQAmAJA1AAQCrABBlhJQA8grAdhDQAghFgThDQgLgrgignIgUgVQgXgWgdgTQgkgVgygTQgUgIgkgLIAAAAQABAKgCAOQgHAhgZAVQgWATgiALQgXAHgnAFQgsAGgUAEIAAAAIAAgzQANADAKAAQAUAAAbgJQAZgJALgNQAGgJACgMIADgSQADgJgCgFQgBgFgIgNQgHgMgWAAQgegBgeABIgNghQApgIAsAAQBCAAA5ASQBgAbBPBJQAKAJAJALQBoBvAACWQAABVghBJQgTAZgYAWQh7B1itAAQhMAAhBgWgAiiE7QAAAAAAAAQgBAAAAAAQAAAAAAABQAAAAAAAAIAAABIABABIAFAAIACAAIAAgCIgBgBIgBgBIgEAAgAg1E5IAAABIABABIAEAAIABgBIABgBIgBgCIgBAAIAAAAIgDAAIgBAAIgBACgAAbExIgBABIAAACIABAAIAEAAIABAAIABgBIAAgBIgBgBIgBAAIAAgBIgDAAgAjXENIgBACIgCABIgNABIgDAAQgBABAAAAQAAAAAAABQAAAAAAAAQAAABABAAIACABIAOgBIADAAIACgDIABgDQABAAAAAAQAAgBAAAAQAAAAgBgBQAAAAAAAAIgBAAQAAAAAAAAQgBAAAAAAQAAAAgBABQAAAAAAAAgAiNECIAAABQAAAAAAABQAAAAABAAQAAAAAAAAQABAAAAAAIAIAAIAAgBQABAAAAAAQAAAAAAAAQAAAAAAgBQAAAAAAAAIgBgCIgBAAIAAAAIgIAAIgBAAIAAACgABuDwIgBABIgCAEIgCACIgFAFIgCACQAAAAAAAAQgBAAAAABQAAAAABAAQAAABAAAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAAAABAAIACgBIAGgGIACgCIAAgCIABgDIAAgCIgBAAIgBAAgAAsDmIgCABIABACIABAAIAEAAIABgBIAAgBIgBgBIAAAAIAAgBIgDAAgAB1C4QgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAAAAABQAAAAAAABQAAAAAAAAIADAAIAEgDQABAAAAgBQAAAAAAAAQAAAAAAAAQAAgBAAAAIgBgBIgCAAIgBABgADGChIgDADQgDAGgGAEQAAAAgBABQAAAAAAAAQgBABAAAAQAAAAAAABQAAAAAAABQABAAAAAAQAAAAABAAQAAAAABAAIADgCQAIgHADgFQACgDgBgCIgEACgADeBgIgCACIgBADIACAAIADgBQAAgBAAAAQABAAAAAAQABAAABAAQAAAAABAAIAEAAIABgBIgBgCQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAAAAAIgHACgAhhkPIgBACQgCACgFAEQgEADgCACIAAADIADAAIADgCIAFgFIAFgEIAAgEIgBgBIAAAAIgBAAgABQkLIgBABIACADIAAACIAAADIABAAIABAAIABAAIAAgCQABgEgCgCIgCgCIgBABgAACkmIgBACIACACIABABIACACIACABIABgBIABgBIgBgCIgDgDIgCgBIAAgBIgCABgAhnk1IgBAEIABAFIABABIABAAIACgDIAAgFIgBgCIgDAAgAiVleIAAAMIABACQAAABAAAAQABAAAAAAQAAABAAAAQABAAAAAAIACgCIAAgCIAAgMIgBgCIgCgBQAAAAAAAAQgBABAAAAQAAAAAAABQAAABgBAAg");
	this.shape_9.setTransform(24.5,12.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy2, new cjs.Rectangle(-1.2,-24.6,51.4,81.3), null);


(lib.inside_fruitcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Albq3QEhAADKDMQDMDMAAEfQAAEgjMDMQjKDMkhAAg");
	mask.setTransform(33.7,0.2);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E57010").s().p("AgEALQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQgBgKADgFQADgDACAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAABABIABABQAAADgDAFIgBAFQAAADgBACIgCACIAAAAIgEgBg");
	this.shape.setTransform(32.7,-13.6,0.417,0.417,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E57010").s().p("AgIAJQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBABAAIABgDIAGgIQACgDACABIABgBIAFABQADABAAADQgBADgEABIgEADIgCAFIgEACQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_1.setTransform(25,-16.4,0.417,0.417,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E57010").s().p("AAOAGIgNgCIgVgBQgFAAgBgDQAAgBACgDIADgBQAbgCAPAFQAGACgBAEQAAADgFAAIgHgBg");
	this.shape_2.setTransform(-3.6,-5.1,0.417,0.417,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E57010").s().p("AANATIgGgFQgDgEgJgHQgJgFgDgGQgDgEABgEIADgCQAAgBABAAQAAAAABAAQAAAAAAAAQABAAAAABIADADQAEAGAKAJQAMAGADAGQACAFgBACIgDABIgEgBg");
	this.shape_3.setTransform(14.1,-13.7,0.417,0.417,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E57010").s().p("AAAAWIgEgDIAAgFIAAgdIABgFQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAQAEAAABAHIAAAbIgBAGQgBADgDAAIAAAAg");
	this.shape_4.setTransform(9.8,-21.7,0.417,0.417,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E57010").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_5.setTransform(14.3,-17.8,0.417,0.417,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E57010").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIAAACQgBABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_6.setTransform(7,13.2,0.417,0.417,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E57010").s().p("AAGATIgIgFQgEgBgBgDIgBgFQAAgDgDgCIgEgFIgBgGIABgFQACgEADABQADABABAEIABAFQACAEADAFIACADIACADIAGAEQADABACACQADAEgDACIgEABIgFgBg");
	this.shape_7.setTransform(14.3,14.5,0.417,0.417,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E57010").s().p("AgCATIABgMIgCgLIgEgOQABgDABgCQADgDACABQACABAAAEIABAFIADAIQACAEAAAIQAAAOgDAGIgFABQgCgCAAgFg");
	this.shape_8.setTransform(11.7,8.6,0.417,0.417,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E57010").s().p("AAAAFIgDgBIgBAAIgBgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgCACQAAABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_9.setTransform(20.7,4,0.417,0.417,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E57010").s().p("AATAIIgFgEIgKgCIgYgBIgEgBQgCgBADgGQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAZAAIAKABQAHADADAEQADAEgDACIgDABIgDAAg");
	this.shape_10.setTransform(22,14.7,0.417,0.417,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E57010").s().p("AADAPQAAAAgBAAQAAgBAAgBQgBAAAAgBQAAAAAAgBIABgGQAAgFgDgEIgDgBIgFgBIgEgDQgBgDADgDIADgBQAJgBAGAIQAEAFACANQACAEgBACQgBACgFAAIgFgCg");
	this.shape_11.setTransform(1.7,29,0.417,0.417,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E57010").s().p("AADACIgIgBIgHAAIgCgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIADAAIAAAAIAMAAQAIABAEADIADADQACAEgDADIgGACQgCgFgEgCg");
	this.shape_12.setTransform(17.3,31.7,0.417,0.417,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E57010").s().p("AgDApQgCgBAAgFIAAg3QAAgMADgGQAEgFADACQADADgDAIIgBAKIAAA0QAAAFgBADQgBAAAAABQgBAAgBABQAAAAAAAAQAAAAgBAAIgCgBg");
	this.shape_13.setTransform(39.9,7.8,0.417,0.417,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E57010").s().p("AgHAQIgBgFIAAgPQAAgFACgCQABgCAFgDIAEgBQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAABAAAAQABABAAABQAAAAgBABQAAABAAAAIgEAEIgCACIgBADQABAFgBAHQAAAGgCACIgCAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_14.setTransform(38.1,-1.7,0.417,0.417,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E57010").s().p("AgDAGIAAAAIgBgBIgBgBIAAgBIAAgBQABgDADgDIABgBIABAAIACAAIACABIABACQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABIgDADIgCABg");
	this.shape_15.setTransform(25.9,-6.1,0.417,0.417,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E57010").s().p("AgPACIACgDIADgDQAAAAABgBQAAAAABAAQAAAAAAAAQABABAAAAIABgBIARAAQADAAACACIAAACQAAABAAAAQAAAAAAABQAAAAAAAAQAAABgBAAIgDABIgZADQgBAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBg");
	this.shape_16.setTransform(28.3,0.8,0.417,0.417,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E57010").s().p("AALAGIgDgEIgXgBIgDgBQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBIACgEQAAAAABABQAAAAABAAQAAAAAAgBQABAAAAAAIAZAAIADABIADADQAEAGgBAEIgEABQgDAAgCgCg");
	this.shape_17.setTransform(32.6,17.5,0.417,0.417,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E57010").s().p("AAIAGIgIgCIgKgBIgCgBQAAgBgBAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIACgDIADAAIABgBQAKAAAFACIAGADQADADgDADIgDAAIgCAAg");
	this.shape_18.setTransform(30.5,25.6,0.417,0.417,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E57010").s().p("AATANIgPgIIgJgFIgIgBIgEgCQgGgBgDgCQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAFgBAGABQAKACALAHIAQAIIAEADQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAgBAAQAAAAgBAAg");
	this.shape_19.setTransform(9.6,28.6,0.417,0.417,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E57010").s().p("AAAAKQgCAAgCgDIAAgFIAAgGQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIACgBIACABIABAAQACAEAAAGIAAAFIgBABIgBABIgCAAIgBABg");
	this.shape_20.setTransform(-4.2,21.3,0.417,0.417,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E57010").s().p("AADAIIgCgCIgKgGQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAABIABgBQAGgBAGAIQAAAAABAAQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgEABIgCAAg");
	this.shape_21.setTransform(-5.8,5.5,0.417,0.417,0,0,180);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E3105").s().p("AgKAEIgBAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIACAAIABgBIARABIAEABIABACQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIgDADg");
	this.shape_22.setTransform(33,46.5,0.417,0.417,0,0,180);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E3105").s().p("AAFACIgPgBIgCgBQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAgBAAgBIACgDIADAAIAAgBIASAAIADABIACADQACAFgDAFIgGABQgCgCAAgEg");
	this.shape_23.setTransform(18.5,52.3,0.417,0.417,0,0,180);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E3105").s().p("AAAAFIgCgCIgDgBIgDgCIACgFIADAAIAAgBIAHABIACAAIABACIACADIgCAFIgBABQgDAAgCABg");
	this.shape_24.setTransform(-11.6,48.3,0.417,0.417,0,0,180);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E3105").s().p("AgLAEIgCAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIAWABIADAAIABADIAAADQgBACgFAAg");
	this.shape_25.setTransform(-1.7,51.5,0.417,0.417,0,0,180);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E3105").s().p("AASAMQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgFgGgGgDIgagCQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgDgDAEgEIAgAAIADAAIARAPQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAADgDABIgFAAg");
	this.shape_26.setTransform(9.9,53.2,0.417,0.417,0,0,180);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E3105").s().p("AgEABIAAgGIABgBIABAAIACgBIACAAIABABIACAFIgCAIIgFABQgCgCAAgFg");
	this.shape_27.setTransform(-0.1,48,0.417,0.417,0,0,180);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E3105").s().p("AgHAFQgBgCACgDIAFgEQAEgCADABIABAAIABABIABACQAAABgBABQAAAAAAAAQAAAAgBABQAAAAAAABIgEACQgDACgDAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAg");
	this.shape_28.setTransform(-15.2,43.1,0.417,0.417,0,0,180);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E3105").s().p("AgEAPIgCgCIABgDIACgEQABgBAAgEIgBgJIABgEQACgEAEACQABABABAGQABAGgBAFQgBAHgBACQgCACgCABIgCAAIgCgBg");
	this.shape_29.setTransform(-24,37,0.417,0.417,0,0,180);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E3105").s().p("AAAAFIgDgBIAAAAIgBgBIgBgBIAAgCIABgDIADAAIAAgBIAEABIABAAIABABIABACIAAABIgBACQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_30.setTransform(-30.1,26.2,0.417,0.417,0,0,180);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E3105").s().p("AACARIgBgGQgBgGgJgQIgBgDQAAgDAEAAQACgBADADQACACAFAJIAEAKQACAEgCAEQAAABAAABQgBAAAAABQgBAAAAAAQAAABgBAAIgCAAIgDgBg");
	this.shape_31.setTransform(-28.3,32.3,0.417,0.417,0,0,180);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E3105").s().p("AABAGIgBAAIgBgBIgEgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAABAAIABAAIABABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABABAAAAQAAAAAAAAQAAABAAABQAAAAAAABIgBACIgCABg");
	this.shape_32.setTransform(-31.6,17.9,0.417,0.417,0,0,180);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E3105").s().p("AgFAKIgCgCIABgEIACgDIABgFQAAgDACgCIABAAIAAgBIAGABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBACQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIgBAFQgBADgBABIgEABg");
	this.shape_33.setTransform(-24.2,31.9,0.417,0.417,0,0,180);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E3105").s().p("AgEAQIAAgEIAAgXIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAQACAAACACQABADAAAGIAAAMIgBAHQgBAGgDAAQgCgBgCgCg");
	this.shape_34.setTransform(-29.4,22.3,0.417,0.417,0,0,180);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#AF410C").s().p("AgBAGIgBAAIgBgBIgBgCIAAgCIABgFIABgBIACAAIABAAIABAAIABACIACADQAAACgCADIgBABg");
	this.shape_35.setTransform(-31.4,14.1,0.417,0.417,0,0,180);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FE831F").s().p("AAIAFIgSgBIgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAAAgBQAAAAABAAQAAAAAAgBQAAAAABgBQAAAAABgBIADAAIAAgBIARABIADAAQABABAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_36.setTransform(10.9,38.5,0.417,0.417,0,0,180);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FE831F").s().p("AgFAEIgEgBIgBgDIACgDIADAAIAAgBIALABIACAAQAAAAABABQAAAAAAAAQABABAAABQAAAAAAAAIAAADIgDACg");
	this.shape_37.setTransform(8.5,44.3,0.417,0.417,0,0,180);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FE831F").s().p("AgMAKIgGgBQgDgBgEgGIgCgFQgBgEACgCQAFgCADAFIADAFIAEABIAeABQAFAAACACQAFADgEAEIgFABg");
	this.shape_38.setTransform(2.1,39.9,0.417,0.417,0,0,180);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAAAABABQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_39.setTransform(19.4,44,0.417,0.417,0,0,180);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FE831F").s().p("AgEAEIgBAAIgCgCIAAgBIAAgBIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_40.setTransform(27.4,43.3,0.417,0.417,0,0,180);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FE831F").s().p("AgEAEIgDgBIAAgDIACgDIACAAIAAgBIAHABIADAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABAAAAIgDACg");
	this.shape_41.setTransform(29.1,35.8,0.417,0.417,0,0,180);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FE831F").s().p("AALATIgGgDIgNgMQgDgDgCgDIgBgFIgDgFQgBgEACgCQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABIAEAJIAEAGQAEAEAKAHIAEAEQACACgDADIgDABIgDgBg");
	this.shape_42.setTransform(35,37.5,0.417,0.417,0,0,180);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FE831F").s().p("AAEAIIgMgIQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAAAgBQABAAABAAQADAAADACQAEACAEADQADADgDAEIgEABIgBAAg");
	this.shape_43.setTransform(36.2,31.3,0.417,0.417,0,0,180);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FE831F").s().p("AAPAXQgDgBgEgEQgSgRgIgLQgFgHADgFQAFAAAEAEIAGAIQAJANANAJQAFADAAADQgBAFgEAAIgCAAg");
	this.shape_44.setTransform(43.8,29.6,0.417,0.417,0,0,180);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FE831F").s().p("AAKAIIgEgDQgEgDgFABQgHACgDgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAgBABAAQADgFAEAAIAAAAQAGgBAJAEIAGAEQACAEgBAEIgEAAIgCAAg");
	this.shape_45.setTransform(47.1,22.5,0.417,0.417,0,0,180);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FE831F").s().p("AAAAFIgDgBIAAAAIgCgBIgBgBIAAgCIACgDIADAAIAAgBIAEABIABAAIABABIABACIABABIgBACQgBABAAAAQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_46.setTransform(-7.1,35.8,0.417,0.417,0,0,180);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FE831F").s().p("AgMAKIAHgIQACgCACgHIAGgJIAFgDQADgBACACQADADgDAEIgFAFQgDADgCAGQgDAGgEAGQgHAFgFAAQgEgEAGgGg");
	this.shape_47.setTransform(-13.9,33.5,0.417,0.417,0,0,180);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FE831F").s().p("AgIALIABgEIAHgKIAAgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgBIADAAIACACQACAGgCAFIgGAKIgCACIgGACQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAg");
	this.shape_48.setTransform(-18.9,29.3,0.417,0.417,0,0,180);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FE831F").s().p("AgBAGIAAAAIgBgBIgBgCIgBgCIABgFIABgBIACAAIAAAAIACAAIACACIABADQAAACgDADIgBABg");
	this.shape_49.setTransform(-16.6,22.4,0.417,0.417,0,0,180);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FE831F").s().p("AgCAMQgCgCAAgEIAAgMQAAgDACgCIACAAIACAAIABACQACADAAAGQAAAHgCADIgBACIgBAAg");
	this.shape_50.setTransform(-23.4,15,0.417,0.417,0,0,180);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FE831F").s().p("AgDAXIgBgFIAAgkIAAgEQABAAAAgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAABQABAAAAABQACADAAAFIAAAbQAAAFgCADQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIAAAAIgDgBg");
	this.shape_51.setTransform(-16.2,13.5,0.417,0.417,0,0,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE831F").s().p("AABAJIgBgFQAAgEgEgDQgEgCgCgCIABgEQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABABIAAgBQAFgBAEAFQAEAEABAFQADAJgDAEIgGACQgCgCAAgEg");
	this.shape_52.setTransform(-18.5,0.8,0.417,0.417,0,0,180);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE831F").s().p("AgBAGIgBAAIAAgBIgBgCIgBgCIABgFIABgBIABAAIABAAIACAAIACACIABADQAAACgCADIgCABg");
	this.shape_53.setTransform(-25.2,-0.7,0.417,0.417,0,0,180);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE831F").s().p("AgEATIAAgnIABgEQAAAAABAAQAAgBABAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQACACAAAGIAAAaQAAAJgCAFIgFABQgCgCAAgFg");
	this.shape_54.setTransform(-13.7,-2.1,0.417,0.417,0,0,180);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE831F").s().p("AAAAHIAAgBQAAAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgDgEQAAAAgBgBQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgDIADAAIAAgCIAEABIABABIACADIABABIgCAFIgBABIgBABg");
	this.shape_55.setTransform(-21.3,-7.6,0.417,0.417,0,0,180);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE831F").s().p("AgEAJIAAgPIAAgEIABgCIACgBIACAAQABAAAAAAQAAABABAAQAAAAAAABQAAABABAAQABADAAAHQAAAJgCAEIgFABQgCgBAAgEg");
	this.shape_56.setTransform(-11.1,-9.2,0.417,0.417,0,0,180);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE831F").s().p("AAAAMIgBgGQAAgDgCgDIgEgHQgCgFACgCQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAABIABgBQACgBACACQADABAAADIABAEIACAFQACADAAAFIgBAFIgCAEIgDAAQgDAAAAgEg");
	this.shape_57.setTransform(-15.1,-13.5,0.417,0.417,0,0,180);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#CD4A0B").s().p("AikEYQg/gcg2gzQhOhKgchdIgJgiQgIgjAAglIAAgJQAAiFBQhnQggBJgBBUQAAAvAKApIAHAcQAHAXAKAVQAfBCA7A5QA2AyA+AeQBTAlBjAAQCvAAB6h1QAYgWATgYQgfBEg7A4Qh8B1iuAAQhjAAhSgmg");
	this.shape_58.setTransform(6.4,24.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FE831F").s().p("AhKEFQg1AAgmgJQgxgMgfgaQgdgbgQgoIgCgIQgLgegDgiIAAgZQACgwARgnQAUgsAjgYQAXgQAjgJQAUgEAsgGQAngFAXgHQAigLAVgTQAZgVAGghQADgOgBgKIAAAAQAkALAVAIQAyATAjAVQAeATAXAWIAUAVQAhAnAMArQASBCgfBGQgeBDg8ArQhjBIinAAIgFAAgAARhmQgDAOgLAKQgIAIgPAEQgKADgRADIgbAEQgPADgKAHQgPAKgJATQgHARgBAVQAAAUAGASQACAJAFAHQAGAJAHAHQAOALAVAGQAQADAXAAQBIAAAsgeQAZgTAOgdQANgegHgbQgFgTgPgRQgNgQgSgKQgQgKgWgIIgBAAIgXgIIAAAKg");
	this.shape_59.setTransform(16.3,10);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#F3964B").s().p("AhGBtQgVgGgOgLQgHgHgGgJQgFgHgCgJQgGgSAAgUQABgVAHgRQAJgTAPgKQAKgHAPgDIAbgEQARgDAKgDQAPgEAIgIQALgKADgOIAAgKIAXAIIABAAQAWAIAQAKQASAKANAQQAPARAFATQAHAbgNAeQgOAdgZATQgsAehIAAQgXAAgQgDg");
	this.shape_60.setTransform(16.3,9.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#4A1D08").s().p("ABcDMIAAgEIgEgMIAAgFIgBgGIgIgdQgEgQgFgQQgNgigJgTIgDgJQgIgQgJgQQgJgSgLgRQgrhHg9g4IgMgMQgGgIgCgIQgBgKAKgRIAEgIQAWAVAHAOQAMAZAuAsQAsAsAdBTIAmBqQALAeALAYIAAABQgGgBgBACQgDABgBAEQgCAFgFACIgFACIgCAAg");
	this.shape_61.setTransform(-9.7,-33.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#5D2408").s().p("AgHAfIgSgFIgNgDQgIgCgGgDQgGgDgEgGQgFgGAAgGQAAgIAGgFQAEgDAQgEQAQgFATgDIACAIIABAHIABAEIACAMIABADQACABAFgCQAFgCABgEQACgEACgCQACgBAGAAIAAAAQADgDgIgTIAXAAQAOAAAEAIIAFAKQABAEgBAEIgCAKQgBAIgEAFQgGAHgQAFQgQAGgMAAQgHAAgKgDg");
	this.shape_62.setTransform(-0.6,-13.6);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#692B0E").s().p("ABYCXIgmhqQgdhTgsgsQgugrgMgaQgHgOgWgVQAHgNABgGQA4AhAoBHIAHAPQAXArAbBLIANAlIAWA7IAMAhIAGARIABAGQAIATgDACQgLgYgLgeg");
	this.shape_63.setTransform(-9,-35.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#A33D0C").s().p("AgMA0IgdgIIgYgGQgNgEgIgEQgLgGgHgJQgIgKAAgLQAAgMAJgJQAHgGAOgDQAjgLAjgFIAIAdIgCgIQgTADgQAFQgQAEgEADQgGAFAAAIQAAAGAFAGQAEAGAGADQAGADAIACIANADIASAFQAKADAHAAQAMAAAQgGQAQgFAGgHQAEgFABgIIACgKQABgEgBgEIgFgKQgEgIgOAAIgXAAIgCgGIgFgQQAdgCAfACQAWAAAHAMQAIANABAFQACAFgDAIIgEASQgBAMgHAIQgKAOgZAIQgcAJgTAAQgNAAgQgFg");
	this.shape_64.setTransform(-0.6,-13.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#EC5913").s().p("AidFDQg/gdg1gyQg7g5gfhDQgKgWgHgWIgHgcQgKgqAAgtQAAhVAhhIQATgYAXgWQAmgkApgYQAZgPAagKQAdgMAggIQAGAQADAQQgjAEgjALQgNADgIAGQgIAJgBANQAAAMAIAKQAHAIALAGQAIAFAOADIAXAGIAdAJQARAFANAAQAUAAAbgJQAZgJALgNQAGgJACgMIACgSQADgJgCgFQgBgFgHgNQgHgMgWAAQgegBgeABIgNghQApgIAsAAQBBAAA5ASQBhAbBPBJQAKAJAJALQBoBvAACWQAABVghBJQgTAZgYAWQh7B1iuAAQhkAAhSgmgABikHQgHAhgZAVQgWATgiALQgWAHgnAFQgsAGgUAEQgjAJgWAQQgkAYgUAsQgRAogBAvIAAAZQACAiALAeIACAIQARAoAcAbQAgAaAwAMQAmAJA1AAQCrABBlhJQA8grAdhDQAghFgThDQgLgrgignIgUgVQgXgWgdgTQgkgVgygTQgVgIgkgLIAAAAQABAKgCAOg");
	this.shape_65.setTransform(10.4,12.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0E6924").ss(0.3,1,1).p("AhzgrQACgNgBgKQgCgNgJgGQgCgBgEgCQgDgCgCgCQgEgDgDgFQgBgCgDgIADRAoIAAgBQAGgNgIghQgGgigUgWQgUgWgdgOQgWgLgggIQg7gPg7gDQgCAAgBAAQgYAAgYABQgmACgkAIQgNACghAJQABABAAAAADPAsQgNAjgjAZQgqAggzABQgwADgvgXQgFgDgGgCQgbgPgYgVQgKgIgIgKQgggggagvQgWgmgUg0QAAgBgBgBAhzgrQgxgegugoACqApQgYgBgYgDQhEgIg8gVQgGgCgGgCQgJgCgNAAQgBAAgBAAQgSAAgKAEQgEADgGADQgGAEgFAAIAAABQgJAAgMgFACqApQgDgHgFgGQgKgMgcgHQgegIgKgIQgHgFgFgHQgLgRgDgVADHApQgPAAgOAAAAsASQgBgVgYgTQgIgGgMgIQgSgJgEgDQgLgHgGgIQgEgFAAgDAgSAEQgLgEgLgEQgbgLgYgOQgMgGgMgIAB6AlQgMgBgMANQgDADgGAHQgEAHgFADQgKAIgTAEQgQADgnAG");
	this.shape_66.setTransform(-30.1,-31.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#0E802B").s().p("Ag4BpIgLgFQgcgOgXgVQgKgIgJgKQgfghgbguQgWgmgTg0IgBgDQAtAoAxAfIAZAOQAYANAbALQAKAGAMADQgJgCgNAAIgDAAQgSAAgKAFIgJAEQgHAEgEAAIAAABQgJABgNgGQANAGAJgBIAAgBQAEAAAHgEIAJgEQAKgFASAAIADAAQANAAAJACIALAEQA9AVBDAHQAYAEAYAAIAeABIABgBIACAAIAGAAIANAAIAEgBIAEgBIALAAQAEAAAEgCIANgDIADgBQAAgBAAAAQAAAAAAABQABAAAAAAQAAABAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgBACQgGAEgIAAQgHACgIgBQgBAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAIgEAAIgEABIgQAAQgNAjgjAaQgqAfgzACIgJAAQgqAAgrgVgABHAlIgJAKQgFAHgEAEQgKAHgUAEQgPADgnAHQAngHAPgDQAUgEAKgHQAEgEAFgHIAJgKIABgBIAAgBQAKgJAKgBIABAAIAAAAIABAAIAAAAIAAAAIgBAAIAAAAIgBAAQgKABgKAJIAAABIgBABIAAAAg");
	this.shape_67.setTransform(-27.4,-30.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#159335").s().p("ACqBZQgDgHgFgGQgKgMgcgHQgegIgKgJQgHgFgFgHQgLgRgDgUQADAUALARQAFAHAHAFQAKAJAeAIQAcAHAKAMQAFAGADAHQgYgBgYgDQhEgIg8gVIgMgEQgLgEgLgFQgbgLgYgOIgYgOIABgOIAAgIQgCgNgJgGIgGgDIgFgEQgEgDgDgFIgEgKIAEAKQADAFAEADIAFAEIAGADQAJAGACANIAAAIIgBAOQgxgdgugoIgBgBQAhgJANgCQAkgIAmgCIAwgBIADAAQA7ADA7APQAgAIAWALQAdAOAUAWQAUAVAGAiQAIAigGANIAAABIgGAAIgDAAIgBABIgdAAgAgogPQAGAIALAHIAWALQAMAIAIAGQAYATABAWQgBgWgYgTQgIgGgMgIIgWgLQgLgHgGgIQgEgFAAgDQAAADAEAFg");
	this.shape_68.setTransform(-30.1,-36.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#ECE9E9").s().p("AFwLnIgIAAQkvgDjWjXQjZjZAAk0QAAkzDZjaQDWjWEvgDIAIAAIAHAAIAAXNIgHAAg");
	this.shape_69.setTransform(-37.6,0);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FF6666").s().p("AAAM3IgHAAQlQgDjujvQjwjwAAlVQAAlUDwjxQDujuFQgDIAHAAIAHAAQFQADDvDuQDxDxgBFUQABFVjxDwQjvDvlQADIgHAAgAooooQjlDlAAFDQAAFEDlDlQDiDiE/ACIAHAAIAHAAQE/gCDijiQDljlABlEQgBlDjljlQjijik/gCIgHAAIgHAAQk/ACjiDig");
	this.shape_70.setTransform(-0.8,0);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFCC00").s().p("AAAMNIgHAAQk/gCjijiQjljlAAlEQAAlDDljlQDijiE/gCIAHAAIAHAAQE/ACDiDiQDlDlABFDQgBFEjlDlQjiDik/ACIgHAAgAoMoNQjZDaAAEzQAAE0DZDZQDWDXEvADIAHAAIAHAAQEvgDDXjXQDajZAAk0QAAkzjajaQjXjWkvgDIgHAAIgHAAQkvADjWDWg");
	this.shape_71.setTransform(-0.8,0);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AlvrmQEvADDWDWQDaDaAAEzQAAE0jaDZQjWDXkvADg");
	this.shape_72.setTransform(36.6,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69}]}).wait(1));

	// Layer 4
	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("rgba(0,0,0,0.027)").s().p("Ap/KAQkJkJAAl3QAAl2EJkJQEJkJF2AAQF3AAEJEJQEJEJAAF2QAAF3kJEJQkJEIl3AAQl2AAkJkIg");
	this.shape_73.setTransform(-0.8,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_73).wait(1));

}).prototype = getMCSymbolPrototype(lib.inside_fruitcopy, new cjs.Rectangle(-91.3,-90.4,181,180.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.ch2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Alaq3QEfAADKDLQDNDNgBEfQABEgjNDMQjKDMkfAAg");
	mask.setTransform(33.8,0.4);

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F3409A").s().p("AE6BlQgChJg0gzQg3g2hNAAIj/AAQhMAAg4A2Qg0AzgCBJIgdAAQABhSA6g7QA9g8BTAAIEXAAQBTAAA8A8QA7A7ABBSg");
	this.shape.setTransform(-0.4,-48.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F3409A").s().p("Ah3hTQArgoA7ACQA4ACApArIAKAKIAGAJQAYAkgBAtQAAAVgHARQgIAbgTAVIgLAMgABUAhIgBAFQgKAYgPATIAXAZQAFgKAEgKQAGgUAAgUQABgIgBgKIgFgWQgBANgGAOg");
	this.shape_1.setTransform(32,-29.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E794BE").s().p("AgSAZQAPgTAJgXIABgEQAGgPABgNIAFAWQABAKgBAIQAAAUgGATQgEALgFAJg");
	this.shape_2.setTransform(39.7,-23.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E794BE").s().p("AgLAeQgHgTAAgUQgBgIACgKIAEgWQABANAHAPIAAAEQAKAXAPATIgWAZQgFgJgEgLg");
	this.shape_3.setTransform(-40.9,-23.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F3409A").s().p("AhUBuQgTgVgIgbQgGgRgBgVQgBgtAYgkIAHgJIAJgKQApgrA4gCQA7gCAqAoIjADNIgLgMgAhfAcQgBAKAAAIQAAAUAIAUQADAKAFAKIAXgZQgPgTgKgYIgBgFQgGgOgCgNIgEAWg");
	this.shape_4.setTransform(-33.2,-29.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgNAaIAAgJIAJAAIAAgcIgJAFIAAgKQAHgDAEgGIAIAAIAAAqIAIAAIAAAJg");
	this.shape_5.setTransform(-11.7,-19.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgNAaIAAgJIAJAAIAAgcIgJAFIAAgKQAHgDAEgGIAIAAIAAAqIAIAAIAAAJg");
	this.shape_6.setTransform(-15.4,-19.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1E1C1A").s().p("AAIBWQgIgJAAgMQAAgGACgGIiGhYQgYgVgwgmQAsAdApAWIB9BaIACgDQAJgIAMAAQANAAAHAHIAHAHIBGgjQgCgGAEgHQADgGAHgCIAKgCIAJgBIATgEQASgCALgFQgPAJgMANQgEAGgCACQgEAHgEADQgGAGgHADIgFABIgFAAIgGgBQgEgDgBgEIhIAkQABADAAAEQAAAMgJAJQgIAJgNAAQgMAAgJgJg");
	this.shape_7.setTransform(-3.3,-1.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgMATQgEgGAAgNQAAgMAEgHQAEgGAIgBQAJABAEAGQAEAHAAAMQAAANgEAGQgEAIgJgBQgJABgDgIgAgDgPQgBADAAAMQAAANABACQABADACAAQABAAAAAAQABAAAAgBQABAAAAAAQAAgBABAAQABgDAAgNQAAgLgBgDQgBgDgDgBQgCAAgBADg");
	this.shape_8.setTransform(-22.6,-9.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgNAaIAAgIIAJAAIAAgeIgJAHIAAgKQAHgEAEgGIAIAAIAAArIAIAAIAAAIg");
	this.shape_9.setTransform(-26.2,-9.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgOAOIAJgCQABAGAEAAQAGAAAAgPIAAgBQgDAFgFgBQgGAAgEgEQgEgFAAgGQAAgHAFgFQAEgEAHAAQAHgBAEAEQADAEACAGIABAMQAAAbgRAAQgMAAgCgNgAgDgPQgCABAAAEQAAAIAFAAQAGAAAAgIQAAgEgCgBQAAgBgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQgBAAAAABQAAAAgBABg");
	this.shape_10.setTransform(-27.9,3.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgMAWQgEgEAAgGQAAgEABgDQACgCAFgDQgHgDAAgIQAAgGAEgFQAEgDAHAAQAIAAAEADQAEAEAAAFQAAAHgHAFQAIADAAAJQAAAGgEAFQgFAEgIABQgIAAgEgFgAgHALQAAAEACACQACACADAAQADAAACgCQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAgBIgCgDQgBgCgGgDQgFADAAAEgAgDgRQAAAAAAABQgBABAAAAQAAABAAAAQAAABAAAAQAAADACACIAFADQADgEAAgDQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQAAAAgBAAQAAAAgBAAQAAABgBAAg");
	this.shape_11.setTransform(-24.5,17.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgIAaQAAgYAOgRIgVAAIABgKIAeAAIAAAJQgNAQAAAag");
	this.shape_12.setTransform(-16.4,28.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgQAAQABgNAEgGQAFgHAHAAQAGAAAEAEQAEADACAHIgJACQgCgHgFgBQgDAAgBAEQgCADAAALQADgGAFAAQAGAAAEAEQADADABAIQgBAJgEAEQgFAFgHgBQgPAAgBgagAgFAKQAAAEACACQAAABABAAQAAAAABABQAAAAABAAQAAAAAAAAQAGAAAAgJQAAgIgGABQgFgBAAAJg");
	this.shape_13.setTransform(-0.9,32.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgLAWQgEgEgBgGIAKgCQABAHAFAAQAGAAAAgIQAAgEgCgCQgBgDgDAAQgEAAgBAEIgJgBIACgcIAaAAIgBAKIgRAAIAAAMQADgFAEAAQAGAAAEAEQAEAEAAAIQAAAJgFAEQgEAFgIAAQgGAAgFgEg");
	this.shape_14.setTransform(12.2,28.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AABAaIAAgMIgSAAIAAgJIARgeIAMAAIAAAeIAGAAIAAAJIgGAAIAAAMgAgJAGIALAAIAAgVg");
	this.shape_15.setTransform(22.5,19.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgKAXQgFgEgBgIIAKgBQABAHAFAAQAAAAABAAQABAAAAAAQABgBAAAAQABAAAAgBQACgCAAgDQAAgHgHAAIgDAAIAAgHIADAAIADgBQAAAAAAAAQABAAAAAAQAAgBABAAQAAAAAAAAIABgFQAAgGgFAAQgEAAgBAGIgKgCQAEgMAMAAQAHAAAEADQAEAEAAAGQAAAIgHADQAIADAAAJQAAAIgEAEQgFADgIAAQgFAAgFgDg");
	this.shape_16.setTransform(26.2,6.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgQAaIAAgKQAOgMADgEQAEgFgBgEQAAgHgEAAQgDAAgBACIgCAHIgKgCQADgQANAAQAIAAAEAEQAEAEAAAHQAAAHgEAEQgFAHgMAIIAWAAIgBAKg");
	this.shape_17.setTransform(22.7,-8.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgNAaIAAgIIAJAAIAAgeIgJAGIAAgJQAHgEAEgGIAIAAIAAArIAIAAIAAAIg");
	this.shape_18.setTransform(13.4,-18.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgQAaIAAgKQAOgMADgEQADgFAAgFQAAgGgEAAQgCAAgCADIgCAGIgKgCQACgQAOAAQAIAAAEAEQAEAFAAAGQAAAHgFAEQgEAHgMAIIAWAAIgBAKg");
	this.shape_19.setTransform(1.8,-21.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgNAaIAAgJIAJAAIAAgcIgJAFIAAgKQAHgDAEgGIAIAAIAAAqIAIAAIAAAJg");
	this.shape_20.setTransform(-1.9,-21.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#D75496").s().p("AjwExQgRgOgRgQIAAAAQgvgvgbg4QgohNAAhfQAAifByhyQByhzCgAAQChAABxBzQBzByAACfQAACihyBxIgBAAQhxByihgBQiJAAhnhTgAj/j+QhqBpAACVQAABfArBNQAZAtAmAnIAUATQATAQAUANQBWA5BuAAQCHABBkhXIAUgTQBrhqAAiWQAAiVhrhpQhqhriVAAQiVAAhqBrg");
	this.shape_21.setTransform(-0.4,5.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E0D6E9").s().p("AhTE1QgqgMgmgWQgRgKgPgNQgPgMgPgOIgCgDQgXgYgSgbQgegsgMg0QgJgkAAgoQAAgvANgqQAVhMA8g9IAGgGQBAg8BQgTQAkgIAoAAQAuAAArALQBMAWA9A8IAAABQA+A9AVBPQAKApAAAsQAAArgKAoQgVBQg+A/IgCABQgcAcgfAUQgnAWgqAMQgoALgsAAQgrAAgpgLg");
	this.shape_22.setTransform(-0.5,5.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#EEAACD").s().p("AhWFDQg4gPgughIgIgFQgUgQgSgSIgDgDQgcgcgTggQgcgrgLgyQgJgmAAgqQAAgwAMgsQAXhQA/g/IAGgGQBCg/BTgTQAmgJApAAQAxAAArANQBRAVA/A/QBABBAWBSQALAqAAAuQAACLhhBhIgCABQgVAWgYAQQgvAhg2APQgqAKguAAQgtAAgpgKgAhMk3QhQATg/A9IgHAGQg8A8gVBNQgNAqAAAuQAAAoAJAkQAMA1AeAsQASAbAYAYIABADQAPAOAPAMQAPAMARAKQAmAXArAMQAoAKArAAQAsAAAogKQArgMAmgXQAggTAcgcIABgBQA/g/AUhRQAKgnAAgsQAAgsgKgoQgVhPg+g9IAAgBQg9g8hMgWQgqgLgvAAQgoAAgkAHg");
	this.shape_23.setTransform(-0.5,5.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#BB387A").s().p("AjEEwQgTgNgUgQIgUgTQgmgngZgsQgrhOAAhfQAAiVBqhpQBqhrCVAAQCVAABrBrQBqBpAACVQAACWhqBqIgUATQhlBXiHAAQhtAAhXg6gAhPlDQhUAThBA/IgHAGQg+A/gXBQQgNAsAAAwQABAqAIAmQAMAyAcArQATAgAbAcIADADQATASAUAQIAIAFQAuAhA4APQApAKAtAAQAuAAApgKQA3gPAvghQAYgQAUgWIADgBQBhhhAAiLQAAgugMgqQgVhShAhBQg/g/hRgVQgrgNgxAAQgpAAgmAJg");
	this.shape_24.setTransform(-0.4,5.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F3409A").s().p("AA+CmIi1kuIAxgcIC+FKg");
	this.shape_25.setTransform(21.2,42.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F3409A").s().p("Ah3CmIC9lKIAyAcIi2Eug");
	this.shape_26.setTransform(-20.6,42.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#ECE9E9").s().p("AFwLnIgIAAQkvgDjWjXQjZjZAAk0QAAkzDZjaQDWjWEvgDIAIAAIAHAAIAAXNIgHAAg");
	this.shape_27.setTransform(-37.6,0);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF6666").s().p("AAAM3IgHAAQlQgDjujvQjwjwAAlVQAAlUDwjxQDujuFQgDIAHAAIAHAAQFQADDvDuQDxDxgBFUQABFVjxDwQjvDvlQADIgHAAgAooooQjlDlAAFDQAAFEDlDlQDiDiE/ACIAHAAIAHAAQE/gCDijiQDljlABlEQgBlDjljlQjijik/gCIgHAAIgHAAQk/ACjiDig");
	this.shape_28.setTransform(-0.8,0);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFCC00").s().p("AAAMNIgHAAQk/gCjijiQjljlAAlEQAAlDDljlQDijiE/gCIAHAAIAHAAQE/ACDiDiQDlDlABFDQgBFEjlDlQjiDik/ACIgHAAgAoMoNQjZDaAAEzQAAE0DZDZQDWDXEvADIAHAAIAHAAQEvgDDXjXQDajZAAk0QAAkzjajaQjXjWkvgDIgHAAIgHAAQkvADjWDWg");
	this.shape_29.setTransform(-0.8,0);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AlvrmQEvADDWDWQDaDaAAEzQAAE0jaDZQjWDXkvADg");
	this.shape_30.setTransform(36.6,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]}).wait(1));

	// Layer 4
	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("rgba(0,0,0,0.027)").s().p("Ap/KAQkJkJAAl3QAAl2EJkJQEJkJF2AAQF3AAEJEJQEJEJAAF2QAAF3kJEJQkJEIl3AAQl2AAkJkIg");
	this.shape_31.setTransform(-0.8,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_31).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch2, new cjs.Rectangle(-91.3,-90.4,181,180.9), null);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ch2 = new lib.ch2();
	this.ch2.name = "ch2";
	this.ch2.parent = this;
	this.ch2.setTransform(373.7,0,1.71,1.71);

	this.ch1 = new lib.inside_fruitcopy();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(-370.8,0,1.71,1.71);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ch1},{t:this.ch2}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-526.9,-154.6,1053.9,309.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ch1 = new lib.inside_fruitcopy2();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(3,-2.6,1.66,1.678,0,0,0,0.4,0.4);

	this.instance = new lib.quscopy4();
	this.instance.parent = this;
	this.instance.setTransform(2,-2.2,1.309,1.309,0,0,0,110.7,110.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.ch1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-156.1,-156.1,312.3,312.4), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.quscopy2();
	this.instance.parent = this;
	this.instance.setTransform(2,-2.3,1.309,1.309,0,0,0,110.7,110.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:110.6,scaleX:1.35,scaleY:1.35,x:1.9},9).to({regX:110.7,scaleX:1.31,scaleY:1.31,x:2},10).to({regX:110.6,scaleX:1.35,scaleY:1.35,x:1.9},10).to({regX:110.7,scaleX:1.31,scaleY:1.31,x:2},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-156.1,-156.1,312.3,312.4);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-243.2,-33.7,472.2,63.6);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.Tween1copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2("synched",12);
	this.instance.parent = this;
	this.instance.setTransform(-97.4,45.3);

	this.instance_1 = new lib.Symbol1("synched",12);
	this.instance_1.parent = this;
	this.instance_1.setTransform(24.1,-170.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-256.7,-204.7,514.6,409.5);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(644.8,335.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(286).to({_off:false},0).wait(26).to({startPosition:0},0).to({alpha:0,startPosition:10},7).wait(11));

	// Layer_11
	this.instance_1 = new lib.Symbol3copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(639.8,312.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(272).to({_off:false},0).to({scaleX:1.1,scaleY:1.1,y:312.3},14).wait(26).to({alpha:0},7).wait(11));

	// Layer_2
	this.instance_2 = new lib.Symbol1copy("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(401.7,628.7,1,1,-14.9,0,0,190.5,64.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(205).to({_off:false},0).to({_off:true},34).wait(91));

	// arrow
	this.instance_3 = new lib.Symbol1_1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(269.3,348.5,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(174).to({_off:false},0).to({_off:true},35).wait(121));

	// Layer_1
	this.ch1 = new lib.inside_fruitcopy2();
	this.ch1.name = "ch1";
	this.ch1.parent = this;
	this.ch1.setTransform(269.2,566.1,1.71,1.71);
	this.ch1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.ch1).wait(240).to({_off:false},0).to({regX:0.4,regY:0.4,scaleX:1.66,scaleY:1.68,x:642.8,y:309.8},27).wait(9).to({alpha:0},5).wait(49));

	// Layer_3
	this.instance_4 = new lib.quscopy5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);

	this.instance_5 = new lib.quscopy4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(641.8,310.2,1.309,1.309,0,0,0,110.7,110.7);

	this.instance_6 = new lib.quscopy();
	this.instance_6.parent = this;
	this.instance_6.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},240).to({state:[{t:this.instance_5}]},27).to({state:[{t:this.instance_6}]},9).to({state:[{t:this.instance_6}]},5).wait(49));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(276).to({_off:false},0).to({alpha:0},5).wait(49));

	// Layer_9
	this.instance_7 = new lib.quscopy3();
	this.instance_7.parent = this;
	this.instance_7.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(130).to({_off:false},0).to({x:1013,y:549.1},15).wait(28).to({alpha:0},6).to({_off:true},1).wait(150));

	// Layer_8
	this.instance_8 = new lib.quscopy3();
	this.instance_8.parent = this;
	this.instance_8.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(130).to({_off:false},0).to({scaleX:1.34,scaleY:1.34,x:268.2,y:567.1},15).wait(28).to({alpha:0},6).to({_off:true},1).wait(150));

	// Layer_7
	this.instance_9 = new lib.Symbol2("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(639.9,312.4);

	this.instance_10 = new lib.Symbol1("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(761.4,96.8);

	this.instance_11 = new lib.Tween4("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(737.3,267.1);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_10},{t:this.instance_9}]},91).to({state:[{t:this.instance_11}]},82).to({state:[{t:this.instance_11}]},6).to({state:[]},1).wait(150));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(173).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(150));

	// Layer_6
	this.instance_12 = new lib.Tween1("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,566.1);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(60).to({_off:false},0).to({alpha:1},6).to({startPosition:0},99).to({startPosition:0},29).to({startPosition:0},88).to({alpha:0},5).wait(43));

	// Layer_5
	this.instance_13 = new lib.qus();
	this.instance_13.parent = this;
	this.instance_13.setTransform(641.9,310,1.309,1.309,0,0,0,110.7,110.6);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(37).to({_off:false},0).to({alpha:1},6).to({regY:110.7,x:641.8,y:310.2},195).to({regY:110.6,x:641.9,y:310},44).to({alpha:0},5).wait(43));

	// Layer_4
	this.questxt = new lib.InstructionText_mc();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(638.7,119.2,1.539,1.539,0,0,0,0.1,0.1);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(13).to({_off:false},0).to({alpha:1},6).wait(263).to({alpha:0},5).wait(43));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(547.3,296.3,1468,851);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;