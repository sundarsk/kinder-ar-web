(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape.setTransform(233.4,6.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhxA8IgCg8IgCgvIgBgjIgBg0IA3gBIABBGQAIgNAKgNQAKgMAMgJQAMgKANgGQANgGAPgBQATgBANAFQAOAEAKAIQAJAHAHALQAGALAEALIAFAWIACAVQACApgBAqIgCBYIgygCIAEhOQABgngCgnIAAgLIgDgPQgCgIgEgIQgDgHgGgGQgGgGgIgDQgJgDgLACQgTADgTAZQgUAZgXAwIACA+IABAjIABATIg0AHIgDhMg");
	this.shape_1.setTransform(203,6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AgyCFQgYgLgRgSQgSgUgKgZQgKgbAAggQAAggAKgZQAKgbASgTQARgSAYgLQAYgKAaAAQAbAAAXALQAYALASATQASATAKAaQAKAaAAAeQAAAfgKAZQgKAbgSATQgSATgYALQgXALgbAAQgaAAgYgKgAgjhaQgPAJgJAOQgKAPgEARQgEASAAARQAAARAEASQAFASAKAOQAJAOAPAJQAPAJATAAQAUAAAPgJQAPgJAKgOQAKgOAEgSQAFgSAAgRQAAgRgFgSQgEgRgJgPQgKgOgPgJQgPgJgVAAQgUAAgPAJg");
	this.shape_2.setTransform(172.8,6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AAPDTQgNgFgJgJQgKgJgHgLQgIgMgEgMQgMgbgDgjIAHkzIAxAAIgCBPIgCBCIgBA1IAAAnIgCBDQABAWAEASQADAHAEAIQAEAHAGAGQAFAFAJAEQAHAEALAAIgGAwQgRAAgOgGg");
	this.shape_3.setTransform(139.4,-2.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AAPDTQgNgFgJgJQgKgJgHgLQgIgMgEgMQgMgbgDgjIAHkzIAxAAIgCBPIgCBCIgBA1IAAAnIgCBDQABAWAEASQADAHAFAIQADAHAGAGQAGAFAHAEQAIAEALAAIgGAwQgRAAgOgGg");
	this.shape_4.setTransform(125.3,-2.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("ABLCLIADgjQgdATgcAJQgcAIgZAAQgRAAgRgEQgRgFgMgJQgNgJgHgPQgIgOABgUQgBgUAGgPQAGgQAKgMQAKgLAOgIQAOgJAQgFQAQgFASgDQAQgDARAAQAQAAAOACIAZADIgIgYQgFgMgHgJQgHgKgLgFQgKgGgOAAQgKAAgLADQgMACgPAIQgOAHgSANQgRAMgUAUIgdgjQAXgWAVgOQAWgOATgIQATgIAQgDQAPgDANAAQAWAAARAIQASAIAOANQANANAJASQAJASAGAVQAGAUADAWQACAVAAAWQAAAXgDAbQgCAbgGAggAgLgBQgVAEgPAKQgPAKgGAOQgJAOAFASQADAPALAFQAJAGAOAAQAOAAARgEQAPgFAQgHIAfgPIAYgOIAAgYQABgMgCgNIgYgFQgNgCgOAAQgVAAgUAFg");
	this.shape_5.setTransform(100.6,5.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AB1A6IgBg1IAAgnQgBgTgGgKQgGgJgJAAQgGgBgGAEQgGAEgGAFIgNAOIgLAPIgKAPIgJANIABAWIABAeIABAkIAAAuIguADIgBhMIgCg1IgDgnQAAgTgGgKQgGgJgJAAQgGAAgHADQgGAEgHAHIgNAPIgMARIgLARIgJAMIACCAIgwADIgHkFIAzgGIABBIIAQgUQAJgLAKgIQAKgJAMgGQAMgFAOAAQAKAAAKAEQAJACAHAIQAIAGAFALQAFAKACAPIAQgUQAIgJAKgJQAKgIALgFQAMgFAOAAQAKAAALAEQAKADAIAIQAIAHAFAMQAFANAAAQIADAtIACA6IABBVIgzADIAAhMg");
	this.shape_6.setTransform(65.5,5.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgjCJIgUgEIgXgIIgWgKQgLgGgKgIIAYglIAbANQAOAHAOAFQAPAFAPADQAOADARAAQANAAAJgDQAJgCAHgEQAFgEAEgEIAFgKQABgEAAgEQgBgFgCgFQgCgFgFgEQgEgFgJgEQgHgEgMgCQgLgDgPAAQgVgBgUgEQgUgDgQgIQgRgIgJgLQgLgMgCgTQgCgRAFgOQADgPAJgLQAHgMAMgIQANgJAOgGQANgGAQgCQAQgDAPAAIAWABQANABAOADQAOADAOAFQAPAGAMAJIgQAsQgRgIgOgFIgagIQgNgDgLAAQglgDgVALQgWAKAAAUQAAAPAIAHQAJAGANADQAOADATABQARABAVADQAYAEAQAHQAQAHAKAJQAKAKAFAMQAEAMAAAMQAAAYgKAPQgJAQgPAKQgQAKgUAFQgUAEgWABQgWAAgXgFg");
	this.shape_7.setTransform(31.4,6.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape_8.setTransform(-9.9,6.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("ABBC5QAHgXADgWIAFgoIAAgnQgBgagIgTQgIgQgLgMQgMgMgNgFQgNgFgOAAQgNABgOAHQgMAGgOAMQgOALgMAWIgBCiIgvABIgFl2IA4gCIgCCUQAOgPAPgIQAQgIANgFQAPgFANgCQAbAAAXAKQAXAKAQASQAPASAKAaQAJAZACAiIgBAjIgBAjIgFAhQgBAQgFAMg");
	this.shape_9.setTransform(-40.9,0.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgdgcIhUACIABgsIBUgDIAChqIAwgDIgCBsIBegDIgDAuIhbACIgDDTIgyABg");
	this.shape_10.setTransform(-69.3,1.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgdgcIhUACIABgsIBUgDIAChqIAvgDIgBBsIBegDIgEAuIhaACIgCDTIgzABg");
	this.shape_11.setTransform(-106,1.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgsCDQgagLgUgTQgUgTgMgaQgMgZAAgfQAAgRAFgRQAFgSAJgQQAJgQAOgNQAMgOAQgJQAQgKATgFQASgGATAAQAVAAATAFQAUAFAPAKQARAKANAOQAMANAKASIAAABIgpAYIAAgBQgHgMgIgKQgKgKgLgHQgKgHgNgEQgNgDgOAAQgTAAgRAHQgSAIgNANQgNANgIASQgHARAAATQAAAUAHARQAIASANANQANANASAIQARAHATAAQANAAAMgDQAMgDALgHQALgGAJgJQAJgJAGgLIABgBIArAYIgBABQgJAQgOANQgOANgQAJQgRAJgSAFQgTAFgTAAQgbAAgZgLg");
	this.shape_12.setTransform(-132.8,6.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape_13.setTransform(-162.5,6.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AAPDTQgNgFgJgJQgKgJgIgLQgHgMgFgMQgMgbgCgjIAGkzIAyAAIgCBPIgCBCIgBA1IgBAnIgBBDQAAAWAGASQACAHAEAIQAEAHAGAGQAFAFAJAEQAHAEALAAIgHAwQgRAAgNgGg");
	this.shape_14.setTransform(-183,-2.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape_15.setTransform(-206,6.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgfDEQgTgFgRgGQgSgIgRgKQgRgKgRgMIAkgpQATAQASAKQATAJAPAFQARAGAPACQARABAPgDQAPgFAMgGQALgHAHgJQAGgKACgLQAAgJgDgHQgEgIgIgGQgHgGgLgFQgKgFgLgDQgMgEgMgDIgXgFIgdgGQgPgEgPgHQgPgGgNgKQgNgJgKgOQgKgOgEgRQgGgTACgYQACgTAGgQQAHgPALgNQAKgMAOgJQAOgJAPgGQAQgFARgDQAQgDAQABQAWABAYAGIAUAGQALADALAGQALAFALAIQAKAGAKAKIgbAqQgIgIgJgGIgRgMIgSgJIgRgFQgSgFgSgBQgVABgSAGQgRAHgMALQgMAKgGANQgGANAAAMQAAANAIALQAHAMANAJQANAKASAHQASAGATADQASACASAFQARADAQAGQAQAIAOAJQANAJAJANQAKANAEAQQAEAPgDATQgCARgHAOQgHANgLAJQgLAKgNAHQgNAGgPAEQgOAFgPABQgPACgNAAQgSAAgSgDg");
	this.shape_16.setTransform(-237,1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FF6600").ss(3,1,1).p("Ego2ABSQABCFBNBdQBNBcBtAAMBJigAFQBsAABKheQBNhcgBiFIAAidQAAiDhNhfQhNhchsAAMhJhAAGQhsAAhNBdQhMBeABCEg");
	this.shape_17.setTransform(0,1.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFCC00").s().p("EgnoAE0QhNhdgBiFIABicQgBiFBMhdQBNhdBsAAMBJhgAGQBsAABNBcQBNBfAACDIAACcQABCGhNBcQhKBehsAAMhJiAAFQhtAAhNhcg");
	this.shape_18.setTransform(0,1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-262.9,-41.3,525.9,84.1);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape.setTransform(233.4,6.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhxA8IgCg8IgCgvIgBgjIgBg0IA3gBIABBGQAIgNAKgNQAKgMAMgJQAMgKANgGQANgGAPgBQATgBANAFQAOAEAKAIQAJAHAHALQAGALAEALIAFAWIACAVQACApgBAqIgCBYIgygCIAEhOQABgngCgnIAAgLIgDgPQgCgIgEgIQgDgHgGgGQgGgGgIgDQgJgDgLACQgTADgTAZQgUAZgXAwIACA+IABAjIABATIg0AHIgDhMg");
	this.shape_1.setTransform(203,6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AgyCFQgYgLgRgSQgSgUgKgZQgKgbAAggQAAggAKgZQAKgbASgTQARgSAYgLQAYgKAaAAQAbAAAXALQAYALASATQASATAKAaQAKAaAAAeQAAAfgKAZQgKAbgSATQgSATgYALQgXALgbAAQgaAAgYgKgAgjhaQgPAJgJAOQgKAPgEARQgEASAAARQAAARAEASQAFASAKAOQAJAOAPAJQAPAJATAAQAUAAAPgJQAPgJAKgOQAKgOAEgSQAFgSAAgRQAAgRgFgSQgEgRgJgPQgKgOgPgJQgPgJgVAAQgUAAgPAJg");
	this.shape_2.setTransform(172.8,6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AAPDTQgNgFgJgJQgKgJgHgLQgIgMgEgMQgMgbgDgjIAHkzIAxAAIgCBPIgCBCIgBA1IAAAnIgCBDQABAWAEASQADAHAEAIQAEAHAGAGQAFAFAJAEQAHAEALAAIgGAwQgRAAgOgGg");
	this.shape_3.setTransform(139.4,-2.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AAPDTQgNgFgJgJQgKgJgHgLQgIgMgEgMQgMgbgDgjIAHkzIAxAAIgCBPIgCBCIgBA1IAAAnIgCBDQABAWAEASQADAHAFAIQADAHAGAGQAGAFAHAEQAIAEALAAIgGAwQgRAAgOgGg");
	this.shape_4.setTransform(125.3,-2.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("ABLCLIADgjQgdATgcAJQgcAIgZAAQgRAAgRgEQgRgFgMgJQgNgJgHgPQgIgOABgUQgBgUAGgPQAGgQAKgMQAKgLAOgIQAOgJAQgFQAQgFASgDQAQgDARAAQAQAAAOACIAZADIgIgYQgFgMgHgJQgHgKgLgFQgKgGgOAAQgKAAgLADQgMACgPAIQgOAHgSANQgRAMgUAUIgdgjQAXgWAVgOQAWgOATgIQATgIAQgDQAPgDANAAQAWAAARAIQASAIAOANQANANAJASQAJASAGAVQAGAUADAWQACAVAAAWQAAAXgDAbQgCAbgGAggAgLgBQgVAEgPAKQgPAKgGAOQgJAOAFASQADAPALAFQAJAGAOAAQAOAAARgEQAPgFAQgHIAfgPIAYgOIAAgYQABgMgCgNIgYgFQgNgCgOAAQgVAAgUAFg");
	this.shape_5.setTransform(100.6,5.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AB1A6IgBg1IAAgnQgBgTgGgKQgGgJgJAAQgGgBgGAEQgGAEgGAFIgNAOIgLAPIgKAPIgJANIABAWIABAeIABAkIAAAuIguADIgBhMIgCg1IgDgnQAAgTgGgKQgGgJgJAAQgGAAgHADQgGAEgHAHIgNAPIgMARIgLARIgJAMIACCAIgwADIgHkFIAzgGIABBIIAQgUQAJgLAKgIQAKgJAMgGQAMgFAOAAQAKAAAKAEQAJACAHAIQAIAGAFALQAFAKACAPIAQgUQAIgJAKgJQAKgIALgFQAMgFAOAAQAKAAALAEQAKADAIAIQAIAHAFAMQAFANAAAQIADAtIACA6IABBVIgzADIAAhMg");
	this.shape_6.setTransform(65.5,5.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgjCJIgUgEIgXgIIgWgKQgLgGgKgIIAYglIAbANQAOAHAOAFQAPAFAPADQAOADARAAQANAAAJgDQAJgCAHgEQAFgEAEgEIAFgKQABgEAAgEQgBgFgCgFQgCgFgFgEQgEgFgJgEQgHgEgMgCQgLgDgPAAQgVgBgUgEQgUgDgQgIQgRgIgJgLQgLgMgCgTQgCgRAFgOQADgPAJgLQAHgMAMgIQANgJAOgGQANgGAQgCQAQgDAPAAIAWABQANABAOADQAOADAOAFQAPAGAMAJIgQAsQgRgIgOgFIgagIQgNgDgLAAQglgDgVALQgWAKAAAUQAAAPAIAHQAJAGANADQAOADATABQARABAVADQAYAEAQAHQAQAHAKAJQAKAKAFAMQAEAMAAAMQAAAYgKAPQgJAQgPAKQgQAKgUAFQgUAEgWABQgWAAgXgFg");
	this.shape_7.setTransform(31.4,6.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape_8.setTransform(-9.9,6.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("ABBC5QAHgXADgWIAFgoIAAgnQgBgagIgTQgIgQgLgMQgMgMgNgFQgNgFgOAAQgNABgOAHQgMAGgOAMQgOALgMAWIgBCiIgvABIgFl2IA4gCIgCCUQAOgPAPgIQAQgIANgFQAPgFANgCQAbAAAXAKQAXAKAQASQAPASAKAaQAJAZACAiIgBAjIgBAjIgFAhQgBAQgFAMg");
	this.shape_9.setTransform(-40.9,0.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AgdgcIhUACIABgsIBUgDIAChqIAwgDIgCBsIBegDIgDAuIhbACIgDDTIgyABg");
	this.shape_10.setTransform(-69.3,1.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgdgcIhUACIABgsIBUgDIAChqIAvgDIgBBsIBegDIgEAuIhaACIgCDTIgzABg");
	this.shape_11.setTransform(-106,1.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AgsCDQgagLgUgTQgUgTgMgaQgMgZAAgfQAAgRAFgRQAFgSAJgQQAJgQAOgNQAMgOAQgJQAQgKATgFQASgGATAAQAVAAATAFQAUAFAPAKQARAKANAOQAMANAKASIAAABIgpAYIAAgBQgHgMgIgKQgKgKgLgHQgKgHgNgEQgNgDgOAAQgTAAgRAHQgSAIgNANQgNANgIASQgHARAAATQAAAUAHARQAIASANANQANANASAIQARAHATAAQANAAAMgDQAMgDALgHQALgGAJgJQAJgJAGgLIABgBIArAYIgBABQgJAQgOANQgOANgQAJQgRAJgSAFQgTAFgTAAQgbAAgZgLg");
	this.shape_12.setTransform(-132.8,6.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape_13.setTransform(-162.5,6.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AAPDTQgNgFgJgJQgKgJgIgLQgHgMgFgMQgMgbgCgjIAGkzIAyAAIgCBPIgCBCIgBA1IgBAnIgBBDQAAAWAGASQACAHAEAIQAEAHAGAGQAFAFAJAEQAHAEALAAIgHAwQgRAAgNgGg");
	this.shape_14.setTransform(-183,-2.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("Ag2CCQgZgJgRgSQgRgSgJgZQgJgZAAgfQAAgdALgbQAKgaASgTQASgUAYgLQAZgLAbAAQAZAAAWAKQAXAJARASQARARALAYQAMAXADAcIjSAeQACARAHAOQAGAOALAJQAKAKAPAFQANAFAQAAQANAAAMgEQANgEALgIQAKgHAIgMQAIgLAEgPIAvAJQgHAWgNASQgMASgQANQgRANgUAHQgUAHgVAAQgfAAgZgKgAgUheQgMAEgLAIQgMAJgJAOQgJAOgEAVICSgSIgCgEQgJgYgQgOQgQgNgYAAQgJAAgNADg");
	this.shape_15.setTransform(-206,6.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgfDEQgTgFgRgGQgSgIgRgKQgRgKgRgMIAkgpQATAQASAKQATAJAPAFQARAGAPACQARABAPgDQAPgFAMgGQALgHAHgJQAGgKACgLQAAgJgDgHQgEgIgIgGQgHgGgLgFQgKgFgLgDQgMgEgMgDIgXgFIgdgGQgPgEgPgHQgPgGgNgKQgNgJgKgOQgKgOgEgRQgGgTACgYQACgTAGgQQAHgPALgNQAKgMAOgJQAOgJAPgGQAQgFARgDQAQgDAQABQAWABAYAGIAUAGQALADALAGQALAFALAIQAKAGAKAKIgbAqQgIgIgJgGIgRgMIgSgJIgRgFQgSgFgSgBQgVABgSAGQgRAHgMALQgMAKgGANQgGANAAAMQAAANAIALQAHAMANAJQANAKASAHQASAGATADQASACASAFQARADAQAGQAQAIAOAJQANAJAJANQAKANAEAQQAEAPgDATQgCARgHAOQgHANgLAJQgLAKgNAHQgNAGgPAEQgOAFgPABQgPACgNAAQgSAAgSgDg");
	this.shape_16.setTransform(-237,1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FF6600").ss(3,1,1).p("Ego2ABSQABCFBNBdQBNBcBtAAMBJigAFQBsAABKheQBNhcgBiFIAAidQAAiDhNhfQhNhchsAAMhJhAAGQhsAAhNBdQhMBeABCEg");
	this.shape_17.setTransform(0,1.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFCC00").s().p("EgnoAE0QhNhdgBiFIABicQgBiFBMhdQBNhdBsAAMBJhgAGQBsAABNBcQBNBfAACDIAACcQABCGhNBcQhKBehsAAMhJiAAFQhtAAhNhcg");
	this.shape_18.setTransform(0,1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-262.9,-41.3,525.9,84.1);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgyB5QgXgIgQgRQgQgQgIgYQgJgXAAgdQAAgbAKgZQAKgZAQgRQARgTAXgKQAWgLAaAAQAXAAAVAKQAVAJAQAQQAQARAKAVQALAWADAaIjDAcQABAQAGANQAHAMAKAJQAKAKANAEQANAFAOgBQAMAAALgDQAMgEAKgHQAKgHAIgKQAHgMADgOIAsAJQgGAVgMARQgLARgQALQgPANgTAGQgSAGgUAAQgdABgXgKgAgShXQgLADgLAIQgLAIgIAOQgJANgDATICHgRIgBgDQgJgXgPgNQgPgMgWAAQgJAAgLADg");
	this.shape.setTransform(93.1,6.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhpA4IgCg4IgBgrIgBghIgCgwIA0gBIABBBQAHgNAKgLQAJgMALgJQALgIAMgGQANgFANgBQASgBAMAEQANAEAJAHQAJAHAGAKQAGAKADALQAEAKABALIACATQACAlgBAoIgCBSIgugCIADhJQACgkgCgkIgBgLIgCgOIgGgOQgDgHgFgFQgGgGgHgDQgIgCgLABQgSADgRAYQgTAXgVAsIACA6IABAhIAAARIgwAHIgDhHg");
	this.shape_1.setTransform(64.7,5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AguB8QgWgKgRgRQgQgSgKgZQgJgYAAgeQAAgdAJgZQAKgYAQgSQARgRAWgKQAWgJAYAAQAZAAAWAKQAWALARARQAQASAKAYQAJAYAAAcQAAAdgJAYQgKAYgQASQgRASgWAKQgWAKgZAAQgYAAgWgJgAgghTQgOAHgJAOQgJANgEARQgEAQAAAQQAAAQAFARQAEAQAJANQAJANAOAJQANAJASgBQATABAOgJQAOgJAJgNQAJgNAEgQQAFgRAAgQQAAgQgFgQQgEgRgIgNQgJgOgOgHQgOgJgUAAQgSAAgOAJg");
	this.shape_2.setTransform(36.6,5.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AAODFQgNgFgHgIQgKgJgHgKQgGgLgFgLQgLgagDggIAHkeIAuAAIgCBKIgCA9IgBAxIAAAlIgCA9QABAWAEAQIAHAOQADAHAGAFQAFAGAHADQAIADAJAAIgGAtQgPAAgNgFg");
	this.shape_3.setTransform(5.5,-2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AAODFQgNgFgHgIQgKgJgHgKQgGgLgFgLQgLgagDggIAHkeIAuAAIgCBKIgCA9IgBAxIAAAlIgCA9QABAWAEAQIAHAOQADAHAGAFQAFAGAHADQAIADAJAAIgGAtQgPAAgNgFg");
	this.shape_4.setTransform(-7.5,-2.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("ABGCCIADghQgbASgbAIQgaAIgWAAQgRAAgPgEQgPgFgMgIQgMgJgHgNQgHgOAAgSQAAgTAGgOQAFgPAKgKQAJgLANgHQAMgJAPgFQAPgEARgDQAPgCAQAAQAPAAANABIAXADQgDgLgFgMQgEgLgGgIQgHgJgKgGQgJgFgPAAQgIAAgLADQgLADgOAGQgNAHgQAMQgQAMgTASIgbghQAWgVAUgMQAUgNARgHQASgIAPgCQAOgEAMAAQAVAAAQAIQARAHAMAMQAMAMAIASQAJAQAGATQAFATADAVQACATAAAVQgBAWgCAZQgDAZgEAdgAgKgBQgTAFgOAJQgOAJgHAMQgHAOAFARQACANAKAGQAJAFAMAAQAOAAAPgEQAPgFAPgGIAcgOIAXgNIAAgXQAAgLgBgLQgLgDgMgCQgNgCgMAAQgUAAgSAEg");
	this.shape_5.setTransform(-30.5,5.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("ABtA2IgBgxIAAgkQgBgTgFgJQgGgJgIAAQgGAAgGAEQgGADgFAGIgMAMIgKAOIgKAOIgIAMIABAVIABAbIABAiIAAArIgrACIgBhGIgCgxIgCgkQgBgTgFgJQgFgJgJAAQgGAAgGAEQgGAEgGAGIgMAOIgMAQIgKAPIgIAMIACB3IgtACIgGjyIAvgFIABBCIAPgTQAIgKAKgIQAJgIALgFQALgFANAAQAKAAAJADQAIADAHAHQAHAGAFAKQAFAKABANIAPgSQAIgJAJgHQAJgIALgFQALgFAMAAQAKAAAKAEQAJADAIAHQAHAHAFAMQAFALAAAPIADAqIACA2IAABPIgvACIAAhGg");
	this.shape_6.setTransform(-63.2,5.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AggCAIgTgEIgVgHIgVgKQgKgGgJgHIAVgiIAaAMQAMAGAOAFQAOAFAOACQAOADAOAAQANAAAJgCQAIgDAGgDQAFgEADgEIAFgJQABgDAAgFQAAgEgCgFQgDgEgEgEQgEgFgIgDQgHgEgKgCIgZgDQgTgBgUgDQgTgEgOgHQgPgHgKgKQgKgMgBgRQgCgQAEgNQAEgOAHgLQAIgKALgIQALgIANgGQANgFAPgCQAPgDANAAIAVABIAZADQANADANAFQAOAFALAJIgPApQgPgIgOgEIgXgHIgXgEQgjgCgTAKQgVAJABATQgBAOAIAGQAIAGANADQANADAQABQARABATADQAXADAOAHQAQAGAJAJQAJAJAFALQADALAAAMQAAAVgIAPQgJAOgPAKQgOAJgTAEQgTAFgUAAQgUAAgVgEg");
	this.shape_7.setTransform(-95,6.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(3,1,1).p("AvtkzIfbAAQCVAAAACVIAAE8QAACWiVAAI/bAAQiVAAAAiWIAAk8QAAiVCVAAg");
	this.shape_8.setTransform(0,0.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AvtEzQiVABAAiVIAAk9QAAiVCVABIfbAAQCVgBAACVIAAE9QAACViVgBg");
	this.shape_9.setTransform(0,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-116.9,-38.1,234,76.3);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#585153").s().p("AgFAJIgEgDIAAgEQgCgEAEgFQAEgEAFABQAIACABAGQAAAGgFAEQAAABgBAAQAAABAAAAQgBAAgBAAQAAABgBAAIgCAAQgCAAgDgCg");
	this.shape.setTransform(175.6,68.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgLBFQgbgPgdgKQgQgUAAgYQAAgfAagWQAYgVAhgBQAjABAYAVQAZAWAAAfQAAAfgZAXQgYAVgjAAIgLgGgAAagwQgEAFACAFIAAAEIAEADQAEADAEgBQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQAFgEAAgGQgBgHgIgBIgCgBQgFAAgDADg");
	this.shape_1.setTransform(172.2,72.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B66D26").s().p("Ag5A2QgagWAAggQAAgfAagVQAYgXAhAAQAjAAAYAXQAZAVAAAfQAAAggZAWQgYAWgjAAQghAAgYgWgAAagwQgEAFACAGIAAADIAEADQAEADAEAAQABgBAAAAQABAAAAAAQABgBAAAAQABAAAAgBQAFgEAAgHQgBgGgIgCIgDgBQgEAAgDADg");
	this.shape_2.setTransform(172.2,71.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#585153").s().p("AgGAKIgEgEIgBgDQgBgGAFgFQAEgEAGABQAHACACAHQABAHgFAFIgFACIgCAAQgDAAgEgCg");
	this.shape_3.setTransform(133.9,68.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C1819").s().p("AhAA+QgcgZAAgkQAAgiAcgYQAbgZAlAAQAmAAAcAZQAbAYAAAiQAAARgGAOQgbAHgaALQggAOgcATQgWgGgQgPgAAdgzQgFAFACAGIABAEIAEADQAEAEAFgBIAFgDQAFgFgBgHQgBgHgIgDIgCAAQgFAAgEAEg");
	this.shape_4.setTransform(130.1,72.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B66D26").s().p("AhAA8QgcgZAAgjQAAgiAcgYQAbgaAlAAQAmAAAcAaQAbAYAAAiQAAAZgNAVQgXAGgXAJQgXAKgWANQgfgDgWgVgAAdg0QgFAEACAHIABAEIAEADQAEADAFgBIAFgCQAFgFgBgIQgBgGgIgDIgCAAQgFAAgEAEg");
	this.shape_5.setTransform(130.1,71.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6F7074").s().p("AAeAVQgEgBgIgEIgLgFQgKgDgZACQgIABgFgCQgJgDgBgIQgCgKAIgFQAGgEAOAAQAYAAAGACQAIABASAGQAMAGAEADQAIAIgDAIQgCAGgHACIgHABIgGgBg");
	this.shape_6.setTransform(156.6,99.7,0.837,0.837);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1C1819").s().p("AgjBRQgIgDgKgMQgIgLgQgfQgTgjgBgVQgBgQAHgNQAHgPANgIIASgBIBEABIAXACQAMABAJADQAZAFAJASQAIAQgDAZQgHAlgdAcQgeAdgmAFIgJABQgNAAgHgFg");
	this.shape_7.setTransform(157.7,104.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(51,51,51,0.298)").ss(1,1,1,3,true).p("AHX2zQABgBABgCQAEgFADgFQAWgcAjgSQAzgYA2AHQA0AIAuAiQAsAfAeAvQAzBQAOB9QAOB0gYBSQgOAygeAnQgdAlglAVQgFACgEADQgfAQgkAEQgEABgDAAQgCAMgCALQgQBKghBEQgzBphbBZQgFAFgFAFQgRAPgSAPQgQANgQAMQgFAIgFAHIgBAAQgPATgTAQQBxCqAtDMQAiCbgICYIAVAfQA4BTAgApQA0BDA2ArQAjAdAnATAoZ2TQABgBABgBQAKgKAJgKQATgSAUgRQDCimEKAAQEXAADKC5QABABABABQACACADACQAFAFAFAFQDODJAAEbQAABCgLA8AiGkgQgegJgYgMQgTgKgQgMQgRgNgMgOQgZgcgNgnQgGgUgDgXQgEgfADgfQADgeAIggQAIgeAMgbQAbg8Atg0QBChMBZgmQBSgjBdACQAGAAAHAAQAMABAMACQBWAIBMApQBNAsAiA7QAEAIAEAKQAVAvgBA9QgBAWgDAZQgDAQgDAQQgLA0gWArQgDAGgDAFQgIAOgKANAFnltQgUAQgWANQgFADgFACQgeAPggAFQgLACgKABQgJAAgJgBQgFAAgEgBQgNgCgLgEQgOgGgLgIIAAgBQgFgEgEgFQgFADgEAEQgVAXgcAPQgKAFgLAEQgGACgGACQggAKgjABQgBAAgBAAQgOABgNgBQgpgDgsgOIgBAAQgngGglgJQhEgTg+gfQgUgKgUgMQgLgHgMgIQgTgMgTgOQgngegkgiQgcgcgZgdQAAgBgBgBQg4hDgihKQgYgygOg3QgYhVAAhfQAAkLC5jDAHXnWQC0CPCoCmQBpBoA8BQQBTBrAjBpQAzCSgqB1QgGAPgGAOQgSAjgZAfQAUACATAEQBVARBEA7QCGBygDDHQgDDKiKCqQg4BFhAAvIAAAAQgBABgBABIAAAAQhfBFhxAXQgCABgBAAQiYAfh0hCQgcgRgbgWQhvhegQibQgEggAAghQAEjJCKiqQBXhrBtg4QAVgLAWgJQAFgCAFgCQAngPAqgJQBGgOA/AGAq6sRQgFACgFADQglAOgqgBQgHAAgGgBQgugEgmgXQhGgsgcheQgYhRAMhiQAKhPAbhCQAfhKAygxQA2g4BIgXQBNgYBEAWQAbAIAaAQQAFAEAFADQADACACABAo5oZQgKAHgLAJQhsBLhPA8QhjBLhdBUQhWBOhNBWQhOBYg+BnQg6BggeBsQghBzAcB0QAdB6BvA+QArAYAsAIQAXgfAbgZQBCg5BcgVQAUgGAUgCQAsgGAtAEQABgBACgCQBQhZAahzQAFgWAGgVQAVhdAVhcQARhNAehJQAEgJAEgKQALgbAOgZQANgaAOgZQASgiATghQAZgqAbgrQAKgPAKgOQAQgYASgVQAEgGAFgFQATgXAVgVAgVkPQgCAAgBAAQgMAAgMAAAl8l3QABgCAAgDAnQSTQADAJADAKQAiB7glBrQgFAOgGANQgSAogZAfQg2BBhXAVQiAAeh9hVQhKgzgvhIQgggzgTg8QgviVA4h/QAPghAUgaQA3hLBfgWQAvgLAuAEQBRAJBNA1QB9BUAvCVgAshJ7QA6AGA6AYQAzAVAzAiQCsB1BCDOQAEANADAMQA3DBhLCkQgIASgJASQgcAyglAmQgrApg2AaQgkAPgnAKQgdAGgbADQgkADgjgDQhCgGhCgcQgtgVgsgeQgVgOgUgQQiKhwg7i1QhBjOBOivQAUguAbgkAD+O/QhFA6hSAmQh0A0h4gBQhhgBhsglASZVcQgpAxgtAiQg5AqhCATQgOAEgQAEQiKAdhhhSQgKgIgHgIQhBhBgKhkQgDgXAAgYQADiRBjh7QBMhdBhgoQAggMAigHQAwgKArADQACAAADAAQBOAIA+AzQBhBTgDCQQgCCThkB7gAIfE8QgBATgCAUQgGA/gNA/QgaB8g1BsQg4BzhTBUQgXAagaAVQAYArAiA+QAxBVAvA1QAWAYAZAW");
	this.shape_8.setTransform(137.9,165.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#552D15").s().p("ABGCVQgvgggog3QgfgmglhBQgZgsgNgQQgLgOgLgLIACgnIAVAfQA4BTAhApQAzBCA2ArQAiAdAoASQgWAKgWALQgRgJgPgJg");
	this.shape_9.setTransform(206.5,213.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#9C5824").s().p("AkOGpQgvhAgehNQgrhyAEh4QAFh2A1huQA1hvBahOQA0guA6gaQBAgeA+AAQBpgBBaBOQBVBIAiBsQArCHgmCWQgjCNhfB1QgPASgPAQIAAAAIgCACIAAAAQhfBFhxAXIgCABQgxAKguAAQhfAAhOgtgAApkfQgiAHgfAMQhhAohMBdQhjB7gDCQQAAAYADAXQAKBkBBBBQAHAIAKAIQBhBSCJgdIAegIQBCgTA5gqQAtgiApgxQBkh7ACiSQADiQhhhTQg+gzhOgIIgFAAIgXgBQghAAgjAIg");
	this.shape_10.setTransform(228.7,276.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F3E2B9").s().p("AppClQgdhnhChOQg9hKhXgdQhfghhIAuQglAXgZArQgXAngKAwQgHApABA0QABAfAHA/IACAZQgggygTg8QgviVA4h/QAPggAUgaQA3hMBfgVQAvgLAuAEQBRAIBNA1QB9BVAvCUIAGASQAiB7glBrQgFAPgGANQgSAogZAeQALhdgZhdgAP+BjQAoihg/iIQgRgjgTgMQgigVhCAeQiGA7hABXQg+BXgJCGQgFA/AJBeQhBhBgKhjQgDgXAAgZQADiPBjh8QBMhcBhgoQAggNAigHQAwgKArAEIAFAAQBOAHA+A0QBhBSgDCRQgCCRhkB7QgpAygtAiQg5AphCATQBphnAliTg");
	this.shape_11.setTransform(146.7,281.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#834719").s().p("AwLHOQgVgOgUgQQiKhwg7i1QhBjOBOivQAUgtAbgkQAXgfAbgZQBCg6BcgVQAUgFAUgDQAsgFAtAEQA6AGA6AXQAzAVAzAjQCsB1BCDOIAHAZQA3C/hLCkIgRAkQgcAyglAmQALgRAJgUQAOgeAKgfQAVhAAChGQAEhjgghlQgchdg6hYQhdiPh3guQhagkhmAWQhlAVhHBFQhGBDgaBkQgbBiAWBeQAXBdAnBIQAhA9AqAiIAOALQBIBbAuAwIAYAYQgtgUgsgegAH7GKQhvhfgQiaQgEggAAgiQAEjICKipQBXhsBtg4QAVgLAWgJIAKgEQAngOAqgJQBGgPA/AGQAUACATAEQBVASBEA6QCGBygDDIQgDDJiKCqQg4BFhAAvQAQgRAOgSQBfh0AjiOQAmiWgriGQgihshVhJQhahNhpABQg/AAhAAdQg6Aag0AuQhaBPg1BuQg1BvgFB2QgEB3ArBzQAeBNAvBAQgcgRgbgWg");
	this.shape_12.setTransform(145.4,275.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#C27333").s().p("AhGHbQh2gJhbhrQhShegdiGQgeiDAQiJQAGgvALgvQARgiATghIA1hVIAUgdQAPgYATgVIAJgLIACAaQAHBOAeBIQAnBgBBAzQAlAcAsAKQAvALAqgMQApgMAlgjQAcgbAegtQBMh0AbiEQALAEANACIAKABIAGAQQBACfgMC2QgQDdh8CrQhCBbhSA0QhVA0hTAAIgXgBg");
	this.shape_13.setTransform(123.4,181);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C47B40").s().p("AgzDIQgsgKglgdQhBgygohgQgdhHgIhOIgCgbQATgWAVgVQA+AfBEASQAlAKAnAGIABAAQArANApAEIAbAAIACAAQAkgBAggKIAMgFIAVgJQAcgOAVgYIAJgHIAJAKIAAAAQALAJAOAGQgcCEhLByQgeAtgdAbQglAkgoAMQgWAGgXAAQgVAAgXgFg");
	this.shape_14.setTransform(127.5,150.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E7D6AC").s().p("AAYAhQAOgYAKgZQAJgVAEgVIADgKQAGghgBgkQANAcgCAgIAAAFQgBAYgJAbQgMAjgbArIgYAlIAAgBIgIgJIgJAHQgVAXgcAPIgVAJQA/glAphEg");
	this.shape_15.setTransform(152,122.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D9955D").s().p("AxMJrQhwg/gdh6QgbhzAghzQAfhtA6hfQA+hmBOhZQBNhWBWhPQBchTBkhMQBOg8BshLQiKB/hKBFQidCRhdBrQhdBog1BgQhAB1gKByQgKCABABwQA0BYBPAmQgbAZgYAfQgsgJgqgXgASRB3QhCiph/iCQgrgshFg5QhPg/gmghQh8hmg+hZIAigeQC0CPCoClQBqBpA8BQQBSBrAkBpQAyCSgqB0IgMAeQgBiMg1iMg");
	this.shape_16.setTransform(125.4,178);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#9F561B").s().p("AgELgQhhgBhsglIgHgZQhCjOish1QgzgigzgVQg6gYg6gGIADgDQBQhZAahzIALgrIAqi4QARhNAehJIgEAUQgKAwgQBvIgmEGQAgB7BBBVQAfApA+A7QBLBGAZAbIArAzQAaAbAYAOQA5AgBdgOQCegWCChjQCChlA+iRQArhjAMiCQAKhggHiNQgHiqgbh8QgYh0guhfIAKgFQAWgNAUgQQBxCqAtDNQAiCbgICYIgDAnQgGA+gNA/QgaB8g1BsQg4BzhTBUQgXAagaAVQhFA6hSAmQhyAzh0AAIgGAAg");
	this.shape_17.setTransform(125,202.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#B66829").s().p("AiZKJQgYgOgagbIgsgzQgYgbhLhGQg/g7gfgpQhBhVgfh7IAlkGQAQhvAKgwIAEgUIAJgTQALgbANgaIAcgzQgLAvgGAvQgQCKAeCDQAdCFBSBeQBbBrB2AJQBfAIBgg7QBSg0BChbQB8iqAQjeQAMi2hAifIgGgQIARABIAVgDQAggFAfgPQAtBfAZB0QAaB8AICqQAGCNgKBgQgMCCgrBjQg9CRiCBlQiDBjicAWQgeAFgZAAQg5AAgngXg");
	this.shape_18.setTransform(125.5,199.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#7A3B14").s().p("AtTHmQAMgZAdgvQAegvAMgYQAcg6AOhPQAJgxAIhhIAJhkIAGgsQANhRAbg7QAbg+A2hDQAagfBPhVIAzg5IAmAbIAYAOQATAMAUALQgVAUgSAXIgJALQgTAVgQAYIgTAdIg1BVQgTAigSAhIgbAzQgOAagLAbIgIAUQgeBIgRBMIgqC5IgLArQgaB0hQBYIgDAEQgtgFgsAGQAGgTAJgWgAMOHYQg1gsg0hDQghgpg4hTIgVgfQAJiYgjiZQgtjOhxiqQATgPAQgUIAAAAQBYBaAuCbQASA/ATBjIAeCjQAyDxBxCHQAmAtAvAiIgKAEQgogTgjgcg");
	this.shape_19.setTransform(135.5,176.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#BE6C2D").s().p("AqLRFQhCgGhCgcIgYgZQgugvhIhcIgOgKQgqgjghg8QgnhIgXheQgWheAbhjQAahjBGhEQBHhFBlgVQBmgWBaAkQB3AvBdCOQA6BYAcBfQAgBlgEBiQgCBGgVBAQgKAggOAeQgJATgLASQgrApg2AaQgkAPgnAKQgdAGgbADQgSACgSAAQgSAAgRgCgAsfFGQhfAWg3BLQgUAagPAhQg4B/AvCVQATA8AgAzQAvBIBKAzQB9BVCAgeQBXgVA2hBQAZgfASgoQAGgNAFgOQAlhrgih7IgGgTQgviVh9hUQhNg1hRgJIgYgBQgiAAgjAIgAHwKFQgvg1gxhVIg6hpQAagVAXgaQBThUA4hzQA1hsAah7QANg/AGg/QAMALALAOQAMAQAaAsQAlBDAeAmQApA2AvAfQAPAJASAJQhtA4hXBrQiKCqgEDJQAAAhAEAgQgZgWgWgYgAxqAoQhAhuAKiAQAKhyBAh2QA1hgBdhpQBchrCdiQQBKhGCLh+IAVgQQAZAdAcAcQAkAiAnAeIgzA4QhPBWgaAfQg2BDgcA+QgaA7gNBRIgGAsIgKBlQgHBhgJAxQgOBPgdA6QgMAYgdAvQgdAugMAZQgKAVgFAUQgUACgUAGQhcAVhCA5QhOglg0hZgANOgKQhxiIgyjxIgfikQgShigTg/QgtichYhZIAKgPIAggZQA/BaB7BmQAnAhBPA+QBEA5AsAsQB/CDBBCpQA2CMABCMQgSAigZAfQg/gGhGAOQgqAJgnAPQgugigmgsg");
	this.shape_20.setTransform(129.1,220.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCF4E0").s().p("Au4StQhKgyguhJIgDgZQgGg/gBgfQgCg0AIgpQAKgxAWgnQAZgrAmgXQBIguBfAhQBXAdA9BKQBCBPAcBnQAZBdgLBdQg2BBhXAVQghAIghAAQheAAhdg/gAKURfQgJgHgIgJQgIheAEg/QAJiHA/hXQBAhXCGg7QBCgeAhAVQAUAMAQAjQBACIgpCiQglCThoBnIgfAIQgkAIghAAQheAAhIg9gAhApuIgCAAIgZAAQgogEgtgNIgBAAQgdgJgYgNQgTgKgQgLQgRgNgNgOQgZgdgNgmQgGgUgCgYQgFgeAEggQACgeAIggQAJgdALgbQAbg9Aug0QBChMBYgmQBTgjBcADIAOAAIAXACQBWAJBMApQBNArAjA7IAIASQAUAvgBA9QgBAXgDAYIgGAhQgLAzgWArIgFALQgJAPgJANIgLAPIAAAAQgQATgSAQQgUAQgWANIgKAFQgfAPggAFIgVACIgRgBIgKAAQgNgCgLgEQgOgGgLgJIAYglQAbgqANglQAIgbACgXIAAgFQABghgNgbQACAjgHAiIgDAJQgEAVgIAVQgKAagPAZQgpBDg/AlIgMAFQgfAKgkABIgDAAg");
	this.shape_21.setTransform(142.1,200.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CB7E34").s().p("AjAKaQhEgTg+gfQgUgKgUgMIABgFQBKAsBSAFQAQAMATAKQAYAMAeAJQgngGglgJgAmBJDIgmgaQgngegkgiQgcgcgZgdIgBgCQg4hDgihKQgYgygOg3QgYhVAAhfQAAkKC5jDIACgCIATgUIAngjQDCimEJAAQEYAADKC5IACACIAFAEIAKAKQDODJAAEaQAABCgLA8IgEAXQgQBKghBEQgzBphbBZIgKAKIgjAeIggAZQAKgNAIgOQA2gyAvg3QBRhgACkCQACkChtiaQhsiaihhSQihhRjIAjQjEAjiGCLQiBCFgqDEQgpC9A0C6QAsCfBvBtQAqAqAtAbIgBAFIgXgPgAlpJNIAAAAg");
	this.shape_22.setTransform(136.1,68.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CD8136").s().p("ArdB+QACh3AxhxQA2iBBbhMIAFgEIAKAHIAFADQi5DDAAEKQAABfAXBVIgKAFQgthiABh1gALCCcQAAkajOjJIgKgKIADgDQBPARBECBQBDCAATBwQAWCFgtBmIgIABQALg8AAhCg");
	this.shape_23.setTransform(135.9,52.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D58A41").s().p("AADKcIAYAAIADAAIgOAAIgNAAgAlHIvQgtgcgqgpQhvhtgsifQg0i7Api8QAqjECBiFQCGiLDEgjQDIgjChBRQChBRBsCbQBtCagCECQgCEChRBgQgvA3g2AxIAGgLQAWgrALgzIAGghQADgYABgXQABg9gVgvIgIgSQgig7hNgrQhMgphWgJIgYgCIgNAAQhdgDhTAjQhYAmhCBMQgtA0gbA9QgMAbgIAdQgIAggDAeQgDAgAEAeQADAYAGAUQANAmAZAdQAMAOARANQhSgFhKgsgArfCsIgNgBQgcgegYglQgjg3gWhDQgjh4AXhlQAfiAB7hLQBnhABrANQhaBMg3CAQgwBygCB3QgCB1AuBhQgkAOgoAAIgDAAgAL3iIQgUhxhCiAQhEiBhQgQIAHgLQAygUA+AKQBSAOA5A6QA1A2AYBRQAWBJgDBVQgGCDg+B4IgJAFQgfAQgkAFQAuhmgWiFg");
	this.shape_24.setTransform(132.7,71.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E7A05B").s().p("As6FlQhHgsgcheQgYhRANhiQAKhOAbhCQAfhKAxgxQA2g4BJgXQBMgYBFAWQAbAIAaAQIgFAEQhrgMhnBAQh7BLgfCAQgXBjAiB5QAWBEAjA3QAYAlAdAdQgugEgmgXgAN0AiQADhTgVhJQgZhSg0g2Qg5g5hSgPQg+gKgyAVQAVgcAjgSQAzgYA3AHQA0AIAuAiQArAfAeAvQA0BQAOB9QAOBzgZBSQgNAygfAnQgcAlglAVQA9h6AGiDg");
	this.shape_25.setTransform(132.1,49.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy, new cjs.Rectangle(-1,-1,277.8,332.3), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.bigcopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#585153").s().p("AgCAEIgCgCIAAgBQAAgBAAAAQAAAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQABAAAAgBQABAAAAAAQAAAAAAAAQAEABABADQAAACgDACIgCABIAAAAIgCgBg");
	this.shape.setTransform(18.7,-42.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgFAfQgLgHgNgFQgIgIAAgLQAAgNAMgLQAKgJAPAAQAQAAAKAJQAMALAAANQAAAOgMAKQgKAKgQAAIgFgDgAALgVQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABIAAABIABACIAEAAIACgBQACgCAAgCQAAgEgEAAIgBAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAg");
	this.shape_1.setTransform(17.2,-41.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B66D26").s().p("AgZAYQgMgKAAgOQAAgOAMgJQAKgKAPAAQAQAAAKAKQAMAJAAAOQAAAOgMAKQgKAKgQAAQgPAAgKgKgAALgVQAAAAAAABQAAAAAAABQAAAAAAABQAAABAAAAIAAACIABACIAEABIACgBQACgCAAgEQAAgDgEAAIgCAAIgDABg");
	this.shape_2.setTransform(17.2,-41.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#585153").s().p("AgCAEIgBgBIgBgCQgBgBACgDQACgCACAAQAEACAAADQABACgCACIgDACIgBAAIgCgCg");
	this.shape_3.setTransform(-0.1,-43.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C1819").s().p("AgcAcQgNgLAAgQQAAgPANgLQAMgLAQAAQARAAANALQAMALAAAPQAAAHgDAGQgMAEgMAEQgOAGgMAJQgKgCgHgHgAANgXQgCADABADIAAABIACABQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAIACgCQACgCAAgDQgBgDgDgCQgDAAgCACg");
	this.shape_4.setTransform(-1.8,-41.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B66D26").s().p("AgcAbQgNgLAAgQQAAgPANgKQAMgMAQAAQARAAANAMQAMAKAAAPQAAALgGAJIgVAIIgUAKQgNgBgKgKgAANgXQgCACABADIAAACIACABQAAABABAAQAAAAABAAQAAAAABAAQAAAAABAAIACgBQACgCAAgDQgBgEgDAAIgCAAIgDABg");
	this.shape_5.setTransform(-1.8,-41.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6F7074").s().p("AAeAVQgEgBgIgEIgLgFQgKgDgZACQgIABgFgCQgJgDgBgIQgCgKAIgFQAGgEAOAAQAYAAAGACQAIABASAGQAMAGAEADQAIAIgDAIQgCAGgHACIgHABIgGgBg");
	this.shape_6.setTransform(10.1,-28.9,0.377,0.377);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1C1819").s().p("AgPAkQgEgCgEgFIgLgTQgJgPAAgKQAAgGADgHQADgGAGgEIAIAAIAeAAIAKABIAKACQALACAEAJQAEAHgCALQgDAQgNANQgOANgQACIgDABQgHAAgDgDg");
	this.shape_7.setTransform(10.6,-26.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(51,51,51,0.298)").ss(1,1,1,3,true).p("ADTqQQABgBABAAQABgDACgCQAJgNAQgIQAXgKAZADQAXADAVAQQATAOAOAVQAXAkAGA4QAHA0gLAlQgGAXgOARQgNARgRAJQgCABgCABQgOAIgQABQgCABgBAAQgBAFgBAFQgHAigPAeQgXAvgpAoQgCADgDACQgHAHgIAGQgHAGgHAGQgDADgCADQgHAJgJAHQgJAHgJAGQgDACgCAAQgOAHgOACQgFABgFABQgEAAgDgBQgDAAgCAAQgGgBgFgCQgGgCgFgEIAAgBQgCgBgCgDQgCACgCACQgJAKgNAHQgFACgEACQgDABgCABQgPAEgPAAIgBAAQgBAAAAAAQgGAAgGAAQgSgBgUgGIAAAAQgRgDgRgEQgfgIgcgOQgJgFgIgFQgGgDgFgEQgIgFgJgHQgRgNgRgQQgNgMgLgNQgEADgFAEQgxAigjAbQgtAhgqAmQgmAjgjAnQgjAngcAuQgaAsgOAwQgOA0AMA0QANA3AyAcQATALAUADQALgOAMgLQAdgZAqgKQAJgDAJgBQAUgCAUACQABgBAAgBQAkgoAMg0QACgJADgKQAKgqAJgpQAHgjAOghQACgEACgEQAFgMAGgLQAGgMAGgLQAIgPAIgPQAMgTAMgTQAFgHAEgGQAHgLAIgKQACgCADgDQAIgKAJgJAjxqCQAAAAABgBQAEgEAEgFQAJgIAJgHQBXhLB3AAQB+AABbBTQAAABAAAAQACABAAABQADACACACQBdBbAAB/QAAAegFAbAg8iBQgNgEgLgGQgJgEgHgGQgHgFgGgHQgMgMgFgSQgDgJgBgKQgCgOACgOQAAgOAEgOQAEgNAFgNQAMgbAVgXQAegiAngRQAlgQApABQADAAAEAAQAFAAAFABQAnAEAiASQAjAUAPAbQACADACAFQAJAVgBAbQAAAKgBALQgBAIgCAHQgFAXgKAUQgBACgBADQgEAGgEAGADTjTQBRBABMBLQAwAvAbAjQAlAwAQAwQAWBBgTA1QgCAHgDAGQgIAQgLAOQAJABAJABQAlAIAgAbQA8AzgCBZQgBBbg+BNQgZAfgdAVQAAAAgBABIAAAAQgrAfgzAKIgBABQhFAOgzgeQgNgIgMgKQgzgqgHhGAF2EbQADgBACgBQARgHATgEQAfgGAdADAISJpQgTAWgUAQQgaASgdAJQgHACgHACQg/ANgrglQgEgEgEgDQgdgegFgtQAAgKAAgLQAAhBAtg3QAjgqArgSQAPgGAPgDQAWgEASABQACAAABAAQAjAEAcAXQArAlgBBBQgBBCgsA3gAD0COQgBAJAAAJQgDAcgGAcQgLA4gYAxQgZAzgmAmQgKAMgMAJQgfAaglASQgzAXg2AAQgsgBgxgRQAZBXgiBKQgEAIgEAIQgMAXgRARQgTASgZAMQgPAHgSAEQgNADgNABQgQACgPgCQgegCgdgNQgVgJgUgOQgJgGgJgHQg+gzgbhRQgdhdAjhPQAJgUAMgRAD0COIAJAOQAZAlAPATQAYAeAYATQAPANASAJAChikQAzBNAUBbQAQBGgEBEAByGvQALAUAPAcQAWAmAVAYQAKALALAJQgBgOAAgPQABhaA+hNQAngwAygZQAJgFAKgEAk6lhQgCABgDABQgQAHgTgBQgDAAgCAAQgVgCgRgKQgggUgNgqQgLglAGgsQAFgkAMgdQAOgiAWgWQAYgZAhgKQAigLAfAKQAMAEAMAHQACACADABQABABABAAAkAjxQAAgBAAAAQgZgegPgiQgMgWgGgZQgKgmAAgrQAAh4BThYAiqioQAAgBAAgCAloEeQAaACAaALQAXAKAXAPQBOA0AdBdQACAGABAFAjQIPQABAEABAEQAQA4gRAwQgDAGgCAGQgIASgMAOQgYAdgnAKQg6ANg4gmQghgXgVggQgOgXgJgbQgVhDAZg6QAHgOAIgMQAZgiArgKQAWgFAUACQAkAEAjAYQA5AmAVBDgAgJh6QgGABgHgB");
	this.shape_8.setTransform(1.7,0.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#552D15").s().p("AAfBDQgVgOgRgZQgOgRgRgdIgRgbIgKgLIABgSIAKAOQAZAlAPATQAWAdAYATQAQANASAJQgLAEgJAFIgPgIg");
	this.shape_9.setTransform(32.6,22.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F3E2B9").s().p("AkVBKQgNgugegjQgbghgngNQgrgPghAUQgRALgLAUQgKARgEAVQgEATABAXQAAANADAdIACAMQgPgXgIgbQgWhDAag5QAGgOAJgMQAZgiAqgKQAWgFAVACQAkAEAjAYQA4AmAVBCIACAIQAQA4gRAwIgFAMQgHASgMAOQAFgqgLgqgAHLAtQAThIgdg+QgHgPgJgGQgPgJgeANQg8AagdAoQgcAngEA8QgCAcAEArQgegegEgtIgBgVQABhAAtg3QAigqAsgSQAOgGAPgDQAWgEATABIACAAQAjAEAcAXQAsAlgBBBQgBBBgtA3QgSAWgVAQQgaASgdAJQAvgvAQhBg");
	this.shape_10.setTransform(5.7,53.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#9C5824").s().p("Ah5C/QgWgdgNgiQgTgzABg2QADg1AYgyQAXgxApgjQAYgVAagMQAcgNAcAAQAvgBAoAjQAnAgAPAxQAUA9gRBDQgQA/grA1IgNAPIgBABIgBAAQgrAfgyAKIAAABQgWAFgVAAQgrAAgjgVgAASiAQgPADgNAFQgsASgiAqQgtA3gBBBIABAUQAEAtAeAeIAHAHQAsAlA9gNIAOgEQAdgIAagTQAVgPASgXQAtg3ABhBQABhBgsglQgcgXgjgEIgCAAIgKAAQgOAAgRAEg");
	this.shape_11.setTransform(42.6,50.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#834719").s().p("AnRDQIgTgOQg+gygbhSQgdhbAjhPQAJgVAMgQQALgOAMgLQAegaApgJQAKgDAJgBQATgDAUACQAbADAaALQAWAJAYAPQBNA1AdBdIAEALQAYBWghBKIgIAQQgNAWgQARIAJgQQAGgOAEgOQAKgdABgfQACgtgOgtQgNgqgbgnQgphAg2gVQgogRguAKQgtAKghAfQgfAegMAtQgMAsAKAqQALAqARAgQAPAbASAQIAHAFQAgApAVAVIALAMQgUgKgUgNgADkCxQgygqgHhGQgCgOAAgPQAChaA+hMQAngwAxgaQAJgFALgEIAEgBQARgHATgEQAggHAcADIASADQAmAIAfAaQA8AzgBBaQgBBag+BMQgaAfgcAWIANgQQArg0AQhAQARhDgUg9QgPgwgnghQgogjgvABQgdAAgcANQgaAMgYAVQgpAjgXAyQgYAxgDA1QgBA2ATAzQANAjAWAdQgNgIgMgKg");
	this.shape_12.setTransform(5.1,50.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#D9955D").s().p("AnuEWQgzgbgNg3QgMg1APgzQAOgxAagrQAcgtAjgoQAignAngjQApgmAtgiQAjgbAxgiIhfBZQhHBAgqAxQgqAvgYArQgcA0gFAzQgEA6AcAyQAYAoAjARQgMALgLAOQgTgEgTgLgAIOA1QgdhLg6g6QgTgUgfgaQgkgcgRgPQg3gugcgoIAPgOQBRBBBMBKQAvAvAbAlQAlAvAQAvQAXBCgTA0IgFANQgBg/gYg/g");
	this.shape_13.setTransform(-3.9,6.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C47B40").s().p("AgXBaQgTgFgRgMQgdgXgSgrQgNgggEgjIAAgMIARgTQAcAOAfAIIAiAHIAAAAQATAGASABIANAAIABAAQAQAAAPgEIAFgCIAJgEQANgHAJgKIAEgEIAEAEIAAABQAFAEAGACQgMA8giAzQgNAUgNAMQgRAQgSAFQgKADgKAAQgKAAgKgCg");
	this.shape_14.setTransform(-3,-6.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#9F561B").s().p("AhdE6IgEgLQgdhdhNg1QgYgPgWgJQgagLgbgDIACgBQAkgoAMg0IAFgTIAShTQAIgiANghIgCAJQgEAVgHAyIgRB2QAOA3AdAmQAOATAdAaQAhAgALAMIAUAXQAMAMAKAGQAaAPApgHQBHgJA7gtQA6gtAchCQATgsAGg7QAEgqgDhAQgDhMgMg4QgLg0gUgrIAEgCIATgNQAyBMAVBcQAPBGgEBFIgBARQgCAbgHAdQgLA4gYAwQgZA0glAmQgLALgLAKQgfAaglARQg1AXg1AAQgsAAgwgRg");
	this.shape_15.setTransform(-4.1,17.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#BE6C2D").s().p("AkkHsQgegDgdgMIgLgMQgVgVgggpIgHgFQgTgQgOgbQgSgggKgqQgLgrANgsQAMgtAfgeQAggfAugKQAtgKApARQA1AVAqBAQAaAnANArQAOAtgBAtQgCAfgJAdQgFAOgGAOIgJAQQgTATgZAMQgPAGgSAFIgaAEIgQABIgPgBgAlnCTQgrAKgZAhQgIAMgHAPQgZA5AVBDQAJAbAOAXQAVAgAhAXQA4AnA6gOQAngJAYgeQAMgOAIgSIAFgMQARgwgQg3IgCgJQgVhDg5gmQgjgXgkgEIgMgBQgPAAgPAEgADfEiQgVgYgWgmIgagvQAMgKAKgLQAmgmAZg0QAYgwALg3QAGgdADgcIAKALIARAbQARAeAOASQASAXAVAOIAPAIQgyAagnAwQg+BMgBBbQAAAPABAOQgLgKgKgLgAn8ASQgdgxAFg6QAEgzAdg1QAYgrAqgvQAqgxBGhAIBghZIAJgHIAYAaQARAPARAOIgXAZIgwA1QgYAegMAcQgMAagGAlIgDATIgEAuQgDArgEAXQgHAjgMAaIgTAgIgTAgQgEAJgCAJQgJABgJADQgqAJgdAaQgkgRgXgogAF9gEQgzg9gXhtIgNhJQgJgsgIgdQgVhGgngoIAFgHIAOgLQAcAoA4AuQARAPAjAcQAgAaATAUQA5A6AeBMQAYA/AAA/QgIAPgLAOQgdgDgfAHQgTAEgRAHQgVgQgRgTg");
	this.shape_16.setTransform(-2.3,25.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#7A3B14").s().p("Al/DbIATghIATgfQAMgaAHgkQAEgWADgsIAEgsIADgUQAGglAMgaQAMgcAYgeIAwg1IAXgZIARAMIALAGIARAKIgRAUIgFAFQgIAKgHAKIgJAOIgYAlIgQAfIgMAWIgLAZIgEAIQgOAggHAiIgTBUIgFATQgMA0gkAoIgBACQgUgCgUACQACgJAEgJgAFgDUQgYgUgYgdQgPgTgZgmIgJgNQAEhFgQhEQgUhcgzhNQAJgIAHgIQAnAoAVBGQAIAdAJAsIANBJQAXBsAzA9QARAVAVAPIgFACQgSgJgPgNg");
	this.shape_17.setTransform(0.6,5.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#B66829").s().p("AhFEkQgKgGgMgMIgUgXQgLgMghggQgdgagOgTQgdgmgOg3IARh2QAHgyAEgVIACgJIAEgJIALgYIANgWQgFAVgDAVQgHA+ANA7QANA7AlAqQApAxA1AEQAqADAsgaQAlgYAdgpQA4hMAHhjQAGhSgdhIIgDgHIAIAAIAJgBQAPgCAOgHQAUArALA0QAMA4ADBMQADBAgEAqQgGA7gTAsQgcBCg6AtQg7AthGAJQgNACgLAAQgaAAgSgKg");
	this.shape_18.setTransform(-3.8,15.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#C27333").s().p("AgfDWQg1gEgpgxQglgqgNg8QgNg7AHg9QADgVAFgVIAQgfIAYgmIAJgNQAGgLAJgJIAEgFIABAMQADAjAOAgQARArAdAXQARANAUAEQAUAFATgFQATgGAQgPQANgMANgVQAjg0AMg7QAFACAGAAIAEABIADAHQAdBIgGBSQgHBig4BNQgdApglAYQgnAXglAAIgKAAg");
	this.shape_19.setTransform(-4.8,7.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E7D6AC").s().p("AALAOQAGgKAFgLQAEgJABgKIACgEQADgPgBgQQAGANgBAOIAAACQgBALgDAMQgGAPgMATIgLARIAAgBIgDgDIgEADQgJAKgNAHIgJAEQAcgQASggg");
	this.shape_20.setTransform(8,-18.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCF4E0").s().p("AmsIbQghgXgVggIgBgMQgDgdAAgNQgBgXADgTQAFgWAKgRQALgUARgLQAggUArAPQAnANAcAhQAdAkANAuQALAqgFAqQgYAdgnAKQgPADgPAAQgqAAgqgcgAEpH4IgIgHQgDgrABgcQAEg9AdgnQAdgoA8gaQAegNAOAJQAJAGAIAPQAcA+gSBJQgRBBguAvIgOAEQgQADgPAAQgrAAgggbgAgckYIgBAAIgMAAQgSgBgUgGIAAAAQgNgEgLgGQgJgEgHgGQgHgFgGgHQgMgMgFgSQgDgJgBgKQgCgOACgOQAAgOAEgOIAJgaQAMgbAVgXQAegiAngRQAmgQAoABIAHAAIAKABQAnAEAiASQAjAUAPAbIAEAIQAJAVgBAbIgBAVIgDAPQgFAXgKAUIgCAFIgIAMIgFAGQgHAJgJAHIgSANIgFACQgOAHgOACIgKACIgHgBIgFAAQgGgBgFgCQgGgCgFgEIALgRQAMgTAGgQQADgMABgLIAAgCQABgPgGgMQABAQgDAPIgCAEQgBAKgEAJQgFAMgGAKQgTAfgcARIgFACQgOAEgQAAg");
	this.shape_21.setTransform(3.6,16.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CD8136").s().p("AlJA5QABg2AWgyQAYg6ApgiIACgCIAFADIACABQhTBYAAB3QAAArAKAmIgFACQgUgsABg0gAE+BGQAAh+hdhbIgFgEIACgBQAjAHAfA6QAeA6AJAyQAKA8gVAtIgDABQAFgbAAgeg");
	this.shape_22.setTransform(0.8,-49.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CB7E34").s().p("AhWEsQgfgJgcgOIgRgKIAAgCQAhAUAlACQAIAFAIAFQALAGANAEIgigHgAitEFIgRgMQgRgNgRgQIgXgaIgBgBQgZgegPghQgMgXgGgYQgKgnAAgqQAAh3BThYIABgBIAIgJIASgPQBYhMB2ABQB+AABbBTIABABIACABIAEAFQBdBbAAB+QAAAegFAaIgCALQgHAhgOAfQgYAvgpAoIgEAFIgQANIgOALIAIgMQAYgWAWgZQAjgrACh1QABhzgyhFQgwhGhJgkQhIglhZAQQhZAPg8A/Qg6A8gTBYQgSBVAXBTQAUBIAxAxQATATAVAMIAAACIgLgGgAiiEJIAAAAg");
	this.shape_23.setTransform(0.9,-43.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D58A41").s().p("AABEsIALAAIABAAIgGABIgGgBgAiTD7QgUgMgTgTQgygxgUhHQgXhUAShUQAThZA6g7QA8g/BZgQQBZgQBIAlQBJAlAxBFQAxBFgBB0QgBB0gkArQgWAZgYAXIADgFQAJgUAFgXIADgPIACgVQAAgbgJgVIgDgIQgQgbgjgUQgigSgngEIgKgBIgGAAQgqgBglAQQgnARgeAiQgUAXgMAbIgJAaQgEAOgBAOQgCAOACAOQABAKADAJQAGASALAMQAGAHAIAFQglgCgigUgAlKBNIgGAAQgNgNgLgRQgQgZgJgdQgQg3ALgtQAOg5A3giQAugdAwAGQgpAigYA6QgWAzgBA2QAAAzAUAsQgQAGgRAAIgCAAgAFVg8QgIgzgfg6Qgeg6gkgHIADgFQAXgJAcAEQAlAHAZAZQAYAZALAlQAJAhgBAlQgDA7gbA2IgEACQgOAIgQABQAUgsgKg8g");
	this.shape_24.setTransform(-0.6,-41.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E7A05B").s().p("AlzChQgggUgNgqQgLglAGgsQAFgjAMgdQAOgiAWgWQAYgZAhgKQAigLAfAKQAMAEAMAHIgCACQgwgGgvAdQg3AigOA5QgKAsAPA3QAKAeAQAZQAKARAOANQgVgCgRgKgAGOAPQABgkgJghQgMglgXgZQgagZglgHQgcgEgWAJQAJgNAQgIQAXgKAZADQAXADAVAQQATAOAOAVQAXAkAGA4QAHAzgLAlQgGAXgOARQgNARgRAJQAcg3ADg7g");
	this.shape_25.setTransform(-0.9,-51.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(1));

	// Layer_2
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("A43Y3QqTqTAAukQAAujKTqUQKTqTOkAAQOkAAKTKTQKUKUAAOjQAAOkqUKTQqTKUukAAQukAAqTqUg");

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_28.setTransform(0,0,0.703,0.703);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#003366").s().p("A7KbKQrPrQAAv6QAAv5LPrQQLRrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArRrQg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bigcopy6, new cjs.Rectangle(-250.7,-250.7,501.5,501.5), null);


(lib.bigcopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#585153").s().p("AgCAEIgCgCIAAgBQAAgBAAAAQAAAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQABAAAAgBQABAAAAAAQAAAAAAAAQAEABABADQAAACgDACIgCABIAAAAIgCgBg");
	this.shape.setTransform(18.7,-42.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#1C1819").s().p("AgFAfQgLgHgNgFQgIgIAAgLQAAgNAMgLQAKgJAPAAQAQAAAKAJQAMALAAANQAAAOgMAKQgKAKgQAAIgFgDgAALgVQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABIAAABIABACIAEAAIACgBQACgCAAgCQAAgEgEAAIgBAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAg");
	this.shape_1.setTransform(17.2,-41.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B66D26").s().p("AgZAYQgMgKAAgOQAAgOAMgJQAKgKAPAAQAQAAAKAKQAMAJAAAOQAAAOgMAKQgKAKgQAAQgPAAgKgKgAALgVQAAAAAAABQAAAAAAABQAAAAAAABQAAABAAAAIAAACIABACIAEABIACgBQACgCAAgEQAAgDgEAAIgCAAIgDABg");
	this.shape_2.setTransform(17.2,-41.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#585153").s().p("AgCAEIgBgBIgBgCQgBgBACgDQACgCACAAQAEACAAADQABACgCACIgDACIgBAAIgCgCg");
	this.shape_3.setTransform(-0.1,-43.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#1C1819").s().p("AgcAcQgNgLAAgQQAAgPANgLQAMgLAQAAQARAAANALQAMALAAAPQAAAHgDAGQgMAEgMAEQgOAGgMAJQgKgCgHgHgAANgXQgCADABADIAAABIACABQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAIACgCQACgCAAgDQgBgDgDgCQgDAAgCACg");
	this.shape_4.setTransform(-1.8,-41.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B66D26").s().p("AgcAbQgNgLAAgQQAAgPANgKQAMgMAQAAQARAAANAMQAMAKAAAPQAAALgGAJIgVAIIgUAKQgNgBgKgKgAANgXQgCACABADIAAACIACABQAAABABAAQAAAAABAAQAAAAABAAQAAAAABAAIACgBQACgCAAgDQgBgEgDAAIgCAAIgDABg");
	this.shape_5.setTransform(-1.8,-41.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6F7074").s().p("AAeAVQgEgBgIgEIgLgFQgKgDgZACQgIABgFgCQgJgDgBgIQgCgKAIgFQAGgEAOAAQAYAAAGACQAIABASAGQAMAGAEADQAIAIgDAIQgCAGgHACIgHABIgGgBg");
	this.shape_6.setTransform(10.1,-28.9,0.377,0.377);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#1C1819").s().p("AgPAkQgEgCgEgFIgLgTQgJgPAAgKQAAgGADgHQADgGAGgEIAIAAIAeAAIAKABIAKACQALACAEAJQAEAHgCALQgDAQgNANQgOANgQACIgDABQgHAAgDgDg");
	this.shape_7.setTransform(10.6,-26.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(51,51,51,0.298)").ss(1,1,1,3,true).p("ADTqQQABgBABAAQABgDACgCQAJgNAQgIQAXgKAZADQAXADAVAQQATAOAOAVQAXAkAGA4QAHA0gLAlQgGAXgOARQgNARgRAJQgCABgCABQgOAIgQABQgCABgBAAQgBAFgBAFQgHAigPAeQgXAvgpAoQgCADgDACQgHAHgIAGQgHAGgHAGQgDADgCADQgHAJgJAHQgJAHgJAGQgDACgCAAQgOAHgOACQgFABgFABQgEAAgDgBQgDAAgCAAQgGgBgFgCQgGgCgFgEIAAgBQgCgBgCgDQgCACgCACQgJAKgNAHQgFACgEACQgDABgCABQgPAEgPAAIgBAAQgBAAAAAAQgGAAgGAAQgSgBgUgGIAAAAQgRgDgRgEQgfgIgcgOQgJgFgIgFQgGgDgFgEQgIgFgJgHQgRgNgRgQQgNgMgLgNQgEADgFAEQgxAigjAbQgtAhgqAmQgmAjgjAnQgjAngcAuQgaAsgOAwQgOA0AMA0QANA3AyAcQATALAUADQALgOAMgLQAdgZAqgKQAJgDAJgBQAUgCAUACQABgBAAgBQAkgoAMg0QACgJADgKQAKgqAJgpQAHgjAOghQACgEACgEQAFgMAGgLQAGgMAGgLQAIgPAIgPQAMgTAMgTQAFgHAEgGQAHgLAIgKQACgCADgDQAIgKAJgJAjxqCQAAAAABgBQAEgEAEgFQAJgIAJgHQBXhLB3AAQB+AABbBTQAAABAAAAQACABAAABQADACACACQBdBbAAB/QAAAegFAbAg8iBQgNgEgLgGQgJgEgHgGQgHgFgGgHQgMgMgFgSQgDgJgBgKQgCgOACgOQAAgOAEgOQAEgNAFgNQAMgbAVgXQAegiAngRQAlgQApABQADAAAEAAQAFAAAFABQAnAEAiASQAjAUAPAbQACADACAFQAJAVgBAbQAAAKgBALQgBAIgCAHQgFAXgKAUQgBACgBADQgEAGgEAGADTjTQBRBABMBLQAwAvAbAjQAlAwAQAwQAWBBgTA1QgCAHgDAGQgIAQgLAOQAJABAJABQAlAIAgAbQA8AzgCBZQgBBbg+BNQgZAfgdAVQAAAAgBABIAAAAQgrAfgzAKIgBABQhFAOgzgeQgNgIgMgKQgzgqgHhGAF2EbQADgBACgBQARgHATgEQAfgGAdADAISJpQgTAWgUAQQgaASgdAJQgHACgHACQg/ANgrglQgEgEgEgDQgdgegFgtQAAgKAAgLQAAhBAtg3QAjgqArgSQAPgGAPgDQAWgEASABQACAAABAAQAjAEAcAXQArAlgBBBQgBBCgsA3gAD0COQgBAJAAAJQgDAcgGAcQgLA4gYAxQgZAzgmAmQgKAMgMAJQgfAaglASQgzAXg2AAQgsgBgxgRQAZBXgiBKQgEAIgEAIQgMAXgRARQgTASgZAMQgPAHgSAEQgNADgNABQgQACgPgCQgegCgdgNQgVgJgUgOQgJgGgJgHQg+gzgbhRQgdhdAjhPQAJgUAMgRAD0COIAJAOQAZAlAPATQAYAeAYATQAPANASAJAChikQAzBNAUBbQAQBGgEBEAByGvQALAUAPAcQAWAmAVAYQAKALALAJQgBgOAAgPQABhaA+hNQAngwAygZQAJgFAKgEAk6lhQgCABgDABQgQAHgTgBQgDAAgCAAQgVgCgRgKQgggUgNgqQgLglAGgsQAFgkAMgdQAOgiAWgWQAYgZAhgKQAigLAfAKQAMAEAMAHQACACADABQABABABAAAkAjxQAAgBAAAAQgZgegPgiQgMgWgGgZQgKgmAAgrQAAh4BThYAiqioQAAgBAAgCAloEeQAaACAaALQAXAKAXAPQBOA0AdBdQACAGABAFAjQIPQABAEABAEQAQA4gRAwQgDAGgCAGQgIASgMAOQgYAdgnAKQg6ANg4gmQghgXgVggQgOgXgJgbQgVhDAZg6QAHgOAIgMQAZgiArgKQAWgFAUACQAkAEAjAYQA5AmAVBDgAgJh6QgGABgHgB");
	this.shape_8.setTransform(1.7,0.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#552D15").s().p("AAfBDQgVgOgRgZQgOgRgRgdIgRgbIgKgLIABgSIAKAOQAZAlAPATQAWAdAYATQAQANASAJQgLAEgJAFIgPgIg");
	this.shape_9.setTransform(32.6,22.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F3E2B9").s().p("AkVBKQgNgugegjQgbghgngNQgrgPghAUQgRALgLAUQgKARgEAVQgEATABAXQAAANADAdIACAMQgPgXgIgbQgWhDAag5QAGgOAJgMQAZgiAqgKQAWgFAVACQAkAEAjAYQA4AmAVBCIACAIQAQA4gRAwIgFAMQgHASgMAOQAFgqgLgqgAHLAtQAThIgdg+QgHgPgJgGQgPgJgeANQg8AagdAoQgcAngEA8QgCAcAEArQgegegEgtIgBgVQABhAAtg3QAigqAsgSQAOgGAPgDQAWgEATABIACAAQAjAEAcAXQAsAlgBBBQgBBBgtA3QgSAWgVAQQgaASgdAJQAvgvAQhBg");
	this.shape_10.setTransform(5.7,53.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#9C5824").s().p("Ah5C/QgWgdgNgiQgTgzABg2QADg1AYgyQAXgxApgjQAYgVAagMQAcgNAcAAQAvgBAoAjQAnAgAPAxQAUA9gRBDQgQA/grA1IgNAPIgBABIgBAAQgrAfgyAKIAAABQgWAFgVAAQgrAAgjgVgAASiAQgPADgNAFQgsASgiAqQgtA3gBBBIABAUQAEAtAeAeIAHAHQAsAlA9gNIAOgEQAdgIAagTQAVgPASgXQAtg3ABhBQABhBgsglQgcgXgjgEIgCAAIgKAAQgOAAgRAEg");
	this.shape_11.setTransform(42.6,50.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#834719").s().p("AnRDQIgTgOQg+gygbhSQgdhbAjhPQAJgVAMgQQALgOAMgLQAegaApgJQAKgDAJgBQATgDAUACQAbADAaALQAWAJAYAPQBNA1AdBdIAEALQAYBWghBKIgIAQQgNAWgQARIAJgQQAGgOAEgOQAKgdABgfQACgtgOgtQgNgqgbgnQgphAg2gVQgogRguAKQgtAKghAfQgfAegMAtQgMAsAKAqQALAqARAgQAPAbASAQIAHAFQAgApAVAVIALAMQgUgKgUgNgADkCxQgygqgHhGQgCgOAAgPQAChaA+hMQAngwAxgaQAJgFALgEIAEgBQARgHATgEQAggHAcADIASADQAmAIAfAaQA8AzgBBaQgBBag+BMQgaAfgcAWIANgQQArg0AQhAQARhDgUg9QgPgwgnghQgogjgvABQgdAAgcANQgaAMgYAVQgpAjgXAyQgYAxgDA1QgBA2ATAzQANAjAWAdQgNgIgMgKg");
	this.shape_12.setTransform(5.1,50.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#D9955D").s().p("AnuEWQgzgbgNg3QgMg1APgzQAOgxAagrQAcgtAjgoQAignAngjQApgmAtgiQAjgbAxgiIhfBZQhHBAgqAxQgqAvgYArQgcA0gFAzQgEA6AcAyQAYAoAjARQgMALgLAOQgTgEgTgLgAIOA1QgdhLg6g6QgTgUgfgaQgkgcgRgPQg3gugcgoIAPgOQBRBBBMBKQAvAvAbAlQAlAvAQAvQAXBCgTA0IgFANQgBg/gYg/g");
	this.shape_13.setTransform(-3.9,6.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#C47B40").s().p("AgXBaQgTgFgRgMQgdgXgSgrQgNgggEgjIAAgMIARgTQAcAOAfAIIAiAHIAAAAQATAGASABIANAAIABAAQAQAAAPgEIAFgCIAJgEQANgHAJgKIAEgEIAEAEIAAABQAFAEAGACQgMA8giAzQgNAUgNAMQgRAQgSAFQgKADgKAAQgKAAgKgCg");
	this.shape_14.setTransform(-3,-6.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#9F561B").s().p("AhdE6IgEgLQgdhdhNg1QgYgPgWgJQgagLgbgDIACgBQAkgoAMg0IAFgTIAShTQAIgiANghIgCAJQgEAVgHAyIgRB2QAOA3AdAmQAOATAdAaQAhAgALAMIAUAXQAMAMAKAGQAaAPApgHQBHgJA7gtQA6gtAchCQATgsAGg7QAEgqgDhAQgDhMgMg4QgLg0gUgrIAEgCIATgNQAyBMAVBcQAPBGgEBFIgBARQgCAbgHAdQgLA4gYAwQgZA0glAmQgLALgLAKQgfAaglARQg1AXg1AAQgsAAgwgRg");
	this.shape_15.setTransform(-4.1,17.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#BE6C2D").s().p("AkkHsQgegDgdgMIgLgMQgVgVgggpIgHgFQgTgQgOgbQgSgggKgqQgLgrANgsQAMgtAfgeQAggfAugKQAtgKApARQA1AVAqBAQAaAnANArQAOAtgBAtQgCAfgJAdQgFAOgGAOIgJAQQgTATgZAMQgPAGgSAFIgaAEIgQABIgPgBgAlnCTQgrAKgZAhQgIAMgHAPQgZA5AVBDQAJAbAOAXQAVAgAhAXQA4AnA6gOQAngJAYgeQAMgOAIgSIAFgMQARgwgQg3IgCgJQgVhDg5gmQgjgXgkgEIgMgBQgPAAgPAEgADfEiQgVgYgWgmIgagvQAMgKAKgLQAmgmAZg0QAYgwALg3QAGgdADgcIAKALIARAbQARAeAOASQASAXAVAOIAPAIQgyAagnAwQg+BMgBBbQAAAPABAOQgLgKgKgLgAn8ASQgdgxAFg6QAEgzAdg1QAYgrAqgvQAqgxBGhAIBghZIAJgHIAYAaQARAPARAOIgXAZIgwA1QgYAegMAcQgMAagGAlIgDATIgEAuQgDArgEAXQgHAjgMAaIgTAgIgTAgQgEAJgCAJQgJABgJADQgqAJgdAaQgkgRgXgogAF9gEQgzg9gXhtIgNhJQgJgsgIgdQgVhGgngoIAFgHIAOgLQAcAoA4AuQARAPAjAcQAgAaATAUQA5A6AeBMQAYA/AAA/QgIAPgLAOQgdgDgfAHQgTAEgRAHQgVgQgRgTg");
	this.shape_16.setTransform(-2.3,25.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#7A3B14").s().p("Al/DbIATghIATgfQAMgaAHgkQAEgWADgsIAEgsIADgUQAGglAMgaQAMgcAYgeIAwg1IAXgZIARAMIALAGIARAKIgRAUIgFAFQgIAKgHAKIgJAOIgYAlIgQAfIgMAWIgLAZIgEAIQgOAggHAiIgTBUIgFATQgMA0gkAoIgBACQgUgCgUACQACgJAEgJgAFgDUQgYgUgYgdQgPgTgZgmIgJgNQAEhFgQhEQgUhcgzhNQAJgIAHgIQAnAoAVBGQAIAdAJAsIANBJQAXBsAzA9QARAVAVAPIgFACQgSgJgPgNg");
	this.shape_17.setTransform(0.6,5.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#B66829").s().p("AhFEkQgKgGgMgMIgUgXQgLgMghggQgdgagOgTQgdgmgOg3IARh2QAHgyAEgVIACgJIAEgJIALgYIANgWQgFAVgDAVQgHA+ANA7QANA7AlAqQApAxA1AEQAqADAsgaQAlgYAdgpQA4hMAHhjQAGhSgdhIIgDgHIAIAAIAJgBQAPgCAOgHQAUArALA0QAMA4ADBMQADBAgEAqQgGA7gTAsQgcBCg6AtQg7AthGAJQgNACgLAAQgaAAgSgKg");
	this.shape_18.setTransform(-3.8,15.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#C27333").s().p("AgfDWQg1gEgpgxQglgqgNg8QgNg7AHg9QADgVAFgVIAQgfIAYgmIAJgNQAGgLAJgJIAEgFIABAMQADAjAOAgQARArAdAXQARANAUAEQAUAFATgFQATgGAQgPQANgMANgVQAjg0AMg7QAFACAGAAIAEABIADAHQAdBIgGBSQgHBig4BNQgdApglAYQgnAXglAAIgKAAg");
	this.shape_19.setTransform(-4.8,7.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E7D6AC").s().p("AALAOQAGgKAFgLQAEgJABgKIACgEQADgPgBgQQAGANgBAOIAAACQgBALgDAMQgGAPgMATIgLARIAAgBIgDgDIgEADQgJAKgNAHIgJAEQAcgQASggg");
	this.shape_20.setTransform(8,-18.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCF4E0").s().p("AmsIbQghgXgVggIgBgMQgDgdAAgNQgBgXADgTQAFgWAKgRQALgUARgLQAggUArAPQAnANAcAhQAdAkANAuQALAqgFAqQgYAdgnAKQgPADgPAAQgqAAgqgcgAEpH4IgIgHQgDgrABgcQAEg9AdgnQAdgoA8gaQAegNAOAJQAJAGAIAPQAcA+gSBJQgRBBguAvIgOAEQgQADgPAAQgrAAgggbgAgckYIgBAAIgMAAQgSgBgUgGIAAAAQgNgEgLgGQgJgEgHgGQgHgFgGgHQgMgMgFgSQgDgJgBgKQgCgOACgOQAAgOAEgOIAJgaQAMgbAVgXQAegiAngRQAmgQAoABIAHAAIAKABQAnAEAiASQAjAUAPAbIAEAIQAJAVgBAbIgBAVIgDAPQgFAXgKAUIgCAFIgIAMIgFAGQgHAJgJAHIgSANIgFACQgOAHgOACIgKACIgHgBIgFAAQgGgBgFgCQgGgCgFgEIALgRQAMgTAGgQQADgMABgLIAAgCQABgPgGgMQABAQgDAPIgCAEQgBAKgEAJQgFAMgGAKQgTAfgcARIgFACQgOAEgQAAg");
	this.shape_21.setTransform(3.6,16.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CD8136").s().p("AlJA5QABg2AWgyQAYg6ApgiIACgCIAFADIACABQhTBYAAB3QAAArAKAmIgFACQgUgsABg0gAE+BGQAAh+hdhbIgFgEIACgBQAjAHAfA6QAeA6AJAyQAKA8gVAtIgDABQAFgbAAgeg");
	this.shape_22.setTransform(0.8,-49.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CB7E34").s().p("AhWEsQgfgJgcgOIgRgKIAAgCQAhAUAlACQAIAFAIAFQALAGANAEIgigHgAitEFIgRgMQgRgNgRgQIgXgaIgBgBQgZgegPghQgMgXgGgYQgKgnAAgqQAAh3BThYIABgBIAIgJIASgPQBYhMB2ABQB+AABbBTIABABIACABIAEAFQBdBbAAB+QAAAegFAaIgCALQgHAhgOAfQgYAvgpAoIgEAFIgQANIgOALIAIgMQAYgWAWgZQAjgrACh1QABhzgyhFQgwhGhJgkQhIglhZAQQhZAPg8A/Qg6A8gTBYQgSBVAXBTQAUBIAxAxQATATAVAMIAAACIgLgGgAiiEJIAAAAg");
	this.shape_23.setTransform(0.9,-43.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#D58A41").s().p("AABEsIALAAIABAAIgGABIgGgBgAiTD7QgUgMgTgTQgygxgUhHQgXhUAShUQAThZA6g7QA8g/BZgQQBZgQBIAlQBJAlAxBFQAxBFgBB0QgBB0gkArQgWAZgYAXIADgFQAJgUAFgXIADgPIACgVQAAgbgJgVIgDgIQgQgbgjgUQgigSgngEIgKgBIgGAAQgqgBglAQQgnARgeAiQgUAXgMAbIgJAaQgEAOgBAOQgCAOACAOQABAKADAJQAGASALAMQAGAHAIAFQglgCgigUgAlKBNIgGAAQgNgNgLgRQgQgZgJgdQgQg3ALgtQAOg5A3giQAugdAwAGQgpAigYA6QgWAzgBA2QAAAzAUAsQgQAGgRAAIgCAAgAFVg8QgIgzgfg6Qgeg6gkgHIADgFQAXgJAcAEQAlAHAZAZQAYAZALAlQAJAhgBAlQgDA7gbA2IgEACQgOAIgQABQAUgsgKg8g");
	this.shape_24.setTransform(-0.6,-41.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E7A05B").s().p("AlzChQgggUgNgqQgLglAGgsQAFgjAMgdQAOgiAWgWQAYgZAhgKQAigLAfAKQAMAEAMAHIgCACQgwgGgvAdQg3AigOA5QgKAsAPA3QAKAeAQAZQAKARAOANQgVgCgRgKgAGOAPQABgkgJghQgMglgXgZQgagZglgHQgcgEgWAJQAJgNAQgIQAXgKAZADQAXADAVAQQATAOAOAVQAXAkAGA4QAHAzgLAlQgGAXgOARQgNARgRAJQAcg3ADg7g");
	this.shape_25.setTransform(-0.9,-51.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(1));

	// Layer_2
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("A43Y3QqTqTAAukQAAujKTqUQKTqTOkAAQOkAAKTKTQKUKUAAOjQAAOkqUKTQqTKUukAAQukAAqTqUg");

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_28.setTransform(0,0,0.703,0.703);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#003366").s().p("A7KbKQrPrQAAv6QAAv5LPrQQLRrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArRrQg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bigcopy5, new cjs.Rectangle(-250.7,-250.7,501.5,501.5), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween17("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(115.5,38.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,234,76.3);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.bigcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4copy();
	this.instance.parent = this;
	this.instance.setTransform(311.3,286.5,1,1,0,0,0,137.8,165.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.02)").s().p("A7sbtQrereAAwPQAAwOLereQLereQOAAQQPAALeLeQLeLeAAQOQAAQPreLeQreLewPAAQwOAArereg");
	this.shape.setTransform(311.4,286.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_1.setTransform(311.3,286.3,0.681,0.681);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BBA6A6").s().p("EgkhAkhQvIvHAA1aQAA1YPIvJQPIvIVZAAQVZAAPJPIQPIPJAAVYQAAVavIPHQvJPJ1ZAAQ1ZAAvIvJg");
	this.shape_2.setTransform(311.3,286.4,0.703,0.703);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#003366").s().p("A7JbKQrQrQAAv6QAAv5LQrQQLQrQP5AAQP6AALQLQQLQLQAAP5QAAP6rQLQQrQLQv6AAQv5AArQrQg");
	this.shape_3.setTransform(311.4,286.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bigcopy3, new cjs.Rectangle(60.7,35.7,501.5,501.5), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigcopy5();
	this.instance.parent = this;
	this.instance.setTransform(287.9,0,0.9,0.9);

	this.instance_1 = new lib.bigcopy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-291,-4.5,0.9,0.9,0,0,0,307.9,281.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.5,-225.6,1027.1,451.3);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigcopy5();
	this.instance.parent = this;
	this.instance.setTransform(287.9,0,0.9,0.9);

	this.instance_1 = new lib.bigcopy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-291,-4.5,0.9,0.9,0,0,0,307.9,281.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.5,-225.6,1027.1,451.3);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigcopy5();
	this.instance.parent = this;
	this.instance.setTransform(287.9,0,0.9,0.9);

	this.instance_1 = new lib.bigcopy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-291,-4.5,0.9,0.9,0,0,0,307.9,281.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-513.5,-225.6,1027.1,451.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Wow
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(944.4,451.4,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(227).to({_off:false},0).to({startPosition:3},62).to({alpha:0,startPosition:6},8).wait(1));

	// hand
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(934.3,615.6,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(183).to({_off:false},0).to({_off:true},31).wait(84));

	// arrow
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(943.1,176.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(152).to({_off:false},0).to({_off:true},31).wait(115));

	// Layer_5
	this.instance_3 = new lib.Symbol8("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(770.7,125,1,1,0,0,0,115.5,38.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(62).to({_off:false},0).to({_off:true},90).wait(146));

	// Layer_4
	this.instance_4 = new lib.Symbol4copy();
	this.instance_4.parent = this;
	this.instance_4.setTransform(935.4,454.5,1,1,0,0,0,137.8,165.1);
	this.instance_4.alpha = 0.5;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(103).to({_off:false},0).to({regX:138.3,regY:165.7,scaleX:0.4,scaleY:0.4,x:939.5,y:455.5},20).wait(91).to({alpha:0},5).to({_off:true},1).wait(78));

	// Q_IMG
	this.instance_5 = new lib.bigcopy6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(937.8,455.3,0.9,0.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(214).to({_off:false},0).to({scaleX:0.99,scaleY:0.99},13).wait(62).to({alpha:0},8).wait(1));

	// Q_IMG
	this.instance_6 = new lib.Tween23("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(649.9,455.3);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween24("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(649.9,455.3);

	this.instance_8 = new lib.Tween2("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(649.9,455.3);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},43).to({state:[{t:this.instance_7}]},6).to({state:[{t:this.instance_8}]},165).to({state:[{t:this.instance_8}]},5).to({state:[]},1).wait(78));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(43).to({_off:false},0).to({_off:true,alpha:1},6).wait(249));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(214).to({_off:false},0).to({alpha:0},5).to({_off:true},1).wait(78));

	// Q_TXT
	this.instance_9 = new lib.Tween19("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(641.2,124.8);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween20("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(641.2,124.8);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(19).to({_off:false},0).to({_off:true,alpha:1},6).wait(273));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(19).to({_off:false},6).wait(189).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(78));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(574,290.9,1415,861);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;