(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.wow_board = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMB9QgFgFgBgIQABgIAFgGQAFgFAHAAQAIAAAFAFQAFAGABAIQgBAIgFAFQgFAGgIAAQgHAAgFgGgAgJBLQgEgEAAgGIAAi0QAAgGAEgEQAEgFAFAAQAGAAAEAFQAEAEAAAGIAAC0QAAAGgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape.setTransform(48,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgMB9QgFgFgBgIQABgIAFgGQAFgFAHAAQAIAAAFAFQAFAGABAIQgBAIgFAFQgFAGgIAAQgHAAgFgGgAgJBLQgEgEAAgGIAAi0QAAgGAEgEQAEgFAFAAQAGAAAEAFQAEAEAAAGIAAC0QAAAGgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_1.setTransform(42.2,-0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMB9QgFgFgBgIQABgIAFgGQAFgFAHAAQAIAAAFAFQAFAGABAIQgBAIgFAFQgFAGgIAAQgHAAgFgGgAgJBLQgEgEAAgGIAAi0QAAgGAEgEQAEgFAFAAQAGAAAEAFQAEAEAAAGIAAC0QAAAGgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_2.setTransform(36.4,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AArBWQgEgDgBgFIgmhsIglBtQgCAEgEADQgEADgFAAQgEAAgEgDQgEgCgCgEIhEiSIgBgGQAAgFADgEQAEgEAGgBQAFABADACQADACACADIA4B2IAoh2QACgEAEgDQADgDAEAAQAEAAAEADQAEADABAEIApB2IA3h2QACgDAEgCQADgCAEgBQAGABAEAEQAEAEAAAFIgBAGIhFCSQgCAEgEACQgDADgFAAQgFAAgEgDg");
	this.shape_3.setTransform(18.8,4.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag5BQQgOgJgHgPQgHgOAAgSQAAgUAJgTQAJgUAPgQQAPgQAUgKQATgKAWgBQAUABAOAIQAOAJAHAOQAHAPAAARQgBAUgIATQgJAUgPARQgPAQgUAKQgTAKgXABQgUgBgNgIgAgFg1QgPAIgMAMQgMANgGAPQgHAOAAAPQAAARAIAKQAJAKAQABQAQAAAOgIQAPgIALgNQAMgNAHgPQAGgOABgPQAAgQgJgKQgIgLgRAAQgQAAgNAIg");
	this.shape_4.setTransform(-4.7,4.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAwCBQgEgDgCgEIgqhjIgqBjQgCAEgEADQgEADgFAAQgFgBgDgCQgEgDgCgEIhljpIgBgDIAAgCQAAgHAEgDQAFgFAFAAQAFAAAEADQADACACADIBZDOIAphfIgqhjIgBgDIgBgCQABgHAEgDQAEgFAGAAQAEAAAEADQADACACADIBYDOIBZjOQACgDADgCQAEgDAEAAQAGAAAEAFQAFADAAAHIgBACIAAADIhmDpQgCAEgEADQgEADgEAAQgGgBgDgCg");
	this.shape_5.setTransform(-32.3,0.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ag5B9QgPgGgKgNQgIgNgBgVQABgXAJgRQALgTAQgNQASgMAUgIQAUgKAUgGQAVgFATgDIAAhJQAAgGADgEQAFgEAFAAQAHAAADAEQAFAEAAAGIAADpQAAAGgFADQgDAEgHABQgFgBgFgEQgDgDAAgGIAAgPQgTANgWAJQgXAIgWABQgTAAgQgHgAAWgGQgWAGgSAKQgUALgMAPQgNAQAAAUQAAARAMAIQALAGASABQAPgBAOgFQAPgEAPgIQAOgIANgJIAAhVQgTADgXAHg");
	this.shape_6.setTransform(21.2,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag5BQQgOgJgHgPQgHgOAAgSQAAgUAJgTQAJgUAPgQQAPgQAUgKQATgKAWgBQAUABAOAIQAOAJAHAOQAHAPAAARQgBAUgIATQgJAUgPARQgPAQgUAKQgTAKgXABQgUgBgNgIgAgFg1QgPAIgMAMQgMANgGAPQgHAOAAAPQAAARAIAKQAJAKAQABQAQAAAOgIQAPgIALgNQAMgNAHgPQAGgOABgPQAAgQgJgKQgIgLgRAAQgQAAgNAIg");
	this.shape_7.setTransform(-16.4,4.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag7B3QgVgNgMgWQgMgXAAgbQAAgkASgdQARgdAcgVQAcgUAhgNQAigNAggFIABAAIACAAQAHAAAEAFQACAEABAGQAAAEgDAEQgDAEgGABQgdAEgdALQgdALgYARQgZARgOAZQgQAXAAAfQAAATAIAQQAIAQAOAJQAQAJATAAQAXAAAVgKQAXgKATgOIAAgcIg+AAQgFAAgFgEQgDgEAAgGQAAgGADgEQAFgEAFAAIBNAAQAFAAAFAEQAEAEAAAGIAABZQAAAGgEAEQgFAEgFAAQgHAAgEgEQgEgEAAgGIAAgRQgMALgPAHQgPAHgPAEQgPADgNABQgdgBgVgNg");
	this.shape_8.setTransform(-37.8,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag6BRQgSgIgKgPQgLgPAAgWQAAgZAOgUQANgUAWgPQAVgOAZgHQAYgIAXAAQAMAAAMAEQAMADAIAIQAIAIABAPQgBATgOAOQgOAPgYAIQgXAKgcAFQgdAEgdAAIgCAAIgDAAQACASAOAKQAOAJAUAAQAWgBAVgHQAVgIAVgKIADgBIADgBQAGAAAEAEQAEADAAAGQAAAFgBACQgCADgEACQgNAIgRAHQgSAGgSAEQgRAEgPABQgVAAgSgIgAAAg1QgWAHgRAOQgRAPgJATQAbgBAZgDQAXgFATgHQATgHALgJQAKgJABgJQAAgFgHgEQgGgEgMAAQgYAAgVAIg");
	this.shape_9.setTransform(15.6,4.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgxBSQgTgHgLgOQgMgOAAgWQAAgWALgQQALgRASgNQATgOAWgJQAVgJAXgFQAXgGATgBIABgBIABAAQAHABADAEQAEAEAAAFQAAAFgDAEQgDADgHABQgVACgZAHQgYAGgWALQgWALgOAPQgOAPAAATQAAANAIAIQAHAIAMAEQAMADANAAQAPABAVgGQAVgGAZgNIAEgBIAEgBQAGABADAEQAEAEAAAFQAAAEgBADQgCADgEACQgaANgZAHQgYAIgVAAQgUAAgSgHg");
	this.shape_10.setTransform(-5.1,4.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgJCAQgEgEAAgGIAAiRQAAgGAEgEQAEgEAFgBQAGABAEAEQAEAEAAAGIAACRQAAAGgEAEQgEAEgGAAQgFAAgEgEgAgMhkQgFgFgBgIQABgIAFgFQAFgFAHAAQAIAAAFAFQAGAFgBAIQABAIgGAFQgFAGgIAAQgHAAgFgGg");
	this.shape_11.setTransform(-17.8,-0.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("ABKCBQgEgBgCgDIiFjDIAAC7QAAAGgEAEQgEAEgGAAQgGAAgFgEQgDgEAAgGIAAjpQAAgGADgEQAFgEAGAAQADAAADACIAGAEICFDDIAAi6QAAgGAEgEQAEgEAGgBQAHABADAEQAFAEAAAGIAADoQAAAGgFAEQgDAEgHAAQgDAAgDgCg");
	this.shape_12.setTransform(-32,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Ag2BUQgEgEgBgGIAAiRQABgGAEgEQADgEAGAAQAHAAADAEQAEAEABAGIAAAKQAIgMANgHQANgGAOgBQAHAAAKACQAJACAIAFQAHAFABAIQAAAGgFAEQgDAFgHAAIgDAAIgFgCQgEgDgFgCQgFgCgFAAQgMABgIAGQgKAGgIALQgHAKgDAMIAABXQgBAGgEAEQgDAEgHAAQgGAAgDgEg");
	this.shape_13.setTransform(31.1,4.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AhXB2QgEgEAAgGIAAi7QAAgEACgDQACgDADgCQANgIASgHQARgHARgEQASgEAPAAQAUAAASAGQARAHAMANQALAOAAAWQAAAbgPAUQgPATgYAOQgYAOgaAIQgbAJgYADIAAA1QgBAGgEAEQgEAEgGAAQgFAAgEgEgAgZhWQgUAGgSAKIAABiQAZgFAXgIQAWgIASgKQASgLALgOQAKgOAAgRQAAgNgHgHQgHgIgMgDQgLgDgNAAQgTAAgUAHg");
	this.shape_14.setTransform(-7.1,7.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Ag8BCQgSgVAAgsIAAhJQAAgGAEgEQAEgEAFgBQAHABAEAEQADAEABAGIAABKQAAAhALANQAKAOATgBQANAAAMgGQAMgGAIgKQAKgKAGgMIAAhZQAAgGADgEQAFgEAGgBQAFABAFAEQADAEABAGIAACQQgBAGgDAEQgFAEgFAAQgGAAgFgEQgDgEAAgGIAAgMQgNANgQAHQgQAHgRABQgfAAgSgWg");
	this.shape_15.setTransform(-27.2,4.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgbCBQgPgDgNgGQgOgGgKgJIgDgFQgBgCAAgDQAAgHAEgEQAEgDAFAAIAGABIAEACQAOAKAOAFQAPAGARAAQAPAAANgFQAMgFAIgKQAHgKABgPQAAgRgLgLQgJgMgQgIQgPgIgRgHQgSgHgPgJQgPgJgLgMQgJgNAAgSQAAgUALgOQAMgNARgHQASgHARAAIASACQAMABAOAEQAOAEAKAGQAKAGAAAIQAAAGgEAEQgFAEgHAAIgDgBIgEgBQgNgHgOgEQgPgEgNAAQgKAAgLAEQgLAEgHAHQgIAHAAALQABAJAFAHQAGAHAIAFIASAJIAgAOQARAIAQAMQAPAKAIAPQAKAQAAATQAAAYgMAQQgMAQgSAIQgUAJgWAAQgNAAgOgEg");
	this.shape_16.setTransform(-45.9,0);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgLBuQgEgFAAgHQAAgHAEgFQAFgFAGAAQAHAAAEAFQAFAFAAAHQAAAHgFAFQgEAEgHAAQgGAAgFgEgAgIBBQgEgDAAgFIAAidQAAgGAEgDQADgEAFAAQAFAAAEAEQADADAAAGIAACdQAAAFgDADQgEAEgFAAQgFAAgDgEg");
	this.shape_17.setTransform(66.3,-0.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgLBuQgEgFAAgHQAAgHAEgFQAFgFAGAAQAHAAAEAFQAFAFAAAHQAAAHgFAFQgEAEgHAAQgGAAgFgEgAgIBBQgEgDAAgFIAAidQAAgGAEgDQADgEAFAAQAFAAAEAEQADADAAAGIAACdQAAAFgDADQgEAEgFAAQgFAAgDgEg");
	this.shape_18.setTransform(61.3,-0.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgLBuQgEgFAAgHQAAgHAEgFQAFgFAGAAQAHAAAEAFQAFAFAAAHQAAAHgFAFQgEAEgHAAQgGAAgFgEgAgIBBQgEgDAAgFIAAidQAAgGAEgDQADgEAFAAQAFAAAEAEQADADAAAGIAACdQAAAFgDADQgEAEgFAAQgFAAgDgEg");
	this.shape_19.setTransform(56.3,-0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgNBbQgPgRAAghIAAg6IgPAAQgGgBgDgDQgDgDAAgGQAAgFADgEQADgCAGgBIAPAAIAAg1QAAgGAEgDQADgEAFABQAGgBADAEQAEADAAAGIAAA1IAqAAQAFABADACQAEAEAAAFQAAAGgEADQgDADgFABIgqAAIAAA6QAAAXAHAKQAIAKATAAIAHgBIAGAAQAFAAADACQAEAEAAAFQAAAIgGADQgHADgLABQggAAgNgSg");
	this.shape_20.setTransform(47.2,0.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAwBJQgEgEAAgFIAAhBQAAgUgEgLQgEgLgHgFQgJgFgLAAQgLAAgKAFQgKAGgIAIQgJAJgEALIAABOQAAAFgEAEQgEADgFABQgFgBgEgDQgDgEAAgFIAAh+QAAgGADgDQAEgEAFAAQAFAAAEAEQAEADAAAGIAAAKQAKgLAOgHQAOgGAPAAQAcAAAPATQAQASAAAnIAABAQAAAFgDAEQgEADgFABQgFgBgEgDg");
	this.shape_21.setTransform(32.9,3.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgzBGQgPgGgJgNQgKgOAAgTQABgWAMgRQALgSATgNQATgMAVgHQAVgFAVAAQAKAAAKACQAKADAIAHQAHAIAAAMQAAARgMANQgNAMgUAIQgVAHgYAFQgZAEgaAAIgCAAIgCAAQACAQAMAIQAMAIASAAQATgBASgGQATgHARgJIADgBIAEAAQAEAAAEADQADADABAFQAAAEgCADIgFAEQgLAGgQAGQgPAGgPADQgPAEgNABQgTAAgQgIgAAAgvQgTAHgPAMQgPANgIAQQAZAAAUgDQAVgEARgGQAQgHAJgIQAKgHAAgHQAAgGgFgDQgGgEgKAAQgVABgTAGg");
	this.shape_22.setTransform(15.5,3.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgIBvQgDgEAAgFIAAjLQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGIAADLQAAAFgDAEQgEADgFABQgEgBgEgDg");
	this.shape_23.setTransform(3.9,0);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgIBvQgDgEAAgFIAAjLQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGIAADLQAAAFgDAEQgEADgFABQgEgBgEgDg");
	this.shape_24.setTransform(-1.1,0);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgyBGQgQgGgJgNQgJgOAAgTQAAgWALgRQAMgSATgNQATgMAVgHQAWgFAUAAQAJAAALACQALADAHAHQAHAIABAMQgBARgNANQgMAMgUAIQgVAHgYAFQgZAEgaAAIgCAAIgCAAQACAQAMAIQAMAIARAAQATgBAUgGQASgHASgJIACgBIADAAQAGAAADADQADADABAFQAAAEgCADIgFAEQgMAGgPAGQgPAGgPADQgPAEgOABQgSAAgPgIgAAAgvQgTAHgPAMQgPANgHAQQAXAAAWgDQAUgEAQgGQARgHAJgIQAKgHAAgHQAAgGgGgDQgEgEgLAAQgVABgTAGg");
	this.shape_25.setTransform(-12.8,3.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgrBHQgQgFgKgNQgKgNgBgSQABgUAJgNQAKgPAQgMQAQgLATgJQATgIAUgFQAUgEARgBIABAAIAAAAQAGgBADAEQAEAEAAAEQAAAFgDADQgDADgFAAQgTACgWAGQgVAGgTAJQgTAKgNANQgMAMAAASQAAALAHAHQAGAHALAEQAKACAMAAQANAAASgEQASgFAXgLIADgBIADgBQAFAAADADQAEAEAAAEQAAAEgCADQgBADgDABQgXAMgWAGQgWAGgSABQgRAAgQgHg");
	this.shape_26.setTransform(-30.9,3.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAsBLIgFgFIgng2IgmA2QgBADgDACIgGABQgFAAgDgEQgEgDgBgGIABgDIACgDIAsg+IgkgzIgCgDIAAgEQAAgFADgEQAEgDAFAAIAGABQADACABACIAeArIAfgrIAEgEQADgBADAAQAFAAADADQAEAFAAAEIAAAEIgCADIgkAzIAtA9QACADAAAEQgBAGgDADQgEAEgFAAIgFgBg");
	this.shape_27.setTransform(-46.4,3.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgzBwQgFAAgEgDQgDgEAAgFIAAjHQAAgFADgEQAEgDAFAAIBkAAQAGAAAEADQADAEABAFQgBAFgDADQgEAEgGAAIhXAAIAAA1IBSAAQAFAAAEADQADAEAAAFQAAAFgDAEQgEADgFAAIhSAAIAABiIBaAAQAFAAAEAEQADADAAAFQAAAFgDAEQgEADgFAAg");
	this.shape_28.setTransform(-60.4,0);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgLBxQgEgFgBgHQABgIAEgFQAFgFAGAAQAHAAAFAFQAFAFAAAIQAAAHgFAFQgFAEgHABQgGgBgFgEgAgIBDQgEgEAAgFIAAihQAAgGAEgEQAEgEAEAAQAGAAADAEQAEAEAAAGIAAChQAAAFgEAEQgDAEgGAAQgEAAgEgEg");
	this.shape_29.setTransform(40.6,-0.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgLBxQgEgFgBgHQABgIAEgFQAFgFAGAAQAHAAAFAFQAFAFAAAIQAAAHgFAFQgFAEgHABQgGgBgFgEgAgIBDQgEgEAAgFIAAihQAAgGAEgEQAEgEAEAAQAGAAADAEQAEAEAAAGIAAChQAAAFgEAEQgDAEgGAAQgEAAgEgEg");
	this.shape_30.setTransform(35.4,-0.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgLBxQgEgFgBgHQABgIAEgFQAFgFAGAAQAHAAAFAFQAFAFAAAIQAAAHgFAFQgFAEgHABQgGgBgFgEgAgIBDQgEgEAAgFIAAihQAAgGAEgEQAEgEAEAAQAGAAADAEQAEAEAAAGIAAChQAAAFgEAEQgDAEgGAAQgEAAgEgEg");
	this.shape_31.setTransform(30.2,-0.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgbBJQgOgHgJgMIgCgDIgBgEQAAgFAEgEQAEgDAEAAIAFABIAEADQAGAHAJAFQAKAEAKAAQAOAAAHgGQAHgGAAgIQAAgJgHgHQgHgHgTgFQgLgDgKgEQgKgGgHgJQgHgJAAgNQAAgOAHgJQAHgKALgEQALgFALAAIAMABQAIABAJACQAJADAGAEQAGADAAAHIAAACIgBADQgCADgDACQgCACgEAAIgCAAIgDgBQgIgEgIgCQgIgCgIAAQgKAAgHAFQgHAEgBAIQABAHAEAEQADAEAHADQAGADAGABQAaAGANANQAMAOAAARQAAAPgHAKQgIAKgLAGQgMAFgNAAQgPgBgPgGg");
	this.shape_32.setTransform(21.3,3.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AhPBrQgDgFAAgEIAAipQAAgEACgDIAEgEQANgIAPgGQAQgFAQgEQAQgEANAAQASAAAPAFQAQAHALAMQAKAMAAAUQAAAYgOASQgNARgWANQgVAMgXAIQgZAHgWADIAAAxQAAAEgEAFQgEADgFAAQgFAAgEgDgAgXhOQgRAGgRAJIAABZQAXgFAUgIQAUgHAQgJQARgJAJgNQAKgNAAgPQAAgMgGgGQgHgHgKgDQgLgDgMAAQgQABgTAFg");
	this.shape_33.setTransform(6.6,7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgzBIQgMgIgHgNQgGgOAAgPQAAgSAIgRQAIgSANgPQAOgPASgJQARgJAUAAQASAAAMAIQANAIAGANQAGANAAAQQAAASgIARQgIASgNAPQgOAPgSAIQgRAJgUABQgSgBgMgHgAgFgwQgNAHgKAMQgLALgGAOQgGAMAAAOQAAAOAIAKQAHAJAPABQAOgBANgHQANgHAKgLQALgMAGgNQAGgNAAgNQAAgPgIgJQgHgKgPAAQgOAAgNAHg");
	this.shape_34.setTransform(-11.4,3.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhEBqQgSgNgKgWQgJgVAAgYQAAgYAKgZQAJgaASgVQARgWAXgNQAYgNAbAAQAbAAATANQASANAJAVQAKAWAAAXQAAAZgKAYQgJAZgSAXQgRAVgYAOQgXANgbAAQgbAAgTgNgAgRhSQgSALgOASQgOASgJAVQgHAUgBAUQAAASAHAQQAGAPANAJQANAKATAAQAWAAARgLQAUgLAOgSQAOgTAHgUQAIgVAAgUQAAgSgGgPQgGgPgOgJQgMgKgTgBQgWABgSALg");
	this.shape_35.setTransform(-31.2,0);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgJBkQgFgEAAgHQAAgGAFgFQAEgDAFgBQAHABAEADQAEAFAAAGQAAAHgEAEQgEAEgHAAQgFAAgEgEgAgHA7QgDgCgBgFIAAiQQABgEADgEQADgEAEABQAFgBAEAEQACAEABAEIAACQQgBAFgCACQgEAEgFAAQgEAAgDgEg");
	this.shape_36.setTransform(67.3,-1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgJBkQgEgEgBgHQABgGAEgFQAEgDAFgBQAGABAEADQAFAFAAAGQAAAHgFAEQgEAEgGAAQgFAAgEgEgAgHA7QgDgCAAgFIAAiQQAAgEADgEQADgEAEABQAFgBAEAEQACAEAAAEIAACQQAAAFgCACQgEAEgFAAQgEAAgDgEg");
	this.shape_37.setTransform(62.6,-1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgKBkQgEgEAAgHQAAgGAEgFQAFgDAFgBQAGABAFADQAEAFAAAGQAAAHgEAEQgFAEgGAAQgFAAgFgEgAgHA7QgDgCgBgFIAAiQQABgEADgEQADgEAEABQAFgBADAEQAEAEAAAEIAACQQAAAFgEACQgDAEgFAAQgEAAgDgEg");
	this.shape_38.setTransform(58,-1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AAsBDQgEgDABgFIAAg8QAAgRgEgLQgEgKgHgFQgHgEgLAAQgJAAgJAFQgKAFgHAIQgIAIgFAKIAABHQAAAFgDADQgDADgFAAQgEAAgEgDQgDgDAAgFIAAhzQAAgFADgEQAEgDAEAAQAFAAADADQADAEAAAFIAAAJQALgKAMgGQANgGANAAQAZAAAPARQAOARAAAjIAAA7QAAAFgDADQgDADgFAAQgFAAgDgDg");
	this.shape_39.setTransform(47.9,2.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgHBmQgDgDAAgEIAAh0QAAgFADgEQADgDAEAAQAFAAADADQADAEAAAFIAAB0QAAAEgDADQgDADgFAAQgEAAgDgDgAgKhQQgEgEAAgGQAAgHAEgEQAFgDAFAAQAGAAAEADQAFAEAAAHQAAAGgFAEQgEAFgGAAQgFAAgFgFg");
	this.shape_40.setTransform(38.1,-1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgtBEQgKgEgIgHQgIgIAAgNQAAgOAJgLQAJgLANgGQAOgHAQgEQAPgFAPgCIAagCQgEgNgNgFQgMgFgPAAQgMAAgMADQgLADgLAHIgDACIgEAAQgEAAgEgCQgDgDAAgFQAAgDABgCIADgEQAIgGAKgEIAXgFIATgCQAVAAAQAHQAQAHAIAOQAIAOABAVIAABCQgBAEgCADQgEADgEAAQgFAAgDgDQgEgDAAgEIAAgOQgQALgSAHQgSAHgVABQgKAAgLgDgAAfgGIgZAFQgNABgNAFQgMAFgJAHQgHAHgBAKQAAAGAGAEQAGAFANAAQAVAAASgIQASgJAQgMIABgbIgTABg");
	this.shape_41.setTransform(27.4,2.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgzBpQgNgEgJgIQgIgJgBgPQAAgNAHgIQAHgJAKgFQAKgFALgDQAMgCAJAAIARABIASAEQADABACADQADACAAAEQAAAFgDADQgEADgEAAIgBAAIgBAAIgPgDIgPgBIgNABQgIABgGADQgHACgGAFQgEAEAAAHQAAAHAFAEQAGAEAHACIAPABQAQAAAQgEQARgEANgJQAOgHAIgMQAIgLABgPQgBgJgFgIQgGgGgIgEQgJgDgKAAIgKABIgKABQgKAGgMAFQgNAFgNAAQgLAAgKgFQgKgEgHgGQgGgJAAgMQAAgPAHgLQAIgMAMgJQAMgIANgFQAOgFAMAAQAGAAAFACIAMADIAWgVIADgDIAFgBQAEAAADAEQADACAAAFIAAAFIgDACIgVAXIADAIIABAIQAAAGgCAHIgFALQAOABAOAGQANAGAIAMQAIALAAAQQABAVgLAPQgLAPgRAMQgSAKgTAFQgTAGgUgBQgLABgMgEgAgZg/QgKADgIAGQgJAHgGAHQgGAIAAAJQAAAHAHAEQAGAEAKAAQAIAAAKgDQAKgEAIgGQAIgGAFgIQAHgHgBgJQAAgHgGgFQgHgDgIAAQgJAAgJADg");
	this.shape_42.setTransform(10.4,4.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("ABLBoIgDgFIgag8IhaAAIgaA7QgCAEgDACQgDABgDAAQgFAAgDgDQgEgDAAgFIAAgCIABgCIBRi7QACgEADgCQADgCADAAQADAAAEACQADACABAEIBRC7IABAEQAAAFgCADQgEAEgFAAQgEAAgDgCgAAkAQIgkhSIgiBSIBGAAg");
	this.shape_43.setTransform(-8.8,-1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("Ag0BgIgLgCQgEgCgCgCQgCgDAAgEQAAgEADgDQADgDAFgBIACABIABAAIAIACIAHAAQAMAAAIgIQAJgIAHgRIg6h7IgBgCIAAgCQAAgFADgEQAEgCAFAAIAFABQADABABAEIAwBpIAuhpQACgDADgCQACgBADAAQAFAAADACQAEAEAAAFIAAACIgBACIg4CBQgLAZgNAMQgOAMgTAAIgKgBg");
	this.shape_44.setTransform(-34.1,5.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgrBDQgDgDAAgFIAAhzQAAgFADgEQADgDAFAAQAEAAAEADQACAEAAAFIAAAHQAIgJAKgGQALgFALAAIANACQAHABAGAEQAHAEgBAGQAAAFgCADQgDAEgFAAIgEAAIgDgBIgIgFQgDgBgEAAQgKAAgHAGQgIAEgGAJQgFAIgEAKIAABFQAAAFgCADQgEADgEAAQgFAAgDgDg");
	this.shape_45.setTransform(-46.5,2.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgHBlQgDgEAAgEIAAitIg1AAQgFAAgDgEQgDgDAAgFQAAgEADgDQADgEAFAAIB/AAQAFAAADAEQADADAAAEQAAAFgDADQgDAEgFAAIg2AAIAACtQAAAEgCAEQgDADgFAAQgEAAgDgDg");
	this.shape_46.setTransform(-60.8,-0.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgJBkQgFgEAAgHQAAgGAFgFQAEgEAFAAQAHAAAEAEQAEAFAAAGQAAAHgEAEQgEAEgHAAQgFAAgEgEgAgHA7QgDgDgBgEIAAiQQABgEADgEQADgDAEAAQAFAAAEADQACAEAAAEIAACQQAAAEgCADQgEAEgFABQgEgBgDgEg");
	this.shape_47.setTransform(33.5,16.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgJBkQgEgEgBgHQABgGAEgFQAEgEAFAAQAGAAAEAEQAFAFAAAGQAAAHgFAEQgEAEgGAAQgFAAgEgEgAgHA7QgDgDAAgEIAAiQQAAgEADgEQADgDAEAAQAFAAADADQADAEAAAEIAACQQAAAEgDADQgDAEgFABQgEgBgDgEg");
	this.shape_48.setTransform(28.9,16.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgKBkQgEgEAAgHQAAgGAEgFQAFgEAFAAQAGAAAFAEQAEAFAAAGQAAAHgEAEQgFAEgGAAQgFAAgFgEgAgHA7QgDgDgBgEIAAiQQABgEADgEQADgDAEAAQAFAAADADQAEAEAAAEIAACQQAAAEgEADQgDAEgFABQgEgBgDgEg");
	this.shape_49.setTransform(24.2,16.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AAmBoIgDgDIhDhJIAABBQAAAFgCADQgEAEgFAAQgEAAgEgEQgCgDAAgFIAAi5QAAgGACgCQAEgEAEAAQAFAAAEAEQACACAAAGIAABnIBDgrIADgCIACAAQAGAAACAEQADADABAEIgCAGIgDADIg2AkIA7BAIACADIABAEQgBAFgDADQgDAEgFAAIgFgBg");
	this.shape_50.setTransform(15.6,16.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgnBBQgPgFgJgLQgJgMgBgRQABgRAIgNQAJgOAPgLQAPgKARgIQARgHASgEQASgEAQgCIABAAIAAAAQAGAAADAEQADADAAAEQAAAEgDADQgCADgFAAQgSACgTAFQgUAFgRAJQgRAJgMAMQgLALAAAQQAAAKAGAHQAGAGAKADQAJADALAAQAMAAAQgEQARgFAUgKIADgBIADgBQAFAAADAEQADADAAAEIgBAFIgFAFQgVAKgTAGQgUAGgRAAQgQAAgOgGg");
	this.shape_51.setTransform(0.8,20.2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgwA1QgOgRAAgjIAAg7QAAgEADgEQAEgDAEAAQAFAAADADQADAEAAAEIAAA8QAAAaAIALQAJALAQgBQAKAAAJgFQAJgEAIgJQAHgHAEgKIAAhIQABgEADgEQADgDAFAAQAEAAAEADQADAEAAAEIAAB0QAAAFgDADQgEADgEAAQgFAAgDgDQgDgDgBgFIAAgKQgKAKgMAGQgNAGgNAAQgZAAgPgRg");
	this.shape_52.setTransform(-14.6,20.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgrBoQgFAAgDgEQgDgDAAgEIAAi5QAAgEADgEQADgDAFAAQAFAAAEADQACAEAAAEIAACtIBMAAQAFABADADQADADAAAFQAAAEgDADQgDAEgFAAg");
	this.shape_53.setTransform(-28.5,16.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgsBDQgCgDAAgFIAAhzQAAgGACgCQAEgEAEAAQAFAAADAEQADACAAAGIAAAHQAIgKAKgFQAKgFAMAAIAMACQAIABAHAEQAFAEAAAGQAAAFgDADQgCAEgGAAIgDAAIgDgCIgHgEQgEgBgEAAQgKAAgHAFQgIAGgGAIQgFAIgEAKIAABFQAAAFgDADQgDADgFAAQgEAAgEgDg");
	this.shape_54.setTransform(38,-13.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AguBAQgOgGgJgMQgIgMAAgSQAAgTALgQQAKgQASgMQARgLAUgGQATgGASAAQAJAAAKADQAJACAHAHQAHAGAAAMQAAAPgMAMQgLALgTAHQgTAIgVADQgXAEgYAAIgCAAIgCAAQACAPALAHQALAHAQAAQARAAARgGQARgGARgIIACgBIADgBQAEAAAEADQADADAAAFQAAADgBACQgCADgDABQgKAGgOAGQgOAFgOADQgOAEgMAAQgRAAgOgHgAAAgrQgRAGgOAMQgOALgHAPQAWAAAUgDQASgDAPgGQAPgGAJgHQAJgHAAgHQAAgFgFgDQgFgDgKAAQgTAAgRAGg");
	this.shape_55.setTransform(23.7,-13.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgMBUQgNgQAAgeIAAg2IgPAAQgEAAgEgDQgDgDAAgFQAAgFADgDQAEgDAEAAIAPAAIAAgwQAAgFADgEQADgDAEAAQAFAAAEADQADAEAAAFIAAAwIAmAAQAFAAADADQADADABAFQgBAFgDADQgDADgEAAIgnAAIAAA2QAAAVAGAJQAIAJASAAIAGgBIAFAAQAEAAAEADQADACAAAFQAAAHgGADQgFADgLAAQgdAAgMgPg");
	this.shape_56.setTransform(9.7,-16.1);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgMBUQgOgQAAgeIAAg2IgNAAQgGAAgCgDQgDgDgBgFQABgFADgDQACgDAGAAIANAAIAAgwQABgFADgEQADgDAEAAQAGAAADADQADAEABAFIAAAwIAlAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAIglAAIAAA2QgBAVAHAJQAHAJARAAIAGgBIAGAAQAEAAAEADQADACABAFQgBAHgGADQgGADgKAAQgcAAgNgPg");
	this.shape_57.setTransform(-1.6,-16.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AguBAQgOgGgJgMQgIgMAAgSQAAgTALgQQAKgQASgMQARgLAUgGQATgGASAAQAJAAAKADQAJACAHAHQAHAGAAAMQAAAPgMAMQgLALgTAHQgTAIgVADQgXAEgYAAIgCAAIgCAAQACAPALAHQALAHAQAAQARAAARgGQARgGARgIIACgBIADgBQAEAAAEADQADADAAAFQAAADgBACQgCADgDABQgKAGgOAGQgOAFgOADQgOAEgMAAQgRAAgOgHgAAAgrQgRAGgOAMQgOALgHAPQAWAAAUgDQASgDAPgGQAPgGAJgHQAJgHAAgHQAAgFgFgDQgFgDgKAAQgTAAgRAGg");
	this.shape_58.setTransform(-15.6,-13.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhSBmQgCgEgBgFIAAirQAAgDACgDQACgDADgBQAJgFALgEIAWgGQAMgCAJAAQAMAAAKAEQALAEAHAIQAHAJAAAOQAAAHgCAGQgDAHgGAFQAQACAOAFQAPAFAKALQAKAKAAARQAAASgJAOQgJAOgQALQgPAKgSAIQgSAIgRAFQgSAFgPACQgPACgKAAQgFAAgDgEgAgZgPQgTAFgSAIIAABVIAdgGQARgEAQgGQAQgGAPgJQAOgIAJgLQAJgLAAgNQAAgLgIgGQgIgHgMgCQgMgDgMAAQgRAAgTAFgAgPhSQgLAAgMADQgNAEgLAFIAAAsIAWgHIAWgFIAFgBIADgBQAIgDAFgHQAFgIABgIQgBgJgGgEQgFgDgJAAIgDAAg");
	this.shape_59.setTransform(-33.2,-16.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AjZFbQgFgGgIgQQgZg0gRgxQgQgxAAgOQAAgEADgEQADgDAFgBQAHABAEARIACAJQAMAxAPAnQAPAnAQAbQAghSAagxQAZgyALAAQAEAAADADQACACAAAEQAAACgSAfQgSAegQAgQgLAZgOAkQgPAogKAAQgGAAgGgHgApVE7QgjgnAAhEQAAhLArgtQApguBEAAQBBAAApAoQAoAmAAA+QAAAkgPAjQgQAjgcAZQgVATgdALQgcALgfAAQg8AAgjgngAo9BnQgkAnAABCQAAA5AeAhQAeAhAyAAQAUAAATgFQASgFAQgLQAfgUATgiQAUgjAAgkQAAg2gjghQgigig3AAQg6AAgjAngAgzFLQgYgVAAgkQAAgVALgXQALgYATgUQAVgWAXgMQAYgMAXAAQAeAAATANQATANAAASQABAWghALQggALhrAJQgEAJgCAJQgDAIAAAIQAAAcASASQATARAfAAQAagBAWgHQAWgIAQgQIAMgLQAHgKAEAAQAEAAADADQACACAAADQAAAEgDAGQgEAFgFAGQgVAWgZAKQgbALggAAQgpgBgYgVgAAGCvQgXAPgTAdQBIgHAigIQAigIAAgLQAAgLgNgIQgNgHgVAAQgbAAgYAQgAJVFZQgFgGAAgIQAAgKAHgJQAIgIAJAAQAIAAAEAFQAFAFAAAIQAAALgIAIQgHAIgKAAQgHAAgEgEgAHgFZQgFgGAAgIQAAgKAHgJQAIgIAKAAQAHAAAEAFQAFAFAAAIQAAALgIAIQgHAIgKAAQgHAAgEgEgAFrFZQgFgGAAgIQAAgKAHgJQAIgIAKAAQAHAAAEAFQAFAFAAAIQAAALgIAIQgHAIgKAAQgHAAgEgEgADFFYQgCgFgBgKIAAgaQABg9gDg8IAAgOQgBgFADgEQACgDAFAAQALAAAAAhIAAABQAXgXATgLQASgMAMABQARgBAHAJQAHAJAAAUQAAAMgDAFQgCAEgGAAQgDAAgCgCQgCgDAAgEIABgHIAAgEQAAgKgEgFQgEgFgHABQgKAAgRALQgRALgbAZIABAsIABA6IAAAFQAAAUgCAFQgCAEgHAAQgEAAgCgDgAJeEPQgDgEAAgGIABgJQACggAAgfQgBg3gFg6IgCgPQAAgGADgEQAEgDAEAAQAFAAADADQACAEABAGIAFA7QABAmAAAtIAAAVQAAAhgCAIQgDAJgIAAQgEAAgDgDgAHpEPQgDgEAAgGIABgJQACggAAgfQgBg3gFg6IgCgPQAAgGADgEQAEgDAEAAQAFAAADADQACAEABAGIAFA7QABAmAAAtIAAAVQAAAhgCAIQgDAJgIAAQgEAAgDgDgAF0EPQgDgEAAgGIABgJQACggAAgfQgBg3gFg6IgBgPQAAgGACgEQAEgDAEAAQAFAAADADQACAEABAGIAFA7QABAmAAAtIAAAVQAAAhgCAIQgDAJgIAAQgEAAgDgDgAnmhLQgjgiABg4QAAgcAHgbQAIgaAPgYQAXgmAkgWQAjgWAkgBQAfAAAYATQAYASAAATQAAAGgCADQgDADgFAAQgFAAgGgKQgGgLgFgFQgLgLgMgFQgLgFgOAAQgXAAgZANQgZAOgUAXQgWAYgLAeQgMAeAAAfQAAAwAdAdQAdAdAxAAQAuAAAegcQAfgbAFgwIhAAJQgcAEgKAAQgHAAgDgDQgEgDAAgFQABgJAQgCIACAAQAkgBAjgGIAjgEQAHAAADAEQADAEABAJQgBATgHAVQgJAWgPARQgTAXgcAMQgcAMggAAQg4AAgigigAFrhAQgYgUAAglQAAgUALgYQALgXATgVQAVgVAYgNQAXgLAYgBQAeABATANQATAMABATQAAAVghAMQggAKhsAJQgEAKgCAIQgDAIAAAJQAAAcASARQATARAgAAQAaAAAWgIQAWgHAQgQIAMgMQAHgJAEAAQAEgBADADQACACAAAEQAAAEgDAFQgDAGgGAGQgVAVgZALQgaAKgiAAQgpAAgYgWgAGljcQgYAPgSAdQBIgGAigJQAigIAAgKQAAgMgNgHQgNgIgVAAQgbAAgYAQgAjJg3QgMgLAAgSQAAgXAggTQAggUBRgXIAAgSQgBgagHgIQgGgKgPABQgKgBgNAEQgMAEgRAHIgRAJQgLAGgDAAQgEAAgCgCQgDgCAAgEQAAgEAFgEQAEgEAKgGQAWgLATgGQAUgFAPAAQAYAAAKALQAKANABAiIADB7QAAAHgDADQgCAEgFgBQgEABgCgEQgCgDgBgGIgBgcQgXAYgaANQgbANgaAAQgVABgMgLgAilhyQgbAPAAAPQAAAJAHAGQAIAGAMgBQAWAAAZgNQAagOAZgbIAAghQhGAWgcAPgAEGg1QgCgEgBgPIgCg/IgBhGQAAgNgDgFQgDgGgJAAQgNAAgUAKQgVAJgYARIgDB9QAAAGgCAEQgDADgFAAQgEAAgDgDQgCgEAAgGIABiKQAAgNgDgEQgDgGgIAAQgNAAgVAKQgVAJgYARIAACCQgBAGgCAEQgDADgEABQgFgBgDgDQgCgEAAgGIAAh5IgCAAIgCAAQAAAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBgDAAgDQAAgDACgDQACgEAFgDIAAgbQAAgGACgDQADgFAFAAQAFAAACAFQACADABAGIAAASQAagRAWgKQAXgJAOAAQAOAAAIAJQAGAJABASQAagRAWgKQAWgJAPAAQAQAAAFAJQAHAJAAAcQAAA+ADBAIABAPQAAAFgDAEQgDADgEABQgFgBgDgDg");

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AjZFaQgFgGgIgRQgZgzgRgxQgQgxAAgOQAAgFADgDQADgEAFAAQAHAAAEASIACAJQAMAwAPAnQAPAnAQAcQAghSAagyQAZgxALAAQAEAAADADQACACAAAEQAAACgSAeQgSAfgQAgQgLAZgOAkQgPAngKAAQgGAAgGgGgApVE6QgjgnAAhFQAAhKArgtQApguBEAAQBBAAApAnQAoAnAAA9QAAAlgPAjQgQAigcAaQgVATgdALQgcAKgfAAQg8AAgjgmgAo9BmQgkAnAABCQAAA5AeAhQAeAhAyAAQAUAAATgFQASgGAQgKQAfgUATgjQAUgiAAgkQAAg2gjghQgigig3AAQg6AAgjAngAgzFKQgYgVAAgkQAAgVALgXQALgYATgUQAVgWAXgMQAYgMAXAAQAeAAATANQATANAAASQABAWghALQggALhrAIQgEAKgCAIQgDAJAAAIQAAAcASARQATARAfAAQAaAAAWgIQAWgHAQgQIAMgMQAHgJAEAAQAEAAADACQACADAAADQAAAEgDAFQgEAGgFAGQgVAWgZAKQgbAKggAAQgpAAgYgVgAAGCtQgXAPgTAdQBIgGAigIQAigIAAgLQAAgLgNgIQgNgHgVAAQgbAAgYAPgAJVFXQgFgFAAgIQAAgLAHgIQAIgIAJAAQAIAAAEAFQAFAFAAAIQAAALgIAIQgHAIgKAAQgHAAgEgFgAHgFXQgFgFAAgIQAAgLAHgIQAIgIAKAAQAHAAAEAFQAFAFAAAIQAAALgIAIQgHAIgKAAQgHAAgEgFgAFrFXQgFgFAAgIQAAgLAHgIQAIgIAKAAQAHAAAEAFQAFAFAAAIQAAALgIAIQgHAIgKAAQgHAAgEgFgADFFWQgCgEgBgKIAAgaQABg9gDg9IAAgNQgBgFADgEQACgDAFAAQALAAAAAhIAAAAQAXgWATgLQASgMAMAAQARAAAHAJQAHAIAAAUQAAAMgDAFQgCAFgGAAQgDAAgCgDQgCgCAAgEIABgHIAAgFQAAgJgEgFQgEgFgHAAQgKAAgRAMQgRALgbAZIABArIABA7IAAAFQAAATgCAFQgCAFgHAAQgEAAgCgEgAJeENQgDgDAAgGIABgJQACggAAgfQgBg3gFg6IgCgPQAAgGADgEQAEgDAEAAQAFAAADADQACAEABAGIAFA7QABAmAAAsIAAAWQAAAhgCAIQgDAJgIAAQgEAAgDgEgAHpENQgDgDAAgGIABgJQACggAAgfQgBg3gFg6IgCgPQAAgGADgEQAEgDAEAAQAFAAADADQACAEABAGIAFA7QABAmAAAsIAAAWQAAAhgCAIQgDAJgIAAQgEAAgDgEgAF0ENQgDgDAAgGIABgJQACggAAgfQgBg3gFg6IgBgPQAAgGACgEQAEgDAEAAQAFAAADADQACAEABAGIAFA7QABAmAAAsIAAAWQAAAhgCAIQgDAJgIAAQgEAAgDgEgAEIhBQgZgVAAgkQABgUAKgYQAMgXASgVQAWgWAXgMQAYgMAYAAQAdAAATANQAUANAAATQAAAVggALQggALhsAJQgFAJgCAJQgDAIAAAIQABAdASARQASARAgAAQAbAAAVgIQAWgIARgPIALgMQAIgKAEAAQADAAADADQADACgBADQAAAEgDAGQgDAFgGAHQgUAVgaAKQgaALghAAQgqAAgXgWgAFBjdQgYAPgSAdQBIgGAigJQAjgIgBgKQABgMgNgHQgOgIgUAAQgbAAgZAQgAlWgxQgDgEAAgGIgCkGIgJAAIhHAEQgIAAgEgDQgEgDgBgFQABgFADgDQADgCAIAAIAkgBIA0gEIBpgIIADAAQAFAAADACQADADAAAFQAAAFgDACQgDADgHAAIhcAIIADEIQAAAGgCAEQgDAEgGAAQgEAAgDgEgAing0QgCgEAAgKIAAgRQAAhIgDhDIgBgMQABgGACgDQACgEAFAAQAEAAADAEQACADABALIAAAEQADBEAABVQAAAPgCAEQgCAEgGAAQgFAAgCgDgACjg2QgDgEgBgPIgBg/IgBhGQAAgNgDgGQgEgFgIAAQgNAAgVAKQgVAJgYARIgCB8QAAAHgDADQgCAEgFAAQgFAAgCgEQgCgDgBgHIACiJQAAgNgDgFQgDgFgJAAQgNAAgTAKQgWAJgYARIAACCQAAAGgCAEQgDADgFAAQgEAAgDgDQgCgEAAgGIgBh5IgCAAIgBAAQgBAAAAAAQgBAAgBAAQAAAAgBgBQAAAAAAgBQgCgCAAgDQAAgDACgDQADgEAFgDIAAgbQAAgGACgEQADgEAFAAQAEAAADAEQACAEAAAGIAAASQAagSAXgJQAVgJAOAAQAPAAAHAJQAHAJAAASQAagSAXgJQAWgJAOAAQAQAAAGAJQAGAJAAAbQAAA/ADBAIABAPQAAAFgDAEQgCADgFAAQgFAAgCgDg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4,p:{x:-4.7}},{t:this.shape_3},{t:this.shape_2,p:{x:36.4}},{t:this.shape_1,p:{x:42.2}},{t:this.shape,p:{x:48}}]}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_4,p:{x:2.1}},{t:this.shape_6},{t:this.shape_2,p:{x:35.1}},{t:this.shape_1,p:{x:40.9}},{t:this.shape,p:{x:46.7}}]},1).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9,p:{x:15.6}},{t:this.shape_2,p:{x:29.2}},{t:this.shape_1,p:{x:35}},{t:this.shape,p:{x:40.8}}]},1).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_9,p:{x:13.3}},{t:this.shape_13},{t:this.shape_2,p:{x:40.9}},{t:this.shape_1,p:{x:46.7}},{t:this.shape,p:{x:52.5}}]},1).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]},1).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29}]},1).to({state:[{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},1).to({state:[{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).wait(1));

	// Layer 1
	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#0099FF").s().p("AqTImQh4AAAAh4IAAtbQAAh4B4AAIUnAAQB4AAAAB4IAANbQAAB4h4AAg");

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FF0033").s().p("AqTImQh4AAAAh4IAAtbQAAh4B4AAIUnAAQB4AAAAB4IAANbQAAB4h4AAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_62}]}).to({state:[{t:this.shape_63}]},5).to({state:[{t:this.shape_62}]},3).to({state:[{t:this.shape_63}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-77.9,-55,156,110.1);


(lib.sTween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FF9900","#FF3300","#800000"],[0,0.58,1],50.5,-3.8,0,50.5,-3.8,123.5).s().p("AH6DXQhIgng2g2Qg+g/haggQg0gSg6gGQhSgKhMAUIAhgTQAbgQAfgLQBlgkBwAQIgIgDQhagchcgWQg3gKg5gGQhMgIhLAAQhYgBhWgLQhRgMhMgZQhYgYhDgyIgEgWQAZAECWANIAdACIABABIByAKICNALQBhAIBWAPQAGAAAEABQEdAwDBB7QDkCPgBCsQhegUhPgpg");
	this.shape.setTransform(1.1,-16.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#010031","#0155D8"],[0.278,1],-30.3,-5.2,28.2,10).s().p("AkNg+IgCgCQgFgYgGgPQE/DgD2gZQg7AIg3AAQkBAAi1img");
	this.shape_1.setTransform(-30.2,9.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#010031","#0155D8"],[0.278,1],-21.6,-3.6,19.8,7.2).s().p("AhOAhQhKg5gwgvQDiCeCvgWQgvAHgrAAQhoAAhVgng");
	this.shape_2.setTransform(-27.4,28.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#010031","#0155D8"],[0.278,1],-33.4,-6.1,31.5,10.8).s().p("Ak2h3IAFgDQFeEHELgZQg7AHg2AAQk3AAjGjyg");
	this.shape_3.setTransform(-30.3,-18.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],23.2,25.9,0,23.2,25.9,83.8).s().p("AlBFaIgJgGIgWgQIAAgCQB5A3CfgXQivAWjjifQhYhWgEgzQgCgYgDgSIgBgEIACABQDbDLFOgrQj3AYlAjgQgDgJgFgFIAAgBQgrhFgjheQgjheAbgBQAdgDAXgLIAMgHQDpEdGFgyQkJAalfkIIgCgDIgFgIQgog6gJgvIgDgVQAYADCWANIAdACIACABIBxAKICOALQBgAJBWAOQAHAAADACQEdAwDBB6QDkCRAACqIgBAMIgBAIIgBAQQgwAIgvARQAeAIgpgIQjsBxitBiQhoA8hmgFQgWACgVAAQiOAAhnheg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.1,-44,138.1,88.1);


(lib.PTween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.451)").s().p("Ah5EyQhNgXgShqQgThqAzh+QAyh+BbhKQBYhKBMAYQBOAYARBqQAUBqgyB+QgzB+hZBKQhEA3g9AAQgTAAgTgGg");
	this.shape.setTransform(43.3,-35.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#51FD00","#47E202","#074A02"],[0,0.42,1],32.9,36.6,0,32.9,36.6,96.9).s().p("AopB2QC5h9AzgvQC9iwBlj5QA2heBBgdQAagMAbgBQBagDBrByIABABQBvBrBADgQAPA0ANA7QACAIABAJQANA1gSBRQg6DKiAC+IghAtIgOAUQgHAPgKAOIgfAoQBoiquYlIg");
	this.shape_1.setTransform(30.5,-20.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#FFFF00","#FF9900"],[0,1],5.2,19.6,0,5.2,19.6,28.2).s().p("AhNDfIgTgBQgpgDgogIQi2gjinhTICCgJIASgBIBugJQARgCAQgFIAPgEQBJgWBEgmQBLgqBEgxQBAgvBDgpQAlgWAggMQAdgKAZgDQAZgCAYAFQAhAGAhASQAXANAUAWQAeAeAVAyQgYAhgaAeQgYAbgbAZQhdBVh2AzQiAA2iGAAIgegBg");
	this.shape_2.setTransform(7.2,45.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],49.3,52.8,0,49.3,52.8,102.4).s().p("ApRJRQgwgmgqgtQgxgzgsg+IgcgrQgcgtgagzQAxgIAvgLIAAAAIAOgDIADgBIBLgSQFEhWDiikQA5gqAygvQC9iwBlj4QA3hfBAgdQAagMAcAAQBagEBqBzIABABQBwBrBADgQAPA0AMA6QADAJABAJQAMA2gSBPQg6DKiAC/IggAtIgOATQgIAPgKAPQgVgygdgfQgUgVgXgNQgigTgggGQgZgEgYACQgaACgcALQghAMgkAWQhDAphBAuQhFAyhJAqQhEAnhJAWIgQAEQgQAEgRACIhtAKIgTABIiBAIQhKgnhBgzgAm7FZQAAABgBABQAAAAAAAAQABAAAAgBQABAAABgCIAAAAIgCABgAGLE2IACgBIgBgCIgBADg");
	this.shape_3.setTransform(0,-13.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0BE0AB").s().p("AmjASQABgBAAAAQAAAAAAAAQABAAAAAAQAAAAAAAAIgDADIABgCgAGkgTIABACIgCAAIABgCg");
	this.shape_4.setTransform(-2.4,19);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#003366").ss(1,1,1).p("ABCAPQhAgJhDgU");
	this.shape_5.setTransform(-38.2,52.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC6600").s().p("AgCgBIAFABIAAAAIgEACIgBgDg");
	this.shape_6.setTransform(-39.2,82);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#993300").s().p("AAAgBIABACIAAABg");
	this.shape_7.setTransform(-28.5,67.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.9,-82.1,171.8,164.3);


(lib.PTween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(243,243,243,0.502)").s().p("AgIBDQgRgBgJgXQgJgUADgbQACgcAPgSQAPgSAOACQATACAJATIACAFQAIAUgDAaQgEAbgPASQgMAQgPAAIgDAAg");
	this.shape.setTransform(3.3,-2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F3F3F3").s().p("AgfA+QgEgCgCgEQgBgGABgGIACgGIADgFIACgBQADgDAFABQADAAACAHQACAFgDAHQAAAAAAABQAAAAAAABQAAAAAAABQgBABAAAAQgCAEgCABQgCAEgEAAIgCAAgAAKADQgJgCgCgLQgDgLADgMQACgMAHgIIACgCQAIgHAIACQAHACADALQAFALgFAMQgCAOgJAHQgFAGgHAAIgDAAg");
	this.shape_1.setTransform(0.5,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000033").s().p("AgQBAIgCAAQABAAAAgBQAAAAABgBQAAAAAAgBQAAgBAAAAQACgHgCgFQgBgHgEAAQgEgBgEADIgBABIgDAFIgFgKQgHgVAGgbQAGgYAQgQIACgCQAPgQAPAEQASAFAGAVQAGAWgFAaQgIAagQAPQgMANgMAAIgIgBgAAMgsIgCACQgIAIgCAMQgDAMADAKQACAMAJACQAIACAIgJQAIgHADgNQAEgMgFgLQgCgLgHgCIgEgBQgGAAgGAGg");
	this.shape_2.setTransform(0.4,-0.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#00FFFF","#0072FF","#0033CC"],[0,0.839,1],0.9,-2.1,0,0.9,-2.1,7.8).s().p("AgPCBQghgFgRgoQgTgoAHgzQAGg3AcgiQAbgiAgACQAhAGASAnIACAFIAAABQAQAmgGAxQgIA0gbAjQgZAggdAAIgFAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8,-12.8,16.1,25.7);


(lib.PTween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066FF").s().p("ADPEyIAAAAIAAgBIgEgIIgEgKIgJgRIgHgOQgchCgqg6Qgqg5gygvQAHAJgHgHIgBgDIjQiQIAAgBIgBgCQgOgcgFgeQgQheBZghIADAHQgkAXAAAPQgBAFANAfQAfBFB9BaQB+BZAuBoQAdBEADANQAKAhgEBAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006699").s().p("AC2C0IgOgaQgzhUhOhGQhFhBhcg0IgbgPQgYgegQgfIDRCRIABADQAHAGgHgIQAyAvApA3QAqA7AdBCIAHAOIgIgOg");
	this.shape_1.setTransform(0.1,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.1,-30.6,42.3,61.3);


(lib.PTween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#009966").s().p("AGlD2QhDg9gwgYQgfg5gqglQgpgmgugQQh7gJjnhTIgygSIgGgEQiDg6gghLIgFgYQAuAzA/AlQBAAjBOAEQBOAEA9AWQA9AVA+AcQBCAdA6ArQA4AmAyAuQAzAwApA8IAPAXIgCgHIAJAQIAFALIADAHIgMgMg");
	this.shape.setTransform(0.6,3.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00FF99").s().p("AGhD/Qgpg7g0gxQgxgtg4gnQg6gshCgbQg+gdg9gVQg9gVhOgEQhPgEg/gkQg/glgugyIgMg8IAMgHQAPAZAKAJQAMAVAnAaIAMADQD4AvCsBbIAbAPQBdAzBGBBQBOBHAyBUIAOAaIAIAOIABAHIgOgXg");
	this.shape_1.setTransform(-0.8,-1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-43.9,-29.2,87.8,58.4);


(lib.PTween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("ADeBGQifhGiOALQgJgBgMgDQhIgHg0gzQgEgIgEgEIgPgSQBIARBIAPQBGAPBHAHQBLAHBCAlQArAXAaAqIgagMg");
	this.shape.setTransform(1.7,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0099CC").s().p("AEJB5QgagqgrgXQhCgmhLgHQhHgHhGgPQhIgPhIgQQgXgigdg1QAZAJCLAgQCJAgBnAoQAvAQAoAlQAqAkAgA5IgSgJg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28.2,-12.9,56.6,25.9);


(lib.PTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],-7.8,-25,8.4,23.7).s().p("Ai/BrQgjgXgYg+QgVg3B8grIgRAAQDVhHC/A9IgDABIAPAEIACACQg+AsgxAgQgTALgRAIQg/AlhjAnIgvAQQgfAHgWAAQgXAAgNgIg");
	this.shape.setTransform(33.3,24.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],14.1,-14,-14.1,14.2).s().p("AAxCXIgLgGQg2gigWgUQgLgJgYgaIg/hJQhMhYgFg1QgHgyA+AGQA0ADBJAtIAkAYQBfBBAiAuQAhAtAdBGQAdBHAAACQAAALgCAGIABADIgBAKQgLAFgOAAQgwAAhfg1g");
	this.shape_1.setTransform(39.4,-16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],3.8,-20.5,2.2,29.4).s().p("AAQCnQjfgSkjgnQhwhNgBhWQAAhZCQgeQAQgDAYgBQChgIHRB6IALACIDgA3IACAAQAEACAEABQB8BDAsgUIgBAKIgCANIgOBgIgMACIgJACQi/g+jWBHQhJgDhQgHg");
	this.shape_2.setTransform(-0.1,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-61.3,-36.3,122.6,72.8);


(lib.P_wing_bk = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FF9900","#FF3300","#800000"],[0,0.58,1],11.9,17.4,0,11.9,17.4,44.7).s().p("ABDBIQgegXgYgcQg7hFhYgXIgBgCIgJgEQhggfhfAZQAUgJAVgHQBQgYA9AHIAFAAQAlAFAeAQQAaAOAbAPQAZAOAXARQAUAMASAOQBBAuBLAPQBOAPA8g5QgRAygtAYQgYALgagDQg4gJgzgWIAMALQAQARAVALQAZAKAbAIIgOAAQhJAAhAgug");
	this.shape.setTransform(5.4,-12.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],0.1,0,0,0.1,0,0.4).s().p("AgBgBIADABIAAACIgDgDg");
	this.shape_1.setTransform(-5.2,19);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#0173DE","#010031"],[0,1],-11.6,-3.1,10.6,4).s().p("AhihVQAWCECvAOQgMAOgWALQifgZgEiSg");
	this.shape_2.setTransform(-2.2,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#0173DE","#010031"],[0,1],-17.2,-3.8,15.5,6.6).s().p("AiVhrQAfC5EMABIgDABIgCAAQgLAbgEABIgQAAQkBAAgGjXg");
	this.shape_3.setTransform(10.5,2.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],17.2,4.8,0,17.2,4.8,44.7).s().p("Ai3DwIgQgFQgxgUgTg+QgOhOgJgXQgFgJg4g/Qgxg2AGgVQAOg3A5gkIABgBQASgLAWgKQAUgJAWgHQBQgYA8AGIAGABQAkAEAeARQAbANAZAQQAZAOAZAQQATAMATAOQBAAvBLAPQBOAQA8g6IAGgFQACAdgDATQgBAMgEAPIgCAGQgOAwgsApQgJAJgJAIQgcAWgdAMQkMgBgfi4QAHDdEQgHIgXATQgbARgrAKQgZAGgfAEQAAAAAAAAQAAAAgBAAQAAAAAAAAQAAgBAAAAIgDACIgEAGQivgOgWiFQAECSCfAZQgMAGgPAEQgUAHgcAEIgcADQgpADgeAAQgjAAgRgEgAgxDAIAAgDIgFgBIAFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-39.5,-24.4,79.2,48.8);


(lib.P_mouth = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Hi
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FF0000","#000000"],[0,1],1.5,1.5,0,1.5,1.5,9.4).s().p("AASA7IgEgBIgFgBIgbgGIgDgBQg0gUANgrQAGgVAFgHQAHgLAMgHQAHgDAJgCIABgBQAGgBAFAFQARAPgEAYIgLAvQADAVA9AUQgagDgUgEg");
	this.shape.setTransform(-5.1,-18.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFF00","#FFFFFF"],[0,0.992],-13.8,-4.2,13.8,4.1).s().p("AAoAzQgXg3gthLQguhLgLg9IAyBmQA3BQAmBYQAQAnAFAwIAHBKQgWhugYg3g");
	this.shape_1.setTransform(12.3,1.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#FFCC00","#FF3300"],[0,1],2.6,-0.9,0,2.6,-0.9,35).s().p("ADQEhIgBgEQgWglgygiQgSgPgYgNIg+gfIgWgMIgagPIgEgCIAAgBQgSgLgjgYQhWg8gagrQgKgRgJgsQgFgYADgUIAAABQACgdAJgfIAEgNIACgFIAHgVQAJgXAFgIQAEgHAEgEQALgOAPgHQASgIAVgBIgDAEQAaARAXASQAtAgAjAlQC0C2gCETIABAKIAAACg");
	this.shape_2.setTransform(1.7,-2.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#FF0000","#000000"],[0,1],-0.9,0,0,-0.9,0,32.7).s().p("ACOEgIAAgJQACkTizi2QglglgsghQgYgSgZgRIADgEIABgBQAbANAZANQFECsg6FIQgEAXgIAcg");
	this.shape_3.setTransform(8.4,-2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[]},1).wait(49));

	// Lo
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#FFCC00","#FF4900","#AA2200"],[0,0.604,1],-7.7,15.4,1.4,0).s().p("AiYBGIAAgCQgDggABgHQABgHAAgVQAAgVAJgwQAJgvAAgQQAAgPAIgPQAGApASAEQAjAWAUATIAvApQAaAXAvArIAvArIABAAIADAJQAMAOAUA/IgpABQjsAAgehcg");
	this.shape_4.setTransform(-5.7,-0.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).to({_off:true},1).wait(49));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.2,-31.1,46.2,58);


(lib.blured1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EguCAuWQzFzMAA7KQAA7JTFzMQTEzMa+AAQa/AATFTMQTETMAAbJQAAbKzETMQzFTM6/AAQ6+AAzEzMg");
	this.shape.setTransform(168.1,143.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.498)").s().p("EhFEBFhUgcngcyAAAgovUAAAgouAcngcyUAcmgcyAoeAAAUAofAAAAcnAcyUAcmAcyAAAAouUAAAAovgcmAcyUgcnAcygofAAAUgoeAAAgcmgcyg");
	this.shape_1.setTransform(168.1,126.4,0.945,0.945);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 8
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.498)").s().p("EiA/BhwMAAAjDfMEB/AAAMAAADDfg");
	this.shape_2.setTransform(163.1,121.9,1.336,1.013);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.blured1, new cjs.Rectangle(-939.9,-511.6,2206.2,1267.1), null);


(lib.P_wing_fr = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sTween6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(55,2.1,1,1,0,0,0,67.8,2.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81.9,-44,138.2,88.1);


(lib.P_tail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.PTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(52.3,22.8,1,1,0,0,0,61,1.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-70,-14.7,122.6,72.8);


(lib.P_hair = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.PTween10("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(31.7,21.2,1,1,0,0,0,28.9,13.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.PTween11("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(43.2,28.7,1,1,0,0,0,43.4,27.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 3
	this.instance_2 = new lib.PTween12("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(43.9,28.6,1,1,0,0,0,20.9,28.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.1,-30.6,88.3,61.3);


(lib.P_eyes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1,3,true).p("AiSgLQABAZACAaQAGBMAWA9QAgBSA1AAQA0AAA0hDQA0hEAPhpQAEgaABgYQAEhMgZg+QghhShCgHQg+gGg3BIQg3BHAABug");
	this.shape.setTransform(-1.4,0.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.instance = new lib.PTween13("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-0.6,-0.6,1.2,1.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FF6600"],[0,1],-15,-20.3,14.4,19).s().p("AiFDMQgbhJgHhcQgBgaAAgXIAAgDQgBh+A/hSQA8hOBDACIAJAAQBMAHAlBfQAfBLgGBcQgBAYgEAYIgBACQgRB6g9BNQg7BNg8AAQg9AAglhegAhcjAQg3BHAABtQABAaACAaQAGBLAWA+QAgBRA1ABQA0gBA0hDQA0hDAPhqQAEgaABgYQAEhLgZg+QghhShCgHIgKgBQg5AAgyBDg");
	this.shape_1.setTransform(-1.3,0.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#999999","#666666"],[0,1],-13,-17.5,12.5,16.6).s().p("AhzCxQgWg9gGhMQgCgagBgZQAAhuA3hHQA3hIA+AGQBCAHAhBSQAZA+gEBMQgBAYgEAaQgPBpg0BEQg0BDg0AAQg1AAgghSgAhWi3Qg1BEAABpQABAXACAZQAGBIAUA6QAeBOAzABQAygBAxg/QAyhBAPhkQADgZABgXQADhIgXg7QgfhPg/gFIgKgBQg2AAgvA/g");
	this.shape_2.setTransform(-1.4,0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#FFFFFF","#F9F9F9","#E2E2E2","#ADADAD"],[0,0.42,0.78,1],-0.2,-0.5,0,-0.2,-0.5,14.8).s().p("AhuCoQgUg6gGhIQgCgZgBgXQAAhpA1hEQAzhFA8AHQA/AFAfBPQAXA7gDBIQgBAXgDAZQgPBkgyBBQgxA/gyABQgzgBgehOg");
	this.shape_3.setTransform(-1.4,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,-29.6,33.8,59.7);


(lib.P_body = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.PTween14("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.9,-82.1,171.8,164.3);


(lib.Parrot_ani = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// hair
	this.instance = new lib.P_hair("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(37.8,-68,0.712,1,-7.5,0,0,40.5,26.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// wing_f
	this.instance_1 = new lib.P_wing_fr("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(11.1,13.2,0.849,1,-47.2,0,0,53,7.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// eyes
	this.instance_2 = new lib.P_eyes("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(25.6,-33.3,1,1,-7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// mouth
	this.instance_3 = new lib.P_mouth("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(72.3,-15.8,1,1,-17.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// body
	this.instance_4 = new lib.P_body("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(-9.6,14.4,1,1,-17.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// tail
	this.instance_5 = new lib.P_tail("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-62.9,55,1.212,1.07,2,0,0,56,26.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// wing_BK
	this.instance_6 = new lib.P_wing_bk("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(26.4,9.1,1.162,1.283,109,0,0,-30.9,-21.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Parrot_ani, new cjs.Rectangle(-215.1,-121.7,317.2,226.2), null);


// stage content:
(lib.ParrotAnimationTesting = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3300").s().p("ApPAhQg4AAACgKQABgFAQgGQALgEAVgFIBVgTQAdgIAsgEQAogEAiAAILbAAQAhAAApAEQArAEAfAIIBUATIAgAJQAOAFABAGQACAKg2AAg");
	this.shape.setTransform(746.9,478.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0099FF").s().p("Ao4AfQg0AAABgKQAAgFAQgFQALgFAUgFIBQgTQAdgGAqgGQAngEAgAAIK+ACQAfAAAoAFQAqAEAdAIIBQATQATAFAMAEQAOAHABAEQACALg0AAg");
	this.shape_1.setTransform(826.2,480.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3300").s().p("AsbAiQhIAAACgLQABgFAVgGQAPgEAcgFIBxgTQAogIA6gEQA2gEAugBIPVAAQAsABA3AEQA7AEAoAIIBxATIArAJQATAGABAFQADALhIAAg");
	this.shape_2.setTransform(850.9,487.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0099FF").s().p("AsbAiQhJAAACgLQABgFAVgFQAPgFAcgFIBygTQAogHA6gFQA2gEAugBIPVAAQAsABA2AEQA8AFAoAHIBxATQAaAFARAFQATAFABAFQADALhIAAg");
	this.shape_3.setTransform(693.8,485.5);

	this.instance = new lib.Parrot_ani();
	this.instance.parent = this;
	this.instance.setTransform(601.8,457.3,1.214,1.215,0,0,0,-2.8,78.6);

	this._mc = new lib.wow_board();
	this._mc.name = "_mc";
	this._mc.parent = this;
	this._mc.setTransform(789.7,417.3,1.214,1.215,0,0,0,0.1,0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC6600").s().p("ACBA8QgZgNgKgCQg0gUgngJQgigJg5AHQg+AKgcACIAAgCIAlgrQAQgUAegdQAaATBwAsQBaAgAuAlIgLAEIgHABQgNAAgTgJg");
	this.shape_4.setTransform(610.8,472.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#993300").s().p("AgBAdQiAgKhPAbIBNhdQAkgtAnAHQgHAFgFAGQgeAdgQAUIglArIAAACQAcgCA+gKQA5gHAiAJQAnAJA0AUQAKACAZANQAYALAPgDIALgEIADACQgFAKgLARQg8gwiGgKg");
	this.shape_5.setTransform(608.1,473);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF9900").s().p("AAAACQhxgrgagTQAFgFAIgGIAFABICIA5QBYAmAlAqIgFAFQguglhZghg");
	this.shape_6.setTransform(615.1,471.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#431600").s().p("AhzAlIAag3QAQgiAVACIgDABIgCAIIgIAgIgNAdQAWgOAcAAIA1ABQAbgBAfAMQAVAHARALIgCAJQgrgbhPADQg8ADgoASIgRAKIAFgPg");
	this.shape_7.setTransform(623.7,469.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#993300").s().p("AA8AVQgggNgbABIg0gBQgdAAgVAPIAMgeIAJgfIBuAhQAZAFAUADQAPAKALALQAAAAABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABgBgBQAAAAAAAAQgBAAAAgBQgBgBAAAAQACAEgDALQgSgLgUgHg");
	this.shape_8.setTransform(626.1,469.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC6600").s().p("AAhAQIhugfIACgJIACgBIAHAAQA4AQAdALQAjAKAYAOQgUgDgZgHg");
	this.shape_9.setTransform(625.8,466.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this._mc},{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.instance_1 = new lib.blured1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(543.9,286.3,0.589,0.586,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(630.1,346.7,1299.7,742);
// library properties:
lib.properties = {
	id: 'E02783C89B02E54D9400B73DF3C4FEBA',
	width: 1280,
	height: 720,
	fps: 24,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['E02783C89B02E54D9400B73DF3C4FEBA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;