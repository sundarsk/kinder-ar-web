var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
var bgvoice = 1;
var script11 = document.createElement("script"); // Make a script DOM node
script11.src = "assets/jquery-3.2.1.min.js"; // Set it's src to the provided URL

document.head.appendChild(script11);
if (iOS == false) {
   // alert("IOS = "+iOS)
    //speaksound();
}

var redirectJsonPath = "http://localhost/project/php/KinderAR/v3/assets/";

function htmlRedirect(nav, url, tqcnt, aqcnt, cqcnt, gscore, gtime, rtime, crtime, wrtime, quesNo) {
	nav="yes";
	url="http://localhost/project/php/KinderAR/v3/index.php/home/result";	
    if (nav == 'yes') {
        var isOnline = navigator.onLine;
        console.log("isOnline= " + isOnline)
        if (!isOnline) {
            console.log("Please check your internet connectivity!");
        } else {
            console.log("console.log");
            //  window.location.href = url + "?gtime1=" + gtime + "&aqcnt1=" + aqcnt + "&rtime1=" + rtime + "&cqcnt1=" + cqcnt + "&crtime1=" + crtime + "&gscore1=" + gscore + "&tqcnt1=" + tqcnt + "&wrtime1=" + wrtime;			
			var getStars = 0;
			if(cqcnt == 0) {
				getStars = 0;
			}
			else if (cqcnt>=1 && cqcnt<= 4) {
				getStars = 1;
			}
			else if (cqcnt >=5 && cqcnt<= 7) {
				getStars = 2;
			}
			else if (cqcnt >=8 && cqcnt<= 10) {
				getStars = 3;
			}
			
            var urlA = url;
			console.log("console.log= "+urlA);
			 $.post(urlA,
                {
                    gtime1: gtime,
                    aqcnt1: aqcnt,
                    rtime1: rtime,
                    cqcnt1: cqcnt,
                    crtime1: crtime,
                    gscore1: gscore,
                    tqcnt1: tqcnt,
                    wrtime1: wrtime,
					quesNo1:quesNo
					
                },
                function (data, status) {
                    console.log("Data: " + data + "\nStatus: " + status);
                    if (status == "success" && data >= 1) 
					{
						if(window.parent.document.getElementById("hdnsparkiespoints"))
						{
							window.parent.document.getElementById("hdnsparkiespoints").value=data;
							window.parent.document.getElementById("hdntotalstars").value=getStars;
							//console.log("getStars: " + getStars + "\ndata: " + data);
						}
						bitmap.visible = false;
                    }
					else if(data=='-1')
					{
						console.log("set Game Play End")
						getGameFinishedImage.visible = true;
						container2.parent.addChild(getGameFinishedImage);                                          
						var setFinishedTxt = new createjs.Text("", "35px Veggieburger-Bold", "white");
						setFinishedTxt.text = "Game Finished...";
						setFinishedTxt.lineWidth = 550;
						setFinishedTxt.lineHeight = 22;
						setFinishedTxt.textBaseline = "top";
						setFinishedTxt.textAlign = "left";
						setFinishedTxt.x = 260
						setFinishedTxt.y = 280;
						stage.addChild(setFinishedTxt);
						
						bitmap.visible = false;
                    } 
					else if(data=='-2')
					{
						console.log("set Game Play End")
						getGameFinishedImage.visible = true;
						container2.parent.addChild(getGameFinishedImage);                                          
						var setFinishedTxt = new createjs.Text("", "35px Veggieburger-Bold", "white");
						setFinishedTxt.text = "Session expired. Try again.";
						setFinishedTxt.lineWidth = 550;
						setFinishedTxt.lineHeight = 22;
						setFinishedTxt.textBaseline = "top";
						setFinishedTxt.textAlign = "left";
						setFinishedTxt.x = 260
						setFinishedTxt.y = 280;
						stage.addChild(setFinishedTxt);
						
						bitmap.visible = false;                            
                    }
                });
        }
    }
}