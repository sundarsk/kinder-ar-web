var container1
var questionTxtR;
var attemptTxtR;
var correctTxtR;
var responseTxtR;
var scoreTxtR;
var getCorrectStr = "";
var removeSkillangesTweenMc
var rightCnt = -1, wrongCnt = -1;
var animEndCnt = 0, validCnt = 0;
var starCnt = -1
var pickQuesInterval
var es1 = [2, 0, 3, 4, 1, 4, 2, 0, 1, 3]
var es2 = [5, 6, 7, 6, 5, 7, 7, 6, 5, 6]
es1.sort(randomSort);
var getStars = 0
//es2.sort(randomSort);


function startAnimationHandler(evt) {
     //handleClick(null)
    //  console.log("checkAnim= "+evt.currentTarget.currentFrame+" checkTotal Frames - "+evt.currentTarget.totalFrames)
    if (evt.currentTarget.currentFrame == evt.currentTarget.totalFrames - 45) {
        gameIntroAnimMc.removeEventListener("tick", startAnimationHandler)
        gameIntroAnimMc.cursor = "pointer";

        introStartBtn.visible = true;
        introStartBtn.addEventListener("click", createDelayToStartGame);
        introStartBtn.cursor = "pointer";
        container.parent.addChild(introStartBtn)

        startBtn.visible = true;
        startBtn.addEventListener("click", createDelayToStartGame);
        startBtn.cursor = "pointer";
        container.parent.addChild(startBtn)

        var StartBtnMc = new createjs.MovieClip()
        container.parent.addChild(StartBtnMc)
    
        StartBtnMc.timeline.addTween(createjs.Tween.get(startBtn).to({ scaleX: .95, scaleY: .95 }, 19).to({ scaleX: 1, scaleY: 1 }, 20).wait(1));
        
        // customAnimation()
    }
    

}
//=============================================================================//
function createDelayToStartGame() {
    introStartBtn.removeEventListener("click", createDelayToStartGame);
    introStartBtn.mouseEnabled = false;
    introStartBtn.visible = false;
    howToPlayImageMc.visible = false;
    startBtn.visible = false;
    SkipBtnMc.visible = false
    SkipBtnMc.removeEventListener("click", createDelayToStartGame);
    howToPlayImageMc.visible = false;

     
    gameIntroAnimMc.stop()
    setTimeout(handleClick, 1000);
}
//=============================================================================/

function customAnimation() {

    new confettiKit({
        confettiCount: 50,
        angle: 90,
        startVelocity: 100,
        colors: randomColor({ hue: 'blue', count: 50 }),
        elements: {
            'confetti': {
                direction: 'down',
                rotation: true,
            },
            'star': {
                count: 10,
                direction: 'down',
                rotation: true,
            },
            'ribbon': {
                count: 10,
                direction: 'down',
                rotation: true,
            },
            'custom': [{
                count: 2,
                width: 100,
                textSize: 40,
                content: 'assets/images/Balloon.png',
                contentType: 'image',
                direction: 'down',
                rotation: false,
            }]
        },
        position: 'bottomLeftRight',
    });
}


function animationEndHandler(e) {
    animEndCnt++

    if (animEndCnt == 1) {
        animEndCnt = 0
        if (getCorrectStr == "") {
            console.log("coming...")
            gameIntroAnimMc.addEventListener("tick", startAnimationHandler)
        } else {
            validCnt++

            if (validCnt == 2) {

             //   getCorrectValidation();
            }
        }
    }

}


function starAnimation() {
    starCnt++;
    if (starCnt < 20) {
        starAnimMc.x = currentX
        starAnimMc.y = currentY
        starAnimMc.visible = true
        container.parent.addChild(starAnimMc)
        
        disableMouse()
        starAnimMc.gotoAndStop(starCnt);
        setTimeout(starAnimation, 10)
    }
    else {
        enableMouse()
        starCnt = -1
        starAnimMc.visible = false
        // disablechoices();
    }
}

//=========================================================================//
function getValidation(aStr) {
    boardMc.boardMc.mouseEnabled = false;
    console.log("es1 " + es1)
    console.log("es2 " + es2)
    getCorrectStr = aStr

    container.parent.addChild(starAnimMc)

    if (aStr == "correct") {

        rightCnt++;
        console.log(rightCnt)
        ccnt = ccnt + 1;
        calculatescore();
        crst = crst + rst;

        boardMc.boardMc.scoreMc.txt.text = score + "";
        correctImageMc.visible = true
        //customAnimation()
        getCorrectValidation();
        createAnimationSound()

    } else {
        wrongCnt++;
        console.log("wrongSoundCnt " + wrongCnt)
        rst1 = rst1 + rst;
        wrst = wrst + rst;
        getWrongValidation()
        wrongImageMc.visible = true
        //=================================wrong==================
        if (!isEffSound) {
            wrongSnd.play();
            wrongSnd.volume = 0;
        } else {
            wrongSnd.play();
            wrongSnd.volume = 1;
        }
        // wrongSnd.addEventListener("complete", handleComplete);
    }

    if (rst == 0) {
        rst = 1
    }
    responseTime += rst;
    console.log("responseTime= " + responseTime)
    answeredQuestions += 1;
}
//=============================================================================//
function createAnimationSound() {
    console.log("rightCnt " + rightCnt)
    if (!isEffSound) {
        correctSnd.play();
        correctSnd.volume = 0;
    } else {
        correctSnd.play();
        correctSnd.volume = 1;
    }
    //  correctSnd.addEventListener("complete", handleComplete);
}
//=============================================================================//
function getCorrectValidation() {
    console.log("getCorrectValidation")
    validCnt = 0
    // if (time != 0) {
    //     if (container.parent) {
    //         // ParrotAnimationTesting.visible = true;
    // container.parent.addChild(ParrotAnimationTesting)
    // ParrotAnimationTesting["_mc"].gotoAndStop(es1[rightCnt]);
    //=================================correct==================
    //     }
    // } else {
    // ParrotAnimationTesting.visible = false;
    // }

    stage.update()

    pickQuesInterval = setInterval(handleComplete, 1000)

}
//=============================================================================//
function getWrongValidation() {
    //=================================wrong==================
    console.log("getWrongValidation")
    validCnt = 0;
    // if (time != 0) {
    //     if (container.parent) {
    // ParrotAnimationTesting.visible = true;
    // container.parent.addChild(ParrotAnimationTesting)
    // ParrotAnimationTesting["_mc"].gotoAndStop(es2[wrongCnt]);
    //     }
    // } else {
    // ParrotAnimationTesting.visible = false;
    // }
    stage.update()
    pickQuesInterval = setInterval(handleComplete, 1000)
}
//=================================================================================================================//
function handleComplete() {
    clearInterval(pickQuesInterval);
    console.log("answ ====== = " + answeredQuestions + " cnt = " + cnt)

    if (cnt < totalQuestions - 1) {
        console.log("get new questions")

        ParrotAnimationTesting.visible = false;
        //restartTimer();
        boardMc.boardMc.mouseEnabled = true;

        pickques();

    } else {
        console.log(" Game Finished ")
        clearInterval(interval);
        if (container.parent) {
            gameOverSnd.play();
            gameOverSnd.volume = 1;
            ParrotAnimationTesting.visible = false;
            container.parent.addChild(ParrotAnimationTesting)
            ParrotAnimationTesting["_mc"].gotoAndStop(8);
            ParrotAnimationTesting["_mc"].visible = false;
            container.parent.addChild(gameOverImageMc)
           gameOverImageMc.visible = true;
            stage.update()
            gameOverSnd.addEventListener("complete", handleComplete1);
        }
    }
}

function handleComplete1(e) {

    clearInterval(interval);
    gameResponseTimerStop();
    correctSnd.stop();
    wrongSnd.stop();
    gameOverSnd.stop();


    if (container.parent) {
        container.parent.removeAllChildren();
    }

    container1 = new createjs.Container();
    stage.addChild(container1)
    container1.parent.addChild(resultLoading)



    computeresult();
}

//=================================================================================================================//
function computeresult() {

    stage.mouseEnabled = false;

    helpMc.helpMc.visible = false;
    var responseValue = 1;
    
    var str1 = gameAssetsPath.split("-")[1]
    
    if (str1 == "KG/") {

        tqcnt = 5;
        aqcnt = answeredQuestions;
        if (aqcnt > 5) {
            aqcnt = 5;
        }
    } else {
        tqcnt = 10;
        aqcnt = answeredQuestions;

        if (aqcnt > 10) {
            aqcnt = 10;
        }
    }

/*    tqcnt = 10;
    aqcnt = answeredQuestions;

    if (aqcnt > 10) {
        aqcnt = 10;
    }
     */
    
    cqcnt = ccnt;
    gscore = score;
   
    if (time == 0)
        gtime = 180;
    else
        gtime = time;

    rtime = responseTime;
    crtime = crst;
    wrtime = wrst;

    removeFullScreen()



   
    /*
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        questionTxtR = new createjs.Text(tqcnt, "bold 35px Veggieburger-Bold", "white");
        container1.parent.addChild(questionTxtR);
        questionTxtR.x = 732; questionTxtR.y = 153;
        questionTxtR.textAlign = "center";
        questionTxtR.textBaseline = "middle";
    
        //
        attemptTxtR = new createjs.Text(aqcnt, "bold 35px Veggieburger-Bold", "white");
        container1.parent.addChild(attemptTxtR);
        attemptTxtR.x = 735; attemptTxtR.y = 267;
        attemptTxtR.textAlign = "center";
        attemptTxtR.textBaseline = "middle";
    
        correctTxtR = new createjs.Text(cqcnt, "bold 35px Veggieburger-Bold", "white");
        container1.parent.addChild(correctTxtR);
        correctTxtR.x = 732; correctTxtR.y = 375;
        correctTxtR.textAlign = "center";
        correctTxtR.textBaseline = "middle";
        //
        responseTxtR = new createjs.Text(rtime, "bold 35px Veggieburger-Bold", "#FF9900");
        container1.parent.addChild(responseTxtR);
        responseTxtR.x = 410; responseTxtR.y = 570;
        responseTxtR.textAlign = "center";
        responseTxtR.textBaseline = "middle";
    
        //
     
        scoreTxtR = new createjs.Text(gscore, "bold 35px Veggieburger-Bold", "#FF9900");
        container1.parent.addChild(scoreTxtR);
        scoreTxtR.x = 840; scoreTxtR.y = 560;
        scoreTxtR.textAlign = "center";
        scoreTxtR.textBaseline = "middle";
    
        
    
        questionTxtR.visible = false;
        attemptTxtR.visible = false;
        correctTxtR.visible = false;
        responseTxtR.visible = false;
        scoreTxtR.visible = false;
    */


    bitmap.visible = true;
    var container2 = new createjs.Container();
    stage.addChild(container2)
    container2.parent.addChild(bitmap)

    
    htmlRedirect(nav, url, tqcnt, aqcnt, cqcnt, gscore, gtime, rtime, crtime, wrtime)
    
    if (cqcnt == 0) {
        resultLoading.resultScreenMc.totalScoreMc.ScoreAnimMc.gotoAndStop(0);
        getStars = 0;
    }
    if (cqcnt>0 && cqcnt<= 4) {
        resultLoading.resultScreenMc.totalScoreMc.ScoreAnimMc.gotoAndStop(3);
        getStars = 1;
    }
    if (cqcnt > 4 && cqcnt<= 7) {
        resultLoading.resultScreenMc.totalScoreMc.ScoreAnimMc.gotoAndStop(6);
        getStars = 2;
    }
    if (cqcnt > 7 && cqcnt<= 10) {
        resultLoading.resultScreenMc.totalScoreMc.ScoreAnimMc.gotoAndStop(10);
        getStars = 3;
    }
    if (cqcnt == 10) {
        resultLoading.resultScreenMc.totalScoreMc.ScoreAnimMc.gotoAndStop(10);
    }


    resultLoading.resultScreenMc.totalScoreMc.cTxt.text = gscore;
    resultLoading.resultScreenMc.totalScoreMc.cTxt.font = "bold 80px Questrial-Regular";
    
    if(cqcnt == 0){
        resultLoading.resultScreenMc.starAnimMc.gotoAndStop(0)
    }
    if(cqcnt > 0 && cqcnt<=4){
        resultLoading.resultScreenMc.starAnimMc.gotoAndPlay(1)
        resultLoading.resultScreenMc.starAnimMc.addEventListener("tick", onStarAnimationHandler)
    }
    if(cqcnt>4 && cqcnt<=7){
        resultLoading.resultScreenMc.starAnimMc.gotoAndPlay(1)
        resultLoading.resultScreenMc.starAnimMc.addEventListener("tick", onStarAnimationHandler1)
    }
    if(cqcnt>7 && cqcnt<= 10){
        resultLoading.resultScreenMc.starAnimMc.gotoAndPlay(1)
        resultLoading.resultScreenMc.starAnimMc.addEventListener("tick", onStarAnimationHandler2)
    }
    
    //////////////////////////////////////////////////////////////////////////////////
}

function onStarAnimationHandler(e){
    console.log("currentFrame= "+e.currentTarget.currentFrame)
    if(e.currentTarget.currentFrame == 18)
    {
        e.currentTarget.gotoAndStop(18);
        e.currentTarget.stop()
        resultLoading.resultScreenMc.starAnimMc.removeEventListener("tick", onStarAnimationHandler)
    }
    
}
//
function onStarAnimationHandler1(e){
    console.log("currentFrame= "+e.currentTarget.currentFrame)
    if(e.currentTarget.currentFrame == 33)
    {
        e.currentTarget.gotoAndStop(33);
        e.currentTarget.stop()
        resultLoading.resultScreenMc.starAnimMc.removeEventListener("tick", onStarAnimationHandler1)
    }
    
}
//
function onStarAnimationHandler2(e){
    console.log("currentFrame= "+e.currentTarget.currentFrame)
    if(e.currentTarget.currentFrame == 66)
    {
        e.currentTarget.gotoAndStop(66);
        e.currentTarget.stop()
        resultLoading.resultScreenMc.starAnimMc.removeEventListener("tick", onStarAnimationHandler2)
    }
}


//----------------------------------------------------------------------------------------------------------------//
function calculatescore() {
    switch (rst) {
        case 0:
        case 1:
        case 2:
        case 3:
            score = score + 10;
            break;

        case 4:
            score = score + 9;
            break;
        case 5:
            score = score + 8;
            break;
        case 6:
            score = score + 7;
            break;
        case 7:
            score = score + 6;
            break;
        case 8:
            score = score + 5;
            break;
        case 9:
            score = score + 4;
            break;
        case 10:
            score = score + 3;
            break;
        default:
            score = score + 2;
            break;

    }
    rst1 = rst1 + rst;
}


function randomSort(a, b) {
    if (Math.random() < 0.5) return -1;
    else return 1;
}