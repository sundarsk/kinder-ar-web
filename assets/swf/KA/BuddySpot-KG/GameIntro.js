(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhfAzIgCgzIgBgnIgBgeIgBgrIAvgBIAAA7QAHgMAJgKQAIgLAKgIQAKgIALgEQALgFANgBQAPgBAMAEQALADAIAHQAJAGAFAJQAFAJADAKIAFATIACARQABAigBAkIgBBKIgqgCIADhCQABghgCggIAAgKIgCgMIgFgNQgDgHgFgEQgFgFgHgDQgHgCgKABQgQADgPAVQgRAVgTAoIABA0IABAeIAAAQIgsAFIgCg/g");
	this.shape.setTransform(233.9,23.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AAHgoIgxCOIg9AFIgojUIAqgDIAfCrIA9irIAmACIAqCtIAbiuIAuABIgoDVIg9ACg");
	this.shape_1.setTransform(205.4,23.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgqBwQgUgJgPgPQgPgRgIgWQgJgWAAgbQAAgaAJgWQAIgXAPgQQAPgPAUgIQAUgJAWAAQAWgBAUAKQAUAJAPAQQAPAQAJAWQAJAWAAAZQAAAagJAWQgJAWgPAQQgPAQgUAJQgUAJgWAAQgWABgUgJgAgdhMQgNAIgIAMQgIAMgDAPQgEAPAAAOQAAAPAEAOQAEAPAIAMQAIAMANAIQAMAHAQAAQARAAANgHQAMgIAIgMQAJgMADgPQAEgOAAgPQAAgOgDgPQgEgPgIgMQgIgMgNgIQgMgHgSAAQgRAAgMAHg");
	this.shape_2.setTransform(177.1,23.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AA3CcQAGgUADgTIADghIAAghQgBgWgGgPQgGgOgKgKQgJgKgMgEQgLgFgLAAQgLABgMAGQgLAFgLALQgMAJgKATIgBCHIgoABIgEk7IAwgBIgCB8QAMgMAMgHQANgHALgEQAMgEAMgBQAXAAATAIQASAIAOAPQAOAQAHAWQAJAVAAAbIAAAeIgBAeIgDAbQgCAOgEAKg");
	this.shape_3.setTransform(151.1,18.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgdB0IgRgEIgTgGIgTgJQgJgFgJgHIAUgfIAXALIAYAKIAZAHQAMACAOAAQALAAAIgCQAIgCAFgDQAFgEADgEIAEgHIAAgHQAAgFgCgEQgCgEgEgEQgEgEgGgDQgHgDgJgCQgKgCgMgBQgSgBgRgCQgRgEgNgGQgOgGgJgJQgIgLgCgPQgCgPAEgMQADgNAHgJQAHgKAKgHQAKgHAMgFQAMgFANgCQANgDANAAIASABQALABAMACQAMADAMAFQAMAEALAIIgOAlQgOgHgMgEQgMgEgKgCIgUgEQgggBgRAIQgSAJAAARQAAAMAGAGQAHAGAMACQALADAQAAIAgAEQAUADAOAGQAOAFAIAIQAIAJAEAKQAEAKAAAKQAAAUgIANQgIANgNAJQgNAIgRAEQgSAEgSABQgSgBgTgDg");
	this.shape_4.setTransform(125.2,24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AguBuQgVgIgOgQQgOgOgHgWQgIgUAAgaQAAgZAJgWQAIgWAPgRQAQgQAUgJQAVgKAWAAQAWAAATAIQASAJAPAOQAOAPAKAUQAJATADAYIiwAZQABAPAGALQAGAMAIAIQAJAIAMAEQAMAEAMAAQALAAALgDQAKgEAKgGQAJgHAHgJQAGgKADgMIAnAIQgFASgLAPQgKAPgOALQgOALgRAGQgQAGgTAAQgZAAgWgIgAgRhPQgKADgJAIQgKAGgIAMQgHAMgDASIB6gOIgBgFQgIgUgOgLQgNgMgVAAQgHAAgLADg");
	this.shape_5.setTransform(90.6,23.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgsCrQgJgDgKgEQgKgEgJgHQgJgHgIgJQgHgJgFgNIAogRQADAJAGAGQAFAGAHAFIANAHIANADQAPAEAPgCQAPgDAKgGQALgGAHgHQAIgIAEgIQAFgIACgHQADgHABgGIABgKIAAgbQgMAMgNAGQgOAGgLADQgNAEgMAAQgYAAgUgIQgVgIgPgQQgPgPgIgVQgJgXAAgbQAAgcAJgWQAKgWAQgPQAPgPAUgHQAUgIAUAAQANABAOAFQAMAEAOAHQAOAHAMAMIAAgoIAoABIgBDqQAAANgDAOQgEANgGAMQgHANgJALQgKALgMAIQgMAJgQAFQgPAFgSACIgGAAQgVAAgTgFgAgeh8QgNAGgJALQgJAKgFAPQgFAOAAARQAAARAFAOQAGAOAJAJQAJAKANAGQAMAGAPAAQANAAAMgFQANgFALgIQAKgHAHgMQAHgMADgOIAAgbQgCgPgIgLQgHgNgLgIQgLgJgMgEQgOgFgMAAQgPAAgMAGg");
	this.shape_6.setTransform(63.4,28.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA/B1IADgdQgZAQgYAHQgXAHgVAAQgOAAgOgDQgOgEgKgIQgLgIgGgMQgHgMAAgRQAAgQAFgNQAFgOAIgKQAJgJAMgHQALgHAOgEQANgFAPgCQAOgDAOABIAZABIAVACIgGgUQgEgKgGgIQgGgHgJgFQgJgGgMAAQgIABgKACQgKACgMAGQgMAGgPALQgOALgRARIgZgeQAUgTASgLQASgMAQgGQAQgIANgCQANgCALgBQATABAOAGQAPAHALALQALALAIAPQAIAPAFARQAFASACASQACASAAATQAAATgCAWIgHAygAgJAAQgRADgNAIQgMAJgHALQgGAMAEAPQADAMAIAFQAIAGAMAAQAMAAAOgFQAMgDAOgHQAOgFAMgHIAUgLIABgVQAAgKgBgLQgKgCgLgBQgLgDgLAAQgTAAgQAFg");
	this.shape_7.setTransform(36.6,22.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("ABiAxIAAgsIgBghQAAgRgFgIQgFgIgIAAQgFAAgFADQgFADgFAFQgGAEgFAHIgJANIgJANIgHALIABASIABAZIAAAeIAAAnIgnADIAAhAIgCgsIgCghQgBgRgEgIQgFgIgIAAQgFAAgGADQgFADgGAHQgGAFgFAHIgKAOIgJAOIgIALIACBrIgpADIgFjcIAqgEIABA7IAOgRQAHgJAJgHQAIgHAKgFQAKgEAMAAQAJAAAIACQAIADAFAGQAHAGAEAJQAFAJABAMIAOgQQAHgJAIgGQAIgHAKgFQAKgEALAAQAJAAAJADQAIADAHAGQAHAHAEAKQAEAKABAOIACAmIACAxIAABHIgqADIgBhAg");
	this.shape_8.setTransform(7.2,22.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgZCaIABg0IABhAIABhYIAqgBIgBA1IgBAsIgBAkIgBAcIgBAsgAgLhhQgEgCgFgEQgEgEgCgFQgCgGAAgFQAAgHACgFQACgGAEgDQAFgEAEgCQAGgDAFAAQAGAAAFADQAGACAEAEQADADADAGQACAFAAAHQAAAFgCAGQgDAFgDAEQgEAEgGACQgFACgGABQgFgBgGgCg");
	this.shape_9.setTransform(-14.7,18.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgtBuQgVgIgOgQQgPgOgIgWQgHgUAAgaQAAgZAJgWQAJgWAOgRQAQgQAUgJQAUgKAYAAQAVAAASAIQATAJAPAOQAPAPAJAUQAJATADAYIixAZQACAPAFALQAHAMAJAIQAIAIAMAEQAMAEANAAQALAAAKgDQALgEAIgGQAKgHAGgJQAHgKADgMIAoAIQgGASgKAPQgLAPgOALQgOALgQAGQgRAGgSAAQgbAAgUgIgAgQhPQgKADgKAIQgKAGgHAMQgIAMgEASIB7gOIgBgFQgIgUgNgLQgOgMgUAAQgIAAgKADg");
	this.shape_10.setTransform(-43.4,23.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AA3CcQAGgUADgTIADghIAAghQgBgWgGgPQgHgOgJgKQgKgKgLgEQgLgFgLAAQgLABgMAGQgLAFgLALQgMAJgKATIgBCHIgoABIgEk7IAwgBIgCB8QAMgMAMgHQANgHALgEQAMgEAMgBQAXAAATAIQASAIAOAPQAOAQAHAWQAJAVAAAbIAAAeIgBAeIgDAbQgCAOgEAKg");
	this.shape_11.setTransform(-69.5,18.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIABhaIAogDIgBBcIBPgDIgDAnIhNACIgBCxIgrACg");
	this.shape_12.setTransform(-93.5,19.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AA3CcQAFgUADgTIAEghIABghQgCgWgGgPQgHgOgJgKQgKgKgLgEQgLgFgMAAQgKABgMAGQgKAFgMALQgMAJgKATIgBCHIgoABIgEk7IAvgBIgBB8QALgMANgHQANgHALgEQANgEALgBQAXAAATAIQATAIANAPQANAQAJAWQAHAVACAbIAAAeIgCAeIgDAbQgCAOgDAKg");
	this.shape_13.setTransform(-127.9,18.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AglBvQgWgKgQgQQgRgPgKgWQgLgWAAgZQAAgPAFgPQAEgOAIgOQAHgNALgMQALgLANgIQAOgIAPgEQAQgFAQgBQASAAAQAFQAQAFAOAHQANAJALALQALAMAHAOIABABIgiAVIgBgBQgFgKgIgJQgHgIgKgGQgJgGgLgDQgKgDgMAAQgQAAgPAGQgOAHgLALQgLALgHAPQgGAOAAARQAAAQAGAOQAHAPALAMQALALAOAGQAPAGAQAAQALAAAKgDQAKgCAJgGQAJgFAIgHQAIgIAFgJIABgCIAjAVIAAABQgIAOgMAKQgLALgOAHQgOAIgPAEQgQAEgQABQgWAAgWgJg");
	this.shape_14.setTransform(-154.6,23.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgYgXIhHACIABgmIBHgCIABhaIAogDIgBBcIBPgDIgDAnIhNACIgBCxIgrACg");
	this.shape_15.setTransform(-177.3,19.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AA/B1IADgdQgZAQgYAHQgXAHgVAAQgOAAgOgDQgOgEgKgIQgLgIgGgMQgHgMAAgRQAAgQAFgNQAFgOAIgKQAJgJAMgHQALgHAOgEQANgFAPgCQAOgDAOABIAZABIAVACIgGgUQgEgKgGgIQgGgHgJgFQgJgGgMAAQgIABgKACQgKACgMAGQgMAGgPALQgOALgRARIgZgeQAUgTASgLQASgMAQgGQAQgIANgCQANgCALgBQATABAOAGQAPAHALALQALALAIAPQAIAPAFARQAFASACASQACASAAATQAAATgCAWIgHAygAgJAAQgRADgNAIQgMAJgHALQgGAMAEAPQADAMAIAFQAIAGAMAAQAMAAAOgFQAMgDAOgHQAOgFAMgHIAUgLIABgVQAAgKgBgLQgKgCgLgBQgLgDgLAAQgTAAgQAFg");
	this.shape_16.setTransform(-201,22.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("ABghjIhEDeIgxAFIhNjXIglDqIgrgFIA4kqIArAAIBRDrIBKjoIAwgBIA3EvIgqAIg");
	this.shape_17.setTransform(-232.9,19.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(3,0,0,6.9).p("EgjOgEtMBGdAAAQBrAABMBMQBNBMAABsIAABTQAABrhNBMQhMBNhrAAMhGdAAAQhrAAhMhNQhNhMAAhrIAAhTQAAhsBNhMQBMhMBrAAg");
	this.shape_18.setTransform(0,18.1,1.1,1.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("EgjOAEuQhrAAhMhNQhNhLAAhsIAAhTQAAhsBNhMQBMhMBrAAMBGcAAAQBsAABMBMQBNBMAABsIAABTQAABshNBLQhMBNhsAAg");
	this.shape_19.setTransform(0,18.1,1.1,1.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-278.1,-16.6,556.2,69.5);


(lib.Tween31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AiVAIQgDgHAAgGQgDg9BLALQCSgPBRBkQAKANgIAPQiKhSigAgg");
	this.shape.setTransform(40.5,-24.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AiUAfQBShkCSANQBKgIgBA7QAAAHgFAHQifgiiKBTQgIgPAJgMg");
	this.shape_1.setTransform(-38,-24.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgpAtQg4gvARg5QAdBdBbgdQAVgFAEgZQAEgUANgNQATBQhOAgQgPAGgMAAQgUAAgRgPg");
	this.shape_2.setTransform(-1.6,17.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AhJBHQgZgcAAgrQAAgQADgPQADAJAGAGQARAQAUAAQAWAAANgQQAPgPABgTQgBgVgPgQQgGgGgFgDQANgEAOAAQAqAAAcAdQAcAeAAApQAAArgcAcQgcAegqAAQgrAAgggeg");
	this.shape_3.setTransform(41.1,-6.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AhcBdQgmgpAAg0QAAg1AmgnQAogmA0AAQA2AAAmAmQAnAnAAA1QAAA0gnApQgmAmg2AAQg0AAgogmgAhpAZQAAArAaAcQAfAeAsAAQApAAAcgeQAcgcAAgrQAAgpgcgeQgcgdgpAAQgPAAgNAEQAFACAHAHQAQAQAAAVQAAATgQAOQgNAQgXAAQgTAAgRgQQgHgHgDgHQgDAOAAARg");
	this.shape_4.setTransform(41.7,-8.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AhIBHQgcgcAAgrQABgQAFgPQABAJAHAGQARAQAVAAQASAAAPgQQAPgPAAgTQAAgVgPgQQgHgGgFgDQAOgEANAAQArAAAbAdQAeAeABApQgBArgeAcQgbAegrAAQgpAAgfgeg");
	this.shape_5.setTransform(-41.9,-6.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AhcBdQgmgpAAg0QAAg1AmgnQAngmA2AAQA2AAAmAmQAmAnAAA1QAAA0gmApQgmAmg2AAQg2AAgngmgAhqAZQAAArAcAcQAfAeAqAAQAqAAAbgeQAegcABgrQgBgpgegeQgbgdgqAAQgOAAgOAEQAFACAHAHQAQAQAAAVQAAATgQAOQgPAQgSAAQgVAAgRgQQgHgHgBgHQgFAOgBARg");
	this.shape_6.setTransform(-41.3,-8.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AjCANQg2gagQgkQEUAsD9gvQgRAjg4AeQhRAohvAAQhzAAhPgog");
	this.shape_7.setTransform(-0.5,35.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("Aj6AMQhBgcgYgrQAmgDAjgBQAQAkA2AbQBPAnB0AAQBuAABRgnQA4geARgkQAmACAnAAQgYArhCAhQhpA3iSAAQiRAAhog3g");
	this.shape_8.setTransform(-0.3,37.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AuGiCQgNhVAAhZQAAjGBEioQgnBEg5BiQAEhiAhhYQAWhIAmhAQBbibCRhSQA9gjBHgUQBIgwBQgdQhUgQhUAUQByghB0AHQBVADBZAbQAHglAcgeQAcgeAxgXQASgJAUgGQBygoBkgRQhXARhMA3QgeAXgdAcQEbAODeCzQBPAtBVBGQB2BhA+ChQA4CSALDGQgLgdgKgcQgOgqgbgeQANBQAABbQAAAogEAqQgCAogFAnArMETQgbifgMhYQgTiOADh3QADhMAOhJQBGgPBYgfQBngeA2gZQAqgTAjgYQgOAZgZAaQhRBkh2BLQB2giBmhFQBEgrBYiKQAcBGARAjQArBYA1A7QAVAYAZAWQgthWgWhcQBdBSC+AgQAfAFAfAFQgfgmgfgwQgfg5gZgqQAuAZBBAMQBHANCTAXQBpAbBTBOQANANAMALQBABGAiBYQAlBugLCoQgDAmgEAyQBfigAfi4QASAVAJAmQAFARAEAQAO2gVQAGBHgfA/QguBShnAuQgPAEgPAGQgqA9g2A4QjEDNkCA0QhdAUhlAAQh4AAhsgcQjtg8i5i9QgigmgfgnQAAgIgDgIABENAIBVAAIBMBQIAMAMADEMqIgrAWADZOeQAMgCAMAAADlOQIgMAOIiVCmIAAACIAAAWIA7EIADEMqIDIhoIhQBZQDUBuCCgOQDigPAADgIAAEYADEJxIAAC5ABEREIgCACIACAAAE8MbIBQEkIibijABENAIAAEEAuGiCQgNAHgMAJQgZASgIAcQgOAqAOA9QAZBcBRBEQA3AsBFAfIABABQAFACAIACArZEPQgBgBAAgCQiHi0gljaAjiMxIjShvIAADaIAACjICVidQAPAAAPAAICRChIAEADICuAAAuiVkIAAkYQAAjgDnAdQCFANCCAGAjCNAIhOBQIAPASAkQOQIgPASAjCNAIBSAAIAAEDIAAADIAEAAAjiMxIAgAPAjiMxIAAjIAhwRGIAAAWIg6EIAhwNAIC0AA");

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AhVCDIgFgDIAAkCIC1AAIAAEDIgCACg");
	this.shape_10.setTransform(-2.2,96.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("AiUCPIA6kHIAAgWIAEAAICvAAIABAAIAAAWIA7EHg");
	this.shape_11.setTransform(-2.2,123.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("ACWEkIg7kIIAAgWIAAgBICWimQAMgBAMAAICaCiIhQkjQDVBtCCgNQDhgQAADgIAAEXgAuKEkIAAkXQAAjgDnAdQCEANCCAHIAACiICWidIAeAAICQCgIAAADIAAAWIg6EIg");
	this.shape_12.setTransform(-2.3,108.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#129293").s().p("AEGAbIgMgLIhMhQIArgVIDHhoIhQBZIBQEigAmgAbIAAjYIDTBuIAgAPIhOBQIgPARIiWCdg");
	this.shape_13.setTransform(-2,89.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#13B0B1").s().p("ABaiBIBVAAIBMBQIgMAOIiVClgAjrggIgPgRIBOhQIBSAAIAAEBg");
	this.shape_14.setTransform(-2.2,96.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("ABMLFIi0AAIhRAAIghgQIAAjIQjsg8i5i9QgigmgggnQAAgIgCgIQgciegMhXQgSiQADh3QAChMAPhJQBFgPBYgeQBogeA1gZQAqgUAkgYQgPAZgYAbQhSBjh2BLQB2giBnhEQBEgsBYiJQAbBFARAjQAsBYA0A8QAVAXAZAXQgthXgWhcQBdBSC/AgIA+ALQgggngegwQggg5gZgqQAuAZBCAMQBHANCTAXQBpAcBSBNIAZAZQBBBFAiBYQAkBvgKCoIgIBYQBgigAei4QASAUAKAmIAJAiIAFAdQAHBIggA/QguBRhmAuQgQAEgOAGQgqA9g2A4QjFDNkBA0QhdAUhlAAQh4AAhtgcQBtAcB4AAQBlAABdgUIAAC6IgrAWgArSCSQhEgeg3gtQhRhEgZhbQgPg+APgqQAHgcAZgRQAMgKANgHQAlDbCHCzQAAABAAAAQABABAAAAQAAAAAAAAQAAABABAAg");
	this.shape_15.setTransform(-0.8,12.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("Ar5IzQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAgBAAAAQgGhFgXhRQgriHgUhEQghhyAFheQAEh0A9hNQAfgnA3gjQAhgWBIgnQBHgmAggWIARgJQBlhKAThYQAXBRgEBRQgCAxgLA0QAigxAWg0QAjhQAKhaQAaBWARBUQAWBhANBgQgCiAgDhBIAAgHQAEADABAEQB8CoCuByQgRhLgmhEQgmhDg2g1IKBCdQBPATAnASQA/AZAnApQAgAjASA8QALAhALA8QgCAogFAnQgfC5hfCgIAHhYQALipglhuQgihZhAhEIgZgZQhThNhpgbQiTgXhHgNQhBgMgugZQAZAqAfA5QAfAwAfAmIg+gLQi+gfhdhTQAWBcAtBWQgZgWgVgXQg1g7grhYQgRgjgchGQhYCKhEArQhmBEh2AjQB2hMBRhjQAZgaAOgZQgjAXgqAUQg2AZhnAeQhYAehGAQQgOBIgDBLQgDB4ATCPQAMBYAbCfQgIgCgFgDg");
	this.shape_16.setTransform(3.2,-29.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AuSGpQgNhVAAhaQAAjGBEinIhgCmQAEhiAhhXQAWhJAmhAQBbiaCRhSQA9gjBHgUQBIgwBQgeQhUgPhUAUQBygiB0AIQBVADBZAaQAHglAcgdQAcgfAxgWQASgJAUgHQBygnBkgRQhXARhMA3QgeAXgdAcQEbAODeCzQBPAsBVBHQB2BhA+ChQA4CRALDFIgVg4QgOgqgbgeQANBQAABaQAAApgEAqQgLg8gLgiQgSg8gggjQgngpg/gZQgngShPgTIqBicQA2A1AmBCQAmBEARBLQiuhyh8inQgBgEgEgDIAAAHQADBBACB/QgNhfgWhhQgRhUgahWQgKBagjBQQgWA0giAxQALg0ACgxQAEhRgXhRQgTBYhlBKIgRAJQggAWhHAmQhIAnghAWQg3AigfAnQg9BNgEB0QgFBfAhByQAUBEArCHQAXBRAGBFQiHi0gljbg");
	this.shape_17.setTransform(1.2,-55.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-97.8,-138.9,195.6,278);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AiEAIQgEgGAAgHQgBg1BDAJQCCgNBIBZQAJAMgHANQh7hIiPAcg");
	this.shape.setTransform(35.9,-23.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AiFAbQBKhYCBALQBEgIgBA2QAAAGgEAGQiOgeh8BKQgGgNAGgMg");
	this.shape_1.setTransform(-33.9,-24.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AglAnQgygpAQgzQAaBTBPgZQAUgFAEgXQADgRALgMQATBIhHAcQgNAGgKAAQgSAAgQgPg");
	this.shape_2.setTransform(-1.5,13.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AhABAQgYgZAAgmQAAgOADgNIAJANQAPANASAAQAUAAAKgNQAOgNAAgRQAAgUgOgOIgJgIQALgEAMAAQAmAAAaAaQAYAaAAAmQAAAmgYAZQgaAagmAAQglAAgcgag");
	this.shape_3.setTransform(36.5,-7.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AhSBTQgiglAAguQAAgwAigjQAkghAuAAQAxAAAgAhQAkAjAAAwQAAAugkAlQggAigxAAQguAAgkgigAheAXQAAAmAYAZQAcAaAmAAQAlAAAagaQAYgZAAgmQAAgmgYgaQgagaglAAQgNAAgLADIAJAIQAPAPAAAUQAAAQgPANQgKANgUAAQgSAAgPgNIgJgMQgDAMAAAPg");
	this.shape_4.setTransform(37.1,-10);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("Ag/BAQgZgZAAgmQAAgOAEgNQABAHAHAGQAPANATAAQAQAAANgNQANgNAAgRQAAgUgNgOIgLgIQANgEALAAQAmAAAYAaQAbAaAAAmQAAAmgbAZQgYAagmAAQglAAgagag");
	this.shape_5.setTransform(-37.5,-7.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AhSBTQgjglAAguQAAgwAjgjQAjghAxAAQAvAAAhAhQAjAjAAAwQAAAugjAlQghAigvAAQgxAAgjgigAhfAXQAAAmAZAZQAbAaAmAAQAlAAAXgaQAcgZAAgmQAAgmgcgaQgXgaglAAQgMAAgNADIALAIQAOAPAAAUQAAAQgOANQgNANgQAAQgTAAgPgNQgHgHgBgFQgEAMgBAPg");
	this.shape_6.setTransform(-36.8,-10);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AisALQgxgWgOghQD1AnDigqQgOAfg0AbQhIAlhhAAQhoAAhFglg");
	this.shape_7.setTransform(-0.6,29.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AjfALQg6gZgVgmIBAgFQAPAhAwAYQBGAkBpAAQBgAABIgkQAzgcAPggQAiACAjAAQgWAog7AdQhdAyiBgBQiCABhdgyg");
	this.shape_8.setTransform(-0.3,30.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("AskiMQgMhLAAhQQAAixA+iVQglA9gxBXQADhXAdhPQAUhAAig6QBQiJCChJQA3gfA9gSQBCgsBHgZQhLgNhLARQBlgdBoAGQBMACBPAYQAGggAbgbQAXgcAsgSQAQgJASgFQBmgkBYgQQhMAQhEAyQgcASgaAbQD9ALDFCgQBHApBLA+QBqBWA3CPQAyCCALCxQgLgagJgXQgMgmgYgcQAMBIAABQQAAAlgFAlQgBAjgGAkAp+DcQgZiMgLhPQgQh/ADhqQABhEANg/QA/gOBNgcQBcgaAygXQAkgSAggUQgNAVgWAYQhIBZhpBDQBpgeBag8QA+goBNh7QAZA/AQAfQAnBOAuA0QATAVAWAVQgnhOgVhQQBUBJCpAcQAaADAdAGQgdgigagsQgdgygVgmQAoAYA7AJQBAAMCCAUQBdAZBKBFQALAMAMAJQA5A/AeBOQAhBigKCWQgDAigDAtQBViPAaijQARASAIAiQAGAOACAPANNgrQAGBBgcA3QgpBIhbApQgNAFgOAFQgmA2gvAyQiwC2jlAwQhTARhZAAQhrAAhhgYQjTg2ikipQgegigdgkQAAgGgCgIACuK6IgmAVIBEBGIgMAMQAMgCAKAAAA7LPIBNAAACuIWIAACkICxhcIAAC6IAACaIiJiTADAMhIiFCUIgBACIABAAIAAATIA/EcAA7O1IAAACADMMVIAKAKAA7LPIAADmAFfMYQB3gKB0gMQDKgPAADJIAAEqAskiMQgMAHgLAHQgWAQgGAZQgOAmAOA2QAVBSBJA8QAyApA8AaIACABIAAAAQAEADAHAAAqJDZQgCgBAAgBQh6iggfjDAjKLAIAdAPIBIAAIAADlIAEADICbAAAjKLAIAAixAj0MVIAPAQICACPIAAADIAAATIg+EcAj0MVIgNAQQANAAAPAAAmGMfIAACTICFiNAhlO3IAEAAAjKLAIi8hiIAADBAitLPIhHBGAs8TmIAAkqQAAjJDNAbQB2ALBzAGAhlLPICgAA");

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#07BABB").s().p("AiOCXIA+kaIAAgTIAEAAICbAAIABAAIAAATIA/Eag");
	this.shape_10.setTransform(-2,110.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#10DADB").s().p("AhLB0IgFgCIAAjlICgAAIAADlIgBACg");
	this.shape_11.setTransform(-2.1,83.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("ACPDyIg/kaIAAgUIAAgBICFiUQALgDALAAICICTIAAiZQB4gLBzgMQDKgOAADJIAAEogAsnDyIAAkoQAAjJDMAaQB2AMB0AFIAACTICEiMIAcAAICBCPIAAACIAAAUIg+Eag");
	this.shape_12.setTransform(-2,101.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("ABQhyIBMAAIBEBGIgLAMIiFCTgAjRgcIgOgQIBGhGIBJAAIAADkg");
	this.shape_13.setTransform(-2,83.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#129293").s().p("ADqAXIgKgKIhEhFIAmgVICxhcIAAC6IAACZgAlyAXIAAjAIC8BjIAdAOIhHBFIgNAQIiFCNg");
	this.shape_14.setTransform(-2,77.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("ABEJ4IigAAIhJAAIgcgOIAAiyQjUg2ikipQgegigdgjIgBgPQgZiLgMhPQgQh/ADhqQABhFANg/QA/gOBOgcQBcgaAygXQAjgSAhgTQgNAUgXAZQhIBYhpBDQBpgeBbg8QA9gnBOh8QAZA/AQAfQAnBOAuA1QASAUAXAWQgohPgVhQQBUBJCqAcQAaADAdAGQgdgigagrIgyhYQAnAYA7AIQBBAMCBAUQBeAZBJBFQAMAMAMAKQA4A+AeBOQAhBigJCXIgHBOQBWiOAaikQARASAIAjQAFAOADAOIAFAbQAFBBgbA3QgpBHhcApIgaALQgmA1gvAyQiwC2jmAwQhTARhZAAQhqAAhhgYQBhAYBqAAQBZAABTgRIAACkIgmAVgAqCCBQg9gagxgpQhKg8gVhRQgNg2ANgnQAHgYAWgRQAKgHAMgGQAgDDB6CfIABACg");
	this.shape_15.setTransform(-0.8,8.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AqkH3IgBgBIgBgCQgGg9gUhHQgnh5gRg8QgdhnAEhUQAEhmA1hFQAcgjAyggIBdg2QBAggAcgWQAIgCAIgGQBZhBARhNQAUBHgDBHQgBAugMAtQAegrAVgwQAghGAJhQQAWBMAQBKQATBXALBWQgBhygDg7IAAgGQAEAEAAACQBvCXCcBkQgPhCgig9Qgig7gwgvII6CMQBGARAkAQQA4AYAjAjQAcAfAPA1QALAdAJA2QgBAkgGAjQgaCkhVCPIAGhPQAKiXghhiQgehOg5g9QgMgKgLgMQhKhFhdgZQiCgUhAgMQg7gIgogYIAyBYQAaArAdAiQgdgGgagDQipgchUhJQAVBQAnBOQgWgVgTgUQgug1gnhOQgQgfgZg/QhNB8g+AnQhaA8hpAeQBphDBIhYQAWgZANgUQggATgkASQgyAXhcAaQhNAcg/AOQgNA/gBBEQgDBqAQB/QALBPAZCMQgHAAgEgCg");
	this.shape_16.setTransform(2.7,-28.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AsvF7QgMhLAAhQQAAixA+iVQglA+gxBVQADhVAdhPQAUhAAig6QBQiKCChIQA3ggA9gSQBCgrBHgZQhLgNhLARQBlgdBoAFQBMADBPAXQAGgfAbgcQAXgbAsgTQAQgJASgFQBmgjBYgQQhMAQhEAxQgcATgaAaQD9AMDFCgQBHApBLA9QBqBXA3CPQAyCBALCxIgUgyQgMgmgYgbQAMBIAABQQAAAkgFAlQgJg2gLgeQgPg0gcggQgjgjg4gYQgkgQhGgRIo6iLQAwAvAiA6QAiA+APBCQichlhviVQAAgDgEgEIAAAHQADA7ABBwQgLhUgThXQgQhLgWhMQgJBQggBHQgVAvgeArQAMgtABgtQADhIgUhHQgRBNhZBCQgIAFgIADQgcAVhAAhIhdA1QgyAfgcAjQg1BFgEBnQgEBUAdBnQARA8AnB5QAUBIAGA8Qh6iggfjDg");
	this.shape_17.setTransform(1.1,-51.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-87.3,-126.3,174.6,252.8);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AiVAIQgEgGAAgIQgCg8BMAKQCSgOBRBkQALAOgJAPQiKhSihAfg");
	this.shape.setTransform(40.4,-20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AiWAeQBThjCRANQBNgJgBA8QAAAHgFAHQiggiiLBTQgHgPAHgNg");
	this.shape_1.setTransform(-38.1,-20.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgqAsQg4guASg5QAdBdBZgcQAWgGAFgaQADgTANgNQAVBQhQAgQgOAGgLAAQgVAAgSgQg");
	this.shape_2.setTransform(-1.7,21.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AhIBIQgbgcAAgrQAAgPADgQIALAPQAQAPAVAAQAWAAALgPQAQgPAAgTQAAgWgQgQIgKgJQAMgEAOAAQAqAAAeAeQAbAcAAArQAAArgbAcQgeAdgqAAQgqAAgfgdg");
	this.shape_3.setTransform(41,-2.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AhcBdQgmgpAAg0QAAg2AmgoQAoglA0AAQA3AAAlAlQAnAoAAA2QAAA0gnApQglAmg3AAQg0AAgogmgAhpAZQAAArAbAcQAfAeAqAAQAqAAAegeQAagcAAgrQAAgqgagdQgegdgqgBQgOAAgMAFIAKAJQAQAQAAAWQAAATgQAOQgMAOgWAAQgVAAgQgOIgKgOQgDAOAAAQg");
	this.shape_4.setTransform(41.7,-4.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AhHBIQgcgcgBgrQABgPAEgQQACAIAHAHQARAPAWAAQARAAAPgPQAPgPAAgTQAAgWgPgQIgMgJQAPgEAMAAQArAAAbAeQAeAcAAArQAAArgeAcQgbAdgrAAQgpAAgegdg");
	this.shape_5.setTransform(-42.1,-2.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AhdBdQgmgpAAg0QAAg2AmgoQAoglA3AAQA1AAAlAlQAoAoAAA2QAAA0goApQglAmg1AAQg3AAgogmgAhrAZQAAArAdAcQAdAeArAAQAqAAAageQAfgcAAgrQAAgqgfgdQgagdgqgBQgNAAgPAFIAMAJQAQAQAAAWQAAATgQAOQgPAOgSAAQgVAAgRgOQgHgHgCgHQgFAOAAAQg");
	this.shape_6.setTransform(-41.4,-4.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AjCAMQg3gZgPglQETAsD/gvQgQAjg6AeQhRAqhuAAQh0AAhPgqg");
	this.shape_7.setTransform(-0.6,39.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("Aj7AMQhBgcgYgrQAlgCAkgDQAPAlA3AaQBPApB1AAQBtAABRgpQA6gfAQgjQAmACAogBQgZAthDAhQhpA4iRAAQiRAAhpg4g");
	this.shape_8.setTransform(-0.3,41.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("AuIhdQgOhVAAhaQAAjHBGioQgqBFg2BiQAChiAhhYQAXhIAmhCQBaiaCShSQA+gjBFgUQBKgxBPgcQhTgPhVATQByghB0AHQBWACBZAbQAHgkAegeQAagfAxgVQASgKAUgGQB0goBjgSQhWAShNA4QgfAVgdAdQEcANDeC0QBPAuBVBGQB3BhA+ChQA4CSAMDHQgMgdgKgbQgNgrgbgeQANBRAABZQAAAqgGApQgBAogGAoArNE4QgciegNhZQgSiOADh4QABhNAPhHQBHgPBXgfQBngeA5gZQAngVAlgWQgPAYgZAbQhRBkh2BLQB2giBmhEQBFgsBXiLQAcBHASAjQAsBXA0A7QAVAYAZAYQgshZgYhaQBeBSC/AgQAdAEAhAGQghgmgdgxQghg4gXgrQAsAbBDAKQBIAOCSAVQBpAdBTBOQANANANAKQBABHAhBXQAlBvgKCpQgEAmgEAzQBhihAdi4QATAUAJAnQAGAQADAQAO3APQAGBKgfA+QguBRhnAuQgPAGgPAGQgrA8g1A4QjGDNkCA2QhdAThkAAQh4AAhtgbQjug8i4i/QgigmghgoQAAgHgBgJADDNRIgqAYIBMBPIgNAOQANgDAMAAABDNpIBWAAADDNRIDHhnIAADRQCHgLCCgOQDjgQAADiIAADOADlO4IAMALADDKZIAAC4ABDRuIAAAWIAqC+ABDRsIgBACIABAAABDRsIAAACABDNpIAAEDADYPGIiVCmAGKO7IAACtIiZilAuIhdQgOAHgMAJQgZARgHAdQgPAqAPA+QAYBbBSBEQA4AuBEAdIABABIABABQAEADAJAAArbE0QgBgBAAgBQiJi0gjjbAjjNYIjThuIAADZIAAClICVieAujVCIAAjOQAAjiDmAeQCFANCCAGAjDNpIhPBPIAQASICRChIAEADICvAAAkSO4IgPASQAPAAAQAAAjjNYIAAjHAjDNpIBSAAIAAECIAAADIAAAWIgqC+AjjNYIAgARAhxRuIAEAAAhxNpIC0AA");

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AhVCCIgFgDIAAkBIC0AAIAAECIgBACg");
	this.shape_10.setTransform(-2.3,100.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("AiEBqIAqi9IAAgWIAEAAICvAAIABAAIAAAWIArC9g");
	this.shape_11.setTransform(-2.3,124);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#01627F").s().p("ACEDQIgqi+IAAgVIAAgCICVimQANgDAMAAICZCmIAAitQCHgMCCgNQDjgRAADhIAADOgAuMDQIAAjOQAAjhDmAeQCFANCCAGIAACmICVieIAfAAICRCgIAAADIAAAVIgqC+g");
	this.shape_12.setTransform(-2.3,113.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("ABaiBIBWAAIBMBQIgNAOIiVCkgAjrgfIgQgSIBPhQIBSAAIAAEBg");
	this.shape_13.setTransform(-2.3,100.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#129293").s().p("AEHAaIgLgLIhNhOIArgYIDHhnIAADQIAACtgAmgAaIAAjYIDUBuIAgARIhQBOIgPASIiVCeg");
	this.shape_14.setTransform(-2.2,93.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("ABMLHIi0AAIhRAAIgggRIAAjHQjug8i5i/IhChOIgCgQQgcidgNhZQgSiPAEh3QABhNAOhHQBHgQBXgfQBogeA4gZQAogUAkgXQgOAYgZAbQhRBkh2BLQB2giBlhDQBGgtBXiLQAcBHASAkQAsBXAzA7QAVAXAZAYQgshYgYhaQBfBSC/AfQAdAEAgAHQgggngdgxQghg4gYgrQAtAbBCALQBJANCRAWQBpAcBTBOQANANAOALQA/BHAiBXQAlBugLCqQgEAlgDAzQBgigAdi5QATAVAJAmQAHAQACARIAGAdQAGBKgfA+QguBQhnAuIgeAMQgqA8g1A4QjGDNkCA2QhdAThlAAQh3AAhtgbQBtAbB3AAQBlAABdgTIAAC4IgrAYgArSCRQhEgdg4guQhThEgXhaQgPg+APgrQAHgcAZgSQAMgIANgIQAjDcCKCzIABACg");
	this.shape_15.setTransform(-0.9,16.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("Ar5I1IAAAAIgBgCQgIhFgWhRQgsiIgThDQghh0AFheQAEh0A8hNQAfgoA5giIBpg9QBIglAfgYQAJgCAJgHQBkhJAThYQAWBQgDBRQgBAzgOAzQAjgwAXg2QAkhPAKhaQAYBVASBUQAWBiAMBgQgBh/gDhDIAAgHQAEAEAAADQB+CpCvByQgRhLgmhEQgnhDg1g1IKBCeQBOASApASQA+AbAoAoQAfAkASA7QALAgALA9QgCAogGAnQgdC5hgChQADgzAEgmQALipglhvQgihXg/hGQgOgLgNgNQhThNhpgdQiRgVhJgOQhCgLgtgaQAYAqAhA5QAdAwAgAnQgggHgdgEQi/gfhfhSQAYBaAsBXQgZgWgVgYQgzg7gshXQgSgjgchIQhXCMhGAsQhlBDh2AjQB2hLBRhlQAZgbAOgXQgkAWgoAUQg4AZhoAeQhXAghHAPQgOBHgBBMQgEB3ASCPQANBZAcCeQgIAAgFgDg");
	this.shape_16.setTransform(3.1,-25.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AuVGqQgNhVAAhaQAAjHBFinQgpBFg3BhQADhhAhhYQAWhIAmhCQBaiaCShSQA+gjBFgUQBKgxBQgcQhUgPhUATQBxghB1AHQBWACBYAbQAHgkAegeQAbgfAxgVQASgKAUgGQBzgoBjgSQhVAShNA4QgfAVgeAdQEdANDdC0QBQAuBUBGQB4BhA+ChQA4CRAMDHIgXg4QgNgrgbgeQAOBRAABZQAAAqgGApQgLg9gLghQgSg7gfgkQgogog+gaQgpgShOgTIqBidQA1A1AnBCQAmBFARBKQivhyh+inQAAgDgEgFIAAAIQADBCABB+QgMhfgWhhQgShUgYhWQgKBagkBQQgXA1gjAwQAOgzABgyQADhRgWhQQgTBXhkBKQgJAGgJADQgfAXhIAlIhpA9Qg5AigfAoQg8BNgEB0QgFBfAhBzQATBDAsCJQAWBQAIBFQiKi0gjjcg");
	this.shape_17.setTransform(1.3,-52);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-98,-135.5,196.1,271.1), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag/AiIgCgiIAAgaIgBgUIgBgdIAfAAIABAnQAEgIAGgGQAGgHAHgGQAGgFAHgDQAIgEAIAAQAKgBAIADQAIACAFAEQAGAFADAGQAEAGACAGIADANIABALQABAXAAAYIgBAxIgcgBIACgsIgBgrIAAgHIgBgIIgEgJQgCgEgDgDQgDgEgFgBQgFgCgGABQgLACgKAOQgLAOgNAaIABAkIAAATIABALIgeAEIgBgrg");
	this.shape.setTransform(180.5,50.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAEgbIggBfIgpAEIgbiOIAcgCIAVByIAphyIAaACIAcBzIASh0IAeABIgbCOIgoABg");
	this.shape_1.setTransform(161.3,50.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgcBLQgNgGgJgKQgLgLgGgPQgFgPgBgSQABgRAFgPQAGgPALgKQAJgLANgGQAOgGAOAAQAPAAAOAHQANAGAKALQAKAKAGAPQAFAPABAQQgBARgFAPQgGAPgKALQgKALgNAGQgOAGgPAAQgOAAgOgGgAgTgyQgIAFgGAIQgFAIgDAKQgCAKAAAJQAAAKACAKQADAKAGAIQAGAIAHAFQAJAFAKAAQAMAAAIgFQAIgFAGgIQAFgIADgKQADgKAAgKQgBgJgCgKQgCgKgGgIQgFgIgIgFQgJgFgMAAQgKAAgJAFg");
	this.shape_2.setTransform(142.3,50.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAlBoQAEgNACgNIACgWIAAgVQAAgQgFgJQgEgKgGgGQgHgHgHgDQgIgDgIAAQgGABgIADQgHAEgIAHQgIAGgHAMIAABbIgbABIgDjTIAgAAIgBBSQAIgHAIgFQAJgFAHgCIAQgEQAPAAANAFQANAGAJAKQAJALAFAOQAFAOABASIAAAVIgBATIgCATIgEAQg");
	this.shape_3.setTransform(124.9,47.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgTBOIgMgDIgMgFIgNgFIgMgIIANgVIAQAHIAQAIIARADQAIACAJABQAHAAAFgCIAJgEIAFgFIADgEQABgDgBgDIgBgFIgEgGIgHgEQgEgDgHgBIgPgCQgLAAgLgCQgMgCgJgEQgJgFgGgFQgGgIgBgKQgBgKADgHQACgJAEgHQAFgGAHgFQAGgEAIgEQAIgDAJgBQAJgCAIAAIANAAIAPADQAIABAIADQAIADAHAFIgJAaQgJgFgJgDIgOgEIgOgDQgUAAgMAFQgMAGAAAMQAAAIAEADQAFAEAHACIATADIAVABQANACAKAFQAJADAFAFQAGAGACAGQADAHAAAIQAAANgFAIQgGAJgIAGQgJAGgMACQgLADgNAAQgLAAgNgCg");
	this.shape_4.setTransform(107.6,51.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgeBKQgOgGgKgKQgJgKgFgOQgFgOAAgSQAAgQAGgOQAFgPALgLQAKgLANgHQAOgGAPAAQAOAAANAGQANAFAJAKQAKAKAGANQAHANABAQIh2ARQABAJAEAIQAEAIAGAFQAGAFAIADQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgEAMgHALQgHAKgJAHQgJAHgMAEQgLAEgMAAQgRAAgOgFgAgLg0QgGACgHAEQgGAFgFAIQgGAIgCAMIBSgKIgBgCQgFgOgJgIQgJgHgOAAQgFAAgHACg");
	this.shape_5.setTransform(84.4,50.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgdByIgNgEQgGgDgGgEQgGgFgFgGQgGgGgDgJIAbgLQACAGAEAEIAIAHIAJAFIAIACQAKACAKgBQAKgCAHgEQAHgEAFgFQAFgFADgFIAFgKIADgJIAAgHIAAgSQgIAIgJAEQgJAFgHACIgRACQgQAAgNgFQgOgGgKgKQgKgLgGgOQgGgOAAgTQAAgTAGgOQAHgPAKgKQALgKANgFQANgFAOAAQAIABAKADQAIACAJAFQAJAFAIAIIABgbIAaABIAACcQAAAJgDAJQgCAJgEAIQgFAJgGAHQgGAHgJAGQgIAGgKADQgKAEgNABIgCAAQgPAAgNgEgAgUhTQgIAEgGAHQgGAIgDAJQgEAKAAALQAAAMAEAJQADAKAGAFQAGAHAJAEQAIAEAKAAQAIAAAJgDQAIgEAHgFQAHgFAFgIQAFgIACgJIAAgSQgCgKgFgIQgFgIgHgGQgHgFgJgEQgIgDgJAAQgJAAgJAEg");
	this.shape_6.setTransform(66.2,53.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AArBOIABgTQgQALgQAFQgQAEgOAAQgJAAgKgCQgJgDgHgFQgHgFgEgIQgFgJAAgLQAAgLAEgJQADgIAGgHQAFgGAIgFQAIgEAJgDQAJgEAKgBQAJgBAKAAIAQAAIAOACIgEgNQgDgHgEgFQgEgGgGgDQgGgDgIAAIgLABQgHACgIAEQgIAEgKAHQgKAIgLALIgRgUQAOgNAMgIQAMgHAKgFQALgEAJgCQAIgCAIAAQAMAAAKAFQAKAEAHAIQAIAHAFAKQAFAKADAMQAEALABANQACALAAANIgCAcQgBAPgEASgAgGAAQgLACgJAGQgIAFgEAIQgEAIACAKQACAIAGAEQAFADAIAAQAIAAAJgDIASgGIARgJIANgIIABgNIgBgOIgOgCQgHgCgIAAQgMAAgLADg");
	this.shape_7.setTransform(48.2,50.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("ABCAhIAAgeIgBgVQAAgMgDgFQgEgGgFAAQgDAAgEACIgHAGIgHAHIgGAJIgGAIIgFAIIABAMIAAARIABAUIAAAaIgaABIAAgqIgBgeIgCgVQAAgMgDgFQgEgGgFAAQgDAAgEADIgIAGIgHAIIgHAKIgGAJIgFAHIABBIIgbABIgEiSIAdgDIAAAoIAKgMQAEgGAGgEQAGgFAHgDQAGgDAIAAQAGAAAFACQAGABADAEQAFAEADAGQADAGAAAIIAJgLIALgKQAFgFAHgCQAGgDAIAAQAGAAAGACQAGACAEAEQAEAEADAHQADAHAAAJIACAaIABAgIABAwIgdABIAAgqg");
	this.shape_8.setTransform(28.5,50.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgQBnIAAgjIABgrIAAg6IAcgBIgBAjIAAAeIgBAYIAAASIgBAegAgGhAIgHgFIgEgGQgBgEAAgDQAAgEABgEIAEgHIAHgDQADgCADAAQAEAAAEACIAGADIAEAHQABAEAAAEQAAADgBAEIgEAGIgGAFQgEABgEAAQgDAAgDgBg");
	this.shape_9.setTransform(13.8,47.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgeBKQgOgGgKgKQgJgKgFgOQgFgOAAgSQAAgQAGgOQAFgPALgLQAKgLANgHQAOgGAPAAQAOAAANAGQANAFAJAKQAKAKAGANQAHANABAQIh2ARQABAJAEAIQAEAIAGAFQAGAFAIADQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgEAMgHALQgHAKgJAHQgJAHgMAEQgLAEgMAAQgRAAgOgFgAgLg0QgGACgHAEQgGAFgFAIQgGAIgCAMIBSgKIgBgCQgFgOgJgIQgJgHgOAAQgFAAgHACg");
	this.shape_10.setTransform(-5.4,50.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAlBoQAEgNACgNIACgWIAAgVQAAgQgFgJQgEgKgGgGQgHgHgHgDQgIgDgIAAQgGABgIADQgHAEgIAHQgIAGgHAMIAABbIgbABIgDjTIAgAAIgBBSQAIgHAIgFQAJgFAHgCIAQgEQAPAAANAFQANAGAJAKQAJALAFAOQAFAOABASIAAAVIgBATIgCATIgEAQg");
	this.shape_11.setTransform(-22.9,47.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgQgPIgwABIABgZIAvgBIACg9IAagBIgBA9IA1gCIgCAZIgzACIgBB3IgcABg");
	this.shape_12.setTransform(-39,48);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAlBoQAEgNACgNIACgWIAAgVQAAgQgFgJQgEgKgGgGQgHgHgHgDQgIgDgIAAQgGABgIADQgHAEgIAHQgIAGgHAMIAABbIgbABIgDjTIAgAAIgBBSQAIgHAIgFQAJgFAHgCIAQgEQAPAAANAFQANAGAJAKQAJALAFAOQAFAOABASIAAAVIgBATIgCATIgEAQg");
	this.shape_13.setTransform(-62,47.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgYBJQgPgFgLgLQgMgKgGgQQgHgOAAgRQAAgJADgKQADgKAFgJQAFgJAHgHQAIgIAIgGQAJgFALgDQAKgDALAAQAMAAAKADQALACAJAGQAJAFAIAJQAHAHAFAKIAAABIgXANIAAgBQgDgGgFgGIgMgJQgGgFgHgCQgIgBgHAAQgLAAgJAEQgKAEgIAIQgHAHgEAKQgEAKAAAKQAAALAEAKQAEAKAHAHQAIAIAKAEQAJAEALAAIAOgBQAHgCAGgEQAGgEAFgFQAFgEAEgHIAAAAIAYANIAAABQgGAJgHAHQgIAHgJAFQgKAGgKACQgLADgKAAQgPAAgOgHg");
	this.shape_14.setTransform(-79.9,50.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgQgPIgwABIABgZIAvgBIACg9IAagBIgBA9IA1gCIgCAZIgzACIgBB3IgcABg");
	this.shape_15.setTransform(-95.1,48);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AArBOIABgTQgQALgQAFQgQAEgOAAQgJAAgKgCQgJgDgHgFQgHgFgEgIQgFgJAAgLQAAgLAEgJQADgIAGgHQAFgGAIgFQAIgEAJgDQAJgEAKgBIATgBIAQAAIAOACIgEgNQgDgHgEgFQgEgGgGgDQgGgDgIAAIgLABQgHACgIAEQgIAEgKAHQgKAIgLALIgRgUQAOgNAMgIQAMgHAKgFQALgEAJgCQAIgCAIAAQAMAAAKAFQAKAEAHAIQAIAHAFAKQAFAKADAMQAEALABANQACALAAANIgCAcQgBAPgEASgAgGAAQgLACgJAGQgIAFgEAIQgEAIACAKQACAIAGAEQAFADAIAAQAIAAAJgDIASgGIARgJIANgIIABgNIgBgOIgOgCQgHgCgIAAQgMAAgLADg");
	this.shape_16.setTransform(-111,50.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("ABAhCIgtCUIghAEIgziQIgZCdIgdgEIAmjHIAcAAIA3CdIAxicIAgAAIAlDLIgcAFg");
	this.shape_17.setTransform(-132.4,48);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#660000").ss(3,1,1).p("Ayci4MAk5AAAQA8AAAqAqQArArAAA8IAABOQAAA9grAqQgqArg8AAMgk5AAAQg8AAgqgrQgrgqAAg9IAAhOQAAg8ArgrQAqgqA8AAg");
	this.shape_18.setTransform(21.9,47,1.441,1.278);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0033").s().p("AycC5Qg8gBgqgqQgrgqAAg9IAAhOQAAg8ArgrQAqgpA8AAMAk5AAAQA8AAAqApQArArAAA8IAABOQAAA9grAqQgqAqg8ABg");
	this.shape_19.setTransform(21.9,47,1.441,1.278);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-170.6,22,385,50.1), null);


(lib.questiontext_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBRQgQgHgKgKQgLgMgFgPQgGgQAAgTQAAgRAHgRQAGgQALgMQALgMAPgHQAQgHAQAAQAQAAAOAGQANAGALALQALALAHAPQAHAOACARIiCASQABALAEAIQAEAJAHAGQAHAGAIADQAJADAJAAQAIAAAIgDQAIgCAGgFQAHgEAFgHQAFgIACgJIAdAGQgEANgIAMQgHALgLAIQgKAIgMAEQgNAFgNAAQgTAAgPgGgAgMg6QgHACgHAFQgIAGgFAJQgGAIgCANIBagKIgBgDQgGgPgKgJQgKgIgPAAQgFAAgIACg");
	this.shape.setTransform(160.4,34.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AggB+QgHgCgHgDQgIgDgGgFQgHgFgFgHQgGgHgEgJIAegNQACAHAEAEIAJAIIAKAFIAJADQALADAMgCQAKgCAIgEQAIgFAFgFQAGgFADgGIAFgMIADgKIABgHIAAgTQgJAIgKAFIgSAHQgKACgIAAQgSAAgPgGQgPgGgLgLQgLgMgHgPQgGgQAAgUQAAgVAHgQQAHgQAMgLQALgLAPgGQAOgGAPAAQAJABALAEIATAIQAKAFAJAJIAAgeIAeABIgBCsQAAAKgDAKQgCAKgFAJQgFAJgGAIQgIAIgJAGQgJAHgLAEQgLAEgOABIgCAAQgRAAgOgEgAgWhbQgJAEgHAIQgGAIgEALQgEALAAAMQAAAMAEALQAEAKAGAHQAHAHAJAEQAKAEALAAQAJAAAJgDQAJgDAIgHQAIgFAFgJQAFgIACgLIAAgTQgBgLgGgJQgFgJgIgGQgIgGgJgEQgKgDgJAAQgLAAgJAEg");
	this.shape_1.setTransform(140.4,38);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAvBWIACgWQgTAMgRAGQgRAFgPAAQgLAAgKgDQgKgCgIgHQgIgFgFgJQgEgJAAgNQgBgMAEgJQADgKAHgHQAHgGAIgGQAIgFALgDQAKgEAKgCQAKgBALAAIATABIAOACIgEgPQgDgHgFgGQgEgGgGgEQgHgDgJAAQgGAAgGACQgIABgJAFIgUAMQgLAIgMAMIgSgVQAPgPANgIQANgJAMgEQALgGAKgBQAKgCAHAAQAPAAAKAFQALAFAIAIQAJAIAFALQAGALAEANQADANABAOQACAMAAAOIgCAfQgBAQgDAUgAgHAAQgMACgJAGQgKAGgEAJQgFAJADALQACAJAGADQAHAEAIAAQAJAAAKgDIATgHIATgJIAPgJIABgPIgBgPIgQgDIgQgBQgOAAgMADg");
	this.shape_2.setTransform(120.8,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ABIAkIAAghIgBgXQAAgNgDgGQgEgGgFAAQgFAAgDACIgIAGIgIAJIgHAJIgGAKIgFAIIAAANIABASIAAAXIAAAcIgcACIAAgvIgBghIgCgXQAAgNgEgGQgEgGgGAAQgDAAgFADIgIAGIgIAKIgHAKIgHAKIgGAIIACBPIgeACIgEihIAfgEIABAsIAKgMIAMgMQAGgFAIgEQAHgDAJAAQAGAAAFACQAHACADAEQAFAEADAHQAEAGABAKIAKgMIALgMQAGgFAHgDQAHgDAJAAQAHAAAGACQAGACAFAFQAFAFADAHQAEAIgBAKIACAcIACAjIAAA1IggACIAAgvg");
	this.shape_3.setTransform(99.1,34);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgSBxIAAgmIABgwIABg/IAegCIAAAnIgBAhIgBAaIAAAVIAAAggAgHhHQgEgCgDgDQgDgDgCgDQgBgEAAgFQAAgEABgEQACgEADgDQADgDAEgCQADgBAEAAQAEAAAEABQAEACADADQADADACAEQABAEAAAEQAAAFgBAEQgCADgDADQgDADgEACQgEABgEAAQgEAAgDgBg");
	this.shape_4.setTransform(82.9,30.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AghBRQgQgHgKgKQgLgMgFgPQgGgQAAgTQAAgRAHgRQAGgQALgMQALgMAPgHQAQgHAQAAQAQAAAOAGQANAGALALQALALAHAPQAHAOACARIiCASQABALAEAIQAEAJAHAGQAHAGAIADQAJADAJAAQAIAAAIgDQAIgCAGgFQAHgEAFgHQAFgIACgJIAdAGQgEANgIAMQgHALgLAIQgKAIgMAEQgNAFgNAAQgTAAgPgGgAgMg6QgHACgHAFQgIAGgFAJQgGAIgCANIBagKIgBgDQgGgPgKgJQgKgIgPAAQgFAAgIACg");
	this.shape_5.setTransform(61.7,34.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAoBzQAFgPACgOIACgYIABgYQgBgQgFgMQgFgKgHgHQgHgHgIgEQgJgDgIAAQgHABgJAEQgIAEgIAHQgJAHgHAOIgBBkIgeAAIgCjnIAjgBIgBBbQAIgJAJgFQAKgFAIgDQAJgDAIgBQARAAAOAGQAOAGAKAMQAKALAGAQQAFAPABAVIAAAVIgBAWIgCAVIgEARg");
	this.shape_6.setTransform(42.4,30.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgSgRIg0ABIABgbIA0gCIABhCIAdgCIgBBDIA7gBIgDAcIg4ABIgBCDIgfABg");
	this.shape_7.setTransform(24.8,31.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AghBRQgQgHgKgKQgLgMgFgPQgGgQAAgTQAAgRAHgRQAGgQALgMQALgMAPgHQAQgHAQAAQAQAAAOAGQANAGALALQALALAHAPQAHAOACARIiCASQABALAEAIQAEAJAHAGQAHAGAIADQAJADAJAAQAIAAAIgDQAIgCAGgFQAHgEAFgHQAFgIACgJIAdAGQgEANgIAMQgHALgLAIQgKAIgMAEQgNAFgNAAQgTAAgPgGgAgMg6QgHACgHAFQgIAGgFAJQgGAIgCANIBagKIgBgDQgGgPgKgJQgKgIgPAAQgFAAgIACg");
	this.shape_8.setTransform(0.4,34.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgYBRIg1iYIAggEIAtCAIAviFIAeADIg7Ceg");
	this.shape_9.setTransform(-17.3,34.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhEAmIgBgmIAAgcIgBgXIgBgfIAigBIABATIAAARIAAATIALgQQAGgIAHgIQAJgHAIgFQAJgGALgBQAMgBAKAGIAJAGQADAFAEAFQADAHADAIQADAJABAMIgfALIgBgNIgDgJIgDgHIgFgEQgFgEgHAAQgEABgFADIgJAIIgJAKIgLAMIgIAMIgHAKIAAAWIABAVIAAARIABAMIghAEIgCgug");
	this.shape_10.setTransform(-33.5,34.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AghBRQgQgHgKgKQgLgMgFgPQgGgQAAgTQAAgRAHgRQAGgQALgMQALgMAPgHQAQgHAQAAQAQAAAOAGQANAGALALQALALAHAPQAHAOACARIiCASQABALAEAIQAEAJAHAGQAHAGAIADQAJADAJAAQAIAAAIgDQAIgCAGgFQAHgEAFgHQAFgIACgJIAdAGQgEANgIAMQgHALgLAIQgKAIgMAEQgNAFgNAAQgTAAgPgGgAgMg6QgHACgHAFQgIAGgFAJQgGAIgCANIBagKIgBgDQgGgPgKgJQgKgIgPAAQgFAAgIACg");
	this.shape_11.setTransform(-51.8,34.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgVBVIgNgDIgNgEIgOgHQgHgDgHgFIAPgXIARAIIASAHIASAFQAJACAKAAQAIAAAGgCIAJgDQAEgDACgDIADgFIABgGIgCgGIgEgGQgDgDgFgCQgFgCgHgCIgQgCQgNAAgMgDQgNgCgKgEQgKgFgGgHQgHgHgBgMQgBgLADgJQACgJAFgHQAFgHAHgFQAIgGAJgDQAIgEAKgBQAKgCAJAAIAOABIAQACQAJACAJADQAJAEAIAFIgLAcQgKgGgJgCIgQgFIgPgDQgXgBgNAGQgNAHAAANQAAAJAFAEQAFAEAJACQAIACAMAAIAXADQAPACAKAFQAKADAGAGQAGAGADAHQADAIAAAIQAAAOgGAKQgGAKgJAGQgKAGgNADQgMADgOAAQgNAAgOgDg");
	this.shape_12.setTransform(-69.8,34.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgNBzQgJgCgKgFQgKgEgKgJIAAAUIgcAAIgCjnIAhgBIgBBZQAJgJAJgEIASgHQAJgDAIAAQAKAAAKADQAKADAJAFQAJAGAIAHQAIAIAFAJQAGAJACAKQADALAAALQAAANgEAMQgDALgGAKQgGAJgHAIQgIAIgIAFQgJAFgJADQgJADgJAAQgIAAgKgDgAgRgTQgIADgHAFQgGAFgFAGQgFAHgDAIIAAAkQADAJAFAHQAFAHAGAFQAHAFAIADQAHACAIAAQAKABAKgFQAJgEAHgHQAHgIAFgKQAEgKABgMQAAgLgDgKQgEgLgHgHQgGgIgKgFQgKgEgLAAQgJAAgIADg");
	this.shape_13.setTransform(-87.7,31);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeBwQgOgDgNgIQgMgIgLgKQgKgMgIgNQgHgMgEgPQgEgPAAgQQAAgPAEgPQAEgPAHgNQAIgNAKgLQALgKAMgIQANgIAOgEQAPgEAPAAQAPAAAPAEQAOAEAOAIQAMAIAKAKQALALAHANQAIANAEAPQAEAPAAAPQAAAQgEAPQgEAPgIAMQgHANgLAMQgKAKgMAIQgOAIgOADQgPAFgPAAQgPAAgPgFgAgghPQgPAHgLAMQgMAMgGAPQgHAQAAARQAAASAHAPQAGARAMALQALAMAPAHQAQAGAQAAQAMAAALgCQAKgDAJgGQAKgGAHgIQAIgHAFgKQAGgKADgLQADgLAAgMQAAgLgDgLQgDgLgGgKQgFgJgIgIQgHgJgKgFQgJgFgKgEQgLgDgMAAQgQAAgQAHg");
	this.shape_14.setTransform(-110.8,31.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000066").ss(3,1,1).p("Azxi4MAniAAAQBBAAAtAqQAuArAAA8IAABPQAAA8guAqQgtArhBAAMgniAAAQhAAAgtgrQgugqAAg8IAAhPQAAg8AugrQAtgqBAAAg");
	this.shape_15.setTransform(22.7,31.1,1.188,1.188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#0066CC").s().p("AzwC4QhAAAgugqQgtgrAAg7IAAhPQAAg8AtgqQAugqBAgBMAnhAAAQBAABAuAqQAtAqAAA8IAABPQAAA7gtArQguAqhAAAg");
	this.shape_16.setTransform(22.7,31.1,1.188,1.188);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mc, new cjs.Rectangle(-147.5,6,340.5,49.1), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AlWIlQhVAAg8g8Qg9g9AAhWIAAqrQAAhWA9g9QA8g8BVAAIKsAAQBWAAA8A8QA9A9AABWIAAKrQAABWg9A9Qg8A8hWAAg");
	mask.setTransform(46.1,43.9);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AgyADQgBgCAAgCQgBgVAaAEQAxgFAbAhQAEAFgDAFQgvgbg2AKg");
	this.shape.setTransform(58.1,41.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgyAKQAcghAxAFQAagDgBAUIgBAEQg2gLgvAcQgCgFACgFg");
	this.shape_1.setTransform(31.4,41.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgNAPQgUgPAGgTQALAfAdgKQAIgCABgIQABgGAEgFQAIAbgcALQgEABgDAAQgIAAgFgFg");
	this.shape_2.setTransform(43.8,55.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AgYAYQgJgJAAgPIABgJIAEAFQAFAEAHAAQAIAAAEgEQAEgFAAgHQAAgHgEgGIgEgDQAEgBAEAAQAPAAAKAKQAJAKAAANQAAAPgJAJQgKAKgPAAQgNAAgLgKg");
	this.shape_3.setTransform(58.3,47.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AgfAgQgMgOAAgSQAAgSAMgNQAOgNARAAQATAAAMANQANANAAASQAAASgNAOQgMANgTgBQgRABgOgNgAgjAJQAAAOAJAKQALAJAOABQANgBALgJQAJgKgBgOQABgOgJgKQgLgKgNAAQgFAAgEABIAEAEQAFAFAAAHQAAAHgFAEQgFAFgHAAQgHAAgGgFIgDgEIgBAKg");
	this.shape_4.setTransform(58.5,46.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AgYAYQgJgJAAgPQAAgEABgFQABACADADQAGAEAGAAQAHAAAEgEQAFgFAAgHQAAgHgFgGIgDgDIAIgBQAPAAAJAKQAKAKAAANQAAAPgKAJQgJAKgPAAQgOAAgKgKg");
	this.shape_5.setTransform(30.1,47.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AgfAgQgNgOAAgSQAAgSANgNQAOgNASAAQASAAAMANQAOANAAASQAAASgOAOQgMANgSgBQgSABgOgNgAgjAJQAAAOAJAKQAKAJAPABQANgBAJgJQALgKAAgOQAAgOgLgKQgJgKgNAAIgKABIAEAEQAGAFAAAHQAAAHgGAEQgFAFgGAAQgHAAgGgFQgCgCgBgCQgBAEAAAGg");
	this.shape_6.setTransform(30.3,46.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AhBAEQgSgIgGgMQBdAPBWgQQgGAMgTAJQgbAOgmAAQgnAAgagOg");
	this.shape_7.setTransform(44.2,61.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AhUAEQgWgJgJgOIAZgCQAGAMASAJQAaANAoAAQAlAAAbgNQATgKAGgMIAaAAQgIAPgWALQgkATgxAAQgxAAgjgTg");
	this.shape_8.setTransform(44.3,62.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(51,51,51,0.098)").ss(0.1,1,1).p("Akyg1QgEgcAAgfQAAhDAXg5QgOAXgSAhQABghALgeQAHgYANgWQAfg1AxgbQAVgMAYgHQAZgRAbgJQgdgFgcAGQAmgLAoACQAdABAeAJQACgMAKgKQAJgLAQgHQAGgDAHgCQAngOAigGQgdAGgaATQgLAHgKAKQBhAFBLA9QAbAPAcAYQApAhAVA2QATAyAEBDQgEgKgEgJQgEgOgJgLQAEAcAAAeQAAAOgCAOQAAAOgCANAjyBUQgKg2gEgeQgGgvABgpQAAgaAFgYQAYgFAegLQAjgKATgIQANgHANgIQgFAIgJAJQgbAigoAaQAogMAigXQAYgPAdgvQAKAYAGAMQAPAeARAUQAHAIAJAIQgPgegIgfQAgAcBAALQAKABALACQgLgNgKgQQgLgTgIgPQAPAJAWAEQAZAEAxAIQAkAJAcAbQAEAEAFAEQAVAYAMAdQAMAmgDA4QgCANgBARQAhg2AKg+QAGAHADANQACAGABAFAkyg1QgEADgEADQgJAGgCAJQgFAPAFAUQAIAfAcAXQATAPAXAKIAAABQACABADAAAhMDJQhRgVg+hAQgMgNgLgOQAAgCAAgDAj3BTQAAgBAAAAQgvg9gMhKAk7HeIAAhyQAAhMBOAKQAtAEAsACIAAA4IAzg1QAFAAAFAAIAxA2IACABIA6AAIABAAAhcEtIAFAGAhcEtIgFAGAhMEMIALAGIgbAbAhMEMIhIglIAABJAhMEMIAAhDAFCgQQACAYgKAVQgQAcgjAPQgFACgFACQgOAVgSATQhDBFhYASQgfAHgiAAQgoAAglgJABJExQAFgBAEAAABOEtIgFAEIgyA5IgBAAABOEtIAEADAAXESIAdAAIAaAbAgmESIA9AAIAABYIAAAAIAAAIIAYBsAgmFpIAAABIACAAAgmFqIAAAIIgXBsACGEuIAAA6Ig0g4ABCEKIgOAIAgmESIAABXACGEuQAtgEAsgFQBNgFAABMIAAByABCEKIBEgjIAABHABCDLIAAA/AhBESIAbAA");
	this.shape_9.setTransform(44.4,50.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#07BABB").s().p("Ag1A6IAXhrIAAgIIACAAIA6AAIAAAAIAAAIIAYBrg");
	this.shape_10.setTransform(43.6,92.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#10DADB").s().p("AgcAsIgCgBIAAhWIA9AAIAABXIgBAAg");
	this.shape_11.setTransform(43.6,82.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#13B0B1").s().p("AAegrIAeAAIAZAbIgEAEIgzA4gAhPgKIgGgGIAbgbIAcAAIAABWg");
	this.shape_12.setTransform(43.6,82.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#129293").s().p("ABZAJIgEgEIgagaIAPgIIBDgjIAABGIAAA7gAiMAJIAAhJIBHAmIALAFIgbAaIgFAGIgyA2g");
	this.shape_13.setTransform(43.6,80.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#01627F").s().p("AA2BcIgYhrIAAgHIAAgBIAzg4IAJgBIA0A4IAAg7IBZgIQBNgGAABNIAABwgAkzBcIAAhwQAAhNBOAKQAtAFAsACIAAA4IAzg2IAKAAIAxA3IAAABIAAAHIgXBrg");
	this.shape_14.setTransform(43.6,89.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AAaDxIg9AAIgbAAIgLgGIAAhDQAlAJAoAAQAiAAAfgHIAAA/IgOAIgAhJCoQhRgVg+hAIgXgbIAAgFIgOhTQgGgwABgpQAAgaAFgYQAYgFAegLQAjgKATgIIAagPQgFAIgJAJQgbAigoAaQAogMAigXQAYgPAdgvQAKAYAGAMQAPAeARAUIAQAQQgPgegIgfQAgAcBAALIAVADQgLgNgKgQIgTgiQAPAJAWAEIBKAMQAkAJAcAbIAJAIQAVAYAMAdQAMAmgDA5IgDAdQAhg1AKg/QAGAHADANIADALIACAKQACAZgKAVQgQAbgjAPIgKAEQgOAVgSATQhDBFhYASQgfAHgiAAQgoAAglgJgAj0AxQgXgKgTgPQgcgXgIgeQgFgVAFgPQACgJAJgGIAIgGQAMBLAvA8IAAABg");
	this.shape_15.setTransform(44.1,54);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AkBDAIgBgBQgCgYgIgbIgVhFQgLgnABggQACgnAUgaQALgNATgMIAjgVQAZgMAKgIIAGgDQAigZAHgdQAHAbgBAbQAAARgFASQAMgRAIgSQAMgbADgfQAJAdAGAdQAHAhAEAgIgBhBIAAgDIABADQArA5A6AmQgFgZgNgXQgNgWgSgTIDZA2QAaAGAOAHQAVAIAOAOQAKAMAGAUQAEAKAEAVIgDAbQgKA/ggA2IACgeQAEg6gNglQgLgegWgWIgJgIQgcgbgjgJIhKgNQgXgDgPgJIATAiQAKAQALANIgVgDQhAgLgggcQAIAfAPAcIgPgPQgSgUgPgdQgGgMgJgYQgeAvgXAPQgjAXgoALQAogZAcgiQAIgJAFgIIgaAPQgTAIgjAKQgdAKgYAGQgFAYgBAZQgBApAGAwIAOBTIgEAAg");
	this.shape_16.setTransform(45.4,39.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("Ak2CQQgEgcgBgfQABhDAXg4IghA3QABggAMgeQAHgYANgWQAfg1AxgbQAVgMAYgHQAZgRAagJQgcgFgdAGQAngLAoACQAcABAeAJQADgMAKgKQAJgLAQgHQAGgDAHgCQAngOAhgGQgcAGgbATQgKAHgKAKQBgAFBMA9QAaAPAdAYQAoAhAWA2QASAxAEBDIgHgTQgEgOgJgLQAEAcAAAeQAAAOgCAOQgDgUgFgMQgFgUgLgMQgOgNgUgJQgOgGgbgHIjZg0QASASANAVQANAYAGAZQg7gngrg4IgBgCIAAACIABBBQgEgggHghQgGgcgJgdQgDAegMAbQgIASgLARQAEgSAAgRQABgbgHgbQgHAdghAZIgHADQgKAIgZANIgjAUQgTALgLAOQgUAagBAnQgCAgALAnIAVBFQAIAcACAXQgug9gMhLg");
	this.shape_17.setTransform(44.8,30.9);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_6
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape_18.setTransform(46.1,43.9,0.459,0.459);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_19.setTransform(46.1,43.9,0.459,0.459);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#003300").s().p("AlWJeQhsABhOhOQhOhOABhsIAAqsQgBhuBOhNQBOhOBsAAIKsAAQBuAABNBOQBOBNAABuIAAKsQAABshOBOQhNBOhugBg");
	this.shape_20.setTransform(46.1,44);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]}).wait(1));

	// Layer_5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(255,255,255,0.02)").s().p("Al8KYQh0AAhShQQhShQAAhxIAAsNQAAhxBShQQBShQB0AAIL5AAQB0AABSBQQBSBQAABxIAAMNQAABxhSBQQhSBQh0AAg");
	this.shape_21.setTransform(46.2,44.2,1.056,1.056);

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy13, new cjs.Rectangle(-23.6,-26,139.6,140.4), null);


(lib.Symbol1copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ak6IlQhgAAhDhCQhEhCAAheIAAqFQAAheBEhCQBDhCBgAAIJ1AAQBgAABDBCQBEBCAABeIAAKFQAABehEBCQhDBChgAAg");
	mask.setTransform(46.1,44);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#622F0D").s().p("AGBAJQh7g5hhArIAvgZQCAgvBVBdIAgAfgAmoAQQBVhdCAAvIAvAZQhhgrh7A5IhIAmIAggfg");
	this.shape.setTransform(45.5,33.7,0.342,0.342);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#0D0D0E").ss(0.3,1,1).p("AAuALQAFAOgKABQgIACgJABQgFAAgEABQgQAAgXAAQgBAAgBAAQgYAAADgRQACgQANgNQAOgNARAAQARAAANANQANANAEAOg");
	this.shape_1.setTransform(57.6,39.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000C00").s().p("AgXAYQgFgIAAgKIAAgDQADAEAFAAQAGAAADgEQAEgDAAgFQAAgGgEgEIgEgDQAHgFAIAAQAMAAAJAJQAIAIAAAMQAAAKgEAHIgJABIgnAAg");
	this.shape_2.setTransform(57.5,40);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F0ECDF").s().p("AgaAeQgYAAADgRQACgQANgNQAOgNARAAQARAAANANQANANAEAOQAFAOgKABIgRADQAEgHAAgKQAAgMgIgJQgJgIgLAAQgJAAgHAFIAEADQAEAEAAAFQAAAFgEAEQgDAEgGAAQgFAAgDgEIAAADQAAAKAFAIIgCAAg");
	this.shape_3.setTransform(57.6,39.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#0D0D0E").ss(0.3,1,1).p("AAuALQAFAOgKABQgIACgJABQgFAAgDABQgQAAgYAAQAAAAgBAAQgZAAADgRQADgQAMgNQAOgNASAAQAQAAAOANQAMANAEAOg");
	this.shape_4.setTransform(34.8,39.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000C00").s().p("AgXAYQgGgIAAgKIAAgDQAEAEAGAAQAFAAAEgEQAEgDAAgFQAAgGgEgEIgFgDQAHgFAIAAQANAAAIAJQAJAIAAAMQAAAKgFAHIgIABIgoAAg");
	this.shape_5.setTransform(34.7,40);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F0ECDF").s().p("AgZAeQgZAAADgRQADgQAMgNQAOgNASAAQAQAAAOANQAMANAEAOQAFAOgKABIgRADQAFgHAAgKQAAgMgJgJQgIgIgMAAQgJAAgHAFIAFADQAEAEAAAFQAAAFgEAEQgEAEgFAAQgGAAgEgEIAAADQAAAKAGAIIgBAAg");
	this.shape_6.setTransform(34.8,39.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C33E41").s().p("AAAARQgYAAgTgKQACgIAKgHQANgIASAAQASAAANAIQALAHACAIQgNAGgPADIgLABIgFAAg");
	this.shape_7.setTransform(46.1,61.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E5CAC7").s().p("AgmACQgIgEgFgHIBnAAQgEAHgKAEQgPAIgXAAQgVAAgRgIg");
	this.shape_8.setTransform(46.1,57.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#4D0614").s().p("AAfAJQgNgIgSABQgSgBgNAIQgLAHgBAJIgNgJQgUgRgDgXIAbAAQAFAHAJAFQAQAJAWAAQAWAAAPgJQAKgFAEgHIAcAAQgCAXgVARQgFAFgIAEQgBgJgLgHg");
	this.shape_9.setTransform(46.2,59.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("rgba(28,14,4,0.098)").ss(0.3,1,1).p("AjCD4QgHgDgHgFQgMgJgHgKQgLgPAAgQQAAgBAAgBQAAgFABgEQgIACgIAAQgYAAgSgSQgIgHgEgKQgBgBAAgCQgFgLAAgMQAAgWAOgPIAAgBQgLgPAAgUQAAgSAIgNQABgCACgCQAAgCACgBQgNgGgKgKQgXgWAAggQAAgOAFgMQgTgFgNgQQgVgXAAggQAAghAVgWQAFgGAGgEQgNgRAAgVQAAgOAGgMQAFgMAMgLQANgKAQgFQgSgTAAgaQAAgbATgTQATgTAbAAQATAAAOAJQgHgQAAgTQAAgmAfgbQARgOAVgHQARgFAUAAQACAAADAAQAnABAcAXQAAgBAAgBQAAgSARgNQACgCADgBQABgBABAAQAPgJAUAAQANAAAMAFQAGACAGADQADgDAEgEQACgBACgCQANgJASgCQADAAADAAQAXAAAQANQABAAABABQAJAIAFAJQADgDACgDQAQgPAWAAQAZAAASAVQATAVAAAfQAAAPgGAOQAIgBAKAAQAkAAAZAVQAaAWAAAeQAAAYgPATQAeABAVAVQAVAVABAcQAAACAAACQAAAggWAVQgCADgDACQgHAGgJAFQAHAEAHAGQAEAEACAEQAXAbAAAnQAAAngXAbQgOASgTAHQAFANAAAQQAAAegWAVQAPAaAAAkQAAApgUAdQgKAOgLAIQgDABgDACQgGAfgoAXQgDACgDACAg9BrQgCgBgCAAQgvgUglgsQgLgMgJgOQgigxgLhEQgfgBgWgWQgXgWAAggQgCgcAQgVQADgDACgEQATgTATAKQABAAAAABQADABACACQACAAACAAQABAAABAAIABABQAEADADACQAEADAFACQADABAEABQALACALgEQALgDAEgKQAFgJgCgOQgDgQgCgIQAAgFAAgFQACgIAEgGQACgCAGgEQAGgEACgCQAGgFAEgJQAAgBAAgCQAEgMADgGQAEgKAHgGQABAAABAAQAEALAJAIQACACACADQARANAVAAQAFAAAGgBQASgDANgOQADgCACgEQAEAGAFAFQABABABAAQAPAMAWAAQAWAAAQgNIABAAQACgCACgCQAFgFACgEQALAFANAAQAEAAADgBQAKAAAIgFQAQANAMASQAQAaABAdQAAABABAOQAAAKACAFQAAABABACIAAABQABACAFAFQAEAIAPACQACAAADABQAKABAFACQAFABABgHQACgHAAgEQAMgEAGAAQASAAAOAPQABACABACQAOAUABAaQAAACAAABQAAAdgPATQgOAVgUAAQgEAAgEgBQgNBOgzA/QgrA1g1AVQgZALgcACQgLAAgLAAQgCAAgEAAQgKAAgKAAQgFAAgEgBQgCAAgCgBQgUgDgSgHgAisDpQA4gmAvgBQAGgPAEgQQADgPgCgVQAAgEgBgEQgBgEgBgFQAAgBAAgCAjCD4QALgIALgHAksIEQAQhgAEgXQAMhTA7g1QAGgFAGgFQACgCABgBADGDhQgJARgXAOQgJAEgKAFQg5AZhQAAQgSAAgSgBQg4gFgsgTQgggPgOgRABbBqQgBASADAYQABAKACALQABANADALQAhAEAlAPQANAFANAIQACAAAAAAQAGAEAFADQALAIALAJQACAAACACQBLA9AHBzQAAAIAAAKIhGCS");
	this.shape_10.setTransform(44.5,59.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#5187BD").s().p("AjWDPQA3jvAOgDQBKgPArgTQA8gaAmgxQAQgUAIgUQAMgdAAgdQALAIALAJIAEACQBLA9AHBzIABASIhHCRQhtB/h2AAQhAAAhDgkg");
	this.shape_11.setTransform(54.8,106.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#6699CC").s().p("Aj+A+IAUh1QAMhUA7g0IAMgLIADgCIAWgPQAOARAgAPQAsATA3AFIAlABQBQAAA5gZIATgKQAXgOAJgQIALAHQAAAdgMAcQgJAVgQAUQgmAwg8AaQgrAThLAPQgMAEg3Dug");
	this.shape_12.setTransform(39.9,104.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DEA54F").s().p("ABGATQgfgDglgHIgdgGIgUgEIgHgCIgIgEIgCgBQgHgDgFgGIgBgCQATAGAUAEIAEABIAJABIAVgBIAFAAIAVABQAcgCAZgLQAAASADAXIgIgCg");
	this.shape_13.setTransform(46,71.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E7B363").s().p("AAFArIgJgCQgHgCgDgDQgGgEgBgJQgBgGACgKIADgRQACgKAEgIQADgHAHgEQAGgFAHAAQAGABADADQACAEgCAEQgCADgDADIgMAJQgDAFAAAHQABAFAFAFIAFADIADAEQACADAAAHQAAARgHAGIgFgCg");
	this.shape_14.setTransform(19.4,40.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#DBA95C").s().p("AgOArIgDgFQgBgDACgDIAIgHQADgDAAgJIABgTIgBgFIgFgCQgEgBgBgBIgBgBIACAAQACgBACgDQACgDgBgGIgDgKQAAgBABgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAAAQAAgBABAAQAAAAABAAQAAAAAAAAIADgBIALABIAFAEQACADgBACQAAADgCAEIgEAGQgBAEACAEIAFAHQAGAIgBAJQgBAGgCACIgEADIgGACQgFADAAAFIACAFIACAGQAAACgDACQgCACgDAAIgBAAQgGAAgDgEg");
	this.shape_15.setTransform(71,41.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F0BD70").s().p("AAPE3Qg3gFgsgTQgggPgOgRQA4gnAvAAIAKgfQADgPgCgWIgBgIIgCgJQAFAFAHAEIACABIAHADIAIADIATAFIAfAGQAlAHAfADIAHACIADAUIAEAZQAhADAlAPQANAGANAHIACABQgJAQgXAOIgTAKQg5AZhQAAIglgBgAASCLQgTgEgSgGIgEgCQgvgUglgrQgLgNgJgNQgigygLhEQgfAAgWgWQgXgXAAggQgCgcAQgUIAFgHQATgTATAJIABABIAFADIAEABIACAAIABABIAHAEIAJAFIAHACQALADALgEQALgEAEgJQAFgJgCgOIgFgYIAAgLQAKgGAKADIAJACIAIABQAHAAAIgJQAIgKAFgCQAGgDAJACIAPAFQAIAEAHAAQAJgBADgGIADgHQACgEADgBQADgDAFACQAEABADAEIAGAGQADAEAEABQAHADARgIQAPgIAHAEQAEACADAHIAEALQAGAKANAGIAJADIAFAIQALAUATAKQAJAFASAHIAcAJQAPAHAKAKQALANABANQABANgKARIgQAeQgFANABAYIAAAnQgCAmgZAjQgXAfglAWQgeASgsAMIgmAKIgEgBgAjSjJQgHAEgDAIQgEAHgCALIgDARQgCALABAGQABAJAGAEQADADAHABIAKACIAFACQAHgFAAgRQAAgHgCgEIgDgDIgFgEQgFgEgBgHQAAgGADgGIAMgJQADgCACgEQACgEgCgDQgDgEgGAAQgHAAgHAEg");
	this.shape_16.setTransform(40.3,56.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#ECB563").s().p("Ag3D3IgGAAIgVABIgJgBIAmgKQArgMAdgSQAlgWAXgfQAagjACgnIgBgnQgBgYAGgNIAQgdQAJgRgBgNQAAgNgLgNQgKgKgPgHIgdgJQgRgHgKgFQgSgKgMgUIgEgIIgJgDQgNgGgFgKIgFgLQgDgHgDgCQgIgEgPAIQgQAIgIgDQgDgBgDgEIgHgGQgCgEgEgBQgFgCgEADQgCABgCAEIgEAHQgEAGgIABQgIAAgIgEIgOgFQgJgCgGADQgFACgJAKQgHAJgHAAIgJgBIgIgCQgLgDgJAGQABgIAFgFIAHgGIAJgHQAFgEAEgJIABgDIAGgTQAEgJAIgGIABgBQAEALAJAJIAFAEQAQAOAVAAIALgCQASgCANgOQAEgDACgDQAEAGAGAFIABAAQAPAMAWAAQAXAAAOgMIABgBIAEgDQAFgFADgFQAKAFAOAAIAHAAQAJgBAIgFQARANALATQAQAaACAdIABAPQAAAJABAGIACACIAAABIAFAIQAFAHAOADIAGABIAOACQAGACABgIIACgLQAMgEAGAAQASAAANAQIADAEQAOATAAAaIAAADQAAAdgOAUQgOAUgUAAIgIgBQgNBOg0A/QgrA1g1AWQgYAKgcADIgVgBgACwhAQACACAEAAIAFACIAAAFIAAAUQgBAJgDADIgIAHQgBADAAADIADAFQAEAFAGgBQAEAAADgBQACgCAAgDIgCgFIgCgGQAAgFAGgCIAFgCIAEgEQACgCABgFQABgKgGgJIgFgHQgCgEABgDIAEgGQADgEAAgDQABgDgCgDIgFgDIgNgBIgCAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAAAAAABQgBAAAAABQAAAAgBABQAAAAAAABQAAABAAAAIADALQAAAGgCADQgBACgDABIgBAAIAAABg");
	this.shape_17.setTransform(51.7,46.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#4F2408").s().p("AhQF/IgEgZIgCgUQgDgYAAgSQA1gWAqg1QA0g/ANhOIAIABQAUAAAOgVQAOgUAAgdIAAgDQAAgZgOgTIgDgEQgNgQgSAAQgGAAgMAEIgCALQgBAIgGgCIgOgCIgGgBQgOgDgFgHIgFgIIAAgBIgCgCQgBgGAAgJIgBgPQgCgdgPgaQgLgTgRgNQgIAFgJABIgHgpQgNg6gggkIgYgYQgOgOgIgMQgIgOAAgQQAGACAFAEIAIgHIADgDQAOgKASgBQgCAIABAIQABAUAIAPQAKARAQAHQAJAEAWAGQAQAHAFAMQACAFABALQAAALADAEQAFALAMACQAHACARAAIAaAAQAOABAIAHQAMALgBAhQgBAhALALQAFAGANAEQAPAFAEAEQATANgCAjIgDAeQgCASAAALQABAPAKAZQAKAbACANQAHAqghA5QgUAfgIAQQgRAcgHAVQgDALgHAhQgGAcgHAPQgVArg0AYQglgPghgDg");
	this.shape_18.setTransform(62.5,40.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#622F0D").s().p("ADnGjQgLgKgLgHIgLgHIgCgBQgNgHgNgFQA1gYAVgsQAGgPAHgcQAGghAEgKQAHgWAQgcQAJgPATggQAig5gHgpQgCgOgLgaQgJgZgCgPQAAgMADgSIACgeQACgjgSgNQgFgEgOgFQgNgEgGgFQgLgLACgiQAAghgLgLQgIgHgOgBIgbAAQgRABgHgDQgNgCgFgKQgCgFgBgLQgBgLgCgFQgFgLgQgHQgVgGgJgEQgQgIgKgRQgJgPgBgUQAAgHABgJIAGAAQAXAAAQANIACABQAJAIAFAJIAFgGQAQgPAWAAQAZAAASAVQATAVAAAfQAAAPgGAOIASgBQAkAAAZAUQAaAXAAAeQAAAYgPATQAeABAVAUQAVAWABAcIAAAEQAAAggWAVIgFAEQgHAHgJAFQAHAEAHAGIAGAIQAXAbAAAmQAAAngXAbQgOASgTAGQAFAOAAAQQAAAegWAWQAPAaAAAjQAAApgUAeQgKAOgLAHIgGAEQgGAfgoAXIgGAEIgEgCgAjQGaQgMgKgHgJQgLgPAAgRIAAgCIABgJQgIADgIAAQgYAAgSgSQgIgHgEgKIgBgDQgFgLAAgMQAAgWAOgQIAAgBQgLgOAAgVQAAgRAIgNIADgEIACgEQgNgFgKgLQgXgXAAgfQAAgOAFgNQgTgEgNgQQgVgXAAggQAAggAVgWIALgKQgNgRAAgVQAAgOAGgMQAFgNAMgKQANgKAQgGQgSgSAAgbQAAgaATgUQATgSAbgBQATAAAOAKQgHgRAAgSQAAgmAfgbQARgPAVgGQARgFAUgBIAFAAQAnACAcAXIAAgCQAAgTARgNIAFgCIACgBQAPgJAUAAQANAAAMAEQABAQAIAPQAHALAPAOIAYAYQAgAkANA7IAGAoIgHAAQgNAAgLgEQgCAEgFAFIgEAEIgBAAQgQAMgWAAQgWAAgPgMIgCAAQgFgFgEgGQgCAEgDACQgNAOgSACIgLACQgVAAgRgNIgEgFQgJgJgEgLIgCABQgHAGgEAJIgHATIAAADQgEAJgGAEIgIAHIgIAGQgEAFgCAJIAAAKIAFAYQACAOgFAJQgEAJgLAEQgLAEgLgCIgHgCIgJgGIgHgEIgBgBIgCAAIgEgBIgFgDIgBgBQgTgJgTATIgFAHQgQAVACAbQAAAgAXAWQAWAWAfABQALBEAiAyQAJANALANQAlAsAvAUIAEABIAAACIACAJIABAIQACAWgDAPIgKAfQgvAAg4AnIgWAPIgOgIg");
	this.shape_19.setTransform(44.5,42.1);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(255,255,255,0.02)").s().p("Ak7IlQhfAAhEhCQhDhCAAheIAAqGQAAhdBDhCQBEhCBfAAIJ2AAQBgAABDBCQBEBCAABdIAAKGQAABehEBCQhDBChgAAg");
	this.shape_20.setTransform(46.2,44.1);

	var maskedShapeInstanceList = [this.shape_20];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_20).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy8, new cjs.Rectangle(-8.4,-10.8,109.2,109.8), null);


(lib.Symbol1copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ak6IlQhgAAhDhCQhEhCAAheIAAqFQAAheBEhCQBDhCBgAAIJ1AAQBgAABDBCQBEBCAABeIAAKFQAABehEBCQhDBChgAAg");
	mask.setTransform(46.1,44);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("Ag4ADIgBgEQgBgXAcAEQA3gGAfAmQAEAFgDAFQg1geg8ALg");
	this.shape.setTransform(59.9,39.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("Ag4AMQAfgmA3AFQAdgDgBAXQAAACgCACQg8gMg0AfQgDgGADgEg");
	this.shape_1.setTransform(30,39.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,102,0,0.6)").s().p("AgPARQgVgRAGgWQALAjAigLQAIgCACgJQABgHAFgFQAIAegeAMQgGACgEAAQgIAAgGgGg");
	this.shape_2.setTransform(43.9,55.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#060000").s().p("AgbAbQgKgLAAgQQAAgGACgFQAAADADACQAHAGAHAAQAJAAAEgGQAGgFAAgHQAAgJgGgFIgEgEQAFgCAFAAQAPAAAMAMQAKALAAAPQAAAQgKALQgMALgPAAQgQAAgMgLg");
	this.shape_3.setTransform(60.2,46.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#ECE7DC").s().p("AgiAjQgPgPAAgUQAAgUAPgPQAPgOATAAQAUAAAPAOQAPAPAAAUQAAAUgPAPQgPAPgUAAQgTAAgPgPgAgoAJQAAARAKAKQANAMAQAAQAPAAALgMQALgKAAgRQAAgOgLgMQgLgLgPAAQgGAAgFABIAFAEQAGAGAAAIQAAAIgGAEQgFAGgJAAQgHAAgGgGQgDgCgBgCQgBAEgBAGg");
	this.shape_4.setTransform(60.4,45.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A0100").s().p("AgbAbQgKgLAAgQQAAgGACgFQAAADADACQAGAGAIAAQAHAAAGgGQAFgFAAgHQAAgJgFgFIgFgEIAKgCQAQAAALAMQALALAAAPQAAAQgLALQgLALgQAAQgPAAgMgLg");
	this.shape_5.setTransform(28.5,46.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4EBF3").s().p("AgjAjQgOgPAAgUQAAgUAOgPQAPgOAUAAQAVAAAPAOQAOAPAAAUQAAAUgOAPQgPAPgVAAQgUAAgPgPgAgoAJQAAARALAKQAMAMAQAAQAPAAALgMQALgKAAgRQAAgOgLgMQgLgLgPAAIgLABIAEAEQAHAGAAAIQAAAIgHAEQgFAGgHAAQgIAAgHgGQgCgCAAgCQgCAEgBAGg");
	this.shape_6.setTransform(28.8,45.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F7F8F7").s().p("AhKAFQgUgJgGgOQBpARBggSQgGANgWALQgfAPgqAAQgrAAgfgPg");
	this.shape_7.setTransform(44.3,62.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#561807").s().p("AhfAFQgYgLgKgQIAcgCQAGAOAVAKQAeAPAsAAQAqAAAegPQAWgLAGgOIAeABQgKARgZAMQgoAUg3ABQg3gBgogUg");
	this.shape_8.setTransform(44.4,63.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("rgba(0,0,0,0.098)").ss(0.1,1,1).p("AlXhhQgFghAAgiQAAhLAahAQgPAagWAlQACglAMgiQAJgbAOgZQAjg7A3gfQAXgNAbgIQAcgSAegLQgggGggAHQAsgMAsACQAgACAiAKQADgOALgMQAKgLASgJQAHgDAIgDQArgPAmgGQghAGgdAVQgLAJgLALQBsAFBUBEQAeARAhAbQAtAlAXA9QAWA4AEBLQgEgLgEgKQgGgQgKgMQAFAfAAAiQAAAQgBAQQgBAPgCAPAkQA4QgLg7gEgiQgHg2ABguQABgdAFgbQAbgGAhgMQAogLAUgKQAQgHAOgJQgGAJgJAKQgfAmgtAdQAtgNAngaQAagRAhg0QALAaAGAOQARAhAUAXQAIAJAJAIQgRghgIgjQAjAgBIAMQAMACAMACQgMgPgMgSQgMgWgJgQQARAKAZAEQAbAFA4AJQAoAKAgAeQAFAFAEAEQAZAbANAhQAOAqgEBBQgCANgBATQAkg8AMhGQAHAIADAOQACAHACAGAlXhhQgFACgFAEQgJAHgDAKQgGAQAGAYQAJAiAfAaQAVARAaALIABABQACABADAAAhWC7QhagXhGhIQgNgPgMgPQAAgDgBgDAkVA3QgBgBAAAAQgzhEgOhTAhWI+IkMAAIAAjLQAAhWBYALQAzAFAxADIAAA+IA5g8QAGAAAGAAIA3A9IAAABIABAAIBCAAAhnErIAGAHAhnErIgGAHAhJENIAfAAIAABiIABABAhJENIgeAeAhWEHIANAGAhWEHIhQgqIAABTAgqFwIAAAJIgsDFICcAAAhWEHIAAhMAFqg4QACAcgMAYQgRAegnARQgGACgGACQgQAXgUAWQhLBOhiAUQgkAHgmAAQgtAAgqgKABLEEIBMgnIAABPIAABCIg7g+ABXErIAFAFABTEwQAEAAAFAAABXErIgEAFIg5BAIAAAAIAAAJIAsDFAAaENIAgAAIAdAeAAaFwIgBAAIABAAAAaENIAABjABLEEIgRAJACXEsQAygEAygFQBWgGAABWIAADLIkLAAABLC+IAABGAgqENIBEAA");
	this.shape_9.setTransform(44.5,53.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#10DADB").s().p("AggAyIgBgBIAAhiIBDAAIAABiIAAABg");
	this.shape_10.setTransform(43.7,85.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#07BABB").s().p("AhNBnIArjEIAAgJIACAAIBCAAIAAAAIAAAJIAsDEg");
	this.shape_11.setTransform(43.7,100.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#129293").s().p("ABkAKIgFgEIgdgdIARgJIBMgoIAABPIAABBgAieAKIAAhSIBQArIANAGIgeAdIgGAHIg5A7g");
	this.shape_12.setTransform(43.7,83.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#13B0B1").s().p("AAigwIAhAAIAdAeIgFAFIg5A/gAhZgLIgGgHIAegeIAfAAIAABhg");
	this.shape_13.setTransform(43.7,85.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#01627F").s().p("ABPCOIgsjFIAAgIIAAgBIA5g/IAJgBIA7A+IAAhBIBkgJQBWgGAABVIAADLgAlZCOIAAjLQAAhVBYALQAzAFAxACIAAA+IA5g8IAMAAIA3A+IAAABIAAAIIgsDFg");
	this.shape_14.setTransform(43.6,97);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3CFA2").s().p("AAdEOIhEAAIgfAAIgMgGIAAhMQApAKAtAAQAnAAAjgHIAABGIgQAJgAhSC8QhagXhHhIIgZgeIgBgGQgKg7gFghQgHg3ABgtQABgdAGgcQAagGAigLQAngMAVgKQAQgHANgJQgFAKgKAKQgfAlgtAdQAtgNAngaQAagRAig0IARAoQAQAiAUAWIASASQgRgigJgjQAkAgBIAMIAXAEQgMgOgLgTIgWglQASAJAZAEIBTAOQAoAKAfAeIAKAJQAYAbANAiQAOApgEBBIgDAgQAlg8ALhGQAHAIAEAOIADANIACAMQADAbgMAYQgSAegnARIgLAEQgQAXgVAWQhLBOhiAUQgjAHgnAAQgtAAgpgKgAkSA3QgagLgVgRQgfgagKgiQgFgYAFgPQADgLAKgHIAJgGQAOBUA0BDIAAABg");
	this.shape_15.setTransform(44.2,53.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D45C25").s().p("AkhDXIgBgBQgCgagJgfIgYhOQgMgrACgkQABgsAXgdQAMgPAVgNIAogXQAbgPAMgIIAHgEQAmgcAIghQAIAegBAfQgBATgEAUQANgTAIgUQAOgeADgiQAKAgAHAgQAIAlAFAlIgChKIAAgCIACACQAvBABCAsQgGgdgPgaQgOgZgUgUIDzA7QAeAIAPAGQAYAKAPAQQAMANAHAXQAEAMAFAXIgDAeQgMBGgkA9IADghQAEhBgOgqQgNghgZgaIgJgJQgggegogKIhTgOQgZgEgRgKIAVAmQAMASAMAPIgYgEQhIgMgjggQAIAjARAgIgRgQQgUgXgRghIgRgoQghA0gaARQgnAagtANQAtgdAfgmQAJgKAGgJQgOAJgQAHQgUAKgoALQghAMgbAGQgFAbgBAcQgBAuAHA2QAEAiALA8IgFgBg");
	this.shape_16.setTransform(45.7,37.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DC622A").s().p("AlcCiQgFghAAghQAAhMAag/IgkA+QABgkANghQAIgcAPgYQAig8A4geQAXgOAbgIQAbgSAfgLQgggGggAHQArgMAsACQAhACAiAKQACgOALgLQALgMASgJIAOgFQAsgQAmgGQghAGgdAVQgMAJgLALQBsAFBVBFQAeAQAgAbQAtAlAYA+QAVA2AEBLIgIgVQgFgQgKgLQAFAeAAAjIgCAfQgEgXgEgNQgHgXgMgNQgPgQgYgJQgPgHgegHIj0g7QAUAUAPAYQAOAaAHAdQhCgsgvg+IgCgDIAAADIACBIQgFgjgJglQgGghgKggQgEAigNAfQgJATgNATQAFgTAAgTQACgggJgeQgHAhgnAdIgGADQgMAJgbAOIgoAXQgVANgMAOQgXAegCAsQgCAkANArIAYBOQAJAfACAaQg0hEgOhUg");
	this.shape_17.setTransform(45,27.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(255,255,255,0.02)").s().p("Ak7IlQhfAAhEhCQhDhCAAheIAAqGQAAhdBDhCQBEhCBfAAIJ2AAQBgAABDBCQBEBCAABdIAAKGQAABehEBCQhDBChgAAg");
	this.shape_18.setTransform(46.2,44.1);

	var maskedShapeInstanceList = [this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-8.4,-10.8,109.2,109.8), null);


(lib.Tween28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween32("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},6).to({scaleX:1,scaleY:1},7).to({scaleX:1.03,scaleY:1.03},7).to({scaleX:1,scaleY:1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-278.1,-16.6,556.2,69.5);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy8();
	this.instance.parent = this;
	this.instance.setTransform(355.5,-15.4,2.624,2.624,0,0,0,44.5,45.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape.setTransform(359.6,-18.4,1.2,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_1.setTransform(359.6,-18.4,1.2,1.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AroUoQjuAAipioQioipAAjuIAA3RQAAjuCoipQCpioDuAAIXRAAQDuAACpCoQCoCpAADuIAAXRQAADuioCpQipCojuAAg");
	this.shape_2.setTransform(359.6,-18.4,1.2,1.2);

	this.instance_1 = new lib.Symbol1copy6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-363.7,-15.4,2.624,2.624,0,0,0,44.5,45.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ALpSrI3RAAQi7AAiEiDQiDiEAAi7IAA3RQAAi7CDiEQCEiDC7AAIXRAAQC7AACECDQCDCEAAC7IAAXRQAAC7iDCEQiECDi7AAIAAAAg");
	this.shape_3.setTransform(-359.6,-18.4,1.2,1.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A19B9B").s().p("AroTdQjPAAiTiSQiSiTAAjPIAA3RQAAjPCSiTQCTiSDPAAIXRAAQDPAACTCSQCSCTAADPIAAXRQAADPiSCTQiTCSjPAAg");
	this.shape_4.setTransform(-359.6,-18.4,1.2,1.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("AroUoQjuAAipioQioipAAjuIAA3RQAAjuCoipQCpioDuAAIXRAAQDuAACpCoQCoCpAADuIAAXRQAADuioCpQipCojuAAg");
	this.shape_5.setTransform(-359.6,-18.4,1.2,1.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.instance_1},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-518,-176.8,1036.1,385.8);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy13();
	this.instance.parent = this;
	this.instance.setTransform(-4.1,3.3,2.624,2.624,0,0,0,44.6,45.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.7,scaleY:2.7},9).to({scaleX:2.62,scaleY:2.62},10).to({scaleX:2.7,scaleY:2.7},10).to({scaleX:2.62,scaleY:2.62},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183.1,-184.1,366.4,368.4);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy13();
	this.instance.parent = this;
	this.instance.setTransform(-4.4,3.3,2.624,2.624,0,0,0,44.5,45.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.7,scaleY:2.7,x:-4.3},9).to({scaleX:2.62,scaleY:2.62,x:-4.4},10).to({scaleX:2.7,scaleY:2.7,x:-4.3},10).to({scaleX:2.62,scaleY:2.62,x:-4.4},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183.1,-184.1,366.4,368.4);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween31("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},7).to({scaleX:1,scaleY:1},7).to({scaleX:1.03,scaleY:1.03},7).to({scaleX:1,scaleY:1},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0000FF").s().p("Ag0B8QgXgKgQgQQgQgRgJgYQgIgXAAgeQAAgcAKgYQAJgaARgSQASgTAXgKQAXgLAZAAQAYAAAWAKQAVAJAQARQARAQAKAWQALAWADAaIjHAeQACAQAGANQAHANAKAJQAKAJANAFQANAEAOAAQANAAAMgEQAMgEAKgGQAKgIAIgKQAHgLAEgPIAsAJQgGAVgMARQgMASgQALQgPAMgTAIQgTAGgVAAQgdAAgYgJgAgShZQgMADgLAJQgLAHgJAOQgIANgDAVICJgRIgBgEQgJgXgPgNQgPgNgYAAQgIAAgLADg");
	this.shape.setTransform(218.8,6.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0000FF").s().p("AgyDAQgKgCgMgFQgKgFgLgIQgKgHgIgLQgJgKgGgOIAtgUQAFAKAGAHQAGAHAHAFQAIAFAHADQAIADAGABQASAEARgCQAQgDAMgHQAMgHAJgIQAHgIAGgJQAFgJADgJQADgIABgHIABgKIAAgfQgOAOgPAHQgPAHgNADQgOAEgOAAQgaAAgXgJQgYgJgRgSQgRgRgJgYQgKgZAAgfQAAggALgYQALgZARgRQASgQAXgJQAWgJAWAAQAPACAPAFQAOAEAQAIQAPAIAOAOIABguIAtACIgBEIQgBAPgEAPQgDAOgIAPQgHANgLANQgKAMgOAKQgOAJgRAGQgSAGgUACIgDABQgaAAgXgHgAgiiMQgOAHgKAMQgLAMgFAQQgGARAAATQAAASAGAQQAGAQAKALQAKALAPAHQAOAGAQAAQAPAAAOgFQAOgGAMgJQAMgJAIgNQAIgNADgQIAAgeQgCgRgJgNQgIgOgMgJQgMgKgPgFQgPgGgOAAQgQAAgOAHg");
	this.shape_1.setTransform(188.1,11.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0000FF").s().p("ABHCEIAEghQgcASgbAIQgbAIgXAAQgRAAgPgEQgQgFgMgIQgMgJgHgOQgHgOAAgTQAAgSAFgPQAGgOAJgMQAKgKANgIQANgJAQgEQAPgGAQgCQAQgCAQAAQAQAAANABIAXADIgHgWQgFgMgGgJQgHgJgKgFQgKgGgOAAQgJAAgLADQgLACgOAIQgOAGgQANQgRAMgSASIgcgiQAWgUAUgNQAVgOASgHQARgIAQgDQAOgCAMAAQAVAAARAIQARAGAMANQANANAJAQQAIARAGAUQAFATADAWQACATAAAVQAAAWgDAaQgCAZgFAegAgKgBQgUAEgOAKQgOAIgHAOQgHANAEASQADAOAKAFQAJAGANgBQANABAQgFQAPgEAPgHQAPgGAOgIIAXgNIABgYIgCgXQgLgCgMgCQgNgCgMAAQgVAAgSAEg");
	this.shape_2.setTransform(158.1,5.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0000FF").s().p("ABvA3IgBgxIAAgmQgBgSgFgKQgGgJgJAAQgFAAgGAEQgGADgGAGIgLAMIgLAPIgKAOIgIANIABAUIABAcIAAAjIAAAsIgrABIgBhHIgCgxIgCgmQgBgSgFgKQgGgJgJAAQgGAAgGAEQgGAEgGAGIgNAOIgLAQIgLAQIgIALIACB7IguABIgGj2IAwgGIABBDIAQgTQAIgJAJgJQAKgHALgFQALgGAOAAQAKAAAJAEQAJADAGAGQAHAGAFALQAFAJACAOIAPgSQAIgKAJgHQAKgIAKgFQALgFANAAQALAAAJAEQAKADAIAIQAHAHAFALQAFALAAAQIADArIACA3IAABRIgwABIAAhHg");
	this.shape_3.setTransform(124.8,5.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0000FF").s().p("AgdCtIACg6IABhJIAChiIAugCIgBA8IgBAyIgBAoIAAAgIgBAxgAgMhtQgFgDgFgEQgFgFgCgGQgDgFAAgHQAAgHADgGQACgGAFgFQAFgEAFgDQAGgCAGAAQAHAAAFACQAHADAEAEQAFAFACAGQADAGAAAHQAAAHgDAFQgCAGgFAFQgEAEgHADQgFADgHgBQgGABgGgDg");
	this.shape_4.setTransform(100.1,0.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0000FF").s().p("AgzB8QgYgKgQgQQgQgRgIgYQgJgXAAgeQAAgcAKgYQAKgaARgSQARgTAXgKQAXgLAaAAQAYAAAVAKQAVAJAQARQARAQAKAWQALAWADAaIjHAeQABAQAHANQAGANALAJQAJAJAOAFQANAEAPAAQAMAAAMgEQAMgEAKgGQAKgIAIgKQAHgLADgPIAtAJQgGAVgMARQgMASgQALQgPAMgTAIQgTAGgUAAQgeAAgXgJgAgShZQgMADgLAJQgLAHgIAOQgJANgDAVICJgRIgBgEQgJgXgPgNQgPgNgXAAQgJAAgLADg");
	this.shape_5.setTransform(67.7,6.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0000FF").s().p("AA+CwQAGgWADgVQADgVABgRQACgTgBgSQgBgZgHgRQgIgQgKgLQgLgLgNgFQgNgFgNAAQgMABgNAHQgMAFgNAMQgNAKgLAWIgCCZIgtABIgEljIA1gCIgBCMQANgOAOgHQAOgJANgEQAOgEANgCQAaAAAVAJQAVAKAQARQAPARAJAZQAJAYABAfIgBAhQAAARgBARIgEAfQgCAPgEALg");
	this.shape_6.setTransform(38.3,0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0000FF").s().p("AgbgaIhQACIAAgrIBRgCIABhlIAtgDIgCBnIBagDIgDArIhXADIgBDIIgxABg");
	this.shape_7.setTransform(11.3,1.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0000FF").s().p("Ag0B8QgXgKgQgQQgQgRgJgYQgIgXAAgeQAAgcAKgYQAJgaARgSQASgTAXgKQAXgLAZAAQAYAAAWAKQAVAJAQARQARAQAKAWQALAWADAaIjHAeQACAQAGANQAGANALAJQAKAJANAFQANAEAOAAQANAAAMgEQAMgEAKgGQAKgIAIgKQAHgLADgPIAtAJQgHAVgLARQgMASgQALQgPAMgTAIQgTAGgVAAQgdAAgYgJgAgShZQgMADgLAJQgLAHgJAOQgIANgDAVICJgRIgBgEQgJgXgPgNQgPgNgYAAQgIAAgLADg");
	this.shape_8.setTransform(-26.1,6.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0000FF").s().p("AglB8IhRjoIAvgHIBFDFIBJjOIAwAHIhcDyg");
	this.shape_9.setTransform(-53.2,5.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0000FF").s().p("AhoA5IgCg5IgBgsIgBgiIgCgxIA1gBIAAAdIABAbIAAAcIAQgXQAKgNAMgLQAMgMANgIQAPgJAQgCQATgBAPAJQAGAEAHAGQAGAGAGAKQAFAJAEAOQADANACASIguARIgCgUIgEgOQgDgGgDgEIgHgGQgIgGgKAAQgHABgHAFQgHAEgHAHIgPAQIgPASIgOATIgLAQIAAAiIABAfIABAaIABATIgxAGIgDhIg");
	this.shape_10.setTransform(-78,6.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0000FF").s().p("Ag0B8QgXgKgQgQQgQgRgIgYQgJgXAAgeQAAgcAKgYQAJgaARgSQASgTAXgKQAXgLAZAAQAZAAAVAKQAVAJAQARQARAQAKAWQALAWADAaIjHAeQABAQAHANQAGANALAJQAJAJAOAFQANAEAOAAQANAAAMgEQAMgEAKgGQAKgIAIgKQAHgLADgPIAtAJQgHAVgLARQgMASgQALQgPAMgTAIQgTAGgVAAQgdAAgYgJgAgShZQgMADgLAJQgLAHgJAOQgIANgDAVICJgRIgBgEQgJgXgPgNQgPgNgYAAQgIAAgLADg");
	this.shape_11.setTransform(-105.9,6.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0000FF").s().p("AghCCIgTgEIgVgHIgVgKQgLgGgKgIIAXgjIAaAOIAbALQAOAEAOADQAOADAPgBQANAAAJgBQAIgDAGgEQAGgEADgEIAEgJIABgIIgCgJQgCgEgFgFQgEgEgIgEQgHgDgLgDIgZgDQgUgBgTgDQgTgEgPgGQgPgIgKgKQgKgMgCgRQgCgSAEgNQAEgNAIgMQAHgKAMgJQALgIANgFQAOgFAPgDQAOgDAPAAIAVABIAaADQANADANAGQAOAFAMAJIgQApQgPgHgOgFIgYgHQgNgDgLAAQgjgDgUAKQgUAKAAATQAAAOAHAGQAIAGANADQANADASABQAQABAUADQAXAEAPAGQAQAHAJAJQAKAJAEALQAEALAAANQAAAVgJAQQgJAOgPAJQgPAKgTAEQgTAFgVABQgUgBgWgEg");
	this.shape_12.setTransform(-133.4,6.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0000FF").s().p("AgVCxQgNgEgPgHQgPgHgPgNIAAAeIgrABIgEljIAygCIgBCJQANgNAPgHQAOgHAMgEQAPgEAMAAQAQAAAPAFQAQAEAOAIQAOAIALAMQAMALAIAPQAJAOAEAPQAEARAAASQgBATgFASQgFARgJAPQgJAPgLALQgMAMgNAIQgNAIgOAFQgOAEgOAAQgNgBgPgDgAgagdQgNAEgKAIQgKAIgIAKQgHAKgEAMIgBA3QAFAOAHALQAIALAKAIQAKAHAMAEQALAEAMAAQAQAAAPgGQAOgHALgLQALgLAHgQQAGgPABgSQACgRgGgQQgFgQgLgMQgKgMgPgHQgPgHgRAAQgOAAgMAFg");
	this.shape_13.setTransform(-160.8,0.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0000FF").s().p("AgtCsQgXgGgUgMQgSgLgRgRQgPgRgMgTQgLgUgHgXQgFgXAAgYQAAgYAFgWQAHgXALgVQAMgTAPgRQARgQASgLQAUgMAXgGQAWgHAXAAQAYAAAWAHQAWAGAUAMQAUALAQAQQAPARAMATQALAVAGAXQAHAWgBAYQABAYgHAXQgGAXgLAUQgMATgPARQgQARgUALQgUAMgWAGQgWAGgYAAQgXAAgWgGgAgxh5QgXAKgSASQgRASgKAYQgLAZABAaQgBAcALAYQAKAYARASQASASAXALQAXAKAaAAQARAAARgFQARgEAOgJQAOgJAMgMQAMgMAIgPQAJgPAEgRQAFgQAAgTQAAgSgFgQQgEgRgJgPQgIgPgMgMQgMgMgOgJQgOgIgRgFQgRgFgRAAQgaAAgXALg");
	this.shape_14.setTransform(-196.1,1.1);

	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(4.7,-0.4,0.961,0.961,0,0,0,-0.1,-0.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("EgjsAFnQhoAAhKhKQhKhKAAhoIAAjVQAAhoBKhKQBKhKBoAAMBHYAAAQBpAABJBKQBLBKAABoIAADVQAABohLBKQhJBKhpAAg");
	this.shape_15.setTransform(5,-1.1,0.961,0.961);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.instance},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238.7,-37.4,487.4,75.1);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween33("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238.7,-33.4,487.4,75.1);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.Tween36 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol10("synched",23);
	this.instance.parent = this;
	this.instance.setTransform(-5.3,35.2);

	this.instance_1 = new lib.Symbol9("synched",23);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-5,-187.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-246.6,-221.5,493.2,443.1);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(636.8,359.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(302).to({_off:false},0).wait(38).to({startPosition:0},0).to({alpha:0,startPosition:10},10).wait(1));

	// Layer_12
	this.instance_1 = new lib.Symbol1copy13();
	this.instance_1.parent = this;
	this.instance_1.setTransform(635.7,333,2.624,2.624,0,0,0,44.5,45.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(293).to({_off:false},0).to({regX:44.6,scaleX:3.03,scaleY:3.03,x:635.9,y:332.9},15).wait(15).to({alpha:0},14).wait(14));

	// Layer_8
	this.instance_2 = new lib.Tween24("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(276.3,553.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(285).to({_off:false},0).to({scaleX:0.9,scaleY:0.9,x:635.4,y:345.5},7).to({alpha:0},16).wait(43));

	// Layer_11
	this.instance_3 = new lib.Symbol1copy("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(446.3,624.8,1,1,-22.2,0,0,190.5,64.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(250).to({_off:false},0).to({_off:true},35).wait(66));

	// Layer_10
	this.instance_4 = new lib.Symbol1copy17("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(289.1,296,1,1,0,0,0,41,60.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(216).to({_off:false},0).to({_off:true},34).wait(101));

	// Layer_7
	this.instance_5 = new lib.Tween22("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(635.4,328.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(154).to({_off:false},0).to({x:887.9,y:486.1,alpha:0.5},9).to({x:1003.9,y:560.1},8).to({startPosition:0},39).to({alpha:0},7).to({_off:true},1).wait(133));

	// Layer_6
	this.instance_6 = new lib.Tween22("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(635.4,328.8);
	this.instance_6._off = true;

	this.instance_7 = new lib.Symbol8();
	this.instance_7.parent = this;
	this.instance_7.setTransform(276.1,550.6);
	this.instance_7.alpha = 0.5;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(154).to({_off:false},0).to({x:388.4,y:486.1,alpha:0.5},9).to({_off:true,x:276.1,y:550.6,mode:"independent"},8).wait(180));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(163).to({_off:false},8).wait(39).to({alpha:0},7).to({_off:true},1).wait(133));

	// Layer_4
	this.instance_8 = new lib.Tween17("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,560.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(93).to({_off:false},0).to({alpha:1},7).to({startPosition:0},32).to({startPosition:0},177).to({alpha:0},7).wait(35));

	// Layer_3
	this.instance_9 = new lib.Symbol11("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640.1,329.7);

	this.instance_10 = new lib.Tween28("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(645.8,103.7);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_10},{t:this.instance_9}]},124).to({state:[{t:this.instance_10}]},100).to({state:[{t:this.instance_10}]},7).to({state:[]},1).wait(119));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(124).to({_off:false},0).wait(100).to({startPosition:6},0).to({alpha:0,startPosition:13},7).to({_off:true},1).wait(119));

	// Layer_5
	this.questiontxt = new lib.questiontext_mccopy();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(606.6,43.4,1.641,1.641,0,0,0,0.1,-0.6);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(93).to({_off:false},0).to({alpha:1},7).wait(209).to({alpha:0},7).wait(35));

	// Layer_1
	this.instance_11 = new lib.Symbol1copy13();
	this.instance_11.parent = this;
	this.instance_11.setTransform(635.7,333,2.624,2.624,0,0,0,44.5,45.4);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(93).to({_off:false},0).to({alpha:1},7).to({regX:44.6,x:635.9},24).to({regX:44.5,x:635.7},185).to({alpha:0},7).wait(35));

	// Layer_9
	this.instance_12 = new lib.Symbol10("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(640.1,329.8);

	this.instance_13 = new lib.Symbol9("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640.4,106.9);

	this.instance_14 = new lib.Tween36("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(645.4,294.6);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_13},{t:this.instance_12}]},50).to({state:[{t:this.instance_14}]},43).to({state:[{t:this.instance_14}]},5).to({state:[]},1).wait(252));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(93).to({_off:false},0).to({alpha:0},5).to({_off:true},1).wait(252));

	// Layer_1
	this.instance_15 = new lib.Symbol1copy13();
	this.instance_15.parent = this;
	this.instance_15.setTransform(635.7,333,2.624,2.624,0,0,0,44.5,45.4);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(34).to({_off:false},0).to({alpha:1},5).wait(54).to({alpha:0},5).to({_off:true},1).wait(252));

	// Layer_2
	this.questiontxt_1 = new lib.questiontext_mc();
	this.questiontxt_1.name = "questiontxt_1";
	this.questiontxt_1.parent = this;
	this.questiontxt_1.setTransform(606.6,58,1.641,1.641,0,0,0,0.1,-0.6);
	this.questiontxt_1.alpha = 0;
	this.questiontxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt_1).wait(15).to({_off:false},0).to({alpha:1},7).wait(71).to({alpha:0},5).to({_off:true},1).wait(252));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(580.8,297.2,1402,848);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;