(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgaBmIgPgDIgQgGIgRgIQgIgEgIgGIASgcIAUAKIAVAJQALAEAMACQAKACAMAAQAKAAAHgCIALgEQAFgDACgEIAEgHQABgCgBgEQAAgEgBgDIgGgHQgDgEgGgDIgOgEQgIgCgMgBQgPAAgPgDQgPgDgMgFQgMgGgIgIQgHgJgCgOQgBgNADgLQADgKAGgJQAGgIAJgHQAJgGAKgEQALgFALgCQAMgCALAAIAQABIAUADQALACALAEQAKAEAKAHIgNAhQgMgGgLgEIgTgGIgSgCQgbgCgQAIQgQAHAAAQQAAAKAGAFQAGAFAKADQALACANAAIAdADQARADAMAGQANAEAHAHQAHAIAEAIQADAJAAAKQAAARgHAMQgHALgMAIQgLAHgPADQgQAEgQABQgQgBgRgDg");
	this.shape.setTransform(296.2,4.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgoBhQgSgHgNgNQgNgNgGgTQgHgTAAgXQAAgVAIgUQAIgTANgPQANgOATgJQASgIATAAQAUAAAQAHQAQAIANANQANAMAJASQAIASACAUIicAWQACANAFAKQAFALAIAHQAIAHAKADQAKAEALAAQAKAAAJgDQAKgDAIgFQAIgGAGgJQAFgIAEgLIAiAHQgFAQgJANQgKAOgLAJQgNAKgOAFQgQAFgQAAQgXAAgSgHgAgOhFQgJACgJAHQgIAGgHALQgGAKgDAQIBsgNIgBgEQgIgSgMgKQgLgKgTAAQgGAAgJADg");
	this.shape_1.setTransform(275.4,4.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AhRAsIgCgsIgBgiIgBgbIAAgnIAoAAIABAXIAAAUIAAAXIANgTQAIgKAJgJQAKgIAKgHQALgGANgCQAOgBANAHIAKAHQAFAGAEAHQAEAHADALQACAKABAOIgjAOQAAgJgBgHIgEgLIgFgIIgFgFQgGgEgIAAQgFABgGADQgFADgGAHQgFAFgHAHIgLAOIgLAOIgJANIABAbIAAAYIABAVIABAOIgnAFIgCg5g");
	this.shape_2.setTransform(255.2,4.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgTBlQgQgBgMgFQgMgGgIgJQgIgKgEgMQgFgMgDgOQgCgNgBgOIgBgbQAAgTACgTQABgTAEgUIAlABQgDAWgBASIgBAgIAAAPIABATQABALADAKQADAKAEAIQAFAIAHAFQAHAEAKgBQANgCAPgSQAPgRARgiIgBguIgBgbIAAgOIAngFIACA5IABAsIABAjIABAaIABAmIgpABIgBg0QgGALgIAJQgHAKgJAHQgJAHgKAEQgIAEgKAAIgCAAg");
	this.shape_3.setTransform(232.3,4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgWgUIg+ABIABghIA+gCIAChPIAjgCIgCBRIBHgDIgDAiIhEABIgBCdIglABg");
	this.shape_4.setTransform(212.2,0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AggBhQgUgHgOgPQgPgOgJgTQgJgTAAgXQAAgMADgNQAEgNAHgMQAHgMAJgKQAKgKAMgHQALgHAOgFQAOgDAOAAQAQAAAOADQAOAEAMAIQAMAGAKALQAKAKAGANIAAABIgeASIAAgBQgFgJgGgIQgHgHgIgFQgJgFgJgDQgKgCgKAAQgOgBgNAGQgNAFgJAKQgKAKgGAOQgFANAAANQAAAPAFANQAGANAKAJQAJALANAFQANAGAOAAQAKAAAJgDQAJgDAIgEQAIgFAGgGQAHgHAFgIIABgBIAfASIAAABQgHAMgLAJQgKAJgMAIQgMAGgOAEQgOADgOAAQgTAAgTgIg");
	this.shape_5.setTransform(192.2,4.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgWCIIABgvIABg4IABhNIAkgCIAAAvIgBAoIgBAfIAAAZIgBAngAgJhVQgFgDgDgDQgEgEgCgEQgCgFAAgFQAAgFACgFQACgFAEgDQADgEAFgCQAFgCAEAAQAFAAAFACQAFACADAEQAEADACAFQACAFAAAFIgCAKIgGAIQgDADgFADQgFACgFAAQgEAAgFgCg");
	this.shape_6.setTransform(176.5,-0.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AhjiSIAogBIAAAaQAKgKALgGIAVgIQALgDAJgCQANAAAMAEQAMAEALAGQALAHAJAJQAJAIAHAMQAGALAEANQADANAAAOQgBAPgEAOQgEANgHALQgHALgJAKQgJAJgKAHQgKAFgLAEQgLADgLAAQgKAAgLgDQgKgDgMgEQgMgGgMgKIgBB7IgiABgAgUhtQgKAEgIAGQgIAHgFAJQgGAIgDAJIAAAjQADALAFAKQAGAJAIAFQAIAGAJADQAJADAKAAQALAAAMgFQALgEAIgIQAIgIAFgMQAFgMABgNQABgOgEgMQgEgMgIgJQgJgJgLgGQgLgFgNAAQgLABgJADg");
	this.shape_7.setTransform(160.2,8.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgoBhQgSgHgNgNQgNgNgGgTQgHgTAAgXQAAgVAIgUQAHgTAOgPQAOgOARgJQATgIAUAAQATAAAQAHQAQAIAOANQANAMAHASQAJASACAUIibAWQABANAFAKQAFALAIAHQAHAHALADQAKAEAMAAQAJAAAKgDQAJgDAIgFQAIgGAGgJQAFgIADgLIAjAHQgFAQgJANQgKAOgMAJQgMAKgOAFQgPAFgQAAQgXAAgTgHgAgOhFQgJACgIAHQgJAGgHALQgHAKgDAQIBsgNIAAgEQgIgSgLgKQgNgKgRAAQgHAAgJADg");
	this.shape_8.setTransform(127.3,4.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AAxCKQAFgSACgRIADgdIABgcQgCgUgFgOQgGgMgIgIQgJgKgKgDQgKgEgKAAQgJABgKAFQgKAEgKAJQgKAJgJAQIgBB4IgjAAIgEkVIAqgCIgBBuQAKgKALgHQAMgGAJgEQALgDAKgBQAUAAARAHQARAIALANQANAOAGATQAIATAAAYIAAAaIgBAaIgCAYIgGAVg");
	this.shape_9.setTransform(104.2,-0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgVgUIg/ABIABghIA/gCIABhPIAjgCIgBBRIBGgDIgDAiIhDABIgCCdIgmABg");
	this.shape_10.setTransform(83.1,0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgaBmIgPgDIgQgGIgRgIQgIgEgIgGIASgcIAUAKIAVAJQALAEAMACQAKACAMAAQAKAAAHgCIALgEQAFgDACgEIAEgHQABgCgBgEQAAgEgBgDIgGgHQgDgEgGgDIgOgEQgIgCgMgBQgPAAgPgDQgPgDgMgFQgMgGgIgIQgHgJgCgOQgBgNADgLQADgKAGgJQAGgIAJgHQAJgGAKgEQALgFALgCQAMgCALAAIAQABIAUADQALACALAEQAKAEAKAHIgNAhQgMgGgLgEIgTgGIgSgCQgbgCgQAIQgQAHAAAQQAAAKAGAFQAGAFAKADQALACANAAIAdADQARADAMAGQANAEAHAHQAHAIAEAIQADAJAAAKQAAARgHAMQgHALgMAIQgLAHgPADQgQAEgQABQgQgBgRgDg");
	this.shape_11.setTransform(54.1,4.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgVgUIg/ABIABghIA/gCIABhPIAjgCIgBBRIBGgDIgDAiIhDABIgCCdIgmABg");
	this.shape_12.setTransform(35.3,0.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AggBhQgUgHgOgPQgPgOgJgTQgJgTAAgXQAAgMADgNQAEgNAHgMQAHgMAJgKQAKgKAMgHQALgHAOgFQAOgDAOAAQAQAAAOADQAOAEAMAIQAMAGAKALQAKAKAGANIAAABIgeASIAAgBQgFgJgGgIQgHgHgIgFQgJgFgJgDQgKgCgKAAQgOgBgNAGQgNAFgJAKQgKAKgGAOQgFANAAANQAAAPAFANQAGANAKAJQAJALANAFQANAGAOAAQAKAAAJgDQAJgDAIgEQAIgFAGgGQAHgHAFgIIABgBIAfASIAAABQgHAMgLAJQgKAJgMAIQgMAGgOAEQgOADgOAAQgTAAgTgIg");
	this.shape_13.setTransform(15.3,4.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgoBhQgSgHgNgNQgNgNgGgTQgHgTAAgXQAAgVAIgUQAIgTANgPQANgOASgJQATgIAUAAQASAAARAHQAQAIAOANQANAMAHASQAJASACAUIicAWQACANAFAKQAFALAIAHQAHAHALADQAKAEAMAAQAJAAAKgDQAJgDAIgFQAIgGAGgJQAFgIADgLIAjAHQgFAQgJANQgKAOgMAJQgMAKgPAFQgOAFgQAAQgXAAgTgHgAgOhFQgJACgIAHQgJAGgHALQgHAKgDAQIBsgNIgBgEQgGgSgNgKQgMgKgRAAQgHAAgJADg");
	this.shape_14.setTransform(-6.7,4.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AhUAtIgBgtIgBgiIgBgbIgBgmIApgBIAAA0QAGgKAIgJQAIgJAJgHQAIgHAKgFQAJgEALgBQAOgBALAEQAKADAHAGQAHAFAFAIQAFAIACAJIAEAQIACAQQABAegBAfIgBBBIglgBIADg6QABgdgCgdIAAgIIgCgLIgEgMQgDgFgFgFQgEgEgGgCQgGgCgJABQgOACgOATQgOATgRAjIABAuIABAaIAAAOIgnAFIgCg4g");
	this.shape_15.setTransform(-29.4,4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AhUAtIgBgtIgBgiIgBgbIgBgmIApgBIAAA0QAGgKAIgJQAIgJAJgHQAIgHAKgFQAJgEALgBQAOgBALAEQAKADAHAGQAHAFAFAIQAFAIACAJIAEAQIACAQQABAegBAfIgBBBIglgBIADg6QABgdgCgdIAAgIIgCgLIgEgMQgDgFgFgFQgEgEgGgCQgGgCgJABQgOACgOATQgOATgRAjIABAuIABAaIAAAOIgnAFIgCg4g");
	this.shape_16.setTransform(-52.2,4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AglBjQgSgIgMgOQgOgOgHgUQgIgTAAgYQAAgXAIgUQAHgTAOgOQAMgOASgIQASgHATAAQAUAAASAIQARAIANAOQANAOAIAUQAIATAAAWQAAAXgIATQgIAUgNAOQgNAOgRAIQgSAIgUAAQgTAAgSgHgAgZhDQgLAHgIALQgHAKgDANQgDAOgBAMQAAANAEANQAEANAHALQAHAKALAHQALAHAOAAQAPAAALgHQAMgHAGgKQAIgLADgNQADgNAAgNQAAgMgCgOQgEgNgHgKQgHgLgLgHQgLgGgQAAQgPAAgKAGg");
	this.shape_17.setTransform(-74.7,4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AggBhQgUgHgOgPQgPgOgJgTQgJgTAAgXQAAgMADgNQAEgNAHgMQAHgMAJgKQAKgKAMgHQALgHAOgFQAOgDAOAAQAQAAAOADQAOAEAMAIQAMAGAKALQAKAKAGANIAAABIgeASIAAgBQgFgJgGgIQgHgHgIgFQgJgFgJgDQgKgCgKAAQgOgBgNAGQgNAFgJAKQgKAKgGAOQgFANAAANQAAAPAFANQAGANAKAJQAJALANAFQANAGAOAAQAKAAAJgDQAJgDAIgEQAIgFAGgGQAHgHAFgIIABgBIAfASIAAABQgHAMgLAJQgKAJgMAIQgMAGgOAEQgOADgOAAQgTAAgTgIg");
	this.shape_18.setTransform(-96.8,4.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0000").s().p("AgVgUIg/ABIABghIA+gCIAChPIAjgCIgCBRIBHgDIgDAiIhEABIgBCdIglABg");
	this.shape_19.setTransform(-126.5,0.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0000").s().p("AA4BnIACgZQgWAOgVAGQgUAGgSAAQgNAAgNgDQgMgDgJgHQgJgHgGgLQgGgLAAgOQAAgPAFgMQAEgLAHgJQAIgIAKgGQALgHAMgEQALgEANgBQAMgCANAAIAXABIASACIgGgSQgEgJgFgHQgFgHgIgEQgIgEgKAAQgHAAgIACQgKACgKAFQgMAGgMAJQgNAKgPAPIgVgbQARgQAPgLQAQgKAPgGQAOgGALgCQALgCALAAQAQAAANAGQANAFAKAKQAJAKAIANQAGAOAFAPQAEAPACARIACAfQAAASgDAUQgCAUgDAXgAgIAAQgPADgLAHQgLAHgFALQgHAKAEANQADALAHAFQAHAEALAAQAKAAAMgDIAYgJQAMgFAKgGQALgGAHgFIAAgSIAAgSIgTgDQgJgCgLAAQgQAAgOAEg");
	this.shape_20.setTransform(-147.5,3.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF0000").s().p("AAwCKQAGgSACgRIADgdIABgcQgBgUgGgOQgGgMgIgIQgIgKgLgDQgKgEgKAAQgJABgKAFQgKAEgKAJQgKAJgJAQIgBB4IgkAAIgDkVIAqgCIgBBuQAKgKALgHQALgGAKgEQALgDALgBQATAAARAHQARAIALANQANAOAGATQAIATAAAYIAAAaIgBAaIgCAYIgGAVg");
	this.shape_21.setTransform(-170.5,-0.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF0000").s().p("AgVgUIg/ABIABghIA/gCIABhPIAjgCIgBBRIBGgDIgDAiIhDABIgCCdIgmABg");
	this.shape_22.setTransform(-191.6,0.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF0000").s().p("AAxCKQAFgSACgRIADgdIABgcQgCgUgFgOQgGgMgIgIQgJgKgKgDQgKgEgKAAQgJABgLAFQgJAEgKAJQgKAJgJAQIgBB4IgjAAIgEkVIAqgCIgBBuQAKgKALgHQAMgGAJgEQALgDAKgBQAUAAARAHQARAIALANQANAOAGATQAIATAAAYIAAAaIgBAaIgCAYIgGAVg");
	this.shape_23.setTransform(-222,-0.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FF0000").s().p("AgVgUIg/ABIABghIA/gCIABhPIAjgCIgBBRIBGgDIgDAiIhDABIgCCdIgmABg");
	this.shape_24.setTransform(-243.2,0.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF0000").s().p("AA4BnIACgZQgWAOgVAGQgUAGgSAAQgNAAgNgDQgMgDgJgHQgKgHgFgLQgGgLAAgOQAAgPAFgMQAEgLAHgJQAIgIAKgGQALgHAMgEQALgEANgBQAMgCANAAIAXABIASACIgGgSQgEgJgFgHQgFgHgIgEQgIgEgKAAQgIAAgHACQgKACgKAFQgMAGgMAJQgNAKgOAPIgXgbQASgQAPgLQAQgKAOgGQAOgGAMgCQALgCALAAQAQAAANAGQANAFAKAKQAKAKAGANQAHAOAFAPQAEAPACARIACAfQgBASgCAUQgCAUgDAXgAgIAAQgPADgLAHQgLAHgFALQgHAKAEANQADALAHAFQAHAEALAAQAKAAAMgDIAYgJQAMgFAKgGQALgGAHgFIAAgSIAAgSIgTgDQgJgCgLAAQgPAAgPAEg");
	this.shape_25.setTransform(-264.1,3.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF0000").s().p("AhjiSIAogBIAAAaQAKgKALgGIAVgIQALgDAJgCQANAAAMAEQAMAEALAGQALAHAJAJQAJAIAHAMQAGALAEANQADANAAAOQgBAPgEAOQgEANgHALQgHALgJAKQgJAJgKAHQgKAFgLAEQgLADgLAAQgKAAgLgDQgKgDgMgEQgMgGgMgKIgBB7IgiABgAgUhtQgKAEgIAGQgIAHgFAJQgGAIgDAJIAAAjQADALAFAKQAGAJAIAFQAIAGAJADQAJADAKAAQALAAAMgFQALgEAIgIQAIgIAFgMQAFgMABgNQABgOgEgMQgEgMgIgJQgJgJgLgGQgLgFgNAAQgLABgJADg");
	this.shape_26.setTransform(-286.6,8.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(3,1,1).p("EgsdgEXMBY7AAAQBrAABMBMQBNBNAABrIAAAnQAABshNBMQhMBMhrAAMhY7AAAQhrAAhNhMQhMhMAAhsIAAgnQAAhrBMhNQBNhMBrAAg");
	this.shape_27.setTransform(2.2,3.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("EgsdAEYQhrAAhNhNQhMhLAAhsIAAgnQAAhsBMhMQBNhMBrAAMBY7AAAQBrAABMBMQBNBMAABsIAAAnQAABshNBLQhMBNhrAAg");
	this.shape_28.setTransform(2.2,3.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-312.7,-31,627,63.6);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(10,1,1).p("Eg/egFDIAAKHMBH1AAAIAAk+MA3IAAAIAAkC");
	this.shape.setTransform(0,-1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-411.2,-38.6,822.6,74.8);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol6copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ApKJKQjzjyAAlYQAAlXDzjyQDzj0FXAAQFYAADzD0QDzDyAAFXQAAFYjzDyQjzD0lYAAQlXAAjzj0g");
	this.shape.setTransform(87.3,87.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6600").s().p("AppJqQkAkAAAlqQAAlpEAkAQEAkAFpAAQFqAAD/EAQEBEAAAFpQAAFqkBEAQj/EAlqAAQlpAAkAkAg");
	this.shape_1.setTransform(87.4,87.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6copy3, new cjs.Rectangle(0,0,174.8,174.8), null);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNA3IgIgCIgJgCIgJgFIgJgFIAKgQIAKAGIAMAFIAMADIALABIAJgBIAHgDIADgDIACgEIABgDIgBgEIgDgEIgFgDIgHgCIgMgCIgPgCQgIgBgHgDQgGgDgEgEQgFgFgBgIQAAgHABgGQACgFAEgEQACgFAGgEQAEgDAGgDQAFgCAHgBIAMgBIAJABQAFgBAGACIALADQAFADAFADIgGASIgNgGIgKgDIgKgBQgOgBgJAEQgIAFAAAIQAAAFADAEQADACAGABQAGACAHgBIAPACQAKACAGADQAHABAEAFQADADACAGQACAEAAAFQAAAKgEAFQgEAHgGAEQgGAEgIACIgRACQgIAAgJgCg");
	this.shape.setTransform(212.2,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_1.setTransform(200.9,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgrAZIgBgZIgBgSIAAgOIgBgVIAWAAIABAMIAAALIAAAMIAHgKIAJgKQAFgFAFgDQAGgEAHgBQAIAAAGADIAGAFIAFAHQACAEACAFIACANIgTAHIgBgIIgCgGIgDgEIgDgDQgDgCgEAAIgGACIgGAFIgGAHIgGAIIgGAIIgFAFIAAAPIABANIAAALIABAIIgVADIgBgeg");
	this.shape_2.setTransform(190,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAbAbIgIALIgIAIIgKAHQgFACgGAAQgIgBgHgDQgGgDgEgFQgFgFgDgHIgDgNIgCgQIgBgOIACgUIACgVIAUABIgCAVIgBARIABAIIAAAKIACAMQACAGACAEQACAEAEACQAEADAFgBQAHgBAIgKQAJgIAJgTIgBgYIAAgPIgBgIIAVgCIABAeIABAYIAAASIABAPIAAAUIgWABg");
	this.shape_3.setTransform(177.7,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIABgSIAiAAIAAgsIATgBIgBAsIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_4.setTransform(166.8,-3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_5.setTransform(156,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgMBJIABgZIAAgeIABgpIATgBIAAAZIgBAVIAAARIAAANIgBAVgAgEguIgFgDIgDgEIgBgFIABgGIADgEIAFgDIAEgBIAFABIAFADIADAEIABAGIgBAFIgDAEIgFADIgFABIgEgBg");
	this.shape_6.setTransform(147.6,-3.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag1hOIAWgBIgBAOQAGgFAGgDIALgFIALgCQAGAAAHACQAGACAGADIALAIQAFAFAEAGQADAGACAHQACAHAAAIQgBAIgCAIQgCAHgEAFQgDAGgFAFIgLAJQgFADgGACIgMACIgLgCIgMgEQgGgDgGgFIgBBCIgSAAgAgLg6QgFACgEADIgHAIIgFAKIAAATQABAGADAFIAIAHIAJAFQAFACAFAAQAGAAAGgDQAGgCAEgEQAFgFADgGQACgGABgHQAAgHgCgHQgCgHgFgFQgEgFgGgDQgGgCgHAAQgFAAgGACg");
	this.shape_7.setTransform(138.8,1.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_8.setTransform(121,-1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAaBLIAEgTIACgQIAAgQQAAgKgDgIQgEgGgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgCQALAAAJADQAJAFAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAOIgBANIgDAMg");
	this.shape_9.setTransform(108.5,-3.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIAAgSIAiAAIABgsIASgBIAAAsIAmgBIgCASIgkABIgBBUIgTAAg");
	this.shape_10.setTransform(97.1,-3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgOA3IgHgCIgJgCIgJgFIgJgFIAKgQIALAGIALAFIAMADIAMABIAJgBIAGgDIAEgDIABgEIABgDIgBgEIgDgEIgFgDIgIgCIgLgCIgQgCQgHgBgHgDQgGgDgEgEQgFgFgBgIQAAgHABgGQACgFAEgEQACgFAGgEQAEgDAGgDQAGgCAGgBIALgBIAJABQAGgBAGACIALADQAGADAEADIgGASIgMgGIgKgDIgKgBQgPgBgJAEQgIAFAAAIQAAAFADAEQAEACAFABQAFACAIgBIAPACQAKACAGADQAGABAFAFQAEADABAGQACAEAAAFQAAAKgEAFQgDAHgHAEQgGAEgIACIgRACQgIAAgKgCg");
	this.shape_11.setTransform(81.5,-0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIABgSIAhAAIABgsIATgBIgBAsIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_12.setTransform(71.3,-3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_13.setTransform(60.5,-1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_14.setTransform(48.6,-1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAbA2IACggIAAgeIgBgFIgBgGIgCgGQgBgEgDgCQgCgCgDgBQgEgBgEABQgIAAgHALQgIAKgJASIABAZIAAAPIAAAHIgVADIgBgeIgBgZIAAgSIgBgOIAAgVIAWAAIAAAcIAIgKIAIgJIAKgHQAFgCAGAAQAIgBAFACQAGACADADQAEADADAEIAEAJIACAJIABAIIAAAhIgBAkg");
	this.shape_15.setTransform(36.4,-1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAbA2IACggIAAgeIgBgFIgBgGIgCgGQgBgEgDgCQgCgCgDgBQgEgBgEABQgIAAgHALQgIAKgJASIABAZIAAAPIAAAHIgVADIgBgeIgBgZIAAgSIgBgOIAAgVIAWAAIAAAcIAIgKIAIgJIAKgHQAFgCAGAAQAIgBAFACQAGACADADQAEADADAEIAEAJIACAJIABAIIAAAhIgBAkg");
	this.shape_16.setTransform(24.1,-1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgTA2QgKgFgHgIQgHgHgEgLQgEgKAAgNQAAgMAEgLQAEgKAHgIQAHgHAKgEQAJgFAKAAQALAAAJAFQAKAFAHAHQAHAIAEAKQAEAKAAAMQAAAMgEALQgEAKgHAIQgHAIgKAEQgJAFgLgBQgKAAgJgDgAgNgjQgGADgEAGQgEAGgCAHIgBANQAAAHACAHQABAHAEAGQAEAGAGAEQAGADAHAAQAIAAAGgDQAGgEAEgGQAEgGACgHQACgHAAgHIgCgNQgCgHgDgGQgEgGgGgDQgGgEgJAAQgHAAgGAEg");
	this.shape_17.setTransform(11.9,-1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_18.setTransform(0,-1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIAAgSIAjAAIAAgsIASgBIAAAsIAmgBIgCASIgkABIgBBUIgTAAg");
	this.shape_19.setTransform(-16,-3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAeA4IABgOQgMAHgKAEQgMADgJABQgHAAgHgCQgGgCgGgEQgEgEgEgFQgDgGAAgJQAAgHACgGQADgHAEgEQAEgFAGgCIAMgHIANgCIANgBIAMAAIAKACIgDgKIgFgJQgDgDgEgDQgEgDgGABIgIABQgFABgGADIgMAIQgHAFgIAIIgMgPQAJgIAJgFQAJgGAHgDQAIgEAGgBIALgBQAJAAAHADQAHADAFAFQAGAGADAHIAGAPIAEARIABARIgCAVIgDAXgAgEAAQgIACgGADQgGAEgDAGQgDAFACAIQABAFAEADQAEACAFABQAGAAAHgCIAMgGIAMgFIAKgGIAAgJIAAgKIgLgCIgKgBQgJAAgHACg");
	this.shape_20.setTransform(-27.3,-1.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAaBLIAEgTIACgQIAAgQQAAgKgDgIQgEgGgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgCQALAAAJADQAJAFAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAOIgBANIgDAMg");
	this.shape_21.setTransform(-39.8,-3.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIABgSIAhAAIABgsIATgBIgBAsIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_22.setTransform(-51.2,-3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAaBLIAEgTIACgQIAAgQQAAgKgDgIQgEgGgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgCQALAAAJADQAJAFAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAOIgBANIgDAMg");
	this.shape_23.setTransform(-67.6,-3.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIAAgSIAjAAIAAgsIASgBIAAAsIAmgBIgCASIgkABIgBBUIgTAAg");
	this.shape_24.setTransform(-79,-3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAeA4IABgOQgMAHgKAEQgMADgJABQgHAAgHgCQgGgCgGgEQgEgEgEgFQgDgGAAgJQAAgHACgGQADgHAEgEQAEgFAGgCIAMgHIANgCIANgBIAMAAIAKACIgDgKIgFgJQgDgDgEgDQgEgDgGABIgIABQgFABgGADIgMAIQgHAFgIAIIgMgPQAJgIAJgFQAJgGAHgDQAIgEAGgBIALgBQAJAAAHADQAHADAFAFQAGAGADAHIAGAPIAEARIABARIgCAVIgDAXgAgEAAQgIACgGADQgGAEgDAGQgDAFACAIQABAFAEADQAEACAFABQAGAAAHgCIAMgGIAMgFIAKgGIAAgJIAAgKIgLgCIgKgBQgJAAgHACg");
	this.shape_25.setTransform(-90.3,-1.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("Ag1hOIAWgBIgBAOQAGgFAGgDIALgFIALgCQAGAAAHACQAGACAGADIALAIQAFAFAEAGQADAGACAHQACAHAAAIQgBAIgCAIQgCAHgEAFQgDAGgFAFIgLAJQgFADgGACIgMACIgLgCIgMgEQgGgDgGgFIgBBCIgSAAgAgLg6QgFACgEADIgHAIIgFAKIAAATQABAGADAFIAIAHIAJAFQAFACAFAAQAGAAAGgDQAGgCAEgEQAFgFADgGQACgGABgHQAAgHgCgHQgCgHgFgFQgEgFgGgDQgGgCgHAAQgFAAgGACg");
	this.shape_26.setTransform(-102.5,1.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_27.setTransform(-120.3,-1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAaBLIAEgTIACgQIAAgQQAAgKgDgIQgEgGgEgEQgFgFgFgCQgGgCgFAAQgEAAgGADIgLAIQgFAEgFAIIAABBIgUABIgBiWIAWgBIAAA7QAFgFAGgEIALgFIALgCQALAAAJADQAJAFAHAGQAGAIAEAKQAEAKAAANIAAAOIgBAOIgBANIgDAMg");
	this.shape_28.setTransform(-132.7,-3.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIABgSIAiAAIAAgsIATgBIgBAsIAmgBIgBASIglABIgBBUIgUAAg");
	this.shape_29.setTransform(-144.1,-3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgLgKIgiAAIAAgSIAiAAIABgsIASgBIAAAsIAmgBIgCASIgkABIgBBUIgUAAg");
	this.shape_30.setTransform(-158.8,-3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgRA1QgKgFgIgHQgIgIgFgKQgFgLAAgMQAAgGACgHQACgHAEgHQADgGAGgFQAFgGAGgEQAGgDAIgDQAHgCAHAAQAJAAAIACQAHACAHAEQAGAEAFAGQAGAFADAHIAAABIgQAJIAAAAIgGgJQgEgEgEgDQgFgDgFgBIgLgCQgHAAgHADQgHADgFAGQgFAFgDAHQgDAHAAAHQAAAIADAHQADAHAFAFQAFAGAHADQAHADAHAAQAGAAAEgCQAFgBAFgCIAIgGIAGgIIAAgBIARAKIAAAAQgEAHgFAFQgGAFgGADQgHAEgHACQgIACgIAAQgKAAgKgEg");
	this.shape_31.setTransform(-169.6,-1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_32.setTransform(-181.5,-1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AAGBVIgIgGQgEgDgDgFIgFgJQgFgMgBgOIADh7IATAAIgBAhIAAAaIgBAVIAAAQIAAAaQAAAJACAIIACAFIAEAGIAFADQADACAFAAIgCAUQgIgBgFgCg");
	this.shape_33.setTransform(-189.7,-4.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgVA0QgKgDgHgIQgHgHgDgKQgEgKAAgMQAAgLAEgLQAEgLAIgHQAHgIAKgFQAJgEALAAQAKAAAJAEQAJAEAHAHQAHAHAEAJQAFAKABALIhUALQABAHADAGQADAFAEAEQAEAEAGACQAFACAGAAIAKgCQAFgBAEgDQAFgEADgEQADgFACgGIASAEQgCAJgFAHQgFAHgHAFQgHAGgIACQgIADgIAAQgMAAgKgEgAgHglQgFABgFAEQgEADgEAGQgEAGgBAIIA6gHIgBgCQgEgKgGgFQgHgFgJAAIgIABg");
	this.shape_34.setTransform(-198.9,-1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgMBPIgPgFIgOgHIgNgJIAOgQQAIAGAHAEQAIAEAFACIANADQAHABAGgCQAGgBAEgDQAFgDADgEQACgEABgEQAAgEgBgDIgFgFQgDgDgEgBIgJgEIgJgCIgJgCIgMgDIgMgEQgGgDgFgEQgFgDgEgFQgEgGgCgHQgCgHABgKQAAgIADgGQADgGAEgFQAEgFAGgEIALgGIANgDIANgBQAJAAAJADIAJACIAIAEIAJAFIAIAHIgLARIgGgGIgHgFIgIgDIgGgDQgIgCgHAAQgIAAgHADQgHADgEAEQgFAEgDAFQgCAFAAAGQAAAFADAEQADAFAFAEQAFAEAHACQAIADAHABIAOACIANAEIAMAHQAGADADAGQAEAEACAHQACAGgBAIQgBAHgDAFQgDAFgFAEIgJAHIgLAEIgMACIgLABIgOgBg");
	this.shape_35.setTransform(-211.3,-3.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#990000").ss(3,1,1).p("A/9inMA/7AAAQBPAAA5AxQA5AxAABFIAAAAQAABGg5AxQg5AxhPAAMg/7AAAQhPAAg5gxQg5gxAAhGIAAAAQAAhFA5gxQA5gxBPAAg");
	this.shape_36.setTransform(0,-1.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF0000").s().p("A/9CoQhPAAg5gxQg5gxABhGIAAAAQgBhFA5gxQA5gxBPAAMA/7AAAQBPAAA5AxQA5AxgBBFIAAAAQABBGg5AxQg5AxhPAAg");
	this.shape_37.setTransform(0,-1.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy3, new cjs.Rectangle(-225.3,-19.9,450.7,36.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween18("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(97.2,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02,x:99.1},9).to({scaleX:1,scaleY:1,x:97.2},10).to({scaleX:1.02,scaleY:1.02,x:99.1},10).to({scaleX:1,scaleY:1,x:97.2},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-215.6,-30.2,627,63.6);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0.5},14).to({alpha:1},15).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-411.2,-38.6,822.6,74.8);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2copy3();
	this.instance.parent = this;
	this.instance.setTransform(0.9,33.1,0.964,0.964,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(-216.4,13.7,434.4,35.4), null);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.pathSets_mccopy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// props
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#23211F").ss(0.5,1,1).p("AOglMQAgAbATAXQAzAzAXA1QATAlAOA5QAGAYAJAqQhKgkgzgWQA0BnAMA/QAKA/gPB0QgFAtgBApQg7grgqgwQgQgTgSgPIABBAQACBHgFAxQgEAygLArQAAgDgBgCQgVhBgJghQgEgOgDgNQgdAugoBlQgsBqg0AqQAZhTAEhrQgSAWgbAVQhJA5gLANQgMAOgQAbQgSAcgJAMQgGAKgIAJQABgXAAgiQAAgiAAgjQgMAKgMAKQg+AxhWAVQglAKgTAFQggAJgUALQgbAPgUAbQAKggANghQAUgxAbgoQgnAKgqAUQguAPgaAeQgfAmgFBdQgEgXgNghQgDgIgBgGQgKgYgFgPQgOgqgEgtQgJAfgFAhQgGAgACAhQhCABgrgCQgRgBgNgCQh1gVhAhKQgQgUgVgkQgYgqgKgPQgWgcgcgRQgCgBgCAAQgLAhAAAqQAAAgARBgQABAHACAHQgDgEgCgEQhShmgdh3QgOASgQAZQghA1gMBVQgegKgXggQgOgVgIgZQgkgNglgvQg5hOgRhAQgCBGgCBzQhOiJgHhqQgDgdADgaQgiAogXAsQAAhEAOhFQALg5AUguQgiAQgTAJQhaArgpAfQAOigA1iCQAJgXALgVQAihGAvg4Qg+AThGA3QgVARgPANQAZgwAlgwQAug6AtgqQAtgqBGg+QArgmBHggQhugghsAUQAvgtBsAAQEPADD9BBQgpgigbgUQg6gogygTQgRgFgWgHQASgBAVABQCHADClAmQA+APBaAaQgKgagTgeQg2hOhNgwQgNgIgNgGQAHAAAKADQCEAmBrBjQAnAnASARQgEgNgEgJQgPg7gYg3QADAAABABQAuAUAEACQAfARAeAyQAPAZANAYQAXgbAbgsQArhHAWgYQAXgXAdgPQgMAbgLAjQgRArgJAfQgLAsgGApQBYg7CIgfQAMgDCGgXQgRARgaATQgjAagSAOQgZATgQATQBBgSAvgLQChgeBxAeQA2AQA5AgQgwAHgaACQgMAChUABQg7ABgkAIQgyAMgjAgQgUATgJAUQAVgDAcABQAPABBAALQAnAKBQAOQAsAJAiAPQASAGAPAJQBLAnBCBhQhXgphOgkQgagNgcgGgAE2AkQAHgHAGgFQgSgBgWAFQgnALg/AeQhHAhggAKQhSAbiNgCQiTgEg5ACQAUgGAZgFQBcgUAqgXQgsgUhCAAQhMAEgmACQggAAg/gGQhAgGggAAQgGAAgFAAQAEgJAHgFQAOgOAggJQBAgWBYgFQhMhAhzAFQgEAAgFAAQACgLAGgLQANgbAbgNQAZgKAgADQArAABaAJQAIgugTgtQARABASAKQAHACAyAiQAAgDABgxQAAgFAAgDQBaAVA0gMQApgIA9glQA6ggAigYQAwgjAXgpQgIBggpBUQBHACBQhIQADgCADgBQANAHARAKQAuAcAdgGQAZgEATgaQANgQAPgiQARgiAOgPQAUgYAagEQguApgOBBQgNBBAcA3QAIhMAog5QgeBOAjBKQAHhbBCg6QgwBCAIBTQAqhGBFguQgeBIhGBcQgjArBlgVQh5B/hAACQgfA4gkAQQglARhSgTQAWgaAngpgAM8lMQA1gKAvAK");
	this.shape.setTransform(399.1,28.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#613E1B").s().p("AhOgPQAagfAjgSQAVAJAWAOQA1AiAnAxQgxgSg3ACQhHACg8AlQAKgsAdgkg");
	this.shape_1.setTransform(475.9,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5F3C15").s().p("ABCH6IgEgOIgPgnQgOgqgEgsQgKAfgFAhQgGAgACAgQhBACgrgCIgegDQh1gWhAhKQgQgTgVgkQgYgqgKgPQgWgcgcgSIgEAAQgLAgAAArQAAAgARBgIADAOIgFgIQhShmgdh4QgOASgQAZQghA2gMBVQgegKgXggQgOgVgIgaQgkgMglgvQg5hOgRhAIgEC5QhOiJgHhqQgDgdADgbQgiAogXAtQAAhEAOhEQALg6AUgtIg1AZQhaArgpAeQAOifA1iDQAJgXALgVQBGgxBLglQAkATAuAKQAxALBaACIASABQhSBBhqAuQAMAVApAAQBnALBcggQgRAXgOAmQgJAVgPApQgXA4AGAjQg0A6gHBKQgEASACAfQAFgGAHgFQAdgeAUg8QAShFAOggQAGBzgEBCQALgVAPgMQANgNAXgKIApgVQAggRAogbQAXgQAsgjQgVAXgQAYQgkAxgTAzQgJAWgEAWQAMgJAYgQQAXgPALgJQAggcAjhCQgLBBAIBYIALBLIADgJIAohCQAZguAAhbQAVBIAXAlQAPASAeAfQAgAgAOARQAZAhASAzQANgrgQg4QgLgsgyhQQBJAbBIBhQAKAPAoBDIAEAFIAAABIAGgFQAPgOAJgiQAMglgCgxQAAgbgHg9QA8BcAmBnQA6gtAyhKQAshDAthmQAFBQgaBeQgOAtgaBFIAvgZQA4gbAZgXQArgoAShHQAMguAEhSQBZB/AyB+QAshpgphwQAfgCAaAYQAYAVAOAjQAJAUAOApIAAADQAFgZARgXQAcg2AFgRQANgwgbgcIBQAuQAEgKAGgFQARgRAYgHQAjgOArgHQgcg/gkggQgqgqhFgRQg7gOhGAHIgHAAQABgLAIgKQANgPAXgKQBQgoBUAPQANACANAEQgPgIgQgGQgngyg1giQgWgOgWgJIAKgFQAcgJANgKQAHgHAKgPQAHgKAEgEQA1gKAvAKQAgAbATAXQAzAzAXA1QATAlAOA5IAPBCQhKgkgzgWQA0BoAMA/QAKA+gPB0QgFAtgBApQg7grgqgwQgQgTgSgPIABA/QACBIgFAwQgEAygLArIgBgEQgVhCgJghIgHgaQgdAugoBlQgsBqg0AqQAZhUAEhqQgSAVgbAVQhJA5gLAOQgMAOgQAaQgSAdgJAMQgGAJgIAKQABgXAAgiIAAhFIgYATQg+AyhWAVIg4APQggAIgUALQgbAQgUAaQAKggANghQAUgwAbgpQgnALgqAUQguAPgaAdQgfAngFBdQgEgXgNgig");
	this.shape_2.setTransform(396.8,50.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#271805").s().p("AitESQiTgEg5ADQATgGAZgGQBcgUArgXQgsgThDAAIhyAFQggAAg/gFQg/gHggAAIgMAAQAFgIAHgGQAOgOAfgJQBBgXBXgFQhLhAhzAGIgKAAQADgLAFgMQAOgaAagOQAZgJAgADQAsAABaAJQAIgtgUguQARACASAJQAHADAyAhIACgzIAAgJQBaAWAzgNQAqgHA9glQA6ggAhgYQAwgkAYgpQgIBhgqBUQBHACBRhIIAFgEIAfARQAtAcAdgFQAZgFAUgaQAMgPAQgjQARghANgQQAVgYAZgDQguApgNBAQgNBBAcA4QAIhNAng5QgdBOAiBKQAHhbBDg6QgwBDAIBSQAphFBGgvQgfBJhFBaQgkArBlgUQh4B/hAACQgfA4gkAQQglARhSgTQAVgaAogoIAMgNQgSgBgVAGQgoAKg+AeQhIAhgfAKQhMAZh8AAIgXAAg");
	this.shape_3.setTransform(402,14.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#624320").s().p("AjPG1QgNgSgggfQgfgfgOgTQgXglgWhIQAABcgYAtIgpBDIgDAJIgKhMQgJhYALhCQgiBDggAdQgLAJgYAPQgYAPgLAKQADgXAJgWQAUgzAkgxQAPgYAVgXQgsAigWARQgoAbghARIgoAVQgXAKgNANQgPANgLAUQAEhCgHhzQgOAggSBFQgTA9geAdQgHAGgEAFQgDgeAEgTQAIhKAzg7QgGgiAYg4QAOgpAKgWQAOgkAQgYQhbAhhogLQgpAAgLgWQBpgtBThCIgTAAQhagDgxgKQgugKgkgUQB+hACJgiQhvguh3APQADgGAFgFQAng4BJgeQBMgfBUAHQBHAEBVAdQAyARBiAsQgehBhDgWQBJAABIAaQBGAZA7AuQACg3geguQgEgIgGgIQgpg1g9gMQDaARCyCOQAigxA1geIAEgBQAggVAigIQAVgFAWgCQgyBLgJALQAUgYAegLQAcgMAfAFQgOAkAGAkQAtg7A8gSQAAAogEAyQAOgGA3gfQAHAXARBEQAPgTAdgGIA4BQQAoAGAmASQgjASgaAeQgeAmgJAsQA7gmBJgBQA3gCAxASIAFAHQhUgOhQAnQgXAKgNAQQgIAKgCAKIAHAAQBHgGA7ANQBFARAqAqQAjAgAdA/QgsAHgiAOQgYAHgSAQQgGAGgEAKIhPguQAbAbgNAxQgFARgdA3QgQAWgFAaIgBgDQgOgpgJgWQgOgigXgVQgbgYgfACQAqBxgsBpQgzh/hYh/QgEBRgNAwQgSBGgrAoQgZAYg4AbIgvAZQAbhFANgtQAbhfgGhQQgsBngtBCQgyBLg6AtQglhng8hdQAHA+AAAbQABAwgLAmQgIAhgQAPIgFAEIgBgBIgDgEQgphDgLgPQhHhhhJgbQAyBQALAsQAPA3gMArQgSgygaghgAi3DCQCMADBTgcQAfgJBIgiQA+geAogKQAVgGASACIgMAMQgoApgVAZQBSATAlgQQAkgRAfg3QBAgCB4h/QhlAUAkgqQBFhcAfhIQhGAugpBFQgIhSAwhCQhDA6gHBaQgihKAdhNQgnA4gIBNQgcg3ANhCQANhAAugpQgZAEgVAXQgNAQgRAiQgQAigMAPQgUAbgZAEQgdAGgtgdIgfgRIgFAEQhRBIhHgCQAqhUAIhhQgYApgwAkQggAYg7AgQg9AlgqAHQgzANhagWIAAAJIgCA0QgygigHgCQgSgKgRgBQAUAtgIAuQhagJgsAAQgggEgZAKQgaAOgOAbQgFALgDALIAKAAQBzgGBLBAQhXAFhBAWQgfAKgOAOQgHAFgFAIIAMAAQAgABA/AGQA/AGAggBIBygFQBDAAAsAUQgrAWhcAVQgZAFgTAGIA5gBQA4AABbACg");
	this.shape_4.setTransform(403,22.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#A8743C").s().p("AvSC7Qg+AThGA3IgkAdQAZgvAlgwQAug6AtgqIBzhoQArglBHghQhugfhsATQAvgsBsAAQEPACD9BCQgpgigbgUQg6gpgygSIgngMQASgCAVACQCHADClAlQA+APBaAbQgKgbgTgdQg2hOhNgwQgNgJgNgGQAHABAKACQCEAnBrBjIA5A4IgIgXQgPg6gYg3IAEABIAyAWQAfAQAeAzQAPAZANAXQAXgaAbgsQArhIAWgXQAXgXAdgQQgMAcgLAjQgRArgJAfQgLAsgGAoQBYg6CIgfQAMgECGgWQgRAQgaAUIg1AoQgZASgQAUQBBgSAvgLQChgeBxAeQA2AQA5AgQgwAHgaACQgMAChUAAQg7ACgkAIQgyAMgjAfQgUATgJAUQAVgDAcABQAPABBAALQAnAKBQANQAsAKAiAPQASAGAPAJQBLAmBCBiIilhNQgagOgcgGQgvgJg1AJQgEAFgHAKQgKAPgHAHQgNAJgcAJIgKAGQgmgSgogGIg3hQQgdAGgQATQgRhEgHgXQg3AfgOAGQAEgyAAgnQg7ASgtA6QgHgkAOgjQgegFgdAMQgeAKgTAYQAIgLAyhKQgVACgWAFQgiAIggAVIgDABQg1AdgjAxQiyiNjZgRQA8AMApA1QAGAIAEAIQAfAtgDA3Qg6guhHgZQhHgZhJAAQBCAVAfBBQhjgsgygRQhUgchHgEQhUgHhMAeQhKAegnA4QgEAFgEAGQB4gPBvAuQiKAih9BAQhLAmhGAxQAihGAvg4g");
	this.shape_5.setTransform(399.1,-19.2);

	this.instance = new lib.Symbol6copy3();
	this.instance.parent = this;
	this.instance.setTransform(399.5,35.4,1.563,1.563,0,0,0,87.4,87.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#39111C").s().p("AgpBDIgCgCQgRgYAAgeQABgiAPgVQAVgXAXABQAagBAUAYQAOAXABAeQgBAegKAVIgBAAIgFAEQgbADgaAAIgggBgAAogUQAGgFABgGQAAgIgIgEQgFgGgIAAQgKAAgEAFQgGAFAAAGQgCAGAIAIQAGAFAHgCQAJADAGgHg");
	this.shape_6.setTransform(-437.7,28.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F0F0EC").s().p("Ag+BfQgZgjABgtQgBgtAZgiQAcggAkABQAiAAAZAeIAEADQAYAigDAsQACAqgSAgQAKgUABgfQgBgegPgXQgTgYgbABQgXgBgUAYQgQAUgBAiQAAAfASAXIACACQgUAAgVgBgAAtANQgHABgHgEQgHgIACgFQAAgHAFgEQAFgFAKAAQAIAAAFAFQAHAEAAAIQAAAFgGAGQgEAEgGAAIgFAAg");
	this.shape_7.setTransform(-439.7,25.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#579320").s().p("ABKBNQheAKhagDQAngGAdgiQAVgZAJgfIAGABQASgBAPgKQAHgFAEgGQgKAhgWAZQgbAignAFQBZAEBegKQgNARgOAQQgWAJgZAHQANgOAMgQgAhZAEQgUgCgNAEQBDgcAXhMIAAgBIAAAAQApAEAYgLQgmBBhEAdQANgEAUACIAcACQgEAGgGAEQgPAIgSABIgigDg");
	this.shape_8.setTransform(-512,60.2,1.85,1.85,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#64AA24").s().p("AjVDmQgzAAgJgcQgOgfA2gkQgPgBgFgRQgFgQAIgOQALgRAmgOQgHgEgEgHIgFgIQgEgKABgKQAbAJAeACQAVAAAWgDQAOgCANgDQAmgKAfgVQgXBMhEAdQANgEAVACIAiADQASgBAPgKQAGgEAEgFIgcgDQgUgCgNAEQBDgdAnhAQgYALgpgDQALgIAKgJQAggfAjhDQArhQARgXQAXBfgSBkQBBgvAXhJQAFA0gLAzQgMAzgaArQAhgFAcgVQAcgUAPgeQACApgWAqQgUAngjAfQgnAig7AYQAOgPANgSQhfAKhZgDQAngGAcgiQAWgZAJghQgEAGgHAFQgOAJgTABIgFAAQgJAggVAZQgdAignAFQBaAEBegKQgMAQgNAOIgNADQhtAgh5AAIgbAAg");
	this.shape_9.setTransform(-510.5,44,1.85,1.85,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#579320").s().p("ABTE6QgQgcAGgcQADgOALgQIAQgaQAGgPgBgQQAAgSgLgJQgIgEgQgEQgQgDgHgGQgSgLABgbQABgWANgZQgdgNgiACQgkACgaAVQACgoAWgkQhlAHhRgSQgigJgOgMQgKgMgBgNQgCgNAKgLIDEg6QgKAKADAPQAAANAKAKQAOANAiAJQBQASBlgHQgWAlgCAnQAbgTAjgDQAjgDAdAOQgMAZgCAVQgCAcATAKQAHAFAQAEQAQADAIAEIAEAFQhEAyg1BDQhCBUgMANQgGAIgIAFIgBgDgAjfk8QCYBTCJgMIADAAIigBUQhBgshDhvg");
	this.shape_10.setTransform(-575.3,99);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#76AF41").s().p("AgOG7QimgCiThCQhdgrhXhFIgIgIQjTivAYhwIABgFIABgFIAGgQQAoBrBgBYQBlBbCCAnQDzBIEZhmQCFguBphRQBzhVBChvQA0hYAQhjQAPhQgLhYIAAgBQAJAeAEAgQAFAdAAAgIAAAZIgBAbQgCAbgEAcQgdDSilC9IgWAXQjXDpkTAAIgHAAg");
	this.shape_11.setTransform(-478.3,72.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E2BB0E").s().p("Ag7gaQAVAFAYADQAmADAqgDQgsAZgxAMIgmAIQAFgbABgag");
	this.shape_12.setTransform(-401.8,48.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FDD422").s().p("AgNB9QgZgCgUgGIAAgbIAAgZQAPAOAiAMQAzAQA4gCIgfAVIgkACQgXAAgVgDgAhLg5QgKgkgRgiIASAOQAcARAnAJQAtAMBLgJQgyA0hCAWQgXAJgbAFQgEgfgIgeg");
	this.shape_13.setTransform(-401.8,33.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#87C152").s().p("AlGH3QiCgnhlhbQhfhYgphsIAGgPQAMgbARgYQAog6BDgkQBwg8CAjMIARgYIAdgmQBoh1C6hGIAUgHQDhhMDZB6IATANQBrBDAyBlQARAhAKAlIAAABQAKBYgOBQQgQBig1BYQhBBwhzBVQhqBRiFAuQilA8iXAAQhsAAhkgeg");
	this.shape_14.setTransform(-478.4,48.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#599C1C").s().p("AihCqQgXhMALheQACAFADAGQAHAXAOASQgLBQANBRQgKgVgGgWgAhqCIQgUgVgJgcQgHgQgCgPIAFAFQAXAZBDArQgTg1gEhFQgBgXABggIAIAKQgCgeABgfQAGAXAPAXIAIALQgEBLACAxQAEBGASA2QhDgsgXgagAgKAVQgWgdgCgiQASAVAYANQgbhbAOhhQAGAxAZArQgKBYAZBUQgggSgTgdgAA+gWQgUgigJgkQAcArApAbQgmhfAGhfQAKAKAOAVQAOAZAHAHQANAPASAIQAJAEAIAAQAGAjAIAlQgRgKgMgOIgWggQgNgUgJgJQgHBfAnBdQgtgdgdgug");
	this.shape_15.setTransform(-497.6,-13.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#64AA24").s().p("AlXPoQgSgQAJgvQAUhvAMg3QADgQgDgOQgIgRgUgBQgUAAgMAOQgLAHgLASIgTAeQgMAOgRAFQgSAFgOgIQgSgOAHgbQAHgZAVgTQAzg6gdgcQgKgNgUAAQgJAAgbAGQhCAKg5gcQgXgMgHgRQgIgXAYgTQAUgRAfgEQBRgLACgnQAGgshegVQg2gPgmgcQgagOADgYQAFgRAdgIQA9gNBlAhQB4AmAtABQAnACA0gKIAigJIArgOQAzgPAcgJQArgJAmABIALABIgGAOIgFAQIgCAFIgBAGQgYBwDSCwIAJAIQg/AZg7AlIgsAfIgDgEQgIgFgPgDQgRgEgHgFQgSgKACgcQACgVALgaQgdgOgiADQgkADgbAUQADgoAWglQhlAHhRgSQgjgIgOgNQgJgLgBgNQgCgPAKgJIChhVIgDAAQiLAMiXhTQBCBvBCAtIjEA6QgKAKABANQACANAJAMQAOANAjAIQBRASBlgGQgWAlgDAnQAbgVAkgCQAigCAeANQgNAagBAVQgCAbASAMQAHAFARAEQAPADAIAEQALAJAAATQACAPgHAPIgQAbQgLAPgCAOQgHAcAQAcIACAEQgqAmgrANQgKADgJAAQgQAAgMgIgAFOkSQgNhRALhQQgOgSgHgXQgDgGgCgFIADgSQAKhDAkh2QADBTAxBHQgBAgABAYQAEBFATA1QhDgrgXgZIgFgFQACAPAHAQQAJAcAUAVQAXAaBDAsQgSg2gEhGQgCgxAEhMIgIgLQgPgXgGgXQAEhEAShEQAbhjA4hTQAIBgAkBPQAOicBhh5Qg2CtASC2IAFAqQgIAAgJgEQgSgIgNgPQgHgHgOgZQgOgVgKgKQgGBfAmBfQgpgbgcgrQAJAkAUAiQAdAvAtAdQgnheAHhfQAJAJANAUIAWAgQAMAOARAKQAWBYAmBRIgUAHQi7BGhoB0Qgugrgag3gAHVnSQAUAdAgASQgZhVAKhYQgZgrgHgxQgOBhAcBbQgZgNgSgVQACAiAWAeg");
	this.shape_16.setTransform(-545.6,35.3);

	this.instance_1 = new lib.Symbol6copy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-504.4,35.4,1.563,1.563,0,0,0,87.3,87.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.instance},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// question
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("EAm4gg0IAAHmMgtWAAAIAAFiMg9RAAAIAAHQEhGhATtIAANIMBPzAAAIAAliMA9QAAAIAAnQ");
	this.shape_17.setTransform(-54.7,5.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_17).wait(1));

	// answer -val
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(255,255,255,0.02)").s().p("Eg/PARzMAAAgjlMB+fAAAMAAAAjlg");
	this.shape_18.setTransform(-29.8,220.3,1.28,0.836);

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

	// wrong - val
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("rgba(255,255,255,0.02)").s().p("Eg/PARzMAAAgjlMB+fAAAMAAAAjlg");
	this.shape_19.setTransform(-29.8,-119.7,1.28,0.873);

	this.timeline.addTween(cjs.Tween.get(this.shape_19).wait(1));

	// Layer_3
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("rgba(255,255,255,0.247)").ss(10,1,1).p("Eg7lghHMB3LAAAQB8AABYBYQBYBYAAB8MAAAA43QAAB7hYBZQhYBYh8AAMh3LAAAQh8AAhYhYQhYhZAAh7MAAAg43QAAh8BYhYQBYhYB8AAg");
	this.shape_20.setTransform(-42.9,48.7,1.527,1.32);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(0,102,204,0.298)").s().p("Eg7lAhHQh8AAhYhXQhYhZAAh8MAAAg41QAAh8BYhZQBYhXB8gBMB3LAAAQB8ABBYBXQBYBZAAB8MAAAA41QAAB8hYBZQhYBXh8AAg");
	this.shape_21.setTransform(-42.9,48.7,1.527,1.32);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20}]}).wait(1));

	// dummybg
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("rgba(255,255,255,0.247)").ss(10,1,1).p("Eha/grtMC1/AAAQC9AACGB0QCHB0AACkMAAABLDQAACjiHB1QiGB0i9AAMi1/AAAQi9AAiHh0QiGh1AAijMAAAhLDQAAikCGh0QCHh0C9AAg");
	this.shape_22.setTransform(-42.9,48.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(1));

}).prototype = getMCSymbolPrototype(lib.pathSets_mccopy7, new cjs.Rectangle(-676,-236,1266.4,569.5), null);


(lib.pathSets_mccopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.question = new lib.pathSets_mccopy7();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(16.1,17.6,0.995,0.995,0,0,0,8.1,17.8);

	this.timeline.addTween(cjs.Tween.get(this.question).wait(1));

}).prototype = getMCSymbolPrototype(lib.pathSets_mccopy5, new cjs.Rectangle(-664.5,-234.9,1259.9,566.6), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.pathSets_mccopy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.question = new lib.pathSets_mccopy7();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(16.1,17.6,0.995,0.995,0,0,0,8.1,17.8);

	this.timeline.addTween(cjs.Tween.get(this.question).wait(1));

}).prototype = getMCSymbolPrototype(lib.pathSets_mccopy10, new cjs.Rectangle(-664.5,-234.9,1259.9,566.6), null);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(618.8,587.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(231).to({_off:false},0).wait(20).to({startPosition:12},0).to({alpha:0,startPosition:0},8).wait(6));

	// Layer_6
	this.instance_1 = new lib.Symbol1copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(687.8,599.6,1,1,-21.5,0,0,190.7,64.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(199).to({_off:false},0).to({_off:true},35).wait(31));

	// arrow
	this.instance_2 = new lib.Symbol1copy2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(589.5,508.8,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(169).to({_off:false},0).to({_off:true},35).wait(61));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(10,1,1).p("Eg/egFDIAAKHMBH1AAAIAAk+MA3IAAAIAAkC");
	this.shape.setTransform(626.6,551.7);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(216).to({_off:false},0).to({_off:true},29).wait(20));

	// Layer_2
	this.instance_3 = new lib.Symbol1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(626.6,552.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(102).to({_off:false},0).to({_off:true},58).wait(105));

	// Layer_5
	this.instance_4 = new lib.Symbol7();
	this.instance_4.parent = this;
	this.instance_4.setTransform(641.4,104.3,0.98,0.98,0,0,0,0.1,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(55).to({_off:false},0).to({_off:true},105).wait(105));

	// Layer_1
	this.question = new lib.pathSets_mccopy5();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(675.2,405.9,0.904,0.904,0,0,0,8.1,17.7);
	this.question.alpha = 0;
	this.question._off = true;

	this.question_1 = new lib.pathSets_mccopy10();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(675.2,405.9,0.904,0.904,0,0,0,8.1,17.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.question}]},38).to({state:[{t:this.question_1}]},4).to({state:[{t:this.question_1}]},118).to({state:[]},85).wait(20));
	this.timeline.addTween(cjs.Tween.get(this.question).wait(38).to({_off:false},0).to({_off:true,alpha:1},4).wait(223));

	// intro
	this.instance_5 = new lib.Symbol2copy();
	this.instance_5.parent = this;
	this.instance_5.setTransform(640,47.7,1.922,1.922);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(17).to({_off:false},0).to({alpha:1},5).wait(219).to({alpha:0},4).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(593.8,320.8,1370,780);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;