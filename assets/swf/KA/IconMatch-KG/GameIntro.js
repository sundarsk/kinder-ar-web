(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000066").s().p("AhtA6IgCg6IgBgtIgBgiIgCgyIA2gBIABBEQAHgOAKgLQAKgMALgJQAMgKAMgFQANgGAOgBQASgBANAEQANAFAKAHQAJAIAGAKQAGAKAEALIAFAVIACAUQABAnAAApIgCBVIgwgCIADhLQACgmgCglIAAgLIgDgPQgCgHgEgHQgDgIgGgFQgFgGgIgDQgJgCgKABQgTADgSAYQgTAZgWAtIACA8IABAiIAAASIgyAHIgDhJg");
	this.shape.setTransform(352.1,4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000066").s().p("AgwB/QgXgKgRgSQgRgRgKgaQgJgZAAgfQAAgeAJgZQAKgaARgSQARgSAXgKQAXgJAZAAQAZAAAXAKQAXAKARASQASATAJAZQAKAZAAAdQAAAegKAYQgJAagSASQgRATgXAKQgXAKgZAAQgZABgXgLgAgihXQgOAJgJAOQgJANgEARQgFARAAARQAAAQAFASQAEAQAKAOQAJAOAPAJQAOAIASAAQATAAAPgIQAOgJAKgOQAJgOAEgQQAFgSAAgQQAAgRgEgRQgFgRgJgNQgJgOgOgJQgPgJgUAAQgTAAgPAJg");
	this.shape_1.setTransform(323,4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AgqB+QgZgKgTgTQgUgSgLgYQgMgZAAgdQAAgRAFgRQAFgRAJgQQAIgPANgNQAMgNAPgIQAQgKARgFQASgGATAAQAUABATAEQASAFAQAKQAPAJANANQAMAOAIAQIABACIgnAXIAAgCQgHgMgIgIQgJgKgLgHQgKgHgNgDQgMgEgNAAQgSABgRAGQgRAIgMAMQgNANgHASQgIAQAAATQAAASAIARQAHARANANQAMANARAGQARAIASAAQAMAAAMgDQAMgEAKgFQAKgHAJgIQAJgJAGgKIABgBIAoAWIAAACQgJAPgNAMQgNAMgQAKQgQAIgSAEQgSAGgSAAQgaAAgYgLg");
	this.shape_2.setTransform(294.2,4.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000066").s().p("AgdCwIABg8IABhJIAChkIAwgCIgCA9IgBAyIAAAqIgCAfIAAAzgAgMhvQgGgDgEgEQgFgEgDgGQgCgHAAgGQAAgHACgGQADgHAFgEQAEgFAGgCQAGgDAGAAQAHAAAGADQAGACAEAFQAFAEADAHQADAGgBAHQABAGgDAHQgDAGgFAEQgEAEgGADQgGADgHAAQgGAAgGgDg");
	this.shape_3.setTransform(274,-1.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000066").s().p("AgzDDIgVgHQgMgFgKgIQgKgIgJgKQgJgLgFgOIAtgUQAEAKAHAHQAGAIAHAEQAIAFAHADIAPAFQARAEASgDQAQgDANgHQAMgHAIgIQAIgIAGgJIAIgSQADgJABgGIABgLIAAgfQgOAOgPAHQgPAHgNADQgPAEgNABQgcAAgXgKQgXgJgSgSQgRgSgKgYQgKgZAAggQAAggALgZQALgZASgRQASgRAXgJQAWgJAXAAQAPACAQAFQAOAFAQAIQAQAIANAOIABgvIAuACIgCEMQAAAPgEAPQgEAPgHAPQgHAOgLAMQgLANgOAJQgOAKgSAGQgRAHgVABIgGABQgZAAgWgHgAgjiOQgOAHgKAMQgLAMgFARQgGARAAATQAAATAGAQQAGAQAKALQAKALAPAHQAOAHASAAQAOAAAOgGQAPgFAMgKQAMgJAIgNQAIgOADgQIAAgeQgDgRgIgOQgIgNgNgKQgMgKgOgFQgPgGgPAAQgRAAgOAHg");
	this.shape_4.setTransform(238.4,9.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000066").s().p("AhtA6IgCg6IgBgtIgBgiIgCgyIA2gBIABBEQAHgOAKgLQAKgMALgJQAMgKAMgFQANgGAOgBQASgBANAEQANAFAKAHQAJAIAGAKQAGAKAEALIAFAVIACAUQABAnAAApIgCBVIgwgCIADhLQACgmgCglIAAgLIgDgPQgCgHgEgHQgDgIgGgFQgFgGgIgDQgJgCgKABQgTADgSAYQgTAZgWAtIACA8IABAiIAAASIgyAHIgDhJg");
	this.shape_5.setTransform(208.5,4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000066").s().p("AgdCwIABg8IABhJIAChkIAvgCIgBA9IgBAyIAAAqIgBAfIgBAzgAgMhvQgGgDgEgEQgFgEgDgGQgDgHABgGQgBgHADgGQADgHAFgEQAEgFAGgCQAGgDAGAAQAHAAAGADQAGACAEAFQAFAEADAHQADAGgBAHQABAGgDAHQgDAGgFAEQgEAEgGADQgGADgHAAQgGAAgGgDg");
	this.shape_6.setTransform(188.1,-1.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000066").s().p("AA/CyQAHgWADgWQACgVABgRQACgUgBgRQgBgagIgRQgHgRgLgKQgKgMgNgFQgOgFgMAAQgNABgNAHQgMAFgOAMQgNALgMAWIgBCbIguABIgDloIA2gCIgCCPQANgOAPgJQAOgIANgEQAOgFANgCQAaAAAWAKQAWAKAPARQAPARAKAaQAJAYAAAfIAAAiIgBAjIgEAfQgCAPgEAMg");
	this.shape_7.setTransform(166.4,-1.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000066").s().p("AgqB+QgZgKgTgTQgUgSgLgYQgMgZAAgdQAAgRAFgRQAFgRAJgQQAIgPANgNQAMgNAPgIQAQgKARgFQASgGATAAQAUABATAEQASAFAQAKQAPAJANANQAMAOAIAQIABACIgnAXIAAgCQgHgMgIgIQgJgKgLgHQgKgHgNgDQgMgEgNAAQgSABgRAGQgRAIgMAMQgNANgHASQgIAQAAATQAAASAIARQAHARANANQAMANARAGQARAIASAAQAMAAAMgDQAMgEAKgFQAKgHAJgIQAJgJAGgKIABgBIAoAWIAAACQgJAPgNAMQgNAMgQAKQgQAIgSAEQgSAGgSAAQgaAAgYgLg");
	this.shape_8.setTransform(135.9,4.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000066").s().p("AgcgaIhRACIABgsIBRgCIAChnIAugDIgCBpIBbgDIgEArIhYADIgBDLIgxABg");
	this.shape_9.setTransform(110,-0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000066").s().p("ABJCGIADgiQgcATgcAIQgbAIgXAAQgRAAgQgEQgQgFgMgIQgMgKgHgNQgHgOgBgTQABgTAFgPQAGgPAJgMQAKgKAOgIQANgJAPgFQAPgFASgCQAQgDAQAAIAdABIAXADIgHgXQgEgLgIgJQgGgJgLgGQgKgFgNAAQgKAAgLACQgLADgOAHQgOAHgRAMQgRANgSATIgdgjQAWgVAWgNQAUgNASgIQASgIAQgCQAOgDANAAQAVAAARAHQARAIANAMQAMANAKARQAIASAFATQAHAUACAVQACAVAAAVQAAAWgDAaQgCAagGAegAgKgBQgUAEgPAKQgOAJgGAOQgIANAEASQAEAOAJAFQAKAGANAAQANAAARgFQAOgEAPgHIAegOIAYgOIAAgXQABgLgCgNQgLgCgNgCQgNgDgNAAQgUAAgTAFg");
	this.shape_10.setTransform(83,3.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000066").s().p("ABwA4IAAgzIgBglQgBgTgFgJQgGgKgIAAQgGAAgGADIgMAKQgGAGgGAHIgLAOIgKAPIgIAMIABAWIABAcIABAkIAAAsIgtACIgBhJIgBgzIgDglQAAgTgGgJQgFgKgJAAQgGAAgHAEQgGAEgGAGIgNAPIgMAQIgKAQIgJAMIACB8IguACIgHj7IAxgGIABBFIAQgTQAIgLAKgHQAKgJALgFQALgGAOABQAKgBAJAEQAJADAHAGQAHAHAFAKQAGALABAOIAPgTQAIgKAKgHQAJgIALgGQALgEAOAAQAKgBAKAEQAKADAHAIQAIAHAFAMQAFAMAAAPIADArIACA4IABBTIgxACIgBhJg");
	this.shape_11.setTransform(49.2,3.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000066").s().p("AgcgaIhRACIABgsIBRgCIAChnIAugDIgCBpIBbgDIgEArIhYADIgBDLIgxABg");
	this.shape_12.setTransform(6,-0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000066").s().p("AgqB+QgZgKgTgTQgUgSgLgYQgMgZAAgdQAAgRAFgRQAFgRAJgQQAIgPANgNQAMgNAPgIQAQgKARgFQASgGATAAQAUABATAEQASAFAQAKQAPAJANANQAMAOAIAQIABACIgnAXIAAgCQgHgMgIgIQgJgKgLgHQgKgHgNgDQgMgEgNAAQgSABgRAGQgRAIgMAMQgNANgHASQgIAQAAATQAAASAIARQAHARANANQAMANARAGQARAIASAAQAMAAAMgDQAMgEAKgFQAKgHAJgIQAJgJAGgKIABgBIAoAWIAAACQgJAPgNAMQgNAMgQAKQgQAIgSAEQgSAGgSAAQgaAAgYgLg");
	this.shape_13.setTransform(-19.9,4.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000066").s().p("ABJCGIADgiQgcATgcAIQgaAIgZAAQgQAAgQgEQgQgFgMgIQgMgKgHgNQgIgOABgTQgBgTAGgPQAFgPAKgMQAKgKAOgIQANgJAQgFQAPgFAQgCQARgDAQAAIAcABIAZADIgIgXQgFgLgGgJQgIgJgJgGQgLgFgOAAQgIAAgLACQgMADgOAHQgOAHgRAMQgRANgSATIgdgjQAXgVAUgNQAVgNASgIQASgIAQgCQAPgDALAAQAXAAAQAHQARAIANAMQAMANAJARQAJASAFATQAGAUADAVQACAVAAAVQAAAWgCAaQgDAagGAegAgKgBQgUAEgOAKQgOAJgIAOQgHANAEASQADAOAKAFQAKAGANAAQAOAAAPgFQAPgEAQgHIAdgOIAYgOIAAgXQAAgLgBgNQgMgCgMgCQgNgDgNAAQgUAAgTAFg");
	this.shape_14.setTransform(-50.1,3.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000066").s().p("AACAkIhFBaIgygNIBahxIhdhyIAxgLIBIBZIBHhaIAsARIhWBtIBbBuIgsARg");
	this.shape_15.setTransform(-77.4,3.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000066").s().p("Ag0B+QgYgKgQgRQgRgRgIgYQgJgYAAgeQAAgcAKgaQAKgZARgTQASgTAXgKQAXgLAbAAQAYAAAVAKQAWAJARARQAQARALAWQALAXADAaIjKAeQABAQAHANQAHANAKAKQAKAJAOAFQANAFAPAAQAMAAAMgFQAMgDALgIQAKgHAIgLQAHgLAEgPIAtAJQgHAWgMARQgMARgQANQgPANgTAGQgUAHgUAAQgeAAgYgJgAgThaQgMADgLAIQgLAJgIANQgJAOgEAUICMgRIgBgEQgJgXgQgNQgPgOgXAAQgJAAgMAEg");
	this.shape_16.setTransform(-104.2,4.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(2,1,1).p("EgkzgFiMBJnAAAQBrAABMBMQBNBMAABsIAAC9QAABshNBMQhMBMhrAAMhJnAAAQhrAAhNhMQhMhMAAhsIAAi9QAAhsBMhMQBNhMBrAAg");
	this.shape_17.setTransform(128.8,0.5,0.98,0.98);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("EgkzAFjQhrAAhNhMQhMhMAAhsIAAi9QAAhsBMhMQBNhMBrAAMBJnAAAQBrAABMBMQBNBMAABsIAAC9QAABshNBMQhMBMhrAAg");
	this.shape_18.setTransform(128.8,0.5,0.98,0.98);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128.5,-39.7,514.7,76);


(lib.Option1_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AgJBaQgHAAgEgFQgEgEgBgHQABgGAEgFQAEgEAHAAIAVAAQAHAAAEAEQAEAFABAGQgBAHgEAEQgEAFgHAAgABEAiIghgMQgRgFgOgBIgFAAIABAAIgDAAQgOABgRAFIghAMQhGAbhGgNQgkgHgfgSQgbgPgTgVQATAEATAAQAggBAcgNQAPgHAigYQAegVAUgHQAYgKAcAEQAbAEATARQATAPAGAXQAHgXATgPQATgRAcgEQAbgEAZAKQATAHAeAVQAiAYAPAHQAcANAgABQATAAASgEQgSAVgbAPQgfASgkAHQgXAFgXAAQgvAAgvgTg");
	this.shape.setTransform(-1.9,36.3,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_1.setTransform(-2.3,-21.8,2.664,2.664);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF00").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy4, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AgJBaQgHAAgEgFQgEgEgBgHQABgGAEgFQAEgEAHAAIAVAAQAHAAAEAEQAEAFABAGQgBAHgEAEQgEAFgHAAgABEAiIghgMQgRgFgOgBIgFAAIABAAIgDAAQgOABgRAFIghAMQhGAbhGgNQgkgHgfgSQgbgPgTgVQATAEATAAQAggBAcgNQAPgHAigYQAegVAUgHQAYgKAcAEQAbAEATARQATAPAGAXQAHgXATgPQATgRAcgEQAbgEAZAKQATAHAeAVQAiAYAPAHQAcANAgABQATAAASgEQgSAVgbAPQgfASgkAHQgXAFgXAAQgvAAgvgTg");
	this.shape.setTransform(-1.9,36.3,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_1.setTransform(-2.3,-21.8,2.664,2.664);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF00").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy2, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAAByQhTAAg8g8Qg7g6AAhVIAAgDQAAgJAGgGQAGgGAIABIFtAAQAIgBAGAGQAGAGAAAJIAAADQAABVg7A6Qg8A7hUABIAAAAg");
	this.shape.setTransform(-3.4,33,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACBAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNgAjCAhIgBAAQgNgOAAgTQAAgTAOgNQAOgOATAAQATAAAOAOQAOANAAATQAAATgOAOIAAAAQgOANgTAAQgTAAgOgNg");
	this.shape_1.setTransform(-1.9,-26.2,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF00").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.questiontext_mccopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy3, null, null);


(lib.questiontext_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000066").s().p("AhDAkIgCgkIgBgcIgBgVIgBgfIAigBIABAqQAEgIAGgHQAHgIAHgGQAHgFAHgEQAIgDAJgBQAMgBAHADQAJADAGAEQAGAFADAGQAEAHADAHIACANIACAMIAAAyIgBA1IgegBIACgvQACgYgCgXIAAgHIgCgIQgBgFgDgFQgBgEgEgEQgEgDgEgCQgFgCgHABQgMACgLAPQgMAPgOAcIABAmIABAVIAAAMIgfAEIgBgug");
	this.shape.setTransform(226.2,26);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000066").s().p("AgeBQQgOgGgKgLQgMgMgGgQQgFgPgBgUQABgSAFgQQAGgQAMgLQAKgMAOgGQAPgGAPAAQAQAAAOAGQAOAHALALQALAMAGAQQAGAPABASQgBATgGAPQgGAQgLAMQgLALgOAHQgOAGgQAAQgPAAgPgGgAgUg2QgKAGgFAIQgGAJgDAKQgCALAAAKQAAAKACALQADAKAGAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJACgKQADgLAAgKQAAgKgCgLQgDgKgGgJQgFgIgKgGQgIgFgNgBQgMABgIAFg");
	this.shape_1.setTransform(208,26);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000066").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_2.setTransform(190,26);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000066").s().p("AgSBuIABglIABguIAAg+IAegBIgBAlIgBAgIAAAaIgBATIAAAggAgHhFIgHgEQgDgDgBgEQgCgEAAgEQAAgEACgEQABgEADgDQADgDAEgBQAEgCADAAQAEAAAEACQAEABADADIAEAHIACAIQAAAEgCAEIgEAHIgHAEIgIACQgDAAgEgCg");
	this.shape_3.setTransform(177.3,22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000066").s().p("AggB6IgNgFIgOgIQgGgFgFgGQgFgHgEgJIAcgMQADAGAEAFQADAEAGADIAJAFIAJADQAKACALgBQALgCAHgEQAIgFAFgFQAGgFADgGIAFgLIACgKIABgGIAAgTQgJAIgJAEQgKAFgHACQgKACgIABQgRAAgPgGQgPgGgLgLQgKgLgHgPQgFgQAAgUQAAgUAGgPQAHgQALgLQAMgKAOgGQAOgGAOAAIAUAFIASAIQAKAFAJAJIAAgdIAdAAIgBCoQgBAJgCAKQgCAJgFAJQgEAJgIAIQgGAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgPgEgAgWhZQgIAFgGAHQgHAIgEAKQgEALAAAMQAAAMAEAKQAEAKAHAGQAGAHAJAEQAJAFALAAQAJAAAJgEQAIgDAIgGQAHgFAGgJQAEgIADgKIAAgTQgCgLgFgIQgGgJgHgGQgIgGgJgEQgJgDgJAAQgKAAgKAEg");
	this.shape_4.setTransform(155,29.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000066").s().p("AhEAkIgBgkIgBgcIgBgVIAAgfIAhgBIABAqQAEgIAHgHQAFgIAIgGQAHgFAHgEQAJgDAIgBQAMgBAHADQAJADAGAEQAFAFAFAGQADAHACAHIAEANIABAMIABAyIgBA1IgfgBIADgvQAAgYgBgXIAAgHIgCgIQgBgFgDgFQgCgEgDgEQgDgDgFgCQgFgCgIABQgLACgLAPQgMAPgNAcIABAmIAAAVIAAAMIgfAEIgCgug");
	this.shape_5.setTransform(136.3,26);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000066").s().p("AgSBuIABglIABguIAAg+IAegBIAAAlIgBAgIgBAaIAAATIAAAggAgHhFIgGgEQgEgDgBgEQgCgEAAgEQAAgEACgEQABgEAEgDQACgDAEgBQADgCAEAAQAEAAAEACQAEABADADIAEAHIACAIQAAAEgCAEIgEAHIgHAEIgIACQgEAAgDgCg");
	this.shape_6.setTransform(123.6,22.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000066").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_7.setTransform(109.9,22.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000066").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_8.setTransform(90.9,26);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000066").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgBIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_9.setTransform(74.7,23);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000066").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_10.setTransform(57.7,25.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000066").s().p("ABGAjIAAggIAAgWQgBgNgDgFQgEgHgFABQgEAAgEABIgHAHIgHAIIgHAJIgGAJIgFAIIAAANIABARIAAAXIAAAbIgbACIgBguIgBggIgBgWQgBgNgDgFQgEgHgFABQgEgBgEADIgIAHIgIAJIgHAKIgHAJIgFAHIABBOIgdACIgEidIAfgDIAAAqIAKgMIALgLQAGgFAIgEQAHgDAIAAQAGAAAGACQAGACADAEQAFAEADAHQAEAGAAAIIAKgLIALgLQAGgEAHgEQAHgDAIAAQAHAAAGACQAGADAFAEQAEAFADAHQAEAHAAAKIABAbIACAjIAAAzIgeACIgBgug");
	this.shape_11.setTransform(36.6,25.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000066").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgBIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_12.setTransform(9.6,23);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000066").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_13.setTransform(-6.6,26);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000066").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_14.setTransform(-25.5,25.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000066").s().p("AABAXIgrA4IgfgJIA4hGIg5hHIAegHIAsA4IAsg5IAcALIg1BEIA5BFIgbAKg");
	this.shape_15.setTransform(-42.6,25.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000066").s().p("AggBPQgPgHgLgKQgKgKgFgQQgGgPABgTQgBgRAHgQQAGgQALgMQALgLAOgHQAPgHAQABQAPAAAOAFQANAHAKAKQALAKAHAPQAGANACARIh+ASQABALAFAIQAEAIAGAGQAGAGAJADQAIACAJAAQAIABAHgDQAIgDAHgEQAGgFAFgGQAFgIACgJIAcAHQgEANgHAKQgIALgKAIQgKAIgMAEQgMAFgNgBQgSAAgPgFgAgMg4QgGACgHAFQgIAFgFAJQgGAJgCAMIBXgLIgBgCQgFgPgKgIQgJgJgPAAQgFAAgIADg");
	this.shape_16.setTransform(-59.4,26.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000066").s().p("AggBPQgPgHgKgKQgKgKgGgQQgFgPgBgTQABgRAGgQQAGgQALgMQALgLAPgHQAOgHAQABQAQAAANAFQANAHAKAKQALAKAHAPQAHANACARIh+ASQABALADAIQAFAIAGAGQAGAGAJADQAIACAJAAQAIABAIgDQAHgDAGgEQAHgFAFgGQAFgIABgJIAdAHQgEANgIAKQgHALgKAIQgKAIgMAEQgMAFgNgBQgTAAgOgFgAgLg4QgHACgIAFQgGAFgGAJQgFAJgCAMIBXgLIgBgCQgGgPgJgIQgLgJgOAAQgFAAgHADg");
	this.shape_17.setTransform(-85,26.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000066").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_18.setTransform(-103.7,22.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000066").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgBIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_19.setTransform(-120.7,23);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000066").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgBIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_20.setTransform(-142.8,23);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000066").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_21.setTransform(-159,26);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000066").s().p("AggBPQgPgHgLgKQgJgKgGgQQgFgPgBgTQABgRAGgQQAGgQALgMQALgLAPgHQAOgHAQABQAQAAANAFQANAHALAKQAKAKAHAPQAHANACARIh+ASQABALADAIQAFAIAGAGQAGAGAJADQAIACAJAAQAIABAIgDQAHgDAGgEQAHgFAFgGQAEgIACgJIAdAHQgEANgIAKQgHALgKAIQgKAIgMAEQgMAFgNgBQgTAAgOgFgAgLg4QgHACgIAFQgGAFgGAJQgFAJgCAMIBXgLIgCgCQgFgPgJgIQgKgJgPAAQgFAAgHADg");
	this.shape_22.setTransform(-176.9,26.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000066").s().p("AAJB/QgIgDgFgFQgGgGgEgHQgFgGgDgHQgHgRgBgVIAEi5IAdAAIgBAwIgBAnIgBAgIAAAYIgBAoQABANACALIAEAJIAGAIQADADAFADQAFACAGAAIgEAdQgKAAgIgEg");
	this.shape_23.setTransform(-189.2,20.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000066").s().p("AggBPQgPgHgKgKQgLgKgFgQQgGgPABgTQgBgRAHgQQAGgQALgMQALgLAOgHQAPgHAQABQAPAAAOAFQANAHAKAKQALAKAHAPQAGANACARIh+ASQABALAFAIQAEAIAGAGQAGAGAJADQAIACAJAAQAIABAHgDQAIgDAHgEQAGgFAFgGQAFgIACgJIAcAHQgEANgHAKQgIALgKAIQgKAIgMAEQgMAFgNgBQgSAAgPgFgAgMg4QgHACgHAFQgGAFgGAJQgGAJgCAMIBXgLIAAgCQgGgPgKgIQgKgJgOAAQgFAAgIADg");
	this.shape_24.setTransform(-203,26.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000066").s().p("AgTB2QgLgCgKgFQgLgEgKgGIgVgNIAWgaQALALALAFQALAGAJADQALAEAIABQALAAAJgCQAJgCAHgEQAHgEAEgGQAEgGABgGQAAgGgCgEQgDgFgEgDQgFgEgGgDIgNgFIgOgEIgOgDIgRgEIgSgGQgJgEgIgGQgIgFgGgIQgGgIgDgLQgDgLABgPQABgLAEgKQAEgJAHgIQAGgHAIgFQAIgGAKgDQAJgEAKgBQAKgCAJAAQAOABAOADIAMAEIANAGQAHADAGAEIANAKIgRAaIgKgJIgKgHIgLgFIgKgEQgLgDgLAAQgMAAgLAEQgKAEgHAGQgHAHgEAIQgEAHAAAIQAAAHAFAHQAEAHAIAGQAIAGALAEQAKAEAMACIAVADQALACAJAEQAKAEAIAGQAIAGAGAHQAFAIADAJQADAKgCALQgCAKgEAIQgEAIgHAGQgGAGgIAEIgRAHQgIACgJABIgSABQgKAAgLgCg");
	this.shape_25.setTransform(-221.7,22.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#CCFF00").ss(5,1,1).p("EglRgD/MBKjAAAQBHAAAxA/QAyA+AABYIAABVQAABYgyA+QgxA/hHAAMhKjAAAQhGAAgyg/Qgyg+AAhYIAAhVQAAhYAyg+QAyg/BGAAg");
	this.shape_26.setTransform(4.5,23.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#99CC00").s().p("EglRAEAQhGAAgyg/Qgyg+AAhYIAAhVQAAhYAyg+QAyg/BGAAMBKjAAAQBHAAAxA/QAyA+AABYIAABVQAABYgyA+QgxA/hHAAg");
	this.shape_27.setTransform(4.5,23.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy2, new cjs.Rectangle(-253.5,-4.4,516.2,56.2), null);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Option1_mccopy();
	this.instance.parent = this;
	this.instance.setTransform(383,0.4,1.769,1.769,0,0,0,-2.3,-2.5);

	this.instance_1 = new lib.Option1_mccopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-383.9,0.1,1.769,1.769,0,0,0,-2.2,-2.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-554.9,-171.5,1109.9,343);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween11("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128.5,-39.7,514.7,76);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.questxt = new lib.questiontext_mccopy2();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(29.6,-4.7,1.125,1.125,0,0,0,11.4,-3.5);

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-268.4,-5.8,580.5,63.2), null);


(lib.questioncopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Option1_mccopy2();
	this.instance.parent = this;
	this.instance.setTransform(-2.6,-3.1,1.076,1.076,0,0,0,-2.2,-2.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("As1SAQiIAAhhhhQhghhgBiJIAA5pQABiJBghhQBhhhCIAAIZrAAQCIAABiBhQBfBhAACJIAAZpQAACJhfBhQhiBhiIAAgAwfweIABAAIAAAAg");
	this.shape.setTransform(-2.3,-2.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#003366").s().p("As1THQilAAh2h2IAAAAQh1h2AAimIAA5pQAAimB1h2IAAAAQB2h2ClAAIZrAAQClAAB2B2IAAAAQB2B2gBCmIAAZpQABCmh2B2Qh2B2ilAAgAx/s0IAAZpQABCJBgBhQBhBhCIAAIZrAAQCIAABihhQBfhhAAiJIAA5pQAAiJhfhhQhihhiIAAI5rAAQiIAAhhBhIAAAAIgBAAIABAAQhgBhgBCJg");
	this.shape_1.setTransform(-2.3,-2.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.02)").s().p("As1UNQjCAAiLiJIAAgBIgBAAQiJiKAAjEIAA5pQAAjECJiKIABAAQCLiKDCAAIZrAAQDCAACMCKIAAAAQCKCKgBDEIAAZpQABDEiKCKIAAAAIAAABQiMCJjCAAgAQgRmIBFhHIh4gxgAxlQfIBGBHIAzh4gAPtvsIB4gyIhFhHgAxlweIB5AyIgzh5g");
	this.shape_2.setTransform(-2.3,-2.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy, new cjs.Rectangle(-131.7,-132.2,258.7,258.7), null);


(lib.question = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.questioncopy();
	this.instance.parent = this;
	this.instance.setTransform(-2,15.6,1.057,1.057,0,0,0,-2.1,-2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.question, new cjs.Rectangle(-139,-121.6,273.4,273.3), null);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.Tween2copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.questioncopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.questioncopy();
	this.instance.parent = this;
	this.instance.setTransform(-2,15.6,1.057,1.057,0,0,0,-2.1,-2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy3, new cjs.Rectangle(-139,-121.6,273.4,273.3), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.questioncopy3();
	this.instance.parent = this;
	this.instance.setTransform(0.4,-27,1.542,1.542,0,0,0,-2.1,-2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.59,scaleY:1.59,x:0.3},9).to({scaleX:1.54,scaleY:1.54,y:-27.1},10).to({scaleX:1.59,scaleY:1.59,y:-27},10).to({scaleX:1.54,scaleY:1.54,x:0.4},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-210.7,-210.7,421.5,421.4);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(640.8,371.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(290).to({_off:false},0).wait(26).to({startPosition:16},0).to({alpha:0,startPosition:6},8).wait(6));

	// Layer_12
	this.instance_1 = new lib.question();
	this.instance_1.parent = this;
	this.instance_1.setTransform(641.4,346.3,1.542,1.542,0,0,0,-2.1,-2.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(280).to({_off:false},0).to({scaleX:1.62,scaleY:1.62},15).wait(21).to({alpha:0},8).wait(6));

	// Layer_7
	this.instance_2 = new lib.Option1_mccopy4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(256.1,533.9,1.769,1.769,0,0,0,-2.2,-2.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(256).to({_off:false},0).to({x:640.2,y:371.8},13).wait(11).to({alpha:0},4).to({_off:true},1).wait(45));

	// Layer_11
	this.instance_3 = new lib.Symbol1copy_1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(395.5,625.8,1,1,-26.5,0,0,190.3,64.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(221).to({_off:false},0).to({_off:true},35).wait(74));

	// Layer_10
	this.instance_4 = new lib.Symbol1copy("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(257.2,276.5,1,1,0,0,0,41,60.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(186).to({_off:false},0).to({_off:true},35).wait(109));

	// Layer_9
	this.instance_5 = new lib.Option1_mccopy2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(640.6,360.1,1.752,1.752,0,0,0,-2.1,-2.6);
	this.instance_5.alpha = 0.699;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(139).to({_off:false},0).to({x:1030.7,y:531.4},12).wait(34).to({alpha:0},6).to({_off:true},1).wait(138));

	// Layer_3
	this.instance_6 = new lib.Option1_mccopy2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(640.6,360.1,1.752,1.752,0,0,0,-2.1,-2.6);
	this.instance_6.alpha = 0.699;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(139).to({_off:false},0).to({x:256.5,y:534.4},12).wait(34).to({alpha:0},6).to({_off:true},1).wait(138));

	// Layer_6
	this.instance_7 = new lib.Symbol4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(677.4,112.6,1,1,0,0,0,0.5,0.5);

	this.instance_8 = new lib.Symbol2("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(641,373.4);

	this.instance_9 = new lib.questiontext_mccopy3("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(801.5,75.5,1.318,1.318,0,0,0,11.3,-3.5);

	this.questxt = new lib.questiontext_mccopy3();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(801.5,75.5,1.318,1.318,0,0,0,11.3,-3.5);
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9},{t:this.instance_8},{t:this.instance_7}]},94).to({state:[]},58).to({state:[{t:this.questxt}]},102).to({state:[{t:this.questxt}]},4).to({state:[]},1).to({state:[]},21).wait(50));
	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(254).to({_off:false},0).to({alpha:0},4).to({_off:true},1).wait(71));

	// Layer_4
	this.instance_10 = new lib.Tween6("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,533.8);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(62).to({_off:false},0).to({alpha:1},7).to({startPosition:0},88).to({startPosition:0},70).to({startPosition:0},11).to({startPosition:0},66).to({alpha:0},4).to({_off:true},1).wait(21));

	// Layer_5
	this.instance_11 = new lib.question();
	this.instance_11.parent = this;
	this.instance_11.setTransform(641.4,346.3,1.542,1.542,0,0,0,-2.1,-2.5);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(39).to({_off:false},0).to({alpha:1},5).wait(95).to({regX:-2,regY:-2.4,x:641.5,y:346.4},13).to({regX:-2.1,regY:-2.5,x:641.4,y:346.3},152).to({alpha:0},4).to({_off:true},1).wait(21));

	// Layer_2
	this.questxt_1 = new lib.questiontext_mccopy();
	this.questxt_1.name = "questxt_1";
	this.questxt_1.parent = this;
	this.questxt_1.setTransform(659.5,69.5,1.465,1.465,0,0,0,11.3,-3.5);
	this.questxt_1.alpha = 0;
	this.questxt_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt_1).wait(15).to({_off:false},0).to({alpha:1},7).wait(282).to({alpha:0},4).to({_off:true},1).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(575.5,310,1413,825);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;