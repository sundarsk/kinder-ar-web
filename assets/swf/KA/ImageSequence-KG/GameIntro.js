(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(5,1,1).p("AzYt9MAmxAAAQBdAABCBBQBCBDAABdIAAU5QAABdhCBCQhCBChdAAMgmxAAAQhdAAhDhCQhBhCAAhdIAA05QAAhdBBhDQBDhBBdAAg");
	this.shape.setTransform(325.9,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(5,1,1).p("AzYt9MAmxAAAQBdAABCBBQBCBDAABdIAAU5QAABdhCBCQhCBChdAAMgmxAAAQhdAAhDhCQhBhCAAhdIAA05QAAhdBBhDQBDhBBdAAg");
	this.shape_1.setTransform(-3.7,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(5,1,1).p("AzYt9MAmxAAAQBdAABCBBQBCBDAABdIAAU5QAABdhCBCQhCBChdAAMgmxAAAQhdAAhDhCQhBhCAAhdIAA05QAAhdBBhDQBDhBBdAAg");
	this.shape_2.setTransform(-325.8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-474.9,-91.9,950,183.9);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ah0GyQgZgXAAgkQAAgnAagYQAZgZAoAAQAmAAAYAYQAYAYAAAmQAAAlgYAYQgYAYgpAAQgoAAgXgYgAh9CbQgBhAAjhDQAihDA0gpQBAgzASgWQAggmABgtQgBgxgngfQglggg+AAQgpAAgwAhQguAhgSAAQgUAAgPgQQgPgSAAgYQAAgqA6ghQA/gmBbAAQBzAABGA7QBEA7AABhQAABghbBSIg3AvQgmAhgYArQgLAUgOBTQgNBGg1AAQg8ABABhOg");
	this.shape.setTransform(0.1,-7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(204,204,204,0.4)").s().p("AD2IaIAQhRIAAA4IAEhCIAmAmIAPAAQgRAjglAQQAAgBAAAAQgBAAAAAAQAAAAgBAAQAAAAgBAAQgHADgHAAIgCAAgAk7oZIgBAJIgCACIADgLg");
	this.shape_1.setTransform(-21.1,26.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.4)").s().p("AAzLbIABgNQgYgRgHgTQgUglgZgBIgFgCIgCABQgVgFgSgGIgugQIgbgNQiBhJhqiiQiHjKgUjaQgDguABgvQgBg4AJg1IACgJIAAgBQAii3Bih7QAKgRANgOQCfi8DfADQDfAHCZC/QA+BPAkBZQAiBTALBdIAEAXQACAvgBAvQgFEOiVEFQhOCKhmA5QgsAagwALQgXAEgQAuQgFAUgLAJIABAEIgBACIgEBCgAoKkIIAEgFIAAgFg");
	this.shape_2.setTransform(0,-1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.6,-79.9,107.3,159.9);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AgRCiIgHgFQgDgDgCgDQgCgEAAgFQAAgEACgEQACgFADgDQADgDAEgCQAEgCAEAAQAFAAAEACQADACADADQADADACAFQACAEAAAEQAAAFgCAEQgCADgDADIgGAFQgEACgFAAQgEAAgEgCgAgeBcQgCgIAAgIQAAgLAEgKQAEgJAGgJQAHgHAIgIIAQgNIARgNQAIgHAGgIQAHgJAEgKQAEgKAAgNQAAgNgFgMQgEgMgIgKQgIgJgMgHQgLgFgNgBQgOAAgKAGQgKAFgIAJQgHAJgEAMQgDALAAANIAAAKIABAKIgjAFIgDgNIgBgNQAAgTAHgTQAHgRANgNQANgOASgHQARgIAVAAQAVAAARAIQASAHANANQANANAIASQAHATAAAWIgCAUQgBAKgEAKQgFAKgGAKQgIAJgKAJIgUAOQgLAHgJAJQgJAJgFALQgFAMADAPg");
	this.shape.setTransform(214.1,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgsBrQgUgJgOgOQgOgPgHgUQgIgUAAgaQAAgXAJgWQAIgVAPgQQAPgQAUgJQAUgKAVAAQAVAAATAJQARAIAOAOQAPAOAJATQAJAUACAWIirAZQACAOAGALQAFALAJAIQAJAIALAEQALAEAMAAQALAAALgEQAKgDAJgGQAIgGAHgKQAGgJADgMIAmAIQgFASgLAOQgKAPgNAKQgOALgQAGQgQAFgSABQgYgBgVgHgAgQhMQgKADgJAGQgJAIgIALQgHAMgDARIB2gPIgBgDQgIgUgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_1.setTransform(191.1,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("AgkBrQgVgJgQgPQgQgQgKgVQgKgVAAgYQAAgOAEgPQAEgOAIgNQAHgNALgLQAKgLANgHQANgJAPgEQAPgEAQgBQARAAAPAEQAQAFANAIQAOAIAKALQALALAHAPIAAAAIghAUIAAgBQgFgKgIgIQgHgIgJgGQgJgFgLgEQgKgDgLAAQgQABgOAGQgOAGgLALQgKAKgGAPQgHAOAAAQQAAAPAHAOQAGAPAKALQALAKAOAHQAOAFAQABQAKAAAKgDQAKgDAJgFQAJgFAHgHQAHgHAFgKIABgBIAjAUIgBABQgHANgMALQgLAKgNAIQgNAHgQADQgPAFgPAAQgWAAgVgJg");
	this.shape_2.setTransform(166.6,5.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("AhcAxIgCgxIgBgmIgBgdIgBgqIAtAAIABA5QAGgMAJgKQAIgKAKgHQAKgIAKgFQALgFAMAAQAPgBALADQALAEAIAGQAIAGAFAJQAFAJADAJIAFASIABARQABAhAAAjIgCBHIgogBIADhAQABgggCggIAAgJIgCgMQgCgGgDgGQgDgHgFgEQgEgFgHgCQgHgDgJACQgQACgPAVQgQAUgTAnIACAzIAAAcIABAQIgrAFIgCg+g");
	this.shape_3.setTransform(141.6,5.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AgsBrQgUgJgOgOQgOgPgHgUQgHgUAAgaQAAgXAIgWQAJgVAOgQQAPgQATgJQAVgKAWAAQAUAAASAJQATAIAOAOQANAOAKATQAJAUADAWIirAZQABAOAFALQAGALAJAIQAJAIALAEQALAEANAAQAKAAAKgEQALgDAIgGQAJgGAGgKQAHgJADgMIAmAIQgGASgJAOQgLAPgNAKQgOALgPAGQgRAFgRABQgZgBgVgHgAgQhMQgJADgKAGQgJAIgHALQgIAMgDARIB2gPIgBgDQgHgUgNgLQgOgLgTAAQgIAAgKADg");
	this.shape_4.setTransform(117.2,5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AA3A2QgHALgIAKQgIALgKAIQgKAIgLAEQgKAEgMAAQgSAAgMgHQgNgFgJgLQgJgKgFgNQgFgOgDgOQgDgPgBgQIgBgdQAAgVACgVIAFgsIApACQgDAZgBATIgBAiIAAARIABAWIAEAWQADAMAFAJQAFAJAIAEQAIAFALgCQAOgCARgTQAQgTATglIgBgzIgBgcIgBgRIArgFIACA+IACAxIABAmIABAcIABArIgtAAg");
	this.shape_5.setTransform(92.1,5.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("ABFCoIABiHIgMAKQgGAEgHADIgMAFIgLADQgNAEgLAAQgWAAgVgIQgTgIgPgPQgOgPgJgVQgIgVAAgaQAAgcAJgUQAJgWAQgOQAPgOASgJQAUgHATAAQAMABAOAEQAMACAOAHQAMAFAMAMIABggIAnADIgBFMgAgdh4QgMAFgJALQgJAKgFAPQgEAOAAAPQAAARAEAOQAFAOAKAIQAIAKAMAGQANAFAOAAQAMAAAMgFQAMgEAKgHQAKgIAHgKQAHgLADgNIAAgfQgCgNgIgMQgHgLgKgIQgKgIgMgFQgNgEgMAAQgOAAgMAGg");
	this.shape_6.setTransform(65.5,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AgsBrQgUgJgOgOQgOgPgHgUQgIgUABgaQgBgXAJgWQAIgVAPgQQAPgQAUgJQATgKAXAAQAUAAASAJQASAIAPAOQAOAOAJATQAJAUACAWIirAZQACAOAFALQAGALAJAIQAJAIALAEQALAEANAAQAKAAALgEQAJgDAJgGQAJgGAHgKQAGgJADgMIAmAIQgGASgKAOQgJAPgOAKQgNALgRAGQgQAFgRABQgZgBgVgHgAgQhMQgKADgJAGQgKAIgGALQgIAMgDARIB2gPIgBgDQgHgUgOgLQgNgLgTAAQgIAAgKADg");
	this.shape_7.setTransform(41,5.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AgcBwIgQgDIgSgHQgKgDgJgFQgJgFgIgHIATgeIAWAMIAYAJQAMAEAMACQALACANABQALAAAIgCQAIgCAFgEQAEgCADgFIAEgHQABgDgBgEQAAgEgCgEQgCgEgDgEQgEgEgHgDQgFgDgKgCIgWgDQgQAAgRgDQgQgDgOgGQgNgGgHgJQgKgKgBgQQgBgOADgMQADgLAHgKQAGgJAKgHQAKgHAMgFQALgEANgDQANgCAMAAIASABIAWADQALACAMAFQAMAEAKAIIgOAkQgNgHgLgEIgWgFIgUgEQgegCgRAIQgRAJAAARQAAALAGAHQAHAEALADQALACAPABIAfADQAUAEANAFQAOAFAHAJQAJAHADAKQADAKABAKQAAATgIANQgHAMgNAJQgNAHgRAEQgRAFgRAAQgSAAgSgEg");
	this.shape_8.setTransform(17.3,5.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AgsBrQgUgJgOgOQgOgPgHgUQgIgUABgaQgBgXAJgWQAIgVAPgQQAPgQAUgJQATgKAXAAQAUAAASAJQASAIAPAOQAOAOAJATQAJAUACAWIirAZQACAOAFALQAGALAJAIQAJAIALAEQALAEANAAQAKAAAKgEQAKgDAJgGQAJgGAHgKQAGgJADgMIAmAIQgGASgKAOQgJAPgOAKQgNALgRAGQgQAFgRABQgZgBgVgHgAgQhMQgKADgJAGQgKAIgGALQgIAMgDARIB2gPIgBgDQgHgUgOgLQgNgLgTAAQgIAAgKADg");
	this.shape_9.setTransform(-16.2,5.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AA1CXQAGgTACgSIAEggIABggQgBgVgHgPQgGgOgKgJQgJgKgLgEQgLgFgLAAQgKABgLAGQgLAFgKAKQgMAJgKASIgBCEIgnAAIgDkwIAtgCIgBB5QALgMANgHQAMgHAKgEQANgEALgBQAWAAATAIQARAIANAPQAOAPAHAVQAIAUABAbIAAAdIgCAdIgCAaQgCANgEAKg");
	this.shape_10.setTransform(-41.5,0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("AgXgWIhFACIABglIBEgCIAChXIAngCIgCBYIBNgDIgDAlIhLADIgBCsIgoABg");
	this.shape_11.setTransform(-64.7,1.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AhcAxIgCgxIgBgmIgBgdIgBgqIAtAAIABA5QAGgMAJgKQAIgKAKgHQAKgIAKgFQALgFAMAAQAPgBALADQALAEAIAGQAIAGAFAJQAFAJADAJIAFASIABARQABAhAAAjIgCBHIgogBIADhAQABgggCggIAAgJIgCgMQgCgGgDgGQgDgHgFgEQgEgFgHgCQgHgDgJACQgQACgPAVQgQAUgTAnIACAzIAAAcIABAQIgrAFIgCg+g");
	this.shape_12.setTransform(-97.5,5.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AgYCVIABgzIABg+IABhUIAogCIgBAzIgBArIgBAjIAAAbIgBArgAgKheQgFgCgEgEQgEgEgCgFQgCgFAAgGQAAgGACgFQACgFAEgEQAEgEAFgCQAFgCAFAAQAGAAAFACQAFACAEAEQAEAEACAFQACAFAAAGQAAAGgCAFQgCAFgEAEIgJAGQgFACgGAAQgFAAgFgCg");
	this.shape_13.setTransform(-114.8,0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("AgXgWIhFACIABglIBEgCIAChXIAngCIgCBYIBNgDIgDAlIhLADIgBCsIgoABg");
	this.shape_14.setTransform(-140.3,1.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("AACAeIg7BNIgqgLIBMhgIhOhgIApgKIA8BLIA9hMIAlAPIhIBcIBMBdIglAPg");
	this.shape_15.setTransform(-161,5.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#663300").s().p("AgsBrQgUgJgOgOQgOgPgHgUQgHgUAAgaQAAgXAIgWQAJgVAOgQQAPgQATgJQAVgKAWAAQAUAAASAJQATAIAOAOQANAOAKATQAJAUADAWIirAZQABAOAFALQAGALAJAIQAJAIALAEQALAEANAAQAKAAAKgEQALgDAIgGQAJgGAGgKQAHgJADgMIAmAIQgGASgJAOQgLAPgNAKQgOALgPAGQgRAFgRABQgZgBgVgHgAgQhMQgJADgKAGQgJAIgHALQgIAMgDARIB2gPIgBgDQgHgUgNgLQgOgLgTAAQgIAAgKADg");
	this.shape_16.setTransform(-183.8,5.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#663300").s().p("AhcAxIgCgxIgBgmIgBgdIgBgqIAtAAIABA5QAGgMAJgKQAIgKAKgHQAKgIAKgFQALgFAMAAQAPgBALADQALAEAIAGQAIAGAFAJQAFAJADAJIAFASIABARQABAhAAAjIgCBHIgogBIADhAQABgggCggIAAgJIgCgMQgCgGgDgGQgDgHgFgEQgEgFgHgCQgHgDgJACQgQACgPAVQgQAUgTAnIACAzIAAAcIABAQIgrAFIgCg+g");
	this.shape_17.setTransform(-208.7,5.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(2,1,1).p("EggOgE+MBAdAAAQBpAABLBLQBLBLAABqIAAB+QAABphLBLQhLBLhpAAMhAdAAAQhpAAhLhLQhLhLAAhpIAAh+QAAhqBLhLQBLhLBpAAg");
	this.shape_18.setTransform(3.8,0.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("EggOAE+QhpABhLhMQhLhKABhpIAAh/QgBhpBLhLQBLhKBpAAMBAdAAAQBpAABKBKQBMBLAABpIAAB/QAABphMBKQhKBMhpgBg");
	this.shape_19.setTransform(3.8,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-235.5,-32.8,472.1,66.6);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AFihuQBSALBSAZAoFBwQESiwENgr");
	this.shape.setTransform(407.5,-17.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("An/kNIAAgBQAiixBeh4QAKgQANgOQCbi3DYADQDaAGCUC7QA+BMAiBXQAhBRALBaQACAMABAKQADAvgBAtQgGEHiQD9QhMCHhjA4QgrAZgvAKQgXAEgPAtQgFATgKAJIABAEIgBACIAlAlIAOAAQgRAigjAPQgBAAgCAAAoCkCQABgHACgEAAyK7IABgNQgYgRgGgSQgUgkgYgBQgCgBgDgBQgBABgBAAQgUgFgSgFQgdgLgQgFQgNgHgNgGQh+hHhnidQiDjFgTjUQgDgsAAguQAAg3AIgzQACgGAAgEAAyLyIAAAVQgHAEgIgBIAPhPgAA2KxIgEBB");
	this.shape_1.setTransform(407.6,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(204,204,204,0.4)").s().p("AAjMKIAPhPIABgNQgYgRgGgSQgUgkgYgBIgFgCIgCABIgmgKIgtgQIgagNQh+hHhnidQiDjFgTjUQESixENgrQkNArkSCxQgDgsAAguQAAg3AIgzIgBABIADgLIAAgBQAiixBeh4QAKgQANgOQCbi3DYADQDaAGCUC7QA+BMAiBXQAhBRALBaIADAWQADAvgBAtQgGEHiQD9QhMCHhjA4QgrAZgvAKQgXAEgPAtQgFATgKAJIABAEIgBACIAlAlIAOAAQgRAigjAPIgDAAIAAgVIAAAVQgGADgGAAIgDAAgAAyLyIAEhBIgEBBIAAg3IAAA3gAIFj6QhSgYhSgMQBSAMBSAYgAoBkDQACgGAAgEQAAAEgCAGgAn8kOIAEgEQAAgBABAAQAAgBAAgBQAAAAAAgBQgBgBAAAAgAAyLyg");
	this.shape_2.setTransform(407.6,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.4)").s().p("AgfA5QgQgJAAgWQgBgXAOgXQAOgXAUgLQASgLAPAIQAOAKABAVQABAYgOAWQgNAXgVALQgKAHgJAAQgIAAgFgEg");
	this.shape_3.setTransform(229.1,-63.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.4)").s().p("Ag9DRQhBg+gdhqQgchoAahWQAahXBAgSQBAgRBBA/QBCA+AcBpQAcBpgaBXQgZBWhBARQgOAEgOAAQgxAAg0gxg");
	this.shape_4.setTransform(263.1,-30);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AoFB1QIPlTH8Ca");
	this.shape_5.setTransform(244.4,-18.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("An/kNIAAgBQAiixBeh4QAKgQANgOQCbi3DYADQDaAGCUC7QA9BMAjBXQAhBRALBaQACAMABAKQADAvgBAtQgGEHiQD9QhNCHhiA4QgrAZgvAKQgXAEgPAtQgFATgKAJIABAEIgBACIAlAlIAOAAQgRAigjAPQgBAAgCAAQgHAEgJgBIAQhPIABgNQgYgRgGgSQgUgkgYgBQgDgBgCgBQgCABAAAAQgUgFgSgFQgdgLgQgFQgNgHgOgGQh9hHhnidQiDjFgTjUQgEgsABguQgBg3AJgzQACgGAAgEgAoCkCQABgHACgEAAyLyIAAAVAAyLyIAEhBAAyK7IAAA3");
	this.shape_6.setTransform(244.5,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(219,104,148,0.612)").s().p("AgHAnIAPhNIAAA1IAAAVQgGADgFAAIgEAAg");
	this.shape_7.setTransform(248.7,73.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(100,0,38,0.612)").s().p("AgbArIAAgVIAEhAIAkAlIAPAAQgRAhgjAPIgDAAg");
	this.shape_8.setTransform(252.3,73.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF77BB").s().p("AgCgUIABgNIAEABIgBACIgEBAg");
	this.shape_9.setTransform(249.8,72);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],18.1,-38.3,0,18.1,-38.3,59.1).s().p("AAxHsQgXgRgHgTQgTgkgZAAIgEgCIgDABIgmgLIgtgQIgagNQh9hGhoieQiDjDgTjWQIPlUH8CaIAEAWQACAugBAuQgFEHiRD9QhMCHhjA3QgqAZgwALQgWAEgPAtQgFATgLAJIABAEg");
	this.shape_10.setTransform(244.7,19.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],15,-12.5,0,15,-12.5,37.1).s().p("AoGBmQgBg2AJgzQHVkEIEBiQAhBRALBaQn8iZoPFTQgDgsABgug");
	this.shape_11.setTransform(244.3,-25.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(196,46,9,0.612)").s().p("AACgEIgCAIIgBACIADgKg");
	this.shape_12.setTransform(193.2,-26.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AgBAAIADAAIgDABg");
	this.shape_13.setTransform(193.5,-27);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],12.9,3.8,0,12.9,3.8,42.2).s().p("AnrD5IAEgBIAEgEIAAgFIgEAJIgEAAQAiixBfh3QAJgQANgOQCbi3DZADQDZAGCVC7QA9BLAiBXQoEhhnVEDIABgKg");
	this.shape_14.setTransform(242.5,-51.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(255,255,255,0.4)").s().p("AggA5QgPgJAAgWQgCgXAPgXQAOgXAUgLQASgLAOAIQAPAKABAWQAAAXgNAWQgOAXgVAMQgKAGgJAAQgHAAgGgEg");
	this.shape_15.setTransform(71,-61.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(255,255,255,0.4)").s().p("Ag9DRQhCg9gchqQgchpAahWQAZhXBBgSQBAgQBBA+QBBA+AdBpQAcBpgaBXQgaBWhAARQgOAEgOAAQgyAAgzgxg");
	this.shape_16.setTransform(105,-28.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AoGB1QIQlTH9Ca");
	this.shape_17.setTransform(81.4,-18.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("AoAkNIABgBQAiixBeh4QAJgQANgOQCci3DZADQDYAGCWC7QA9BMAhBXQAhBRAMBaQABAMADAKQACAvgBAtQgFEHiRD9QhMCHhkA4QgqAZgwAKQgWAEgPAtQgEATgLAJIABAEIgBACIAkAlIAQAAQgRAigkAPQgBAAgCAAQgIAEgHgBIAOhPIACgNQgXgRgIgSQgUgkgYgBQgBgBgDgBQgBABgCAAQgTgFgSgFQgdgLgQgFQgMgHgPgGQh9hHhnidQiEjFgTjUQgDgsABguQgBg3AKgzQAAgGAAgEgAoCkCQACgHAAgEAAyLyIAAAVAAyLyIAEhBAAxK7IABA3");
	this.shape_18.setTransform(81.5,0);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("rgba(219,167,104,0.612)").s().p("AgHAnIAOhNIABA1IAAAVQgHADgFAAIgDAAg");
	this.shape_19.setTransform(85.8,73.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(100,38,0,0.612)").s().p("AgbArIAAgVIAEhAIAjAlIAQAAQgRAhgjAPIgDAAg");
	this.shape_20.setTransform(89.3,73.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF77BB").s().p("AgCgUIACgNIADABIgBACIgDBAg");
	this.shape_21.setTransform(86.7,72);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],18.2,-38.3,0,18.2,-38.3,59.1).s().p("AAxHsQgXgRgHgTQgTgkgZAAIgEgCIgDABIgmgLIgsgQIgbgNQh+hGhmieQiEjDgTjWQIQlUH8CaQABALACALQACAuAAAuQgGEHiQD9QhNCHhjA3QgqAZgwALQgXAEgOAtQgFATgKAJIAAAEg");
	this.shape_22.setTransform(81.7,19.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],15,-12.5,0,15,-12.5,37.1).s().p("AoHBmQgBg2AKgzQHVkEIEBiQAhBRAMBaQn8iZoQFTQgEgsABgug");
	this.shape_23.setTransform(81.3,-25.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(196,46,9,0.612)").s().p("AABgEIAAAIIgBACIABgKg");
	this.shape_24.setTransform(30.1,-26.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF0000").s().p("AAAAAIACAAIgDABg");
	this.shape_25.setTransform(30.4,-27);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.rf(["#C7FA10","#00B606"],[0.031,1],13,3.8,0,13,3.8,42.2).s().p("AnrD5IADgBIAFgEQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBIgFAJIgCAAQAhixBeh3QAKgQANgOQCci3DYADQDZAGCVC7QA9BLAiBXQoEhhnVEDIABgKg");
	this.shape_26.setTransform(79.5,-51.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(255,255,255,0.4)").s().p("AggA5QgPgJAAgWQgBgXAOgXQAOgXAUgLQASgLAPAIQAOAKABAVQABAYgOAWQgOAXgVALQgJAHgJAAQgIAAgGgEg");
	this.shape_27.setTransform(-102.9,-63.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(255,255,255,0.4)").s().p("Ag9DRQhCg+gchqQgchoAahWQAahXBAgSQBAgRBBA/QBCA+AcBpQAcBpgaBXQgaBWhAARQgOAEgOAAQgyAAgzgxg");
	this.shape_28.setTransform(-68.9,-30);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AoFB1QIPlTH8Ca");
	this.shape_29.setTransform(-87.6,-18.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("An/kNIAAgBQAhixBfh4QAKgQAMgOQCbi3DZADQDaAGCUC7QA9BMAjBXQAgBRAMBaQABAMACAKQACAvAAAtQgGEHiRD9QhMCHhjA4QgqAZgvAKQgXAEgPAtQgFATgKAJIAAAEIAAACIAkAlIAPAAQgRAigjAPQgBAAgCAAQgIAEgIgBIAQhPIAAgNQgXgRgGgSQgUgkgYgBQgDgBgCgBQgCABgBAAQgUgFgRgFQgdgLgQgFQgNgHgOgGQh9hHhoidQiCjFgUjUQgDgsABguQgBg3AJgzQACgGAAgEgAoDkCQACgHACgEAAyLyIAAAVAAyK7IAAA3IAEhB");
	this.shape_30.setTransform(-87.5,0);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("rgba(219,104,148,0.612)").s().p("AgHAnIAPhNIAAA1IAAAVQgGADgGAAIgDAAg");
	this.shape_31.setTransform(-83.3,73.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FF77BB").s().p("AgBgUIAAgNIADABIAAACIgDBAg");
	this.shape_32.setTransform(-82.3,72);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("rgba(100,0,38,0.612)").s().p("AgbArIAAgVIAFhAIAjAlIAOAAQgQAhgiAPIgEAAg");
	this.shape_33.setTransform(-79.7,73.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],18.2,-38.3,0,18.2,-38.3,59.1).s().p("AAxHsQgXgRgGgTQgUgkgYAAIgFgCIgDABIglgLIgtgQIgbgNQh9hGhoieQiCjDgUjWQIQlUH8CaIADAWQACAuAAAuQgGEHiRD9QhMCHhjA3QgqAZgvALQgXAEgPAtQgFATgKAJIAAAEg");
	this.shape_34.setTransform(-87.4,19.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],15,-12.5,0,15,-12.5,37.1).s().p("AoGBmQgBg2AJgzQHWkEIEBiQAgBRAMBaQn8iZoQFTQgDgsABgug");
	this.shape_35.setTransform(-87.8,-25.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("rgba(196,46,9,0.612)").s().p("AACgEQAAAEgCAEIgBACIADgKg");
	this.shape_36.setTransform(-138.9,-26.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF0000").s().p("AAAAAIABAAIgBABg");
	this.shape_37.setTransform(-138.5,-27);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],13,3.8,0,13,3.8,42.2).s().p("AnqD5IACgBIgCAAQAhixBfh3QAJgQANgOQCbi3DYADQDaAGCVC7QA9BLAiBXQoEhhnVEDQACgGAAgEgAnoD4IAFgEQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBg");
	this.shape_38.setTransform(-89.5,-51.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("rgba(255,255,255,0.4)").s().p("AgfA5QgQgJAAgWQgBgXAOgXQAOgXAUgLQASgLAPAIQAOAKABAVQABAYgOAWQgOAXgVALQgJAHgJAAQgHAAgGgEg");
	this.shape_39.setTransform(-423,-63.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("rgba(255,255,255,0.4)").s().p("Ag9DRQhCg+gchqQgchoAahWQAahXBAgSQBAgRBBA/QBCA+AcBpQAcBpgaBXQgZBWhBARQgOAEgOAAQgxAAg0gxg");
	this.shape_40.setTransform(-389,-30);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AoFB1QIPlTH8Ca");
	this.shape_41.setTransform(-407.8,-18.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("An/kNIAAgBQAhixBfh4QAKgQANgOQCbi3DYADQDaAGCUC7QA9BMAjBXQAhBRALBaQABAMACAKQACAvAAAtQgGEHiRD9QhMCHhjA4QgqAZgvAKQgXAEgPAtQgFATgKAJIAAAEIAAACIAlAlIAOAAQgRAigjAPQgBAAgCAAAoDkCQACgHACgEAAyK7IAAgNQgXgRgGgSQgUgkgYgBQgDgBgCgBQgCABAAAAQgVgFgRgFQgdgLgQgFQgNgHgOgGQh9hHhnidQiDjFgUjUQgDgsABguQgBg3AJgzQACgGAAgEAAyLyIAAAVQgIAEgIgBIAQhPIAAA3IAEhB");
	this.shape_42.setTransform(-407.6,0);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("rgba(219,104,148,0.612)").s().p("AgHAnIAPhNIAAA1IAAAVQgGADgGAAIgDAAg");
	this.shape_43.setTransform(-403.4,73.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FF77BB").s().p("AgBgUIAAgNIADABIAAACIgDBAg");
	this.shape_44.setTransform(-402.4,72);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("rgba(100,0,38,0.612)").s().p("AgbArIAAgVIAFhAIAkAlIANAAQgQAhgjAPIgDAAg");
	this.shape_45.setTransform(-399.9,73.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],18.2,-38.3,0,18.2,-38.3,59.1).s().p("AAxHsQgXgRgGgTQgUgkgYAAIgFgCIgCABIgmgLIgtgQIgbgNQh9hGhnieQiDjDgUjWQIQlUH8CaIADAWQACAuAAAuQgGEHiRD9QhMCHhjA3QgqAZgvALQgXAEgPAtQgFATgKAJIAAAEg");
	this.shape_46.setTransform(-407.5,19.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],15,-12.5,0,15,-12.5,37.1).s().p("AoGBmQgBg2AJgzQHVkEIEBiQAhBRALBaQn8iZoPFTQgDgsABgug");
	this.shape_47.setTransform(-407.9,-25.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("rgba(196,46,9,0.612)").s().p("AACgEIgCAIIgBACIADgKg");
	this.shape_48.setTransform(-459,-26.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FF0000").s().p("AAAAAIABAAIgBABg");
	this.shape_49.setTransform(-458.7,-27);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],12.9,3.8,0,12.9,3.8,42.2).s().p("AnrD5IADgBIgDAAQAiixBfh3QAJgQANgOQCbi3DZADQDZAGCVC7QA9BLAiBXQoEhhnVEDIABgKgAnoD4IAEgEQAAgBABAAQAAgBAAAAQAAgBAAgBQgBAAAAgBg");
	this.shape_50.setTransform(-409.7,-51.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("rgba(255,255,255,0.4)").s().p("AgfA5QgQgJAAgWQgBgXAOgXQAOgXAUgLQASgLAPAIQAOAKABAWQABAXgOAWQgOAXgUAMQgKAGgJAAQgHAAgGgEg");
	this.shape_51.setTransform(-255.2,-61.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("rgba(255,255,255,0.4)").s().p("Ag9DRQhCg9gchqQgchpAahWQAahXBAgSQBAgQBBA+QBCA+AcBpQAcBpgaBXQgZBWhBARQgOAEgOAAQgxAAg0gxg");
	this.shape_52.setTransform(-221.2,-28.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AoGB1QIRlTH8Ca");
	this.shape_53.setTransform(-244.7,-18.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("AoAkNIABgBQAiixBeh4QAJgQANgOQCci3DYADQDZAGCWC7QA9BMAhBXQAhBRAMBaQABAMACAKQADAvgBAtQgGEHiQD9QhMCHhkA4QgqAZgwAKQgWAEgPAtQgFATgKAJIABAEIgBACIAkAlIAPAAQgQAigkAPQgBAAgCAAQgIAEgHgBIAOhPIACgNQgXgRgIgSQgUgkgYgBQgBgBgDgBQgBABgCAAQgTgFgTgFQgdgLgPgFQgNgHgOgGQh9hHhnidQiEjFgTjUQgDgsABguQgBg3AJgzQABgGAAgEgAoCkCQABgHABgEAAyLyIAAAVAAyLyIAEhBAAxK7IABA3");
	this.shape_54.setTransform(-244.6,0);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("rgba(219,167,104,0.612)").s().p("AgHAnIAOhNIABA1IAAAVQgHADgFAAIgDAAg");
	this.shape_55.setTransform(-240.4,73.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FF77BB").s().p("AgCgUIACgNIADABIgBACIgDBAg");
	this.shape_56.setTransform(-239.4,72);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("rgba(100,38,0,0.612)").s().p("AgaArIAAgVIADhAIAjAlIAQAAQgRAhgjAPIgCAAg");
	this.shape_57.setTransform(-236.8,73.2);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],18.2,-38.3,0,18.2,-38.3,59.1).s().p("AAxHsQgXgRgHgTQgTgkgZAAIgEgCIgDABIgmgLIgsgQIgcgNQh9hGhnieQiDjDgTjWQIQlUH8CaIADAWQACAuAAAuQgGEHiRD9QhMCHhjA3QgqAZgwALQgXAEgOAtQgFATgKAJIAAAEg");
	this.shape_58.setTransform(-244.5,19.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],15,-12.5,0,15,-12.5,37.1).s().p("AoHBmQgBg2AKgzQHVkEIEBiQAhBRAMBaQn8iZoQFTQgEgsABgug");
	this.shape_59.setTransform(-244.9,-25.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("rgba(196,46,9,0.612)").s().p("AABgEIgBAIIAAACIABgKg");
	this.shape_60.setTransform(-296,-26.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FF0000").s().p("AAAAAIACAAIgDABg");
	this.shape_61.setTransform(-295.7,-27);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.rf(["#C7FA10","#00B606"],[0.031,1],13,3.8,0,13,3.8,42.2).s().p("AnrD5IADgBIgCAAQAiixBdh3QAKgQANgOQCci3DYADQDZAGCWC7QA8BLAiBXQoEhhnVEDIABgKgAnoD4IAFgEQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBg");
	this.shape_62.setTransform(-246.7,-51.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("rgba(12,39,63,0.02)").s().p("EhFnAT0QltAAkCkCQkEkDAAluIAAsBQAAluEEkCQECkDFtAAMCLPAAAQFtAAEDEDQEDECAAFuIAAMBQAAFukDEDQkDECltAAg");

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("rgba(26,52,138,0.6)").s().p("EhFmASkQlNAAjrjrQjrjrAAlNIAAsBQAAlNDrjrQDrjrFNAAMCLNAAAQFMAADsDrQDrDrAAFNIAAMBQAAFNjrDrQjsDrlMAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-533.8,-126.8,1067.7,253.7);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AglBCQgRgLAAgZQgCgbAQgaQARgbAXgMQAVgMARAJQAQAKACAYQAAAcgPAaQgQAcgZAMQgMAHgKAAQgIAAgHgEg");
	this.shape.setTransform(-4.6,-69.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.4)").s().p("AhFDxQhMhIgih5Qggh4AehkQAdhkBLgVQBJgTBKBJQBNBHAgB5QAhB4geBlQgcBkhLAUQgQAEgQAAQg6AAg6g5g");
	this.shape_1.setTransform(34.6,-31.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("ApVCHQJimIJJCz");
	this.shape_2.setTransform(7.4,-19.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("ApOk2IABgBQAnjNBsiLQALgSAQgPQCyjUD6AEQD6AHCtDYQBGBYAmBjQAmBeAOBpQABAMADAMQADA2gBA1QgHEuimElQhZCbhxA/QgxAdg3ANQgaAEgRAzQgGAXgMAKIABAEIgBACIAqAsIARAAQgUAngoARQgBAAgCAAAA5MnIACgQQgbgUgJgVQgXgpgbgBQgCAAgEgCQgCACgBAAQgWgHgUgGQgjgNgRgFQgPgIgRgIQiQhRh2i1QiYjkgWj0QgEg0ABg0QgBhAALg6QABgHAAgEAA6NkIAAAZQgKAEgIAAIARhaIABA9IAEhLApRkoQACgKABgE");
	this.shape_3.setTransform(7.5,1.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(219,167,104,0.612)").s().p("AgIAtIAQhZIABA8IAAAZQgJAEgGAAIgCAAg");
	this.shape_4.setTransform(12.4,86.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF77BB").s().p("AgCgWIACgPIADAAIgBACIgDBJg");
	this.shape_5.setTransform(13.5,84.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(100,38,0,0.612)").s().p("AgeAyIAAgaIADhJIAqAsIARAAQgUAmgnARIgDAAg");
	this.shape_6.setTransform(16.5,85.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(196,46,9,0.612)").s().p("AACgGIgBAKIgCADIADgNg");
	this.shape_7.setTransform(-51.7,-28.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AAAAAIACAAIgDABg");
	this.shape_8.setTransform(-51.4,-29.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#C7FA10","#00B606"],[0.031,1],15,4.6,0,15,4.6,48.6).s().p("Ao2EgIAEgBIgDAAQAnjNBsiKQALgTAQgPQCyjTD6AEQD6AHCtDXQBGBXAmBjQpThwocErIABgKgAoyEfIAFgGQABgDgBgDg");
	this.shape_9.setTransform(5.1,-58.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],17.5,-14.3,0,17.5,-14.3,42.7).s().p("ApWB2QAAhAALg5QIbksJTBxQAnBeANBoQpJiyphGIQgDg0AAg0g");
	this.shape_10.setTransform(7.2,-28.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],21,-44,0,21,-44,68.1).s().p("AA5I3QgbgTgJgVQgVgpgdgBIgFgDIgEACIgqgMIg0gTQgOgIgSgHQiPhSh3i0QiYjjgWj1QJhmJJJCyQACANADALQACA2gBA1QgHEwimEjQhYCbhyA/QgxAdg2ANQgaAFgSAzQgGAXgMAKIABAEg");
	this.shape_11.setTransform(7.7,23.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.7,-89.1,122.5,181.4);


(lib.qtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AgKBhIgEgDIgDgEIgBgFIABgFIADgFIAEgDIAFgBIAFABIAEADIADAFIABAFIgBAFIgDAEIgEADIgFABIgFgBgAgRA3IgCgJQAAgHADgGQACgGAEgFIAJgIIAJgJIAKgHIAJgJIAGgLQACgGAAgIQAAgIgCgHQgDgHgFgGQgFgGgHgDQgGgEgIAAQgIAAgGAEQgGADgEAFQgFAFgCAHQgCAHAAAIIAAAGIABAGIgVADIgCgIIgBgIQAAgMAFgKQAEgLAHgHQAIgIALgFQAKgFAMAAQAMAAALAFQALAEAHAIQAIAIAFALQAEALAAANIgBAMIgDAMQgDAGgEAGQgEAEgHAGIgMAJQgGAEgFAFQgGAFgDAHQgDAHACAJg");
	this.shape.setTransform(201.7,-0.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgaA/QgMgEgIgJQgIgIgFgNQgEgMAAgPQAAgOAFgNQAFgNAJgJQAIgJAMgGQAMgFANAAQAMAAALAEQALAFAJAJQAHAIAGAMQAFALACAOIhlAOQAAAIAEAHQADAGAFAFQAFAFAHACQAHACAHAAQAGAAAGgCQAGgCAGgDQAFgDAEgHQAEgFABgHIAXAEQgEALgFAJQgGAIgJAHQgHAGgKADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgGAFQgFAEgFAHQgEAHgBAKIBGgIIgBgDQgEgLgJgHQgHgHgMAAQgEAAgGACg");
	this.shape_1.setTransform(188,2.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("AgVA/QgMgEgKgKQgKgJgGgNQgGgMAAgPQAAgIADgIQACgJAFgIQAEgHAHgGQAGgHAIgFQAHgEAJgDQAJgDAJAAQAKAAAKADQAJACAIAFQAIAEAGAHQAGAHAEAJIABAAIgUAMIAAgBQgDgGgFgEIgJgJIgMgFQgGgCgHAAQgJAAgIAEQgJAEgGAGQgGAGgEAJQgEAIAAAJQAAAJAEAJQAEAIAGAHQAGAHAJADQAIAEAJAAQAGAAAGgCQAGgBAFgDQAGgEAEgEQAEgEADgFIABgBIAUALIAAABQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgMAAgNgGg");
	this.shape_2.setTransform(173.4,2.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("AAhBBIACgmQABgTgBgTIgBgFIgBgHIgDgIQgBgEgDgCQgDgDgEgCQgEgBgGABQgJABgJANQgJAMgLAWIAAAfIABARIAAAJIgZADIgBglIgCgdIAAgWIgBgRIgBgZIAbgBIABAiQAEgHAFgFQAFgHAFgEQAGgFAGgDQAGgCAIgBQAJAAAGACQAHACAFAEQAEADADAGQAEAFABAFIADALIABAKQABATgBAVIgBArg");
	this.shape_3.setTransform(158.5,2.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AgaA/QgMgEgIgJQgIgIgFgNQgEgMAAgPQAAgOAFgNQAFgNAJgJQAIgJAMgGQAMgFANAAQAMAAALAEQALAFAJAJQAHAIAGAMQAFALACAOIhlAOQAAAIAEAHQADAGAFAFQAFAFAHACQAHACAHAAQAGAAAGgCQAGgCAGgDQAFgDAEgHQAEgFABgHIAXAEQgEALgFAJQgHAIgIAHQgIAGgJADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgFAFQgGAEgFAHQgEAHgBAKIBGgIIgBgDQgEgLgJgHQgIgHgLAAQgEAAgGACg");
	this.shape_4.setTransform(144,2.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AAhAgIgJANQgFAGgGAFQgGAFgHACQgFADgHAAQgLgBgHgDQgIgEgFgGQgFgGgDgIIgGgRIgBgSIgBgRIABgZIADgaIAZABIgDAaIgBAVIABAKIAAAMIADAOQABAHADAFQADAFAFADQAFADAGgBQAJgBAKgMQAJgLALgWIAAgeIgBgRIAAgKIAZgDIABAlIABAcIABAXIAAARIABAZIgbABg");
	this.shape_5.setTransform(129.1,2.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AApBkIABhQIgHAFIgHAFIgIADIgHACQgHACgGAAQgOAAgMgFQgMgEgJgJQgIgJgFgMQgGgNAAgQQABgPAFgNQAGgNAJgIQAJgJAMgFQALgEALAAQAHAAAIADQAHABAJAEQAHAEAIAGIAAgTIAWACIAADFgAgRhHQgHADgFAHQgGAFgCAJQgDAIAAAKQAAAKADAIQADAIAFAFQAFAGAIADQAHAEAJgBQAGABAHgDQAHgDAHgFQAFgEAFgFQAEgHABgIIABgSQgCgIgEgHQgFgGgFgGQgHgEgHgDQgHgCgHgBQgJABgHADg");
	this.shape_6.setTransform(113.2,5.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AgaA/QgMgEgIgJQgIgIgFgNQgEgMAAgPQAAgOAFgNQAFgNAJgJQAIgJAMgGQAMgFANAAQAMAAALAEQALAFAJAJQAHAIAGAMQAFALACAOIhlAOQAAAIAEAHQADAGAFAFQAFAFAHACQAHACAHAAQAGAAAGgCQAGgCAGgDQAFgDAEgHQAEgFABgHIAXAEQgEALgFAJQgHAIgIAHQgIAGgJADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgFAFQgGAEgFAHQgEAHgBAKIBGgIIgBgDQgEgLgJgHQgIgHgLAAQgEAAgGACg");
	this.shape_7.setTransform(98.5,2.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AgQBDIgKgCIgLgEIgLgFIgLgHIAMgSIANAHIAPAGIAOADIAOABIALgBIAIgCIAEgFIADgEIAAgFIgBgEIgEgFIgGgEIgJgDIgNgCIgTgCQgKgCgIgDQgIgDgFgFQgFgGgBgJQAAgJACgHQABgHAEgFQAEgGAGgEQAGgFAHgCIAOgFQAIgBAGAAIAMAAIAMADIAOADQAHADAGAFIgIAVQgHgEgIgCIgMgEIgMgBQgRgCgLAFQgLAFAAAKQAAAIAFACQAEAEAGABIAQACIASACQALACAIAEQAJACAEAFQAFAFACAFQACAGAAAGQABALgFAIQgFAHgHAGQgIAEgKACQgKADgLAAQgJAAgLgCg");
	this.shape_8.setTransform(84.5,3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AgaA/QgMgEgIgJQgIgIgEgNQgFgMAAgPQAAgOAFgNQAFgNAJgJQAJgJALgGQAMgFANAAQAMAAALAEQALAFAIAJQAJAIAFAMQAFALACAOIhlAOQABAIADAHQADAGAFAFQAGAFAGACQAHACAHAAQAGAAAHgCQAGgCAFgDQAFgDAEgHQADgFACgHIAXAEQgEALgFAJQgGAIgJAHQgHAGgKADQgJAEgLAAQgOAAgNgFgAgJgtQgGABgFAFQgGAEgEAHQgFAHgCAKIBHgIIgBgDQgEgLgJgHQgHgHgMAAQgEAAgGACg");
	this.shape_9.setTransform(64.5,2.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AAgBaQADgLACgLIACgTIAAgTQgBgNgDgJQgEgIgGgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgHAGQgHAFgFAKIgBBPIgXAAIgCi1IAbgBIgBBIQAHgHAHgEIAOgGQAHgDAGgBQANAAALAFQALAFAIAJQAIAJAEAMQAFAMAAAQIAAARIgBARIgBAQIgDAOg");
	this.shape_10.setTransform(49.4,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("AgOgNIgoABIAAgWIApgBIABg0IAWgBIgBA1IAugCIgBAWIgtABIAABnIgZAAg");
	this.shape_11.setTransform(35.6,0.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AAhBBIACgmQABgTgBgTIgBgFIgBgHIgDgIQgBgEgDgCQgDgDgEgCQgEgBgGABQgJABgJANQgJAMgLAWIAAAfIABARIAAAJIgZADIgBglIgCgdIAAgWIgBgRIgBgZIAbgBIABAiQAEgHAFgFQAFgHAFgEQAGgFAGgDQAGgCAIgBQAJAAAGACQAHACAFAEQAEADADAGQAEAFABAFIADALIABAKQABATgBAVIgBArg");
	this.shape_12.setTransform(16,2.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AgOBZIAAgeIABglIABgyIAXgBIAAAeIgBAaIAAAUIgBAQIAAAagAgFg3IgGgEQgCgCgBgDQgCgDAAgEQAAgDACgDQABgDACgDQACgCAEgBQACgCADAAIAHACQADABACACIAEAGIABAGIgBAHIgEAFIgFAEIgHABIgFgBg");
	this.shape_13.setTransform(5.7,-0.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("AgNgNIgqABIABgWIApgBIABg0IAXgBIgBA1IAugCIgCAWIgsABIgCBnIgXAAg");
	this.shape_14.setTransform(-9.4,0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("AABASIgjAuIgYgHIAtg5Igvg5IAZgFIAjAsIAkgtIAWAJIgrA2IAuA3IgWAJg");
	this.shape_15.setTransform(-21.8,2.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#663300").s().p("AgaA/QgMgEgIgJQgIgIgEgNQgFgMAAgPQAAgOAFgNQAFgNAJgJQAJgJALgGQAMgFANAAQAMAAALAEQALAFAIAJQAJAIAFAMQAFALACAOIhlAOQABAIADAHQADAGAFAFQAGAFAGACQAHACAHAAQAGAAAHgCQAGgCAFgDQAFgDAEgHQADgFACgHIAXAEQgEALgGAJQgFAIgJAHQgHAGgKADQgJAEgLAAQgOAAgNgFgAgJgtQgGABgFAFQgGAEgEAHQgFAHgCAKIBHgIIgBgDQgEgLgJgHQgHgHgMAAQgEAAgGACg");
	this.shape_16.setTransform(-35.4,2.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#663300").s().p("AAhBBIACgmQABgTgBgTIgBgFIgBgHIgDgIQgBgEgDgCQgDgDgEgCQgEgBgGABQgJABgJANQgJAMgLAWIAAAfIABARIAAAJIgZADIgBglIgCgdIAAgWIgBgRIgBgZIAbgBIABAiQAEgHAFgFQAFgHAFgEQAGgFAGgDQAGgCAIgBQAJAAAGACQAHACAFAEQAEADADAGQAEAFABAFIADALIABAKQABATgBAVIgBArg");
	this.shape_17.setTransform(-50.2,2.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#663300").s().p("AgRBDIgJgCIgLgEIgLgFIgLgHIAMgSIANAHIAPAGIAOADIAOABIALgBIAHgCIAFgFIADgEIAAgFIgBgEIgEgFIgGgEIgJgDIgNgCIgTgCQgLgCgHgDQgIgDgFgFQgFgGgBgJQgBgJACgHQACgHAEgFQAEgGAGgEQAGgFAGgCIAPgFQAIgBAGAAIAMAAIAMADIAOADQAHADAGAFIgIAVQgIgEgGgCIgNgEIgMgBQgRgCgLAFQgLAFAAAKQABAIAEACQADAEAHABIAQACIASACQAMACAHAEQAJACAEAFQAFAFACAFQADAGAAAGQAAALgFAIQgFAHgHAGQgIAEgKACQgJADgMAAQgJAAgMgCg");
	this.shape_18.setTransform(-70.7,3.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#663300").s().p("AgaA/QgMgEgIgJQgJgIgEgNQgEgMAAgPQAAgOAFgNQAFgNAJgJQAJgJALgGQAMgFANAAQAMAAALAEQALAFAJAJQAHAIAGAMQAFALACAOIhmAOQACAIADAHQADAGAGAFQAEAFAHACQAHACAHAAQAGAAAGgCQAHgCAEgDQAGgDADgHQAEgFACgHIAXAEQgDALgGAJQgGAIgJAHQgIAGgJADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgGAFQgFAEgFAHQgEAHgBAKIBFgIIAAgDQgEgLgJgHQgIgHgLAAQgEAAgGACg");
	this.shape_19.setTransform(-84.4,2.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#663300").s().p("AA5AcIAAgaIgBgSQAAgJgDgFQgDgFgEAAQgDAAgDACIgGAFIgGAGIgGAHIgFAIIgEAGIABAKIAAAPIAAASIAAAVIgVACIgBglIgBgaIgBgSQAAgJgDgFQgDgFgEAAQgDAAgEACIgGAFIgGAIIgGAIIgGAIIgEAFIABA+IgXACIgEh+IAZgDIABAjIAIgKIAJgJQAFgEAFgDQAGgDAHAAQAFAAAFACQAEABADADQAEAEACAFQADAFABAIIAHgKQAEgFAFgDQAFgEAGgDQAFgDAHAAQAFAAAFACQAFABAEAEQAEAEACAGQADAGAAAIIABAVIABAcIABApIgZACIAAglg");
	this.shape_20.setTransform(-101.5,2.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#663300").s().p("AgYBAQgLgFgIgIQgJgKgGgNQgEgMAAgQQAAgPAEgMQAGgNAJgKQAIgIALgFQALgFANgBQANAAAMAGQALAFAJAKQAIAJAFANQAGAMAAAOQAAAPgGAMQgFANgIAKQgJAJgLAFQgMAGgNAAQgNAAgLgGgAgQgrQgIAEgEAHQgFAHgCAJQgCAIAAAIQAAAIACAJQADAIAEAIQAFAGAHAFQAHAFAJgBQAKABAHgFQAHgFAFgGQAFgIACgIQADgJgBgIQABgIgDgIQgCgJgFgHQgEgHgHgEQgIgFgKABQgJgBgHAFg");
	this.shape_21.setTransform(-118.5,2.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#663300").s().p("AgVA/QgMgEgKgKQgKgJgGgNQgGgMAAgPQAAgIADgIQACgJAFgIQAEgHAHgGQAGgHAIgFQAHgEAJgDQAJgDAJAAQAKAAAKADQAJACAIAFQAIAEAGAHQAGAHAEAJIABAAIgUAMIAAgBQgDgGgFgEIgJgJIgMgFQgGgCgHAAQgJAAgIAEQgJAEgGAGQgGAGgEAJQgEAIAAAJQAAAJAEAJQAEAIAGAHQAGAHAJADQAIAEAJAAQAGAAAGgCQAGgBAFgDQAGgEAEgEQAEgEADgFIABgBIAUALIAAABQgEAIgHAGQgHAGgIAFQgIAEgJADQgJACgJAAQgMAAgNgGg");
	this.shape_22.setTransform(-133,2.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#663300").s().p("AgNgNIgqABIABgWIApgBIABg0IAWgBIgBA1IAugCIgBAWIgtABIgBBnIgYAAg");
	this.shape_23.setTransform(-152.4,0.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#663300").s().p("AAlBDIACgQQgPAJgOAEQgNAEgMAAQgJAAgHgCQgJgCgGgFQgFgEgEgHQgEgIAAgJQAAgKADgHQADgIAEgGQAFgEAHgEQAHgFAIgCIAQgEIAQgBIAOAAIANACIgFgMQgCgGgDgEQgDgFgFgCQgGgDgHAAIgKABQgFABgHAEQgIADgIAHQgIAGgKAKIgOgSQAMgKAKgHQAKgHAKgEQAJgEAIgBIAMgBQALAAAJADQAJAEAGAHQAGAGAEAJQAFAIADAKIAEAVIABAUIgBAZIgEAcgAgEAAQgLABgHAFQgHAFgDAHQgFAHADAIQABAHAFADQAFADAHAAQAHAAAHgCIAPgGIAPgHIAMgHIAAgMIAAgMIgMgBIgNgBQgLAAgIACg");
	this.shape_24.setTransform(-166,2.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#663300").s().p("AAgBaQADgLACgLIACgTIAAgTQgBgNgDgJQgEgIgGgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgHAGQgHAFgFAKIgBBPIgXAAIgCi1IAbgBIgBBIQAHgHAHgEIAOgGQAHgDAGgBQANAAALAFQALAFAIAJQAIAJAEAMQAFAMAAAQIAAARIgBARIgBAQIgDAOg");
	this.shape_25.setTransform(-181.1,0);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#663300").s().p("AhGBbIggirIAZgEIAVCGIAsh7IAcADIAnB/IAYiSIAZAEIghCvIgcgBIgqiFIgvCHg");
	this.shape_26.setTransform(-200.3,-0.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#CC6600").ss(3,1,1).p("EAh7gCwQg3g1hNAAMg/tAAAQhNAAg2A1Qg2A0AABKIAABlQAABKA2A0QA2A1BNAAMA/tAAAQBNAAA3g1QA1g0AAhKIAAhlQAAhKg1g0g");

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFCC00").s().p("A/2DlQhNABg2g1Qg2g0AAhKIAAhlQAAhKA2g0QA2g1BNAAMA/tAAAQBNAAA3A1QA1A0AABKIAABlQAABKg1A0Qg3A1hNgBg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.qtext, new cjs.Rectangle(-223.9,-24.4,447.8,48.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.patterncopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgTAkQgKgGAAgOQgBgOAJgOQAJgOAMgHQALgHAJAFQAJAGABAOQAAAOgIAOQgJAOgNAHQgFAEgGAAQgEAAgEgCg");
	this.shape.setTransform(139.2,-43.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.4)").s().p("AglCCQgqgmgRhCQgRhBAPg2QAQg2AogLQAogKAoAnQApAmASBCQASBBgRA2QgPA2goAKQgJADgJAAQgfAAgfgfg");
	this.shape_1.setTransform(160.4,-22.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AlCBJQFIjTE9Bg");
	this.shape_2.setTransform(148.7,-14.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Ak+inIAAgBQAVhuA7hLQAGgKAIgJQBghyCHACQCHAEBdB0QAmAwAVA2QAVAyAHA5QABAHABAGQABAdAAAdQgECjhaCeQgvBUg+AiQgaAQgeAGQgOADgJAcQgDAMgHAFIABADIgBABIAXAXIAJAAQgKAVgWAKQgBgBgBAAQgFADgFgBIAKgxIAAAiIAAANAlAigQABgFABgCAAfGzIAAgIQgOgKgEgMQgNgWgOgBQgCAAgBgBQgBABgBAAQgMgDgLgEQgSgGgKgEQgIgEgJgEQhOgshAhiQhSh6gMiEQgCgcABgcQgBgiAGggQABgEAAgCAAfHVIACgo");
	this.shape_3.setTransform(148.8,-3.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF77BB").s().p("AgBgMIABgIIACABIgBABIgCAng");
	this.shape_4.setTransform(152,41.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(219,104,148,0.612)").s().p("AgEAYIAJgwIAAAhIAAANQgEADgDAAIgCgBg");
	this.shape_5.setTransform(151.4,42.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(100,0,38,0.612)").s().p("AgQAaIAAgMIACgoIAWAYIAJAAQgKATgVAKIgCgBg");
	this.shape_6.setTransform(153.6,42);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],11.3,-23.8,0,11.3,-23.8,36.8).s().p("AAfEyQgPgKgEgMQgMgWgPgBIgDgBIgBABIgYgHIgcgKIgQgIQhOgshBhiQhRh5gMiFQFIjUE8BgIACANQACAdgBAdQgDCkhaCdQgwBUg9AiQgbAQgdAGQgOADgKAcQgDAMgGAFIAAADg");
	this.shape_7.setTransform(148.8,8.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],9.4,-7.7,0,9.4,-7.7,23.1).s().p("AlCBAQgBgiAGgfQEkiiFBA9QAVAyAHA4Qk9hflIDTQgCgcABgcg");
	this.shape_8.setTransform(148.6,-19.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(196,46,9,0.612)").s().p("AABgDIgBAFIAAABIABgGg");
	this.shape_9.setTransform(116.8,-20);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AAAAAIABAAIgBAAg");
	this.shape_10.setTransform(117,-20.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],8.1,2.4,0,8.1,2.4,26.2).s().p("AkxCbIACAAIgCAAQAVhvA7hKQAGgKAIgIQBghyCHACQCHAEBdB0QAmAuAVA2QlBg8kkChIABgGgAkvCbIACgDIAAgDg");
	this.shape_11.setTransform(147.5,-35.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(255,255,255,0.4)").s().p("AgTAkQgKgGAAgOQgBgOAJgOQAJgOAMgHQALgHAJAFQAJAGABAOQAAAOgIAOQgJAOgNAHQgFAEgGAAQgEAAgEgCg");
	this.shape_12.setTransform(40.7,-42.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(255,255,255,0.4)").s().p("AgmCCQgogmgShCQgRhBAQg2QAPg2AogLQApgKAnAnQAqAmARBCQARBBgPA2QgRA2goAKQgIADgJAAQgfAAgggfg");
	this.shape_13.setTransform(61.9,-21.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AlCBJQFIjTE9Bg");
	this.shape_14.setTransform(47.2,-14.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Ak+inIAAgBQAVhuA7hLQAGgKAIgJQBhhyCGACQCHAEBdB0QAmAwAVA2QAVAyAHA5QABAHABAGQACAdgBAdQgDCjhaCeQgwBUg+AiQgaAQgeAGQgOADgJAcQgDAMgGAFIAAADIAAABIAWAXIAKAAQgLAVgWAKQgBgBgBAAQgFADgEgBIAJgxIABgIQgPgKgEgMQgNgWgOgBQgBAAgCgBQgBABgBAAQgMgDgLgEQgSgGgKgEQgIgEgJgEQhOgshAhiQhSh6gMiEQgCgcABgcQgBgiAGggQABgEAAgCgAlAigQABgFABgCAAfHVIADgoAAfGzIAAAiIAAAN");
	this.shape_15.setTransform(47.2,-3.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF77BB").s().p("AgBgMIABgIIACABIgBABIgBAng");
	this.shape_16.setTransform(50.5,41.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(219,167,104,0.612)").s().p("AgEAYIAIgwIABAhIAAANQgEADgDAAIgCgBg");
	this.shape_17.setTransform(49.9,42.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(100,38,0,0.612)").s().p("AgQAaIAAgMIACgoIAWAYIAJAAQgKATgVAKIgCgBg");
	this.shape_18.setTransform(52.1,42);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],11.3,-23.8,0,11.3,-23.8,36.8).s().p("AAfEyQgPgKgEgMQgMgWgPgBIgDgBIgCABIgXgHIgcgKIgRgIQhOgshAhiQhSh5gMiFQFJjUE8BgIACANQACAdgBAdQgDCkhaCdQgwBUg+AiQgaAQgeAGQgOADgJAcQgDAMgGAFIAAADg");
	this.shape_19.setTransform(47.3,8.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],9.4,-7.7,0,9.4,-7.7,23.1).s().p("AlDBAQAAgiAGgfQEkiiFBA9QAUAyAIA4Qk9hflIDTQgCgcAAgcg");
	this.shape_20.setTransform(47.1,-19.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(196,46,9,0.612)").s().p("AABgDIgBAFIAAABIABgGg");
	this.shape_21.setTransform(15.2,-20);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF0000").s().p("AAAAAIABAAIgBAAg");
	this.shape_22.setTransform(15.4,-20.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.rf(["#C7FA10","#00B606"],[0.031,1],8.1,2.4,0,8.1,2.4,26.2).s().p("AkxCbIABAAIgBAAQAVhvA7hKQAFgKAIgIQBihyCGACQCHAEBdB0QAmAuAVA2QlBg8kkChIABgGgAkwCbIAEgDIAAgDg");
	this.shape_23.setTransform(46,-35.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AhGEHQgPgOAAgWQAAgXAPgPQAQgPAYAAQAXAAAOAPQAPAOAAAXQAAAWgPAPQgOAPgZAAQgYAAgOgPgAhMBeQAAgmAVgpQAVgoAggaQAmgeALgOQATgXAAgbQAAgdgXgUQgXgTglAAQgZAAgdAUQgcAUgLAAQgMAAgJgKQgJgLAAgOQAAgaAjgUQAmgWA3gBQBGAAAqAkQApAjAAA8QAAA6g3AyIghAcQgXAUgPAZQgGANgJAyQgIArggAAQgkAAAAgvg");
	this.shape_24.setTransform(253,-7.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("rgba(204,204,204,0.4)").s().p("ACVFGIAKgxIAAAiIACgoIAXAXIAJAAQgKAVgWAKIgCgBQgEACgEAAIgCAAgAi+lFIgBAGIgBABIACgHg");
	this.shape_25.setTransform(240.1,13);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(0,0,0,0.4)").s().p("AAfG7IAAgIQgOgKgEgMQgNgWgOgBIgDgBIgCABIgXgHIgcgKIgRgIQhOgshAhiQhSh6gMiEQgCgcABgcQgBgiAGggIABgGIAAgBQAVhuA7hLQAGgKAIgJQBghyCHACQCHAEBdB0QAmAwAVA2QAVAyAHA5IACANQABAdAAAdQgECjhaCeQgvBUg+AiQgaAQgeAGQgOADgJAcQgDAMgHAFIABADIgBABIgCAogAk8igIACgDIAAgDg");
	this.shape_26.setTransform(252.9,-3.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(255,255,255,0.4)").s().p("AgTAkQgKgGAAgOQgBgOAJgOQAJgOAMgHQALgHAJAFQAJAGABAOQAAAOgIAOQgJAOgNAHQgFAEgGAAQgEAAgEgCg");
	this.shape_27.setTransform(-67.6,-43.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(255,255,255,0.4)").s().p("AgmCCQgpgmgRhCQgRhBAPg2QARg2AogLQAogKAoAnQAoAmASBCQASBBgRA2QgQA2goAKQgIADgJAAQgfAAgggfg");
	this.shape_28.setTransform(-46.4,-22.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AlCBJQFIjTE9Bg");
	this.shape_29.setTransform(-58.1,-14.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Ak+inIAAgBQAVhuA7hLQAGgKAIgJQBghyCHACQCHAEBdB0QAmAwAVA2QAVAyAHA5QABAHABAGQABAdAAAdQgECjhaCeQgvBUg+AiQgaAQgeAGQgOADgJAcQgDAMgHAFIABADIgBABIAXAXIAJAAQgKAVgWAKQgBgBgBAAAlAigQABgFABgCAAfGzIAAgIQgOgKgEgMQgNgWgOgBQgCAAgBgBQgBABgBAAQgMgDgLgEQgSgGgKgEQgIgEgJgEQhOgshAhiQhSh6gMiEQgCgcABgcQgBgiAGggQABgEAAgCAAfHVIAAANQgFADgFgBIAKgxIAAAiIACgo");
	this.shape_30.setTransform(-58,-3.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("rgba(219,104,148,0.612)").s().p("AgEAYIAJgwIAAAhIAAANQgEADgDAAIgCgBg");
	this.shape_31.setTransform(-55.4,42.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("rgba(100,0,38,0.612)").s().p("AgQAaIAAgMIACgoIAWAYIAJAAQgKATgVAKIgCgBg");
	this.shape_32.setTransform(-53.2,42);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF77BB").s().p("AgBgMIABgIIACABIgBABIgCAng");
	this.shape_33.setTransform(-54.8,41.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],11.3,-23.8,0,11.3,-23.8,36.8).s().p("AAfEyQgPgKgEgMQgMgWgPgBIgDgBIgBABIgYgHIgcgKIgQgIQhOgshBhiQhRh5gMiFQFIjUE8BgIACANQACAdgBAdQgDCkhaCdQgwBUg9AiQgbAQgdAGQgOADgKAcQgDAMgGAFIAAADg");
	this.shape_34.setTransform(-58,8.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],9.4,-7.7,0,9.4,-7.7,23.1).s().p("AlCBAQgBgiAGgfQEkiiFBA9QAVAyAHA4Qk9hflIDTQgCgcABgcg");
	this.shape_35.setTransform(-58.2,-19.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("rgba(196,46,9,0.612)").s().p("AABgDIgBAFIAAABIABgGg");
	this.shape_36.setTransform(-90,-20);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF0000").s().p("AAAAAIABAAIgBAAg");
	this.shape_37.setTransform(-89.8,-20.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],8.1,2.4,0,8.1,2.4,26.2).s().p("AkxCbIACAAIgCAAQAVhvA7hKQAGgKAIgIQBghyCHACQCHAEBdB0QAmAuAVA2QlBg8kkChIABgGgAkvCbIACgDIAAgDg");
	this.shape_38.setTransform(-59.3,-35.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("rgba(255,255,255,0.4)").s().p("AgTAkQgKgGAAgOQgBgOAJgOQAJgOAMgHQALgHAJAFQAJAGABAOQAAAOgIAOQgJAOgNAHQgFAEgGAAQgEAAgEgCg");
	this.shape_39.setTransform(-267,-43.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("rgba(255,255,255,0.4)").s().p("AglCCQgpgmgShCQgShBARg2QAPg2AogLQAogKAoAnQAqAmARBCQARBBgPA2QgQA2goAKQgJADgJAAQgfAAgfgfg");
	this.shape_40.setTransform(-245.8,-22.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AlCBJQFIjTE9Bg");
	this.shape_41.setTransform(-257.5,-14.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Ak+inIAAgBQAVhuA7hLQAGgKAIgJQBghyCHACQCHAEBdB0QAmAwAVA2QAVAyAHA5QABAHABAGQABAdAAAdQgECjhaCeQgvBUg+AiQgaAQgeAGQgOADgJAcQgDAMgHAFIABADIgBABIAXAXIAJAAQgKAVgWAKQgBgBgBAAQgFADgFgBIAKgxIAAgIQgOgKgEgMQgNgWgOgBQgCAAgBgBQgBABgBAAQgMgDgLgEQgSgGgKgEQgIgEgJgEQhOgshAhiQhSh6gMiEQgCgcABgcQgBgiAGggQABgEAAgCgAlAigQABgFABgCAAfHVIAAANAAfHVIACgoAAfGzIAAAi");
	this.shape_42.setTransform(-257.4,-3.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("rgba(100,0,38,0.612)").s().p("AgQAaIAAgMIACgoIAWAYIAJAAQgKATgVAKIgCgBg");
	this.shape_43.setTransform(-252.6,42);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("rgba(219,104,148,0.612)").s().p("AgEAYIAJgwIAAAhIAAANQgEADgDAAIgCgBg");
	this.shape_44.setTransform(-254.8,42.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF77BB").s().p("AAAgMIAAgIIABABIAAABIgBAng");
	this.shape_45.setTransform(-254.2,41.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],11.3,-23.8,0,11.3,-23.8,36.8).s().p("AAfEyQgPgKgEgMQgMgWgPgBIgDgBIgBABIgYgHIgcgKIgQgIQhOgshBhiQhRh5gMiFQFIjUE8BgIACANQACAdgBAdQgDCkhaCdQgwBUg9AiQgbAQgdAGQgOADgKAcQgDAMgGAFIAAADg");
	this.shape_46.setTransform(-257.4,8.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],9.4,-7.7,0,9.4,-7.7,23.1).s().p("AlCBAQgBgiAGgfQEkiiFBA9QAVAyAHA4Qk9hflIDTQgCgcABgcg");
	this.shape_47.setTransform(-257.6,-19.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("rgba(196,46,9,0.612)").s().p("AABgDIgBAFIAAABIABgGg");
	this.shape_48.setTransform(-289.4,-20);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],8.1,2.4,0,8.1,2.4,26.2).s().p("AkxCbIACAAIACgDIAAgDIgCAGIgCAAQAVhvA7hKQAGgKAIgIQBghyCHACQCHAEBdB0QAmAuAVA2QlBg8kkChIABgGg");
	this.shape_49.setTransform(-258.7,-35.9);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FF0000").s().p("AAAAAIABAAIgBAAg");
	this.shape_50.setTransform(-289.2,-20.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("rgba(255,255,255,0.4)").s().p("AgTAkQgKgGAAgOQgBgOAJgOQAJgOAMgHQALgHAJAFQAJAGABAOQAAAOgIAOQgJAOgNAHQgFAEgGAAQgEAAgEgCg");
	this.shape_51.setTransform(-162.5,-42.2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("rgba(255,255,255,0.4)").s().p("AgmCCQgogmgShCQgShBARg2QAQg2AogLQAogKAoAnQAoAmASBCQASBBgRA2QgQA2goAKQgIADgJAAQgfAAgggfg");
	this.shape_52.setTransform(-141.3,-21.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AlCBJQFJjTE8Bg");
	this.shape_53.setTransform(-156,-14.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Ak+inIAAgBQAVhuA7hLQAGgKAIgJQBhhyCGACQCHAEBdB0QAmAwAVA2QAVAyAHA5QABAHABAGQACAdgBAdQgDCjhaCeQgwBUg+AiQgaAQgeAGQgOADgJAcQgDAMgGAFIAAADIAAABIAWAXIAKAAQgLAVgWAKQgBgBgBAAQgFADgEgBIAJgxIAAAiIADgoAAfGzIABgIQgPgKgEgMQgNgWgOgBQgBAAgCgBQgBABgBAAQgMgDgLgEQgSgGgKgEQgIgEgJgEQhOgshAhiQhSh6gMiEQgCgcABgcQgBgiAGggQABgEAAgCAlAigQABgFABgCAAfHVIAAAN");
	this.shape_54.setTransform(-155.9,-3.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("rgba(219,167,104,0.612)").s().p("AgEAYIAIgwIABAhIAAANQgEADgDAAIgCgBg");
	this.shape_55.setTransform(-153.3,42.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("rgba(100,38,0,0.612)").s().p("AgQAaIAAgMIACgoIAWAYIAJAAQgKATgVAKIgCgBg");
	this.shape_56.setTransform(-151.1,42);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FF77BB").s().p("AgBgMIABgIIACABIgBABIgBAng");
	this.shape_57.setTransform(-152.7,41.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],11.3,-23.8,0,11.3,-23.8,36.8).s().p("AAfEyQgPgKgEgMQgMgWgPgBIgDgBIgCABIgXgHIgcgKIgRgIQhOgshAhiQhSh5gMiFQFJjUE8BgIACANQACAdgBAdQgDCkhaCdQgwBUg+AiQgaAQgeAGQgOADgJAcQgDAMgGAFIAAADg");
	this.shape_58.setTransform(-155.8,8.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],9.4,-7.7,0,9.4,-7.7,23.1).s().p("AlDBAQAAgiAGgfQEkiiFBA9QAUAyAIA4Qk9hflIDTQgCgcAAgcg");
	this.shape_59.setTransform(-156.1,-19.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("rgba(196,46,9,0.612)").s().p("AABgDIgBAFIAAABIABgGg");
	this.shape_60.setTransform(-187.9,-20);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FF0000").s().p("AAAAAIABAAIgBAAg");
	this.shape_61.setTransform(-187.7,-20.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.rf(["#C7FA10","#00B606"],[0.031,1],8.1,2.4,0,8.1,2.4,26.2).s().p("AkxCbIABAAIgBAAQAVhvA7hKQAGgKAHgIQBhhyCHACQCHAEBdB0QAmAuAVA2QlBg8kkChIABgGgAkwCbIADgDIAAgDg");
	this.shape_62.setTransform(-157.2,-35.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("rgba(26,52,138,0.6)").s().p("EgrWALkQjPAAiTiTQiSiSAAjPIAAnfQAAjPCSiSQCTiTDPAAMBWtAAAQDPAACTCTQCSCSAADPIAAHfQAADPiSCSQiTCTjPAAg");
	this.shape_63.setTransform(-3.5,-3.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_63).wait(1));

	// Layer_2
	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("rgba(12,39,63,0.02)").s().p("EgrWAMWQjkAAihihQihihAAjkIAAnfQAAjkChihQChihDkAAMBWtAAAQDkAAChChQChChAADkIAAHfQAADkihChQihChjkAAg");
	this.shape_64.setTransform(-3.5,-3.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_64).wait(1));

}).prototype = getMCSymbolPrototype(lib.patterncopy2, new cjs.Rectangle(-336,-82.6,665,158), null);


(lib.choice1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ApmJmQj+j+AAloQAAlnD+j/QD/j+FnAAQFoAAD/D+QD+D/AAFnQAAFoj+D+Qj/D/loAAQlnAAj/j/g");
	this.shape.setTransform(-20.2,-5.2,0.98,0.98);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#999999").s().p("ApmJmQj+j+AAloQAAlnD+j/QD/j+FnAAQFoAAD/D+QD+D/AAFnQAAFoj+D+Qj/D/loAAQlnAAj/j/g");
	this.shape_1.setTransform(-20.2,-5.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#003366").s().p("AqGKHQkMkMAAl7QAAl6EMkMQEMkMF6AAQF7AAEMEMQEMEMAAF6QAAF7kMEMQkMEMl7AAQl6AAkMkMg");
	this.shape_2.setTransform(-20.2,-5.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 4
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0)").s().p("AqgKhQkXkXAAmKQAAmJEXkXQEWkXGKAAQGKAAEXEXQEXEXAAGJQAAGKkXEXQkXEXmKAAQmKAAkWkXg");
	this.shape_3.setTransform(-20.2,-5.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.choice1copy, new cjs.Rectangle(-115.4,-100.4,190.4,190.4), null);


(lib.choice1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// ans
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgXArQgMgHAAgRQgBgRALgRQAKgRAPgIQANgIALAGQALAHABAQQAAASgKAQQgKARgQAJQgHAFgHAAQgFAAgEgDg");
	this.shape.setTransform(-90.1,-47.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.4)").s().p("AgtCcQgxgugVhPQgVhOAThAQAThBAwgNQAwgNAwAvQAxAuAVBPQAVBOgTBBQgTBAgwANQgKADgLAAQgkAAgnglg");
	this.shape_1.setTransform(-64.7,-22.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AmCBYQGJj+F8B0");
	this.shape_2.setTransform(-78.7,-13.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Al+jJIAAAAQAZiFBHhaQAHgMAKgKQB0iJChACQCjAFBvCLQAtA5AaBBQAZA9AIBEQABAIACAIQABAjAAAiQgEDEhsC9Qg5BlhKApQggATgjAIQgRADgMAhQgDAPgIAGIABADIgBACIAcAcIAKAAQgMAZgbALQAAAAgCAAQgFADgGgBIALg7IABgJQgSgNgFgOQgPgbgRAAQgCgBgCAAQgBAAgBAAQgPgDgNgFQgVgHgMgFQgKgEgKgFQheg1hNh2QhiiTgOieQgDghABgiQgBgpAHgnQABgEAAgDgAmAjAQABgGABgDAAlIzIAAAQAAlIKIAAApIADgw");
	this.shape_3.setTransform(-78.6,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(219,104,148,0.612)").s().p("AgFAdIALg6IAAAoIAAAQQgFADgEAAIgCgBg");
	this.shape_4.setTransform(-75.5,55.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(100,0,38,0.612)").s().p("AgTAgIAAgQIACgvIAbAcIAKAAQgMAYgaALIgBAAg");
	this.shape_5.setTransform(-72.8,54.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF77BB").s().p("AgBgPIABgJIACAAIgBACIgCAvg");
	this.shape_6.setTransform(-74.7,53.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FCDCF1","#D22C85"],[0.031,1],13.6,-28.5,0,13.6,-28.5,44.2).s().p("AAlFwQgSgNgFgOQgOgbgSAAIgEgBIgCAAQgPgDgNgFIghgMIgUgJQheg1hNh2QhiiSgOifQGJj/F8B0IADAQQABAjAAAiQgEDFhsC8Qg5BlhKApQggATgjAIQgRADgMAhQgDAPgIAGIABADg");
	this.shape_7.setTransform(-78.5,14.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFFF00","#FF9900"],[0.031,1],11.3,-9.2,0,11.3,-9.2,27.7).s().p("AmDBNQgBgpAHgmQFfjCGBBJQAZA9AIBDQl8hzmJD+QgDghABgig");
	this.shape_8.setTransform(-78.8,-19.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(196,46,9,0.612)").s().p("AABgDIgBAGIAAABIABgHg");
	this.shape_9.setTransform(-117,-19.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AAAAAIABAAIgBAAg");
	this.shape_10.setTransform(-116.8,-20.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FEBDAB","#FF0000"],[0.031,1],9.7,2.9,0,9.7,2.9,31.5).s().p("AluC7IACgBIgCAAQAZiFBHhZQAHgMAJgKQB0iJCiADQCiAFBvCLQAuA4AaBBQmChJlfDCIACgHgAlsC6IADgEQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBAAAAg");
	this.shape_11.setTransform(-80.2,-38.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("ApZJaQj6j5AAlhQAAlgD6j5QD5j6FgAAQFhAAD5D6QD6D5AAFgQAAFhj6D5Qj5D6lhAAQlgAAj5j6g");
	this.shape_12.setTransform(-74.3,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#999999").s().p("AqGKHQkMkMAAl7QAAl6EMkMQEMkMF6AAQF7AAEMEMQEMEMAAF6QAAF7kMEMQkMEMl7AAQl6AAkMkMg");
	this.shape_13.setTransform(-74.3,0,0.95,0.95);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#003366").s().p("AqGKHQkMkMAAl7QAAl6EMkMQEMkMF6AAQF7AAEMEMQEMEMAAF6QAAF7kMEMQkMEMl7AAQl6AAkMkMg");
	this.shape_14.setTransform(-74.3,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer 4
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(255,255,255,0)").s().p("AqgKhQkXkXAAmKQAAmJEXkXQEXkXGJAAQGKAAEXEXQEXEXAAGJQAAGKkXEXQkXEXmKAAQmJAAkXkXg");
	this.shape_15.setTransform(-74.3,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(1));

}).prototype = getMCSymbolPrototype(lib.choice1, new cjs.Rectangle(-169.5,-95.2,190.4,190.4), null);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgkA/QgQgKAAgYQgCgaAQgZQAPgaAXgMQAUgLAQAIQAQAKABAXQAAAcgOAZQgPAagYALQgLAHgKAAQgIAAgHgEg");
	this.shape.setTransform(222.4,-69.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.4)").s().p("AhCDoQhKhFggh1QgfhzAdhgQAchhBIgTQBGgTBIBGQBJBEAfB1QAgB0gdBgQgbBhhIASQgQAEgPAAQg3AAg4g2g");
	this.shape_1.setTransform(260,-32.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("Ao9CCQJJl5IyCs");
	this.shape_2.setTransform(233.9,-21.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("Ao3kqIABgBQAljFBoiFQALgSAPgPQCrjLDxAEQDwAHCmDPQBDBUAlBgQAlBaANBlQABAMADALQACA0gBAzQgGEjigEZQhVCVhtA8QgvAcg1ANQgZAEgQAxQgGAWgMAKIABAEIgBABIApAqIAQAAQgTAmgmARQgBgBgCAAAo6kdQACgJABgEAA3MHIABgPQgZgTgJgUQgWgngagBQgCgBgDgCQgCACgCAAQgVgGgTgGQghgMgRgGQgOgHgRgHQiKhPhyitQiSjbgVjrQgDgyAAgyQAAg+AKg4QABgGAAgEAA4NCIAAAYQgKAEgIAAIARhXIABA7IADhI");
	this.shape_3.setTransform(234,-1.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(219,167,104,0.612)").s().p("AgIArIAQhVIABA6IAAAYQgJADgGAAIgCAAg");
	this.shape_4.setTransform(238.7,80.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF77BB").s().p("AgCgVIACgPIADAAIgBACIgDBHg");
	this.shape_5.setTransform(239.8,78.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(100,38,0,0.612)").s().p("AgdAwIAAgYIADhHIAoAqIAQAAQgTAlglAQIgDAAg");
	this.shape_6.setTransform(242.6,79.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],20.2,-42.3,0,20.2,-42.3,65.5).s().p("AA3IhQgagTgJgUQgUgngcgBIgFgDIgDACIgpgMIgygSQgOgHgQgHQiKhPhyitQiSjagVjsQJJl6IyCsIAEAXQADA0gBAzQgHEkifEYQhVCVhuA8QgvAcg0ANQgZAEgRAxQgGAWgLAKIABAEg");
	this.shape_7.setTransform(234.2,20.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],16.8,-13.7,0,16.8,-13.7,41.1).s().p("Ao/ByQAAg+AKg3QIHkgI8BsQAlBaANBkQoziqpJF5QgDgzAAgxg");
	this.shape_8.setTransform(233.7,-29.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(196,46,9,0.612)").s().p("AACgGIgBAJIgCADIADgMg");
	this.shape_9.setTransform(177.1,-30.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AAAAAIACAAIgDAAg");
	this.shape_10.setTransform(177.4,-31.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#C7FA10","#00B606"],[0.031,1],14.5,4.4,0,14.5,4.4,46.7).s().p("AogEVIAEgBIAFgFQABgDgBgEIgFAMIgDAAQAljFBoiFQALgSAPgOQCrjLDxAEQDwAGCmDPQBDBUAlBfQo8hroHEfIABgKg");
	this.shape_11.setTransform(231.7,-58.7);

	this.choice1 = new lib.choice1copy();
	this.choice1.name = "choice1";
	this.choice1.parent = this;
	this.choice1.setTransform(241.3,-10.4,1.439,1.457,0,0,0,-16.8,-12.4);

	this.choice1_1 = new lib.choice1();
	this.choice1_1.name = "choice1_1";
	this.choice1_1.parent = this;
	this.choice1_1.setTransform(-230,-0.1,1.439,1.457,0,0,0,-73.1,-0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.choice1_1},{t:this.choice1},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-368.8,-138.7,742.2,277.5);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween17("synched",0);
	this.instance.parent = this;
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},7).to({startPosition:0},8).to({alpha:0},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-474.9,-91.9,950,183.9);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween15("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1},13).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.6,-79.9,107.3,159.9);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(62.1,-1.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.02,scaleY:1.02},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-173.4,-34.6,472.1,66.6);


(lib.pattern = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.question = new lib.patterncopy2();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(-0.7,0.5,1.014,1.014,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.question).wait(1));

}).prototype = getMCSymbolPrototype(lib.pattern, new cjs.Rectangle(-341.5,-83.4,674.1,160.2), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(1050.6,299.5,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(305).to({_off:false},0).wait(30).to({startPosition:13},0).to({alpha:0,startPosition:2},5).wait(10));

	// Layer_10
	this.instance_1 = new lib.Tween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1044.3,300.4,0.9,0.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(296).to({_off:false},0).to({regX:0.1,regY:0.1,scaleX:1.14,scaleY:1.14,y:300.5},13).to({startPosition:0},17).to({alpha:0},8).wait(16));

	// Layer_4
	this.instance_2 = new lib.Tween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(875.2,574.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(266).to({_off:false},0).to({scaleX:0.9,scaleY:0.9,x:1044.3,y:300.4},20).wait(42).to({x:1047.3,y:304},0).to({alpha:0},5).to({_off:true},1).wait(16));

	// Layer_9
	this.instance_3 = new lib.Symbol1copy("synched",12);
	this.instance_3.parent = this;
	this.instance_3.setTransform(1003.8,643.8,1,1,0,0,0,190.5,64.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(236).to({_off:false},0).to({_off:true},30).wait(84));

	// Layer_3
	this.instance_4 = new lib.Symbol1("synched",12);
	this.instance_4.parent = this;
	this.instance_4.setTransform(689.3,463.6,1,1,-48.4,0,0,41.1,60.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(206).to({_off:false},0).to({_off:true},30).wait(114));

	// Layer_12
	this.instance_5 = new lib.Symbol8("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(640.5,305.6);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(106).to({_off:false},0).to({_off:true},76).wait(168));

	// Layer_11
	this.instance_6 = new lib.Symbol7("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(1050.3,305.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(103).to({_off:false},0).to({_off:true},83).wait(164));

	// Layer_5
	this.instance_7 = new lib.Symbol6("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(705,114,1,1,0,0,0,0.5,0.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(101).to({_off:false},0).to({_off:true},85).wait(164));

	// Layer_7
	this.instance_8 = new lib.Tween1("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,576.3);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(74).to({_off:false},0).to({alpha:1},5).wait(158).to({startPosition:0},0).to({startPosition:0},91).to({alpha:0},5).to({_off:true},1).wait(16));

	// Layer_6
	this.question = new lib.pattern();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(645.7,308.6,1.584,1.584);
	this.question.alpha = 0;
	this.question._off = true;

	this.instance_9 = new lib.Tween12("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(638.6,303.4);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question).wait(46).to({_off:false},0).to({alpha:1},7).to({_off:true},275).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(328).to({_off:false},0).to({alpha:0},5).to({_off:true},1).wait(16));

	// Layer_2
	this.instance_10 = new lib.qtext();
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,112.2,1.679,1.679);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(21).to({_off:false},0).to({alpha:1},6).wait(301).to({alpha:0},5).to({_off:true},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(547.6,296.2,1468,850);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;