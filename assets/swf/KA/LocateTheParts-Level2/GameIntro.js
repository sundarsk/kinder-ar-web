(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgsBsQgVgJgOgOQgOgOgHgWQgIgUAAgaQAAgYAJgVQAJgWAOgQQAPgQAUgJQAUgKAXAAQAUAAATAJQASAIAOAOQAPAOAJAUQAJAUADAWIitAZQABAOAGAMQAGAKAIAJQAJAHAMAEQALAEANABQAKgBALgDQAKgDAJgHQAJgFAGgKQAHgKADgMIAmAIQgFASgKAPQgLAPgNAKQgOALgQAGQgRAFgRABQgagBgUgHgAgQhNQgKADgJAHQgKAHgHALQgIAMgDASIB4gPIgBgDQgIgUgNgMQgOgLgTAAQgIAAgKADg");
	this.shape.setTransform(188,5.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AhaAxIgBgxIgCgnIgBgdIgBgqIAtgBIABAZIABAYIAAAXIANgUQAJgLAKgJQALgKAMgIQAMgHAOgCQAQAAANAHQAGADAFAGQAGAFAFAIQAFAJADALQADALABAQIgoAPQAAgKgBgHIgDgMIgGgJIgGgGQgHgEgJAAQgFABgHADQgGAFgGAFQgHAGgGAJIgNAPIgLAQIgKAOIAAAdIABAbIABAXIAAARIgrAFIgCg/g");
	this.shape_1.setTransform(165.7,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgsBsQgVgJgOgOQgOgOgHgWQgIgUAAgaQAAgYAJgVQAJgWAOgQQAPgQAUgJQAUgKAXAAQAUAAATAJQASAIAOAOQAPAOAJAUQAJAUADAWIitAZQABAOAGAMQAGAKAIAJQAJAHAMAEQALAEANABQAKgBALgDQAKgDAJgHQAJgFAGgKQAHgKADgMIAmAIQgFASgKAPQgLAPgNAKQgOALgQAGQgRAFgRABQgagBgUgHgAgQhNQgKADgJAHQgKAHgHALQgIAMgDASIB4gPIgBgDQgIgUgNgMQgOgLgTAAQgIAAgKADg");
	this.shape_2.setTransform(141.3,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AA2CZQAGgUACgSIAEghQABgQgBgQQgBgVgGgPQgHgOgJgKQgJgJgLgEQgMgFgKAAQgLABgMAGQgKAFgLAKQgLAJgKASIgBCFIgoABIgDk0IAugCIgBB6QALgMAMgHQANgHALgDQAMgEALgCQAXAAASAIQASAIAOAPQANAPAIAWQAHAUABAcIAAAdIgBAdIgDAbQgCANgDAKg");
	this.shape_3.setTransform(115.8,0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AhdAyIgCgyIgBgmIgBgdIgBgrIAtgBIABA6QAHgLAIgKQAJgLAJgHQAKgIALgFQALgFAMgBQAPgBALAEQAMAEAIAGQAIAGAFAJQAFAJADAJIAFATIABARQABAhAAAjIgCBIIgpgBIADhBQABgggBggIgBgJIgCgMIgFgNQgDgGgEgFQgFgFgHgCQgHgDgJACQgQACgPAVQgRAVgTAnIACAzIABAdIAAAQIgrAFIgCg+g");
	this.shape_4.setTransform(78.5,5.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AAHgnIgwCLIg8AEIgnjOIApgEIAfCoIA7ioIAmADIApCpIAbirIAsACIgoDQIg7ACg");
	this.shape_5.setTransform(50.6,5.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgpBtQgTgIgPgQQgOgPgJgWQgJgVAAgbQAAgaAJgVQAJgWAOgQQAPgPATgIQAUgJAVAAQAWAAATAJQAUAJAPAQQAOAPAJAWQAJAWgBAYQABAZgJAWQgJAVgOAQQgPAQgUAJQgTAJgWAAQgVAAgUgJgAgdhKQgMAHgHAMQgJAMgDAPQgDAOgBAOQAAAOAFAPQADAOAIAMQAIAMAMAHQAMAIAQAAQARAAAMgIQAMgHAJgMQAHgMAEgOQAEgPAAgOQAAgOgEgOQgDgPgIgMQgIgMgMgHQgMgIgSAAQgQAAgNAIg");
	this.shape_6.setTransform(22.9,5.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AA2CZQAGgUACgSIAEghIAAggQgBgVgGgPQgHgOgJgKQgJgJgLgEQgMgFgKAAQgLABgMAGQgKAFgLAKQgLAJgKASIgBCFIgoABIgDk0IAugCIgBB6QALgMAMgHQANgHALgDQAMgEALgCQAXAAASAIQASAIAOAPQANAPAIAWQAHAUABAcIAAAdIgBAdIgDAbQgCANgDAKg");
	this.shape_7.setTransform(-2.6,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgcBxIgRgEIgSgGQgKgDgJgFQgJgFgJgHIAUgeIAXALQALAFAMAEQAMAEANADQALACAOAAQALAAAHgCQAIgCAFgDIAIgHIADgIIABgHQAAgEgCgEQgCgEgEgDQgEgEgGgDQgGgEgKgCQgJgCgNAAQgRgBgQgDQgRgDgNgGQgNgGgJgJQgIgLgCgPQgCgOAEgMQADgMAHgKQAHgJAJgHQAKgHAMgFQALgFANgCQANgCANAAIASABQALAAALADQAMACAMAFQALAEALAIIgOAkQgNgHgMgDIgVgHQgLgCgKgBQgegCgSAJQgRAIAAARQAAAMAGAGQAHAFALADQAMACAPABIAgADQATADAOAGQANAFAIAIQAIAIAEAKQAEAKAAAKQAAATgIANQgIANgNAIQgNAIgQAEQgRAEgSABQgSAAgSgEg");
	this.shape_8.setTransform(-27.9,5.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgsBsQgVgJgOgOQgOgOgHgWQgIgUAAgaQAAgYAJgVQAJgWAOgQQAPgQAUgJQAUgKAXAAQAUAAATAJQASAIAOAOQAPAOAJAUQAJAUADAWIitAZQABAOAGAMQAGAKAIAJQAJAHAMAEQALAEANABQAKgBALgDQAKgDAJgHQAJgFAGgKQAHgKADgMIAmAIQgFASgKAPQgLAPgNAKQgOALgQAGQgRAFgRABQgagBgUgHgAgQhNQgKADgJAHQgKAHgHALQgIAMgDASIB4gPIgBgDQgIgUgNgMQgOgLgTAAQgIAAgKADg");
	this.shape_9.setTransform(-61.9,5.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AhaAxIgBgxIgCgnIgBgdIgBgqIAtgBIABAZIAAAYIAAAXIAPgUQAIgLAKgJQALgKAMgIQAMgHAOgCQAQAAAOAHQAFADAFAGQAGAFAFAIQAEAJAEALQADALABAQIgoAPQABgKgCgHIgDgMIgGgJIgGgGQgHgEgJAAQgFABgHADQgGAFgGAFQgHAGgGAJIgNAPIgLAQIgLAOIABAdIABAbIABAXIABARIgsAFIgCg/g");
	this.shape_10.setTransform(-84.3,5.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgVBwQgSgBgNgGQgNgGgJgLQgJgKgFgNQgFgOgDgPQgDgPgBgQIgBgdQAAgVACgVQACgWADgWIApABQgDAZgBAUIgBAjIAAARIACAVQABAMADALQADAMAFAJQAFAIAIAFQAHAFAMgCQAOgCARgTQARgTASgmIgBgzIgBgdIAAgRIArgFIACA/IACAxIABAnIABAcIABArIguABIgBg6QgGAMgJALQgIAKgKAIQgKAIgLAEQgJAFgLAAIgCAAg");
	this.shape_11.setTransform(-109.6,5.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgXgXIhGACIABglIBGgCIABhYIAmgCIgBBZIBOgCIgDAmIhLABIgCCuIgpABg");
	this.shape_12.setTransform(-131.9,1.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AgkBsQgWgJgQgQQgRgPgJgWQgKgVAAgZQAAgOAEgOQAEgPAHgNQAIgNALgLQAKgLANgIQANgIAQgEQAPgFAPAAQASAAAQAEQAPAFAOAIQANAHALAMQAKALAIAPIAAAAIghAVIgBgCQgFgKgHgIQgIgIgJgGQgJgFgLgDQgKgDgMAAQgPAAgOAGQgPAGgKALQgLALgGAOQgHAPAAAPQAAAQAHAPQAGAOALALQAKALAPAGQAOAGAPAAQALAAAKgCQAKgDAJgFQAJgGAHgHQAIgHAFgJIABgBIAjATIgBACQgIANgLAKQgLALgNAHQgOAIgPAEQgQAEgQAAQgVAAgVgJg");
	this.shape_13.setTransform(-154.1,5.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgZCXIABg0IACg/IAAhVIApgBIgBAzIgBAsIgBAjIgBAbIAAAsgAgLhfIgJgGQgDgEgCgFQgDgFAAgGQAAgGADgFQACgFADgEQAFgEAEgCQAGgDAFAAQAFAAAGADQAFACAEAEQAEAEACAFQADAFAAAGQAAAGgDAFQgCAFgEAEQgEAEgFACQgGACgFAAQgFAAgGgCg");
	this.shape_14.setTransform(-171.4,0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AhuiiIAsgBIAAAcQALgKANgHQAMgGALgDQAMgEAKAAQAOAAAOADQANAEAMAHQAMAHAKAKQAKALAIAMQAHAMAEAOQAEAOgBAQQgBASgEAOQgFAQgHAMQgIAMgKAKQgKAKgLAHQgLAIgMADQgNAEgMAAQgLgBgNgDQgLgDgNgFQgNgGgNgLIgBCIIglABgAAAh9QgMAAgLAFQgLAEgJAHQgIAHgGAKQgHAJgDAKIAAAmQADAOAGAKQAHAJAJAGQAIAHAKAEQALADAKAAQANAAAMgFQANgFAJgJQAJgJAGgOQAFgNABgOQABgOgFgOQgEgOgJgKQgJgKgMgGQgMgGgNAAIgCAAg");
	this.shape_15.setTransform(-189.5,10.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("EggUAElQhrAAABigIAAkJQgBigBrAAMBApAAAQBrAAgBCgIAAEJQABCghrAAg");
	this.shape_16.setTransform(0,2.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-217.5,-33,435.1,66);


(lib.Tween25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(77,140,15,0.996)").s().p("AghMAQAIiYAGicQAAgSgIgDQgDgBAAgFIAFAAQAIhAgEhAQgLAGgSgDQhngMhIguQh+hPhxhbIAAAEIAAACQgBgMgIgDQAFAAAEABIADAAQCHBGB/BOQBMAtBmATQAEAAAAgDQACgTgDgMIAAgCQhshfhLiBQhMiCgvicIgsiVICAEaQBACLBVB4QAhAvAoAsQAJjEABjIQACjMAFjGIAFi+QAPC+AGDJQAGDDADDKIADDNQBfhvA9iQQA9iPBEiGQAVgrAfgjQg2CXg/COQg/CMhSB8QgjA1goA0QBUgLBDgnQCBhLCFhIQALgGANgDQhvBdiBBMQhMAshmATQgJABgKAAQAAA+ACA9IABAAQgEAXAAAbQgCCTgECPg");
	this.shape.setTransform(-1.1,1.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(104,170,39,0.996)").s().p("AguJrQg3gCgpgRQhxgphRhJIgdgYQhQhIgjh2IAAgCIAAgEQBwBbB+BPQBIAtBoANQASADALgGQAEA/gJBBgAAXJmQgCg9AAg+QAKAAAIgBQBngTBLgsQCChNBvhcQgOADgLAGQiEBIiBBKQhDAnhUAMQAog0Ajg2QBSh7A/iNQA/iNA2iXQgfAjgWArQhECFg8CPQg+CQheBwIgDjOQgDjKgGjDQgHjJgNi9IgHC+QgEDFgCDLQgCDKgIDEQgogsgigvQhUh5hBiLIiAkZIAsCVQAwCbBMCDQBLCABsBgIAAACQADAMgCASQAAADgEABQhmgThNguQh+hNiHhGQAhgOArgGIAMgDQgTi7ABjFQAAg0ASgeQAlgIAcAVQAbAUAfAOQANgBAKgBQAug7AXhRQArieBfhvQALgMAOgLQBdA8AoBzQApB4AnB9QBBgDA4gXQAVgKATAHQANAogFA5QgKCeAGClIABANQAgASA1gDQATAAAJAJQAAAsgUAjQhLCCh7BSQhkBBiLAcg");
	this.shape_1.setTransform(0.5,-16.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-48.5,-78.6,97.2,157.3);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#336633").s().p("Eg1zAVfQiwAAh9h8Qh9h9ABiwIAA9rQgBiwB9h9QB9h8CwAAMBrnAAAQCwAAB9B8QB9B9gBCwIAAdrQABCwh9B9Qh9B8iwAAg");
	this.shape.setTransform(-0.7,8.3,0.885,1.111);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Eg1zAWqQjPAAiTiSQiTiTAAjPIAA9rQAAjPCTiTQCTiSDPAAMBrnAAAQDQAACSCSQCTCTAADPIAAdrQAADPiTCTQiSCSjQAAg");
	this.shape_1.setTransform(-0.7,8.3,0.885,1.111);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-349.6,-152.8,697.9,322.3);


(lib.ro2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("Ai0KqQhGhGAAhtQAAhqBJhIQBIhKBoAAQBnAABLBLQBKBLgBBnQABBnhKBJQhKBIhoAAQhtAAhGhGgAjLDDIAAhCQAAiDAeg/QAdg/BOgjIBHgfQBRglAAhBQAAgmgagaQgbgagkAAQhpAAAACPIlrAAQAAjABZh4QBAhaBpg1QBpg1BuAAQDGAACJCEQCHCCAADDQAADujDBhQg/AfgOAXQgNAYAABMg");
	this.shape.setTransform(113.1,116.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("AizKqQhHhHAAhsQAAhqBJhIQBIhKBpAAQBmAABLBLQBKBLAABnQAABnhKBJQhKBIhnAAQhtAAhGhGgAjLDDIAAhCQAAiDAeg/QAdg/BOgjIBHgfQBRglAAhBQAAgmgagaQgbgagkAAQhoAAAACQIlsAAQAAjBBZh5QBBhaBog0QBpg1BuAAQDHAACICEQCHCCAADDQAADujCBgQhAAhgOAWQgNAYAABMg");
	this.shape_1.setTransform(118.9,117.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000033").ss(5,1,1).p("ATGAAQAAH6lmFmQllFmn7AAQn5AAlmlmQlmlmAAn6QAAn5FmlmQFmlmH5AAQH7AAFlFmQFmFmAAH5g");
	this.shape_2.setTransform(115.9,116.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AtgNgQlllmAAn6QAAn5FllmQFmlmH6AAQH6AAFmFmQFmFmAAH5QAAH6lmFmQlmFmn6AAQn6AAlmlmg");
	this.shape_3.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AuLOLQl3l4AAoTQAAoTF3l4QF4l3ITAAQIUAAF3F3QF4F4AAITQAAITl4F4Ql3F4oUAAQoTAAl4l4g");
	this.shape_4.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ro2copy3, new cjs.Rectangle(-12.4,-11.5,256.6,256.6), null);


(lib.ro2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(77,140,15,0.996)").s().p("AghLxQAIiVAGiZQAAgRgIgDQgCgCAAgEIAEAAQAIg/gDg/QgLAGgSgDQhmgMhGgtQh7hNhvhaIAAAFIAAABQgCgMgHgDIAJACIADAAQCFBEB7BNQBLAsBlASQABAAAAAAQABAAAAgBQABAAAAAAQAAgBAAgBQACgSgDgMIAAgBQhqhehJh/QhKh/gviZIgriSIB+EUQA+CJBUB2QAgAuAnArQAJjAABjFQACjIAFjCIAEi6QAQC6AGDFQAGC/ADDGIACDKQBdhtA8iNQA7iNBDiDQAWgqAegiQg1CUg/CLQg9CJhRB5QghA1goAyQBTgKBCgnQB+hJCChGQAKgGAOgDQhtBbh/BKQhKAshlASIgSABQABA9ABA8IABAAQgEAXAAAaQgCCQgECMg");
	this.shape.setTransform(115.7,119.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(104,170,39,0.996)").s().p("AgtJfQg2gCgogQQhvgphPhHIgcgYQhQhGgih0IAAgCIAAgFQBvBaB7BOQBHAsBlAMQASADALgFQAEA+gJA/gAAWJaQgBg8AAg8IASgCQBkgSBKgrQB/hLBthbQgNADgLAGQiCBGh+BJQhCAnhSALQAngzAig1QBRh5A9iKQA+iKA1iUQgeAjgVApQhDCCg8CNQg8COhcBtIgDjJQgDjHgGi/QgGjEgOi7IgGC7QgFDCgBDGQgCDGgIDBQgngsghguQhUh2g+iKIh+kSIArCSQAvCXBKCAQBKB/BqBeIAAACQACAMgBARQAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAQhlgShKgsQh8hNiFhEQAhgNAqgHIAMgCQgUi4ACjBQAAgzASgeQAjgHAdAUQAZAUAgANQAMABAKgCQAtg6AWhPQAricBdhtQAKgMAOgKQBbA7AnBxQApB1AmB7QA/gDA3gWQAVgKAUAHQAMAmgFA4QgKCcAGChIABAOQAfARA1gDQASAAAJAJQAAArgUAiQhJB/h5BRQhiBBiIAag");
	this.shape_1.setTransform(117.3,101.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000033").ss(5,1,1).p("ATGAAQAAH6lmFmQllFmn7AAQn5AAlmlmQlmlmAAn6QAAn5FmlmQFmlmH5AAQH7AAFlFmQFmFmAAH5g");
	this.shape_2.setTransform(115.9,116.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AtgNgQlllmAAn6QAAn5FllmQFmlmH6AAQH6AAFmFmQFmFmAAH5QAAH6lmFmQlmFmn6AAQn6AAlmlmg");
	this.shape_3.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AuLOLQl3l4AAoTQAAoTF3l4QF4l3ITAAQIUAAF3F3QF4F4AAITQAAITl4F4Ql3F4oUAAQoTAAl4l4g");
	this.shape_4.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ro2copy2, new cjs.Rectangle(-12.4,-11.5,256.6,256.6), null);


(lib.ro1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000033").ss(5,1,1).p("ATGAAQAAH6lmFmQllFmn7AAQn5AAlmlmQlmlmAAn6QAAn5FmlmQFmlmH5AAQH7AAFlFmQFmFmAAH5g");
	this.shape.setTransform(115.9,116.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AtgNgQlllmAAn6QAAn5FllmQFmlmH6AAQH6AAFmFmQFmFmAAH5QAAH6lmFmQlmFmn6AAQn6AAlmlmg");
	this.shape_1.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AuLOLQl3l4AAoTQAAoTF3l4QF4l3ITAAQIUAAF3F3QF4F4AAITQAAITl4F4Ql3F4oUAAQoTAAl4l4g");
	this.shape_2.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.ro1copy3, new cjs.Rectangle(-12.4,-11.5,256.6,256.6), null);


(lib.ro1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EEAB69").s().p("AgsLfQhXglhChDQgJgGgIgIIlullQgkgjAAgyQAAgyAkgjQAkgjAyAAQAzAAAkAjIBxBvIAAprQAAg0AUgdQAVgdAmAAQBLAAAABuIAAFnIAtgeQAegSASgJIAAorQAAgwAVgbQAVgaAlAAQAlAAAWAeQAWAeAAAxIAAIAIAegBQAYAAAYACIAAn7QAAgvAaghQAbghAlAAQAlAAAaAhQAbAhAAAvIAAIyQAaAOA+AyIAElaQAAgyARgZQASgbAjAAQAlAAAXAbQAXAcAAAvIAAJ7IgBAOQACAWAAAQQAABeglBWQgkBThCBAQhBBBhVAjQhYAkhhAAQhiAAhZgmg");
	this.shape.setTransform(109.8,108.7,0.98,0.98);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000033").ss(5,1,1).p("ATGAAQAAH6lmFmQllFmn7AAQn5AAlmlmQlmlmAAn6QAAn5FmlmQFmlmH5AAQH7AAFlFmQFmFmAAH5g");
	this.shape_1.setTransform(115.9,116.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AtgNgQlllmAAn6QAAn5FllmQFmlmH6AAQH6AAFmFmQFmFmAAH5QAAH6lmFmQlmFmn6AAQn6AAlmlmg");
	this.shape_2.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AuLOLQl3l4AAoTQAAoTF3l4QF4l3ITAAQIUAAF3F3QF4F4AAITQAAITl4F4Ql3F4oUAAQoTAAl4l4g");
	this.shape_3.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.ro1copy2, new cjs.Rectangle(-12.4,-11.5,256.6,256.6), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween38 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003399").s().p("AgZBkIgPgEIgQgFIgQgHQgJgFgGgFIARgbIATAKIAVAIQALADALACQAKACAMABQAJAAAHgCQAHgCAEgDQAEgDADgDIADgHQACgDgBgDIgCgHIgFgHQgDgEgGgCQgFgDgJgBQgIgDgLAAQgPgBgOgCQgPgDgMgFQgLgFgIgIQgHgKgCgNQgBgMADgLQACgLAHgIQAFgIAJgHQAJgGAKgEQAKgEAMgCQALgCAKgBIARABIATADIAVAGQAKAEAKAHIgMAgQgNgGgKgDIgTgGIgSgCQgagCgQAHQgPAIAAAOQAAALAGAFQAFAFALACIAXADQANAAAPADQARACAMAGQAMAEAHAHQAHAHADAJQADAIABAKQAAAQgHAMQgHAMgLAGQgMAIgOADQgPADgQABQgPAAgRgDg");
	this.shape.setTransform(186.1,4.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#003399").s().p("AgnBfQgSgIgNgMQgMgNgGgSQgHgSAAgXQAAgVAIgTQAHgTANgPQANgNASgJQASgIATAAQATAAAQAIQAQAGANANQAMAMAIASQAIARADAUIiYAWQABAMAFAKQAFAKAHAHQAIAHAKADQAKAEALAAQAKAAAJgDQAJgDAIgFQAHgGAGgIQAGgJADgKIAiAGQgFARgJANQgJANgMAJQgMAJgPAGQgOAFgQAAQgWAAgSgHgAgOhEQgJACgIAHQgJAGgGAKQgHAKgCAQIBpgNIgBgEQgHgRgLgKQgMgKgSAAQgGABgJACg");
	this.shape_1.setTransform(165.6,4.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#003399").s().p("AhPAsIgCgsIgBgiIAAgaIgCglIApgBIAAAWIAAAVIAAAVIANgSQAHgJAKgJQAJgJAKgGQALgHAMgBQAPgBALAHIAKAHQAFAFAEAHQAEAIADAKQADAKABAOIgjANIgBgPIgEgLIgEgHIgGgGQgGgEgIAAQgEABgGADQgGAEgFAFIgLAMIgMAOIgKAPIgJALIABAbIAAAYIABAUIABAOIgmAFIgCg3g");
	this.shape_2.setTransform(145.8,4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#003399").s().p("AgTBiQgQAAgLgGQgLgFgIgJQgIgJgEgMQgFgMgDgNQgCgOgBgNIgBgaIACgmIAEgmIAlABQgDAWgBARIgBAfIAAAPIABATQABAKADAKQACAKAFAIQAEAIAHAEQAHAFAKgCQANgCAPgRQAOgQARgiIgBgtIgBgaIAAgOIAmgEIABA3IACArIABAiIABAZIABAmIgpAAIAAgyQgGAKgIAKQgHAJgJAHQgIAHgKAEQgIADgJAAIgDAAg");
	this.shape_3.setTransform(123.5,4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#003399").s().p("AgVgUIg9ACIABghIA9gCIABhNIAigCIgBBPIBFgDIgDAiIhCABIgBCZIglABg");
	this.shape_4.setTransform(103.9,0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#003399").s().p("AgfBfQgUgHgOgPQgPgNgIgTQgJgTAAgWQAAgMAEgNQADgMAHgMQAGgLAKgLQAKgJALgHQALgHAOgEQANgEAOAAQAPAAAOADQAOAFAMAGQAMAHAJAKQAJALAHANIAAAAIgdASIAAgBQgFgKgGgGQgHgIgIgFQgIgEgKgDQgJgDgKAAQgOAAgMAGQgNAFgJAKQgJAJgGANQgFAMgBAOQABAPAFAMQAGANAJAKQAJAJANAFQAMAGAOAAQAKAAAIgCQAJgDAIgEQAIgFAGgGQAHgHAFgIIAAgBIAfASIgBABQgHALgKAJQgJAJgMAIQgMAGgNADQgOAEgOAAQgTAAgSgIg");
	this.shape_5.setTransform(84.3,4.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#003399").s().p("AgWCFIABguIABg3IABhLIAkgBIgBAtIgBAmIAAAgIgBAXIAAAngAgJhUQgEgCgEgDQgDgDgCgFQgCgFAAgEQAAgGACgEQACgFADgDQAEgEAEgCQAFgCAEAAQAFAAAFACQAEACAEAEQADADACAFQACAEAAAGQAAAEgCAFQgCAFgDADQgEADgEACQgFACgFABQgEgBgFgCg");
	this.shape_6.setTransform(69,-0.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#003399").s().p("AhgiPIAngBIgBAaQAKgKALgGQALgFAJgDQALgDAJgBQAMAAAMAEQAMADALAGQAKAHAJAIQAJAJAHALQAGALAEAMQADANgBAOQAAAPgEANQgEAOgHAKQgGALgJAJQgJAJgKAHQgKAGgLADQgKADgLAAQgKAAgLgDQgKgCgLgGQgMgFgLgKIgBB4IghABgAgUhqQgJAEgIAGQgIAGgFAJQgGAIgCAJIgBAiQADALAGAJQAFAJAIAFQAIAGAJADQAJADAJAAQALAAALgEQALgFAIgIQAIgIAFgLQAFgMABgNQAAgNgDgMQgFgMgHgJQgIgJgLgFQgLgFgNAAQgKAAgKAEg");
	this.shape_7.setTransform(53,8.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#003399").s().p("AgnBfQgSgIgNgMQgMgNgGgSQgHgSAAgXQAAgVAIgTQAHgTANgPQANgNASgJQASgIATAAQATAAAQAIQAQAGANANQAMAMAIASQAIARADAUIiYAWQABAMAFAKQAFAKAHAHQAIAHAKADQAKAEALAAQAKAAAJgDQAJgDAIgFQAHgGAGgIQAGgJADgKIAiAGQgFARgJANQgJANgMAJQgMAJgPAGQgOAFgQAAQgWAAgSgHgAgOhEQgJACgIAHQgJAGgGAKQgHAKgCAQIBpgNIgBgEQgHgRgLgKQgMgKgSAAQgGABgJACg");
	this.shape_8.setTransform(20.8,4.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#003399").s().p("AAvCHQAFgRADgQIADgdIAAgcQgBgTgFgOQgGgMgIgIQgIgJgKgDQgKgEgKAAQgJABgKAFQgJAEgKAJQgKAIgJAQIgBB1IgiABIgDkQIAogBIgBBrQAKgKALgGQALgHAJgDQALgDAKgBQAUAAAQAHQAQAHAMANQAMANAHATQAGASABAYIAAAaIgBAZIgDAYIgFAUg");
	this.shape_9.setTransform(-1.8,-0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#003399").s().p("AgVgUIg9ACIABghIA9gCIABhNIAigCIgBBPIBFgDIgDAiIhCABIgCCZIgkABg");
	this.shape_10.setTransform(-22.4,0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#003399").s().p("AgnBfQgSgIgNgMQgMgNgGgSQgHgSAAgXQAAgVAIgTQAHgTANgPQANgNASgJQASgIATAAQATAAAQAIQAQAGANANQAMAMAIASQAIARADAUIiYAWQABAMAFAKQAFAKAHAHQAIAHAKADQAKAEALAAQAKAAAJgDQAJgDAIgFQAHgGAGgIQAGgJADgKIAiAGQgFARgJANQgJANgMAJQgMAJgPAGQgOAFgQAAQgWAAgSgHgAgOhEQgJACgIAHQgJAGgGAKQgHAKgCAQIBpgNIgBgEQgHgRgLgKQgMgKgSAAQgGABgJACg");
	this.shape_11.setTransform(-51,4.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#003399").s().p("AgbBeIg+ixIAjgFIA1CXIA3ieIAlAGIhGC4g");
	this.shape_12.setTransform(-71.8,3.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#003399").s().p("AhPAsIgCgsIgBgiIAAgaIgCglIApgBIAAAWIAAAVIAAAVIANgSQAHgJAKgJQAJgJAKgGQALgHAMgBQAPgBALAHIAKAHQAFAFAEAHQAEAIADAKQADAKABAOIgjANIgBgPIgEgLIgEgHIgGgGQgGgEgIAAQgEABgGADQgGAEgFAFIgLAMIgMAOIgKAPIgJALIABAbIAAAYIABAUIABAOIgmAFIgCg3g");
	this.shape_13.setTransform(-90.8,4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#003399").s().p("AgnBfQgSgIgNgMQgMgNgGgSQgHgSAAgXQAAgVAIgTQAHgTANgPQANgNASgJQASgIATAAQATAAAQAIQAQAGANANQAMAMAIASQAIARADAUIiYAWQABAMAFAKQAFAKAHAHQAIAHAKADQAKAEALAAQAKAAAJgDQAJgDAIgFQAHgGAGgIQAGgJADgKIAiAGQgFARgJANQgJANgMAJQgMAJgPAGQgOAFgQAAQgWAAgSgHgAgOhEQgJACgIAHQgJAGgGAKQgHAKgCAQIBpgNIgBgEQgHgRgLgKQgMgKgSAAQgGABgJACg");
	this.shape_14.setTransform(-112.1,4.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#003399").s().p("AgZBkIgPgEIgQgFIgQgHQgIgFgIgFIARgbIAUAKIAVAIQALADALACQAKACAMABQAJAAAHgCQAHgCAFgDQAEgDACgDIAEgHQAAgDAAgDIgBgHIgGgHQgEgEgFgCQgGgDgHgBQgJgDgLAAQgPgBgPgCQgPgDgLgFQgLgFgIgIQgHgKgCgNQgCgMADgLQAEgLAFgIQAHgIAIgHQAJgGAKgEQAKgEALgCQAMgCALgBIAQABIAUADIAUAGQALAEAIAHIgLAgQgMgGgLgDIgTgGIgSgCQgagCgPAHQgQAIAAAOQAAALAGAFQAFAFAKACIAYADQAMAAAQADQARACAMAGQAMAEAHAHQAHAHAEAJQADAIAAAKQgBAQgGAMQgHAMgLAGQgMAIgPADQgOADgQABQgQAAgQgDg");
	this.shape_15.setTransform(-133.1,4.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#003399").s().p("AgQCHQgJgDgMgFQgMgFgMgKIAAAXIggAAIgDkPIAmgCIgBBpQAKgKAMgFQALgGAJgDQALgCAJAAQAMgBAMAEQAMADALAGQAKAHAJAJQAJAIAHALQAGALADAMQAEANgBANQAAAQgEANQgEANgHALQgHALgIAKQgJAIgKAHQgKAGgLADQgLAEgKAAQgKgBgMgDgAgUgWQgKAEgHAGQgIAFgGAHQgFAJgEAIIAAAqQADAMAGAIQAGAIAIAGQAHAGAJADQAJACAJABQAMAAAMgFQALgFAIgIQAIgKAGgLQAFgMAAgNQABgOgEgMQgEgNgIgHQgIgKgMgGQgLgFgNAAQgKABgKADg");
	this.shape_16.setTransform(-154.2,-0.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#003399").s().p("AgjCEQgRgFgPgJQgPgJgMgNQgMgMgJgPQgIgPgFgSQgFgRAAgTQAAgSAFgSQAFgRAIgPQAJgPAMgNQAMgMAPgJQAPgJARgFQARgFASAAQASAAARAFQARAFAPAJQAPAJAMAMQANANAIAPQAJAPAFARQAFASAAASQAAATgFARQgFASgJAPQgIAPgNAMQgMANgPAJQgPAJgRAFQgRAFgSAAQgSAAgRgFgAgmhdQgRAIgOAOQgNAOgIASQgIATAAAUQAAAVAIASQAIATANAOQAOAOARAIQASAIAUAAQANAAANgEQANgEALgGQALgHAJgJQAJgJAGgMQAHgLADgNQAEgNAAgOQAAgNgEgNQgDgNgHgLQgGgMgJgJQgJgJgLgHQgLgHgNgDQgNgEgNAAQgUAAgSAIg");
	this.shape_17.setTransform(-181.3,0.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#003366").ss(3,0,0,6.9).p("EgiWgE+MBEtAAAQCZAAAACIIAAFtQAACIiZAAMhEtAAAQiZAAAAiIIAAltQAAiICZAAg");

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("EgiWAE+QiZAAAAiHIAAlsQAAiICZAAMBEtAAAQCZAAAACIIAAFsQAACHiZAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-236.7,-33.4,473.5,66.7);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeBLQgPgGgKgKQgKgKgEgOQgGgPAAgRQAAgRAHgPQAGgPAJgLQALgLAOgHQAOgGAPAAQAPAAANAGQAMAFAKAKQAKAKAHAOQAFANADAQIh4ARQABAKAEAIQAEAHAFAGQAGAFAJADQAHADAJAAQAIAAAGgCQAIgDAGgEQAGgEAFgHQAEgHADgIIAaAFQgEANgHAKQgHALgKAHQgJAHgLAEQgLAEgNAAQgRAAgOgFgAgLg1QgHACgGAFQgHAFgFAIQgFAIgDAMIBUgKIgBgDQgGgOgJgHQgJgIgOAAQgFAAgHACg");
	this.shape.setTransform(202.4,18.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag+AiIgBgiIgBgbIgBgUIgBgdIAggBIAAASIAAAQIABARIAKgOQAFgIAIgHQAHgHAIgFQAIgFAKgBQALgBAJAGQAEACAEAEIAHAJQAEAGACAIIADATIgcAKIgBgMIgCgJIgEgGIgEgDQgFgEgGAAQgEABgEADIgJAGIgJAKIgJALIgIALIgHAJIABAVIAAATIABAQIAAALIgeAEIgBgsg");
	this.shape_1.setTransform(186.8,18.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgfBLQgOgGgJgKQgLgKgFgOQgEgPgBgRQABgRAFgPQAHgPAKgLQAKgLAOgHQAOgGAPAAQAPAAANAGQAMAFAKAKQAKAKAGAOQAHANABAQIh3ARQABAKADAIQAFAHAFAGQAGAFAJADQAIADAIAAQAHAAAIgCQAHgDAGgEQAGgEAEgHQAFgHACgIIAbAFQgEANgHAKQgHALgJAHQgKAHgLAEQgMAEgMAAQgSAAgOgFgAgLg1QgHACgHAFQgGAFgFAIQgFAIgCAMIBSgKIgBgDQgEgOgKgHQgJgIgOAAQgFAAgHACg");
	this.shape_2.setTransform(170,18.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAlBqQAEgOACgMIADgXIAAgWQgBgPgEgKQgFgKgGgGQgHgHgHgDQgIgDgIAAQgHABgIADQgHAEgIAHQgIAGgGAMIgBBdIgbABIgDjWIAggBIgBBUQAIgIAJgEQAIgFAIgDQAIgDAIgBQAPAAANAGQANAGAJAKQAJALAGAOQAFAOABATIAAAUIgBAVIgCASIgEAQg");
	this.shape_3.setTransform(152.2,15.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhAAiIgBgiIgBgbIgBgUIgBgdIAggBIABAoQAEgIAGgHQAGgHAHgFQAHgGAGgDQAIgDAIgBQALgBAIADQAIADAFAEQAGAEAEAHQADAGACAGIADANIABAMQABAXAAAYIgBAyIgdgBIACgtQABgWgBgWIAAgGIgCgJIgDgJIgFgHQgEgEgEgBQgFgCgHABQgLACgKAOQgLAOgOAbIABAkIABAUIAAALIgeAEIgBgsg");
	this.shape_4.setTransform(126.4,18.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAEgbIggBgIgqAEIgbiQIAcgCIAWB0IAph0IAaACIAcB1IATh2IAfABIgcCQIgpABg");
	this.shape_5.setTransform(107,18.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgcBMQgOgGgKgKQgKgLgGgPQgGgPABgTQgBgRAGgPQAGgPAKgLQAKgLAOgGQANgGAPAAQAPAAAOAHQAOAGAKALQAKALAGAPQAFAPAAAQQAAASgFAPQgGAPgKALQgKALgOAGQgOAGgPAAQgPAAgNgGgAgTgzQgJAFgGAIQgFAIgDALQgCAKAAAJQAAAKADAKQACAKAGAJQAGAIAIAFQAJAFAKAAQAMAAAIgFQAIgFAGgIQAGgJADgKQACgKAAgKQAAgJgCgKQgDgLgFgIQgGgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_6.setTransform(87.7,18.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAlBqQAEgOACgMIADgXIAAgWQgBgPgEgKQgFgKgGgGQgHgHgHgDQgIgDgIAAQgHABgIADQgHAEgIAHQgIAGgGAMIgBBdIgbABIgDjWIAggBIgBBUQAIgIAJgEQAIgFAIgDQAIgDAIgBQAPAAANAGQANAGAJAKQAJALAGAOQAFAOABATIAAAUIgBAVIgCASIgEAQg");
	this.shape_7.setTransform(70,15.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgTBPIgNgDIgMgEIgNgGIgMgIIAOgVIAPAIIAQAGIARAEIARACIANgBQAGgCADgCQAEgCACgDIACgFIABgFIgCgGIgDgFIgIgFQgEgCgGgBIgQgCQgLgBgMgCQgLgCgKgEQgJgEgGgHQgGgGgBgLQgBgKADgIQACgIAFgHQAEgGAHgGQAGgEAJgDQAIgEAJgCQAJgBAIAAIANABIAQABQAIACAHAEQAJACAHAGIgKAZQgJgFgIgCIgPgFIgOgCQgVgBgMAFQgMAGAAAMQAAAIAEAFQAFAEAIABIATACIAVADQAOACAJAEQAJADAGAGQAGAFACAHQADAGAAAIQAAANgFAJQgGAJgJAGQgJAGgMADQgLACgNABQgLgBgNgCg");
	this.shape_8.setTransform(52.5,19.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgeBLQgPgGgKgKQgJgKgGgOQgEgPAAgRQAAgRAFgPQAGgPAKgLQALgLAOgHQAOgGAPAAQAOAAANAGQANAFAKAKQAKAKAHAOQAFANADAQIh4ARQABAKAEAIQADAHAHAGQAGAFAHADQAJADAIAAQAHAAAIgCQAHgDAGgEQAGgEAFgHQAEgHACgIIAbAFQgEANgHAKQgHALgKAHQgJAHgLAEQgLAEgNAAQgSAAgNgFgAgLg1QgHACgHAFQgGAFgFAIQgGAIgCAMIBUgKIgCgDQgEgOgKgHQgJgIgOAAQgFAAgHACg");
	this.shape_9.setTransform(29,18.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag+AiIgBgiIgBgbIgBgUIgBgdIAggBIAAASIAAAQIABARIAKgOQAFgIAIgHQAHgHAIgFQAIgFAKgBQALgBAJAGQAEACAEAEIAHAJQAEAGACAIIADATIgcAKIgBgMIgCgJIgEgGIgEgDQgFgEgGAAQgEABgEADIgJAGIgJAKIgJALIgIALIgHAJIABAVIAAATIABAQIAAALIgeAEIgBgsg");
	this.shape_10.setTransform(13.4,18.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgOBNQgNAAgJgEQgJgFgGgHQgGgHgEgJQgDgKgCgKQgCgLgBgLIgBgUIACgdIADgfIAdABIgDAfIgBAZIAAAMIABAOIADAQQACAIAEAGQADAGAGAEQAFADAIgBQAKgCALgNQAMgNANgbIgBgjIgBgUIAAgLIAegEIACArIABAiIABAbIAAAUIABAeIggAAIAAgoIgLAQQgGAHgHAGQgGAFgIADQgGAEgHAAIgCgBg");
	this.shape_11.setTransform(-4.2,18.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgQgQIgwACIABgaIAvgBIABg9IAbgCIgBA/IA2gDIgCAaIg0ACIgBB5IgcAAg");
	this.shape_12.setTransform(-19.7,16);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgZBLQgPgGgLgLQgMgLgGgPQgHgPAAgRQAAgKACgKQADgKAGgJQAFgJAHgIQAIgHAJgFQAJgGAKgDQALgDAKAAQANAAALADQAKADAKAFQAJAGAHAHQAIAIAFAKIAAABIgXAOIAAAAQgEgIgFgGQgFgFgHgEQgGgEgHgCQgIgCgIAAQgKAAgKAEQgKAFgHAHQgIAHgEAKQgEALAAAKQAAALAEALQAEAKAIAHQAHAHAKAFQAKAEAKAAQAIAAAHgCQAHgCAGgDQAGgEAFgFQAFgFAEgGIABgBIAYANIAAABQgGAJgIAIQgHAHgKAFQgJAFgLADQgLADgLAAQgOAAgPgGg");
	this.shape_13.setTransform(-35.1,18.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgRBpIABgkIAAgsIABg7IAdgBIgCAkIAAAeIgBAZIAAASIgBAfgAgHhCQgEgBgCgDIgEgGQgCgEAAgEQAAgEACgEIAEgGQACgDAEgBQAEgCADAAQAEAAAEACQADABADADIAEAGIACAIQAAAEgCAEIgEAGQgDADgDABQgEACgEAAQgDAAgEgCg");
	this.shape_14.setTransform(-47.2,15.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AhMhwIAfgBIAAAUQAHgIAJgEQAJgEAHgCIAQgEQAJAAAJADQAKADAIAFQAIAFAHAHQAIAHAFAIQAFAJACAKQADAJAAALQgBAMgDALQgDAKgFAIQgGAJgHAHQgHAHgHAFQgIAFgJADQgIACgIAAIgRgCIgRgGQgJgEgJgIIgBBeIgZABgAAAhWQgIAAgIADQgHADgGAFQgGAFgEAGQgFAHgCAHIAAAaQACAJAEAHQAFAHAGAEQAGAFAHACQAHADAHAAQAJAAAJgEQAIgEAHgFQAGgHAEgJQAEgJAAgKQABgKgDgKQgDgJgHgHQgGgHgIgEQgIgEgJAAIgCAAg");
	this.shape_15.setTransform(-59.7,22.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgeBLQgPgGgKgKQgKgKgEgOQgGgPAAgRQAAgRAHgPQAGgPAJgLQALgLAOgHQAOgGAPAAQAPAAANAGQAMAFAKAKQAKAKAHAOQAFANADAQIh4ARQABAKAEAIQAEAHAFAGQAGAFAJADQAHADAJAAQAIAAAGgCQAIgDAGgEQAGgEAFgHQAEgHADgIIAaAFQgEANgHAKQgHALgKAHQgJAHgLAEQgLAEgNAAQgRAAgOgFgAgLg1QgHACgGAFQgHAFgFAIQgFAIgDAMIBUgKIgBgDQgGgOgJgHQgJgIgOAAQgFAAgHACg");
	this.shape_16.setTransform(-85.1,18.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAlBqQAEgOACgMIADgXIAAgWQgBgPgEgKQgFgKgGgGQgHgHgHgDQgIgDgIAAQgHABgIADQgHAEgIAHQgIAGgGAMIgBBdIgbABIgDjWIAggBIgBBUQAIgIAJgEQAIgFAIgDQAIgDAIgBQAPAAANAGQANAGAJAKQAJALAGAOQAFAOABATIAAAUIgBAVIgCASIgEAQg");
	this.shape_17.setTransform(-102.9,15.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgQgQIgwACIABgaIAvgBIABg9IAbgCIgBA/IA2gDIgCAaIg0ACIgBB5IgcAAg");
	this.shape_18.setTransform(-119.2,16);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgQgQIgwACIABgaIAvgBIABg9IAbgCIgBA/IA2gDIgCAaIg0ACIgBB5IgcAAg");
	this.shape_19.setTransform(-140.1,16);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgZBLQgPgGgLgLQgMgLgGgPQgHgPAAgRQAAgKACgKQADgKAGgJQAFgJAHgIQAIgHAJgFQAJgGAKgDQALgDAKAAQANAAALADQAKADAKAFQAJAGAHAHQAIAIAFAKIAAABIgXAOIAAAAQgEgIgFgGQgFgFgHgEQgGgEgHgCQgIgCgIAAQgKAAgKAEQgKAFgHAHQgIAHgEAKQgEALAAAKQAAALAEALQAEAKAIAHQAHAHAKAFQAKAEAKAAQAIAAAHgCQAHgCAGgDQAGgEAFgFQAFgFAEgGIABgBIAYANIAAABQgGAJgIAIQgHAHgKAFQgJAFgLADQgLADgLAAQgOAAgPgGg");
	this.shape_20.setTransform(-155.5,18.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgfBLQgOgGgJgKQgLgKgEgOQgFgPgBgRQABgRAFgPQAGgPALgLQAKgLAOgHQAOgGAPAAQAOAAAOAGQAMAFAKAKQAKAKAGAOQAHANABAQIh3ARQABAKADAIQAFAHAFAGQAGAFAJADQAHADAJAAQAIAAAGgCQAIgDAGgEQAGgEAEgHQAFgHADgIIAaAFQgEANgHAKQgHALgJAHQgKAHgLAEQgMAEgMAAQgSAAgOgFgAgLg1QgHACgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgDQgEgOgKgHQgJgIgOAAQgFAAgHACg");
	this.shape_21.setTransform(-172.6,18.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAJB5QgIgDgFgFQgGgGgEgGIgHgNQgGgQgCgUIAEivIAcAAIgBAtIgBAlIgBAfIAAAWIgBAmQABANACAKIAEAJIAFAHQAEADAEACQAFACAGABIgEAcQgKgBgHgDg");
	this.shape_22.setTransform(-184.3,13.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgfBLQgOgGgKgKQgJgKgFgOQgGgPAAgRQAAgRAHgPQAFgPALgLQAKgLAOgHQAOgGAPAAQAPAAANAGQAMAFAKAKQAKAKAGAOQAGANADAQIh4ARQABAKADAIQAFAHAFAGQAHAFAIADQAHADAJAAQAIAAAGgCQAIgDAGgEQAGgEAFgHQAEgHADgIIAaAFQgEANgHAKQgHALgJAHQgKAHgLAEQgMAEgMAAQgRAAgPgFgAgLg1QgHACgGAFQgHAFgFAIQgGAIgBAMIBTgKIgBgDQgGgOgJgHQgJgIgOAAQgFAAgHACg");
	this.shape_23.setTransform(-197.5,18.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgSBwQgKgCgKgEQgKgEgKgGIgUgNIAVgYQALAKAKAGQALAFAIADQAKADAIABQAKABAJgCQAJgCAGgEQAHgEADgGQAEgFABgGQABgGgDgEQgCgEgEgEIgKgGIgNgFIgNgDIgNgDIgRgEIgRgGQgJgEgHgFQgIgFgFgIQgGgIgCgKQgDgKABgOQABgLADgJQAEgJAGgHQAGgHAIgFQAIgFAJgEQAJgDAJgBQAKgCAIAAQANABAOADIAMAEIAMAFQAGADAGAEIAMAJIgQAZIgJgJIgKgGIgKgFIgKgDQgKgDgLgBQgLAAgKAEQgKAEgHAGQgHAGgDAIQgEAHAAAHQAAAHAFAHQAEAHAHAFQAIAGAKAEQAKADALACIAUADQAKACAJAEQAJAEAIAFQAIAGAFAHQAGAHACAJQACAJgBALQgCAKgEAHQgEAIgGAFQgGAGgIAEQgHAEgIACIgRADIgRABQgJAAgLgCg");
	this.shape_24.setTransform(-215.3,15.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#993333").ss(3,1,1).p("EgicgDsMBE5AAAQByAAAACBIAADXQAACBhyAAMhE5AAAQhyAAAAiBIAAjXQAAiBByAAg");
	this.shape_25.setTransform(-3,15.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF3333").s().p("EgicADsQhyAAABiAIAAjWQgBiCByAAMBE5AAAQBxAAABCCIAADWQgBCAhxAAg");
	this.shape_26.setTransform(-3,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(-236.3,-9.6,466.7,50.3), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgWBWIgMgDIgOgFIgOgGQgHgEgGgFIAPgXIARAIIASAIIASAFQAJABAKAAQAIAAAGgBQAGgCAEgCIAFgFIADgGIABgFIgBgHIgFgFQgDgDgFgDQgFgCgHgCIgRgCQgMAAgNgCQgNgDgJgEQgLgFgFgHQgIgHAAgMQgCgLADgJQADgJAFgHQAEgHAIgGQAHgFAJgEQAJgDAKgCQAKgCAIAAIAPABIARACQAIACAJAEQAJADAIAGIgLAbIgTgIIgPgEIgQgDQgXgBgNAGQgOAGAAANQAAAJAGAFQAFAEAIACIAVACIAXADQAPACAKAFQAKADAHAGQAGAGADAIQACAHAAAIQAAAOgFAKQgHAKgJAGQgKAGgNADQgNADgNABQgNAAgPgDg");
	this.shape.setTransform(158.2,19.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghBRQgQgGgKgLQgLgLgGgPQgFgQAAgTQAAgTAGgQQAHgRALgLQALgMAPgIQAQgGAQAAQAQAAAOAFQAOAGALALQAKALAHAPQAHAPACARIiCATQABALAEAIQAEAIAHAHQAHAFAIAEQAJADAJAAQAIgBAIgCQAIgCAHgFQAGgFAFgHQAFgIADgIIAdAFQgEAOgIALQgIALgKAIQgLAJgMAEQgMAEgOAAQgTABgPgHgAgMg6QgHACgIAFQgHAGgFAIQgGAJgCAOIBagMIgBgDQgGgOgKgJQgKgJgPABQgFAAgIACg");
	this.shape_1.setTransform(140.5,18.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhEAlIgBglIgBgdIgBgWIgBggIAjAAIAAASIAAASIAAASIALgPQAGgIAIgIQAIgIAJgFQAJgGALgBQAMgBAKAHIAJAGQAEAEADAGQAEAHACAIQADAJABAMIgeALIgBgNIgDgKIgEgGIgFgEQgFgEgHABQgEAAgFADIgJAHIgKALIgKAMIgJAMIgHAKIAAAXIABAUIABASIAAALIggAFIgCgwg");
	this.shape_2.setTransform(123.5,18.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAqApQgFAJgHAIQgGAIgHAGQgIAGgIADQgIAEgJAAQgNgBgLgFQgJgEgHgIQgGgIgFgKQgEgKgBgMQgDgLgBgMIAAgXIABgfIAEghIAfAAIgDAjIgBAaIABANIAAAQIADARQADAJADAHQAEAGAGAEQAHAEAIgCQALgCANgOQAMgOAOgdIgBgnIgBgVIAAgNIAhgDIABAuIABAlIABAeIABAWIABAgIgiABg");
	this.shape_3.setTransform(104.4,18.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgSgRIg0ACIAAgdIA1gBIABhCIAdgCIgBBDIA7gCIgCAcIg5ACIgBCEIgfAAg");
	this.shape_4.setTransform(87.4,15.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbBRQgQgGgNgMQgMgMgIgQQgIgQAAgTQABgKADgLQADgLAFgKQAGgKAIgIQAJgJAJgFQALgHALgDQAMgDALAAQANAAAMADQAMADAKAGQALAGAHAJQAJAIAFALIABABIgaAPIAAgBQgEgIgGgGQgGgGgGgEQgIgEgHgDQgJgCgIAAQgLAAgLAEQgLAFgIAJQgIAHgFAMQgFALAAALQAAAMAFALQAFALAIAIQAIAIALAGQALAEALAAQAIAAAIgCQAHgCAHgEQAHgEAGgGQAFgFAEgHIABgBIAaAPIAAABQgGAKgJAIQgIAIgLAGQgKAFgLAEQgMACgMAAQgQABgQgIg");
	this.shape_5.setTransform(70.7,18.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgSByIABgnIAAgvIABhBIAegBIAAAnIgBAhIgBAbIAAAUIgBAhgAgHhHIgHgFQgEgDgBgEQgCgEAAgEQAAgFACgEQABgEAEgDQADgDAEgBQADgCAEAAQAEAAAFACQADABAEADIAEAHQACAEgBAFQABAEgCAEIgEAHIgHAFQgFABgEAAQgEAAgDgBg");
	this.shape_6.setTransform(57.5,15);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhTh6IAhgCIAAAXQAJgJAJgEQAKgFAIgDIARgDQAKAAAKADQAKADAJAFQAKAGAHAHQAIAIAFAJQAGAJADALQADALgBAMQAAANgEAMQgDALgGAJQgFAJgIAIQgIAIgIAFQgJAFgJADQgJADgJAAQgJAAgJgDQgIgCgKgEQgKgFgKgIIgBBnIgcAAgAgRhbQgJADgGAGQgHAFgEAHQgFAHgCAIIgBAdQADAKAFAIQAEAHAHAEQAHAFAHADQAIADAIAAQAKAAAJgEQAJgEAHgGQAHgIAFgJQAEgKAAgLQABgLgDgKQgEgLgHgIQgGgHgKgFQgJgEgLAAQgJAAgIADg");
	this.shape_7.setTransform(43.8,22.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AghBRQgQgGgKgLQgLgLgGgPQgFgQAAgTQAAgTAGgQQAHgRALgLQALgMAPgIQAQgGAQAAQAQAAAOAFQAOAGALALQAKALAHAPQAHAPACARIiCATQABALAEAIQAEAIAHAHQAHAFAIAEQAJADAJAAQAIgBAIgCQAIgCAHgFQAGgFAFgHQAFgIADgIIAdAFQgEAOgIALQgIALgKAIQgLAJgMAEQgMAEgOAAQgTABgPgHgAgMg6QgHACgIAFQgHAGgFAIQgGAJgCAOIBagMIgBgDQgGgOgKgJQgKgJgPABQgFAAgIACg");
	this.shape_8.setTransform(16,18.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AApBzQAEgOACgOIADgZIAAgYQAAgQgFgMQgFgKgHgHQgHgIgJgDQgIgDgIAAQgIABgJAEQgIADgIAJQgJAGgHAOIgBBkIgeABIgDjpIAjgBIgBBcQAJgJAJgFQAKgFAIgDQAJgDAJgBQARAAAOAGQANAGALALQAKALAFARQAGAPABAVIAAAWIgBAWIgCAUIgEASg");
	this.shape_9.setTransform(-3.4,15.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgSgRIg0ACIAAgdIA1gBIABhCIAdgCIgBBDIA7gCIgCAcIg5ACIgBCEIgfAAg");
	this.shape_10.setTransform(-21.1,15.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AghBRQgQgGgKgLQgLgLgGgPQgFgQAAgTQAAgTAGgQQAHgRALgLQALgMAPgIQAQgGAQAAQAQAAAOAFQAOAGALALQAKALAHAPQAHAPACARIiCATQABALAEAIQAEAIAHAHQAHAFAIAEQAJADAJAAQAIgBAIgCQAIgCAHgFQAGgFAFgHQAFgIADgIIAdAFQgEAOgIALQgIALgKAIQgLAJgMAEQgMAEgOAAQgTABgPgHgAgMg6QgHACgIAFQgHAGgFAIQgGAJgCAOIBagMIgBgDQgGgOgKgJQgKgJgPABQgFAAgIACg");
	this.shape_11.setTransform(-45.8,18.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgYBSIg1iZIAfgFIAtCBIAwiGIAfAEIg8Cfg");
	this.shape_12.setTransform(-63.6,18.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhEAlIgBglIgBgdIgBgWIgBggIAjAAIAAASIAAASIAAASIALgPQAGgIAIgIQAIgIAJgFQAJgGALgBQAMgBAKAHIAJAGQAEAEADAGQAEAHACAIQADAJABAMIgeALIgBgNIgDgKIgEgGIgFgEQgFgEgHABQgEAAgFADIgJAHIgKALIgKAMIgJAMIgHAKIAAAXIABAUIABASIAAALIggAFIgCgwg");
	this.shape_13.setTransform(-79.9,18.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AghBRQgQgGgKgLQgLgLgGgPQgFgQAAgTQAAgTAGgQQAHgRALgLQALgMAPgIQAQgGAQAAQAQAAAOAFQAOAGALALQAKALAHAPQAHAPACARIiCATQABALAEAIQAEAIAHAHQAHAFAIAEQAJADAJAAQAIgBAIgCQAIgCAHgFQAGgFAFgHQAFgIADgIIAdAFQgEAOgIALQgIALgKAIQgLAJgMAEQgMAEgOAAQgTABgPgHgAgMg6QgHACgIAFQgHAGgFAIQgGAJgCAOIBagMIgBgDQgGgOgKgJQgKgJgPABQgFAAgIACg");
	this.shape_14.setTransform(-98.4,18.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgWBWIgMgDIgOgFIgOgGQgHgEgGgFIAPgXIARAIIASAIIASAFQAJABAKAAQAIAAAGgBIAKgEIAFgFIADgGIABgFIgCgHIgEgFQgDgDgFgDQgFgCgHgCIgQgCQgNAAgNgCQgNgDgKgEQgJgFgGgHQgIgHAAgMQgBgLACgJQADgJAFgHQAEgHAIgGQAHgFAJgEQAJgDAKgCQAJgCAJAAIAPABIARACQAIACAJAEQAJADAIAGIgLAbIgTgIIgQgEIgPgDQgXgBgNAGQgOAGAAANQAAAJAGAFQAFAEAIACIAUACIAYADQAPACAKAFQAKADAHAGQAGAGADAIQACAHAAAIQAAAOgGAKQgFAKgKAGQgKAGgNADQgNADgNABQgNAAgPgDg");
	this.shape_15.setTransform(-116.4,19.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgNB0QgIgCgLgFQgKgEgKgKIAAAUIgcABIgDjpIAigBIgBBZQAJgIAJgEQAKgGAIgBQAJgDAIAAQAKAAALADQAKADAJAFQAJAGAIAHQAHAHAGAKQAFAJADAKQADALAAAMQgBANgDAMQgEALgFAJQgGAKgIAIQgHAHgJAGQgIAFgKADQgJADgJAAQgIgBgKgCgAgRgTQgIADgHAFQgGAFgFAGQgFAHgDAHIAAAlQADAJAFAIQAFAHAGAFQAHAEAIADQAHADAIAAQAKAAAKgEQAJgEAIgIQAHgIAEgJQAFgLAAgMQABgLgEgKQgDgLgHgHQgHgIgKgFQgKgFgLABQgJAAgIADg");
	this.shape_16.setTransform(-134.6,15.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeBxQgOgEgNgIQgNgHgKgLQgLgLgHgNQgIgNgEgPQgEgPAAgQQAAgPAEgPQAEgPAIgNQAHgOALgKQAKgLANgIQANgHAOgEQAPgFAPAAQAQAAAPAFQAOAEANAHQANAIAKALQALAKAHAOQAIANAEAPQAEAPAAAPQAAAQgEAPQgEAPgIANQgHANgLALQgKALgNAHQgNAIgOAEQgPAFgQAAQgPAAgPgFgAgghPQgPAGgMAMQgLAMgHAQQgHAQAAARQAAASAHAQQAHAQALAMQAMAMAPAHQAPAGARAAQAMAAALgDQAKgDAKgFQAJgGAIgIQAIgIAFgKQAGgKADgLQADgLAAgMQAAgLgDgLQgDgLgGgKQgFgKgIgIQgIgIgJgFQgKgGgKgDQgLgDgMAAQgRAAgPAHg");
	this.shape_17.setTransform(-157.8,15.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000066").ss(3,1,1).p("EgicgDsMBE5AAAQByAAAACBIAADXQAACBhyAAMhE5AAAQhyAAAAiBIAAjXQAAiBByAAg");
	this.shape_18.setTransform(-3,15.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#0066FF").s().p("EgicADsQhyAAABiAIAAjXQgBiAByAAMBE5AAAQBxAAABCAIAADXQgBCAhxAAg");
	this.shape_19.setTransform(-3,15.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-236.3,-10.1,466.7,51.1), null);


(lib.ro2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(77,140,15,0.996)").s().p("AghLxQAIiVAGiZQAAgRgIgDQgCgCAAgEIAEAAQAIg/gDg/QgLAGgSgDQhmgMhGgtQh7hNhvhaIAAAFIAAABQgCgMgHgDIAJACIADAAQCFBEB7BNQBLAsBlASQABAAAAAAQABAAAAgBQABAAAAAAQAAgBAAgBQACgSgDgMIAAgBQhqhehJh/QhKh/gviZIgriSIB+EUQA+CJBUB2QAgAuAnArQAJjAABjFQACjIAFjCIAEi6QAQC6AGDFQAGC/ADDGIACDKQBdhtA8iNQA7iNBDiDQAWgqAegiQg1CUg/CLQg9CJhRB5QghA1goAyQBTgKBCgnQB+hJCChGQAKgGAOgDQhtBbh/BKQhKAshlASIgSABQABA9ABA8IABAAQgEAXAAAaQgCCQgECMg");
	this.shape.setTransform(113.5,126.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(104,170,39,0.996)").s().p("AgtJfQg2gCgogQQhvgphPhHIgcgYQhQhGgih0IAAgCIAAgFQBvBaB7BOQBHAsBlAMQASADALgFQAEA+gJA/gAAWJaQgBg7AAg9IASgCQBkgSBKgrQB/hLBthbQgNADgLAGQiCBGh+BJQhCAnhSALQAngzAig1QBRh5A9iKQA+iKA1iUQgeAjgVApQhDCCg8CNQg8COhcBtIgDjJQgDjIgGi+QgGjEgOi7IgGC6QgFDCgBDHQgCDGgIDBQgngsghguQhUh2g+iKIh+kSIArCSQAvCXBKCAQBKB/BqBeIAAACQACAMgBARQAAABgBABQAAAAAAABQgBAAAAAAQgBAAgBAAQhlgShKgsQh8hNiFhEQAhgNAqgHIAMgCQgUi4ACjBQAAgzASgdQAjgIAdAUQAZAUAgANQAMAAAKgBQAtg6AWhPQAricBdhtQAKgMAOgKQBbA7AnBxQApB2AmB6QA/gDA3gWQAVgLAUAIQAMAngFA3QgKCcAGChIABAOQAfARA1gDQASAAAJAJQAAArgUAiQhJB/h5BSQhiBAiIAag");
	this.shape_1.setTransform(115,108.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000033").ss(5,1,1).p("ATGAAQAAH6lmFmQllFmn7AAQn5AAlmlmQlmlmAAn6QAAn5FmlmQFmlmH5AAQH7AAFlFmQFmFmAAH5g");
	this.shape_2.setTransform(115.9,116.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AtgNgQlllmAAn6QAAn5FllmQFmlmH6AAQH6AAFmFmQFmFmAAH5QAAH6lmFmQlmFmn6AAQn6AAlmlmg");
	this.shape_3.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AuLOLQl3l4AAoTQAAoTF3l4QF4l3ITAAQIUAAF3F3QF4F4AAITQAAITl4F4Ql3F4oUAAQoTAAl4l4g");
	this.shape_4.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ro2copy, new cjs.Rectangle(-12.4,-11.5,256.6,256.6), null);


(lib.ro1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFF00","#FFCC00"],[0,1],0,0,0,0,0,13.6).s().p("AhaBbQglglAAg2QAAg0AlgmQAmgmA0AAQA1AAAmAmQAmAmgBA0QABA2gmAlQglAmg2AAQg0AAgmgmgAhcAeQAKAfAZASQAaATAfAAQAgAAAagTQAagSAKgfIABgDIgMAAIgBACQgJAagWAQQgXAQgcAAQgbAAgWgQQgXgQgJgaIAAgCIgMAAgAAqgsQgFAJAAANQAAANAFAJQAGAIAGAAQAIAAAGgIQAEgJAAgNQAAgNgEgJQgGgJgIAAQgGAAgGAJgAhCgsQgFAJAAANQAAANAFAJQAGAIAGAAQAIAAAGgIQAEgJAAgNQAAgNgEgJQgGgJgIAAQgGAAgGAJg");
	this.shape.setTransform(115.6,113.5,5.847,5.847);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhLBMQgggfABgtQgBgsAggfQAgggArAAQAsAAAgAgQAgAfAAAsQAAAtggAfQggAggsgBQgrABggggg");
	this.shape_1.setTransform(120.9,120.3,5.847,5.847);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000033").ss(5,1,1).p("ATGAAQAAH6lmFmQllFmn7AAQn5AAlmlmQlmlmAAn6QAAn5FmlmQFmlmH5AAQH7AAFlFmQFmFmAAH5g");
	this.shape_2.setTransform(115.9,116.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AtgNgQlllmAAn6QAAn5FllmQFmlmH6AAQH6AAFmFmQFmFmAAH5QAAH6lmFmQlmFmn6AAQn6AAlmlmg");
	this.shape_3.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AuLOLQl3l4AAoTQAAoTF3l4QF4l3ITAAQIUAAF3F3QF4F4AAITQAAITl4F4Ql3F4oUAAQoTAAl4l4g");
	this.shape_4.setTransform(115.9,116.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ro1copy, new cjs.Rectangle(-12.4,-11.5,256.6,256.6), null);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ro2copy3();
	this.instance.parent = this;
	this.instance.setTransform(165.4,4.8,1.02,1.02,0,0,0,115.8,115.8);

	this.instance_1 = new lib.ro1copy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-167.1,3.2,1.02,1.02,0,0,0,115.8,115.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-297.9,-126.6,594.2,263.3);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ro2copy2();
	this.instance.parent = this;
	this.instance.setTransform(-233.3,-2.9,0.972,0.972,0,0,0,115.7,115.8);

	this.instance_1 = new lib.ro1copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(233,1.1,0.972,0.972,0,0,0,115.7,115.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-357.8,-126.6,715.6,253.4);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ro2copy();
	this.instance.parent = this;
	this.instance.setTransform(166.5,5.7,1.04,1.04,0,0,0,115.9,115.8);

	this.instance_1 = new lib.ro1copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-165.5,4.7,1.02,1.02,0,0,0,115.8,115.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-296.3,-126.7,596.2,266.8);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ro2copy();
	this.instance.parent = this;
	this.instance.setTransform(164.7,-1,1.04,1.04,0,0,0,115.9,115.8);

	this.instance_1 = new lib.ro1copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-167.3,-2,1.02,1.02,0,0,0,115.8,115.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-298.1,-133.4,596.2,266.8);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween29("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},9).to({scaleX:1.03,scaleY:1.03},11).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-217.5,-33,435.1,66);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ro2copy3();
	this.instance.parent = this;
	this.instance.setTransform(0,-0.9,1.02,1.02,0,0,0,115.8,115.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.07,scaleY:1.07},9).to({scaleX:1.02,scaleY:1.02},10).to({scaleX:1.07,scaleY:1.07},10).to({scaleX:1.02,scaleY:1.02},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-130.8,-130.8,261.7,261.7);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween25("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0,1.05,1.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.16,scaleY:1.16},9).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1.16,scaleY:1.16},10).to({scaleX:1.05,scaleY:1.05},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51,-82.6,102.1,165.2);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween38("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,0,1.03,1.03);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.06,scaleY:1.06},9).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1.06,scaleY:1.06},10).to({scaleX:1.03,scaleY:1.03},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-243.8,-34.4,487.7,68.7);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween17copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(1.8,6.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-296.3,-126.7,596.2,266.8);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(802.9,297.5,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(412).to({_off:false},0).wait(31).to({startPosition:11},0).to({alpha:0,startPosition:0},8).wait(1));

	// Layer_20
	this.instance_1 = new lib.ro2copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(805.9,293.4,1.02,1.02,0,0,0,115.8,115.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(391).to({_off:false},0).to({scaleX:1.12,scaleY:1.12,y:293.3},21).to({regY:115.9,y:293.5},31).to({regY:115.8,y:293.3,alpha:0},8).wait(1));

	// Layer_19
	this.instance_2 = new lib.ro1copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(472.5,292.1,1.02,1.02,0,0,0,115.8,115.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(379).to({_off:false},0).wait(64).to({alpha:0},8).wait(1));

	// Layer_18
	this.instance_3 = new lib.ro2copy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(406.7,567.9,0.972,0.972,0,0,0,115.7,115.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(357).to({_off:false},0).to({regX:115.8,scaleX:1.02,scaleY:1.02,x:805.9,y:293.4},22).to({_off:true},12).wait(61));

	// Layer_17
	this.instance_4 = new lib.Symbol1copy_1("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(519.7,624.8,1,1,-0.7,0,0,190.5,64.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(322).to({_off:false},0).to({_off:true},35).wait(95));

	// Layer_15
	this.instance_5 = new lib.Symbol1copy6("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(239.1,425.2,1,1,-36,0,0,41,60.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(287).to({_off:false},0).to({_off:true},35).wait(130));

	// Layer_13
	this.instance_6 = new lib.Symbol1("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(806.1,302);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(238).to({_off:false},0).to({_off:true},49).wait(165));

	// Layer_16
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF3333").s().p("Ai3K4QhIhIAAhvQAAhsBLhKQBJhLBrAAQBoAABNBNQBKBMAABpQAABqhKBKQhMBJhpAAQhwAAhHhHgAjPDHIAAhEQAAiFAehAQAehABQgkIBIggQBTglAAhDQAAgmgbgbQgbgagmAAQhqAAAACSIlyAAQAAjEBah7QBChcBrg2QBrg2BxAAQDJAACLCHQCKCEAADHQAAD0jGBiQhAAggPAYQgNAYAABNg");
	this.shape.setTransform(803.6,294.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("Ai2K4QhIhJAAhuQAAhsBKhKQBKhLBqAAQBoAABNBMQBLBMAABpQAABqhLBKQhMBKhpAAQhvAAhHhHgAjPDHIAAhEQAAiFAehAQAehBBQgjIBIggQBTglAAhDQAAgmgbgbQgbgbglAAQhqAAAACTIlzAAQAAjFBbh7QBBhcBrg1QBrg2BxAAQDKAACLCHQCJCEAADHQAADzjGBiQhAAhgOAXQgOAYAABOg");
	this.shape_1.setTransform(809.4,296);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1},{t:this.shape}]},287).to({state:[]},92).wait(73));

	// Layer_14
	this.instance_7 = new lib.ro2copy();
	this.instance_7.parent = this;
	this.instance_7.setTransform(806.5,292.1,1.04,1.04,0,0,0,115.9,115.8);

	this.instance_8 = new lib.ro1copy();
	this.instance_8.parent = this;
	this.instance_8.setTransform(474.5,291.1,1.02,1.02,0,0,0,115.8,115.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8},{t:this.instance_7}]},238).to({state:[]},49).wait(165));

	// Layer_8
	this.instance_9 = new lib.Symbol2("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(805.4,294.2);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(189).to({_off:false},0).to({_off:true},46).wait(217));

	// Layer_5
	this.instance_10 = new lib.Symbol8("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(758.1,102.6);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(189).to({_off:false},0).to({_off:true},98).wait(165));

	// Layer_5
	this.instance_11 = new lib.Tween18("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,570.8);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(166).to({_off:false},0).to({alpha:1},3).to({startPosition:0},22).to({startPosition:0},43).to({startPosition:0},9).wait(200).to({startPosition:0},0).to({alpha:0},8).wait(1));

	// Layer_7
	this.instance_12 = new lib.Symbol1copy4();
	this.instance_12.parent = this;
	this.instance_12.setTransform(644.5,81.8,1.5,1.5);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(146).to({_off:false},0).to({alpha:1},7).wait(290).to({alpha:0},8).wait(1));

	// Layer_6
	this.instance_13 = new lib.Tween19("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,288.4);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(146).to({_off:false},0).to({alpha:1},7).to({startPosition:0},9).to({startPosition:0},61).to({startPosition:0},3).to({startPosition:0},3).to({_off:true},150).wait(73));

	// Layer_9
	this.instance_14 = new lib.Tween17copy3("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,288.4);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(97).to({_off:false},0).wait(43).to({startPosition:3},0).to({alpha:0,startPosition:8},5).to({_off:true},1).wait(306));

	// Layer_1
	this.instance_15 = new lib.Tween31("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(640,105.6,1.1,1.1);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(97).to({_off:false},0).wait(43).to({startPosition:0},0).to({alpha:0,startPosition:5},5).to({_off:true},1).wait(306));

	// Layer_4
	this.instance_16 = new lib.Tween17("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(640,288.4);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(44).to({_off:false},0).to({alpha:1},7).to({startPosition:0},16).to({startPosition:0},17).to({startPosition:0},38).to({alpha:0},23).to({_off:true},1).wait(306));

	// Layer_3
	this.instance_17 = new lib.Tween16("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(640,288.4,0.9,0.9);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(44).to({_off:false},0).to({startPosition:0},7).to({startPosition:0},55).to({startPosition:0},39).to({startPosition:0},298).to({alpha:0},8).wait(1));

	// Layer_2
	this.instance_18 = new lib.Symbol1copy();
	this.instance_18.parent = this;
	this.instance_18.setTransform(644.5,81.8,1.5,1.5);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(20).to({_off:false},0).to({alpha:1},7).wait(97).to({alpha:0},21).to({_off:true},1).wait(306));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(589.7,318.6,1376,806);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;