
///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 5, answeredQuestions = 0, choiceCnt = 2, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;
var attemptCnt;
var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var currentX
var currentY
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var qno = [];
var choiceArr = []
var currentObj = []
var atype = [1, 0, 0, 1, 1, 0, 0, 1, 1, 0]
var atype = [1, 0, 0, 1, 1]

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function(e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////

function init() {

    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);
    createLoader()
    createCanvasResize()


    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "FaceCut-KG/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(
            { id: "choice1", src: gameAssetsPath + "ChoiceImages1.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
            { id: "answer", src: gameAssetsPath + "answer.png" },
            { id: "choice2", src: gameAssetsPath + "question.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}
//=================================================================DONE LOADING=================================================================//
function doneLoading1(event) {

    loaderBar.visible = false;
    stage.update();
    var event = assets[i];
    var id = event.item.id;

    if (id == "questionText") {
        questionText = new createjs.Bitmap(preload.getResult('questionText'));
        container.parent.addChild(questionText);
        questionText.visible = false;
    }

    if (id == "choice1") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 386, "count": 0, "regY": 50, "width": 383 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        choice1 = new createjs.Sprite(spriteSheet1);
        choice1.visible = false;
        container.parent.addChild(choice1);
    };

    if (id == "choice2") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice2")],
            "frames": { "regX": 50, "height": 321, "count": 0, "regY": 50, "width": 320 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        choice2 = new createjs.Sprite(spriteSheet1);
        choice2.visible = false;
        container.parent.addChild(choice2);
    };

    if (id == "answer") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("answer")],
            "frames": { "regX": 50, "height": 321, "count": 0, "regY": 50, "width": 320 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        answer = new createjs.Sprite(spriteSheet1);
        answer.visible = false;
        container.parent.addChild(answer);
    };


    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("D044BEAA30B3F146B78DA97BB48504C8");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }
}

function tick(e) {
    stage.update();
}
/////////////////////////////////////////////////////////////////=======HANDLE CLICK========///////////////////////////////////////////////////////////////////
function handleClick(e) {
    qno = between(0, 29)
    atype.sort(randomSort)
    toggleFullScreen()
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}
////////////////////////////////////////////////////////////=======CREATION OF GAME ELEMENTS========///////////////////////////////////////////////////////////////////
function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Match the pictures";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 60;

    bg.visible = true;
    answer.visible = true;
    answer.x = 520
    answer.y = 200

    choice2.x = 520
    choice2.y = 200


    container.parent.addChild(questionText);
    questionText.visible = true;

    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i] = choice1.clone()
        container.parent.addChild(choiceArr[i])
        choiceArr[i].name = i
        choiceArr[i].y = 405;
        choiceArr[i].scaleX = choiceArr[i].scaleY = .8;
    }
    container.parent.addChild(correctImageMc);
    container.parent.addChild(wrongImageMc);
    
    pickques();
}
//==============================================================HELP ENABLE/DISABLE===================================================================//
function helpDisable() {
    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].mouseEnabled = true;
    }
}
//==================================================================PICKQUES==========================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    var ansArr = []
    attemptCnt = 0;
    currentObj = []
    correctImageMc.visible = false
    wrongImageMc.visible = false
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    boardMc.boardMc.openMc.mouseEnabled = true;
    //=================================================================================================================================//
    choice2.visible = true;

    choice2.gotoAndStop(qno[cnt])


    if (atype[cnt] == 1) {
        choiceArr[1].x = 150
        choiceArr[2].x = 890
    }
    else {
        choiceArr[2].x = 150
        choiceArr[1].x = 890
    }


    var temp = between(0, 29)
    temp.splice(temp.indexOf(qno[cnt]), 1);
    ansArr.push(qno[cnt], temp[0])

    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].visible = true;
        choiceArr[i].gotoAndStop(ansArr[i - 1])
    }

    ans = 1
    enablechoices();
    rst = 0;
    gameResponseTimerStart();

    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
}
//====================================================================CHOICE ENABLE/DISABLE==============================================================//
function enablechoices() {
    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].visible = true;
        choiceArr[i].alpha = 1
        choiceArr[i].addEventListener("click", answerSelected)
        choiceArr[i].cursor = "pointer";
        choiceArr[i].mouseEnabled = true;
    }
}

function disablechoices() {
    for (i = 1; i <= choiceCnt; i++) {
        choiceArr[i].removeEventListener("click", answerSelected)
        choiceArr[i].cursor = "pointer";
        //   choiceArr[i].visible = false;

    }
    boardMc.boardMc.openMc.mouseEnabled = false;
}
//===================================================================MOUSE ROLL OVER/ROLL OUT==============================================================//
function onRoll_over(e) {
    e.currentTarget.alpha = .5
    stage.update();
}

function onRoll_out(e) {
    e.currentTarget.alpha = 1;
    stage.update();
}
//=================================================================ANSWER SELECTION=======================================================================//
function answerSelected(e) {
   
    e.preventDefault();
    uans = e.currentTarget.name;
    console.log(ans + " =correct= " + uans)
    gameResponseTimerStop();
    //  pauseTimer();

    console.log(ans)
    if (ans == uans) {
        currentX = e.currentTarget.x + 75
        currentY = e.currentTarget.y + 55
        starAnimation()

        e.currentTarget.removeEventListener("click", answerSelected)
        for (i = 1; i <= choiceCnt; i++) {
            choiceArr[i].mouseEnabled = false;
        }

        setTimeout(correct1, 800)

    } else {
        wrongImageMc.visible = true;
        getValidation("wrong");
        disablechoices();
    }

}
function correct1() {
    choice2.visible = false
    answer.gotoAndStop(qno[cnt])
    answer.visible = true
    setTimeout(correct, 1000)
}
function correct() {
    getValidation("correct");
    disablechoices();
}

function disableMouse() {

}

function enableMouse() {

}
