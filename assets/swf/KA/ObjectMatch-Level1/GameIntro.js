(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgIQAOgJAQgBQASgCAPAJQAGADAHAHQAGAFAFAKQAFAJAEAOQAEAMABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAEgHAIQgHAGgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape.setTransform(135.4,5.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgcCpIACg6IABhGIABhgIAtgCIAAA7IgBAxIgBAnIgBAeIAAAxgAgMhqQgGgDgEgEQgEgEgDgGQgCgGAAgHQAAgGACgGQADgGAEgEQAEgFAGgCQAGgDAGAAQAHAAAFADQAGACAEAFQAFAEADAGQACAGAAAGQAAAHgCAGQgDAGgFAEQgEAEgGADQgFACgHAAQgGAAgGgCg");
	this.shape_1.setTransform(116.2,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("ABGCAIADgfQgbARgbAIQgaAIgWAAQgRAAgPgEQgPgEgMgJQgLgJgHgNQgHgNAAgTQAAgSAFgOQAGgPAJgLQAJgKANgIQANgHAPgFQAPgFAQgDQAPgCAQAAQAPAAAMABIAYADIgIgWQgEgLgGgJQgIgIgJgGQgKgFgNAAQgJAAgKACQgMADgNAHQgOAGgQAMQgPAMgTASIgbghQAWgUATgNQAUgNASgHQASgHAPgDQANgDAMAAQAVAAAQAIQAQAHAMAMQANAMAIARQAJAQAFATQAGATACAVQACATAAAUQAAAWgCAZQgDAZgFAdgAgKAAQgTADgOAJQgOAJgGANQgHANAEARQADAOAJAFQAKAFAMAAQANAAAQgEQAOgEAPgHIAbgNIAXgNIABgXIgBgXQgLgCgNgCQgMgCgMAAQgUAAgSAFg");
	this.shape_2.setTransform(95.1,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("Ah7i2IAxgCIAAAhQANgNAOgGQAOgHALgEQAOgEAMgBQAPAAAPAEQAQAFANAIQAOAIALALQAMALAIAOQAIAOAEAQQAEAQAAARQgBAUgFARQgFARgJANQgIAPgLALQgMALgMAIQgNAIgOAFQgNAEgOAAQgMgBgPgDQgMgEgPgGQgPgHgOgMIgBCYIgqABgAAAiMQgOAAgMAFQgMAFgKAIQgKAHgGALQgHALgEALIAAArQADAPAHALQAIALAJAHQAKAHAMAEQALAEAMAAQAPABANgGQAOgGALgKQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgMQgKgLgOgHQgNgGgPAAIgCAAg");
	this.shape_3.setTransform(67.2,11.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_4.setTransform(28.7,1.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgpB5QgYgKgSgRQgSgSgMgYQgLgXAAgdQAAgPAFgRQAFgQAIgPQAIgOAMgNQAMgMAPgJQAOgJASgFQARgFASAAQATAAASAFQASAEAOAJQAPAJAMANQAMANAJAQIAAABIgmAXIAAgCQgGgLgIgJQgIgJgKgHQgLgGgMgEQgLgDgNAAQgSAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHASAAQALAAALgDQAMgDAKgGQAKgFAJgJQAIgIAFgKIABgBIAoAWIgBABQgJAPgNAMQgMALgPAJQgQAIgRAFQgRAEgRAAQgZAAgYgKg");
	this.shape_5.setTransform(3.8,6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_6.setTransform(-23.7,6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgIQAOgJAQgBQASgCAPAJQAGADAHAHQAGAFAFAKQAFAJAEAOQAEAMABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAEgHAIQgHAGgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_7.setTransform(-48.8,5.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgIQAOgJAQgBQASgCAPAJQAGADAHAHQAGAFAFAKQAFAJAEAOQAEAMABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAEgHAIQgHAGgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_8.setTransform(-73.9,5.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AguB7QgWgKgQgRQgQgSgKgYQgJgYAAgeQAAgdAJgYQAKgZAQgRQAQgRAWgKQAVgJAZAAQAZAAAVAKQAXAKAQASQAQARAKAYQAKAZAAAbQAAAcgKAZQgKAYgQARQgQASgXAKQgVAKgZAAQgZAAgVgJgAgghTQgOAIgJANQgJANgDARQgFAQAAAQQABAQAEAQQAEARAJANQAKANANAIQAOAJARAAQATAAAOgJQAOgIAIgNQAKgNAEgRQAEgQAAgQQAAgQgEgQQgEgRgJgNQgJgNgNgIQgOgJgUAAQgSAAgOAJg");
	this.shape_9.setTransform(-101.5,5.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgpB5QgXgKgTgRQgSgSgMgYQgLgXAAgdQAAgPAFgRQAFgQAIgPQAJgOALgNQAMgMAPgJQAPgJARgFQARgFASAAQATAAASAFQASAEAPAJQAPAJAMANQALANAJAQIAAABIglAXIgBgCQgGgLgIgJQgIgJgLgHQgKgGgMgEQgMgDgMAAQgSAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHASAAQAMAAAKgDQAMgDAKgGQAKgFAJgJQAHgIAGgKIACgBIAnAWIgBABQgIAPgOAMQgMALgPAJQgPAIgRAFQgSAEgRAAQgZAAgYgKg");
	this.shape_10.setTransform(-129.1,6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(3,1,1).p("A2qkrMAtVAAAQCgAAAACgIAAEXQAACgigAAMgtVAAAQigAAAAigIAAkXQAAigCgAAg");
	this.shape_11.setTransform(0,6.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("A2qEsQigAAABigIAAkXQgBigCgAAMAtVAAAQCgAAgBCgIAAEXQABCgigAAg");
	this.shape_12.setTransform(0,6.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-162.5,-36.7,325.1,75);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgIAQgBQASgBAPAIQAGADAHAHQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape.setTransform(260.1,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgcCpIABg6IABhGIAChgIAugCIgCA7IgBAxIgBAnIAAAeIgBAxgAgLhqQgHgDgDgEQgFgEgCgGQgDgGAAgHQAAgGADgGQACgGAFgEQADgFAHgCQAFgDAGAAQAHAAAGADQAFACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAGgEAEQgFAEgFADQgGACgHAAQgGAAgFgCg");
	this.shape_1.setTransform(240.9,0.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("ABGCAIADgfQgcARgZAIQgaAIgXAAQgQAAgPgEQgQgEgLgJQgMgJgHgNQgHgNAAgTQAAgSAGgOQAEgPAKgLQAJgKAOgIQAMgHAPgFQAPgFAQgDQAPgCAQAAQAPAAANABIAWADIgGgWQgFgLgHgJQgGgIgKgGQgKgFgNAAQgJAAgKACQgMADgNAHQgNAGgQAMQgQAMgTASIgbghQAWgUAUgNQATgNASgHQASgHAOgDQAOgDAMAAQAVAAAQAIQAQAHANAMQAMAMAJARQAHAQAGATQAFATADAVQACATAAAUQAAAWgDAZQgCAZgFAdgAgKAAQgTADgNAJQgOAJgHANQgHANAEARQADAOAKAFQAIAFANAAQANAAAPgEQAOgEAQgHIAcgNIAWgNIABgXIgBgXQgLgCgMgCQgMgCgMAAQgVAAgSAFg");
	this.shape_2.setTransform(219.8,5.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("Ah7i2IAxgCIAAAhQANgNAOgGQAOgHALgEQAOgEAMgBQAPAAAPAEQAQAFANAIQAOAIALALQAMALAIAOQAIAOAEAQQAEAQAAARQgBAUgFARQgFARgJANQgIAPgLALQgMALgMAIQgNAIgOAFQgNAEgOAAQgMgBgPgDQgMgEgPgGQgPgHgOgMIgBCYIgqABgAAAiMQgOAAgMAFQgMAFgKAIQgKAHgGALQgHALgEALIAAArQADAPAHALQAIALAJAHQAKAHAMAEQALAEAMAAQAPABANgGQAOgGALgKQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgMQgKgLgOgHQgNgGgPAAIgCAAg");
	this.shape_3.setTransform(191.9,11.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_4.setTransform(153.4,1.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgoB5QgYgKgTgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAFgQAIgPQAJgOAMgNQAMgMAOgJQAPgJAQgFQASgFARAAQAUAAASAFQASAEAPAJQAOAJANANQAMANAHAQIABABIglAXIgBgCQgFgLgJgJQgJgJgKgHQgKgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQANAAALgDQALgDAKgGQAKgFAIgJQAIgIAHgKIABgBIAmAWIAAABQgIAPgNAMQgNALgPAJQgQAIgQAFQgSAEgSAAQgYAAgXgKg");
	this.shape_5.setTransform(128.5,6.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgQgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAQAQAKAWQAKAVADAaIjCAcQACAQAGANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAAMgEQALgDAKgHQAKgIAHgKQAHgLAEgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgLAIgIAOQgJANgCATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_6.setTransform(101,6.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgIAQgBQASgBAPAIQAGADAHAHQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_7.setTransform(75.9,6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgIAQgBQASgBAPAIQAGADAHAHQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgKIgHgGQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAZIABASIgxAGIgChGg");
	this.shape_8.setTransform(50.8,6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AguB7QgWgKgQgRQgQgSgKgYQgJgYAAgeQAAgdAJgYQAKgZAQgRQAQgRAWgKQAVgJAZAAQAZAAAVAKQAXAKAQASQAQARAKAYQAKAZAAAbQAAAcgKAZQgKAYgQARQgQASgXAKQgVAKgZAAQgZAAgVgJgAgghTQgOAIgJANQgJANgDARQgFAQAAAQQABAQAEAQQAFARAIANQAKANANAIQAOAJARAAQATAAAOgJQAOgIAIgNQAKgNAEgRQAEgQAAgQQAAgQgEgQQgEgRgJgNQgJgNgOgIQgNgJgUAAQgSAAgOAJg");
	this.shape_9.setTransform(23.2,6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgpB5QgYgKgSgRQgTgSgLgYQgLgXAAgdQAAgPAFgRQAFgQAIgPQAJgOALgNQAMgMAPgJQAPgJAQgFQASgFARAAQAUAAASAFQARAEAPAJQAPAJAMANQANANAHAQIABABIgmAXIAAgCQgGgLgIgJQgJgJgJgHQgLgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQAMAAALgDQAMgDAKgGQAKgFAIgJQAJgIAFgKIABgBIAnAWIAAABQgJAPgNAMQgMALgPAJQgPAIgSAFQgRAEgSAAQgYAAgYgKg");
	this.shape_10.setTransform(-4.4,6.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_11.setTransform(-44,6.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AA8CsQAHgXADgUIAEglIAAgkQgBgXgHgSQgHgPgLgLQgKgLgNgEQgMgGgMAAQgMACgNAGQgMAFgMAMQgNAKgLAVIgCCVIgsABIgElaIA0gCIgBCJQAMgOAOgHQAOgIAMgEQAOgEANgCQAZAAAVAJQAVAJAOAQQAPARAJAZQAJAXABAfIgBAgQAAARgBAQIgDAeQgDAPgDALg");
	this.shape_12.setTransform(-72.8,0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_13.setTransform(-99,1.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_14.setTransform(-132.9,1.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("AgoB5QgYgKgTgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAFgQAIgPQAIgOANgNQALgMAPgJQAPgJAQgFQASgFARAAQAUAAASAFQASAEAPAJQAPAJAMANQALANAIAQIABABIglAXIgBgCQgGgLgIgJQgJgJgKgHQgKgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQANAAALgDQALgDAKgGQAKgFAIgJQAJgIAGgKIABgBIAmAWIAAABQgIAPgNAMQgNALgPAJQgQAIgQAFQgSAEgSAAQgYAAgXgKg");
	this.shape_15.setTransform(-157.8,6.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_16.setTransform(-185.3,6.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#663300").s().p("AAODEQgNgFgIgIQgJgJgGgKQgHgLgFgLQgKgZgDghIAGkcIAuAAIgCBJIgCA9IgBAxIAAAlIgBA9QAAAVAFAQIAFAOQAEAHAFAFQAGAGAHADQAIADAJAAIgGAtQgQAAgMgFg");
	this.shape_17.setTransform(-204.1,-2.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#663300").s().p("AgyB5QgXgJgQgRQgPgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQARgSAWgKQAXgLAYAAQAYAAAUAKQAWAJAPAQQAQAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAKAJAMAFQAOAEANAAQANAAAMgEQALgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgMAQQgLARgQAMQgPAMgSAGQgSAHgVAAQgcAAgXgJgAgShXQgMAEgKAHQgLAIgIAOQgJANgDATICHgQIgBgEQgJgXgPgMQgPgNgXAAQgIAAgLADg");
	this.shape_18.setTransform(-225.5,6.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#663300").s().p("AgdC1QgRgEgQgGQgRgHgPgKQgQgJgQgKIAhgnQASAPARAJQARAJAOAFQAQAFANACQARABAOgEQANgDALgHQAKgGAHgJQAGgJABgKQABgIgEgGQgDgHgHgHQgHgFgKgFIgUgIIgWgFIgVgFQgNgCgOgEQgOgDgOgHQgOgGgMgIQgMgJgJgNQgJgMgEgRQgFgRACgWQABgSAGgOQAGgPAKgMQAKgLANgIQAMgIAPgFQAOgGAQgCQAPgDAOAAQAWACAVAFIATAGIAUAIIAUALQAKAHAJAJIgZAnQgHgHgJgGIgQgLIgQgIIgPgFQgSgFgQgBQgUAAgQAHQgQAGgLAKQgLAJgFAMQgGAMAAANQAAALAHAKQAHALAMAJQAMAJAQAGQARAGASADQAQABAQAFQARADAPAGQAOAHANAJQAMAIAJAMQAIALAFAPQADAPgCARQgCAPgHANQgHAMgKAJQgJAJgNAHQgMAFgNAFQgOADgOACIgaABQgQAAgRgDg");
	this.shape_19.setTransform(-254.2,1.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#CC9900").ss(6,1,1).p("EgsugGPMBZdAAAQB+AABaBsQBaBrAACZIAAA/QAACZhaBrQhaBsh+AAMhZdAAAQh+AAhahsQhahrAAiZIAAg/QAAiZBahrQBahsB+AAg");

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFCC00").s().p("EgsuAGQQh+AAhahsQhahsAAiZIAAg+QAAiYBahsQBahsB+AAMBZdAAAQB+AABaBsQBaBsAACYIAAA+QAACZhaBsQhaBsh+AAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-319.9,-43,639.8,86);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol2copy15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AFhnsIiNDHAFknsIDbBAADcqhICGC0AJACvIgCDnAFbBhIDjBOALHgQIiHC9AlRnjICCC+Ao5mlIDmhBAlPnnICDi6Ao0CsIgKDtAACJNIjhBGAlUBnIjgBFIiSizAAEJNIDYBVAAEJMIAGjm");
	this.shape.setTransform(-0.7,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("ADUK5IFjkMICEBoQgnAzgvAwQicCcjCA8gApkJ4Qgygygng2IB4hhIFfD7IgoCjQi+g9iYiYgAlbB+ICFmMIGiAAICHGFIlQEGgAtiATQAAkfCjjdIB+BbIiMGcIiVAKIAAgFgALAAGIiImcIB8hhQCsDeADEkgAjTqKIg1ieQB8gnCNAAQCQAACBAqIg9Cbg");
	this.shape_1.setTransform(0,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AkOMnIAoikIlfj6IAKjtIgKDtIh4BhQijjbgCkdICVgJICSCzIDghFIjgBFIiSizICMmeIh+haQApg2AygzIAtgqQBThJBdgvQA8ggBDgVIA1CeIGoAAIA9iaQC7A8CYCXQArAsAkAuIh8BiICIGcICjAFIAAAIQAAEjioDfIiEhpIACjnIAAgCICHi9IiHC9IgCACIACAAIgCDnIljEMIAzCYQh7AmiLAAQiQAAh/gpgADUKSIjWhVIAAgBIgCABIACAAgAjmKDIDihGgAgCI8IAFjmIFQkFIiHmFICOjHIiODHImiAAIiCi/IABgDIgDABIACACICCC/IiFGLIFeD/gAI3CfIjkhOgApBm1IDnhAgAI4m8Ijcg/gAlXn2ICEi7gAFbn9IgBACIACAAIgBgCIiGi0gAgCI9gAI5CfgArNgXgAjWk0g");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy15, new cjs.Rectangle(-86.7,-86.7,173.4,173.5), null);


(lib.Symbol2copy12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AFhnsIiNDHAFknsIDbBAADcqhICGC0AJACvIgCDnAFbBhIDjBOALHgQIiHC9AlRnjICCC+Ao5mlIDmhBAlPnnICDi6Ao0CsIgKDtAACJNIjhBGAlUBnIjgBFIiSizAAEJNIDYBVAAEJMIAGjm");
	this.shape.setTransform(-0.7,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("ADUK5IFjkMICEBoQgnAzgvAwQicCcjCA8gApkJ4Qgygygng2IB4hhIFfD7IgoCjQi+g9iYiYgAlbB+ICFmMIGiAAICHGFIlQEGgAtiATQAAkfCjjdIB+BbIiMGcIiVAKIAAgFgALAAGIiImcIB8hhQCsDeADEkgAjTqKIg1ieQB8gnCNAAQCQAACBAqIg9Cbg");
	this.shape_1.setTransform(0,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AkOMnIAoikIlfj6IAKjtIgKDtIh4BhQijjbgCkdICVgJICSCzIDghFIjgBFIiSizICMmeIh+haQApg2AygzIAtgqQBThJBdgvQA8ggBDgVIA1CeIGoAAIA9iaQC7A8CYCXQArAsAkAuIh8BiICIGcICjAFIAAAIQAAEjioDfIiEhpIACjnIAAgCICHi9IiHC9IgCACIACAAIgCDnIljEMIAzCYQh7AmiLAAQiQAAh/gpgADUKSIjWhVIAAgBIgCABIACAAgAjmKDIDihGgAgCI8IAFjmIFQkFIiHmFICOjHIiODHImiAAIiCi/IABgDIgDABIACACICCC/IiFGLIFeD/gAI3CfIjkhOgApBm1IDnhAgAI4m8Ijcg/gAlXn2ICEi7gAFbn9IgBACIACAAIgBgCIiGi0gAgCI9gAI5CfgArNgXgAjWk0g");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy12, new cjs.Rectangle(-86.7,-86.7,173.4,173.5), null);


(lib.Symbol2copy11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AFhnsIiNDHAFknsIDbBAADcqhICGC0AJACvIgCDnAFbBhIDjBOALHgQIiHC9AlRnjICCC+Ao5mlIDmhBAlPnnICDi6Ao0CsIgKDtAACJNIjhBGAlUBnIjgBFIiSizAAEJNIDYBVAAEJMIAGjm");
	this.shape.setTransform(-0.7,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("ADUK5IFjkMICEBoQgnAzgvAwQicCcjCA8gApkJ4Qgygygng2IB4hhIFfD7IgoCjQi+g9iYiYgAlbB+ICFmMIGiAAICHGFIlQEGgAtiATQAAkfCjjdIB+BbIiMGcIiVAKIAAgFgALAAGIiImcIB8hhQCsDeADEkgAjTqKIg1ieQB8gnCNAAQCQAACBAqIg9Cbg");
	this.shape_1.setTransform(0,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AkOMnIAoikIlfj6IAKjtIgKDtIh4BhQijjbgCkdICVgJICSCzIDghFIjgBFIiSizICMmeIh+haQApg2AygzIAtgqQBThJBdgvQA8ggBDgVIA1CeIGoAAIA9iaQC7A8CYCXQArAsAkAuIh8BiICIGcICjAFIAAAIQAAEjioDfIiEhpIACjnIAAgCICHi9IiHC9IgCACIACAAIgCDnIljEMIAzCYQh7AmiLAAQiQAAh/gpgADUKSIjWhVIAAgBIgCABIACAAgAjmKDIDihGgAgCI8IAFjmIFQkFIiHmFICOjHIiODHImiAAIiCi/IABgDIgDABIACACICCC/IiFGLIFeD/gAI3CfIjkhOgApBm1IDnhAgAI4m8Ijcg/gAlXn2ICEi7gAFbn9IgBACIACAAIgBgCIiGi0gAgCI9gAI5CfgArNgXgAjWk0g");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy11, new cjs.Rectangle(-86.7,-86.7,173.4,173.5), null);


(lib.Symbol2copy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AFhnsIiNDHAFknsIDbBAADcqhICGC0AJACvIgCDnAFbBhIDjBOALHgQIiHC9AlRnjICCC+Ao5mlIDmhBAlPnnICDi6Ao0CsIgKDtAACJNIjhBGAlUBnIjgBFIiSizAAEJNIDYBVAAEJMIAGjm");
	this.shape.setTransform(-0.7,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("ADUK5IFjkMICEBoQgnAzgvAwQicCcjCA8gApkJ4Qgygygng2IB4hhIFfD7IgoCjQi+g9iYiYgAlbB+ICFmMIGiAAICHGFIlQEGgAtiATQAAkfCjjdIB+BbIiMGcIiVAKIAAgFgALAAGIiImcIB8hhQCsDeADEkgAjTqKIg1ieQB8gnCNAAQCQAACBAqIg9Cbg");
	this.shape_1.setTransform(0,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AkOMnIAoikIlfj6IAKjtIgKDtIh4BhQijjbgCkdICVgJICSCzIDghFIjgBFIiSizICMmeIh+haQApg2AygzIAtgqQBThJBdgvQA8ggBDgVIA1CeIGoAAIA9iaQC7A8CYCXQArAsAkAuIh8BiICIGcICjAFIAAAIQAAEjioDfIiEhpIACjnIAAgCICHi9IiHC9IgCACIACAAIgCDnIljEMIAzCYQh7AmiLAAQiQAAh/gpgADUKSIjWhVIAAgBIgCABIACAAgAjmKDIDihGgAgCI8IAFjmIFQkFIiHmFICOjHIiODHImiAAIiCi/IABgDIgDABIACACICCC/IiFGLIFeD/gAI3CfIjkhOgApBm1IDnhAgAI4m8Ijcg/gAlXn2ICEi7gAFbn9IgBACIACAAIgBgCIiGi0gAgCI9gAI5CfgArNgXgAjWk0g");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy10, new cjs.Rectangle(-86.7,-86.7,173.4,173.5), null);


(lib.Symbol2copy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AFhnsIiNDHAFknsIDbBAADcqhICGC0AJACvIgCDnAFbBhIDjBOALHgQIiHC9AlRnjICCC+Ao5mlIDmhBAlPnnICDi6Ao0CsIgKDtAACJNIjhBGAlUBnIjgBFIiSizAAEJNIDYBVAAEJMIAGjm");
	this.shape.setTransform(-0.7,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("ADUK5IFjkMICEBoQgnAzgvAwQicCcjCA8gApkJ4Qgygygng2IB4hhIFfD7IgoCjQi+g9iYiYgAlbB+ICFmMIGiAAICHGFIlQEGgAtiATQAAkfCjjdIB+BbIiMGcIiVAKIAAgFgALAAGIiImcIB8hhQCsDeADEkgAjTqKIg1ieQB8gnCNAAQCQAACBAqIg9Cbg");
	this.shape_1.setTransform(0,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AkOMnIAoikIlfj6IAKjtIgKDtIh4BhQijjbgCkdICVgJICSCzIDghFIjgBFIiSizICMmeIh+haQApg2AygzIAtgqQBThJBdgvQA8ggBDgVIA1CeIGoAAIA9iaQC7A8CYCXQArAsAkAuIh8BiICIGcICjAFIAAAIQAAEjioDfIiEhpIACjnIAAgCICHi9IiHC9IgCACIACAAIgCDnIljEMIAzCYQh7AmiLAAQiQAAh/gpgADUKSIjWhVIAAgBIgCABIACAAgAjmKDIDihGgAgCI8IAFjmIFQkFIiHmFICOjHIiODHImiAAIiCi/IABgDIgDABIACACICCC/IiFGLIFeD/gAI3CfIjkhOgApBm1IDnhAgAI4m8Ijcg/gAlXn2ICEi7gAFbn9IgBACIACAAIgBgCIiGi0gAgCI9gAI5CfgArNgXgAjWk0g");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy7, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Symbol2copy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AFhnsIiNDHAFknsIDbBAADcqhICGC0AJACvIgCDnAFbBhIDjBOALHgQIiHC9AlRnjICCC+Ao5mlIDmhBAlPnnICDi6Ao0CsIgKDtAACJNIjhBGAlUBnIjgBFIiSizAAEJNIDYBVAAEJMIAGjm");
	this.shape.setTransform(-0.7,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A0A0A").s().p("ADUK5IFjkMICEBoQgnAzgvAwQicCcjCA8gApkJ4Qgygygng2IB4hhIFfD7IgoCjQi+g9iYiYgAlbB+ICFmMIGiAAICHGFIlQEGgAtiATQAAkfCjjdIB+BbIiMGcIiVAKIAAgFgALAAGIiImcIB8hhQCsDeADEkgAjTqKIg1ieQB8gnCNAAQCQAACBAqIg9Cbg");
	this.shape_1.setTransform(0,-1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#ECE7E7").s().p("AkOMnIAoikIlfj6IAKjtIgKDtIh4BhQijjbgCkdICVgJICSCzIDghFIjgBFIiSizICMmeIh+haQApg2AygzIAtgqQBThJBdgvQA8ggBDgVIA1CeIGoAAIA9iaQC7A8CYCXQArAsAkAuIh8BiICIGcICjAFIAAAIQAAEjioDfIiEhpIACjnIAAgCICHi9IiHC9IgCACIACAAIgCDnIljEMIAzCYQh7AmiLAAQiQAAh/gpgADUKSIjWhVIAAgBIgCABIACAAgAjmKDIDihGgAgCI8IAFjmIFQkFIiHmFICOjHIiODHImiAAIiCi/IABgDIgDABIACACICCC/IiFGLIFeD/gAI3CfIjkhOgApBm1IDnhAgAI4m8Ijcg/gAlXn2ICEi7gAFbn9IgBACIACAAIgBgCIiGi0gAgCI9gAI5CfgArNgXgAjWk0g");
	this.shape_2.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy6, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Symbol2copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.4)").s().p("AgqBMQgVgNAAgdQAAgdASgfQATggAagNQAZgPASALQAVAMAAAdQAAAfgRAdQgSAggcAQQgNAHgMAAQgKAAgIgFg");
	this.shape.setTransform(-14,-82.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.4)").s().p("AhREWQhYhRgliNQgliMAihzQAjhzBVgYQBWgVBUBTQBYBRAmCNQAmCLgkBzQghBzhVAXQgTAFgTAAQhBAAhFhBg");
	this.shape_1.setTransform(31.2,-37.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("rgba(165,146,141,0.302)").ss(1,1,1,3,true).p("AqvCcQK9nEKiDO");
	this.shape_2.setTransform(-0.1,-24.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("rgba(102,40,26,0.302)").ss(1,1,1,3,true).p("AqolmIACAAQAsjrB9igQAMgVARgUQDPjxEgADQEgAIDGD4QBRBlAtBzQAsBsAQB5QABAPADANQAEA/gBA8QgIFdjAFQQhlC0iFBJQg2AihAAOQgfAEgTA8QgGAagNAMIAAAFIAAABIAwAzIAUAAQgXAtgvAUQgDgBAAAAABCOhIACgRQgggYgJgYQgbgwggAAQgBgBgEgCQgBACgDAAQgagHgZgIQglgOgVgHQgQgIgUgJQinhdiHjRQivkGgZkaQgGg7ABg8QgBhJAOhEQAAgIAAgGABEPpIAAAcQgLAGgLgDIAUhnIACBIIAFhXAqrlWQADgKAAgG");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(219,167,104,0.612)").s().p("AgKAzIAThmIACBHIAAAcQgIAEgGAAIgHgBg");
	this.shape_4.setTransform(5.7,98.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(100,38,0,0.612)").s().p("AgjA5IAAgcIAEhWIAvAzIAVAAQgYAsgtAUIgDgBg");
	this.shape_5.setTransform(10.5,97.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF77BB").s().p("AgDgbIADgRIADACIAAABIgDBWg");
	this.shape_6.setTransform(7,95.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(196,46,9,0.612)").s().p("AACgHIgBAMIgCADQACgJABgGg");
	this.shape_7.setTransform(-68.2,-35.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFA6B4","#FF2323"],[0.031,1],20,-16.4,0,20,-16.4,49.2).s().p("AqxCIQgBhIAOhEQJtlZKuCBQAsBsAQB5QqjjNq8HEQgGg8ABg8g");
	this.shape_8.setTransform(-0.4,-34.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#F3FF00","#FF8B00"],[0.031,1],23.9,-50.7,0,23.9,-50.7,78.6).s().p("ABBKNQgfgXgJgYQgagwgigBIgFgDQAAABAAAAQgBAAAAABQAAAAgBAAQAAAAgBAAQgbgGgYgIIg6gVIglgRQimhdiIjRQivkGgZkaQK9nFKiDOQABAPADANQAEA+gBA8QgHFfjBFPQhlC0iEBJQg2AhhBAOQgfAFgSA7QgGAbgOALIAAAGg");
	this.shape_9.setTransform(0.3,25.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#C7FA10","#00B606"],[0.031,1],17.2,5.1,0,17.2,5.1,56).s().p("AqMFKIABAAQAsjrB+ifQAMgVAQgUQDPjxEgADQEgAIDGD4QBRBkAtBzQquiAptFYIABgOgAqIFKIAGgGQACgDgCgEg");
	this.shape_10.setTransform(-2.7,-68.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy5, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Symbol2copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CCFF00").s().p("AiyD4QBlgHBfhoQBphuAmicIAIglQAJgtABglQgBA9gRBJQgHAbgLAdQgoBxhOBXQhbBkhgAGIgQABg");
	this.shape.setTransform(71.2,-2.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#003300").s().p("AmRJLIAUgNIHjpTQA/g3BCgOQgGg2ATg0IApgyQglBcAHBbQhEgThRBnInjJTIgJABQgUAAAFgegAm5JiIl2jaIgwhIIGKDsQAEANAMAHIAOAigACDBTIA6hHQArgrAsAuIAAADQAGAqAOAkQAaBEAqAfgAFQAxQgwhsAnidQAoifBnhwQBohuBqAAQBsAAAvBuQAdBDAABVQgDhGgag4QgrhphmAAQhpAAhiBpQhkBrglCZQgmCVAvBrQAqBfBVAJQADACAEAAIAAABQhsAAgxhxgAjEmcIC0jMIgFBcIAAAFIgCAFIh4B9g");
	this.shape_1.setTransform(2.6,6.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000B0D").s().p("AiRAtIA2iYIACgFIAAgFIDrBpIh2CDg");
	this.shape_2.setTransform(9.4,-34.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#006600").s().p("AssHMIgCAAIhPhxIAtAbIAwBIIF2DaIAQAXIAYAfgAGrEQIgwgXQgqgfgahEQgOgkgGgqIAAgDQgPhlAgiCQAgh9BBhmQCWi7CSgRQAbgDAYACIABAAQBOAXAkBhQAzCEgvC0QgeB+hBBjQgeAuglApQh0CDhzAAQgbAAgYgJgAHlmxQhnBwgoCfQgnCdAwBsQAxBxBsAAIAHAAIAHAAIAQgBQBfgGBchkQBOhXAphxQAKgdAHgbQAShJAAg9QAAhVgdhDQgvhuhsAAQhqAAhoBugAEDiOQgTA0AGA2QhCAOg/A2gAiAlPIB4h9Ig2CZgAgGnWIAFhcIADgFIEqB8IhHBOgAKcpzQAlgsgOgsQAogJAMAcQAABJgfANg");
	this.shape_3.setTransform(1.1,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#99FF00").s().p("AmZKeQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAInmkZQAQgNAkAAIAAADIHaETIMLunQBuiIBsgvIATgIQATgHAQgDIAVgDIm1inQiOgriRCDIADgDQCWiSCWAvIDZBSIA/AYIC8hcQAEAEACAGIi5BVIBaAiIAgALIAsARIAeALIAEADIAGACQgYgCgbADQiSARiWC7IsTO0QgdgQgTAKgAIKFKQhVgIgqhgQgvhrAmiVQAliZBjhrQBjhoBpAAQBlAAAsBoQAaA4ADBGQgBAmgJAsQANhmgjhOQgshlhiAAQhjAAhgBlQhdBngnCPQgiCQAtBmQAmBVBKAPg");
	this.shape_4.setTransform(-0.9,-10.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#009900").s().p("AlDLnIgigSIgYgfIgQgXIACAAIgOgjQgMgHgEgNImKjrIgtgcIghgSQgEgLAAgHIHcEXIAHgXIANgMQABAAAAgBQAAAAABAAQAAgBABAAQABAAAAAAQAUgKAcAQIMTu0QhBBmggB9QggCDAPBlQgsgwgrAtIg6BHImwIVQARAqgkAdQgMAEgKAAQgPAAgKgJgAEgiHIiOCuInjJTIgUANQgGAlAegHIHjpUQBRhnBEATQgHhaAlhcgAIZDdQgEAAgDgCIABAAQhLgPgmhVQgthmAjiQQAmiPBdhnQBghlBjAAQBiAAAtBlQAiBOgNBmIgIAlQglCchqBuQhfBphlAGIgHABIgHgBgALlpcQAfgNAAhJQgMgbgoAIQg+ALAIBCIhbgiIC5hVIACAAQAgAOAEA6QABA5gcAdg");
	this.shape_5.setTransform(-1.8,0.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00CC00").s().p("As6GXIAAgDQgFgYAPgKIHmEZIgNALIgHAYgAr9FoIACgEIM0urQCSiDCOArIG1CnIgVADQgRADgSAGIgTAJQhsAvhuCHIsLOogABmnSIi0DNIA2AWIBAAcICuBKIB2iDIBHhPIkqh8gAJCpbICxhSQA1AAAUAOIi7Bcg");
	this.shape_6.setTransform(-9.2,-8.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.502)").s().p("AmPFhImMjfIOdnlIJ8A8IAeAVIl8CaQgFAAgDAEIr9HNIAAADQgOAIgMAAQgJAAgHgDg");
	this.shape_7.setTransform(12.4,43);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy4, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282727").s().p("AHQE9QgjAAgbgbQgYgaAAglIAAipIDVAAIAEAAIAACpQAAAlgbAaQgXAbgkAAgAn8E9QglAAgagbQgXgaAAglIAAipIDYAAIAACpQAAAlgaAaQgYAbgkAAgAjRgSQgMAAgKgKQgJgIAAgNQAAgMAJgLQAKgIAMAAIGfAAQANAAAJAIQAKALAAAMQAAANgKAIQgJAKgNAAgAjRiIQgMAAgKgKQgJgIAAgNQAAgNAJgJQAKgJAMgBIGfAAQANABAJAJQAKAJAAANQAAANgKAIQgJAKgNAAgAjRj+QgMABgKgLQgJgIAAgMQAAgNAJgKQAKgIAMgBIGfAAQANABAJAIQAKAKAAANQAAAMgKAIQgJALgNgBg");
	this.shape.setTransform(5.3,54.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A3737").s().p("AjfC0QhDAAAAhDIAAh7IAAg2IAAgwIABgSQAHgxA7AAIG+AAQA7AAAIAxQABAIAAAKIAAAwIAAA2IAAB7QAABDhEAAgAjiBeQgIALAAALQAAANAIAJQAKAKANAAIGfAAQANAAAIgKQALgJAAgNQAAgLgLgLQgIgJgNAAImfAAQgNAAgKAJgAjigWQgIAJAAAMQAAAMAIAJQAKAJANAAIGfAAQANAAAIgJQALgJAAgMQAAgMgLgJQgIgKgNAAImfAAQgNAAgKAKgAjiiMQgIAJAAANQAAAMAIAJQAKAKANAAIGfAAQANAAAIgKQALgJAAgMQAAgNgLgJQgIgJgNAAImfAAQgNAAgKAJg");
	this.shape_1.setTransform(4.7,37.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AISBqQgigEgZgaQgLgMgHgMQgOgXAAgcQAAgqAggfQAfghAsAAQAsAAAfAhQAeAfAAAqQAAAcgLAXQgIAMgLAMQgaAaghAEgApZBbQgKgHgIgIQgMgMgFgMQgOgXAAgcQAAgqAfgfQAfghAsAAQArAAAhAhQAfAfAAAqQAAAcgOAXQgFAMgMAMQgSASgSAHQgRAEgRAAQgiAAgdgOg");
	this.shape_2.setTransform(4.8,26);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#212B28").s().p("AIzCXQAhgEAZgaQAMgMAHgMIAMAAIBPAAIAAA2gAEjCXIAAg2ICNAAIAVAAQAHAMAMAMQAYAaAiAEgAoOCXQALgBAMgEQASgHASgSQALgMAGgMIAWAAICJAAIAAA2gAraCXIAAg2IBSAAIAMAAQAGAMALAMQAIAIAKAHQASAMAZADgAKMhSQgDgGgGgGQgpgog8AAQg7AAgrAoIgJAMItbAAIgKgMQgpgog7AAQg8AAgpAoIgLAMIhQAAQAAgmAOgeIWcAAIADBEg");
	this.shape_3.setTransform(4.8,21.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DEB50A").s().p("AJ/B0QALgXAAgcQAAgrgegeQgfghgsAAQgsAAgfAhQggAeAAArQAAAcAOAXIgWAAQggglAAg2QAAgxAfgnIAKgMQAqgoA7AAQA8AAApAoQAHAGACAGQAhAnAAAxQAAA2ghAlgAnEB0QAOgXAAgcQAAgrgfgeQghghgrAAQgsAAgfAhQgfAeAAArQAAAcAOAXIgNAAQggglAAg2QAAgxAfgnIAKgMQApgoA9AAQA7AAApAoIAKAMQAfAnAAAxQAAA2gfAlg");
	this.shape_4.setTransform(4.9,19.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FECF09").s().p("AJUDrIjVAAIr0AAIjYAAIgHAAQg2gBgggWQgwgfAAhPIAAhmICsAAIAOABIASgBIDrAAIAAB8QAABDBDAAIG/AAQBEAAAAhDIAAh8IDvAAIARABIAQgBICoAAIAABmQAABPgvAfQggAVg0ACIgEAAgAKMg1QAgglAAg2QAAgzgggmIBHAAIACAyIAGCCgAEjg1IAAgxQAAgJgCgIQgHgyg7ABIm/AAQg6gBgIAyIgBARIAAAxIiJAAQAfglAAg2QAAgzgfgmINbAAQgfAmAAAzQAAA2AgAlgArag1IAAi0IBQAAQgeAmAAAzQAAA2AgAlg");
	this.shape_5.setTransform(4.8,36.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EEBE25").s().p("ArQE1QAcg9BcAAIADAAISlAAIAFAAQBZAAAfA9gAJVDaIgDAAQAAgBAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAgBgBAAQAAgBAAgBQAAAAABgBQAAAAAAAAQAAgCADgDIACAAQAegJAVgWQAmgmgFhMIAAgCQgVgBgOgRQgTgQAAgXIAAh7QAAgXATgSQAPgRAYAAIABAAIgBgPQgGgogfgeQgVgXgegJIgCAAQAAAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAAAIAAgCQAAAAAAgBQgBgBAAAAQAAgBAAAAQABgBAAAAIACgFIADAAIACAAIARAGQAWAKATATQAgAhAHAtQACAIAAAIIAAABQARADANANQARASAAAXIAAB7QAAAXgRAQQgQARgUABQAFBRgoAsQgZAZghAJIgCAAgApVDaIgDAAQgfgJgagZQgngsADhRQgTgBgPgRQgRgQAAgXIAAh7QAAgXARgSQALgNAUgDIAAgBQABg1AmgpQAagaAfgJIADAAIACAAQABABAAAAQABABAAAAQAAABAAAAQAAABAAABQABAAAAABQAAAAAAABQAAAAAAABQAAABgBAAQAAABAAAAQAAAAAAABQAAAAgBAAQAAABgBAAIgCAAQgdAJgXAXQgiAjgDAyIABAAQAYAAAQARQATASAAAXIAAB7QAAAXgTAQQgPARgWABIAAACQgEBMAlAmQAXAWAdAJIACAAQADADgBACQABAAAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAABAAAAQAAAAAAABQAAAAgBAAQAAAAgBABIgCAAg");
	this.shape_6.setTransform(5.1,-24.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#98C8F7").s().p("AmwD8QAjhQA8hIQCDiLDjhfQC3hMDegoQAHAOAAAWIAABbIgBAAQkmBSjTCRQhkBFhMBPg");
	this.shape_7.setTransform(16.7,-28.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#84C0FC").s().p("AHmELIpnAAQBMhPBihFQDUiREnhSIABAAIAAE0QAAA7g0AIIgPAAgAnlELIgIAAQg7gFAAg+IAAmPQAAgoAXgQQASgLAaAAIPLAAQAbAAAQALQALAHAGANQjeAoi3BMQjkBgiDCKQg7BJgkBPg");
	this.shape_8.setTransform(4.8,-29.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F7FFC2").s().p("Ak8A5QgYAAgPgPQgSgSABgYQgBgWASgRQAPgRAYAAIJ5AAQAXAAARARQARARAAAWQAAAYgRASQgRAPgXAAg");
	this.shape_9.setTransform(6.9,-69.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E0AE10").s().p("AJRGLIymAAIAAgeIADAAQABgBAAAAQAAAAABAAQAAgBAAAAQAAAAAAgBQAAgBABAAQAAgBAAgBQAAAAAAgBQgBAAAAAAQAAgCgCgDIgDAAIAAn2IADAAQAAAAABgBQAAAAABAAQAAgBAAAAQAAAAAAgBQAAAAABgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBIgDAAIAAhjQAAiGCFAAIOiAAQCEAAAACGIAABjIgDAAIgCAFQgBAAAAABQAAAAAAABQAAAAAAABQABABAAAAIAAACQAAAAAAAAQABABAAAAQAAAAABAAQAAAAABAAIACAAIAAH2IgCAAQgDADAAACQAAAAgBAAQAAABAAAAQAAABAAABQAAAAABABQAAABAAAAQAAAAABABQAAAAAAAAQABAAAAABIADAAIAAAeIgFAAgAoTihQgYAQAAAoIAAGPQAAA+A8AFIAHAAICsAAIC4AAIJnAAIAPAAQA0gIAAg7IAAk0IAAhbQAAgWgHgOQgGgNgLgHQgPgLgcABIvLAAQgagBgRALgAlRlWQgRARAAAWQAAAZARARQAPAQAZAAIJ5AAQAXAAAQgQQARgRAAgZQAAgWgRgRQgQgSgXAAIp5AAQgZAAgPASg");
	this.shape_10.setTransform(5,-39.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy2, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E794BE").s().p("AgfApQAZgeAPgoIADgGQAIgXAEgVQAEATABASQADANAAAPQgBAggMAfQgEAQgIAPg");
	this.shape.setTransform(67,-31.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F3409A").s().p("Ai/iFQBEhABeADQBcACBDBGQAHAHAGAIQAGAIAEAHQAnA7AABHQgCAigKAdQgNApgeAjQgJALgJAIgACHA2IgCAGQgQAogZAfIAmAnQAIgQAFgPQALgfABghQABgPgDgNQgBgTgEgTQgEAWgJAXg");
	this.shape_1.setTransform(54.6,-40.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E794BE").s().p("AgTAxQgKgfgCggQAAgPACgNQADgSAFgTQACAVAIAXIADAGQAQAoAYAeIgkAnQgJgPgGgQg");
	this.shape_2.setTransform(-62.6,-31.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F3409A").s().p("AiHCxQgfgjgNgpQgKgdgCgiQAAhHAmg7IAKgPIAPgPQBBhGBcgCQBfgDBEBAIk2FJQgJgIgIgLgAiZAvQgCANAAAPQABAhALAfQAGAPAIAQIAlgnQgXgfgRgoIgDgGQgJgXgCgWQgEATgDATg");
	this.shape_3.setTransform(-50.2,-40.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#6666FF").s().p("AH5CiQgDh2hVhSQhYhXh7AAImbAAQh7AAhYBXQhUBSgDB2IguAAQAAiFBehfQBghfCHAAIHBAAQCFAABhBfQBfBfAACFg");
	this.shape_4.setTransform(2.6,-70.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_5.setTransform(-15.6,-38.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_6.setTransform(-21.5,-38.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#993300").s().p("AANCKQgNgOAAgUQAAgKAEgJIjXiPQgoghhNg8QBFAtBDAlIDJCQIAEgDQAPgQAUAAQATAAANAOIAIAKIBzg5QgDgIAGgNQAFgJALgEIARgDQAHAAAIgDIAdgEQAdgEARgIQgWAOgVAVQgGAKgEAFQgGAJgGAFQgJAMgNADQgDACgEAAIgIAAIgJgDQgHgEgBgGIh0A7QACAEAAAGQAAAUgPAOQgOAOgTAAQgUAAgPgOg");
	this.shape_7.setTransform(-2.2,-10);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgUAgQgHgLAAgUQAAgWAHgKQAGgLAOAAQAPAAAGALQAHALAAAUQAAAVgHALQgGALgPAAQgOAAgGgLgAgGgYQgCAFAAATQAAAUACAFQACAEAEAAQAEAAACgEQACgEAAgVQAAgSgBgFQgCgFgFAAQgEAAgCAEg");
	this.shape_8.setTransform(-32.8,-22.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_9.setTransform(-38.6,-23);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgYAWIAQgDQABAKAHAAQAKAAAAgXIAAgDQgFAIgJAAQgKAAgGgIQgGgIAAgKQAAgLAHgIQAGgIANAAQALAAAGAHQAGAGACAJQACAKAAAKQAAArgbAAQgTAAgFgVgAgGgZQgCADAAAGQAAANAIAAQAJAAAAgNQAAgGgCgDQgDgDgEAAQgDAAgDADg");
	this.shape_10.setTransform(-41.6,-1.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgUAkQgHgGAAgKQAAgGADgFQADgEAHgEQgLgHAAgMQABgKAGgHQAHgHAMAAQANAAAFAGQAHAGAAAJQAAAMgKAGQAMAHAAAOQAAAKgHAHQgIAIgNAAQgOAAgGgHgAgLATQAAAFAEADQADADAEAAQAFAAADgDQADgDAAgEQAAgDgCgDQgDgDgJgFQgIAFAAAIgAgFgbQgDACABAEQAAAFAEADIAHAFQAHgGgBgGQAAgEgCgDQgCgDgFAAQgDAAgDADg");
	this.shape_11.setTransform(-36.1,20.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgNApQAAgmAXgbIgiAAIABgQIAwAAIAAAOQgTAZgBAqg");
	this.shape_12.setTransform(-23.1,38.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgaABQAAgVAIgLQAHgLANAAQAJAAAHAGQAGAFACALIgPAFQgCgNgIAAQgEAAgDAFQgDAFAAASQAFgKAJAAQAJAAAGAHQAHAGAAAMQAAAOgIAHQgHAHgLAAQgbAAAAgqgAgIAQQAAAHACADQADADADAAQAJAAAAgOQAAgNgJAAQgIAAAAAOg");
	this.shape_13.setTransform(1.8,44.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgSAkQgHgHgCgKIARgEQABAMAJAAQAKAAAAgOQAAgFgDgEQgDgEgEABQgGAAgDAGIgOgDIADgtIArAAIgCAQIgcAAIgBATQAFgIAIAAQAJABAHAGQAHAHAAAMQAAAPgIAIQgIAHgMAAQgLAAgHgGg");
	this.shape_14.setTransform(22.7,38.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AACAqIAAgTIgdAAIAAgPIAbgxIAUAAIAAAxIAJAAIAAAPIgJAAIAAATgAgPAJIASAAIAAggg");
	this.shape_15.setTransform(39.4,23.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgRAlQgHgGgCgNIAPgDQACANAJAAQADAAADgDQADgDAAgGQAAgLgMAAIgFAAIAAgMIAFAAIAGgBQACgBABgCQACgDAAgEQAAgKgIAAQgHAAgCALIgPgDQAFgWAVAAQALAAAHAGQAGAGAAALQAAANgNAFQAOADABAQQAAAMgIAGQgHAHgMAAQgKAAgIgGg");
	this.shape_16.setTransform(45.2,2.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgaAqIAAgQQAXgUAEgGQAGgJAAgHQAAgKgHAAQgFAAgCAEQgCAEAAAIIgRgEQAEgbAWAAQANAAAGAHQAHAHAAAKQAAALgHAIQgIAKgSAOIAiAAIgBAQg");
	this.shape_17.setTransform(39.6,-21.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_18.setTransform(24.8,-37.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgaAqIAAgQQAXgTAFgHQAFgIAAgIQAAgKgHAAQgFAAgBAEQgDAEgBAHIgQgDQAEgbAWAAQANAAAHAHQAGAHAAAKQAAAKgHAJQgIALgTANIAjAAIgBAQg");
	this.shape_19.setTransform(6.2,-43);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgWAqIAAgOIAOAAIAAgwQgGAHgIAEIAAgQQALgGAHgKIANAAIAABFIAOAAIAAAOg");
	this.shape_20.setTransform(0.3,-43);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF9900").s().p("AmCHrQgcgXgagZIgBAAQhMhNgrhYQg/h9AAiYQAAkCC3i4QC3i3ECAAQEBAAC3C3QC3C4AAECQAAECi1C4IgCAAQi3C2kBAAQjdAAimiGgAmbmaQiqCqAADxQAACYBFB9QApBIA8A9QAPAQASAQQAfAZAgAVQCKBcCxAAQDZAACiiKQARgQAOgQQCsiqAAjwQAAjxisiqQiqirjwABQjxgBiqCrg");
	this.shape_21.setTransform(2.4,0.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF6600").s().p("Ak7HpQgggVgfgZQgSgQgPgQQg8g9gphIQhFh9AAiYQAAjxCqiqQCqirDxABQDwgBCqCrQCsCqAADxQAADwisCqQgOAQgRAQQiiCKjZAAQixAAiKhcgAiAoGQiGAehpBkIgLALQhkBjglCCQgUBHAABOQAABBAPA+QASBSAsBEQAfA1AsAsIAFAFQAdAdAiAZIALAJQBLA0BYAWQBDASBKABQBKgBBDgSQBWgWBNg0QAlgbAjgjIADgBQCdieAAjdQAAhMgThDQgkiEhmhnQhmhniCgjQhFgVhOABQhCAAg+ARg");
	this.shape_22.setTransform(2.4,0.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AiGHwQhEgRg9glQgbgQgYgVQgYgSgZgYIgCgDQgngngcgrQgvhIgThTQgPg6AAhBQAAhMAUhEQAih6BghhIAKgJQBlhiCDgeQA5gOBCAAQBLAABDATQB7AjBgBhIABACQBjBiAiB+QARBBAABIQAABGgPBAQgiCAhmBlIAAACQguAugzAfQg9AlhFARQhBAThFAAQhHAAhBgTg");
	this.shape_23.setTransform(2.3,0.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CC3333").s().p("AiNIGQhXgXhMg0IgLgJQgigZgdgdIgEgEQgtgtgfg0QgshFgShSQgPg+AAhBQAAhOAVhHQAkiCBkhjIALgKQBphlCGgeQA+gRBCAAQBOAABGAVQCBAjBmBmQBmBoAlCDQASBEAABLQAADeidCdIgDABQgjAkglAaQhNA0hVAXQhDAShLAAQhJAAhEgSgAh6nzQiDAdhlBjIgKAJQhgBhgiB6QgVBDABBMQgBBBAQA7QASBTAwBIQAbArAnAmIADADQAZAZAYARQAXAVAcARQA9AkBEASQBAASBHAAQBGAABAgSQBGgSA9gkQAzggAtgtIABgDQBmhkAhiAQAQhAAAhHQAAhHgShBQghh/hjhhIgBgCQhghih8giQhCgThMAAQhBAAg5AOg");
	this.shape_24.setTransform(2.4,0.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#993333").s().p("ABjEKIkknlIBQguIEyITg");
	this.shape_25.setTransform(37.2,60.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#993333").s().p("AjBEKIEzoTIBQAuIklHlg");
	this.shape_26.setTransform(-30.1,60.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(255,255,255,0.02)").s().p("AtQNRQlflggBnxQABnwFflgQFglfHwgBQHxABFgFfQFfFgABHwQgBHxlfFgQlgFfnxABQnwgBlglfg");

	this.timeline.addTween(cjs.Tween.get(this.shape_27).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-120,-120,240.1,240.1), null);


(lib.Tween41 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy4();
	this.instance.parent = this;
	this.instance.setTransform(297.2,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy15();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.5,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-292.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(297.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.5,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-292.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Tween40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy4();
	this.instance.parent = this;
	this.instance.setTransform(297.2,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy15();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.5,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-292.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(297.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.5,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-292.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween33("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-162.5,-36.7,325.1,75);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy6();
	this.instance.parent = this;
	this.instance.setTransform(-144.8,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-244.8,-219.3,195,195);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy4();
	this.instance.parent = this;
	this.instance.setTransform(297.2,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy11();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.5,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_2 = new lib.Symbol2copy5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-292.1,121.1,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(297.2,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_4 = new lib.Symbol2copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.5,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.instance_5 = new lib.Symbol2copy10();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-292.1,-122.6,0.812,0.812,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,0.855,0.855);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-434.4,-233.4,868.9,467);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(342.8,324.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(189).to({_off:false},0).to({_off:true},20).wait(73).to({_off:false,x:640.8,y:576.3},0).wait(49).to({startPosition:14},0).to({alpha:0,startPosition:1},7).wait(1));

	// hand
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(254.2,409.5,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(160).to({_off:false},0).to({_off:true},30).wait(61).to({_off:false,x:570.2,y:619.5},0).to({_off:true},31).wait(57));

	// arrow
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(347.1,188.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(129).to({_off:false},0).to({_off:true},31).wait(60).to({_off:false,x:647.1,y:438.5},0).to({_off:true},31).wait(88));

	// 2ND BALL
	this.instance_3 = new lib.Symbol2copy7();
	this.instance_3.parent = this;
	this.instance_3.setTransform(642.5,572,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(282).to({_off:false},0).to({regX:3.3,regY:-0.8,scaleX:1.12,scaleY:1.12,x:642.6},17).wait(32).to({alpha:0},7).wait(1));

	// Layer_2
	this.instance_4 = new lib.Symbol2copy15();
	this.instance_4.parent = this;
	this.instance_4.setTransform(642.5,572.2,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(89).to({_off:false},0).to({regX:3.2,scaleX:0.89,scaleY:0.89},7).to({regX:3.1,scaleX:0.81,scaleY:0.81},8).to({regX:3.2,scaleX:0.89,scaleY:0.89},8).to({regX:3.1,scaleX:0.81,scaleY:0.81},9).wait(210).to({alpha:0},7).wait(1));

	// 1BALL
	this.instance_5 = new lib.Symbol2copy12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(347.9,328.5,0.812,0.812,0,0,0,3.1,-0.9);
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween5("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(344.6,329.1,1,1,0,0,0,-148.1,-122);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(89).to({_off:false},0).to({scaleX:0.89,scaleY:0.89,x:347.8},7).to({scaleX:0.81,scaleY:0.81,x:347.9},8).to({scaleX:0.89,scaleY:0.89,x:347.8},8).to({scaleX:0.81,scaleY:0.81,x:347.9},9).wait(8).to({_off:true},62).wait(148));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(191).to({_off:false},0).to({scaleX:1.32,scaleY:1.32,x:344.5,y:329},18).to({startPosition:0},122).to({alpha:0},7).wait(1));

	// Layer_1
	this.instance_7 = new lib.Tween19("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(777.2,127.7);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(61).to({_off:false},0).to({_off:true},68).wait(210));

	// Layer_7
	this.instance_8 = new lib.Tween40("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,451.1);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween41("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640,451.1);

	this.instance_10 = new lib.Tween4("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,451.1);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},42).to({state:[{t:this.instance_9}]},7).to({state:[{t:this.instance_10}]},142).to({state:[{t:this.instance_10}]},29).to({state:[{t:this.instance_10}]},111).to({state:[{t:this.instance_10}]},7).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(42).to({_off:false},0).to({_off:true,alpha:1},7).wait(290));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(191).to({_off:false},0).to({startPosition:0},29).to({startPosition:0},111).to({alpha:0},7).wait(1));

	// Layer_5
	this.instance_11 = new lib.Tween2("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,136.1);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(20).to({_off:false},0).to({alpha:1},7).to({startPosition:0},164).to({startPosition:0},140).to({alpha:0},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(593.8,320.8,1370,778);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;