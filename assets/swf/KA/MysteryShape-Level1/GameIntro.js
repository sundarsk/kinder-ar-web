(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgagZIhLACIABgoIBLgCIAChgIAqgCIgBBhIBUgDIgDApIhSACIgBC9IgtABg");
	this.shape.setTransform(133.8,0.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhiA2IgCg2IgBgqIgCgfIgBgvIAygBIABAcIAAAZIAAAaIAPgWQAJgMAMgKQAMgLAMgIQAOgIAPgCQARgBAPAJQAGADAGAGQAGAGAFAJQAFAJAEAMQADANABARIgrAQQAAgLgBgIIgEgNIgGgJIgHgHQgHgFgKAAQgGABgHAEIgNALIgOAPIgPASIgMARIgLAPIABAgIABAeIAAAYIABASIguAGIgDhEg");
	this.shape_1.setTransform(112.2,4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("ABEB8IADgeQgbAQgZAIQgZAIgWAAQgQAAgPgEQgOgEgMgJQgLgIgHgNQgHgNAAgRQAAgTAGgOQAFgNAJgLQAJgJANgJQAMgHAOgFQAPgFAPgCQAPgCAPAAIAbABIAWADQgCgLgFgKQgEgMgGgHQgHgJgJgGQgJgEgNAAQgJAAgKACQgLADgNAGQgNAHgPALQgQAMgSARIgagfQAVgVATgMQATgMARgHQARgHAPgDQANgCAMAAQAUAAAPAGQAQAHAMAMQAMAMAIAQQAIAQAGASQAFATACAUQACASAAAUQAAAVgCAYQgCAYgFAdgAgKgBQgSAEgNAIQgOAJgGANQgHAMAEARQADANAJAFQAJAGAMAAQANAAAPgFIAcgKIAbgNIAWgNIABgVIgCgXQgKgCgMgCQgMgCgMAAQgTAAgSAEg");
	this.shape_2.setTransform(84.1,4.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ah3iwIAwgCIgBAgQANgMANgHQAOgHALgDQANgEAMgBQAPAAAPAEQAOAEANAIQANAIALALQAMALAHANQAIANAFAQQAEAPgBARQAAATgGARQgFAQgIANQgIAOgLALQgLALgMAIQgMAIgNAEQgOAEgNAAQgMgBgOgDQgMgDgOgHQgPgGgOgMIgBCUIgoAAgAAAiIQgNAAgMAFQgMAFgJAHQgKAIgGAKQgHALgDALIgBApQAEAPAHAKQAGALAKAHQAJAHAMAEQALAEALAAQAOAAAOgGQANgFAKgKQAKgKAGgOQAGgOABgQQABgQgFgPQgFgPgJgLQgKgLgOgHQgMgGgPAAIgCAAg");
	this.shape_3.setTransform(57.1,10.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AgvC2QgKgDgKgEQgLgFgJgHQgKgHgIgKQgIgKgFgNIAqgTQAEAKAGAGQAGAHAHAFQAGAEAHADQAHADAHABQAQAEARgCQAPgDALgHQAMgGAHgIQAIgIAFgIQAFgJADgIQADgHABgHIABgKIAAgcQgNAMgOAHQgPAGgMADQgNAEgNABQgZAAgWgJQgWgJgQgQQgQgRgJgXQgKgXAAgeQAAgdALgXQAKgYARgQQAQgQAVgIQAVgIAWAAQANABAPAFQANAEAPAIQAPAIANANIAAgsIArACIgBD5QgBAOgDAPQgEAOgGANQgHANgKAMQgLALgNAJQgNAJgQAGQgRAGgTABIgGABQgXAAgUgGgAggiEQgOAGgJAMQgKALgFAQQgFAPAAASQAAASAFAPQAGAPAJAKQAKAKAOAHQANAGAQAAQANAAAOgFQANgFAMgJQAKgIAIgNQAIgNADgPIAAgcQgDgQgIgMQgIgNgLgJQgLgKgOgFQgOgFgNAAQgQAAgNAHg");
	this.shape_4.setTransform(14.8,9.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AhlA2IgCg2IgBgqIgCgfIgBgvIAygBIABA/QAHgMAJgLQAJgLALgJQALgIALgFQAMgGANgBQARgBAMAEQAMAEAJAHQAJAHAFAKQAGAJADAKQAEAKABAKIACATQABAkAAAmIgCBPIgtgCIAEhGQABgjgCgjIAAgKIgDgNIgFgOQgDgHgFgFQgGgFgHgDQgIgCgKABQgRADgRAXQgRAWgVArIACA4IAAAfIABARIgvAGIgChEg");
	this.shape_5.setTransform(-13,4.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AgbCkIABg4IABhEIAChdIAsgCIgBA5IgBAvIgBAmIAAAdIgBAwgAgLhnQgGgDgEgEQgEgEgCgGQgDgFAAgHQAAgGADgGQACgGAEgEQAEgEAGgDQAGgCAFAAQAGAAAGACQAGADAEAEIAHAKQACAGAAAGQAAAHgCAFQgDAGgEAEQgEAEgGADQgGACgGAAQgFAAgGgCg");
	this.shape_6.setTransform(-32,-0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AggB7IgSgDIgUgHIgTgKQgLgFgIgIIAUggIAZALQANAGANAFQANAEAOADQAMACAPAAQAMAAAIgCQAIgCAGgDQAFgEADgEIAEgIQACgEgBgDQAAgFgCgEQgCgFgFgEQgEgFgGgDQgIgDgKgDQgKgBgOgBQgSgBgSgDQgTgDgNgHQgPgHgJgJQgKgMgBgQQgCgQAEgNQADgNAHgKQAIgKAKgIQALgIANgFQANgGAOgBQAOgEANAAIAUABIAYAEQANACANAGQAMAEALAIIgOAoQgPgHgMgFIgYgGIgWgEQghgCgTAKQgTAJAAATQAAANAHAFQAHAGAMADQANADARAAIAiAEQAWADAOAHQAPAGAIAJQAJAIAFALQADAKAAALQAAAWgIANQgIAOgOAKQgOAIgTAEQgSAFgTABQgUgBgVgEg");
	this.shape_7.setTransform(-50.4,5.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AggB7IgSgDIgUgHIgTgKQgLgFgIgIIAUggIAZALQANAGANAFQANAEAOADQAMACAPAAQAMAAAIgCQAIgCAGgDQAFgEADgEIAEgIQACgEgBgDQAAgFgCgEQgCgFgFgEQgEgFgGgDQgIgDgKgDQgKgBgOgBQgSgBgSgDQgTgDgNgHQgPgHgJgJQgKgMgBgQQgCgQAEgNQADgNAHgKQAIgKAKgIQALgIANgFQANgGAOgBQAOgEANAAIAUABIAYAEQANACANAGQAMAEALAIIgOAoQgPgHgMgFIgYgGIgWgEQghgCgTAKQgTAJAAATQAAANAHAFQAHAGAMADQANADARAAIAiAEQAWADAOAHQAPAGAIAJQAJAIAFALQADAKAAALQAAAWgIANQgIAOgOAKQgOAIgTAEQgSAFgTABQgUgBgVgEg");
	this.shape_8.setTransform(-75.2,5.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgbCkIABg4IAChEIABhdIAsgCIgBA5IgBAvIgBAmIgBAdIAAAwgAgLhnQgGgDgEgEQgEgEgDgGQgCgFAAgHQAAgGACgGQADgGAEgEQAEgEAGgDQAGgCAFAAQAGAAAGACQAGADAEAEIAHAKQACAGAAAGQAAAHgCAFQgDAGgEAEQgEAEgGADQgGACgGAAQgFAAgGgCg");
	this.shape_9.setTransform(-92.7,-0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("ABoA0IAAgvIAAgjQgBgRgFgJQgGgJgHAAQgGAAgFADIgMAJIgLAMIgKAOIgJANIgIAMIABAUIABAaIAAAhIAAApIgoACIgBhEIgCgvIgDgjQAAgRgFgJQgFgJgJAAQgFAAgGAEIgMAJIgMAOIgKAPIgKAPIgIALIABBzIgrACIgFjqIAtgFIABBAIAOgSQAIgJAJgIQAJgIALgEQALgFAMAAQAKAAAIADQAIACAHAHQAGAGAFAJQAFAKACANIANgRQAIgJAJgIQAJgHAKgFQALgEAMAAQAKAAAJADQAJADAHAHQAHAHAEALQAFAKABAPIACAoIACA0IAABNIgtACIgBhEg");
	this.shape_10.setTransform(-116.6,4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(3,1,1).p("A4QloMAwhAAAQCgAAAACgIAAGRQAACgigAAMgwhAAAQigAAAAigIAAmRQAAigCgAAg");
	this.shape_11.setTransform(1,1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("A4QFpQigAAAAigIAAmRQAAigCgAAMAwgAAAQChAAAACgIAAGRQAACgihAAg");
	this.shape_12.setTransform(1,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-172.2,-37.1,346,75.7);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AwmKVIlvkaIQxgbIFjvvIAAgFIFlP0IQyAbIlvEag");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-142.9,-66,286,132.1);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AtYIVIkojjINhgWIEfssIAAgEIEfMwINiAWIkoDjg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-115.3,-53.2,230.7,106.6);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AsdHwIkUjUIMmgUIELr0IAAgDIELL3IMnAUIkUDUg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-107.3,-49.6,214.8,99.2);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AtYIVIkojjINhgWIEfssIAAgEIEfMwINiAWIkoDjg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-115.3,-53.2,230.7,106.6);


(lib.Symbol4copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AABA1IgBAAIpEGOIDJqiIkpjjIVJAAIkpDjIABABIDHKhg");
	this.shape.setTransform(67.7,45.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy3, new cjs.Rectangle(0,0,135.5,90.3), null);


(lib.Symbol4copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AABA1IgBAAIpEGOIDJqiIkpjjIVJAAIkpDjIABABIDHKhg");
	this.shape.setTransform(67.7,45.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy2, new cjs.Rectangle(0,0,135.5,90.3), null);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AABA1IgBAAIpEGOIDJqiIkpjjIVJAAIkpDjIABABIDHKhg");
	this.shape.setTransform(67.7,45.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy, new cjs.Rectangle(0,0,135.5,90.3), null);


(lib.Shapesqustcopy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape.setTransform(2.5,63.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(4,1,1).p("APEAEIAAPAI+HAAIAAvAIAAvHIeHAAgAvDAEIeHAA");
	this.shape_1.setTransform(-1,13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_2.setTransform(-1,61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 6
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(4,1,1).p("AvDAEIAAvHIeHAAIAAPHIAAPAI+HAAIAAvAIFdAAITPAAIFbAA");
	this.shape_3.setTransform(-1,13.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#003366").s().p("AABHDIgBgBIoPFqIC2plIkOjOITPAAIkODOIAAABIC1JkgApngHIjui3IKBgRIDUpaIAAgCIDVJcIKBARIjuC3gApngHg");
	this.shape_4.setTransform(-0.9,14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 6
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#003366").s().p("AABEsIgBAAIlfDyIB5maIlTkCIGsgMICNmRIAAgBICOGSIGrAMIlTECIABABIB5GZg");
	this.shape_5.setTransform(-0.9,14.3,1.5,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 7
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,13.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_7.setTransform(-1,13.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy8, new cjs.Rectangle(-99.4,-85.3,196.8,196.8), null);


(lib.Shapesqustcopy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape.setTransform(2.5,63.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(4,1,1).p("APEAEIAAPAI+HAAIAAvAIAAvHIeHAAgAvDAEIeHAA");
	this.shape_1.setTransform(-1,13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_2.setTransform(-1,61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 6
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(4,1,1).p("AvDAEIAAvHIeHAAIAAPHIAAPAI+HAAIAAvAIFdAAITPAAIFbAA");
	this.shape_3.setTransform(-1,13.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#003366").s().p("AABHDIgBgBIoPFqIC2plIkOjOITPAAIkODOIAAABIC1JkgApngHIjui3IKBgRIDUpaIAAgCIDVJcIKBARIjuC3gApngHg");
	this.shape_4.setTransform(-0.9,14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 6
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#003366").s().p("AABEsIgBAAIlfDyIB5maIlTkCIGsgMICNmRIAAgBICOGSIGrAMIlTECIABABIB5GZg");
	this.shape_5.setTransform(-0.9,14.3,1.5,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 7
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,13.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_7.setTransform(-1,13.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy7, new cjs.Rectangle(-99.4,-85.3,196.8,196.8), null);


(lib.Shapesqustcopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("APEAEIAAPAI+HAAIAAvAIAAvHIeHAAg");
	this.shape.setTransform(-1,13.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_1.setTransform(-1,61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(4,1,1).p("AvDAEIAAvHIeHAAIAAPHAvDAEIFdAAITPAAIFbAAIAAPAI+HAAg");
	this.shape_2.setTransform(-1,13.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#003366").s().p("AABHDIgBgBIoPFqIC2plIkOjOITPAAIkODOIAAABIC1JkgApngHIjui3IKBgRIDUpaIAAgCIDVJcIKBARIjuC3gApngHg");
	this.shape_3.setTransform(-0.9,14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 6
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#003366").s().p("AABEsIgBAAIlfDyIB5maIlTkCIGsgMICNmRIAAgBICOGSIGrAMIlTECIABABIB5GZg");
	this.shape_4.setTransform(-0.9,14.3,1.5,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_5.setTransform(-1,13.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,13.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// Layer 5
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.027)").s().p("AyLSMMAAAgkXMAkXAAAMAAAAkXg");
	this.shape_7.setTransform(0,14.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy4, new cjs.Rectangle(-116.4,-102.2,232.8,232.7), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Shapesqustcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape.setTransform(2.5,63.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(4,1,1).p("APEAEIAAPAI+HAAIAAvAIAAvHIeHAAgAvDAEIeHAA");
	this.shape_1.setTransform(-1,13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_2.setTransform(-1,61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 6
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(4,1,1).p("AvDAEIAAvHIeHAAIAAPHIAAPAI+HAAIAAvAIFdAAITPAAIFbAA");
	this.shape_3.setTransform(-1,13.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#003366").s().p("AABHDIgBgBIoPFqIC2plIkOjOITPAAIkODOIAAABIC1JkgApngHIjui3IKBgRIDUpaIAAgCIDVJcIKBARIjuC3gApngHg");
	this.shape_4.setTransform(-0.9,14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 6
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#003366").s().p("AABEsIgBAAIlfDyIB5maIlTkCIGsgMICNmRIAAgBICOGSIGrAMIlTECIABABIB5GZg");
	this.shape_5.setTransform(-0.9,14.3,1.5,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 7
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,13.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_7.setTransform(-1,13.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy3, new cjs.Rectangle(-99.4,-85.3,196.8,196.8), null);


(lib.Shapesqustcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape.setTransform(2.5,63.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(4,1,1).p("APEAEIAAPAI+HAAIAAvAIAAvHIeHAAgAvDAEIeHAA");
	this.shape_1.setTransform(-1,13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_2.setTransform(-1,61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 6
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(4,1,1).p("AvDAEIAAvHIeHAAIAAPHAvDAEIFdAAITPAAIFbAAIAAPAI+HAAg");
	this.shape_3.setTransform(-1,13.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#003366").s().p("AABHDIgBgBIoPFqIC2plIkOjOITPAAIkODOIAAABIC1JkgApngHIjui3IKBgRIDUpaIAAgCIDVJcIKBARIjuC3gApngHg");
	this.shape_4.setTransform(-0.9,14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 6
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#003366").s().p("AABEsIgBAAIlfDyIB5maIlTkCIGsgMICNmRIAAgBICOGSIGrAMIlTECIABABIB5GZg");
	this.shape_5.setTransform(-0.9,14.3,1.5,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 7
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,13.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_7.setTransform(-1,13.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy2, new cjs.Rectangle(-99.4,-85.3,196.8,196.8), null);


(lib.questiontext_mccopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgMgMIglABIAAgUIAlgBIABgvIAVgBIgBAwIApgBIgBATIgoABIgBBeIgWAAg");
	this.shape.setTransform(113.9,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgvAbIgBgbIgBgUIAAgPIgCgYIAZAAIAAANIAAANIAAANIAIgLQAEgGAGgFQAGgFAGgEQAGgEAIgBQAIgBAIAFIAGAEIAFAHIAFALIACAPIgVAIIgCgJIgCgHIgCgFIgDgDQgEgDgFABIgGACIgHAGIgGAHIgHAIIgGAJIgGAHIAAAPIABAPIAAANIABAIIgXADIgBghg");
	this.shape_1.setTransform(103.2,2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AAhA9IACgPQgNAIgNAEQgLAEgLAAQgIAAgIgCQgHgCgFgEQgGgEgDgHQgEgGAAgJQAAgJADgHQADgGAEgGQAEgEAHgEIANgGQAHgCAIgBIAOgBIANAAIALABIgDgKIgGgJQgDgFgFgCQgEgDgGAAIgJACQgFABgHADIgOAJQgIAFgIAJIgNgQQAKgJAKgGQAJgGAIgEQAJgDAHgCIALgBQALAAAHADQAIAEAGAGQAGAFAEAIIAGARQADAJABAKIABATIgBAWIgEAagAgEAAQgKABgGAFQgGAEgEAGQgDAGACAIQABAHAEACQAFADAGAAQAHAAAGgCIAOgFIANgHIALgGIAAgLIAAgLIgLgBIgLgBQgLAAgHACg");
	this.shape_2.setTransform(89.5,2.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ag6hWIAXgBIAAAPQAGgFAHgEIAMgFIAMgDQAHABAHACQAHACAHAEQAGADAGAGQAFAGAEAGQAEAGACAIQACAIAAAIQAAAJgDAJQgCAHgEAGQgEAIgGAFQgFAGgGADQgGAEgHACQgGACgHAAIgMgCIgNgFQgHgDgHgGIgBBJIgUAAgAgMhAQgGADgEADQgFAEgDAFQgEAFgBAGIgBAUQACAHADAFQAEAGAFACQAEAEAGACIALACQAGAAAHgDQAHgDAEgEQAFgFADgHQADgHABgIQAAgIgCgHQgDgHgEgGQgFgFgHgEQgHgCgHAAQgGAAgGACg");
	this.shape_3.setTransform(76.1,5.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AgXBaIgKgEIgKgGIgJgIQgDgFgDgHIAVgJQACAFADADQACAEAEACIAHAEIAGACQAIABAIgBQAHgBAHgDQAFgDAEgEIAGgIIAEgIIABgHIABgFIAAgOQgGAGgHADIgNAFQgHACgFAAQgNAAgLgEQgKgFgJgIQgHgIgFgLQgEgLgBgPQAAgOAGgMQAFgLAHgIQAJgIAKgEQAKgEALAAIAOADIAOAGQAHAEAGAGIAAgVIAWAAIgBB7IgCAOIgFANIgIAMQgFAGgHAEQgGAFgJADQgIADgKAAIgCAAQgLAAgKgCgAgQhAQgGADgEAFQgFAGgDAHQgDAIABAJQgBAJADAHQADAIAFAEQAEAFAHADQAGADAJAAQAFAAAHgCQAHgDAFgEQAGgEAEgGQADgGABgHIAAgPQgBgHgDgGQgEgHgGgEQgFgFgIgCQgGgDgGAAQgIAAgHAEg");
	this.shape_4.setTransform(55.3,5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AgxAbIgBgbIgBgUIAAgPIgBgXIAYgBIABAfIAIgLQAEgGAGgEQAFgEAFgDQAGgCAGgBQAJAAAGACQAGACAEADIAHAIIAFAKIACAKIABAJIAAAkIgBAnIgWgBIACgjQABgRgBgQIAAgFIgCgHIgCgHIgEgGIgHgEQgDgBgFABQgJABgIALQgIALgKAVIAAAbIABAQIAAAIIgXADIgBghg");
	this.shape_5.setTransform(41.6,2.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AgNBRIABgcIABgiIAAgtIAVgBIAAAcIgBAXIAAATIAAAOIgBAYgAgFgyIgFgEIgDgFQgBgCAAgDQAAgEABgCIADgFIAFgEIAFgBIAGABIAFAEIADAFIABAGIgBAFIgDAFIgFAEIgGABIgFgBg");
	this.shape_6.setTransform(32.2,0.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgPA9IgJgCIgJgEIgKgEIgKgHIALgQIALAGIANAFIANAEIAOABIAKgBIAGgDIAEgEIACgEIAAgEIgBgEIgCgEIgGgEIgJgDIgLgBIgSgCQgJgCgHgDQgHgDgEgFQgFgFgBgIQAAgIABgGQACgHAEgFQADgFAGgEQAEgDAHgDIANgEIANgBIAKAAIAMACIANAEQAFACAGAEIgIAUIgMgGIgMgDIgLgCQgQgBgJAFQgKAEABAJQAAAHADADQAEADAGABIAOACIARABQAKACAHADQAHADAFAEQAEAEACAFQACAGAAAFQAAAKgEAHQgFAHgGAFQgHAEgJACQgJACgKABQgJAAgKgCg");
	this.shape_7.setTransform(23.2,3.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgPA9IgJgCIgKgEIgKgEIgJgHIAKgQIANAGIAMAFIAOAEIAMABIAKgBIAHgDIAEgEIACgEIABgEIgBgEIgEgEIgFgEIgIgDIgMgBIgSgCQgJgCgHgDQgHgDgFgFQgEgFgBgIQgBgIACgGQACgHADgFQAEgFAFgEQAGgDAGgDIANgEIANgBIAKAAIAMACIAMAEQAHACAFAEIgHAUIgOgGIgLgDIgLgCQgQgBgJAFQgKAEAAAJQABAHADADQADADAGABIAPACIAQABQALACAHADQAHADAEAEQAFAEACAFQACAGAAAFQAAAKgEAHQgFAHgGAFQgHAEgJACQgJACgKABQgJAAgKgCg");
	this.shape_8.setTransform(11,3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgNBRIABgcIABgiIAAgtIAVgBIAAAcIgBAXIAAATIAAAOIgBAYgAgFgyIgFgEIgDgFQgBgCAAgDQAAgEABgCIADgFIAFgEIAFgBIAGABIAFAEIADAFIABAGIgBAFIgDAFIgFAEIgGABIgFgBg");
	this.shape_9.setTransform(2.3,0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AA0AaIgBgXIAAgRQAAgJgDgEQgCgEgEgBIgGACIgFAFIgGAFIgFAHIgEAHIgEAGIABAJIAAAMIAAARIAAAUIgUABIAAghIgBgXIgBgRQAAgJgDgEQgCgEgEgBQgDAAgDACIgGAFIgGAHIgFAHIgFAHIgEAGIABA4IgVABIgDhzIAWgCIABAgIAHgJIAIgJIAKgGQAFgDAHABQAEgBAEACQAEACADACQADADADAFQACAFABAHIAHgJIAIgIQAEgEAFgCQAFgDAGABQAFgBAFACIAIAFQADAEACAFQADAFAAAIIABATIABAaIAAAlIgWABIAAghg");
	this.shape_10.setTransform(-9.5,2.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgXA6QgLgFgIgIQgHgHgEgMQgEgLAAgNQAAgMAFgMQAEgMAIgJQAIgIALgFQAKgFAMAAQALAAAKAFQAKAEAHAHQAIAIAFAKQAFALABAMIhcANQABAHADAHQADAGAFAEQAEAEAHADQAGACAGAAQAGgBAFgBQAGgCAEgEQAFgDAEgFQADgFACgGIAVADQgDALgGAHQgFAJgIAFQgHAGgJADQgJADgJAAQgNAAgLgEgAgIgoQgFABgFADQgFAEgEAGQgFAHgBAJIBAgIIgBgBQgEgMgHgGQgHgFgLgBQgEAAgFADg");
	this.shape_11.setTransform(-30.5,2.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AAdBRQADgJABgLIACgRIAAgRQAAgLgDgJQgEgHgFgFQgFgFgGgCQgGgCgGAAQgFAAgGADQgGADgFAFQgHAFgFAKIgBBGIgVABIgBilIAYgBIAABCQAGgHAGgEIAMgFIANgDQALAAAKAFQALADAGAIQAIAJAEALQAEALABAOIAAAQIgBAQIgCAOIgDANg");
	this.shape_12.setTransform(-44.2,0.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgMgMIglABIAAgUIAlgBIABgvIAVgBIgBAwIApgBIgBATIgoABIgBBeIgWAAg");
	this.shape_13.setTransform(-56.8,0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgMgMIglABIAAgUIAlgBIABgvIAVgBIgBAwIApgBIgBATIgoABIgBBeIgWAAg");
	this.shape_14.setTransform(-72.9,0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AgTA6QgLgFgJgIQgJgJgFgLQgGgLABgOQAAgHACgIQACgIAEgGQAEgIAGgFQAGgGAGgFQAIgEAIgCQAHgCAJgBQAJABAJACQAIACAHAEQAHAFAGAFQAGAHAEAIIAAAAIgSALIAAgBQgDgGgEgDQgEgFgFgDIgLgFIgLgCQgIAAgIAEQgIAEgFAFQgGAGgEAHQgDAJAAAHQAAAJADAIQAEAIAGAFQAFAGAIAEQAIADAIAAQAGgBAFgBIAKgEIAIgGIAIgJIAAgBIATALIAAAAQgFAHgGAFQgGAHgHADQgIAFgHACQgJABgIAAQgMAAgLgEg");
	this.shape_15.setTransform(-84.8,2.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgXA6QgLgFgIgIQgHgHgEgMQgEgLAAgNQAAgMAFgMQAEgMAIgJQAIgIALgFQAKgFAMAAQALAAAKAFQAKAEAHAHQAIAIAFAKQAFALABAMIhcANQABAHADAHQADAGAFAEQAEAEAHADQAGACAGAAQAGgBAFgBQAGgCAEgEQAFgDAEgFQADgFACgGIAVADQgDALgGAHQgFAJgIAFQgHAGgJADQgJADgJAAQgNAAgLgEgAgIgoQgFABgFADQgFAEgEAGQgFAHgBAJIBAgIIgBgBQgEgMgHgGQgHgFgLgBQgEAAgFADg");
	this.shape_16.setTransform(-98,2.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AAGBdIgJgGIgIgJIgEgKQgGgMgBgQIADiHIAWAAIgBAjIgBAdIgBAXIAAASIgBAdQABAKACAHIACAHIAFAGIAGAEQADACAFAAIgDAVQgHAAgHgDg");
	this.shape_17.setTransform(-107,-1.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgXA6QgLgFgIgIQgHgHgEgMQgEgLAAgNQAAgMAFgMQAEgMAIgJQAIgIALgFQAKgFAMAAQALAAAKAFQAKAEAHAHQAIAIAFAKQAFALABAMIhcANQABAHADAHQADAGAFAEQAEAEAHADQAGACAGAAQAGgBAFgBQAGgCAEgEQAFgDAEgFQADgFACgGIAVADQgDALgGAHQgFAJgIAFQgHAGgJADQgJADgJAAQgNAAgLgEgAgIgoQgFABgFADQgFAEgEAGQgFAHgBAJIBAgIIgBgBQgEgMgHgGQgHgFgLgBQgEAAgFADg");
	this.shape_18.setTransform(-117.2,2.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgOBWIgPgEQgIgEgIgEIgPgKIAQgSQAIAHAJAEIAOAHQAIADAGAAQAIABAGgCQAHgBAFgEQAFgDADgEQADgEAAgFQABgEgCgDIgFgGIgIgFIgJgEIgLgCIgJgDIgNgDIgOgEQgGgDgGgFQgGgDgEgGQgEgGgCgIQgDgIABgLQABgIADgHQADgHAEgGQAFgFAGgEIANgHQAHgCAHgBIAOgBQAKAAAKADIAJADIAKAEQAFACAEAEIAJAGIgMATIgHgGIgIgGIgHgDIgIgDIgQgCQgJAAgHADQgIADgFAEQgFAGgDAFQgDAGAAAGQAAAFADAFQAEAFAFAEQAGAFAIADQAIACAIABIAQADIAOAEQAIAEAGAEIAKAKQAEAFACAHQABAHgBAIQgBAHgDAHQgDAFgFAFQgFAEgFADQgGADgHACIgNACIgMABQgIAAgIgCg");
	this.shape_19.setTransform(-130.9,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#CC6600").ss(3,1,1).p("A1bjEMAq3AAAQA8AAAqAqQAqAqAAA8IAABpQAAA8gqAqQgqAqg8AAMgq3AAAQg7AAgrgqQgqgqAAg8IAAhpQAAg8AqgqQArgqA7AAg");
	this.shape_20.setTransform(-5.9,1.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFCC00").s().p("A1bDFQg7AAgrgqQgqgqAAg9IAAhoQAAg7AqgrQArgqA7AAMAq3AAAQA8AAAqAqQAqArAAA7IAABoQAAA9gqAqQgqAqg8AAg");
	this.shape_21.setTransform(-5.9,1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy3, new cjs.Rectangle(-159,-19.3,306.2,42.4), null);


(lib.ch1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#003366").s().p("AggG/IgagCQiggJiMg8IghgRQhug1hghXIglgkQhXhVg4hhQgRgdgQgfQgjhHgThKQgbhbgFhiIgCgqIAAgLIcFAAQgBEpiqDiQgXAggcAeIgpAsQgVAVgYAVQgeAbgfAYQgdAWggAVQjQCAkFAAIggAAg");
	this.shape.setTransform(-0.9,44.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape_1.setTransform(-1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_2.setTransform(-1,-0.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_3.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_4.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.Tween27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Shapesqustcopy3();
	this.instance.parent = this;
	this.instance.setTransform(0,-19.1,1.35,1.35);

	this.instance_1 = new lib.Shapesqustcopy8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,-19.1,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-134.2,-134.2,265.7,265.6);


(lib.Tween26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Shapesqustcopy2();
	this.instance.parent = this;
	this.instance.setTransform(0,-19.1,1.35,1.35);

	this.instance_1 = new lib.Shapesqustcopy8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,-19.1,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-134.2,-134.2,265.7,265.6);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween24("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-172.2,-37.1,346,75.7);


(lib.Symbol6copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Shapesqustcopy4();
	this.instance.parent = this;
	this.instance.setTransform(157.2,138,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6copy2, new cjs.Rectangle(0,0,314.3,314.1), null);


(lib.Symbol6copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Shapesqustcopy4();
	this.instance.parent = this;
	this.instance.setTransform(157.2,138,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6copy, new cjs.Rectangle(0,0,314.3,314.1), null);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.questiontext_mccopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.questxt = new lib.questiontext_mccopy3();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(23,-7.8,1.299,1.3,0,0,0,0.1,-10.1);

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy2, new cjs.Rectangle(-183.8,-19.7,401,58), null);


(lib.ch1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol4copy();
	this.instance.parent = this;
	this.instance.setTransform(-0.9,45.1,1,1,0,0,0,67.7,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape.setTransform(-1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_1.setTransform(-1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_2.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_3.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy2, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1copy2();
	this.instance.parent = this;
	this.instance.setTransform(360.6,1.6,1.2,1.2,0,0,0,-0.9,-0.4);

	this.instance_1 = new lib.ch1copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-359.9,-0.9,1.2,1.2,0,0,0,-0.9,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-518.5,-155,1037,310);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(635.8,338.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(289).to({_off:false},0).wait(62).to({startPosition:15},0).to({alpha:0.02,startPosition:19},6).wait(1));

	// hand
	this.instance_1 = new lib.handanim("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(960.3,645.6,1,1,0,0,0,41,60.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(234).to({_off:false},0).to({_off:true},29).wait(95));

	// arrow
	this.instance_2 = new lib.arrowanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(1001.3,344.5,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(205).to({_off:false},0).to({_off:true},29).wait(124));

	// Layer_6
	this.instance_3 = new lib.Symbol4copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(993.3,589.3,1,1,0,0,0,67.7,45.1);
	this.instance_3._off = true;

	this.instance_4 = new lib.Symbol4copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(637.2,387.2,1.493,1.493,0,0,0,67.7,45.1);

	this.instance_5 = new lib.Symbol4copy3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(637.2,387.2,1.493,1.493,0,0,0,67.7,45.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},263).to({state:[{t:this.instance_4}]},21).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_5}]},18).to({state:[{t:this.instance_5}]},47).to({state:[{t:this.instance_5}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(263).to({_off:false},0).to({_off:true,scaleX:1.49,scaleY:1.49,x:637.2,y:387.2},21).wait(74));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(286).to({_off:false},0).to({scaleX:1.83,scaleY:1.83,x:636.6,y:398.8},18).wait(47).to({alpha:0.02},6).wait(1));

	// Layer_5
	this.instance_6 = new lib.Tween15("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(637.9,278.8);
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween16("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(1000.8,495.8,0.99,0.99);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(130).to({_off:false},0).to({_off:true,scaleX:0.99,scaleY:0.99,x:1000.8,y:495.8},21).wait(207));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(130).to({_off:false},21).wait(46).to({scaleX:1,scaleY:1,y:493.8},0).to({alpha:0.02},5).to({_off:true},1).wait(155));

	// valid
	this.instance_8 = new lib.Tween17("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(637.9,278.8);
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween18("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(279.9,483.7,0.9,0.9);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(130).to({_off:false},0).to({_off:true,scaleX:0.9,scaleY:0.9,x:279.9,y:483.7},21).wait(207));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(130).to({_off:false},21).wait(46).to({scaleX:1,scaleY:1,y:475.7},0).to({alpha:0.02},5).to({_off:true},1).wait(155));

	// Layer_1
	this.instance_10 = new lib.Tween22("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(808.1,132.2);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(89).to({_off:false},0).to({_off:true},114).wait(155));

	// Layer_4
	this.instance_11 = new lib.Tween12("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,540.6);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(69).to({_off:false},0).to({alpha:1},7).to({startPosition:0},208).to({alpha:0},4).to({_off:true},1).wait(69));

	// Layer_3
	this.instance_12 = new lib.Shapesqustcopy7();
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,315.5,1.35,1.35);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_13 = new lib.Tween26("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,334.6);
	this.instance_13._off = true;

	this.instance_14 = new lib.Tween27("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,334.6);

	this.instance_15 = new lib.Symbol6copy();
	this.instance_15.parent = this;
	this.instance_15.setTransform(640.1,334.6,1,1,0,0,0,157.2,157.1);

	this.instance_16 = new lib.Symbol6copy2();
	this.instance_16.parent = this;
	this.instance_16.setTransform(640.1,334.6,1,1,0,0,0,157.2,157.1);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},43).to({state:[{t:this.instance_12}]},6).to({state:[{t:this.instance_13}]},40).to({state:[{t:this.instance_13}]},14).to({state:[{t:this.instance_13}]},8).to({state:[{t:this.instance_13}]},10).to({state:[{t:this.instance_13}]},9).to({state:[{t:this.instance_13}]},9).to({state:[{t:this.instance_13}]},10).to({state:[{t:this.instance_13}]},9).to({state:[{t:this.instance_13}]},10).to({state:[{t:this.instance_13}]},10).to({state:[{t:this.instance_13}]},8).to({state:[{t:this.instance_13}]},9).to({state:[{t:this.instance_14}]},8).to({state:[{t:this.instance_15}]},81).to({state:[{t:this.instance_16}]},2).to({state:[{t:this.instance_16}]},18).to({state:[{t:this.instance_16}]},47).to({state:[{t:this.instance_16}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(43).to({_off:false},0).to({alpha:1},6).to({_off:true},40).wait(269));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(89).to({_off:false},0).to({scaleX:1.06,scaleY:1.06},14).to({scaleX:1,scaleY:1},8).to({scaleX:1.06,scaleY:1.06},10).to({scaleX:1,scaleY:1},9).to({scaleX:1.06,scaleY:1.06},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.06,scaleY:1.06},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.06,scaleY:1.06},10).to({scaleX:1,scaleY:1},8).to({scaleX:1.06,scaleY:1.06},9).to({_off:true,scaleX:1,scaleY:1},8).wait(155));
	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(286).to({_off:false},0).to({regX:157.3,regY:157.2,scaleX:1.22,scaleY:1.22},18).wait(47).to({alpha:0.02},6).wait(1));

	// Layer_2
	this.questxt = new lib.questiontext_mccopy2();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(640.1,102.1,1.735,1.735,0,0,0,0.1,-10.2);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(20).to({_off:false},0).to({alpha:1},6).wait(258).to({alpha:0},4).to({_off:true},1).wait(69));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(586.5,327.4,1389,789);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;