///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 4, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice0, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0, attemptCnt = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc, answerMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;
var currentObj = []
var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var ans
var currentX
var currentY
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var qno = [];
var chpos = [];
var ansArr = [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1]

var ansArr = [1, 2, 2, 3, 1, 2, 2, 3, 0, 2, 3, 3, 2, 3, 1]
//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////

function init() {

    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);


    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = " WhoHits-KG/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(
            { id: "question", src: gameAssetsPath + "question.png" },
            { id: "answer", src: gameAssetsPath + "answer.png" },
            { id: "choice1", src: gameAssetsPath + "ChoiceImages1.png" },
            { id: "choice2", src: gameAssetsPath + "ChoiceImages2.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },

            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}
//=====================================================================//

function doneLoading1(event) {

    var event = assets[i];
    var id = event.item.id;
    console.log(" doneLoading ")
    loaderBar.visible = false;
    stage.update();

    if (id == "questionText") {
        questionText = new createjs.Bitmap(preload.getResult('questionText'));
        container.parent.addChild(questionText);
        questionText.visible = false;
    }

    if (id == "choice1" || id == "choice2") {
        //
        var spriteSheet5a = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 559, "count": 0, "regY": 50, "width": 805 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });

        choice1 = new createjs.Sprite(spriteSheet5a);
        container.parent.addChild(choice1);
        choice1.visible = false;
        //
        var spriteSheet5b = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice2")],
            "frames": { "regX": 50, "height": 559, "count": 0, "regY": 50, "width": 805 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        choice2 = new createjs.Sprite(spriteSheet5b);
        container.parent.addChild(choice2);
        choice2.visible = false;
    }

    if (id == "question") {
        var spriteSheet5 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question")],
            "frames": { "regX": 50, "height": 559, "count": 0, "regY": 50, "width": 805 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        question = new createjs.Sprite(spriteSheet5);
        question.visible = false;
        container.parent.addChild(question);

    };

    if (id == "answer") {
        var spriteSheet5 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("answer")],
            "frames": { "regX": 50, "height": 559, "count": 0, "regY": 50, "width": 805 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        answerMc = new createjs.Sprite(spriteSheet5);
        answerMc.visible = false;
        container.parent.addChild(answerMc);
    };

    //
    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("D044BEAA30B3F146B78DA97BB48504C8");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }

}


function tick(e) {
    stage.update();
}
/////////////////////////////////////////////////////////////////=======GAME START========///////////////////////////////////////////////////////////////////
function handleClick(e) {
    qno = between(0, 14);
    toggleFullScreen();
    CreateGameStart();
    CreateGameElements()
    interval = setInterval(countTime, 1000);

}
function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Match the right arrow to its arrowhead";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 30

   
    
    container.parent.addChild(questionText);
    questionText.visible = true;
    //
    question.visible = true;
    container.parent.addChild(question)
    question.x = 285; question.y = 220;

    answerMc.visible = false;
    container.parent.addChild(answerMc)
    answerMc.x = 285; answerMc.y = 220;

    //
    for (i = 1; i <= 2; i++) {
        this["choice" + i].visible = true;
        this["choice" + i].name = i;
        container.parent.addChild(this["choice" + i])
    }

    choice1.x = 285; choice1.y = 220;
    choice2.x = 285; choice2.y = 220;

  


    pickques();
}

function helpDisable() {
    for (i = 1; i <= 2; i++) {
        this["choice" + i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 1; i <= 2; i++) {
        this["choice" + i].mouseEnabled = true;
    }
}
//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    chpos = [];
    currentObj = []
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    boardMc.boardMc.openMc.mouseEnabled = true;
    
    correctImageMc.visible = false
    wrongImageMc.visible = false
    container.parent.addChild(correctImageMc);
    container.parent.addChild(wrongImageMc);
    //====================================================================================================================//
    question.visible = true;
    answerMc.visible = false;
    //qno[cnt] = 0
    question.gotoAndStop(qno[cnt]);

    for (i = 1; i <= 2; i++) {
        this["choice" + i].gotoAndStop(qno[cnt]);
    }

    ans = "ch1";
    console.log(ans)
    enablechoices();
    rst = 0;
    gameResponseTimerStart();

    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
}
function enablechoices() {
    for (i = 1; i <= 2; i++) {
        this["choice" + i].cursor = "pointer";
        this["choice" + i].addEventListener("click", answerSelected);
        this["choice" + i].visible = true;
        this["choice" + i].alpha = 1;
        this["choice" + i].name = "ch" + i
        this["choice" + i].mouseEnabled = true;
    }
}

function disablechoices() {
    for (i = 1; i <= 2; i++) {
        this["choice" + i].removeEventListener("click", answerSelected);
        this["choice" + i].visible = false;
        this["choice" + i].alpha = .5;
        this["choice" + i].cursor = "default";
    }
    // answerMc.visible = false
    boardMc.boardMc.openMc.mouseEnabled = false;
}

// function onRoll_over(e) {
//     e.currentTarget.alpha = .5;
//     stage.update();
// }

// function onRoll_out(e) {
//     e.currentTarget.alpha = 1;
//     stage.update();
// }

function answerSelected(e) {

    e.preventDefault();
    uans = e.currentTarget.name;
    console.log(ans + " =correct= " + uans)
    gameResponseTimerStop();
    //  pauseTimer();
  
    console.log(ans)
    if (ans == uans) {
        if (ansArr[qno[cnt]] == 1) {
            currentX = 293
            currentY = 274          
        }
        else if (ansArr[qno[cnt]] == 0) {
            currentX = 886
            currentY = 274          
        }
        else if (ansArr[qno[cnt]] == 2) {
            currentX = 293
            currentY = 574             
        }
        else {
            currentX = 893
            currentY = 514            
        }
        starAnimation()
        e.currentTarget.removeEventListener("click", answerSelected)
        for (i = 1; i <= 2; i++) {
            this["choice" + i].mouseEnabled = false;
        }
        setTimeout(correct1, 800)

    } else {
        wrongImageMc.visible = true;
        getValidation("wrong");
        disablechoices();
    }

}
function correct1() {
    question.visible = false;
    answerMc.visible = true;
    answerMc.gotoAndStop(qno[cnt])
    setTimeout(correct, 2000)
}
function correct() {
    getValidation("correct");
    disablechoices();
}

function disableMouse() {

}

function enableMouse() {

}
// ====================================================
