(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgRCiQgEgCgDgDIgFgHQgCgEABgEQgBgFACgEQACgEADgDIAHgFQAEgCAEAAQAFAAAEACIAGAFQADADACAEQACAEAAAFQAAAEgCAEIgFAHQgDADgDACQgEABgFAAQgEAAgEgBgAgeBcQgCgIAAgIQAAgMAEgJQAEgKAGgIQAHgIAIgHIAQgNIARgNQAIgHAGgIQAHgJADgKQAFgKAAgNQgBgNgEgMQgEgMgIgKQgIgJgLgGQgMgGgNAAQgNAAgLAFQgKAFgIAKQgHAIgEAMQgDALAAANIAAAKIABAKIgjAFIgDgNIAAgMQAAgVAGgRQAIgRAMgOQANgNASgIQARgHAVAAQAUAAATAHQARAHANAOQANANAHASQAIASgBAWIgBAUQgBAKgFAKQgEAKgGAKQgIAIgLAJIgTAPQgLAHgJAJQgJAJgFALQgFAMADAPg");
	this.shape.setTransform(297.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AA1CWQAGgTACgSIAEggIAAgfQgBgWgGgOQgHgOgJgKQgJgJgLgEQgLgFgLAAQgKABgMAGQgJAFgMAKQgLAJgKASIgBCDIgnABIgDkwIAugCIgCB4QALgLAMgHQANgHAKgEQANgEALgBQAWAAASAIQASAIANAPQANAPAIAVQAIAUABAbIgBAcIgBAdIgDAbQgCANgDAKg");
	this.shape_1.setTransform(273.1,0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgqClQgKgCgJgEQgJgEgJgHQgJgGgHgJQgHgJgFgNIAmgQQAEAIAFAGQAFAHAHAEQAGAEAGACIANAEQAOADAPgCQAOgCAKgGQAKgGAIgHQAHgHAEgIIAHgPIAEgNIAAgJIAAgaQgLAMgNAGQgNAGgLADQgMADgMAAQgXAAgTgIQgUgIgPgPQgPgPgIgUQgIgVAAgbQAAgbAJgVQAJgVAQgOQAPgPATgHQATgIATAAQANABANAFQAMAEANAGQAOAHALAMIABgnIAmABIgBDiQAAANgDANQgDANgGAMQgHAMgJAKQgJALgMAIQgMAIgPAGQgPAFgRABIgDAAQgWAAgTgFgAgdh3QgMAFgJALQgIAKgFAOQgFAOAAAQQAAAQAFAOQAFAOAJAJQAIAJANAGQAMAFAOAAQAMAAANgEQAMgFAKgIQAKgHAHgLQAHgMACgOIAAgZQgCgPgHgLQgHgMgKgIQgLgIgMgFQgNgEgMAAQgOAAgMAGg");
	this.shape_2.setTransform(245.4,9.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgVBuQgRgBgNgGQgNgGgJgKQgJgKgEgNQgGgOgDgPQgDgPgBgPIgBgdQAAgVACgVQACgVADgWIApABQgDAZgBATIgBAjIAAARIABAVQABALAEALQADAMAEAJQAGAIAHAFQAIAFALgCQAPgCAQgTQAQgTASglIgBgzIgBgcIAAgRIArgEIACA9IABAwIACAnIAAAcIACAqIgtABIgCg5QgGAMgIAKQgIALgKAIQgKAHgKAFQgKAEgLAAIgCAAg");
	this.shape_3.setTransform(220,5.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AgoBsQgUgIgOgPQgOgQgJgVQgIgWAAgaQAAgZAIgVQAJgWAOgPQAOgPAUgJQASgIAWAAQAVAAAUAJQAUAJAOAPQAOAQAIAVQAJAVAAAYQAAAZgJAVQgIAWgOAPQgOAQgUAJQgUAIgVAAQgWAAgSgIgAgdhJQgMAHgHAMQgIAMgDAOQgEAPAAANQAAAOAEAPQAEAOAHAMQAIALAMAIQAMAHAQAAQARAAAMgHQALgIAJgLQAHgMAEgOQAEgPAAgOQAAgNgEgPQgDgOgIgMQgHgMgNgHQgMgHgRAAQgQAAgNAHg");
	this.shape_4.setTransform(195.3,5.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AhZAxIgCgxIgBgmIgBgdIgBgqIAtAAIABAYIAAAXIAAAYIAOgUQAIgLALgJQAKgKALgHQAMgHAOgCQAQgBANAHIAMAJQAFAFAFAIQAEAJAEALQACALACAQIgnAOIgBgQIgFgNIgEgIIgGgGQgIgFgIABQgGAAgGAEIgMAKIgNAOIgMAPIgNAQIgJAOIAAAdIABAbIABAWIABAQIgqAFIgDg9g");
	this.shape_5.setTransform(173.3,5.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AA1CWQAGgTACgSIAEggIAAgfQgBgWgGgOQgHgOgJgKQgJgJgLgEQgLgFgLAAQgKABgMAGQgJAFgMAKQgLAJgKASIgBCDIgnABIgDkwIAugCIgCB4QALgLAMgHQANgHAKgEQANgEALgBQAWAAASAIQASAIANAPQANAPAIAVQAIAUABAbIgBAcIgBAdIgDAbQgCANgDAKg");
	this.shape_6.setTransform(148.1,0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgXgWIhFABIABgkIBFgCIABhXIAmgCIgBBYIBNgCIgDAlIhKACIgCCrIgpABg");
	this.shape_7.setTransform(125,1.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgJQgJgEgIgHIATgeIAWALIAYAKQALADANADQALACAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgDgBgDIgBgIQgCgFgEgDQgEgEgHgDQgGgDgJgCQgJgCgNgBQgQAAgRgDQgQgDgNgGQgNgHgJgJQgIgKgCgPQgBgOADgMQADgLAHgKQAHgJAJgHQAKgHALgFQAMgEANgDQAMgCAMAAIATABIAWADQALACAMAFQALAEALAIIgOAkQgNgHgMgEIgVgGIgUgDQgegCgRAIQgRAJAAARQAAAMAGAFQAHAGALACIAaADIAfADQAUAEANAFQANAFAIAIQAIAIAEAKQADAJAAAKQAAATgHAOQgIAMgMAIQgNAIgRAEQgQAEgSABQgRAAgTgEg");
	this.shape_8.setTransform(93.3,5.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgsBqQgUgIgOgPQgOgOgHgUQgHgVAAgZQAAgYAIgVQAJgVAOgQQAPgQAUgJQATgJAWAAQAVAAASAIQASAIAOAOQAOAOAKATQAJAUACAWIiqAZQABAOAGAKQAFAMAJAIQAIAHAMAFQALADAMAAQALABAKgEQAKgDAJgGQAJgHAGgJQAHgJADgMIAmAHQgGASgKAPQgKAPgNAKQgOAKgQAGQgQAGgSAAQgZAAgUgIgAgQhMQgKADgJAGQgJAIgIALQgHAMgDARIB2gPIgBgDQgIgUgNgLQgNgLgUAAQgHAAgKADg");
	this.shape_9.setTransform(70.4,5.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgJQgJgEgIgHIATgeIAWALIAYAKQALADANADQALACAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgDgBgDIgBgIQgCgFgEgDQgEgEgHgDQgGgDgJgCQgJgCgNgBQgQAAgRgDQgQgDgNgGQgNgHgJgJQgIgKgCgPQgBgOADgMQADgLAHgKQAHgJAJgHQAKgHALgFQAMgEANgDQAMgCAMAAIATABIAWADQALACAMAFQALAEALAIIgOAkQgNgHgMgEIgVgGIgUgDQgegCgRAIQgRAJAAARQAAAMAGAFQAHAGALACIAaADIAfADQAUAEANAFQANAFAIAIQAIAIAEAKQADAJAAAKQAAATgHAOQgIAMgMAIQgNAIgRAEQgQAEgSABQgRAAgTgEg");
	this.shape_10.setTransform(46.9,5.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgcBwIgRgEIgSgGIgSgJQgJgEgIgHIATgeIAWALIAYAKQALADANADQALACAOAAQAKAAAIgCQAHgCAFgDQAFgDADgEQADgEABgEQABgDgBgDIgBgIQgCgFgEgDQgEgEgHgDQgGgDgJgCQgJgCgNgBQgQAAgRgDQgQgDgNgGQgNgHgJgJQgIgKgCgPQgBgOADgMQADgLAHgKQAHgJAJgHQAKgHALgFQAMgEANgDQAMgCAMAAIATABIAWADQALACAMAFQALAEALAIIgOAkQgNgHgMgEIgVgGIgUgDQgegCgRAIQgRAJAAARQAAAMAGAFQAHAGALACIAaADIAfADQAUAEANAFQANAFAIAIQAIAIAEAKQADAJAAAKQAAATgHAOQgIAMgMAIQgNAIgRAEQgQAEgSABQgRAAgTgEg");
	this.shape_11.setTransform(24.4,5.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AA9BxIADgcQgYAPgXAHQgWAHgVAAQgOAAgNgDQgNgEgLgIQgKgHgGgMQgGgMgBgQQABgQAEgNQAFgMAIgKQAJgJALgHQALgHANgEQANgEAPgCQANgCANAAIAZABIAUACQgCgKgFgJQgDgKgGgHQgGgIgIgFQgJgFgMAAQgIAAgJADQgJACgMAGQgMAGgOAKQgOAKgQARIgYgdQATgSARgLQASgMAPgGQAPgHAOgCQALgCALAAQASAAAOAGQAPAGALALQALALAHAOQAHAPAFARQAFAQACASQACARAAASQAAATgCAWIgHAvgAgJAAQgQADgMAIQgNAIgFALQgHALAEAPQACAMAJAFQAIAFALAAQALAAAOgEIAagKQANgFALgHIAUgLIABgUIgBgUQgKgCgLgCQgKgCgLAAQgSAAgQAFg");
	this.shape_12.setTransform(0.2,4.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AhsigIArgBIAAAcQALgKAMgHQANgGAKgDQAMgEALAAQANAAANADQAOAEALAHQAMAIAKAJQAKAKAIANQAHALAEAOQADAOAAAQQgBARgEAPQgFAPgHALQgIANgKAKQgJALgMAGQgLAHgMAEQgMAEgMgBQgLAAgMgDQgLgDgNgGQgNgFgNgMIgBCGIglABgAgXh3QgKAEgJAIQgJAGgGAKQgGAJgDAKIAAAmQADANAGAJQAHAKAIAGQAJAHAKADQAKAEAKAAQANAAAMgFQAMgGAKgIQAJgKAFgMQAGgNAAgPQABgOgEgNQgFgNgIgLQgJgJgNgGQgMgHgOAAQgMABgLAEg");
	this.shape_13.setTransform(-24.4,10.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AAGgnIgvCJIg7AFIgmjMIAogDIAfClIA6ilIAlACIAoCnIAbipIAsACIgnDOIg7ABg");
	this.shape_14.setTransform(-64,5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgoBsQgUgIgOgPQgOgQgJgVQgIgWAAgaQAAgZAIgVQAJgWAOgPQAOgPAUgJQASgIAWAAQAVAAAUAJQAUAJAOAPQAOAQAIAVQAJAVAAAYQAAAZgJAVQgIAWgOAPQgOAQgUAJQgUAIgVAAQgWAAgSgIgAgdhJQgMAHgHAMQgIAMgDAOQgEAPAAANQAAAOAEAPQAEAOAHAMQAIALAMAIQAMAHAQAAQARAAAMgHQALgIAJgLQAHgMAEgOQAEgPAAgOQAAgNgEgPQgDgOgIgMQgHgMgNgHQgMgHgRAAQgQAAgNAHg");
	this.shape_15.setTransform(-91.3,5.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AhZAxIgCgxIgBgmIgBgdIgBgqIAtAAIAAAYIABAXIAAAYIAOgUQAIgLALgJQAKgKALgHQAMgHAOgCQAQgBANAHIAMAJQAFAFAFAIQAEAJAEALQACALACAQIgnAOIgBgQIgFgNIgEgIIgGgGQgIgFgIABQgGAAgGAEIgMAKIgNAOIgMAPIgNAQIgJAOIAAAdIABAbIABAWIABAQIgqAFIgDg9g");
	this.shape_16.setTransform(-113.4,5.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AhZAxIgCgxIgBgmIgBgdIgBgqIAtAAIAAAYIABAXIAAAYIAOgUQAIgLALgJQAKgKALgHQAMgHAOgCQAQgBANAHIAMAJQAFAFAFAIQAEAJAEALQACALACAQIgnAOIgBgQIgFgNIgEgIIgGgGQgIgFgIABQgGAAgGAEIgMAKIgNAOIgMAPIgNAQIgJAOIAAAdIABAbIABAWIABAQIgqAFIgDg9g");
	this.shape_17.setTransform(-135.4,5.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AA9BxIADgcQgYAPgXAHQgWAHgVAAQgOAAgNgDQgNgEgLgIQgKgHgGgMQgGgMgBgQQABgQAEgNQAFgMAIgKQAJgJALgHQALgHANgEQANgEAOgCQANgCAOAAIAZABIAUACQgDgKgEgJQgDgKgGgHQgGgIgIgFQgJgFgMAAQgIAAgIADQgKACgMAGQgMAGgOAKQgOAKgQARIgYgdQATgSARgLQASgMAPgGQAQgHAMgCQAMgCALAAQATAAANAGQAPAGALALQAKALAIAOQAHAPAFARQAFAQACASQACARAAASQAAATgCAWIgHAvgAgJAAQgQADgNAIQgLAIgHALQgFALADAPQADAMAHAFQAJAFALAAQAMAAANgEIAagKQAMgFAMgHIAUgLIABgUIgBgUQgKgCgLgCQgLgCgLAAQgRAAgQAFg");
	this.shape_18.setTransform(-160.7,4.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0000").s().p("AA1CWQAGgTACgSIAEggIAAgfQgBgWgGgOQgHgOgJgKQgJgJgLgEQgLgFgLAAQgKABgMAGQgJAFgMAKQgLAJgKASIgBCDIgnABIgDkwIAugCIgCB4QALgLAMgHQANgHAKgEQANgEALgBQAWAAASAIQASAIANAPQANAPAIAVQAIAUABAbIgBAcIgBAdIgDAbQgCANgDAKg");
	this.shape_19.setTransform(-196.5,0.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0000").s().p("AgjBrQgVgJgRgQQgQgPgKgVQgKgVAAgZQAAgNAFgPQADgOAIgNQAHgNALgLQALgLAMgHQANgIAPgFQAPgEAQAAQARAAAQAEQAPAEANAIQANAIALALQAKALAIAPIAAAAIghAUIAAgBQgFgKgIgIQgHgIgJgFQgJgGgLgDQgKgDgLAAQgQAAgOAGQgOAGgKALQgLAKgGAPQgGAOAAAPQAAAQAGAPQAGAOALALQAKAKAOAGQAOAHAQAAQAKAAAKgDQAKgDAJgFQAJgFAHgHQAHgIAFgIIABgCIAjAUIgBABQgIANgLAKQgLALgNAHQgNAHgPAEQgQAEgPAAQgWAAgUgIg");
	this.shape_20.setTransform(-222.3,5.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF0000").s().p("AgZCVIABgzIACg+IABhUIAogCIgBAzIgBArIgBAjIAAAbIgBArgAgKheQgFgCgEgEQgEgDgCgGQgCgFAAgFQAAgGACgGQACgFAEgDQAEgEAFgDQAFgCAFAAQAFAAAGACQAFADAEAEQADADADAFQACAGAAAGQAAAFgCAFQgDAGgDADQgEAEgFACQgGACgFAAQgFAAgFgCg");
	this.shape_21.setTransform(-239.4,0.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF0000").s().p("AA1CWQAGgTACgSIAEggIAAgfQgBgWgGgOQgHgOgJgKQgJgJgLgEQgLgFgLAAQgKABgMAGQgJAFgMAKQgLAJgKASIgBCDIgnABIgDkwIAugCIgCB4QALgLAMgHQANgHAKgEQANgEALgBQAWAAASAIQASAIANAPQANAPAIAVQAIAUABAbIgBAcIgBAdIgDAbQgCANgDAKg");
	this.shape_22.setTransform(-257.8,0.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF0000").s().p("Ah1CYIg3kgIApgFIAkDhIBKjPIAwAFIBCDWIAnj3IApAIIg1ElIgvgBIhHjgIhPDjg");
	this.shape_23.setTransform(-290.1,0.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#990000").ss(3,1,1).p("EguqgFxMBdVAAAQBrAABMBMQBNBNAABrIAADbQAABrhNBMQhMBNhrAAMhdVAAAQhrAAhNhNQhMhMAAhrIAAjbQAAhrBMhNQBNhMBrAAg");

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("EguqAFyQhrAAhNhNQhMhMAAhrIAAjbQAAhrBMhNQBNhMBrAAMBdVAAAQBrAABMBMQBNBNAABrIAADbQAABrhNBMQhMBNhrAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-326.1,-38.5,652.3,77);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-50,-50,100,100), null);


(lib.setscopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// WRONG
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.02)").s().p("Ax/XiQlNgji7kBIAAAAQi6kBBFlIQBGlIEdjPIeu2UQEdjPFNAkQFOAjC6EBIAAAAQC7EBhGFIQhFFIkdDPI+uWUQjzCwkXAAQgwAAgxgFg");
	this.shape.setTransform(-231.3,232.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// RIGHT
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("EBJoATgMiVugQ4Qklghi5jlQi4jmAhkmIAAgBQAhklDni4QDni4ElAhMCVvAQ3QElAhC4DmQC4DmghEmIAAAAQghEmjnC4QjDCcjxAAQgrAAgtgFg");
	this.shape_1.setTransform(-15.3,-23.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// question
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF9900").s().p("Ar4D7Ii/qgIJJJZIhXm7IHtGgIh5muIIjIhIh7oSIJhKsg");
	this.shape_2.setTransform(-424.9,-100.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC6600").s().p("AH6h8IqHGmIDQmKIo6EyICvmhIq2HZIFGpzIgCgFIaxCqIAIAKIroIpg");
	this.shape_3.setTransform(-431.1,-38.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_4.setTransform(-180.8,-40.4,1.724,1.724,-15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#793F06").s().p("AngiLIUHiGI5NIjg");
	this.shape_5.setTransform(446.6,63.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF9900").s().p("AnCCkIABgCIi9m0IT9Ilg");
	this.shape_6.setTransform(463.3,8.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#9E5104").s().p("AnGh8IRBBuIARABIgPAFI0ICFg");
	this.shape_7.setTransform(463.7,37.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FBAA30").s().p("ALMHZIz+olIismNIW9OxIgDACg");
	this.shape_8.setTransform(455.7,-11.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("A6pg/IhAjwMA3TAF5IgVDmg");
	this.shape_9.setTransform(307.4,10.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A17446").s().p("AhrO8QhdgLgHgVQgIgWAIgTQAHgXgDAAIAAg/QADiGAMghQALgeAWgwIAihKQBykZASiWQAPiPg4i6QgOg/hgkAQgthzAQgpQAGgdATgTQAdgaA1gTQCIhRApgPQA0gWASAWQATAPAAAwQAAApAEAlQAWMIh7ISQgSBKgWBKQgmB+gZA8QgdA7AHAhQAEAfg+AAIgfgCg");
	this.shape_10.setTransform(35.6,-197.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_11.setTransform(162.4,-266.2,7.423,7.423,0,-48.2,131.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_12.setTransform(158.1,-276,7.423,7.423,0,-48.2,131.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#A8D64A").s().p("AwwP2QopkAk4q6Qh6kJhZlpIAAgLQA0kTBzjMQAdgwAigtQAMC6AwC5QBuGzETEsQF8GbJ9BeQJGBVJajJQBcghBVgiQGLinDikKQEdlKALnjQAEhhgHhgQEKFkBAILQnRIMpCF9QhkBDhlBAQmuEAmyBjQjlAzjUAAQlpAAk3iTg");
	this.shape_13.setTransform(19.7,-9.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#94CD05").s().p("AFGcFQhQgIilgeQihgdhVgIQhagHi9ALQivALhZgLQlkgXk8jeQkljUi1lFQkloMgjtOQgSmYBDk/IAAALQBZFpB6EKQE4K5IpEAQHuDpJriJQGyhjGukAQBlhABkhDQJCl8HRoNIAAAJQBZKNkAJtQkAJ0oIGVQiYBviVBPQktCUkYAAIgNAAgAix2JQi5gsjVirQBsAaB7ADQDpATGqg4QCNgSBOgaQBzglBAhLQh+Dnj9BsQidBDidAAQhiAAhjgbg");
	this.shape_14.setTransform(18.2,48.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#D1EC93").s().p("AgMHFQingdh6iJQhwiKg4hCQBwgtBWhmQAlgqA7hdQA7hcAmgpQA6hKBOgmQBZglBHAaQBrAmBHCfQBQDQgGCFQgTDBiNByQhuBMhzAAQgwAAgxgNg");
	this.shape_15.setTransform(91.8,-64.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#C6E57E").s().p("AmYQyQp9hel8mcQkTkshumyQgwi5gMi6QCcjXDqh7QCxhgCvgPQCDgPB9AeQBnAaCOBDQBSAhCYBOQCKAtCBgFQBPggBUgtQHBjuCmggQCVgiCYAPQCNALCNAzQEbBsDWDfQBHBGBABSQAHBggEBhQgLHikdFKQjiELmLCnQhVAihcAhQmnCNmcAAQiwAAitgZgAM8mBQhPAmg6BKQgnApg7BcQg7BdglAqQhVBmhxAtQA5BCBvCKQB7CJCnAdQCnAtCchsQCNhyATjBQAGiGhRjPQhHifhrgmQgdgLggAAQguAAg0AWgAiQjlQD/BGEAhuQD9hsB+jnQhABLhzAlQhOAaiNASQmqA4jpgTQh7gDhsgaQDVCrC5Asg");
	this.shape_16.setTransform(14.9,-70.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF9900").s().p("AvXDMINJAmIlykHIKGghIl/jmIMEAWInFktIOSBRI1VQWg");
	this.shape_17.setTransform(-306.4,263.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC6600").s().p("AqrC5IgEgDIVVwWIAKABIiiOQIipoCIi4LvIh4mtIjMJmIiempIi1Mzg");
	this.shape_18.setTransform(-275.8,301.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#330000").s().p("A4ZIPMAwzgTyIAAD5MgvZATOg");
	this.shape_19.setTransform(-207.3,213.5,1,1,-15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_8
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FF3333").ss(10,1,1).p("EhLvg+eMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAMiXeAAAQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAg");
	this.shape_20.setTransform(-12.8,15.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CCCCCC").s().p("EhLvA+fQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAgEhVAg4SQj2D2AAFbMAAABeCQAAFcD2D2QD2D2FbAAMCXeAAAQFcAAD2j2QD2j2AAlcMAAAheCQAAlbj2j2Qj2j2lcAAMiXeAAAIAAAAQlbAAj2D2g");
	this.shape_21.setTransform(-12.8,15.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("EhLvA8JQlbAAj2j2Qj2j2AAlcMAAAheCQAAlbD2j2QD2j2FbAAMCXeAAAQFcAAD2D2QD2D2AAFbMAAABeCQAAFcj2D2Qj2D2lcAAg");
	this.shape_22.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20}]}).wait(1));

	// Layer_11
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(255,255,255,0.02)").s().p("EhNQBCIQngAAlVlVQlVlVAAnhMAAAhf6QAAngFVlVQFVlVHgAAMCagAAAQHiAAFUFVQFUFVAAHgMAAABf6QAAHhlUFVQlUFVniAAg");
	this.shape_23.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy6, new cjs.Rectangle(-623.5,-407.7,1221.3,846.4), null);


(lib.setscopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// WRONG
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.02)").s().p("Ax/XiQlNgji7kBIAAAAQi6kBBFlIQBGlIEdjPIeu2UQEdjPFNAkQFOAjC6EBIAAAAQC7EBhGFIQhFFIkdDPI+uWUQjzCwkXAAQgwAAgxgFg");
	this.shape.setTransform(-231.3,232.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// RIGHT
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("EBJoATgMiVugQ4Qklghi5jlQi4jmAhkmIAAgBQAhklDni4QDni4ElAhMCVvAQ3QElAhC4DmQC4DmghEmIAAAAQghEmjnC4QjDCcjxAAQgrAAgtgFg");
	this.shape_1.setTransform(-15.3,-23.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// question
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#793F06").s().p("AngiLIUHiGI5NIjg");
	this.shape_2.setTransform(446.6,63.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF9900").s().p("AnCCkIABgCIi9m0IT9Ilg");
	this.shape_3.setTransform(463.3,8.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9E5104").s().p("AnGh8IRBBuIARABIgPAFI0ICFg");
	this.shape_4.setTransform(463.7,37.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FBAA30").s().p("ALMHZIz+olIismNIW9OxIgDACg");
	this.shape_5.setTransform(455.7,-11.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("A6pg/IhAjwMA3TAF5IgVDmg");
	this.shape_6.setTransform(307.4,10.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A17446").s().p("AhrO8QhdgLgHgVQgIgWAIgTQAHgXgDAAIAAg/QADiGAMghQALgeAWgwIAihKQBykZASiWQAPiPg4i6QgOg/hgkAQgthzAQgpQAGgdATgTQAdgaA1gTQCIhRApgPQA0gWASAWQATAPAAAwQAAApAEAlQAWMIh7ISQgSBKgWBKQgmB+gZA8QgdA7AHAhQAEAfg+AAIgfgCg");
	this.shape_7.setTransform(35.6,-197.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#578A0A").s().p("AhcAmIAGgDIgWgIIgGgDIAdALQAUgNAVgKIgBgBQgTgGgTgDQASACAVAGIBAgdIA/gdIAGgEIAvgYIAPgIIAAAAIAAABIABADIAAAGIAAABIgGADIgPAIIghAPQgCASgeAxIASgjIANgfIABgBIgHAEIAAgBQgUAKgUAIIgBABIgQAGQgfAOgfAOQgbAOgaAOIgKAGQgeASgeAYQAcgbAfgUgAgmAQIABAAQgEAOgKAQIgWAeQAggtADgPgAhSAmQgCAIgGAJIgNARQASgZADgJgAgEAYQAHgRAFgOIABgBQgCARgdAzgAgQgDIgmgNIgMgGIAzATgAACgaQgFgEgSgGIAmANIADABQgJAAgJgEgAAmgqIgngUQAsAUAOAEIABAAIgDAAQgIAAgJgEgABsg+IgmgMIgMgGIAzASg");
	this.shape_8.setTransform(162.4,-266.2,7.423,7.423,0,-48.2,131.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#6EB008").s().p("AiBBtQgOgDgEgHQgEgFACgPQAShPAjgrQAVgcAdgSQAQgLARgFQANgFAPgCQAbgCAZAJIAPAGQAYAMAPATIAAAAIAGAKIADgBIAJgDIADgBIgvAZIgGADIg/AdIhAAeQgVgHgSgCQATADATAGIABABQgVAKgUANIgdgLIAGADIAWAIIgGAEQgfATgcAbQAegYAegSIAKgFQAagPAbgNQAfgQAfgNIAQgGIABgBQAUgIAUgKIAAABIAHgEIAhgPIACAEIAHAPQALAggOAdQgRAlg7AXIgPAFQg5ARg8AAQggAAgggEgAhDBZIAWgeQAKgQAEgOIgBAAQgDAPggAtgAhhBWIANgSQAGgJACgIQgDAJgSAagAAOAGQgFAOgIARIgRAkQAdgyACgSgABnghIgNAfIgSAjQAegxACgSgAgwgDIAmAMIABAAIgzgSIAMAGgAAIgNQAJAEAJABIgDgCIgmgNQARAHAGADgAAsgdQALAFAJgBIgBAAQgOgDgtgUIAoATgABMg9IAmANIABAAIgzgTIAMAGg");
	this.shape_9.setTransform(158.1,-276,7.423,7.423,0,-48.2,131.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A8D64A").s().p("AwwP2QopkAk4q6Qh6kJhZlpIAAgLQA0kTBzjMQAdgwAigtQAMC6AwC5QBuGzETEsQF8GbJ9BeQJGBVJajJQBcghBVgiQGLinDikKQEdlKALnjQAEhhgHhgQEKFkBAILQnRIMpCF9QhkBDhlBAQmuEAmyBjQjlAzjUAAQlpAAk3iTg");
	this.shape_10.setTransform(19.7,-9.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#94CD05").s().p("AFGcFQhQgIilgeQihgdhVgIQhagHi9ALQivALhZgLQlkgXk8jeQkljUi1lFQkloMgjtOQgSmYBDk/IAAALQBZFpB6EKQE4K5IpEAQHuDpJriJQGyhjGukAQBlhABkhDQJCl8HRoNIAAAJQBZKNkAJtQkAJ0oIGVQiYBviVBPQktCUkYAAIgNAAgAix2JQi5gsjVirQBsAaB7ADQDpATGqg4QCNgSBOgaQBzglBAhLQh+Dnj9BsQidBDidAAQhiAAhjgbg");
	this.shape_11.setTransform(18.2,48.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#D1EC93").s().p("AgMHFQingdh6iJQhwiKg4hCQBwgtBWhmQAlgqA7hdQA7hcAmgpQA6hKBOgmQBZglBHAaQBrAmBHCfQBQDQgGCFQgTDBiNByQhuBMhzAAQgwAAgxgNg");
	this.shape_12.setTransform(91.8,-64.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#C6E57E").s().p("AmYQyQp9hel8mcQkTkshumyQgwi5gMi6QCcjXDqh7QCxhgCvgPQCDgPB9AeQBnAaCOBDQBSAhCYBOQCKAtCBgFQBPggBUgtQHBjuCmggQCVgiCYAPQCNALCNAzQEbBsDWDfQBHBGBABSQAHBggEBhQgLHikdFKQjiELmLCnQhVAihcAhQmnCNmcAAQiwAAitgZgAM8mBQhPAmg6BKQgnApg7BcQg7BdglAqQhVBmhxAtQA5BCBvCKQB7CJCnAdQCnAtCchsQCNhyATjBQAGiGhRjPQhHifhrgmQgdgLggAAQguAAg0AWgAiQjlQD/BGEAhuQD9hsB+jnQhABLhzAlQhOAaiNASQmqA4jpgTQh7gDhsgaQDVCrC5Asg");
	this.shape_13.setTransform(14.9,-70.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF9900").s().p("AvXDMINJAmIlykHIKGghIl/jmIMEAWInFktIOSBRI1VQWg");
	this.shape_14.setTransform(-306.4,263.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CC6600").s().p("AqrC5IgEgDIVVwWIAKABIiiOQIipoCIi4LvIh4mtIjMJmIiempIi1Mzg");
	this.shape_15.setTransform(-275.8,301.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#330000").s().p("A4ZIPMAwzgTyIAAD5MgvZATOg");
	this.shape_16.setTransform(-207.3,213.5,1,1,-15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF9900").s().p("Ar4D7Ii/qgIJJJZIhXm7IHtGgIh5muIIjIhIh7oSIJhKsg");
	this.shape_17.setTransform(-424.9,-100.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC6600").s().p("AH6h8IqHGmIDQmKIo6EyICvmhIq2HZIFGpzIgCgFIaxCqIAIAKIroIpg");
	this.shape_18.setTransform(-431.1,-38.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#330000").s().p("A6wpVIAuh+MA0zAUXIAACQg");
	this.shape_19.setTransform(-180.8,-40.4,1.724,1.724,-15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_8
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FF3333").ss(10,1,1).p("EhLvg+eMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAMiXeAAAQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAg");
	this.shape_20.setTransform(-12.8,15.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CCCCCC").s().p("EhLvA+fQmZAAkikiQkikiAAmaMAAAheCQAAmZEikiQEikiGZAAMCXeAAAQGaAAEiEiQEiEiAAGZMAAABeCQAAGakiEiQkiEimaAAgEhVAg4SQj2D2AAFbMAAABeCQAAFcD2D2QD2D2FbAAMCXeAAAQFcAAD2j2QD2j2AAlcMAAAheCQAAlbj2j2Qj2j2lcAAMiXeAAAIAAAAQlbAAj2D2g");
	this.shape_21.setTransform(-12.8,15.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("EhLvA8JQlbAAj2j2Qj2j2AAlcMAAAheCQAAlbD2j2QD2j2FbAAMCXeAAAQFcAAD2D2QD2D2AAFbMAAABeCQAAFcj2D2Qj2D2lcAAg");
	this.shape_22.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20}]}).wait(1));

	// Layer_11
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(255,255,255,0.02)").s().p("EhNQBCIQngAAlVlVQlVlVAAnhMAAAhf6QAAngFVlVQFVlVHgAAMCagAAAQHiAAFUFVQFUFVAAHgMAAABf6QAAHhlUFVQlUFVniAAg");
	this.shape_23.setTransform(-12.8,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy3, new cjs.Rectangle(-623.5,-407.7,1221.3,846.4), null);


(lib.questiontext_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMB4IgFgDQgDgCgBgEQgCgDAAgDQAAgDACgDQABgEADgCQABgCAEgBQADgCADABIAGABQACABADACIADAGIABAGIgBAGIgDAGIgFADIgGABIgGgBgAgWBFIgCgMQAAgJAEgHQADgHAEgGIALgLIALgKIANgJQAGgGAFgGQAEgGAEgHQADgIAAgJQgBgKgDgJQgDgJgGgIQgGgHgIgDQgJgFgJAAQgKAAgIAEQgIADgFAIQgGAGgDAJQgCAIAAAKIAAAGIABAIIgaAFIgCgKIgBgKQAAgPAGgNQAFgNAJgJQAJgLAOgFQANgGAPAAQAPAAANAFQAOAGAJAKQAJAKAGANQAFANABARIgBAOIgFAPQgDAIgFAHQgFAGgJAHIgOALQgIAFgGAHQgHAGgEAIQgDAJABAMg");
	this.shape.setTransform(237.7,-16.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_1.setTransform(219.7,-15.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgfB6IgOgFIgNgIQgHgFgGgGQgFgHgDgJIAcgMQADAGAEAFQADAEAFADIAJAFIAKADQALACALgBQAKgCAIgEQAHgFAFgFQAFgFADgGIAGgLIACgKIABgGIAAgTQgJAIgJAEQgJAFgJACQgJACgIABQgRAAgPgGQgOgGgLgLQgLgLgGgPQgHgQAAgUQAAgUAHgPQAHgQALgLQAMgKAOgGQAOgGAOAAIATAFIATAIQAKAFAIAJIAAgdIAeAAIgCCoQAAAJgCAKQgDAJgEAJQgEAJgIAIQgGAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgOgEgAgVhZQgJAFgHAHQgGAIgEAKQgDALAAAMQAAAMADAKQAEAKAGAGQAHAHAJAEQAJAFALAAQAJAAAIgEQAKgDAHgGQAHgFAFgJQAGgIABgKIAAgTQgBgLgGgIQgFgJgHgGQgIgGgJgEQgKgDgIAAQgKAAgJAEg");
	this.shape_2.setTransform(199.3,-9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPBRQgNAAgKgFQgKgEgGgIQgGgHgEgKQgEgKgCgLQgCgLAAgLIgBgWQAAgPABgQIADggIAfABIgEAhIAAAZIAAANIABAPIADARQACAIAEAHQAEAHAFADQAGAEAJgCQAKgBAMgOQAMgOANgcIAAglIgBgVIAAgMIAggEIABAuIABAjIABAdIAAAVIABAfIghAAIgBgqIgLARQgFAHgIAGQgHAGgIADQgGAEgIAAIgCgBg");
	this.shape_3.setTransform(180.4,-12.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgeBQQgOgHgLgKQgKgMgHgQQgFgPgBgUQABgSAFgQQAHgQAKgLQALgMAOgGQAOgGAQAAQAQAAAPAHQAOAGAKAMQALALAGAQQAGAQABARQgBATgGAPQgGAQgLAMQgKALgOAHQgPAGgQAAQgQAAgOgGgAgUg2QgJAGgHAIQgFAJgDAKQgCALAAAKQAAAKACALQADAKAGAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJACgKQADgLAAgKQAAgKgCgLQgDgKgGgJQgGgIgIgGQgKgFgMgBQgMABgIAFg");
	this.shape_4.setTransform(162.2,-12.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_5.setTransform(145.8,-12.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_6.setTransform(127.2,-15.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygCIACg/IAcgCIgBBBIA5gCIgDAcIg3ABIgBB+IgeABg");
	this.shape_7.setTransform(110.1,-15.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgNgHQgHgDgHgFIAPgXIAQAJIARAHQAJADAJABQAJACAKAAQAHAAAGgBQAFgCAEgCQAEgCACgDIADgGIAAgFIgBgGQgBgDgEgDIgHgFIgMgDIgPgCQgMgBgNgCQgMgCgKgFQgJgEgGgHQgHgHgBgLQgBgLADgIQABgJAGgHQAEgHAIgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIARAFQAJAEAIAFIgKAbQgKgFgJgDIgQgFIgPgCQgVgBgNAGQgNAGAAANQAAAIAFAEQAEAEAJACIATADIAXACQAPACAJAFQAKADAGAGQAGAGADAHQACAHAAAIQABAOgGAJQgGAJgJAGQgKAGgMADQgMADgNABQgNAAgNgDg");
	this.shape_8.setTransform(86.7,-12);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AggBPQgPgHgLgKQgJgLgGgPQgFgPgBgTQABgRAGgQQAGgQALgMQALgLAPgHQAOgHAQAAQAQABANAFQANAHALAKQAKALAHAOQAHANACARIh+ASQABAKADAJQAFAIAGAGQAGAGAJADQAIACAJAAQAIABAIgDQAHgDAGgEQAHgFAFgGQAEgIACgJIAdAHQgEANgIAKQgHALgKAIQgKAIgMAEQgMAFgNgBQgTAAgOgFgAgLg4QgHACgIAFQgGAFgGAJQgFAJgCAMIBXgLIgCgCQgFgPgJgIQgKgJgPAAQgFABgHACg");
	this.shape_9.setTransform(69.7,-12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgCgDgDgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgJgEgHgHQgGgHgBgLQgBgLADgIQACgJAEgHQAGgHAHgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgWgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQADAHgBAIQABAOgGAJQgGAJgJAGQgJAGgNADQgMADgNABQgNAAgNgDg");
	this.shape_10.setTransform(52.3,-12);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUBTIgNgDIgNgEIgOgHQgHgDgFgFIANgXIARAJIARAHQAJADAJABQAJACAJAAQAJAAAFgBQAGgCADgCQAEgCACgDIADgGIAAgFIgBgGQgCgDgDgDIgHgFIgMgDIgQgCQgMgBgMgCQgMgCgKgFQgKgEgGgHQgGgHgBgLQgBgLADgIQACgJAEgHQAGgHAHgFQAHgFAJgEQAIgDAKgCQAIgCAKAAIANABIAQACIASAFQAIAEAIAFIgKAbQgKgFgJgDIgPgFIgQgCQgWgBgMAGQgNAGAAANQAAAIAFAEQAFAEAIACIAUADIAWACQAOACAKAFQAKADAGAGQAGAGADAHQADAHgBAIQABAOgGAJQgGAJgJAGQgJAGgNADQgMADgNABQgNAAgNgDg");
	this.shape_11.setTransform(35.6,-12);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_12.setTransform(17.6,-12.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhQh2IAhgBIgBAVQAJgIAJgFIAQgHQAJgCAIgBQAKAAAKADQAJADAJAFQAJAFAHAIQAIAHAFAJQAFAJADAKQADALAAALQgBANgDALQgDALgGAIQgFAKgIAHQgHAIgIAFQgJAFgIADQgJACgJAAQgIAAgJgCQgIgCgKgFQgKgEgJgIIgBBjIgbABgAgQhYQgIADgHAGQgGAFgEAHQgFAGgCAIIgBAcQADAJAEAIQAFAHAHAEQAGAFAHADQAIACAHAAQAKAAAJgDQAJgEAGgGQAHgHAEgKQAEgJABgLQAAgLgDgKQgDgKgHgHQgGgHgJgFQgJgEgLAAQgIAAgIADg");
	this.shape_13.setTransform(-0.5,-8.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAEgdIgiBlIgrAEIgdiXIAegCIAWB6IAsh6IAbACIAeB7IAUh8IAgABIgdCYIgsABg");
	this.shape_14.setTransform(-29.8,-12.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgeBQQgOgHgKgKQgMgMgFgQQgHgPAAgUQAAgSAHgQQAFgQAMgLQAKgMAOgGQAPgGAPAAQAQAAAOAHQAOAGALAMQALALAGAQQAHAQAAARQAAATgHAPQgGAQgLAMQgLALgOAHQgOAGgQAAQgPAAgPgGgAgVg2QgIAGgGAIQgGAJgCAKQgDALAAAKQAAAKADALQADAKAFAJQAGAIAJAGQAJAGALgBQAMABAJgGQAJgGAGgIQAGgJADgKQACgLAAgKQABgKgDgLQgDgKgFgJQgHgIgJgGQgIgFgNgBQgMABgJAFg");
	this.shape_15.setTransform(-50.1,-12.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_16.setTransform(-66.4,-12.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_17.setTransform(-82.8,-12.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAuBTIACgUQgSALgRAFQgRAGgPAAQgKAAgKgDQgKgDgHgFQgIgGgFgJQgEgJAAgLQAAgMADgKQAEgJAGgHQAGgGAIgGQAJgFAJgDQAKgDALgCIATgBIATAAIAOACIgEgOQgDgHgEgGQgFgFgGgEQgGgDgJAAIgMABQgIACgIAEQgJAFgLAHQgKAIgMAMIgSgVQAPgOAMgIQANgIAMgFQALgFAKgCIAQgBQAOAAAKAEQALAFAIAIQAIAIAFALQAGAKADANQAEAMABANIACAaIgCAeQgBAQgEATgAgGAAQgMACgJAGQgJAGgFAIQgEAJACALQACAIAGAEQAGADAJAAQAIAAAKgCIATgHIASgJIAPgJIAAgOIgBgPIgPgDIgPgBQgOAAgLADg");
	this.shape_18.setTransform(-101.5,-12.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_19.setTransform(-128,-15.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_20.setTransform(-147,-12.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgSBuIABglIABguIABg+IAdgBIgBAlIAAAgIgBAaIAAATIAAAggAgHhFIgGgEQgDgDgCgEQgCgEAAgEQAAgEACgEQACgEADgDQACgDAEgBQADgCAEAAQAEAAAEACQAEABADADIAFAHIABAIQAAAEgBAEIgFAHIgHAEIgIACQgEAAgDgCg");
	this.shape_21.setTransform(-159.7,-15.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_22.setTransform(-173.4,-15.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AhXBwIgojUIAegEIAbCnIA3iaIAjAEIAwCfIAei3IAeAGIgnDYIgjAAIg1inIg6Cog");
	this.shape_23.setTransform(-197.2,-16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(6,1,1).p("EglAgEnMBKBAAAQCcAAAACcIAAEXQAACcicAAMhKBAAAQicAAAAicIAAkXQAAicCcAAg");
	this.shape_24.setTransform(16.3,-15.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF3333").s().p("EglAAEoQicAAAAicIAAkXQAAicCcAAMBKBAAAQCcAAAACcIAAEXQAACcicAAg");
	this.shape_25.setTransform(16.3,-15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy, new cjs.Rectangle(-239.2,-47.8,511.1,65.3), null);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween11("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0,-1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.97,scaleY:0.97},9).to({scaleX:1,scaleY:1},10).to({scaleX:0.97,scaleY:0.97},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-326.1,-40.4,652.3,77);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.setscopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.question = new lib.setscopy6();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(-9.5,48.4,1.073,1.073,0,0,0,2.8,0.3);

	this.timeline.addTween(cjs.Tween.get(this.question).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy5, new cjs.Rectangle(-681.3,-389.2,1310,907.8), null);


(lib.setscopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.question = new lib.setscopy3();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(-9.5,48.4,1.073,1.073,0,0,0,2.8,0.3);

	this.timeline.addTween(cjs.Tween.get(this.question).wait(1));

}).prototype = getMCSymbolPrototype(lib.setscopy, new cjs.Rectangle(-681.3,-389.2,1310,907.8), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(650.3,411.4,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(227).to({_off:false},0).wait(31).to({startPosition:3},0).to({alpha:0,startPosition:11},8).wait(7));

	// Layer_8
	this.question = new lib.setscopy5();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(654.7,392.4,0.608,0.608,0,0,0,2.7,0.1);
	this.question._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question).wait(227).to({_off:false},0).to({regX:2.9,scaleX:0.64,scaleY:0.64},17).wait(7).to({alpha:0},8).wait(14));

	// Layer_5
	this.instance_1 = new lib.Symbol1copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(403.4,420.5,1,1,-9.8,0,0,193.6,70.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(189).to({_off:false},0).to({_off:true},35).wait(49));

	// ARROW
	this.instance_2 = new lib.Symbol1copy2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(335.8,280.5,1,1,-26,0,0,41.1,60.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(156).to({_off:false},0).to({_off:true},36).wait(81));

	// Layer_7
	this.question_1 = new lib.setscopy5();
	this.question_1.name = "question_1";
	this.question_1.parent = this;
	this.question_1.setTransform(654.7,392.4,0.608,0.608,0,0,0,2.7,0.1);
	this.question_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question_1).wait(110).to({_off:false},0).wait(39).to({alpha:0},6).to({_off:true},1).wait(117));

	// Layer_4
	this.instance_3 = new lib.Tween6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(639.6,116.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(71).to({_off:false},0).wait(78).to({startPosition:28},0).to({alpha:0,startPosition:34},6).to({_off:true},1).wait(117));

	// Layer_3
	this.question_2 = new lib.setscopy();
	this.question_2.name = "question_2";
	this.question_2.parent = this;
	this.question_2.setTransform(654.7,392.4,0.608,0.608,0,0,0,2.7,0.1);
	this.question_2.alpha = 0.352;
	this.question_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question_2).wait(45).to({_off:false},0).to({alpha:1},7).wait(197).to({alpha:0},8).wait(16));

	// Layer_2
	this.questiontxt = new lib.questiontext_mccopy();
	this.questiontxt.name = "questiontxt";
	this.questiontxt.parent = this;
	this.questiontxt.setTransform(620.9,122.3,1.351,1.351,0,0,0,2.2,-9.3);
	this.questiontxt.alpha = 0;
	this.questiontxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questiontxt).wait(21).to({_off:false},0).to({alpha:1},7).wait(221).to({alpha:0},8).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(260.9,112.9,2025,1284);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;