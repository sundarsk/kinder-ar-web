(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape.setTransform(251,7.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("Ag6DfQgMgDgMgGQgNgFgMgJQgMgJgKgMQgKgMgGgRIAzgWQAFALAIAJQAHAIAIAGQAJAFAIAEQAJADAIACQATAEAWgDQASgDAOgIQANgIAKgJQAKgKAGgKQAGgLAEgKIAEgRIACgMIAAgjQgRAPgRAIQgRAIgPAEQgRAFgPAAQggAAgbgLQgagLgUgUQgUgUgLgcQgMgdAAgkQAAgkANgdQAMgcAVgUQAVgTAZgLQAagKAaAAQASACASAGQAQAFASAJQASAKAQAQIABg1IA0ABIgCEzQAAARgFASQgDARgJAQQgJAQgMAOQgNAPgQALQgQALgUAHQgUAHgYACIgJAAQgbAAgYgHgAgoiiQgQAIgMANQgMAOgGATQgHATABAWQgBAWAHATQAHASAMANQAMANAQAHQAQAIAVAAQAQAAAQgGQARgHAOgLQANgKAJgPQAKgQAEgSIAAgjQgEgTgJgQQgKgPgNgMQgPgLgRgGQgQgGgSAAQgSAAgRAIg");
	this.shape_1.setTransform(215.5,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("ABTCZIAEgmQghAVgfAJQgfAJgcAAQgTAAgRgEQgTgFgOgLQgNgKgJgQQgIgQAAgWQAAgVAHgRQAFgRAMgOQALgMAQgJQAPgJARgGQASgGATgDQASgDAUAAIAgABIAbAEQgDgNgGgNQgEgNgIgLQgIgKgLgGQgMgHgQAAQgKAAgNADQgNADgQAIQgRAIgSAPQgUANgWAWIgfgnQAZgYAXgPQAYgQAVgIQAUgJASgDQARgDAPAAQAYAAAUAIQASAJAQAOQAOAPAKAUQAKATAGAXQAGAWADAZQADAXABAYQgBAagDAdQgDAegGAigAgMgBQgXAFgRALQgQAKgHAQQgJAPAFAUQADAQAMAHQAKAGAQAAQAPAAASgFQARgFASgIIAhgQIAbgQIABgaQAAgNgCgPQgMgDgPgCQgPgCgNAAQgZAAgVAFg");
	this.shape_2.setTransform(180.6,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("ACBBAIgBg6IgBgrQgBgWgFgLQgHgKgKAAQgHAAgHAEQgHAEgGAGQgHAHgHAIIgMARIgLARIgKANIABAZIACAhIAAAoIAAAyIgzADIgBhTIgCg6IgCgrQgCgWgFgLQgHgKgKAAQgHAAgIAEQgGAFgIAHIgPAQIgMATIgNASIgKAOIACCNIg0ADIgIkfIA4gGIABBOIASgWQAKgLALgKQALgJANgGQANgGAQgBQALAAALAEQAKAEAIAIQAIAHAGAMQAGALABAQIASgVQAJgLALgJQALgJANgFQAMgHAPAAQAMAAAMAEQAKAFAKAIQAIAJAFANQAGANAAASQACAUABAdIADBAIABBeIg4ADIAAhTg");
	this.shape_3.setTransform(142,6.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AghDJIABhEIAChUIABhyIA3gCIgCBFIgBA6IgBAvIgBAkIgBA6gAgOh/QgHgDgFgFQgFgFgDgHQgDgHAAgIQAAgIADgGQADgHAFgGQAFgFAHgDQAHgDAHAAQAIAAAHADQAGADAGAFQAFAGADAHQADAGAAAIQAAAIgDAHQgDAHgFAFQgGAFgGADQgHADgIAAQgHAAgHgDg");
	this.shape_4.setTransform(113.4,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_5.setTransform(61.5,7.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("ABIDMQAHgaAEgYQADgZABgTQACgWgBgVQgBgcgJgVQgIgTgNgMQgMgNgPgGQgPgGgPAAQgOACgPAHQgOAHgPANQgPANgNAYIgCCyIg0ABIgFmcIA+gCIgCCjQAPgQAQgJQARgKAOgFQARgFAPgCQAeAAAZALQAYALASAUQASAUAKAdQAKAbACAkIgBAnIgBAnIgEAkQgDARgEAOg");
	this.shape_6.setTransform(27.4,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgggeIhcACIABgxIBcgDIACh1IA0gEIgBB4IBogDIgFAyIhkACIgCDpIg3ABg");
	this.shape_7.setTransform(-3.8,1.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_8.setTransform(-47.2,7.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgqCQIhekOIA2gIIBQDlIBUjvIA3AIIhpEZg");
	this.shape_9.setTransform(-78.6,6.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("Ah5BCIgChCIgBg0IgBgnIgDg4IA9gBIABAhIAAAfIABAgQAHgMAMgPQALgOAOgOQAOgNAQgJQAQgKAUgDQAUAAASAJQAIAFAIAHQAGAHAHAMQAGALAEAPQAFAPABAVIg1AUQAAgNgBgKQgCgKgEgGIgGgMIgIgIQgKgGgMAAQgHABgIAFQgJAGgIAIQgJAIgIAKIgSAVIgQAWIgNATIABAnIABAkIABAfIABAVIg5AIIgEhUg");
	this.shape_10.setTransform(-107.3,7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_11.setTransform(-139.8,7.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgmCXIgXgEIgYgJQgNgFgMgGQgMgHgLgJIAagpIAeAPQAPAIAQAFQAQAFARADQAQAEASAAQAPAAAKgCQAKgEAHgDQAGgFAEgFIAFgKQABgEAAgFQAAgGgDgGQgCgEgGgGQgFgFgIgEQgJgFgMgCQgNgCgRgBQgXgBgWgFQgWgDgSgJQgRgIgMgMQgLgOgDgUQgBgUAEgPQAEgQAKgNQAIgMANgKQAOgJAPgHQAQgGARgDQARgDARgBIAYABIAeAFQAQADAPAHQAQAFAOAKIgTAxQgRgJgQgGIgcgHQgPgEgMgBQgpgDgYAMQgXALAAAXQAAAQAJAHQAIAIAQADQAPADAUABQAUACAXADQAaAEASAIQASAHALALQALAKAEAOQAFAMAAAPQAAAZgKARQgKARgRAMQgSAKgWAFQgXAGgYAAQgXABgZgGg");
	this.shape_12.setTransform(-171.6,7.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgYDMQgPgDgSgIQgSgJgRgPIAAAjIgyABIgEmcIA6gCIgBCeQAPgPARgIQARgIAOgEQAQgEAPgBQASAAASAGQASAEAQAKQAQAKAOANQANAOAKAQQAKAQAFASQAFATgBAVQAAAXgHAUQgGAVgKARQgKARgNANQgNAOgPAKQgQAJgQAFQgQAFgQAAQgQgBgRgFgAgfgiQgOAFgMAJQgMAKgIAKQgJAMgFAOIAABAQAFARAJANQAIAMAMAJQAMAJANAEQAOAEAOAAQASABARgIQARgHANgNQANgNAHgTQAIgRABgVQABgUgGgSQgGgTgMgOQgNgNgRgJQgRgIgUAAQgQABgPAFg");
	this.shape_13.setTransform(-203.5,0.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("Ag1DIQgagIgXgNQgWgOgSgTQgTgTgNgXQgNgXgIgaQgGgagBgdQABgcAGgaQAIgbANgXQANgXATgSQASgTAWgOQAXgNAagIQAagHAbAAQAbAAAbAHQAaAIAWANQAWAOATATQATASANAXQANAXAHAbQAIAagBAcQABAdgIAaQgHAagNAXQgNAXgTATQgTATgWAOQgWANgaAIQgbAHgbAAQgcAAgZgHgAg6iNQgaANgVAUQgUAVgLAcQgMAcgBAfQABAgAMAcQALAcAUAVQAVAVAaAMQAbAMAfAAQAVAAASgFQATgGARgKQARgKAOgOQANgOAKgRQAJgRAGgUQAGgUgBgVQABgUgGgUQgGgTgJgSQgKgRgNgOQgOgOgRgKQgRgKgTgFQgSgGgVAAQgfAAgbAMg");
	this.shape_14.setTransform(-244.5,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(3,1,1).p("EA0RgFbMhogAAAQiQAAhnBlQhmBnAACPQAACQBmBmQBnBmCQAAMBogAAAQCQAABlhmQBnhmAAiQQAAiPhnhnQhlhliQAAg");
	this.shape_15.setTransform(0,0.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFCC00").s().p("Eg0PAFcQiQAAhmhmQhnhmAAiQQAAiPBnhnQBmhlCQAAMBofAAAQCQAABmBlQBmBnABCPQgBCQhmBmQhmBmiQAAg");
	this.shape_16.setTransform(0,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-370.7,-42.9,741.5,86);


(lib.Tween32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape.setTransform(251,7.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("Ag6DfQgMgDgMgGQgNgFgMgJQgMgJgKgMQgKgMgGgRIAzgWQAFALAIAJQAHAIAIAGQAJAFAIAEQAJADAIACQATAEAWgDQASgDAOgIQANgIAKgJQAKgKAGgKQAGgLAEgKIAEgRIACgMIAAgjQgRAPgRAIQgRAIgPAEQgRAFgPAAQggAAgbgLQgagLgUgUQgUgUgLgcQgMgdAAgkQAAgkANgdQAMgcAVgUQAVgTAZgLQAagKAaAAQASACASAGQAQAFASAJQASAKAQAQIABg1IA0ABIgCEzQAAARgFASQgDARgJAQQgJAQgMAOQgNAPgQALQgQALgUAHQgUAHgYACIgJAAQgbAAgYgHgAgoiiQgQAIgMANQgMAOgGATQgHATABAWQgBAWAHATQAHASAMANQAMANAQAHQAQAIAVAAQAQAAAQgGQARgHAOgLQANgKAJgPQAKgQAEgSIAAgjQgEgTgJgQQgKgPgNgMQgPgLgRgGQgQgGgSAAQgSAAgRAIg");
	this.shape_1.setTransform(215.5,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("ABTCZIAEgmQghAVgfAJQgfAJgcAAQgTAAgRgEQgTgFgOgLQgNgKgJgQQgIgQAAgWQAAgVAHgRQAFgRAMgOQALgMAQgJQAPgJARgGQASgGATgDQASgDAUAAIAgABIAbAEQgDgNgGgNQgEgNgIgLQgIgKgLgGQgMgHgQAAQgKAAgNADQgNADgQAIQgRAIgSAPQgUANgWAWIgfgnQAZgYAXgPQAYgQAVgIQAUgJASgDQARgDAPAAQAYAAAUAIQASAJAQAOQAOAPAKAUQAKATAGAXQAGAWADAZQADAXABAYQgBAagDAdQgDAegGAigAgMgBQgXAFgRALQgQAKgHAQQgJAPAFAUQADAQAMAHQAKAGAQAAQAPAAASgFQARgFASgIIAhgQIAbgQIABgaQAAgNgCgPQgMgDgPgCQgPgCgNAAQgZAAgVAFg");
	this.shape_2.setTransform(180.6,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("ACBBAIgBg6IgBgrQgBgWgFgLQgHgKgKAAQgHAAgHAEQgHAEgGAGQgHAHgHAIIgMARIgLARIgKANIABAZIACAhIAAAoIAAAyIgzADIgBhTIgCg6IgCgrQgCgWgFgLQgHgKgKAAQgHAAgIAEQgGAFgIAHIgPAQIgMATIgNASIgKAOIACCNIg0ADIgIkfIA4gGIABBOIASgWQAKgLALgKQALgJANgGQANgGAQgBQALAAALAEQAKAEAIAIQAIAHAGAMQAGALABAQIASgVQAJgLALgJQALgJANgFQAMgHAPAAQAMAAAMAEQAKAFAKAIQAIAJAFANQAGANAAASQACAUABAdIADBAIABBeIg4ADIAAhTg");
	this.shape_3.setTransform(142,6.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AghDJIABhEIAChUIABhyIA3gCIgCBFIgBA6IgBAvIgBAkIgBA6gAgOh/QgHgDgFgFQgFgFgDgHQgDgHAAgIQAAgIADgGQADgHAFgGQAFgFAHgDQAHgDAHAAQAIAAAHADQAGADAGAFQAFAGADAHQADAGAAAIQAAAIgDAHQgDAHgFAFQgGAFgGADQgHADgIAAQgHAAgHgDg");
	this.shape_4.setTransform(113.4,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_5.setTransform(61.5,7.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("ABIDMQAHgaAEgYQADgZABgTQACgWgBgVQgBgcgJgVQgIgTgNgMQgMgNgPgGQgPgGgPAAQgOACgPAHQgOAHgPANQgPANgNAYIgCCyIg0ABIgFmcIA+gCIgCCjQAPgQAQgJQARgKAOgFQARgFAPgCQAeAAAZALQAYALASAUQASAUAKAdQAKAbACAkIgBAnIgBAnIgEAkQgDARgEAOg");
	this.shape_6.setTransform(27.4,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgggeIhcACIABgxIBcgDIACh1IA0gEIgBB4IBogDIgFAyIhkACIgCDpIg3ABg");
	this.shape_7.setTransform(-3.8,1.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_8.setTransform(-47.2,7.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgqCQIhekOIA2gIIBQDlIBUjvIA3AIIhpEZg");
	this.shape_9.setTransform(-78.6,6.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("Ah5BCIgChCIgBg0IgBgnIgDg4IA9gBIABAhIAAAfIABAgQAHgMAMgPQALgOAOgOQAOgNAQgJQAQgKAUgDQAUAAASAJQAIAFAIAHQAGAHAHAMQAGALAEAPQAFAPABAVIg1AUQAAgNgBgKQgCgKgEgGIgGgMIgIgIQgKgGgMAAQgHABgIAFQgJAGgIAIQgJAIgIAKIgSAVIgQAWIgNATIABAnIABAkIABAfIABAVIg5AIIgEhUg");
	this.shape_10.setTransform(-107.3,7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_11.setTransform(-139.8,7.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgmCXIgXgEIgYgJQgNgFgMgGQgMgHgLgJIAagpIAeAPQAPAIAQAFQAQAFARADQAQAEASAAQAPAAAKgCQAKgEAHgDQAGgFAEgFIAFgKQABgEAAgFQAAgGgDgGQgCgEgGgGQgFgFgIgEQgJgFgMgCQgNgCgRgBQgXgBgWgFQgWgDgSgJQgRgIgMgMQgLgOgDgUQgBgUAEgPQAEgQAKgNQAIgMANgKQAOgJAPgHQAQgGARgDQARgDARgBIAYABIAeAFQAQADAPAHQAQAFAOAKIgTAxQgRgJgQgGIgcgHQgPgEgMgBQgpgDgYAMQgXALAAAXQAAAQAJAHQAIAIAQADQAPADAUABQAUACAXADQAaAEASAIQASAHALALQALAKAEAOQAFAMAAAPQAAAZgKARQgKARgRAMQgSAKgWAFQgXAGgYAAQgXABgZgGg");
	this.shape_12.setTransform(-171.6,7.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgYDMQgPgDgSgIQgSgJgRgPIAAAjIgyABIgEmcIA6gCIgBCeQAPgPARgIQARgIAOgEQAQgEAPgBQASAAASAGQASAEAQAKQAQAKAOANQANAOAKAQQAKAQAFASQAFATgBAVQAAAXgHAUQgGAVgKARQgKARgNANQgNAOgPAKQgQAJgQAFQgQAFgQAAQgQgBgRgFgAgfgiQgOAFgMAJQgMAKgIAKQgJAMgFAOIAABAQAFARAJANQAIAMAMAJQAMAJANAEQAOAEAOAAQASABARgIQARgHANgNQANgNAHgTQAIgRABgVQABgUgGgSQgGgTgMgOQgNgNgRgJQgRgIgUAAQgQABgPAFg");
	this.shape_13.setTransform(-203.5,0.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("Ag1DIQgagIgXgNQgWgOgSgTQgTgTgNgXQgNgXgIgaQgGgagBgdQABgcAGgaQAIgbANgXQANgXATgSQASgTAWgOQAXgNAagIQAagHAbAAQAbAAAbAHQAaAIAWANQAWAOATATQATASANAXQANAXAHAbQAIAagBAcQABAdgIAaQgHAagNAXQgNAXgTATQgTATgWAOQgWANgaAIQgbAHgbAAQgcAAgZgHgAg6iNQgaANgVAUQgUAVgLAcQgMAcgBAfQABAgAMAcQALAcAUAVQAVAVAaAMQAbAMAfAAQAVAAASgFQATgGARgKQARgKAOgOQANgOAKgRQAJgRAGgUQAGgUgBgVQABgUgGgUQgGgTgJgSQgKgRgNgOQgOgOgRgKQgRgKgTgFQgSgGgVAAQgfAAgbAMg");
	this.shape_14.setTransform(-244.5,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(3,1,1).p("EA0RgFbMhogAAAQiQAAhnBlQhmBnAACPQAACQBmBmQBnBmCQAAMBogAAAQCQAABlhmQBnhmAAiQQAAiPhnhnQhlhliQAAg");
	this.shape_15.setTransform(0,0.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFCC00").s().p("Eg0PAFcQiQAAhmhmQhnhmAAiQQAAiPBnhnQBmhlCQAAMBofAAAQCQAABmBlQBmBnABCPQgBCQhmBmQhmBmiQAAg");
	this.shape_16.setTransform(0,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-370.7,-42.9,741.5,86);


(lib.Tween31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape.setTransform(251,7.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("Ag6DfQgMgDgMgGQgNgFgMgJQgMgJgKgMQgKgMgGgRIAzgWQAFALAIAJQAHAIAIAGQAJAFAIAEQAJADAIACQATAEAWgDQASgDAOgIQANgIAKgJQAKgKAGgKQAGgLAEgKIAEgRIACgMIAAgjQgRAPgRAIQgRAIgPAEQgRAFgPAAQggAAgbgLQgagLgUgUQgUgUgLgcQgMgdAAgkQAAgkANgdQAMgcAVgUQAVgTAZgLQAagKAaAAQASACASAGQAQAFASAJQASAKAQAQIABg1IA0ABIgCEzQAAARgFASQgDARgJAQQgJAQgMAOQgNAPgQALQgQALgUAHQgUAHgYACIgJAAQgbAAgYgHgAgoiiQgQAIgMANQgMAOgGATQgHATABAWQgBAWAHATQAHASAMANQAMANAQAHQAQAIAVAAQAQAAAQgGQARgHAOgLQANgKAJgPQAKgQAEgSIAAgjQgEgTgJgQQgKgPgNgMQgPgLgRgGQgQgGgSAAQgSAAgRAIg");
	this.shape_1.setTransform(215.5,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("ABTCZIAEgmQghAVgfAJQgfAJgcAAQgTAAgRgEQgTgFgOgLQgNgKgJgQQgIgQAAgWQAAgVAHgRQAFgRAMgOQALgMAQgJQAPgJARgGQASgGATgDQASgDAUAAIAgABIAbAEQgDgNgGgNQgEgNgIgLQgIgKgLgGQgMgHgQAAQgKAAgNADQgNADgQAIQgRAIgSAPQgUANgWAWIgfgnQAZgYAXgPQAYgQAVgIQAUgJASgDQARgDAPAAQAYAAAUAIQASAJAQAOQAOAPAKAUQAKATAGAXQAGAWADAZQADAXABAYQgBAagDAdQgDAegGAigAgMgBQgXAFgRALQgQAKgHAQQgJAPAFAUQADAQAMAHQAKAGAQAAQAPAAASgFQARgFASgIIAhgQIAbgQIABgaQAAgNgCgPQgMgDgPgCQgPgCgNAAQgZAAgVAFg");
	this.shape_2.setTransform(180.6,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("ACBBAIgBg6IgBgrQgBgWgFgLQgHgKgKAAQgHAAgHAEQgHAEgGAGQgHAHgHAIIgMARIgLARIgKANIABAZIACAhIAAAoIAAAyIgzADIgBhTIgCg6IgCgrQgCgWgFgLQgHgKgKAAQgHAAgIAEQgGAFgIAHIgPAQIgMATIgNASIgKAOIACCNIg0ADIgIkfIA4gGIABBOIASgWQAKgLALgKQALgJANgGQANgGAQgBQALAAALAEQAKAEAIAIQAIAHAGAMQAGALABAQIASgVQAJgLALgJQALgJANgFQAMgHAPAAQAMAAAMAEQAKAFAKAIQAIAJAFANQAGANAAASQACAUABAdIADBAIABBeIg4ADIAAhTg");
	this.shape_3.setTransform(142,6.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AghDJIABhEIAChUIABhyIA3gCIgCBFIgBA6IgBAvIgBAkIgBA6gAgOh/QgHgDgFgFQgFgFgDgHQgDgHAAgIQAAgIADgGQADgHAFgGQAFgFAHgDQAHgDAHAAQAIAAAHADQAGADAGAFQAFAGADAHQADAGAAAIQAAAIgDAHQgDAHgFAFQgGAFgGADQgHADgIAAQgHAAgHgDg");
	this.shape_4.setTransform(113.4,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_5.setTransform(61.5,7.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("ABIDMQAHgaAEgYQADgZABgTQACgWgBgVQgBgcgJgVQgIgTgNgMQgMgNgPgGQgPgGgPAAQgOACgPAHQgOAHgPANQgPANgNAYIgCCyIg0ABIgFmcIA+gCIgCCjQAPgQAQgJQARgKAOgFQARgFAPgCQAeAAAZALQAYALASAUQASAUAKAdQAKAbACAkIgBAnIgBAnIgEAkQgDARgEAOg");
	this.shape_6.setTransform(27.4,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgggeIhcACIABgxIBcgDIACh1IA0gEIgBB4IBogDIgFAyIhkACIgCDpIg3ABg");
	this.shape_7.setTransform(-3.8,1.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_8.setTransform(-47.2,7.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgqCQIhekOIA2gIIBQDlIBUjvIA3AIIhpEZg");
	this.shape_9.setTransform(-78.6,6.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("Ah5BCIgChCIgBg0IgBgnIgDg4IA9gBIABAhIAAAfIABAgQAHgMAMgPQALgOAOgOQAOgNAQgJQAQgKAUgDQAUAAASAJQAIAFAIAHQAGAHAHAMQAGALAEAPQAFAPABAVIg1AUQAAgNgBgKQgCgKgEgGIgGgMIgIgIQgKgGgMAAQgHABgIAFQgJAGgIAIQgJAIgIAKIgSAVIgQAWIgNATIABAnIABAkIABAfIABAVIg5AIIgEhUg");
	this.shape_10.setTransform(-107.3,7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_11.setTransform(-139.8,7.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgmCXIgXgEIgYgJQgNgFgMgGQgMgHgLgJIAagpIAeAPQAPAIAQAFQAQAFARADQAQAEASAAQAPAAAKgCQAKgEAHgDQAGgFAEgFIAFgKQABgEAAgFQAAgGgDgGQgCgEgGgGQgFgFgIgEQgJgFgMgCQgNgCgRgBQgXgBgWgFQgWgDgSgJQgRgIgMgMQgLgOgDgUQgBgUAEgPQAEgQAKgNQAIgMANgKQAOgJAPgHQAQgGARgDQARgDARgBIAYABIAeAFQAQADAPAHQAQAFAOAKIgTAxQgRgJgQgGIgcgHQgPgEgMgBQgpgDgYAMQgXALAAAXQAAAQAJAHQAIAIAQADQAPADAUABQAUACAXADQAaAEASAIQASAHALALQALAKAEAOQAFAMAAAPQAAAZgKARQgKARgRAMQgSAKgWAFQgXAGgYAAQgXABgZgGg");
	this.shape_12.setTransform(-171.6,7.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgYDMQgPgDgSgIQgSgJgRgPIAAAjIgyABIgEmcIA6gCIgBCeQAPgPARgIQARgIAOgEQAQgEAPgBQASAAASAGQASAEAQAKQAQAKAOANQANAOAKAQQAKAQAFASQAFATgBAVQAAAXgHAUQgGAVgKARQgKARgNANQgNAOgPAKQgQAJgQAFQgQAFgQAAQgQgBgRgFgAgfgiQgOAFgMAJQgMAKgIAKQgJAMgFAOIAABAQAFARAJANQAIAMAMAJQAMAJANAEQAOAEAOAAQASABARgIQARgHANgNQANgNAHgTQAIgRABgVQABgUgGgSQgGgTgMgOQgNgNgRgJQgRgIgUAAQgQABgPAFg");
	this.shape_13.setTransform(-203.5,0.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("Ag1DIQgagIgXgNQgWgOgSgTQgTgTgNgXQgNgXgIgaQgGgagBgdQABgcAGgaQAIgbANgXQANgXATgSQASgTAWgOQAXgNAagIQAagHAbAAQAbAAAbAHQAaAIAWANQAWAOATATQATASANAXQANAXAHAbQAIAagBAcQABAdgIAaQgHAagNAXQgNAXgTATQgTATgWAOQgWANgaAIQgbAHgbAAQgcAAgZgHgAg6iNQgaANgVAUQgUAVgLAcQgMAcgBAfQABAgAMAcQALAcAUAVQAVAVAaAMQAbAMAfAAQAVAAASgFQATgGARgKQARgKAOgOQANgOAKgRQAJgRAGgUQAGgUgBgVQABgUgGgUQgGgTgJgSQgKgRgNgOQgOgOgRgKQgRgKgTgFQgSgGgVAAQgfAAgbAMg");
	this.shape_14.setTransform(-244.5,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(3,1,1).p("EA0RgFbMhogAAAQiQAAhnBlQhmBnAACPQAACQBmBmQBnBmCQAAMBogAAAQCQAABlhmQBnhmAAiQQAAiPhnhnQhlhliQAAg");
	this.shape_15.setTransform(0,0.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFCC00").s().p("Eg0PAFcQiQAAhmhmQhnhmAAiQQAAiPBnhnQBmhlCQAAMBofAAAQCQAABmBlQBmBnABCPQgBCQhmBmQhmBmiQAAg");
	this.shape_16.setTransform(0,0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-370.7,-42.9,741.5,86);


(lib.Tween30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape.setTransform(250,7.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#330000").s().p("Ag6DfQgMgDgMgGQgNgFgMgJQgMgJgKgMQgKgMgGgRIAzgWQAGALAHAJQAHAIAIAGQAJAFAIAEQAJADAIACQATAEAWgDQASgDAOgIQANgIAKgJQAKgKAGgKQAGgLAEgKIAEgRIABgMIAAgjQgPAPgSAIQgRAIgPAEQgRAFgQAAQgfAAgbgLQgagLgUgUQgUgUgLgcQgMgdAAgkQAAgkANgdQAMgcAVgUQAUgTAagLQAagKAaAAQARACATAGQAQAFASAJQASAKAQAQIABg1IA0ABIgCEzQAAARgEASQgEARgJAQQgJAQgMAOQgNAPgPALQgRALgUAHQgUAHgYACIgJAAQgbAAgYgHgAgoiiQgQAIgMANQgMAOgGATQgHATAAAWQAAAWAHATQAHASAMANQAMANAQAHQARAIATAAQARAAAQgGQARgHAOgLQANgKAJgPQAKgQAEgSIAAgjQgEgTgJgQQgKgPgNgMQgPgLgRgGQgQgGgSAAQgSAAgRAIg");
	this.shape_1.setTransform(214.5,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#330000").s().p("ABTCZIAEgmQghAVgfAJQgfAJgcAAQgSAAgSgEQgSgFgPgLQgOgKgHgQQgJgQAAgWQAAgVAHgRQAFgRAMgOQALgMAQgJQAPgJARgGQASgGATgDQASgDAUAAIAgABIAbAEQgDgNgGgNQgEgNgIgLQgIgKgLgGQgMgHgQAAQgLAAgMADQgOADgPAIQgRAIgSAPQgUANgWAWIgfgnQAZgYAXgPQAYgQAVgIQAUgJASgDQARgDAPAAQAYAAAUAIQASAJAQAOQAOAPAKAUQAKATAGAXQAGAWADAZQADAXAAAYQAAAagDAdQgDAegGAigAgMgBQgXAFgRALQgQAKgHAQQgJAPAFAUQADAQAMAHQAKAGAQAAQAPAAASgFQARgFASgIIAhgQIAbgQIABgaQABgNgCgPQgNgDgPgCQgPgCgNAAQgZAAgVAFg");
	this.shape_2.setTransform(179.6,6.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#330000").s().p("ACABAIAAg6IgBgrQgBgWgFgLQgHgKgKAAQgHAAgHAEQgGAEgHAGQgHAHgHAIIgMARIgLARIgKANIABAZIACAhIAAAoIAAAyIgzADIgBhTIgCg6IgDgrQgBgWgFgLQgHgKgKAAQgHAAgIAEQgGAFgIAHIgPAQIgMATIgNASIgKAOIACCNIg1ADIgHkfIA4gGIABBOIATgWQAJgLALgKQALgJANgGQANgGAQgBQALAAALAEQAKAEAIAIQAIAHAGAMQAGALABAQIASgVQAJgLALgJQALgJANgFQAMgHAQAAQALAAAMAEQAKAFAKAIQAIAJAFANQAGANAAASQACAUABAdIADBAIABBeIg4ADIgBhTg");
	this.shape_3.setTransform(141,6.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#330000").s().p("AghDJIABhEIAChUIABhyIA3gCIgCBFIgBA6IgBAvIgBAkIgBA6gAgOh/QgHgDgFgFQgFgFgDgHQgDgHAAgIQAAgIADgGQADgHAFgGQAFgFAHgDQAHgDAHAAQAIAAAHADQAGADAGAFQAFAGADAHQADAGAAAIQAAAIgDAHQgDAHgFAFQgGAFgGADQgHADgIAAQgHAAgHgDg");
	this.shape_4.setTransform(112.4,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_5.setTransform(60.5,7.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#330000").s().p("ABIDMQAHgaAEgYQADgZABgTQACgWgBgVQgBgcgJgVQgIgTgNgMQgMgNgPgGQgPgGgPAAQgOACgPAHQgOAHgPANQgPANgNAYIgCCyIg0ABIgFmcIA+gCIgCCjQAPgQAQgJQARgKAOgFQARgFAPgCQAeAAAZALQAYALASAUQASAUAKAdQAKAbACAkIgBAnIgBAnIgEAkQgDARgEAOg");
	this.shape_6.setTransform(26.4,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AgggeIhcACIABgxIBcgDIACh1IA0gEIgBB4IBogDIgFAyIhkACIgCDpIg4ABg");
	this.shape_7.setTransform(-4.8,1.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_8.setTransform(-48.2,7.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#330000").s().p("AgqCQIhekOIA2gIIBQDlIBUjvIA3AIIhpEZg");
	this.shape_9.setTransform(-79.6,6.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#330000").s().p("Ah5BCIgChCIgBg0IgBgnIgDg4IA9gBIABAhIAAAfIABAgQAHgMAMgPQALgOAOgOQAOgNAQgJQAQgKAUgDQAVAAASAJQAHAFAIAHQAGAHAHAMQAGALAEAPQAFAPACAVIg2AUQAAgNgBgKQgDgKgDgGIgHgMIgHgIQgKgGgMAAQgHABgJAFQgIAGgIAIQgJAIgIAKIgSAVIgQAWIgNATIAAAnIACAkIABAfIABAVIg6AIIgDhUg");
	this.shape_10.setTransform(-108.3,7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#330000").s().p("Ag8CQQgbgLgTgTQgSgUgKgcQgKgbAAgiQAAghAMgcQALgeAUgVQATgVAbgNQAbgMAeAAQAcAAAYALQAZALATATQATATAMAaQANAaADAeIjnAiQACATAIAPQAHAPAMAKQALALAQAFQAPAGARAAQAOAAAOgFQAOgEAMgJQAMgIAIgNQAJgMAEgRIA0ALQgIAYgNAUQgOAUgSAOQgTAOgVAIQgWAHgYAAQgiAAgcgKgAgWhnQgNAEgNAJQgMAJgKAQQgKAQgEAXICggUIgCgFQgKgagSgPQgRgPgbAAQgKAAgOAEg");
	this.shape_11.setTransform(-140.8,7.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#330000").s().p("AgmCXIgXgEIgYgJQgNgFgMgGQgMgHgLgJIAagpIAeAPQAPAIAQAFQAQAFARADQAQAEASAAQAPAAAKgCQAKgEAHgDQAGgFAEgFIAFgKQABgEAAgFQAAgGgDgGQgCgEgGgGQgFgFgIgEQgJgFgMgCQgNgCgRgBQgXgBgWgFQgWgDgSgJQgRgIgMgMQgLgOgDgUQgBgUAEgPQAEgQAKgNQAIgMANgKQAOgJAPgHQAQgGARgDQARgDARgBIAYABIAeAFQAQADAPAHQAQAFAOAKIgTAxQgRgJgQgGIgcgHQgPgEgMgBQgpgDgYAMQgXALAAAXQAAAQAJAHQAIAIAQADQAPADAUABQAUACAXADQAaAEASAIQASAHALALQALAKAEAOQAFAMAAAPQAAAZgKARQgKARgRAMQgSAKgWAFQgXAGgYAAQgXABgZgGg");
	this.shape_12.setTransform(-172.6,7.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#330000").s().p("AgYDMQgPgDgSgIQgSgJgRgPIAAAjIgyABIgEmcIA6gCIgBCeQAPgPARgIQARgIAOgEQAQgEAPgBQASAAASAGQASAEAQAKQAQAKAOANQANAOAKAQQAKAQAFASQAFATgBAVQAAAXgHAUQgGAVgKARQgKARgNANQgNAOgPAKQgQAJgQAFQgQAFgQAAQgQgBgRgFgAgfgiQgOAFgMAJQgMAKgIAKQgJAMgFAOIAABAQAFARAJANQAIAMAMAJQAMAJANAEQAOAEAOAAQASABARgIQARgHANgNQANgNAHgTQAIgRABgVQABgUgGgSQgGgTgMgOQgNgNgRgJQgRgIgUAAQgQABgPAFg");
	this.shape_13.setTransform(-204.5,0.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#330000").s().p("Ag1DIQgagIgXgNQgWgOgSgTQgTgTgNgXQgNgXgIgaQgHgaAAgdQAAgcAHgaQAIgbANgXQANgXATgSQASgTAWgOQAXgNAagIQAagHAbAAQAbAAAbAHQAaAIAWANQAWAOATATQASASAOAXQANAXAHAbQAHAaAAAcQAAAdgHAaQgHAagNAXQgOAXgSATQgTATgWAOQgWANgaAIQgbAHgbAAQgcAAgZgHgAg6iNQgbANgUAUQgUAVgMAcQgLAcgBAfQABAgALAcQAMAcAUAVQAUAVAbAMQAcAMAeAAQAVAAASgFQAUgGAQgKQARgKANgOQAOgOAKgRQAJgRAGgUQAGgUgBgVQABgUgGgUQgGgTgJgSQgKgRgOgOQgNgOgRgKQgQgKgUgFQgSgGgVAAQgeAAgcAMg");
	this.shape_14.setTransform(-245.5,1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(3,1,1).p("EgnzgEmMBPnAAAQCgAAAABuIAAFxQAABuigAAMhPnAAAQigAAAAhuIAAlxQAAhuCgAAg");
	this.shape_15.setTransform(1,6.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("EgnzAEmQigAAAAhuIAAlwQAAhuCgABMBPnAAAQCggBAABuIAAFwQAABuigAAg");
	this.shape_16.setTransform(1,6.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-271.7,-42.9,545.1,86);


(lib.Tween29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0000CC").s().p("AgwB1QgWgJgQgQQgPgQgIgWQgIgXAAgcQAAgaAKgXQAJgYAQgRQAQgSAWgKQAWgKAYAAQAWAAAUAJQAUAJAQAQQAPAPAKAWQAKAVADAYIi7AbQABAQAGAMQAGAMAKAIQAJAJANAFQAMAEAOAAQALAAAMgDQALgEAKgHQAJgHAHgKQAHgKADgOIAqAIQgGAVgLAPQgLAQgPAMQgOAMgSAGQgSAGgTABQgcAAgWgJgAgRhUQgLADgKAIQgLAIgIAMQgIANgDATICCgQIgBgEQgJgWgOgMQgPgMgVAAQgIAAgLADg");
	this.shape.setTransform(90.6,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0000CC").s().p("AhlA2IgCg2IgCgpIgBggIgBguIAygBIABA/QAHgNAJgLQAJgLALgIQAKgJAMgFQALgFAOgBQAQgBANAEQAMAEAJAHQAIAHAGAJQAGAKADAKQADAKACAKIABASQACAlgBAlIgBBPIgtgCIADhGQACgjgCgiIgBgKIgCgOIgFgNQgEgHgFgFQgFgGgHgCQgIgDgKACQgRACgRAXQgSAWgUArIABA4IABAfIABARIgvAGIgChEg");
	this.shape_1.setTransform(63.4,5.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0000CC").s().p("AgtB3QgVgJgQgRQgPgRgKgYQgIgXgBgdQABgcAIgXQAKgYAPgRQAQgQAVgKQAWgJAXAAQAYAAAVAKQAVAKARARQAQARAIAXQAJAYAAAaQAAAcgJAXQgIAXgQARQgRASgVAJQgVAKgYAAQgXAAgWgJgAgfhQQgNAIgJAMQgJANgEAQQgDAQAAAPQAAAPAEAQQAEAQAJANQAJANANAIQANAIARAAQASAAAOgIQANgIAIgNQAJgNAFgQQADgQAAgPQAAgPgDgQQgEgQgJgNQgIgMgOgIQgNgIgTAAQgSAAgNAIg");
	this.shape_2.setTransform(36.4,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0000CC").s().p("AgZgYIhMABIABgoIBLgCIAChfIAqgDIgCBhIBVgDIgDApIhSACIgCC9IgtABg");
	this.shape_3.setTransform(0.9,1.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0000CC").s().p("AA6ClQAHgUACgUIAEgkIABgjQgBgXgHgQQgHgPgKgKQgKgLgNgFQgMgEgMAAQgLABgNAFQgKAGgNALQgMAKgLAUIgBCQIgrABIgElPIAygBIgBCEQAMgNAOgHQANgIAMgEQANgEANgCQAYAAAUAJQAUAJAOAQQAPARAIAXQAIAWACAeIgBAfIgBAgIgDAdQgDAOgDALg");
	this.shape_4.setTransform(-24.1,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0000CC").s().p("AgvC2QgKgDgKgEQgLgFgJgHQgKgHgIgKQgIgKgFgOIAqgSQAEAKAGAGQAGAHAHAFIANAHIAOAEQAQAEARgDQAPgDAMgGQAKgGAIgIQAIgIAFgIQAFgJACgIIAEgOIACgKIAAgcQgNAMgOAHQgOAGgMADQgOAEgNABQgaAAgVgJQgWgJgQgQQgQgRgJgWQgKgYAAgdQABgeAJgXQALgXAQgQQARgQAWgIQAUgJAVAAQAOACAQAFQAMAEAPAHQAPAIANANIAAgrIArABIgCD5QAAAOgEAOQgDAOgGAOQgIANgKALQgKAMgNAJQgOAJgQAGQgQAGgUABIgCAAQgYAAgWgFgAghiEQgNAHgJALQgJALgGAQQgFAPAAASQAAASAFAPQAFAPALAKQAJALANAGQAOAGAQAAQANAAAOgFQAOgFAKgJQALgIAIgNQAHgMAEgPIAAgdQgDgPgIgNQgIgNgLgJQgMgJgNgFQgOgFgOAAQgPAAgOAGg");
	this.shape_5.setTransform(-54.5,10.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0000CC").s().p("AgbCkIABg4IABhEIAChdIAsgCIgCA4IgBAwIAAAmIgBAdIgBAwgAgLhnQgFgCgFgFQgEgEgDgFQgCgGAAgGQAAgHACgGQADgFAEgEQAFgFAFgCQAGgDAFAAQAGAAAGADQAFACAFAFQAEAEACAFQADAGAAAHQAAAGgDAGQgCAFgEAEQgFAFgFACQgGACgGAAQgFAAgGgCg");
	this.shape_6.setTransform(-73.6,0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0000CC").s().p("AhiA2IgCg2IgBgpIgBghIgBgtIAxgBIABAbIAAAZIAAAbQAGgKAKgNQAIgMAMgKQALgLANgHQANgIAQgDQARgBAPAJQAGADAGAGQAGAGAFAJQAFAJADAMQAEANABARIgrAQQAAgKgBgJIgEgNIgGgJIgHgHQgHgFgKABQgGAAgHAEQgHAEgGAHQgHAHgHAIIgOASIgNARIgLAPIABAgIAAAdIABAZIABARIguAHIgDhEg");
	this.shape_7.setTransform(-90.5,5.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(3,1,1).p("AuNkGIcbAAQCgAAAACgIAADNQAACgigAAI8bAAQigAAAAigIAAjNQAAigCgAAg");
	this.shape_8.setTransform(0,4.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AuNEHQifAAAAigIAAjNQAAigCfAAIcbAAQCfAAAACgIAADNQAACgifAAg");
	this.shape_9.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.4,-34.9,216.9,69.9);


(lib.Tween20copy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhNIhQgVgJgQgPQgPgOgJgWQgJgUAAgYQAAgXAJgVQAJgVAPgPQAQgPAVgJQAVgJAYAAQAXAAAUAJQAVAJAPAPQAPAPAJAVQAJAVAAAXQAAAYgJAUQgJAWgPAOQgPAPgVAJQgUAJgXAAQgYAAgVgJgAhaChIgNh6IAAgHIAAgHQAAggAQgYQARgZAZgVQAYgVAbgVQAdgTAZgZQAZgXAQgeQARgeAAgoQAAgbgLgWQgKgVgSgPQgSgQgZgIQgZgJgcAAQgrAAgdAKQgeAKgVALQgUALgOAKQgPAJgLAAQgbAAgNgWIgwhNQAagXAegVQAfgTAjgQQAjgOApgJQAqgJAvAAQBAAAA2ASQA2ASAmAiQAmAhAVAvQAVAwAAA6QAAA5gQAqQgRApgZAfQgZAfgdAXIg4AqQgaAUgTASQgTATgEAYIgRBug");
	this.shape.setTransform(0.1,3.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.6,-93,69.3,186);


(lib.Tween20copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhNIhQgVgJgQgPQgPgOgJgWQgJgUAAgYQAAgXAJgVQAJgVAPgPQAQgPAVgJQAVgJAYAAQAXAAAUAJQAVAJAPAPQAPAPAJAVQAJAVAAAXQAAAYgJAUQgJAWgPAOQgPAPgVAJQgUAJgXAAQgYAAgVgJgAhaChIgNh6IAAgHIAAgHQAAggAQgYQARgZAZgVQAYgVAbgVQAdgTAZgZQAZgXAQgeQARgeAAgoQAAgbgLgWQgKgVgSgPQgSgQgZgIQgZgJgcAAQgrAAgdAKQgeAKgVALQgUALgOAKQgPAJgLAAQgbAAgNgWIgwhNQAagXAegVQAfgTAjgQQAjgOApgJQAqgJAvAAQBAAAA2ASQA2ASAmAiQAmAhAVAvQAVAwAAA6QAAA5gQAqQgRApgZAfQgZAfgdAXIg4AqQgaAUgTASQgTATgEAYIgRBug");
	this.shape.setTransform(0.1,3.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.6,-93,69.3,186);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhNIhQgVgJgQgPQgPgOgJgWQgJgUAAgYQAAgXAJgVQAJgVAPgPQAQgPAVgJQAVgJAYAAQAXAAAUAJQAVAJAPAPQAPAPAJAVQAJAVAAAXQAAAYgJAUQgJAWgPAOQgPAPgVAJQgUAJgXAAQgYAAgVgJgAhaChIgNh6IAAgHIAAgHQAAggAQgYQARgZAZgVQAYgVAbgVQAdgTAZgZQAZgXAQgeQARgeAAgoQAAgbgLgWQgKgVgSgPQgSgQgZgIQgZgJgcAAQgrAAgdAKQgeAKgVALQgUALgOAKQgPAJgLAAQgbAAgNgWIgwhNQAagXAegVQAfgTAjgQQAjgOApgJQAqgJAvAAQBAAAA2ASQA2ASAmAiQAmAhAVAvQAVAwAAA6QAAA5gQAqQgRApgZAfQgZAfgdAXIg4AqQgaAUgTASQgTATgEAYIgRBug");
	this.shape.setTransform(0.1,3.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.6,-93,69.3,186);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0066CC").ss(7,1,1).p("A21ABQAAJcGtGsQGqGtJeAAQJeAAGsmtQGsmsAApcQAApfmsmtQmsmqpeAAQpeAAmqGqQmtGtAAJfg");
	this.shape.setTransform(1,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvVPVQmXmWAAo/QAAo/GXmYQGVmVJAAAQJAAAGWGVQGXGYAAI/QAAI/mXGWQmWGYpAAAQpAAAmVmYg");
	this.shape_1.setTransform(1.1,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CCCCCC").s().p("AwIQJQmtmtAApcQAApeGtmtQGqmqJeAAQJeAAGsGqQGsGtAAJeQAAJcmsGtQmsGtpeAAQpeAAmqmtgAvUvXQmYGYAAI/QAAI/GYGWQGUGYJAAAQJAAAGXmYQGWmWAAo/QAAo/mWmYQmXmVpAAAQpAAAmUGVg");
	this.shape_2.setTransform(1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148.7,-150.7,299.5,299.4);


(lib.Option1_mccopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AidC4QhBhvAAiaIAAgQQhbgkhchAQgSgOgDgVQgEgVAOgTQALgTAWgCQAVgEATANQB9BXB7AcQBlAWBngWQB1gcB0hXQARgNAXAEQAVACANASQAOARgDAXQgEAVgRANQhcBEhcAmIAAAMQAACahBBvQhCBvhbAAQhbAAhChvg");
	this.shape.setTransform(-3.9,37.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_1.setTransform(-1.9,-29,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6600").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FA883B").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy3, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AidC4QhBhvAAiaIAAgQQhbgkhchAQgSgOgDgVQgEgVAOgTQALgTAWgCQAVgEATANQB9BXB7AcQBlAWBngWQB1gcB0hXQARgNAXAEQAVACANASQAOARgDAXQgEAVgRANQhcBEhcAmIAAAMQAACahBBvQhCBvhbAAQhbAAhChvg");
	this.shape.setTransform(-3.9,37.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_1.setTransform(-1.9,-29,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6600").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FA883B").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mc, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween20copy9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhNIhQgVgJgQgPQgPgOgJgWQgJgUAAgYQAAgXAJgVQAJgVAPgPQAQgPAVgJQAVgJAYAAQAXAAAUAJQAVAJAPAPQAPAPAJAVQAJAVAAAXQAAAYgJAUQgJAWgPAOQgPAPgVAJQgUAJgXAAQgYAAgVgJgAhaChIgNh6IAAgHIAAgHQAAggAQgYQARgZAZgVQAYgVAbgVQAdgTAZgZQAZgXAQgeQARgeAAgoQAAgbgLgWQgKgVgSgPQgSgQgZgIQgZgJgcAAQgrAAgdAKQgeAKgVALQgUALgOAKQgPAJgLAAQgbAAgNgWIgwhNQAagXAegVQAfgTAjgQQAjgOApgJQAqgJAvAAQBAAAA2ASQA2ASAmAiQAmAhAVAvQAVAwAAA6QAAA5gQAqQgRApgZAfQgZAfgdAXIg4AqQgaAUgTASQgTATgEAYIgRBug");
	this.shape_1.setTransform(0.1,3.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-34.6,-93,69.3,186);


(lib.Tween14_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#0066CC").ss(7,1,1).p("A21ABQAAJcGtGsQGqGtJeAAQJeAAGsmtQGsmsAApcQAApfmsmtQmsmqpeAAQpeAAmqGqQmtGtAAJfg");
	this.shape_3.setTransform(1,-1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AvVPVQmXmWAAo/QAAo/GXmYQGVmVJAAAQJAAAGWGVQGXGYAAI/QAAI/mXGWQmWGYpAAAQpAAAmVmYg");
	this.shape_4.setTransform(1.1,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCCCCC").s().p("AwIQJQmtmtAApcQAApeGtmtQGqmqJeAAQJeAAGsGqQGsGtAAJeQAAJcmsGtQmsGtpeAAQpeAAmqmtgAvUvXQmYGYAAI/QAAI/GYGWQGUGYJAAAQJAAAGXmYQGWmWAAo/QAAo/mWmYQmXmVpAAAQpAAAmUGVg");
	this.shape_5.setTransform(1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-148.7,-150.7,299.5,299.4);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Option1_mccopy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ag6BFQgYgpAAg6IAAgGQgigNgjgYQgHgFgBgIQgCgIAFgHQAFgHAIgBQAIgCAHAFQAvAhAuAKQAlAJAngJQAsgKAsghQAGgFAIACQAIABAGAGQAEAHgBAIQgBAIgGAFQgjAagiAOIAAAEQAAA6gZApQgYAqgjAAQghAAgZgqg");
	this.shape.setTransform(-4,37.8,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_1.setTransform(-1.9,-29,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6600").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FA883B").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy7, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("Ag6BFQgYgpAAg6IAAgGQgigNgjgYQgHgFgBgIQgCgIAFgHQAFgHAIgBQAIgCAHAFQAvAhAuAKQAlAJAngJQAsgKAsghQAGgFAIACQAIABAGAGQAEAHgBAIQgBAIgGAFQgjAagiAOIAAAEQAAA6gZApQgYAqgjAAQghAAgZgqg");
	this.shape.setTransform(-4,37.8,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_1.setTransform(-1.9,-29,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6600").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FA883B").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy5, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("ACiBLQgHgDgEgIQgMgdgYgYQgwgvhDAAQhDAAgwAvQgYAYgLAdQgDAIgIADQgIADgHgDQgIgDgDgIQgDgHADgIQAOgkAegdQA7g8BUAAQBUAAA8A8QAdAdAPAkQADAHgDAIQgDAIgHADQgEACgEAAQgEAAgEgCg");
	this.shape.setTransform(-1.8,35.2,2.664,2.664);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("ACFA4IgBgBQgNgNAAgTQAAgUAOgMQAOgOATAAQATAAAOAOQAOAMAAAUQAAATgOANIAAABQgOANgTAAQgTAAgOgNgAi+A4IgBgBQgNgNAAgTQAAgUAOgMQAOgOATAAQATAAAOAOQAOAMAAAUQAAATgOANIAAABQgOANgTAAQgTAAgOgNgABaghIAAgjICcABIAAAjgAj1ghIAAgjICcABIAAAjg");
	this.shape_1.setTransform(-1.8,-12.4,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_2.setTransform(-6.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FCD845").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_3.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_5
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_4.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy4, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.Option1_mccopy3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("AAACxQgeAAgZgSIAAAAQgUgNgQgYQgigygBhIQABhHAigyIAAAAQAmg4A1ACQA2gBAlA3QAkAygBBHQABBIgkAyIAAAAQgQAYgTANQgZASgfAAIAAAAg");
	this.shape_5.setTransform(-1.9,35.7,1.332,1.332);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333333").s().p("AB9AkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPgAjGAkQgPgOAAgWQAAgVAPgPQAQgOAVAAQAVAAAPAOQAQAPAAAVQAAAWgQAOQgPAPgVAAQgVAAgQgPg");
	this.shape_6.setTransform(-2,-20.8,2.664,2.664);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// Layer_3
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EE257D").s().p("Am/LpQhTg1hLhLQkOkPAAl9QAAl+EOkPQAegdAfgaQA0gjA5gZQCRhECogNQAlgDAmAAQBmABBfATQBvAXBkAyQA4AbA0AlQBHAwBBBCQEPEOAAF+QAAF9kPEPQgdAegfAaQjcCQkWAAQkUAAjaiPg");
	this.shape_7.setTransform(-6.7,0.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EC5698").s().p("AGJMbQEOkOAAl+QAAl+kOkOQhBhBhHgxQg1glg3gbQhlgyhugXQhegThnAAQgmAAglACQipANiRBEQg4Aag1AiQD8jWFVAAQDeAAC6BcQCDBCBwBwQEPEOAAF+QAAF+kPEOQhJBKhSA1QAfgaAegeg");
	this.shape_8.setTransform(14.5,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7}]}).wait(1));

	// Layer_5
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(255,255,255,0.02)").s().p("AoIMzQhWg4hPhOQikikhFjLQgyiVAAipQAAmREbkcQAggfAggbQBjhVBzg1QCYhHCwgOQAngCAoAAQBrAABkAUQB0AYBrA0QCIBFB3B2QEcEcAAGRQAAGRkcEcQhNBNhWA4QjmCXkkAAQkiAAjmiWg");
	this.shape_9.setTransform(-2,-2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(1));

}).prototype = getMCSymbolPrototype(lib.Option1_mccopy3_1, new cjs.Rectangle(-98.9,-99.6,193.8,193.9), null);


(lib.InstructionText_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionText
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBPQgPgHgKgKQgLgKgFgQQgGgPAAgTQAAgRAHgPQAGgRALgLQALgMAPgHQAOgGAQAAQAPAAAOAFQANAHAKAKQALALAHANQAGAPACAQIh+ASQABAKAFAJQAEAIAGAGQAHAFAIADQAIADAJAAQAIAAAHgCQAIgDAHgEQAGgEAFgIQAFgGACgKIAcAGQgEAOgHALQgIAKgKAIQgKAIgMAEQgMAFgNAAQgTgBgOgFgAgLg4QgIACgHAFQgGAFgGAJQgGAIgCANIBYgLIgBgDQgGgOgKgIQgKgJgOABQgFAAgHACg");
	this.shape.setTransform(145.6,-11.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhDAkIgCgkIgBgcIgBgVIgBgfIAigBIAAAqQAFgIAGgHQAHgIAHgGQAHgFAIgEQAHgDAJgBQAMgBAIADQAIADAGAEQAFAFAEAGQAFAHACAHIACANIACAMIAAAyIgBA1IgegBIACgvQABgYgBgXIAAgHIgCgIQgBgFgCgFQgDgEgDgEQgEgDgFgCQgEgCgHABQgMACgLAPQgMAPgOAcIABAmIABAVIAAAMIgfAEIgBgug");
	this.shape_1.setTransform(127.3,-11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeBQQgOgGgLgMQgKgLgHgQQgFgQgBgTQABgTAFgQQAHgPAKgMQALgLAOgGQAOgGAQAAQAQAAAPAGQAOAHAKALQALAMAGAPQAGAQABASQgBASgGAQQgGAQgLAMQgKALgOAHQgPAGgQAAQgQAAgOgGgAgUg2QgJAFgHAJQgFAJgDALQgCAKAAAKQAAAKACALQADAKAGAJQAGAJAJAFQAJAGALAAQAMAAAJgGQAJgFAGgJQAGgJACgKQADgLAAgKQAAgKgCgKQgDgLgGgJQgGgJgIgFQgKgGgMAAQgMAAgIAGg");
	this.shape_2.setTransform(109.1,-11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgCIgBBCIA5gCIgDAbIg3ACIgBB+IgeACg");
	this.shape_3.setTransform(85.2,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_4.setTransform(68.4,-14.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggB6IgNgFIgOgIQgGgFgFgGQgFgHgEgJIAcgMQADAGAEAFQADAEAGADIAJAFIAJADQAKACALgBQALgCAHgEQAIgFAFgFQAGgFADgGIAFgLIACgKIABgGIAAgTQgJAIgJAEQgKAFgHACQgKACgIABQgRAAgPgGQgPgGgLgLQgKgLgHgPQgFgQAAgUQAAgUAGgPQAHgQALgLQAMgKAOgGQAOgGAOAAIAUAFIASAIQAKAFAJAJIAAgdIAdAAIgBCoQgBAJgCAKQgCAJgFAJQgEAJgIAIQgGAIgJAGQgJAGgLAEQgLADgNACIgCAAQgQAAgPgEgAgWhZQgIAFgGAHQgHAIgEAKQgEALABAMQgBAMAEAKQAEAKAHAGQAGAHAJAEQAJAFALAAQAJAAAJgEQAIgDAIgGQAHgFAGgJQAEgIADgKIAAgTQgCgLgFgIQgGgJgHgGQgIgGgJgEQgJgDgJAAQgKAAgKAEg");
	this.shape_5.setTransform(47.9,-7.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgSBuIABglIABguIABg+IAdgBIAAAlIgBAgIgBAaIAAATIAAAggAgHhFIgGgEQgDgDgCgEQgCgEAAgEQAAgEACgEQACgEADgDQACgDAEgBQAEgCADAAQAEAAAEACQAEABADADIAFAHIABAIQAAAEgBAEIgFAHIgHAEIgIACQgDAAgEgCg");
	this.shape_6.setTransform(35,-14.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhBAkIgCgkIgBgcIAAgVIgBgfIAhgBIABASIAAARIAAASIAKgPQAGgIAIgHQAIgHAIgFQAJgGAKgBQAMgBAKAGIAIAGQAEAEADAGQAEAGACAJIADATIgdALIgBgMIgDgJIgDgHIgFgEQgFgDgGAAQgEAAgFADIgJAIIgJAKIgKALIgIAMIgIAKIABAVIAAAUIABARIABAMIggAEIgBgug");
	this.shape_7.setTransform(23.7,-11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AggBPQgPgHgKgKQgLgKgFgQQgGgPABgTQgBgRAHgPQAGgRALgLQALgMAOgHQAPgGAQAAQAPAAAOAFQANAHAKAKQALALAHANQAGAPACAQIh+ASQABAKAFAJQAEAIAGAGQAGAFAJADQAIADAJAAQAIAAAHgCQAIgDAHgEQAGgEAFgIQAFgGACgKIAcAGQgEAOgHALQgIAKgKAIQgKAIgMAEQgMAFgNAAQgSgBgPgFgAgMg4QgHACgHAFQgGAFgGAJQgGAIgCANIBYgLIgBgDQgGgOgKgIQgKgJgOABQgFAAgIACg");
	this.shape_8.setTransform(-1.9,-11.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAnBvQAFgOABgNIADgYIAAgXQAAgQgFgLQgFgKgGgHQgHgHgJgDQgIgDgIAAQgHABgJAEQgHADgIAIQgJAGgHAOIgBBhIgcAAIgDjhIAigBIgBBZQAIgJAJgFQAJgFAIgCQAJgDAIgBQARAAANAGQAOAFAJALQAKALAGAQQAFAPABAUIAAAVIgBAVIgDAUQgBAJgCAIg");
	this.shape_9.setTransform(-20.6,-14.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgCIgBBCIA5gCIgDAbIg3ACIgBB+IgeACg");
	this.shape_10.setTransform(-37.6,-14.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgRgQIgzABIABgbIAygCIAChAIAcgCIgBBCIA5gCIgDAbIg3ACIgBB+IgeACg");
	this.shape_11.setTransform(-59.7,-14.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgaBPQgQgGgMgMQgMgLgHgQQgHgPAAgTQAAgKADgKQADgLAFgJQAGgKAHgIQAIgIAKgGQAJgGALgDQAMgDALAAQAMAAAMADQALADAKAGQAKAGAIAIQAIAIAFALIAAAAIgYAPIAAgBQgEgHgGgGQgFgGgHgEQgHgEgHgDQgIgCgIAAQgLAAgLAFQgKAEgIAIQgIAIgEALQgFAKAAALQAAAMAFALQAEAKAIAIQAIAIAKAFQALAEALAAQAHAAAIgCQAHgCAHgEQAGgDAGgGQAFgFAEgHIAAAAIAaAOIAAABQgGAJgIAIQgJAIgJAFQgKAGgLADQgMACgLAAQgQAAgPgGg");
	this.shape_12.setTransform(-75.9,-11.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AggBPQgPgHgKgKQgLgKgFgQQgGgPAAgTQAAgRAHgPQAGgRALgLQALgMAPgHQAOgGAQAAQAPAAAOAFQANAHAKAKQALALAHANQAGAPACAQIh+ASQABAKAFAJQAEAIAGAGQAHAFAIADQAIADAJAAQAIAAAHgCQAIgDAHgEQAGgEAFgIQAFgGACgKIAcAGQgEAOgHALQgIAKgKAIQgKAIgMAEQgMAFgNAAQgTgBgOgFgAgLg4QgIACgHAFQgGAFgGAJQgGAIgCANIBYgLIgBgDQgGgOgKgIQgKgJgOABQgFAAgHACg");
	this.shape_13.setTransform(-93.8,-11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAJB/QgIgDgFgFQgGgGgEgHQgFgGgDgHQgHgRgBgVIAEi5IAdAAIgBAwIgBAnIgBAgIAAAYIgBAoQABANACALIAEAJIAGAIQADADAFADQAFACAGAAIgEAdQgKAAgIgEg");
	this.shape_14.setTransform(-106.1,-16.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AggBPQgPgHgLgKQgKgKgFgQQgFgPAAgTQAAgRAGgPQAGgRALgLQALgMAOgHQAPgGAQAAQAPAAAOAFQANAHALAKQAKALAHANQAGAPADAQIh/ASQACAKADAJQAEAIAHAGQAHAFAIADQAIADAJAAQAIAAAIgCQAHgDAGgEQAHgEAFgIQAEgGACgKIAdAGQgEAOgIALQgHAKgKAIQgKAIgMAEQgMAFgNAAQgSgBgPgFgAgMg4QgHACgGAFQgIAFgFAJQgGAIgBANIBWgLIgBgDQgFgOgJgIQgLgJgOABQgFAAgIACg");
	this.shape_15.setTransform(-119.9,-11.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgTB2QgLgCgKgFQgLgEgKgGIgVgNIAWgaQALALALAFQALAGAJADQALAEAIABQALAAAJgCQAJgCAHgEQAHgEAEgGQAEgGABgGQAAgGgCgEQgDgFgEgDQgFgEgGgDIgNgFIgOgEIgOgDIgRgEIgSgGQgJgEgIgGQgIgFgGgIQgGgIgDgLQgDgLABgPQABgLAEgKQAEgJAHgIQAGgHAIgFQAIgGAKgDQAJgEAKgBQAKgCAJAAQAOABAOADIAMAEIANAGQAHADAGAEIANAKIgRAaIgKgJIgKgHIgLgFIgKgEQgLgDgLAAQgMAAgLAEQgKAEgHAGQgHAHgEAIQgEAHAAAIQAAAHAFAHQAEAHAIAGQAIAGALAEQAKAEAMACIAVADQALACAJAEQAKAEAIAGQAIAGAGAHQAFAIADAJQADAKgCALQgCAKgEAIQgEAIgHAGQgGAGgIAEIgRAHQgIACgJABIgSABQgKAAgLgCg");
	this.shape_16.setTransform(-138.6,-14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(3,1,1).p("EAjLgDqMhGVAAAQhhAAhFBFQhFBFAABgQAABhBFBFQBFBFBhAAMBGVAAAQBhAABFhFQBFhFAAhhQAAhghFhFQhFhFhhAAg");
	this.shape_17.setTransform(1,-14.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#0000CC").s().p("EgjKADrQhhgBhFhFQhEhEAAhhQAAhgBEhFQBFhEBhAAMBGVAAAQBhAABFBEQBFBFgBBgQABBhhFBEQhFBFhhABg");
	this.shape_18.setTransform(1,-14.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.InstructionText_mccopy, new cjs.Rectangle(-249.1,-39.5,500.1,49.9), null);


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Option1_mccopy3_1();
	this.instance.parent = this;
	this.instance.setTransform(-0.2,0.2,1.31,1.31,0,0,0,-2.2,-2.5);

	this.instance_1 = new lib.Option1_mccopy4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(391.2,0.1,1.31,1.31,0,0,0,-2.3,-2.6);

	this.instance_2 = new lib.Option1_mccopy5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-391.8,0.2,1.31,1.31,0,0,0,-2.2,-2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-518.5,-127,1037.1,254);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween30("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(271.8,43);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03,y:42.9},9).to({scaleX:1,scaleY:1,y:43},10).to({scaleX:1.03,scaleY:1.03,y:42.9},10).to({scaleX:1,scaleY:1,y:43},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,545.1,86);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Option1_mccopy3();
	this.instance.parent = this;
	this.instance.setTransform(144.9,152.1,1.213,1.213,0,0,0,-2.3,-2.6);

	this.instance_1 = new lib.Tween14("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(145.2,147.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-3.5,-3.5,299.5,299.4), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(146.2,146.2,1,1,0,0,0,146.2,146.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.05,scaleY:1.05},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1,mode:"synched",startPosition:0},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.5,-3.5,299.5,299.4);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween29("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(107,35);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:0.1,scaleX:1.06,scaleY:1.06},9).to({regY:0,scaleX:1,scaleY:1},10).to({regY:0.1,scaleX:1.06,scaleY:1.06},10).to({regY:0,scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,216.9,69.9);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween20copy9_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(145.6,143.6);

	this.instance_1 = new lib.Tween14_1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(145.2,147.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(-3.5,-3.5,299.5,299.4), null);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(146.2,146.2,1,1,0,0,0,146.2,146.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.05,scaleY:1.05},9).to({scaleX:1,scaleY:1},10).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.5,-3.5,299.5,299.4);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(639.9,311.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(330).to({_off:false},0).wait(81).to({startPosition:1},0).to({alpha:0,startPosition:9},8).wait(1));

	// Layer_4
	this.instance_1 = new lib.Option1_mccopy5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(248.2,580.1,1.31,1.31,0,0,0,-2.2,-2.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(306).to({_off:false},0).to({x:639.8,y:304},18).wait(6).to({scaleX:1.59,scaleY:1.59,x:639.5,y:304.2},32).wait(49).to({alpha:0},8).wait(1));

	// Layer_12
	this.instance_2 = new lib.handanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(216.3,654.6,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(277).to({_off:false},0).to({_off:true},29).wait(114));

	// Layer_11
	this.instance_3 = new lib.arrowanim("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(253.5,402.5,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(247).to({_off:false},0).to({_off:true},30).wait(143));

	// Layer_10
	this.instance_4 = new lib.Option1_mccopy7();
	this.instance_4.parent = this;
	this.instance_4.setTransform(639.8,302,1.31,1.31,0,0,0,-2.2,-2.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(177).to({_off:false},0).to({alpha:0.699},11).wait(45).to({alpha:0},10).to({_off:true},1).wait(176));

	// Layer_15
	this.instance_5 = new lib.Symbol1copy2("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(641,303.1,1,1,0,0,0,146.2,146.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(150).to({_off:false},0).to({_off:true},27).wait(243));

	// Layer_9
	this.instance_6 = new lib.Symbol10("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(767.2,89.8,1,1,0,0,0,107,35);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(150).to({_off:false},0).wait(83).to({startPosition:1},0).to({alpha:0,startPosition:11},10).to({_off:true},1).wait(176));

	// Layer_2
	this.instance_7 = new lib.Tween24("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(640,579.9);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(120).to({_off:false},0).to({alpha:1},7).wait(197).to({startPosition:0},0).to({alpha:0},12).wait(84));

	// Layer_1
	this.instance_8 = new lib.Tween19("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640.4,300.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween20copy9("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640.4,300.5);

	this.instance_10 = new lib.Tween20copy8("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640.4,300.5);
	this.instance_10.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},120).to({state:[{t:this.instance_9}]},7).to({state:[{t:this.instance_10,p:{alpha:0}}]},50).to({state:[{t:this.instance_10,p:{alpha:1}}]},67).to({state:[]},80).wait(96));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(120).to({_off:false},0).to({_off:true,alpha:1},7).wait(293));

	// Layer_6
	this.questxt = new lib.InstructionText_mccopy();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(638.8,116.1,1.486,1.486,0,0,0,0.1,0.1);
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(120).to({_off:false},0).wait(204).to({alpha:0},12).wait(84));

	// Layer_3
	this.instance_11 = new lib.Symbol13();
	this.instance_11.parent = this;
	this.instance_11.setTransform(640.8,91.7,1,1,0,0,0,272.5,43);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(62).to({_off:false},0).wait(51).to({alpha:0},7).to({_off:true},1).wait(299));

	// Layer_14
	this.instance_12 = new lib.Symbol11("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(641,303.1,1,1,0,0,0,146.2,146.2);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(62).to({_off:false},0).to({_off:true},59).wait(299));

	// Layer_8
	this.instance_13 = new lib.Option1_mc();
	this.instance_13.parent = this;
	this.instance_13.setTransform(639.7,309,1.213,1.213,0,0,0,-2.3,-2.6);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(43).to({_off:false},0).to({alpha:1},7).wait(63).to({alpha:0},7).wait(300));

	// Layer_7
	this.instance_14 = new lib.Tween14("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,304.1);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(43).to({_off:false},0).to({alpha:1},7).wait(280).to({startPosition:0},0).to({regX:0.1,scaleX:1.21,scaleY:1.21,x:639.8,y:304.3},32).wait(49).to({startPosition:0},0).to({alpha:0},8).wait(1));

	// Layer_5
	this.instance_15 = new lib.Tween32("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(640,93.4);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.instance_16 = new lib.Tween33("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(640,93.4);

	this.instance_17 = new lib.Tween31("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(640,93.4);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_15}]},19).to({state:[{t:this.instance_16}]},9).to({state:[{t:this.instance_17}]},85).to({state:[{t:this.instance_17}]},7).to({state:[]},1).wait(299));
	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(19).to({_off:false},0).to({_off:true,alpha:1},9).wait(392));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(113).to({_off:false},0).to({alpha:0},7).to({_off:true},1).wait(299));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(586.8,315.2,1390,812);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;