(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AhdiJIAmgCIgBAZQAKgKALgFIATgIQAKgDAJgBQAMAAALAEQAMADAKAGQAKAGAJAJQAJAIAGALQAGAKADAMQADAMAAAOQgBAOgEANQgDANgHAKQgGALgJAIQgIAJgKAGQgJAGgLADQgKADgKAAQgKAAgKgDQgKgCgLgFQgLgFgLgKIgBB0IggABgAAAhqQgKAAgJAEQgJAEgIAGQgHAGgFAIQgGAIgCAIIAAAhQACALAGAIQAFAJAHAFQAIAFAIADQAJADAJAAQALABAKgFQALgEAIgIQAIgIAEgLQAFgLABgMQAAgNgDgLQgEgMgIgIQgIgJgKgFQgKgFgLAAIgCAAg");
	this.shape.setTransform(214.5,8.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AAvAuQgFAKgIAJQgHAJgIAGQgJAIgJADQgIAEgLAAQgPgBgLgFQgLgGgIgIQgHgJgEgLQgFgMgDgMQgCgNAAgOIgBgZQgBgSACgSIAEglIAjABIgDAmIgBAeIAAAOIACASQABAKACAKQACAKAEAHQAFAIAGAEQAHAEAKgBQAMgCAOgQQAOgRAQggIgBgrIgBgZIAAgOIAkgEIADA1IABAqIABAgIABAZIABAkIgnABg");
	this.shape_1.setTransform(191.6,4.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("AgiBdQgRgIgMgMQgNgOgHgSQgHgTAAgWQAAgVAHgTQAHgSANgOQAMgNARgHQAQgHASAAQATAAARAHQAQAIAMAOQANAMAHATQAHASAAAVQAAAWgHASQgHASgNANQgMANgQAJQgRAHgTAAQgSAAgQgHgAgYg/QgLAGgGAKQgHAKgDANQgDANAAALQAAAMADAMQAEAMAGALQAHAJAKAHQALAGANAAQAOAAALgGQAKgHAHgJQAGgLAEgMQADgMAAgMQAAgLgDgNQgDgNgGgKQgHgKgLgGQgLgGgOAAQgOAAgKAGg");
	this.shape_2.setTransform(170.4,4.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AhMAqIgCgqIgBggIAAgZIgBgkIAmgBIAAAVIAAAUIABAVIAMgRQAHgKAJgIQAJgIAJgHQALgGAMgCQAOAAALAGQAFADAFAFQAEAEAEAHQAEAHADAKQACAKACANIgiAMQAAgIgCgGIgCgLIgFgHIgFgEQgGgEgIgBQgFABgEAEIgLAIQgGAFgFAHIgLANIgKAOIgJAMIABAYIAAAYIABASIABAPIglAEIgBg1g");
	this.shape_3.setTransform(151.5,4.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AglCOIgPgFIgQgJQgIgGgGgIQgGgIgEgKIAhgOQADAHAEAFIAKAJIALAGIALADQAMADANgCQAMgCAJgFQAJgFAGgGQAGgGAEgHIAGgNIADgLIAAgIIAAgWQgKAKgLAFQgLAFgJACQgLADgJABQgUAAgRgHQgRgHgNgNQgMgNgIgRQgHgTAAgXQAAgXAIgSQAIgSANgMQANgNARgGQAQgHARAAQAKABAMAEQAKADAMAGQALAGAKAKIAAghIAiABIgBDCQAAALgDALQgDALgFALQgGAKgHAJQgIAJgLAHQgKAHgNAFQgNAEgPABIgGAAQgQAAgQgEgAgZhnQgLAFgHAJQgHAJgEAMQgFAMAAAOQAAAOAFAMQAEAMAHAHQAIAIALAFQAKAFANAAQAKAAAKgEQALgEAJgHQAIgGAGgKQAGgKACgLIAAgXQgCgMgGgKQgGgKgJgHQgJgHgKgEQgLgEgLAAQgMAAgKAFg");
	this.shape_4.setTransform(128.9,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgUgTIg7ABIABgfIA7gCIABhKIAhgCIgBBMIBCgDIgDAgIhAACIgBCTIgjABg");
	this.shape_5.setTransform(100.6,1.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AgeBcQgSgIgPgNQgOgOgIgRQgIgTAAgUQAAgMADgMQADgNAHgLQAGgLAJgKQAKgJAKgHQALgGAOgEQANgEAMAAQAQAAANADQAOAEALAHQALAHAJAJQAJAKAGAMIABABIgcARIgBgBQgEgJgGgGQgHgHgIgGQgHgEgKgDQgJgCgKgBQgMABgNAFQgMAGgIAIQgKAJgFANQgGANABANQgBANAGANQAFALAKAKQAIAJAMAGQANAEAMAAQAKABAIgDQAJgCAIgFQAHgEAGgGQAGgHAFgHIABgBIAdARIAAABQgGALgLAJQgJAJgLAGQgMAHgNADQgNADgOAAQgRAAgSgHg");
	this.shape_6.setTransform(81.8,4.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgmBcQgRgIgMgMQgMgNgGgRQgGgSAAgVQAAgUAHgSQAHgTANgOQAMgOARgHQARgIATAAQASAAAQAHQAPAGAMANQAMAMAIARQAIAQACAUIiSAUQABAMAFAKQAEAJAIAHQAHAHAKAEQAKADAKAAQAJAAAJgDQAJgDAHgFQAIgFAFgJQAGgIACgKIAhAHQgEAPgJAMQgJAOgLAIQgMAKgOAEQgOAFgPAAQgVAAgSgGgAgOhCQgIADgIAGQgIAGgGAKQgHAKgCAPIBlgNIgBgDQgGgRgMgKQgLgJgRAAQgGAAgJACg");
	this.shape_7.setTransform(60.9,4.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AhMAqIgCgqIgBggIAAgZIgBgkIAmgBIABAVIAAAUIAAAVIAMgRQAHgKAJgIQAJgIAJgHQALgGAMgCQAOAAALAGQAFADAFAFQAEAEAEAHQAEAHADAKQADAKABANIgiAMQAAgIgBgGIgEgLQgBgEgDgDIgFgEQgGgEgHgBQgFABgFAEIgLAIQgGAFgFAHIgLANIgKAOIgJAMIABAYIAAAYIABASIABAPIglAEIgBg1g");
	this.shape_8.setTransform(42,4.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AhMAqIgCgqIgBggIgBgZIgBgkIAngBIAAAVIAAAUIABAVIAMgRQAHgKAJgIQAJgIAKgHQAKgGAMgCQAOAAALAGQAFADAFAFQAEAEAEAHQAEAHADAKQACAKABANIghAMQAAgIgCgGIgCgLIgFgHIgFgEQgGgEgIgBQgEABgGAEIgKAIQgFAFgGAHIgLANIgKAOIgJAMIABAYIABAYIABASIAAAPIgkAEIgCg1g");
	this.shape_9.setTransform(23,4.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgiBdQgRgIgMgMQgNgOgHgSQgHgTAAgWQAAgVAHgTQAHgSANgOQAMgNARgHQAQgHASAAQATAAARAHQAQAIANAOQAMAMAHATQAHASAAAVQAAAWgHASQgHASgMANQgNANgQAJQgRAHgTAAQgSAAgQgHgAgYg/QgLAGgGAKQgHAKgDANQgDANAAALQAAAMADAMQAEAMAGALQAHAJAKAHQALAGANAAQAOAAALgGQAKgHAHgJQAGgLAEgMQADgMAAgMQAAgLgDgNQgDgNgGgKQgHgKgLgGQgLgGgOAAQgOAAgKAGg");
	this.shape_10.setTransform(2.1,4.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AgfBcQgRgIgOgNQgPgOgIgRQgJgTAAgUQABgMADgMQAEgNAGgLQAHgLAIgKQAKgJALgHQAKgGANgEQANgEANAAQAPAAAOADQANAEALAHQAMAHAJAJQAJAKAGAMIAAABIgcARIAAgBQgFgJgFgGQgHgHgIgGQgIgEgJgDQgIgCgLgBQgNABgMAFQgMAGgJAIQgJAJgFANQgGANABANQgBANAGANQAFALAJAKQAJAJAMAGQAMAEANAAQAKABAIgDQAJgCAHgFQAIgEAGgGQAGgHAFgHIABgBIAdARIAAABQgHALgKAJQgJAJgLAGQgMAHgNADQgNADgOAAQgSAAgSgHg");
	this.shape_11.setTransform(-18.8,4.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgmBcQgRgIgMgMQgMgNgGgRQgGgSAAgVQAAgUAHgSQAHgTANgOQAMgOARgHQARgIATAAQASAAAQAHQAPAGAMANQAMAMAIARQAIAQACAUIiSAUQABAMAFAKQAEAJAIAHQAHAHAKAEQAKADAKAAQAJAAAJgDQAJgDAHgFQAIgFAFgJQAGgIACgKIAhAHQgEAPgJAMQgJAOgLAIQgMAKgOAEQgOAFgPAAQgVAAgSgGgAgOhCQgIADgIAGQgIAGgGAKQgHAKgCAPIBlgNIgBgDQgGgRgMgKQgLgJgRAAQgGAAgJACg");
	this.shape_12.setTransform(-48.8,4.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AAuCBQAEgQADgPIADgcIAAgbQgBgTgFgMQgGgMgIgIQgIgIgJgEQgKgEgJAAQgIABgKAFQgJAEgKAJQgJAHgJAQIgBBxIghAAIgDkFIAngBIgBBnQAKgKAKgGQALgGAJgDQAKgDAKgCQATAAAQAHQAPAHALANQAMANAGASQAHARABAXIAAAZIgCAZIgCAXQgCAKgDAJg");
	this.shape_13.setTransform(-70.5,0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgUgTIg7ABIABgfIA7gCIABhKIAhgCIgBBMIBCgDIgDAgIhAACIgBCTIgjABg");
	this.shape_14.setTransform(-90.4,1.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgUgTIg7ABIABgfIA7gCIABhKIAhgCIgBBMIBCgDIgDAgIhAACIgBCTIgjABg");
	this.shape_15.setTransform(-116.1,1.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AgfBcQgRgIgOgNQgPgOgIgRQgJgTAAgUQABgMADgMQAEgNAGgLQAHgLAIgKQAKgJALgHQAKgGANgEQANgEANAAQAPAAAOADQANAEALAHQAMAHAJAJQAJAKAGAMIAAABIgcARIAAgBQgFgJgFgGQgHgHgIgGQgIgEgJgDQgIgCgLgBQgNABgMAFQgMAGgJAIQgJAJgFANQgGANABANQgBANAGANQAFALAJAKQAJAJAMAGQAMAEANAAQAKABAIgDQAJgCAHgFQAIgEAGgGQAGgHAFgHIABgBIAdARIAAABQgHALgKAJQgJAJgLAGQgMAHgNADQgNADgOAAQgSAAgSgHg");
	this.shape_16.setTransform(-134.9,4.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AgmBcQgRgIgMgMQgMgNgGgRQgGgSAAgVQAAgUAHgSQAHgTANgOQAMgOARgHQARgIATAAQASAAAQAHQAPAGAMANQAMAMAIARQAIAQACAUIiSAUQABAMAFAKQAEAJAIAHQAHAHAKAEQAKADAKAAQAJAAAJgDQAJgDAHgFQAIgFAFgJQAGgIACgKIAhAHQgEAPgJAMQgJAOgLAIQgMAKgOAEQgOAFgPAAQgVAAgSgGgAgOhCQgIADgIAGQgIAGgGAKQgHAKgCAPIBlgNIgBgDQgGgRgMgKQgLgJgRAAQgGAAgJACg");
	this.shape_17.setTransform(-155.8,4.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AALCUQgJgEgGgGQgIgGgEgIQgGgIgDgJQgIgTgDgYIAFjXIAjAAIgBA3IgCAuIgBAlIAAAcIgBAuQABAQADAMQABAGADAFQADAFAEAEQAEAEAGADQAFACAIAAIgFAiQgMAAgJgEg");
	this.shape_18.setTransform(-170.1,-1.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0000").s().p("AgmBcQgRgIgMgMQgMgNgGgRQgGgSAAgVQAAgUAHgSQAHgTANgOQAMgOARgHQARgIATAAQASAAAQAHQAPAGAMANQAMAMAIARQAIAQACAUIiSAUQABAMAFAKQAEAJAIAHQAHAHAKAEQAKADAKAAQAJAAAJgDQAJgDAHgFQAIgFAFgJQAGgIACgKIAhAHQgEAPgJAMQgJAOgLAIQgMAKgOAEQgOAFgPAAQgVAAgSgGgAgOhCQgIADgIAGQgIAGgGAKQgHAKgCAPIBlgNIgBgDQgGgRgMgKQgLgJgRAAQgGAAgJACg");
	this.shape_19.setTransform(-186.3,4.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0000").s().p("AgYBgIgOgDIgQgFIgQgHIgOgKIAQgbIATAKIAUAJIAVAFQAJACAMAAQAJAAAHgCQAGgCAEgDQAFgCACgDIAEgGIAAgHIgCgHIgEgGQgEgDgFgDQgGgDgIgCIgTgCQgOAAgOgDQgOgDgLgFQgLgFgIgIQgHgIgCgNQgBgNADgJQADgLAGgHQAFgJAJgGQAIgFAKgEQAKgEALgDQALgCAKAAIAQABIATADQAJACALAEQAKADAJAHIgMAfQgLgGgLgDIgSgGIgRgCQgZgBgPAGQgPAIgBAOQABALAFAEQAGAEAJACIAXADQAMABAPADQAQACAMAFQALAEAHAHQAHAGADAJQADAIABAJQAAAQgHAMQgHAKgKAIQgLAGgPAEQgOADgPAAQgPAAgQgDg");
	this.shape_20.setTransform(-206.5,5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(3,1,1).p("EghbgDfMBC3AAAQCTAAAAB2IAADTQAAB2iTAAMhC3AAAQiTAAAAh2IAAjTQAAh2CTAAg");
	this.shape_21.setTransform(0,3.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("EghbADfQiTABAAh2IAAjTQAAh1CTAAMBC3AAAQCTAAAAB1IAADTQAAB2iTgBg");
	this.shape_22.setTransform(0,3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-230.2,-28,460.5,56.1);


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E50000").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(0,0,0.335,0.335);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.7,-52.6,121.6,105.2);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E50000").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(0,0,0.335,0.335);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.7,-52.6,121.6,105.2);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E748B2").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.666,0.403,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.6,-32.4,107.2,64.9);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E748B2").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(0,0,0.666,0.403,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.6,-32.4,107.2,64.9);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(0,0,0.381,0.381);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54.8,-52.1,109.6,104.3);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(0,0,0.381,0.381);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54.8,-52.1,109.6,104.3);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E50000").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(-145.4,2,0.323,0.323);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape_1.setTransform(146.8,0,0.397,0.397);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E748B2").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_2.setTransform(0.7,2.3,0.667,0.403,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_3.setTransform(0,0,1.063,1.063);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_4.setTransform(0,0,1.063,1.063);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333366").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_5.setTransform(0,0,1.074,1.074);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_6.setTransform(0,0,1.074,1.074);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-245.9,-96.6,491.9,193.4);


(lib.Symbol7copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(58,55.2,0.403,0.403);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7copy2, new cjs.Rectangle(0,0,116,110.4), null);


(lib.Symbol6copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E748B2").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape.setTransform(54.5,33,0.677,0.41,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6copy2, new cjs.Rectangle(0,0,109.1,66), null);


(lib.Symbol5copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E50000").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(59.5,51.5,0.328,0.328);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy3, new cjs.Rectangle(0,0,119.1,103.1), null);


(lib.Symbol3copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E50000").s().p("AuLYlIuM4lIOM4kIcXAAIOMYkIuMYlg");
	this.shape.setTransform(84.8,141.4,0.304,0.304);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape_1.setTransform(359.7,139.5,0.373,0.373);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E748B2").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_2.setTransform(222.2,141.6,0.627,0.379,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_3.setTransform(221.6,139.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_4.setTransform(221.6,139.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333366").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_5.setTransform(221.6,139.5,1.01,1.01);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer_4
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_6.setTransform(221.6,139.5,1.01,1.01);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy3, new cjs.Rectangle(-9.7,48.6,462.7,181.9), null);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape.setTransform(88.6,139.5,0.373,0.373);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#46B0F7").s().p("At4VYIol6aIWdwVIWeQVIomaag");
	this.shape_1.setTransform(354.6,139.5,0.373,0.373);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E748B2").s().p("AskMlIAA5JIZJAAIAAZJg");
	this.shape_2.setTransform(221.5,139.5,0.627,0.379,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape_3.setTransform(221.6,139.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_4.setTransform(221.6,139.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_5.setTransform(221.6,139.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer_4
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_6.setTransform(221.6,139.5,1.01,1.01);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy2, new cjs.Rectangle(-9.7,48.6,462.7,181.9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhKhtIAegBIAAAUQAIgIAIgFIAQgGIAPgDQAJAAAJADQAJACAIAGQAIAEAHAHQAHAGAFAJQAFAJADAJQACAJAAALQAAAMgEAKQgDALgFAHQgFAJgHAHQgGAGgIAFQgIAFgIADQgIACgIAAQgIAAgIgCIgRgGQgJgEgIgIIgBBcIgZABgAgPhRQgHADgGAFQgGAEgEAHQgFAGgCAHIAAAaQACAJAFAGQAEAHAGAEQAGAEAHADIAOACQAIABAJgEQAIgDAGgGQAGgGAEgKQAEgIAAgKQABgKgDgJQgDgJgGgIQgGgGgJgEQgIgEgKgBQgIABgHADg");
	this.shape.setTransform(167.5,25.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAmAlQgFAIgGAHQgGAHgHAFQgGAGgIADQgGADgIAAQgMgBgJgEQgJgEgGgHQgGgHgDgJQgEgKgCgKQgCgKAAgLIgBgTIABgdIADgeIAcABQgCARgBAOIAAAXIAAAMIABAOIACAQQACAHAEAGQAEAGAFAEQAFADAIgBQAKgCALgNQALgMAMgaIAAgjIgBgTIAAgMIAdgDIACAqIABAhIABAbIAAATIABAdIgfABg");
	this.shape_1.setTransform(149.3,21.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbBKQgOgGgJgKQgKgLgGgOQgGgPAAgSQAAgRAGgPQAGgPAKgKQAJgKAOgGQANgGAOAAQAPAAANAHQAOAFAKALQAJAKAGAQQAGAOAAAQQAAARgGAPQgGAOgJALQgKALgOAGQgNAGgPAAQgOAAgNgGgAgTgyQgIAFgGAIQgFAIgCAKQgDAKAAAJQAAAJADAKQADALAFAHQAFAIAJAGQAIAEAKAAQALAAAJgEQAIgGAGgIQAFgHADgLQACgKAAgJQAAgJgCgKQgDgKgFgIQgFgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_2.setTransform(132.3,21.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_3.setTransform(117.2,21.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgdBxIgNgEIgMgHQgGgFgFgGQgFgGgDgJIAagLQADAGADAEQAEAEAEADIAJAFIAIACQALACAKgBQAJgCAHgEQAHgEAFgFQAEgEAEgGIAEgKIADgJIABgGIAAgSQgJAIgIAEQgJAEgHACQgKACgHABQgQAAgNgGQgOgFgKgLQgKgKgGgOQgFgOAAgTQgBgSAHgPQAGgOALgKQAKgKAOgFQANgFANAAQAIABAKADQAHACAKAFQAJAFAIAIIAAgbIAbABIgBCbQgBAJgCAIQgCAJgEAIQgEAJgHAHQgGAHgIAGQgIAFgKAEQgKAEgNABIgDAAQgOAAgNgEgAgThSQgJAEgGAHQgGAHgDAKQgDAJAAALQAAAMADAJQAEAJAFAGQAHAHAIAEQAIADAKAAQAIAAAJgDQAIgDAHgFQAHgFAFgIQAEgIACgJIAAgSQgCgKgEgHQgFgIgHgGQgIgGgIgDQgIgDgIAAQgLAAgHAEg");
	this.shape_4.setTransform(99.2,24.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_5.setTransform(76.6,18.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgYBJQgOgGgLgLQgMgKgGgOQgIgPAAgRQAAgJADgKQADgJAFgJQAGgKAHgHQAHgIAIgEQAJgGALgDQAKgDAKAAQAMAAALADQALADAJAFQAJAGAHAHQAHAIAFAJIABABIgXAOIgBgBQgDgHgFgGQgFgFgHgEQgFgDgHgDQgIgCgIAAQgKAAgJAFQgKAEgHAHQgIAHgEALQgEAJAAAKQAAALAEAKQAEAKAIAHQAHAHAKAFQAJAEAKAAQAIAAAHgCIAMgGQAGgDAFgFQAFgFAEgGIAAgBIAYAOIAAABQgGAIgHAHQgIAHgJAGQgJAEgLAEQgKACgLAAQgOAAgOgGg");
	this.shape_6.setTransform(61.7,21.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeBJQgNgGgKgJQgKgKgFgOQgEgOAAgSQgBgQAGgOQAGgPAKgLQAKgLAOgGQANgHAPAAQAOAAANAGQAMAFAJALQALAJAFANQAHANABAQIh0AQQABAKAEAHQADAIAHAGQAFAFAIADQAIACAIABQAHgBAHgBQAIgDAFgEQAGgFAEgGQAFgGACgJIAbAGQgEAMgHAKQgHAKgJAHQgKAHgLAEQgLAFgMAAQgRgBgOgFgAgKg0QgIACgGAFQgGAEgFAIQgGAIgCAMIBRgJIgBgEQgFgNgIgHQgJgIgOAAQgFAAgGACg");
	this.shape_7.setTransform(45,21.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_8.setTransform(29.8,21.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_9.setTransform(14.7,21.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgbBKQgOgGgJgKQgKgLgGgOQgGgPAAgSQAAgRAGgPQAGgPAKgKQAJgKAOgGQANgGAOAAQAPAAANAHQAOAFAKALQAJAKAGAQQAGAOAAAQQAAARgGAPQgGAOgJALQgKALgOAGQgNAGgPAAQgOAAgNgGgAgTgyQgIAFgGAIQgFAIgCAKQgDAKAAAJQAAAJADAKQADALAFAHQAFAIAJAGQAIAEAKAAQALAAAJgEQAIgGAGgIQAFgHADgLQACgKAAgJQAAgJgCgKQgDgKgFgIQgFgIgJgFQgIgFgMAAQgLAAgIAFg");
	this.shape_10.setTransform(-2,21.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgYBJQgPgGgKgLQgMgKgGgOQgIgPABgRQAAgJACgKQADgJAFgJQAFgKAHgHQAIgIAIgEQAKgGAKgDQAKgDALAAQAMAAAKADQALADAIAFQAKAGAHAHQAHAIAFAJIABABIgXAOIgBgBQgDgHgFgGQgFgFgGgEQgHgDgHgDQgHgCgHAAQgLAAgKAFQgJAEgHAHQgIAHgEALQgEAJAAAKQAAALAEAKQAEAKAIAHQAHAHAJAFQAKAEALAAQAGAAAHgCIAOgGQAGgDAEgFQAFgFAEgGIAAgBIAYAOIAAABQgGAIgHAHQgIAHgJAGQgJAEgLAEQgKACgKAAQgPAAgOgGg");
	this.shape_11.setTransform(-18.6,21.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgeBJQgNgGgKgJQgKgKgFgOQgEgOgBgSQAAgQAGgOQAGgPAKgLQAKgLANgGQAOgHAPAAQAOAAANAGQAMAFAJALQAKAJAHANQAGANACAQIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHgBAHgBQAHgDAGgEQAGgFAFgGQAEgGACgJIAbAGQgFAMgHAKQgHAKgIAHQgKAHgLAEQgLAFgMAAQgRgBgOgFgAgLg0QgGACgHAFQgGAEgFAIQgFAIgCAMIBRgJIgBgEQgGgNgJgHQgIgIgOAAQgFAAgHACg");
	this.shape_12.setTransform(-42.6,21.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAkBnQAEgMACgNIACgWIABgWQgBgPgEgJQgEgKgHgGQgGgHgIgCQgHgEgIAAQgHABgHAEQgHADgIAHQgHAGgIAMIAABaIgbABIgCjQIAfgBIAABSQAHgIAIgFQAJgFAHgCQAIgDAIgBQAPABAMAFQANAFAJALQAJAJAFAPQAGAOAAASIAAAUIgBATIgCATIgDAPg");
	this.shape_13.setTransform(-59.9,18.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_14.setTransform(-75.8,18.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_15.setTransform(-96.3,18.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgYBJQgPgGgKgLQgMgKgGgOQgIgPABgRQAAgJACgKQADgJAFgJQAFgKAHgHQAIgIAIgEQAKgGAKgDQAKgDALAAQAMAAAKADQALADAIAFQAKAGAHAHQAHAIAFAJIABABIgXAOIgBgBQgDgHgFgGQgFgFgGgEQgHgDgHgDQgHgCgHAAQgLAAgKAFQgJAEgHAHQgIAHgEALQgEAJAAAKQAAALAEAKQAEAKAIAHQAHAHAJAFQAKAEALAAQAGAAAHgCIAOgGQAGgDAEgFQAFgFAEgGIAAgBIAYAOIAAABQgGAIgHAHQgIAHgJAGQgJAEgLAEQgKACgKAAQgPAAgOgGg");
	this.shape_16.setTransform(-111.3,21.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeBJQgOgGgJgJQgKgKgEgOQgGgOAAgSQABgQAFgOQAGgPAKgLQAKgLANgGQAOgHAPAAQAOAAAMAGQANAFAKALQAKAJAGANQAGANABAQIh0AQQABAKAEAHQAEAIAFAGQAGAFAIADQAIACAIABQAHgBAHgBQAHgDAGgEQAGgFAFgGQAEgGACgJIAaAGQgDAMgIAKQgHAKgJAHQgJAHgLAEQgLAFgMAAQgRgBgOgFgAgLg0QgHACgGAFQgGAEgFAIQgFAIgDAMIBSgJIgCgEQgEgNgJgHQgKgIgNAAQgFAAgHACg");
	this.shape_17.setTransform(-128,21.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAJB3QgIgDgFgFQgFgFgEgHIgHgNQgHgPgBgUIAEisIAbAAIgBAtIgBAlIgBAdIAAAXIgBAkQAAAMADALIAEAIIAFAHIAIAGQAEABAGAAIgEAbQgJAAgHgCg");
	this.shape_18.setTransform(-139.4,16.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgeBJQgOgGgJgJQgKgKgEgOQgGgOAAgSQABgQAFgOQAGgPAKgLQAKgLANgGQAOgHAPAAQAOAAAMAGQANAFAKALQAKAJAGANQAGANABAQIh0AQQABAKAEAHQAEAIAFAGQAGAFAIADQAIACAIABQAHgBAHgBQAHgDAGgEQAGgFAFgGQAEgGACgJIAaAGQgDAMgIAKQgHAKgJAHQgJAHgLAEQgLAFgMAAQgRgBgOgFgAgLg0QgHACgGAFQgGAEgFAIQgFAIgDAMIBSgJIgCgEQgEgNgJgHQgKgIgNAAQgFAAgHACg");
	this.shape_19.setTransform(-152.3,21.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgTBNIgLgCIgNgFIgNgFIgLgIIANgVIAQAHIAPAHIARAFQAIABAIAAQAIAAAFgBIAJgEIAFgFIADgFIAAgEQAAgDgBgDQgCgDgCgDIgIgEQgEgCgGgCIgOgCQgMAAgLgCQgLgCgJgEQgKgFgFgFQgGgHgBgLQgBgJACgIQACgJAFgGQAEgGAHgFQAHgFAHgDQAJgDAJgBQAIgCAIAAIAMAAIAQACIAQAFQAHADAHAFIgJAZIgRgIIgOgDIgOgCQgVgCgLAGQgMAFAAAMQAAAIAFAEQAEADAHACIASACIAWADQANACAJAEQAJADAGAFQAFAGADAHQACAGAAAHQAAANgFAJQgGAJgIAFQgJAFgLADQgLADgMAAQgMAAgNgCg");
	this.shape_20.setTransform(-168.5,22.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgeBhQgOgFgKgLQgKgKgGgPQgFgOgBgTQAAgSAHgOQAGgOALgKQAKgKAOgFQANgFANAAQAIAAAJACQAIACAKAEQAIAFAIAIIAChGIAZACIAADIIgcAAIAAgRIgIAGIgJAFIgIAEIgIACIgQADQgQAAgNgGgAgUgXQgJAEgGAHQgGAIgDAIQgDAKAAALQAAAMADAJQAEAKAFAHQAGAHAKAEQAIAEAKAAQAIAAAIgDQAIgDAGgFQAHgFAFgHQAFgHACgIIABgdQgCgJgFgGQgGgHgHgFQgGgFgJgDQgIgDgHAAQgKAAgJAEg");
	this.shape_21.setTransform(160.2,-13.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("Ag/AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAAnQAFgHAGgHQAFgHAHgFQAHgGAGgDQAIgDAIgBQAKgBAIADQAIACAFAFQAGAEADAGQAEAGACAGIADANIABALQABAXgBAXIgBAxIgcgBIACgrQABgWgBgWIAAgGIgCgIIgDgJQgCgEgDgDIgIgFQgFgCgGABQgLACgKAOQgLAOgNAaIABAjIABAUIAAAKIgdAEIgCgqg");
	this.shape_22.setTransform(142.8,-10.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAqBNIACgTQgQALgRAFQgPAFgOgBQgJABgKgDQgJgDgHgFQgGgFgFgIQgEgIAAgLQAAgLADgJQAEgIAFgHQAGgGAHgFQAJgEAIgEQAJgDAKgBQAIgBAKAAIARABIAOABIgFgNQgCgGgEgGQgEgFgGgEQgGgDgIAAIgLACQgHABgIAFQgIADgKAIQgKAHgKALIgRgUQANgMAMgIQAMgHAKgFQALgFAJgBQAIgCAHAAQANAAAKAFQAJADAIAIQAHAHAGAKQAFALADALQADAMABAMQACALAAANIgCAbQgBAQgDARgAgGAAQgLACgJAFQgIAGgEAIQgEAIADAKQABAIAGADQAFADAIAAQAIAAAJgCIASgHIARgIIANgIIAAgOIgBgNIgNgDIgPgBQgMABgLACg");
	this.shape_23.setTransform(125.3,-11.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTBNIgMgCIgMgFIgNgGIgLgHIANgVIAPAHIAQAHIARAEQAHACAKAAQAHAAAFgCIAJgDIAFgFIADgFIAAgFQAAgDgBgCQgCgDgCgDIgHgEQgFgDgGgBIgOgCQgMAAgLgCQgLgCgJgEQgKgFgFgFQgGgHgBgKQgBgKACgIQACgJAFgGQAFgGAGgFQAHgFAHgDQAJgDAIgBQAJgCAIgBIANABIAOADIARAEQAIADAHAFIgKAZIgRgIIgPgDIgNgDQgUgBgMAGQgMAFAAAMQAAAIAEAEQAFAEAHACIASABIAWADQANACAJAEQAJADAFAFQAGAGADAGQACAHAAAHQAAANgFAJQgGAIgIAHQgJAFgLADQgMACgMAAQgLABgNgDg");
	this.shape_24.setTransform(101.8,-10.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgeBJQgNgGgKgKQgKgJgFgOQgEgPgBgRQAAgPAGgPQAGgPAKgLQAKgLANgGQAOgGAPgBQAOABANAFQAMAGAJAKQAKAJAHANQAGAOACAPIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAFgHQAEgGACgJIAbAGQgFAMgHAKQgHAKgIAHQgKAIgLADQgLAFgMAAQgRAAgOgGgAgLg0QgGACgHAFQgGAFgFAHQgFAJgCALIBRgKIgBgDQgGgNgJgHQgIgIgOAAQgFAAgHACg");
	this.shape_25.setTransform(86.1,-10.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgdBxIgMgEIgNgHQgGgFgFgGQgFgGgDgJIAagLQADAGADAEIAIAHIAJAFIAJACQAJACALgBQAJgCAHgEQAHgEAEgFQAFgEAEgGIAFgKIACgJIABgGIAAgSQgJAIgJAEQgIAEgIACQgIACgIABQgPAAgOgGQgNgFgLgLQgKgKgFgOQgHgOAAgTQABgSAGgPQAHgOAKgKQAKgKANgFQANgFANAAQAJABAJADQAIACAJAFQAKAFAIAIIAAgbIAaABIAACbQgBAJgBAIQgCAJgFAIQgFAJgFAHQgHAHgIAGQgIAFgLAEQgKAEgMABIgDAAQgOAAgNgEgAgUhSQgIAEgGAHQgGAHgDAKQgDAJgBALQABAMADAJQADAJAHAGQAFAHAJAEQAIADAKAAQAIAAAIgDQAJgDAHgFQAHgFAEgIQAFgIACgJIAAgSQgBgKgGgHQgFgIgGgGQgIgGgIgDQgIgDgJAAQgKAAgIAEg");
	this.shape_26.setTransform(68,-7.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAqBNIACgTQgRALgQAFQgOAFgOgBQgKABgJgDQgKgDgGgFQgIgFgDgIQgFgIAAgLQAAgLADgJQAEgIAFgHQAGgGAHgFQAJgEAJgEQAIgDAKgBQAIgBAKAAIARABIAOABIgEgNQgDgGgEgGQgEgFgGgEQgGgDgIAAIgLACQgHABgIAFQgIADgKAIQgKAHgLALIgQgUQANgMAMgIQAMgHALgFQAKgFAJgBQAIgCAIAAQAMAAAJAFQAKADAIAIQAHAHAGAKQAEALADALQAEAMABAMQACALAAANIgCAbQgBAQgEARgAgGAAQgMACgHAFQgJAGgEAIQgEAIACAKQACAIAGADQAFADAIAAQAIAAAJgCIASgHIAQgIIAOgIIAAgOIAAgNIgPgDIgOgBQgNABgKACg");
	this.shape_27.setTransform(50.3,-11.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("ABBAgIAAgdIgBgWQAAgLgDgFQgDgGgFABQgEgBgDADIgHAFIgHAHIgGAJIgGAIIgEAHIAAANIABAQIAAAVIAAAZIgaACIAAgrIgBgdIgBgWQgBgLgDgFQgDgGgGABQgDgBgDADIgIAGIgHAJIgHAJIgHAJIgEAHIABBHIgbACIgEiSIAdgDIAAAoIAKgMIAKgKQAFgFAHgDQAGgDAIAAQAGAAAFACQAGACADAEQAEADADAHQADAGACAHIAJgLIAKgKQAFgEAGgDQAGgDAJAAQAGAAAFACQAGACAFAFQAEAEADAGQACAIAAAJIACAZIABAfIABAwIgdACIAAgrg");
	this.shape_28.setTransform(30.7,-11.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgRBmIABgjIABgrIABg5IAbgBIgBAjIAAAdIgBAZIAAASIgBAdgAgHhAIgGgEIgEgGQgBgDAAgEQAAgFABgDQACgEACgDIAGgDQAEgCADAAQAEAAADACIAHADQACADACAEQABADAAAFQAAAEgBADIgEAGIgHAEQgDACgEAAQgDAAgEgCg");
	this.shape_29.setTransform(16.1,-14.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgeBJQgNgGgKgKQgKgJgFgOQgEgPgBgRQAAgPAGgPQAGgPAKgLQAKgLANgGQAOgGAPgBQAOABANAFQAMAGAJAKQAKAJAHANQAGAOACAPIh1AQQABAKADAHQAFAIAFAGQAGAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAFgHQAEgGACgJIAbAGQgFAMgHAKQgHAKgIAHQgKAIgLADQgLAFgMAAQgRAAgOgGgAgLg0QgGACgHAFQgGAFgFAHQgFAJgCALIBRgKIgBgDQgGgNgJgHQgIgIgOAAQgFAAgHACg");
	this.shape_30.setTransform(-3,-10.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAlBoQADgOACgMIADgXIAAgVQgBgPgEgKQgFgJgGgHQgGgGgIgDQgIgDgGAAQgIABgIAEQgGADgIAHQgIAGgGANIgBBaIgbAAIgCjQIAfgBIgBBSQAIgIAJgFQAHgFAIgCQAJgDAGgBQAQAAAMAGQANAFAJAKQAJALAFAOQAFAOABASIAAAUIgBATIgCATIgEAPg");
	this.shape_31.setTransform(-20.4,-14.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgQgPIgvABIABgZIAvgBIABg8IAagBIgBA8IA1gBIgCAZIgzABIgBB2IgcAAg");
	this.shape_32.setTransform(-36.3,-13.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgeBJQgOgGgJgKQgKgJgEgOQgGgPAAgRQABgPAFgPQAGgPAKgLQAKgLANgGQAOgGAPgBQAOABAMAFQANAGAKAKQAJAJAGANQAHAOABAPIh0AQQABAKAEAHQADAIAHAGQAFAFAIADQAIACAIABQAHAAAHgCQAHgDAGgEQAGgEAEgHQAFgGACgJIAaAGQgEAMgGAKQgIAKgJAHQgJAIgLADQgLAFgMAAQgRAAgOgGgAgKg0QgIACgGAFQgGAFgFAHQgGAJgCALIBRgKIgBgDQgFgNgIgHQgKgIgNAAQgFAAgGACg");
	this.shape_33.setTransform(-58.3,-10.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgVBJIgviJIAbgEIAoB0IAqh5IAcAEIg1CPg");
	this.shape_34.setTransform(-74.3,-11.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("Ag9AiIgBgiIgBgaIAAgTIgBgdIAfgBIAAARIAAAQIAAAQIAKgNIANgPQAHgGAHgFQAJgFAKgBQAKgBAJAFQAEACAEAEQAEAEADAFQADAGACAIIADASIgbAKIgBgLIgCgJIgEgGIgEgDQgFgEgGAAQgDABgFADIgIAGIgIAKIgJALIgIAKIgHAJIAAAUIABATIAAAPIABALIgdAEIgCgqg");
	this.shape_35.setTransform(-88.9,-10.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgeBJQgOgGgJgKQgKgJgFgOQgEgPAAgRQgBgPAGgPQAGgPAKgLQAKgLAOgGQANgGAPgBQAOABAMAFQANAGAJAKQALAJAFANQAHAOABAPIh0AQQABAKAEAHQADAIAHAGQAFAFAIADQAIACAIABQAHAAAHgCQAIgDAFgEQAGgEAEgHQAFgGACgJIAbAGQgEAMgHAKQgHAKgJAHQgKAIgLADQgLAFgMAAQgRAAgOgGgAgKg0QgIACgGAFQgGAFgFAHQgGAJgCALIBRgKIgBgDQgFgNgIgHQgJgIgOAAQgFAAgGACg");
	this.shape_36.setTransform(-105.3,-10.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgTBNIgMgCIgMgFIgMgGIgMgHIANgVIAQAHIAPAHIARAEQAHACAJAAQAIAAAFgCIAJgDIAFgFIACgFIABgFQAAgDgCgCQAAgDgDgDIgIgEQgDgDgHgBIgPgCQgLAAgLgCQgMgCgJgEQgIgFgGgFQgGgHgBgKQgBgKACgIQADgJAEgGQAFgGAGgFQAHgFAIgDQAHgDAKgBQAIgCAIgBIANABIAPADIAPAEQAIADAHAFIgJAZIgRgIIgPgDIgNgDQgUgBgMAGQgMAFAAAMQAAAIAEAEQAFAEAIACIASABIAUADQAOACAJAEQAJADAFAFQAHAGACAGQACAHAAAHQAAANgFAJQgFAIgJAHQgJAFgLADQgMACgLAAQgMABgNgDg");
	this.shape_37.setTransform(-121.5,-10.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgMBoQgHgCgJgEQgJgEgJgIIAAASIgaAAIgCjQIAegBIgBBQQAIgIAJgEQAIgEAHgCIAQgDQAJAAAJADQAJADAIAFQAIAEAHAHQAHAHAFAIQAFAJADAIQACAKAAAKQAAAMgEALQgDAKgFAJQgFAIgHAHIgOAMQgIAEgIADQgIACgIAAQgIAAgJgCgAgPgRQgHADgGAEQgGAFgFAFQgEAGgCAHIgBAhQADAIAEAHQAFAGAGAFQAGAEAGACIAOADQAJAAAJgEQAJgEAGgGQAGgHAEgJQAEgJABgLQAAgKgDgJQgDgKgGgGQgGgHgJgFQgJgEgKAAQgIAAgHADg");
	this.shape_38.setTransform(-137.7,-14.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgaBmQgNgFgMgGQgLgHgKgJQgJgKgHgMQgGgMgEgNQgEgOAAgOQAAgNAEgOQAEgNAGgMQAHgMAJgKQAKgJALgHQAMgHANgEQANgDANAAQAOAAANADQANAEAMAHQALAHAKAJQAJAKAHAMQAHAMADANQAEAOAAANQAAAOgEAOQgDANgHAMQgHAMgJAKQgKAJgLAHQgMAGgNAFQgNADgOAAQgNAAgNgDgAgchHQgOAGgKALQgLALgGANQgGAPAAAPQAAAQAGAPQAGANALALQAKALAOAGQANAGAPAAQALAAAJgCQAKgEAIgFQAJgEAHgHQAHgIAFgIQAFgJACgKQADgKAAgLQAAgKgDgKQgCgKgFgJQgFgIgHgHQgHgIgJgFQgIgEgKgEQgJgCgLAAQgPAAgNAGg");
	this.shape_39.setTransform(-158.6,-13.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FFCC00").ss(5,1,1).p("A9gmNMA7BAAAQCkAAAACkIAAHTQAACkikAAMg7BAAAQikAAAAikIAAnTQAAikCkAAg");
	this.shape_40.setTransform(0.4,4.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#DE2020").s().p("A9gGOQikgBAAijIAAnTQAAikCkAAMA7BAAAQCkAAAACkIAAHTQAACjikABg");
	this.shape_41.setTransform(0.4,4.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-207.4,-37.6,415.6,84.5), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy4, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Tween28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy2();
	this.instance.parent = this;
	this.instance.setTransform(149.3,-0.1,1,1,0,0,0,58,55.1);

	this.instance_1 = new lib.Symbol6copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.8,2.3,1,1,0,0,0,54.5,33);

	this.instance_2 = new lib.Symbol5copy3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-147.6,2.1,1,1,0,0,0,59.6,51.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape.setTransform(0,0,1.08,1.08);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_1.setTransform(0,0,1.08,1.08);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E02B2B").s().p("A+sNrQh8AAhZhYQhXhYgBh8IAAx9QABh8BXhYQBZhYB8AAMA9aAAAQB8AABXBYQBZBYAAB8IAAR9QAAB8hZBYQhXBYh8AAg");
	this.shape_2.setTransform(0,0,1.08,1.08);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_3.setTransform(0,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-249.8,-98.2,499.7,196.4);


(lib.Tween15copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(299.7,-18.9,1.08,1.08,0,0,0,218.1,122);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(53.6,-98.1,499.7,196.4);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy2();
	this.instance.parent = this;
	this.instance.setTransform(-154.1,-0.1,1,1,0,0,0,58,55.1);

	this.instance_1 = new lib.Symbol6copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-302.6,2.3,1,1,0,0,0,54.5,33);

	this.instance_2 = new lib.Symbol5copy3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-451,2.1,1,1,0,0,0,59.6,51.6);

	this.instance_3 = new lib.Symbol3copy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(299.7,-18.9,1.08,1.08,0,0,0,218.1,122);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape.setTransform(-303.4,0,1.08,1.08);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_1.setTransform(-303.4,0,1.08,1.08);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E02B2B").s().rr(-226.55,-87.55,453.1,175.1,30);
	this.shape_2.setTransform(-303.4,0,1.08,1.08);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_3.setTransform(-303.4,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-553.2,-98.2,1106.5,196.5);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy2();
	this.instance.parent = this;
	this.instance.setTransform(-154.1,-0.1,1,1,0,0,0,58,55.1);

	this.instance_1 = new lib.Symbol6copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-302.6,2.3,1,1,0,0,0,54.5,33);

	this.instance_2 = new lib.Symbol5copy3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-451,2.1,1,1,0,0,0,59.6,51.6);

	this.instance_3 = new lib.Symbol3copy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(299.7,-18.9,1.08,1.08,0,0,0,218.1,122);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A+sMgQhdAAhDhCQhBhCAAhdIAAx9QAAhdBBhCQBDhCBdAAMA9aAAAQBcAABCBCQBCBCABBdIAAR9QgBBdhCBCQhCBChcAAg");
	this.shape.setTransform(-303.4,0,1.08,1.08);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D9CBCB").s().p("AeuM5Mg9aAAAQhoAAhJhJQhKhJAAhoIAAx9QAAhoBKhJQBJhJBoAAMA9aAAAQBnAABJBJQBKBJgBBoIAAR9QABBohKBJQhJBJhnAAIAAAAg");
	this.shape_1.setTransform(-303.4,0,1.08,1.08);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E02B2B").s().rr(-226.55,-87.55,453.1,175.1,30);
	this.shape_2.setTransform(-303.4,0,1.08,1.08);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AeuOEMg9aAAAQiHAAhfhfQhghfABiHIAAx9QgBiHBghfQBfhfCHAAMA9aAAAQCGAABfBfQBgBfAACHIAAR9QAACHhgBfQhfBfiGAAIAAAAg");
	this.shape_3.setTransform(-303.4,0,1.091,1.091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-553.2,-98.2,1106.5,196.5);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy2();
	this.instance.parent = this;
	this.instance.setTransform(147.3,0,1,1,0,0,0,58,55.1);

	this.instance_1 = new lib.Symbol6copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.8,2.9,1,1,0,0,0,54.5,33);

	this.instance_2 = new lib.Symbol5copy3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-145.6,3.7,1,1,0,0,0,59.6,51.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-205.2,-55.1,410.5,110.4);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7copy2();
	this.instance.parent = this;
	this.instance.setTransform(149.3,0,1,1,0,0,0,58,55.1);

	this.instance_1 = new lib.Symbol6copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(13.2,2.4,1,1,0,0,0,54.5,33);

	this.instance_2 = new lib.Symbol5copy3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-147.6,2.2,1,1,0,0,0,59.6,51.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-207.2,-55.1,414.4,110.4);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween26("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(228.7,28.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03,y:28},9).to({scaleX:1,scaleY:1,y:28.1},10).to({scaleX:1.03,scaleY:1.03,y:28},10).to({scaleX:1,scaleY:1,y:28.1},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0,460.5,56.1);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1},9).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-245.9,-96.6,491.9,193.4);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy4();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol3copy3();
	this.instance.parent = this;
	this.instance.setTransform(494,231.2,1.701,1.701,0,0,0,218.2,122);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1_1, new cjs.Rectangle(106.2,106.3,786.9,309.3), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(343.4,579.3,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(326).to({_off:false},0).to({startPosition:13},52).to({alpha:0,startPosition:19},6).wait(1));

	// Layer_6
	this.instance_1 = new lib.Tween28("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(337.3,563.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(314).to({_off:false},0).to({regY:0.1,scaleX:1.1,scaleY:1.1},23).to({startPosition:0},41).to({alpha:0},6).wait(1));

	// hand
	this.instance_2 = new lib.handanim("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(360.1,642.1,1,1,0,0,0,41,60.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(264).to({_off:false},0).to({_off:true},32).wait(89));

	// arrow
	this.instance_3 = new lib.arrowanim("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(353.6,416.6,1,1,0,0,0,41,60.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(232).to({_off:false},0).to({_off:true},32).wait(121));

	// Layer_5
	this.instance_4 = new lib.Tween6("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(337.3,563.9);
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween12("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(642.4,317.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(296).to({_off:false},0).to({_off:true,x:642.4,y:317.3},18).wait(71));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(296).to({_off:false},18).to({startPosition:0},64).to({alpha:0},6).wait(1));

	// Layer_14
	this.instance_6 = new lib.Tween16("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(789.8,316.8);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(168).to({_off:false},0).to({x:1086.9,y:565.1,alpha:0.602},20).wait(36).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(155));

	// Layer_12
	this.instance_7 = new lib.Tween18("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(654.2,319.1);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(168).to({_off:false},0).to({x:943.6,y:565.9},20).wait(36).to({x:944},0).to({alpha:0},5).to({_off:true},1).wait(155));

	// Layer_10
	this.instance_8 = new lib.Tween20("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(499.2,319.8);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(168).to({_off:false},0).to({x:800.3,y:569.7,alpha:0.602},20).wait(36).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(155));

	// Layer_13
	this.instance_9 = new lib.Tween17("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(789.8,316.8);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(133).to({_off:false},0).to({x:486.7,y:564.1},20).wait(71).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(155));

	// Layer_11
	this.instance_10 = new lib.Tween19("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(654.2,319.1);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(133).to({_off:false},0).to({x:338.3,y:565.9,alpha:0.602},20).wait(71).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(155));

	// valid
	this.instance_11 = new lib.Tween21("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(499.2,319.8);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(133).to({_off:false},0).to({x:190,y:565.6,alpha:0.602},20).wait(71).to({startPosition:0},0).to({alpha:0},5).to({_off:true},1).wait(155));

	// Layer_8
	this.instance_12 = new lib.Symbol2("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(642.6,316.6);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(91).to({_off:false},0).to({_off:true},42).wait(252));

	// Layer_4
	this.instance_13 = new lib.Symbol17("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(639.1,140.6,1,1,0,0,0,228.7,28.1);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(91).to({_off:false},0).wait(133).to({startPosition:10},0).to({alpha:0,startPosition:14},5).to({_off:true},1).wait(155));

	// Layer_7
	this.instance_14 = new lib.Tween14("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(640.7,563.9);
	this.instance_14.alpha = 0.02;
	this.instance_14._off = true;

	this.instance_15 = new lib.Tween15("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(640.7,563.9);

	this.instance_16 = new lib.Tween15copy2("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(640.7,563.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14}]},67).to({state:[{t:this.instance_15}]},7).to({state:[{t:this.instance_16,p:{alpha:1}}]},240).to({state:[{t:this.instance_16,p:{alpha:1}}]},64).to({state:[{t:this.instance_16,p:{alpha:0}}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(67).to({_off:false},0).to({_off:true,alpha:1},7).wait(311));

	// Layer_3
	this.instance_17 = new lib.Symbol1_1();
	this.instance_17.parent = this;
	this.instance_17.setTransform(642.7,327,0.625,0.625,0,0,0,499.8,277.6);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(43).to({_off:false},0).to({alpha:1},5).wait(330).to({alpha:0},6).wait(1));

	// Layer_2
	this.instance_18 = new lib.Symbol1();
	this.instance_18.parent = this;
	this.instance_18.setTransform(639.5,117.6,1.315,1.315);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(20).to({_off:false},0).to({alpha:1},5).wait(353).to({alpha:0},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(495,271.3,1576,904);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;