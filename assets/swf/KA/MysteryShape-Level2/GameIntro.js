(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgagZIhLACIABgoIBLgCIAChgIAqgCIgBBhIBUgDIgDApIhSACIgBC9IgtABg");
	this.shape.setTransform(127.9,1.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhiA2IgCg2IgBgqIgCgfIgBgvIAygBIABAcIAAAZIAAAaIAPgWQAJgMAMgKQAMgLAMgIQAOgIAPgCQARgBAPAJQAGADAGAGQAGAGAFAJQAFAJAEAMQADANABARIgrAQQAAgLgBgIIgEgNIgGgJIgHgHQgHgFgKAAQgGABgHAEIgNALIgOAPIgPASIgMARIgLAPIABAgIABAeIABAYIAAASIguAGIgDhEg");
	this.shape_1.setTransform(106.3,5.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("ABEB8IADgeQgbAQgZAIQgZAIgWAAQgQAAgPgEQgOgEgMgJQgLgIgHgNQgHgNAAgRQAAgTAGgOQAFgNAJgLQAJgJANgJQAMgHAOgFQAPgFAPgCQAPgCAPAAIAbABIAWADQgCgLgFgLQgEgLgGgHQgHgJgJgGQgJgEgNAAQgJgBgKADQgLACgNAHQgNAGgPAMQgQALgSASIgagfQAVgVATgMQATgMARgHQARgHAPgDQANgCAMAAQAUAAAPAGQAQAHAMAMQAMAMAIAQQAIAQAGASQAFATACAUQACASAAAUQAAAVgCAYQgCAYgFAdgAgKgBQgSAEgNAJQgOAJgGAMQgHAMAEARQADANAJAFQAJAFAMABQANAAAPgFIAcgKIAbgNIAWgNIABgVIgCgXQgKgCgMgCQgMgCgMAAQgTAAgSAEg");
	this.shape_2.setTransform(78.3,5.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ah3iwIAwgCIgBAgQANgMANgHQAOgHALgDQANgEAMgBQAPAAAPAEQAOAEANAIQANAIALALQAMALAHANQAIANAFAQQAEAPgBARQAAATgGARQgFAQgIANQgIAOgLALQgLALgMAIQgMAIgNAEQgOAEgNAAQgMgBgOgDQgMgDgOgHQgPgGgOgMIgBCUIgoAAgAAAiIQgNAAgMAFQgMAFgJAHQgKAIgGAKQgHALgDALIgBApQAEAPAHAKQAGALAKAHQAJAHAMAEQALAEALAAQAOAAAOgGQANgFAKgKQAKgKAGgOQAGgOABgQQABgQgFgPQgFgPgJgLQgKgLgOgHQgMgGgPAAIgCAAg");
	this.shape_3.setTransform(51.2,11.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AgvC2QgKgDgKgEQgLgFgJgHQgKgHgIgKQgIgKgFgNIAqgTQAEAKAGAGQAGAHAHAFQAGAEAHADQAHADAHABQAQAEARgCQAPgDALgHQAMgGAHgIQAIgIAFgIQAFgJADgIQADgHABgHIABgKIAAgcQgNAMgOAHQgPAGgMADQgNAEgNABQgZAAgWgJQgWgJgQgQQgQgRgJgXQgKgXAAgeQAAgdALgXQAKgYARgQQAQgQAVgIQAVgIAWAAQANABAPAFQANAEAPAIQAPAIANANIAAgsIArACIgBD5QgBAOgDAPQgEAOgGANQgHANgKAMQgLALgNAJQgNAJgQAGQgRAGgTABIgGABQgXAAgUgGgAggiEQgOAGgJAMQgKALgFAQQgFAPAAASQAAASAFAPQAGAPAJAKQAKAKAOAHQANAGAQAAQANAAAOgFQANgFAMgJQAKgIAIgNQAIgNADgPIAAgcQgDgQgIgMQgIgNgLgJQgLgKgOgFQgOgFgNAAQgQAAgNAHg");
	this.shape_4.setTransform(9,10.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AhlA2IgCg2IgBgqIgCgfIgBgvIAygBIABA/QAHgMAJgLQAJgLALgJQALgIALgFQAMgGANgBQARgBAMAEQAMAEAJAHQAJAHAFAKQAGAJADAKQAEAKABAKIACATQABAkAAAmIgCBPIgtgCIAEhGQABgjgCgjIAAgKIgDgNIgFgOQgDgHgFgFQgGgFgHgDQgIgCgKABQgRADgRAXQgRAWgVArIACA4IAAAfIABARIgvAGIgChEg");
	this.shape_5.setTransform(-18.9,5.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AgbCkIABg4IABhEIAChdIAsgCIgBA5IgBAvIgBAmIAAAdIgBAwgAgLhnQgGgDgEgEQgEgEgCgGQgDgFAAgHQAAgHADgFQACgGAEgEQAEgEAGgCQAGgDAFAAQAGAAAGADQAGACAEAEIAHAKQACAFAAAHQAAAHgCAFQgDAGgEAEQgEAEgGADQgGACgGAAQgFAAgGgCg");
	this.shape_6.setTransform(-37.8,0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgfB7IgSgDIgUgIIgVgJQgJgFgKgHIAWghIAYALQAMAHANAEQANAEAOADQANACAPAAQAMAAAIgCQAIgCAGgDQAFgDADgFIAFgIQABgEgBgEQAAgEgCgFQgCgEgEgEQgFgFgHgDQgHgDgJgCQgLgDgNAAQgTgBgSgDQgSgDgPgHQgOgHgKgJQgIgMgDgQQgBgQADgNQAEgNAIgKQAGgLAMgHQAKgIAMgFQANgFAOgCQAOgEAOAAIAUABIAYAEQANADAMAFQANAFAMAHIgQApQgOgIgNgFIgXgGIgWgEQghgCgTAKQgTAJAAATQAAANAHAFQAHAGAMADQANACAQABIAjAEQAVAEAPAGQAOAGAKAJQAIAIAEALQAFAKAAALQAAAWgJANQgIAOgPAKQgOAIgSAEQgSAFgUABQgTAAgUgFg");
	this.shape_7.setTransform(-56.2,6.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AggB7IgSgDIgUgIIgTgJQgLgFgIgHIAUghIAZALQANAHANAEQANAEAOADQAMACAPAAQAMAAAIgCQAIgCAGgDQAFgDADgFIAEgIQACgEgBgEQAAgEgCgFQgCgEgFgEQgEgFgGgDQgIgDgKgCQgKgDgOAAQgSgBgSgDQgTgDgNgHQgPgHgJgJQgKgMgBgQQgCgQAEgNQADgNAHgKQAIgLAKgHQALgIANgFQANgFAOgCQAOgEANAAIAUABIAYAEQANADANAFQAMAFALAHIgOApQgPgIgMgFIgYgGIgWgEQghgCgTAKQgTAJAAATQAAANAHAFQAHAGAMADQANACARABIAiAEQAWAEAOAGQAPAGAIAJQAJAIAFALQADAKAAALQAAAWgIANQgIAOgOAKQgOAIgTAEQgSAFgTABQgUAAgVgFg");
	this.shape_8.setTransform(-81,6.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgbCkIABg4IAChEIABhdIAsgCIgBA5IgBAvIgBAmIgBAdIAAAwgAgLhnQgGgDgEgEQgEgEgDgGQgCgFAAgHQAAgHACgFQADgGAEgEQAEgEAGgCQAGgDAFAAQAGAAAGADQAGACAEAEIAHAKQACAFAAAHQAAAHgCAFQgDAGgEAEQgEAEgGADQgGACgGAAQgFAAgGgCg");
	this.shape_9.setTransform(-98.5,0.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("ABoA0IAAgvIAAgjQgBgRgFgJQgGgJgHAAQgGAAgFADIgMAJIgLAMIgKAOIgJANIgIAMIABAUIABAaIAAAhIAAApIgoACIgBhEIgCgvIgDgjQAAgRgFgJQgFgJgJAAQgFAAgGAEIgMAJIgMAOIgKAPIgKAPIgIALIABBzIgrACIgFjqIAtgFIABBAIAOgSQAIgJAJgIQAJgIALgEQALgFAMAAQAKAAAIADQAIACAHAHQAGAGAFAJQAFAKACANIANgRQAIgJAJgIQAJgHAKgFQALgEAMAAQAKAAAJADQAJADAHAHQAHAHAEALQAFAKABAPIACAoIACA0IAABNIgtACIgBhEg");
	this.shape_10.setTransform(-122.5,5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(3,1,1).p("A0KjvMAoVAAAQCgAAAACgIAACfQAACgigAAMgoVAAAQigAAAAigIAAifQAAigCgAAg");
	this.shape_11.setTransform(0.8,5.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("A0KDwQifAAAAigIAAifQAAigCfAAMAoUAAAQChAAAACgIAACfQAACgihAAg");
	this.shape_12.setTransform(0.8,5.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-145.8,-36.1,293.1,72.2);


(lib.Symbol8copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC0000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape.setTransform(19.2,25.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy2, new cjs.Rectangle(0,0,38.3,51), null);


(lib.Symbol4copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AqCE8QADkDC4i5QC9i7EKAAQELAAC7C7QC5C5AEEDAmtE8QADiqB5h7QB/h8CxAAQCwAAB+B8QB6B7AECq");
	this.shape.setTransform(84.3,51.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AGrE8QgEiqh7h7Qh8h8ixAAQixAAh+B8Qh5B7gECqIjVAAQADkDC5i5QC8i7EKAAQELAAC7C7QC5C5AEEDg");
	this.shape_1.setTransform(84.3,51.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#336600").s().p("AKCGgQgEkDi5i5Qi8i7kJAAQkLAAi9C7Qi4C5gDEDIjIAAIAAs/IaXAAIAAM/gAmuGgQADiqB5h7QB/h8CxAAQCwAAB9B8QB7B7AECqg");
	this.shape_2.setTransform(84.4,41.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy4, new cjs.Rectangle(0,0,168.8,84.3), null);


(lib.Symbol4copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AqCE8QADkDC4i5QC9i7EKAAQELAAC7C7QC5C5AEEDAmtE8QADiqB5h7QB/h8CxAAQCwAAB+B8QB6B7AECq");
	this.shape.setTransform(84.3,51.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AGrE8QgEiqh7h7Qh8h8ixAAQixAAh+B8Qh5B7gECqIjVAAQADkDC5i5QC8i7EKAAQELAAC7C7QC5C5AEEDg");
	this.shape_1.setTransform(84.3,51.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#336600").s().p("AKCGgQgEkDi5i5Qi8i7kJAAQkLAAi9C7Qi4C5gDEDIjIAAIAAs/IaXAAIAAM/gAmuGgQADiqB5h7QB/h8CxAAQCwAAB9B8QB7B7AECqg");
	this.shape_2.setTransform(84.4,41.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy2, new cjs.Rectangle(0,0,168.8,84.3), null);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AqCE8QADkDC4i5QC9i7EKAAQELAAC7C7QC5C5AEEDAmtE8QADiqB5h7QB/h8CxAAQCwAAB+B8QB6B7AECq");
	this.shape.setTransform(84.3,51.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AGrE8QgEiqh7h7Qh8h8ixAAQixAAh+B8Qh5B7gECqIjVAAQADkDC5i5QC8i7EKAAQELAAC7C7QC5C5AEEDg");
	this.shape_1.setTransform(84.3,51.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#336600").s().p("AKCGgQgEkDi5i5Qi8i7kJAAQkLAAi9C7Qi4C5gDEDIjIAAIAAs/IaXAAIAAM/gAmuGgQADiqB5h7QB/h8CxAAQCwAAB9B8QB7B7AECqg");
	this.shape_2.setTransform(84.4,41.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4copy, new cjs.Rectangle(0,0,168.8,84.3), null);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_1.setTransform(-11.7,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol8copy2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC0000").s().p("AgwD/QhCgGALg3QAQgdAlgFQA0gDARAsQAAA2g9AAIgGAAgAhmB4IgDgLIgBgDIgBgFIgBgEIgCgHIgBgCIgCgMIgDgLIgCgEIAAgFIgFgSIAAgCIgEgPIgCgLIgIgdIAAgCIgHgZIgBgIIAwgBIAEgBIAVgBIADgBIAQgCIARgCIADAAIAMgDIAHgCIALgDIACAAIALgEIADAAIAOgHIAGgDIADgBQAIgGAFgMQgDgKgJgIIgIgEIgFgEIgrgNIgCAAIgFgBIgCgBIgSgCIgDgCIgLgBIgCAAIgCgBIgPgBIgNAAIgCgCIgHAAIgEAAIgQAAIgEgBIgtABIgBgJIgCgGIAAgCQgCgEgBgFIAAgCIgEgSIgEgNQgBgFgCgHIgDgMIAAgBIgBgHIARgCIAEAAQAFAAACgCIAMAAIALgBIAYgCIAJgBIAYgCIBaAAIADAAIAXACIACABIAKABIAJABIAFABIADAAIAIACIADAAIADABIALACIALADIAHADIATAGIAYAMIABABIAMAIQASAOAKAXQADAIgBAMIAAAVIgCAMIgGARIgDAFIgCAEIgHANIgRATIgFAEIgFAEIgCADIgNAJIgBAAIgJAHIgFACIgDADIgCAAIgOAIIgKAEIgIADIgKAFIgfAKIgCAAIgHADIgCAAIgLADIgJACIgPAFIgFAAIgFACIgFAAIgCABQgBAFABAIIABADIAAABIAAAJIABAHIABACIABARIAAAFIACAGIAAAFIACALIAAAHIABAKIABACIAAAFIACAJIAAAEIgOABIgJABQgGACgIAAIgCAAIgKABIgJAAIgZADg");
	this.shape_1.setTransform(19.2,25.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8copy2_1, new cjs.Rectangle(0,0,38.3,51), null);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Shapesqustcopy17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("APEgDIAAvAI+HAAIAAPAIAAPHIeHAAgAvDgDIeHAA");
	this.shape.setTransform(-1,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_1.setTransform(-1,-49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AKDAAQAAELi9C7Qi7C9kLAAQkKAAi9i9Qi7i7AAkLQAAkKC7i9QC9i7EKAAQELAAC7C7QC9C9AAEKgAGrAAQAACxh+B9Qh9B/iwAAQiyAAh/h/Qh8h9AAixQAAiwB8h/QB/h9CyAAQCwAAB9B9QB+B/AACwg");
	this.shape_2.setTransform(-0.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99CC00").s().p("AnGHGQi8i7AAkLQAAkJC8i+QC8i7EKAAQEKAAC8C7QC9C+AAEJQAAELi9C7Qi8C9kKAAQkKAAi8i9gAkxkvQh8B/AACwQAACyB8B8QB/B/CxAAQCxAAB9h/QB+h8AAiyQAAiwh+h/Qh9h9ixAAQixAAh/B9g");
	this.shape_3.setTransform(-0.1,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AtLNMIAA6XIaXAAIAAaXg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_5.setTransform(-1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy17, new cjs.Rectangle(-99.4,-99.4,196.8,196.8), null);


(lib.Shapesqustcopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("APEgDIAAvAI+HAAIAAPAIAAPHIeHAAgAvDgDIeHAA");
	this.shape.setTransform(-1,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_1.setTransform(-1,-49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AKDAAQAAELi9C7Qi7C9kLAAQkKAAi9i9Qi7i7AAkLQAAkKC7i9QC9i7EKAAQELAAC7C7QC9C9AAEKgAGrAAQAACxh+B9Qh9B/iwAAQiyAAh/h/Qh8h9AAixQAAiwB8h/QB/h9CyAAQCwAAB9B9QB+B/AACwg");
	this.shape_2.setTransform(-0.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99CC00").s().p("AnGHGQi8i7AAkLQAAkJC8i+QC8i7EKAAQEKAAC8C7QC9C+AAEJQAAELi9C7Qi8C9kKAAQkKAAi8i9gAkxkvQh8B/AACwQAACyB8B8QB/B/CxAAQCxAAB9h/QB+h8AAiyQAAiwh+h/Qh9h9ixAAQixAAh/B9g");
	this.shape_3.setTransform(-0.1,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AtLNMIAA6XIaXAAIAAaXg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_5.setTransform(-1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// Layer 5
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(0,0,0,0.027)").s().p("AyLSLMAAAgkVMAkXAAAMAAAAkVg");
	this.shape_7.setTransform(0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy6, new cjs.Rectangle(-116.4,-116.3,232.8,232.7), null);


(lib.questiontext_mccopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgMgMIglABIAAgUIAlgBIABgvIAVgBIgBAwIApgBIgBATIgoABIgBBeIgWAAg");
	this.shape.setTransform(113.9,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgvAbIgBgbIgBgUIAAgPIgCgYIAZAAIAAANIAAANIAAANIAIgLQAEgGAGgFQAGgFAGgEQAGgEAIgBQAIgBAIAFIAGAEIAFAHIAFALIACAPIgVAIIgCgJIgCgHIgCgFIgDgDQgEgDgFABIgGACIgHAGIgGAHIgHAIIgGAJIgGAHIAAAPIABAPIAAANIABAIIgXADIgBghg");
	this.shape_1.setTransform(103.2,2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AAhA9IACgPQgNAIgNAEQgLAEgLAAQgIAAgIgCQgHgCgFgEQgGgEgDgHQgEgGAAgJQAAgJADgHQADgGAEgGQAEgEAHgEIANgGQAHgCAIgBIAOgBIANAAIALABIgDgKIgGgJQgDgFgFgCQgEgDgGAAIgJACQgFABgHADIgOAJQgIAFgIAJIgNgQQAKgJAKgGQAJgGAIgEQAJgDAHgCIALgBQALAAAHADQAIAEAGAGQAGAFAEAIIAGARQADAJABAKIABATIgBAWIgEAagAgEAAQgKABgGAFQgGAEgEAGQgDAGACAIQABAHAEACQAFADAGAAQAHAAAGgCIAOgFIANgHIALgGIAAgLIAAgLIgLgBIgLgBQgLAAgHACg");
	this.shape_2.setTransform(89.5,2.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("Ag6hWIAXgBIAAAPQAGgFAHgEIAMgFIAMgDQAHABAHACQAHACAHAEQAGADAGAGQAFAGAEAGQAEAGACAIQACAIAAAIQAAAJgDAJQgCAHgEAGQgEAIgGAFQgFAGgGADQgGAEgHACQgGACgHAAIgMgCIgNgFQgHgDgHgGIgBBJIgUAAgAgMhAQgGADgEADQgFAEgDAFQgEAFgBAGIgBAUQACAHADAFQAEAGAFACQAEAEAGACIALACQAGAAAHgDQAHgDAEgEQAFgFADgHQADgHABgIQAAgIgCgHQgDgHgEgGQgFgFgHgEQgHgCgHAAQgGAAgGACg");
	this.shape_3.setTransform(76.1,5.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AgXBaIgKgEIgKgGIgJgIQgDgFgDgHIAVgJQACAFADADQACAEAEACIAHAEIAGACQAIABAIgBQAHgBAHgDQAFgDAEgEIAGgIIAEgIIABgHIABgFIAAgOQgGAGgHADIgNAFQgHACgFAAQgNAAgLgEQgKgFgJgIQgHgIgFgLQgEgLgBgPQAAgOAGgMQAFgLAHgIQAJgIAKgEQAKgEALAAIAOADIAOAGQAHAEAGAGIAAgVIAWAAIgBB7IgCAOIgFANIgIAMQgFAGgHAEQgGAFgJADQgIADgKAAIgCAAQgLAAgKgCgAgQhAQgGADgEAFQgFAGgDAHQgDAIABAJQgBAJADAHQADAIAFAEQAEAFAHADQAGADAJAAQAFAAAHgCQAHgDAFgEQAGgEAEgGQADgGABgHIAAgPQgBgHgDgGQgEgHgGgEQgFgFgIgCQgGgDgGAAQgIAAgHAEg");
	this.shape_4.setTransform(55.3,5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AgxAbIgBgbIgBgUIAAgPIgBgXIAYgBIABAfIAIgLQAEgGAGgEQAFgEAFgDQAGgCAGgBQAJAAAGACQAGACAEADIAHAIIAFAKIACAKIABAJIAAAkIgBAnIgWgBIACgjQABgRgBgQIAAgFIgCgHIgCgHIgEgGIgHgEQgDgBgFABQgJABgIALQgIALgKAVIAAAbIABAQIAAAIIgXADIgBghg");
	this.shape_5.setTransform(41.6,2.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AgNBRIABgcIABgiIAAgtIAVgBIAAAcIgBAXIAAATIAAAOIgBAYgAgFgyIgFgEIgDgFQgBgCAAgDQAAgEABgCIADgFIAFgEIAFgBIAGABIAFAEIADAFIABAGIgBAFIgDAFIgFAEIgGABIgFgBg");
	this.shape_6.setTransform(32.2,0.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgPA9IgJgCIgJgEIgKgEIgKgHIALgQIALAGIANAFIANAEIAOABIAKgBIAGgDIAEgEIACgEIAAgEIgBgEIgCgEIgGgEIgJgDIgLgBIgSgCQgJgCgHgDQgHgDgEgFQgFgFgBgIQAAgIABgGQACgHAEgFQADgFAGgEQAEgDAHgDIANgEIANgBIAKAAIAMACIANAEQAFACAGAEIgIAUIgMgGIgMgDIgLgCQgQgBgJAFQgKAEABAJQAAAHADADQAEADAGABIAOACIARABQAKACAHADQAHADAFAEQAEAEACAFQACAGAAAFQAAAKgEAHQgFAHgGAFQgHAEgJACQgJACgKABQgJAAgKgCg");
	this.shape_7.setTransform(23.2,3.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgPA9IgJgCIgKgEIgKgEIgJgHIAKgQIANAGIAMAFIAOAEIAMABIAKgBIAHgDIAEgEIACgEIABgEIgBgEIgEgEIgFgEIgIgDIgMgBIgSgCQgJgCgHgDQgHgDgFgFQgEgFgBgIQgBgIACgGQACgHADgFQAEgFAFgEQAGgDAGgDIANgEIANgBIAKAAIAMACIAMAEQAHACAFAEIgHAUIgOgGIgLgDIgLgCQgQgBgJAFQgKAEAAAJQABAHADADQADADAGABIAPACIAQABQALACAHADQAHADAEAEQAFAEACAFQACAGAAAFQAAAKgEAHQgFAHgGAFQgHAEgJACQgJACgKABQgJAAgKgCg");
	this.shape_8.setTransform(11,3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgNBRIABgcIABgiIAAgtIAVgBIAAAcIgBAXIAAATIAAAOIgBAYgAgFgyIgFgEIgDgFQgBgCAAgDQAAgEABgCIADgFIAFgEIAFgBIAGABIAFAEIADAFIABAGIgBAFIgDAFIgFAEIgGABIgFgBg");
	this.shape_9.setTransform(2.3,0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AA0AaIgBgXIAAgRQAAgJgDgEQgCgEgEgBIgGACIgFAFIgGAFIgFAHIgEAHIgEAGIABAJIAAAMIAAARIAAAUIgUABIAAghIgBgXIgBgRQAAgJgDgEQgCgEgEgBQgDAAgDACIgGAFIgGAHIgFAHIgFAHIgEAGIABA4IgVABIgDhzIAWgCIABAgIAHgJIAIgJIAKgGQAFgDAHABQAEgBAEACQAEACADACQADADADAFQACAFABAHIAHgJIAIgIQAEgEAFgCQAFgDAGABQAFgBAFACIAIAFQADAEACAFQADAFAAAIIABATIABAaIAAAlIgWABIAAghg");
	this.shape_10.setTransform(-9.5,2.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgXA6QgLgFgIgIQgHgHgEgMQgEgLAAgNQAAgMAFgMQAEgMAIgJQAIgIALgFQAKgFAMAAQALAAAKAFQAKAEAHAHQAIAIAFAKQAFALABAMIhcANQABAHADAHQADAGAFAEQAEAEAHADQAGACAGAAQAGgBAFgBQAGgCAEgEQAFgDAEgFQADgFACgGIAVADQgDALgGAHQgFAJgIAFQgHAGgJADQgJADgJAAQgNAAgLgEgAgIgoQgFABgFADQgFAEgEAGQgFAHgBAJIBAgIIgBgBQgEgMgHgGQgHgFgLgBQgEAAgFADg");
	this.shape_11.setTransform(-30.5,2.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AAdBRQADgJABgLIACgRIAAgRQAAgLgDgJQgEgHgFgFQgFgFgGgCQgGgCgGAAQgFAAgGADQgGADgFAFQgHAFgFAKIgBBGIgVABIgBilIAYgBIAABCQAGgHAGgEIAMgFIANgDQALAAAKAFQALADAGAIQAIAJAEALQAEALABAOIAAAQIgBAQIgCAOIgDANg");
	this.shape_12.setTransform(-44.2,0.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgMgMIglABIAAgUIAlgBIABgvIAVgBIgBAwIApgBIgBATIgoABIgBBeIgWAAg");
	this.shape_13.setTransform(-56.8,0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgMgMIglABIAAgUIAlgBIABgvIAVgBIgBAwIApgBIgBATIgoABIgBBeIgWAAg");
	this.shape_14.setTransform(-72.9,0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AgTA6QgLgFgJgIQgJgJgFgLQgGgLABgOQAAgHACgIQACgIAEgGQAEgIAGgFQAGgGAGgFQAIgEAIgCQAHgCAJgBQAJABAJACQAIACAHAEQAHAFAGAFQAGAHAEAIIAAAAIgSALIAAgBQgDgGgEgDQgEgFgFgDIgLgFIgLgCQgIAAgIAEQgIAEgFAFQgGAGgEAHQgDAJAAAHQAAAJADAIQAEAIAGAFQAFAGAIAEQAIADAIAAQAGgBAFgBIAKgEIAIgGIAIgJIAAgBIATALIAAAAQgFAHgGAFQgGAHgHADQgIAFgHACQgJABgIAAQgMAAgLgEg");
	this.shape_15.setTransform(-84.8,2.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgXA6QgLgFgIgIQgHgHgEgMQgEgLAAgNQAAgMAFgMQAEgMAIgJQAIgIALgFQAKgFAMAAQALAAAKAFQAKAEAHAHQAIAIAFAKQAFALABAMIhcANQABAHADAHQADAGAFAEQAEAEAHADQAGACAGAAQAGgBAFgBQAGgCAEgEQAFgDAEgFQADgFACgGIAVADQgDALgGAHQgFAJgIAFQgHAGgJADQgJADgJAAQgNAAgLgEgAgIgoQgFABgFADQgFAEgEAGQgFAHgBAJIBAgIIgBgBQgEgMgHgGQgHgFgLgBQgEAAgFADg");
	this.shape_16.setTransform(-98,2.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AAGBdIgJgGIgIgJIgEgKQgGgMgBgQIADiHIAWAAIgBAjIgBAdIgBAXIAAASIgBAdQABAKACAHIACAHIAFAGIAGAEQADACAFAAIgDAVQgHAAgHgDg");
	this.shape_17.setTransform(-107,-1.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgXA6QgLgFgIgIQgHgHgEgMQgEgLAAgNQAAgMAFgMQAEgMAIgJQAIgIALgFQAKgFAMAAQALAAAKAFQAKAEAHAHQAIAIAFAKQAFALABAMIhcANQABAHADAHQADAGAFAEQAEAEAHADQAGACAGAAQAGgBAFgBQAGgCAEgEQAFgDAEgFQADgFACgGIAVADQgDALgGAHQgFAJgIAFQgHAGgJADQgJADgJAAQgNAAgLgEgAgIgoQgFABgFADQgFAEgEAGQgFAHgBAJIBAgIIgBgBQgEgMgHgGQgHgFgLgBQgEAAgFADg");
	this.shape_18.setTransform(-117.2,2.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgOBWIgPgEQgIgEgIgEIgPgKIAQgSQAIAHAJAEIAOAHQAIADAGAAQAIABAGgCQAHgBAFgEQAFgDADgEQADgEAAgFQABgEgCgDIgFgGIgIgFIgJgEIgLgCIgJgDIgNgDIgOgEQgGgDgGgFQgGgDgEgGQgEgGgCgIQgDgIABgLQABgIADgHQADgHAEgGQAFgFAGgEIANgHQAHgCAHgBIAOgBQAKAAAKADIAJADIAKAEQAFACAEAEIAJAGIgMATIgHgGIgIgGIgHgDIgIgDIgQgCQgJAAgHADQgIADgFAEQgFAGgDAFQgDAGAAAGQAAAFADAFQAEAFAFAEQAGAFAIADQAIACAIABIAQADIAOAEQAIAEAGAEIAKAKQAEAFACAHQABAHgBAIQgBAHgDAHQgDAFgFAFQgFAEgFADQgGADgHACIgNACIgMABQgIAAgIgCg");
	this.shape_19.setTransform(-130.9,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#CC6600").ss(3,1,1).p("A1bjEMAq3AAAQA8AAAqAqQAqAqAAA8IAABpQAAA8gqAqQgqAqg8AAMgq3AAAQg7AAgrgqQgqgqAAg8IAAhpQAAg8AqgqQArgqA7AAg");
	this.shape_20.setTransform(-5.9,1.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFCC00").s().p("A1bDFQg7AAgrgqQgqgqAAg9IAAhoQAAg7AqgrQArgqA7AAMAq3AAAQA8AAAqAqQAqArAAA7IAABoQAAA9gqAqQgqAqg8AAg");
	this.shape_21.setTransform(-5.9,1.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questiontext_mccopy4, new cjs.Rectangle(-159,-19.3,306.2,42.4), null);


(lib.ch1copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AqiFMIAAqXIVFAAIAAKX");
	this.shape.setTransform(0,-34.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AqiFMIAAqXIVFAAIAAKXg");
	this.shape_1.setTransform(0,-34.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#336600").s().p("AKjGgIAAqXI1FAAIAAKXIipAAIAAs/IaXAAIAAM/g");
	this.shape_2.setTransform(0,-42.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape_3.setTransform(-1,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_4.setTransform(-1,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_5.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_6.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy5, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.ch1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AJBleIAVANIyrKwIAAq9");
	this.shape.setTransform(0.1,34);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("ApVldISWAAIAVAMIyrKwg");
	this.shape_1.setTransform(0.1,34);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#336600").s().p("AtLGsIAAtWID2AAIAAK8ISsqwIgWgMIELAAIAANWg");
	this.shape_2.setTransform(0,41.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape_3.setTransform(-1,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_4.setTransform(-1,-0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_5.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_6.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy4, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4copy();
	this.instance.parent = this;
	this.instance.setTransform(0.2,-52,1.346,1.346,0,0,0,84.5,41.9);
	this.instance.alpha = 0.801;

	this.instance_1 = new lib.Shapesqustcopy6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-157.1,-157,314.3,314.2);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(145.8,36.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},9).to({scaleX:1.03,scaleY:1.03},10).to({scaleX:1,scaleY:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,293.1,72.2);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Shapesqustcopy16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.instance = new lib.Symbol8copy2();
	this.instance.parent = this;
	this.instance.setTransform(2.6,-51.1,1,1,0,0,0,19.2,25.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("APEgDIAAvAI+HAAIAAPAIAAPHIeHAAgAvDgDIeHAA");
	this.shape.setTransform(-1,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_1.setTransform(-1,-49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AC+AAYAABphVBVhpAAYhoAAhVhVAAhpYAAhoBVhVBoAAYBpAABVBVAABogAB/AAYAABGg5A5hGAAYhFAAg5g5AAhGYAAhFA5g5BFAAYBGAAA5A5AABFg");
	this.shape_2.setTransform(-0.2,0,3.375,3.375);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99CC00").s().p("Ai9AAQAAgfAJgcQAKgdARgXQASgYAYgSQAXgRAdgKQAcgJAfAAQAgAAAcAJQAdAKAXARQAYASASAYQARAXAKAdQAJAcAAAfQAAAggJAcQgKAdgRAXQgSAYgYASQgXARgdAKQgcAJggAAQgnAAgigPQgigOgbgaQgagbgOgiQgPgiAAgogAh+AAQAAAbAKAXQAJAXASARQARASAXAJQAXAKAaAAQAbAAAXgKQAXgJARgSQASgRAJgXQAKgXAAgbQgBg1gkgkQgkgkg2gBQgaAAgXAKQgXAJgRASQgSARgJAXQgKAXAAAag");
	this.shape_3.setTransform(-0.2,0,3.375,3.375);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AtLNMIAA6XIaXAAIAAaXg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_5.setTransform(-1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy16, new cjs.Rectangle(-99.4,-99.4,196.8,196.8), null);


(lib.Shapesqustcopy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.instance = new lib.Symbol8copy2_1();
	this.instance.parent = this;
	this.instance.setTransform(2.6,-51.1,1,1,0,0,0,19.2,25.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("APEgDIAAvAI+HAAIAAPAIAAPHIeHAAgAvDgDIeHAA");
	this.shape.setTransform(-1,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_1.setTransform(-1,-49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AKDAAQAAELi9C7Qi7C9kLAAQkKAAi9i9Qi7i7AAkLQAAkKC7i9QC9i7EKAAQELAAC7C7QC9C9AAEKgAGrAAQAACxh+B9Qh9B/iwAAQiyAAh/h/Qh8h9AAixQAAiwB8h/QB/h9CyAAQCwAAB9B9QB+B/AACwg");
	this.shape_2.setTransform(-0.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99CC00").s().p("AnGHGQi8i7AAkLQAAkJC8i+QC8i7EKAAQEKAAC8C7QC9C+AAEJQAAELi9C7Qi8C9kKAAQkKAAi8i9gAkxkvQh8B/AACwQAACyB8B8QB/B/CxAAQCxAAB9h/QB+h8AAiyQAAiwh+h/Qh9h9ixAAQixAAh/B9g");
	this.shape_3.setTransform(-0.1,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AtLNMIAA6XIaXAAIAAaXg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_5.setTransform(-1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy13, new cjs.Rectangle(-99.4,-99.4,196.8,196.8), null);


(lib.Shapesqustcopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// line
	this.instance = new lib.Symbol8copy2();
	this.instance.parent = this;
	this.instance.setTransform(2.6,-51.1,1,1,0,0,0,19.2,25.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("APEgDIAAvAI+HAAIAAPAIAAPHIeHAAgAvDgDIeHAA");
	this.shape.setTransform(-1,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AvDHgIAAu/IeHAAIAAO/g");
	this.shape_1.setTransform(-1,-49.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AC+AAYAABphVBVhpAAYhoAAhVhVAAhpYAAhoBVhVBoAAYBpAABVBVAABogAB/AAYAABGg5A5hGAAYhFAAg5g5AAhGYAAhFA5g5BFAAYBGAAA5A5AABFg");
	this.shape_2.setTransform(-0.2,0,3.375,3.375);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99CC00").s().p("Ai9AAQAAgfAJgcQAKgdARgXQASgYAYgSQAXgRAdgKQAcgJAfAAQAgAAAcAJQAdAKAXARQAYASASAYQARAXAKAdQAJAcAAAfQAAAggJAcQgKAdgRAXQgSAYgYASQgXARgdAKQgcAJggAAQgnAAgigPQgigOgbgaQgagbgOgiQgPgiAAgogAh+AAQAAAbAKAXQAJAXASARQARASAXAJQAXAKAaAAQAbAAAXgKQAXgJARgSQASgRAJgXQAKgXAAgbQgBg1gkgkQgkgkg2gBQgaAAgXAKQgXAJgRASQgSARgJAXQgKAXAAAag");
	this.shape_3.setTransform(-0.2,0,3.375,3.375);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#336600").s().p("AtLNMIAA6XIaXAAIAAaXg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#330000").ss(4,1,1).p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_5.setTransform(-1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AvDPEIAA+HIeHAAIAAeHg");
	this.shape_6.setTransform(-1,-1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Shapesqustcopy5, new cjs.Rectangle(-99.4,-99.4,196.8,196.8), null);


(lib.ch1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Symbol4copy();
	this.instance.parent = this;
	this.instance.setTransform(0.1,-42.4,1,1,0,0,0,84.4,41.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(4,1,1).p("AuryWIdXAAQBoAABJBJQBJBKAABnIAAc5QAABnhJBJQhJBKhoAAI9XAAQhoAAhJhKQhJhJAAhnIAA85QAAhnBJhKQBJhJBoAAg");
	this.shape.setTransform(-1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCCCC").s().p("AurSXQhnAAhKhKQhJhJAAhnIAA85QAAhnBJhKQBKhJBnAAIdXAAQBoAABIBJQBKBKAABnIAAc5QAABnhKBJQhIBKhoAAgAwVwGIgBAAQgrAsAAA+IAAc5QAAA+AsArQAsAtA+AAIdXAAQA+AAAsgsIAAgBQAsgrAAg+IAA85QAAg+gsgsQgsgsg+AAI9XAAQg+AAgsAsg");
	this.shape_1.setTransform(-1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AurQzQg+AAgsgtQgsgrAAg+IAA85QAAg+ArgsIABAAQAsgsA+AAIdXAAQA+AAAsAsQAsAsAAA+IAAc5QAAA+gsArIAAABQgsAsg+AAg");
	this.shape_2.setTransform(-1,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.027)").s().p("AvtUBQiBAAhchbQhchcAAiBIAA+RQAAiBBchcQBchbCBAAIfbAAQCCAABbBbQBcBcAACBIAAeRQAACBhcBcQhbBbiCAAg");
	this.shape_3.setTransform(-1.2,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.ch1copy3, new cjs.Rectangle(-133,-128.8,263.8,256.3), null);


(lib.arrowanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.ch1copy4();
	this.instance.parent = this;
	this.instance.setTransform(0.3,0.3,0.96,0.96,0,0,0,-0.9,-0.4);

	this.instance_1 = new lib.ch1copy5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(360.6,0.3,0.96,0.96,0,0,0,-0.9,-0.4);

	this.instance_2 = new lib.ch1copy3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-359.9,0.3,0.96,0.96,0,0,0,-0.9,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-486.8,-123,973.7,246);


(lib.handanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween2copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Shapesqustcopy13();
	this.instance.parent = this;
	this.instance.setTransform(131.5,131.6,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,regY:0.1,scaleX:1.42,scaleY:1.42,x:131.6},9).to({regX:0,regY:0,scaleX:1.35,scaleY:1.35,x:131.5,y:131.5},10).to({regX:0.1,regY:0.1,scaleX:1.42,scaleY:1.42,x:131.6,y:131.6},10).to({regX:0,regY:0,scaleX:1.35,scaleY:1.35,x:131.5},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.7,-2.7,265.7,265.7);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(638.8,320.1,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(313).to({_off:false},0).wait(60).to({startPosition:0},0).to({alpha:0,startPosition:6},6).wait(1));

	// Layer_7
	this.instance_1 = new lib.Symbol4copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(278.1,545.7,1,1,0,0,0,84.4,41.9);
	this.instance_1._off = true;

	this.instance_2 = new lib.Symbol4copy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(640.2,263.5,1.346,1.346,0,0,0,84.5,41.9);

	this.instance_3 = new lib.Symbol4copy4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(640.2,263.5,1.346,1.346,0,0,0,84.5,41.9);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1}]},284).to({state:[{t:this.instance_2}]},18).to({state:[{t:this.instance_3}]},6).to({state:[{t:this.instance_3}]},29).to({state:[{t:this.instance_3}]},36).to({state:[{t:this.instance_3}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(284).to({_off:false},0).to({_off:true,regX:84.5,scaleX:1.35,scaleY:1.35,x:640.2,y:263.5},18).wait(78));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(308).to({_off:false},0).to({scaleX:1.79,scaleY:1.79,y:252.6},29).wait(36).to({scaleX:1.79,x:640.1},0).to({alpha:0},6).wait(1));

	// Layer_8
	this.instance_4 = new lib.handanim("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(223.2,657.7,1,1,0,0,0,41,60.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(254).to({_off:false},0).to({_off:true},30).wait(96));

	// Layer_6
	this.instance_5 = new lib.arrowanim("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(280.4,412.4,1,1,0,0,0,41,60.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(224).to({_off:false},0).to({_off:true},30).wait(126));

	// Layer_9
	this.instance_6 = new lib.Symbol4copy();
	this.instance_6.parent = this;
	this.instance_6.setTransform(640.2,263.5,1.346,1.346,0,0,0,84.5,41.9);
	this.instance_6.alpha = 0.801;

	this.instance_7 = new lib.Shapesqustcopy6();
	this.instance_7.parent = this;
	this.instance_7.setTransform(640,315.5,1.35,1.35);

	this.instance_8 = new lib.Tween18("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,315.5);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7},{t:this.instance_6}]},171).to({state:[{t:this.instance_8}]},45).to({state:[{t:this.instance_8}]},5).to({state:[]},1).wait(158));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(216).to({_off:false},0).to({alpha:0.801},5).to({_off:true},1).wait(158));

	// Layer_11
	this.instance_9 = new lib.Symbol1copy("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(639.2,315.1,1,1,0,0,0,130.2,130.1);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(90).to({_off:false},0).to({_off:true},81).wait(209));

	// Layer_5
	this.instance_10 = new lib.Symbol9("synched",19);
	this.instance_10.parent = this;
	this.instance_10.setTransform(757.3,119.7,1,1,0,0,0,146.6,36.1);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(90).to({_off:false},0).wait(76).to({startPosition:15},0).to({alpha:0,startPosition:19},4).to({_off:true},1).wait(209));

	// Layer_2
	this.instance_11 = new lib.Tween14("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,586.7);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(74).to({_off:false},0).to({alpha:1},7).to({startPosition:0},221).to({alpha:0},15).to({startPosition:0},56).to({startPosition:0},6).wait(1));

	// Layer_1
	this.instance_12 = new lib.Shapesqustcopy5();
	this.instance_12.parent = this;
	this.instance_12.setTransform(640,315.5,1.35,1.35);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_13 = new lib.Shapesqustcopy16();
	this.instance_13.parent = this;
	this.instance_13.setTransform(640,315.5,1.35,1.35);

	this.instance_14 = new lib.Shapesqustcopy17();
	this.instance_14.parent = this;
	this.instance_14.setTransform(640,315.5,1.35,1.35);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},45).to({state:[{t:this.instance_13}]},7).to({state:[{t:this.instance_14}]},250).to({state:[{t:this.instance_14}]},6).to({state:[{t:this.instance_14}]},29).to({state:[{t:this.instance_14}]},36).to({state:[{t:this.instance_14}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(45).to({_off:false},0).to({_off:true,alpha:1},7).wait(328));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(302).to({_off:false},0).wait(6).to({regX:0.1,regY:0.1,scaleX:1.8,scaleY:1.8,x:640.1},29).wait(36).to({alpha:0},6).wait(1));

	// Layer_3
	this.questxt = new lib.questiontext_mccopy4();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(652.3,99.1,2.03,2.031,0,0,0,0.1,-10.2);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(19).to({_off:false},0).to({alpha:1},7).wait(276).to({regY:-10.1,x:652.2,y:99.2,alpha:0},15).to({scaleY:2.03,x:652.3},56).to({scaleY:2.03,x:652.2},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(581.5,322.4,1399,799);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;