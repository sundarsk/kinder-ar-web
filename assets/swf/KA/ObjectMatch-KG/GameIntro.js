(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#413030").s().p("AgLAKQgCgDAAgHQAAgGACgFQAGgCAFAAQAIAAABACQAFAFAAAGQAAAHgFADQgBAEgIAAQgFAAgGgEg");
	this.shape.setTransform(-75.6,-46);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EE6B18").s().p("AE1DlIgIgDQgJgDgFgFQgLhJgfhGQg3h8hkg3QhEgsiVgRQiIgHg2A4IgYgbIgGgMIgEgCQBfhMCGAAQChgEBTBQQBPBOA9BbQAsBIAuBOQAAAkggAgIAAAEQgFgEgGgBg");
	this.shape_1.setTransform(-53.9,-48.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663366").s().p("AkyBuQgZgHgWgKQg1gWgvgfQgRgMgQgIIgpghQgbgagVgZIgRgYQgUghgKgiQAFAEAfAEQALAAAVADQgHAWgEAYQASAUAWAgQAFAFACAFQAPgeAUgZIAhAbQBEA4BMAjQCJA+CoAAQEEAAC4iZQAIgEAFgHQAJAXAIAZIAYgTQApgeAWgcIgJgoIAvAAIAZAAIgZAnIgfAnQgkApgvAiQgWASgXANQgxAfgzAWQiZA/jBAAQikAAiHgug");
	this.shape_2.setTransform(-10.7,42.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0099FF").s().p("AnnCCQgcgTgXgTQgfgXgagYQhUhTgdhZICDg5QATgIAVgLQAKAiAUAhIARAYQAVAZAbAaIApAiQAQAIARAMQAvAeA1AWQAWAKAZAHQCHAuCjAAQDCAACZg/QAzgWAxgeQAXgNAWgSQAvgjAkgpIAfgnIAZgnIABAAQAABOgtBMQg9BthuA2QhdAxiDAPQhBAIiuAGIl6ABQhLgehAgsg");
	this.shape_3.setTransform(-19.2,47);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0066CC").s().p("Ai1DwQjsgBi9hSIF6gBQCugGBAgIQCDgPBdgxQBvg2A8htQAthMAAhOICeBTQgYA+g7BAIgeAdQgtAmg6ApQjwCilDAAIgKAAg");
	this.shape_4.setTransform(6.7,51.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EC6108").s().p("AI4EcQhUhAgsggQhLg2g9gRQgbgJgbAAQgGgGgHgCQA7gwA2gMQAdgJAbAEQAkABAqAaQAaAMAvAgQB4BBBggIQA3gEA5gaIgJAfQgPAtgRAbQgUAkggAaQgiAbgmAGIgQABQg9AAhLgwgAsFBNQgRgpgWgmQgeg2gqgwQAUgPAagRIAHgFQAyAnAcBHQAigCAngLQAogMAwgYQBSgwAsgjIghgYQgsgdgxgOQgpgMgvAAQhPABg5A0QgEgUgNgWQgMgRgcghQA1g4CIAHQCXARBDArQBkA4A3B9QAgBFAKBJQgDgDgEAAQgNgIgLgJQglgegpg9QgigzgpglQgeAZgmAYIhHAnQgWAJgTAKQgVAIgaAHQgdAIgjAJQAIAXAFAaIAFAbIgsAIg");
	this.shape_5.setTransform(1.8,-31.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EE8427").s().p("AIpF8QgcgQghgkIg6g7QgygshDgTIgNgUQgWgegdgPQAbAAAbAJQA9ARBLA2QAsAgBUBAQBWA3BCgIQAmgGAigbQAggaAUgkQARgbAPgtIAJgfIARgIQgYB1hKBHQg0AwhIALQgSADgRAAQg2AAgpgcgAkjh3Qg8hdhQhOQhThQiiAFQiFAAhfBLQAAgQAGgPQAJgYAWgPQAagTBzgUQB2gWBaAoQBeAoBQBpQBQBmAgA/QAdA8ACAoQgthMgthIg");
	this.shape_6.setTransform(1.5,-34.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF3333").s().p("AhBCHQgegEgbgIQgcgKgHgSQgHgNADgmQAAgVAFgcQADgeAGgcQAIgoALgmQAGABAFAEQAZAJAeAGIAcADQAZAFAZAAQA1AAA7gHIATBIQAIAqAFAqQAJBRgRAGIhEALQgoAIgnAAQgjAAgjgHg");
	this.shape_7.setTransform(-10.1,-11);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6699FF").s().p("AgECyQhUgEgjgHQghgJgpgWQgFgBgFgEQgKgDADg4IAOh/QAHhDANg4QALAKANAIQAEAAADACIAAACQgkBsgGBoQgGAfAMATQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABQARAXAkAJQBaAaCWgPQAxAAAUgZQAHgNAAg1QgLhCgHg6QgIglgDgeIAZgBQAKAXAJAeQAHAoAFAyIAAAQQAFBHgFAcQgEAJgIAJQgUAPgmAJQgxAKg8AAIgmgBg");
	this.shape_8.setTransform(-10.2,-10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EAF0EF").s().p("Ah+CNQgkgIgRgYQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgMgUAGgfQAGhnAkhsIAAgCQAFAEAJADIAIADQgKAmgJAoQgFAcgDAeQgFAcAAAVQgEAmAHANQAHASAdAKQAbAIAdAEQBIANBOgOIBEgLQARgGgJhRQgFgqgJgqIgThIIAQgBQAFgDAHAAQADAfAIAkQAHA6ALBCQAAA1gHAOQgUAZgxAAQg3AFgvAAQhRAAg5gRg");
	this.shape_9.setTransform(-10.6,-10.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FCD518").s().p("AJGGsQgbg/gzgqQgYgWgdgRQhYguhvgRQhPgOhXAEQhVAEhKASQhDAQg8AgQgbANgbAVIgWAPIgGAHIgWARQgfAbgWAfQgUAZgPAeQgCgFgFgFQgWgggSgUQAEgYAHgWIAxjGIAmiYIALg1QAGgjAAgGQADgWgJgRQgFgLgGgGQgTgXgigFIgWgBQgIAAgFABQgRAAgWAFIgHABIgFgbQgFgagIgYQAjgJAdgIQAagHAVgIQATgKAWgJIBHgnQAmgYAegZQApAlAiAzQApA+AlAeQgNA4gHBDIgOB/QgDA4AKACQAFAFAFAAQApAWAgAJQAjAIBUAEQBUAEBAgNQAmgJAUgPQAIgKAEgIQAFgcgFhIIAAgOQgFgzgHgoQgJgfgKgXQAWgEAnACIAxAGQAjACAeAPQAHACAGAGQAdAPAWAeIANAUQAlA2AQBAIAJAkQAMA3AKA5QAPBLANBNIAJAoQgWAcgpAeIgYATQgIgZgJgXgAp5iuQgngdgJglQgLgnASgWQALgOAegWQAqAwAeA2QAWAnARApQgSACgQAAQgwAAgdgVgApEl2QAWgTgHgdQA5g0BPgBQAvAAApAMQAxAOAsAdIAhAYQgsAjhSAwQgwAYgoAMQgnALgiACQgchHgygngAoVmEQgDAFAAAGQAAAIADACQAFAFAGAAQAIAAABgFQAFgCAAgIQAAgGgFgFQgBgDgIAAQgGAAgFADg");
	this.shape_10.setTransform(-23.3,-8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.9,-75.6,184,151.3);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#413030").s().p("AgLAKQgCgDAAgHQAAgGACgFQAGgCAFAAQAIAAABACQAFAFAAAGQAAAHgFADQgBAEgIAAQgFAAgGgEg");
	this.shape.setTransform(-75.6,-46);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EE6B18").s().p("AE1DlIgIgDQgJgDgFgFQgLhJgfhGQg3h8hkg3QhEgsiVgRQiIgHg2A4IgYgbIgGgMIgEgCQBfhMCGAAQChgEBTBQQBPBOA9BbQAsBIAuBOQAAAkggAgIAAAEQgFgEgGgBg");
	this.shape_1.setTransform(-53.9,-48.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0066CC").s().p("Ai1DwQjsgBi9hSIF6gBQCugGBAgIQCDgPBdgxQBvg2A8htQAthMAAhOICeBTQgYA+g7BAIgeAdQgtAmg6ApQjwCilDAAIgKAAg");
	this.shape_2.setTransform(6.7,51.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0099FF").s().p("AnnCCQgcgTgXgTQgfgXgagYQhUhTgdhZICDg5QATgIAVgLQAKAiAUAhIARAYQAVAZAbAaIApAiQAQAIARAMQAvAeA1AWQAWAKAZAHQCHAuCjAAQDCAACZg/QAzgWAxgeQAXgNAWgSQAvgjAkgpIAfgnIAZgnIABAAQAABOgtBMQg9BthuA2QhdAxiDAPQhBAIiuAGIl6ABQhLgehAgsg");
	this.shape_3.setTransform(-19.2,47);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663366").s().p("AkyBuQgZgHgWgKQg1gWgvgfQgRgMgQgIIgpghQgbgagVgZIgRgYQgUghgKgiQAFAEAfAEQALAAAVADQgHAWgEAYQASAUAWAgQAFAFACAFQAPgeAUgZIAhAbQBEA4BMAjQCJA+CoAAQEEAAC4iZQAIgEAFgHQAJAXAIAZIAYgTQApgeAWgcIgJgoIAvAAIAZAAIgZAnIgfAnQgkApgvAiQgWASgXANQgxAfgzAWQiZA/jBAAQikAAiHgug");
	this.shape_4.setTransform(-10.7,42.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EC6108").s().p("AI4EcQhUhAgsggQhLg2g9gRQgbgJgbAAQgGgGgHgCQA7gwA2gMQAdgJAbAEQAkABAqAaQAaAMAvAgQB4BBBggIQA3gEA5gaIgJAfQgPAtgRAbQgUAkggAaQgiAbgmAGIgQABQg9AAhLgwgAsFBNQgRgpgWgmQgeg2gqgwQAUgPAagRIAHgFQAyAnAcBHQAigCAngLQAogMAwgYQBSgwAsgjIghgYQgsgdgxgOQgpgMgvAAQhPABg5A0QgEgUgNgWQgMgRgcghQA1g4CIAHQCXARBDArQBkA4A3B9QAgBFAKBJQgDgDgEAAQgNgIgLgJQglgegpg9QgigzgpglQgeAZgmAYIhHAnQgWAJgTAKQgVAIgaAHQgdAIgjAJQAIAXAFAaIAFAbIgsAIg");
	this.shape_5.setTransform(1.8,-31.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EE8427").s().p("AIpF8QgcgQghgkIg6g7QgygshDgTIgNgUQgWgegdgPQAbAAAbAJQA9ARBLA2QAsAgBUBAQBWA3BCgIQAmgGAigbQAggaAUgkQARgbAPgtIAJgfIARgIQgYB1hKBHQg0AwhIALQgSADgRAAQg2AAgpgcgAkjh3Qg8hdhQhOQhThQiiAFQiFAAhfBLQAAgQAGgPQAJgYAWgPQAagTBzgUQB2gWBaAoQBeAoBQBpQBQBmAgA/QAdA8ACAoQgthMgthIg");
	this.shape_6.setTransform(1.5,-34.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF3333").s().p("AhBCHQgegEgbgIQgcgKgHgSQgHgNADgmQAAgVAFgcQADgeAGgcQAIgoALgmQAGABAFAEQAZAJAeAGIAcADQAZAFAZAAQA1AAA7gHIATBIQAIAqAFAqQAJBRgRAGIhEALQgoAIgnAAQgjAAgjgHg");
	this.shape_7.setTransform(-10.1,-11);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EAF0EF").s().p("Ah+CNQgkgIgRgYQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgMgUAGgfQAGhnAkhsIAAgCQAFAEAJADIAIADQgKAmgJAoQgFAcgDAeQgFAcAAAVQgEAmAHANQAHASAdAKQAbAIAdAEQBIANBOgOIBEgLQARgGgJhRQgFgqgJgqIgThIIAQgBQAFgDAHAAQADAfAIAkQAHA6ALBCQAAA1gHAOQgUAZgxAAQg3AFgvAAQhRAAg5gRg");
	this.shape_8.setTransform(-10.6,-10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#6699FF").s().p("AgECyQhUgEgjgHQghgJgpgWQgFgBgFgEQgKgDADg4IAOh/QAHhDANg4QALAKANAIQAEAAADACIAAACQgkBsgGBoQgGAfAMATQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABQARAXAkAJQBaAaCWgPQAxAAAUgZQAHgNAAg1QgLhCgHg6QgIglgDgeIAZgBQAKAXAJAeQAHAoAFAyIAAAQQAFBHgFAcQgEAJgIAJQgUAPgmAJQgxAKg8AAIgmgBg");
	this.shape_9.setTransform(-10.2,-10.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FCD518").s().p("AJGGsQgbg/gzgqQgYgWgdgRQhYguhvgRQhPgOhXAEQhVAEhKASQhDAQg8AgQgbANgbAVIgWAPIgGAHIgWARQgfAbgWAfQgUAZgPAeQgCgFgFgFQgWgggSgUQAEgYAHgWIAxjGIAmiYIALg1QAGgjAAgGQADgWgJgRQgFgLgGgGQgTgXgigFIgWgBQgIAAgFABQgRAAgWAFIgHABIgFgbQgFgagIgYQAjgJAdgIQAagHAVgIQATgKAWgJIBHgnQAmgYAegZQApAlAiAzQApA+AlAeQgNA4gHBDIgOB/QgDA4AKACQAFAFAFAAQApAWAgAJQAjAIBUAEQBUAEBAgNQAmgJAUgPQAIgKAEgIQAFgcgFhIIAAgOQgFgzgHgoQgJgfgKgXQAWgEAnACIAxAGQAjACAeAPQAHACAGAGQAdAPAWAeIANAUQAlA2AQBAIAJAkQAMA3AKA5QAPBLANBNIAJAoQgWAcgpAeIgYATQgIgZgJgXgAp5iuQgngdgJglQgLgnASgWQALgOAegWQAqAwAeA2QAWAnARApQgSACgQAAQgwAAgdgVgApEl2QAWgTgHgdQA5g0BPgBQAvAAApAMQAxAOAsAdIAhAYQgsAjhSAwQgwAYgoAMQgnALgiACQgchHgygngAoVmEQgDAFAAAGQAAAIADACQAFAFAGAAQAIAAABgFQAFgCAAgIQAAgGgFgFQgBgDgIAAQgGAAgFADg");
	this.shape_10.setTransform(-23.3,-8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.9,-75.6,184,151.3);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AhkA3IgCg3IgBgrIgBggIgCgvIAzgBIABAbIAAAaIAAAbIAQgWQAJgMAMgLQALgLANgIQAOgJAQgCQASAAAOAIQAHAEAGAGQAGAFAFAKQAFAJAEANQAEAMABASIgsAQIgCgTIgEgNIgGgKIgGgGQgIgGgKAAQgGABgHAFQgHAEgHAHIgOAPIgPASIgNASIgLAPIABAhIAAAeIABAZIABASIgvAGIgDhFg");
	this.shape.setTransform(187.8,5.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgbCoIABg6IABhFIABhfIAtgCIgBA5IgBAxIAAAnIgBAeIgBAxgAgLhqQgGgCgEgEQgFgFgCgFQgDgGAAgHQAAgGADgGQACgGAFgEQAEgFAGgCQAFgDAGAAQAHAAAFADQAGACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAFgEAFQgFAEgGACQgFADgHAAQgGAAgFgDg");
	this.shape_1.setTransform(168.8,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("ABFB/IADgfQgbARgaAIQgZAHgXAAQgQAAgPgDQgPgFgMgIQgLgJgHgNQgHgNAAgTQAAgRAFgOQAGgPAJgLQAJgKANgIQANgIAOgEQAPgFAQgCQAPgDAQAAQAOAAANACIAXACIgHgVQgFgLgGgJQgHgIgJgGQgKgFgNgBQgJAAgKAEQgLABgOAIQgNAGgQAMQgQAMgSASIgbghQAWgUATgNQAUgMARgIQARgHAPgDQAOgDAMAAQAUABAQAHQAQAHANAMQAMAMAIAQQAIARAGASQAFATADAUQACAUAAAUQAAAWgDAYQgCAZgFAdgAgKAAQgTADgNAJQgOAJgHANQgHANAEAQQAEANAJAGQAJAGAMAAQANAAAPgFIAdgLIAcgNIAWgNIABgWIgBgXIgXgEQgMgCgMAAQgUAAgSAFg");
	this.shape_2.setTransform(147.9,5.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("Ah6i0IAxgCIAAAhQAMgNAOgHQAOgGALgEQAOgEAMgBQAPAAAPAEQAPAFANAIQAOAHALALQALAMAIANQAIAOAFAQQAEAQAAARQgBATgFARQgFARgJANQgIAOgLAMQgLALgNAIQgNAIgNAEQgOAEgNAAQgMgBgPgDQgMgDgOgHQgPgGgOgNIgCCXIgpABgAAAiLQgOAAgMAFQgMAFgJAIQgKAIgHAKQgHALgDALIgBAqQAEAPAHALQAHALAKAHQAJAHAMAEQALAEAMAAQAOABAOgGQAOgGAKgKQAKgLAGgOQAGgOABgRQABgQgFgPQgFgPgKgLQgKgMgNgGQgNgHgPAAIgCAAg");
	this.shape_3.setTransform(120.3,11.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AA8CpQAGgVADgUIAEglIABgjQgCgYgHgRQgHgPgKgLQgLgLgMgEQgNgFgMAAQgLABgNAGQgLAGgNALQgNAKgLAVIgBCTIgsABIgDlWIAzgCIgBCHQAMgNAOgIQANgHAMgEQAOgFANgCQAZAAAUAJQAVAJAOARQAPARAJAYQAIAXABAeIAAAgIgBAgIgEAeQgCAPgDALg");
	this.shape_4.setTransform(78.1,0.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgbgZIhMACIABgpIBMgDIADhhIArgDIgCBkIBWgDIgDApIhUADIgBDBIguABg");
	this.shape_5.setTransform(52.1,1.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AgbCoIABg6IABhFIABhfIAtgCIgBA5IgBAxIAAAnIgBAeIgBAxgAgLhqQgGgCgEgEQgFgFgCgFQgDgGAAgHQAAgGADgGQACgGAFgEQAEgFAGgCQAFgDAGAAQAHAAAFADQAGACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAFgEAFQgFAEgGACQgFADgHAAQgGAAgFgDg");
	this.shape_6.setTransform(35.8,0.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AAHgsIg1CbIhCAFIgsjmIAugEIAiC6IBCi6IAqADIAtC9IAei/IAyACIgsDoIhDACg");
	this.shape_7.setTransform(12.7,5.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AgxB3QgXgIgPgRQgRgQgIgXQgIgXAAgcQAAgbAKgYQAJgYARgSQARgSAVgLQAXgKAZABQAXgBAVAKQAUAIAQARQAPAQALAVQAKAWADAZIjAAbQACAQAFANQAHANAJAIQAKAJANAEQANAFAOAAQAMAAALgEQAMgDAKgIQAJgHAIgKQAHgLADgNIArAIQgGAVgLARQgMAQgPAMQgPAMgSAGQgTAHgTgBQgcAAgXgJgAgShWQgLADgLAIQgKAIgIANQgJANgDAUICFgRIgBgEQgJgXgOgLQgPgNgWAAQgIAAgMADg");
	this.shape_8.setTransform(-29.7,6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AhkA3IgCg3IgBgrIgBggIgCgvIAzgBIABAbIAAAaIAAAbIAQgWQAJgMAMgLQALgLANgIQAOgJAQgCQASAAAOAIQAHAEAGAGQAGAFAFAKQAFAJAEANQAEAMABASIgsAQIgCgTIgEgNIgGgKIgGgGQgIgGgKAAQgGABgHAFQgHAEgHAHIgOAPIgPASIgNASIgLAPIABAhIAAAeIABAZIABASIgvAGIgDhFg");
	this.shape_9.setTransform(-54.5,5.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgYB8QgUgBgOgHQgOgHgKgLQgKgMgGgPQgGgPgDgQQgDgRgBgSIgBghQAAgXACgXIAFgxIAvABQgFAcgBAWIgBAnIABATIABAYQABANAEAMQADANAGAKQAFAKAJAFQAIAGAOgCQAQgDASgVQASgVAVgrIgBg4IgBghIAAgSIAwgFIACBFIABA2IACAsIABAgIABAvIgzABIgBhAQgHANgKAMQgJALgKAJQgMAJgMAFQgKAFgMAAIgDAAg");
	this.shape_10.setTransform(-82.6,5.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("AgbgZIhNACIACgpIBMgDIAChhIArgDIgBBkIBXgDIgEApIhUADIgBDBIguABg");
	this.shape_11.setTransform(-107.5,1.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AgoB4QgYgKgSgRQgTgRgLgYQgLgYAAgcQAAgPAFgQQAEgRAJgOQAIgOAMgNQAMgMAOgJQAPgJARgEQARgGARAAQAUAAARAFQASAFAPAJQAOAIAMANQAMANAIAQIABABIglAWIgBgBQgGgMgIgIQgIgKgKgGQgLgGgLgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHAQQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAGARAAQAMAAALgDQALgDAKgFQAKgGAIgIQAJgIAFgKIABgCIAnAWIAAACQgJAOgNAMQgMALgPAJQgPAIgRAEQgRAFgSAAQgYAAgXgKg");
	this.shape_12.setTransform(-132.1,6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AgbCoIABg6IABhFIABhfIAtgCIgBA5IgBAxIAAAnIgBAeIgBAxgAgLhqQgGgCgEgEQgFgFgCgFQgDgGAAgHQAAgGADgGQACgGAFgEQAEgFAGgCQAFgDAGAAQAHAAAFADQAGACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAFgEAFQgFAEgGACQgFADgHAAQgGAAgFgDg");
	this.shape_13.setTransform(-151.4,0.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("Ah6i0IAxgCIAAAhQAMgNAOgHQAOgGALgEQAOgEAMgBQAPAAAPAEQAPAFANAIQAOAHALALQALAMAIANQAIAOAFAQQAEAQAAARQgBATgFARQgFARgJANQgIAOgLAMQgLALgNAIQgNAIgNAEQgOAEgNAAQgMgBgPgDQgMgDgOgHQgPgGgOgNIgCCXIgpABgAAAiLQgOAAgMAFQgMAFgJAIQgKAIgHAKQgHALgDALIgBAqQAEAPAHALQAHALAKAHQAJAHAMAEQALAEAMAAQAOABAOgGQAOgGAKgKQAKgLAGgOQAGgOABgRQABgQgFgPQgFgPgKgLQgKgMgNgGQgNgHgPAAIgCAAg");
	this.shape_14.setTransform(-171.4,11.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(2,1,1).p("A7WlJMA2tAAAQBsAABMBNQBMBMAABrIAACLQAABrhMBMQhMBMhsAAMg2tAAAQhsAAhMhMQhMhMAAhrIAAiLQAAhrBMhMQBMhNBsAAg");
	this.shape_15.setTransform(0.9,1.8,0.99,0.99);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("A7WFJQhsABhMhNQhMhMAAhsIAAiJQAAhsBMhNQBMhLBsgBMA2tAAAQBsABBMBLQBMBNAABsIAACJQAABshMBMQhMBNhsgBg");
	this.shape_16.setTransform(0.9,1.8,0.99,0.99);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-202.7,-36.3,405.5,72.8);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgHAQgDQASAAAPAIQAGAEAHAGQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgJIgHgHQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAaIABASIgxAFIgChGg");
	this.shape.setTransform(310.1,-15.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgcCpIABg6IABhGIAChgIAugCIgCA7IgBAxIgBAnIAAAeIgBAxgAgLhqQgHgDgDgEQgFgEgCgGQgDgGAAgHQAAgGADgGQACgGAFgEQADgFAHgCQAFgDAGAAQAHAAAGADQAFACAFAFQAEAEACAGQADAGAAAGQAAAHgDAGQgCAGgEAEQgFAEgFADQgGACgHAAQgGAAgFgCg");
	this.shape_1.setTransform(290.9,-21.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("ABGCAIADgfQgcARgZAIQgaAIgXAAQgQAAgPgEQgQgEgLgJQgMgJgHgNQgHgNAAgTQAAgSAGgOQAEgPAKgLQAJgKAOgIQAMgHAPgFQAPgFAQgDQAPgCAQAAQAPAAANABIAWADIgGgWQgFgLgHgJQgGgIgKgGQgKgFgNAAQgJAAgKACQgMADgNAHQgNAGgQAMQgRAMgRASIgcghQAWgUAUgNQATgNASgHQARgHAPgDQAOgDAMAAQAVAAAQAIQAQAHANAMQAMAMAJARQAHAQAGATQAFATADAVQACATAAAUQAAAWgDAZQgCAZgFAdgAgKAAQgTADgNAJQgOAJgHANQgHANAEARQADAOAKAFQAIAFANAAQANAAAPgEQAOgEAQgHIAcgNIAWgNIABgXIgBgXQgLgCgMgCQgMgCgMAAQgVAAgSAFg");
	this.shape_2.setTransform(269.8,-16.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("Ah7i2IAxgCIAAAhQANgNAOgGQAOgHALgEQAOgEAMgBQAPAAAPAEQAQAFANAIQAOAIALALQAMALAIAOQAIAOAEAQQAEAQAAARQgBAUgFARQgFARgJANQgIAPgLALQgMALgMAIQgNAIgOAFQgNAEgOAAQgMgBgPgDQgMgEgPgGQgPgHgOgMIgBCYIgqABgAAAiMQgOAAgMAFQgMAFgKAIQgKAHgGALQgHALgEALIAAArQADAPAHALQAIALAJAHQAKAHAMAEQALAEAMAAQAPABANgGQAOgGALgKQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgMQgKgLgOgHQgNgGgPAAIgCAAg");
	this.shape_3.setTransform(241.9,-10.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AA8CsQAHgXADgUIAEglIAAgkQgBgYgHgQQgHgRgLgKQgKgLgNgEQgMgGgMAAQgMABgNAHQgMAGgMALQgNAKgLAVIgCCVIgsABIgElaIA0gCIgBCJQAMgOAOgHQAOgIAMgEQAOgEANgCQAZAAAVAJQAVAJAOAQQAPARAJAZQAJAXABAfIgBAgQAAARgBAPIgDAfQgDAOgDAMg");
	this.shape_4.setTransform(199.2,-21.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_5.setTransform(173,-20.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AgcCpIACg6IAAhGIAChgIAtgCIgBA7IgBAxIgBAnIAAAeIAAAxgAgLhqQgGgDgFgEQgEgEgCgGQgDgGAAgHQAAgGADgGQACgGAEgEQAFgFAGgCQAFgDAGAAQAGAAAGADQAGACAEAFQAFAEACAGQADAGAAAGQAAAHgDAGQgCAGgFAEQgEAEgGADQgGACgGAAQgGAAgFgCg");
	this.shape_6.setTransform(156.6,-21.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AAHgsIg2CcIhDAFIgrjoIAugEIAiC8IBDi8IAqADIAuC+IAejAIAyACIgtDqIhCACg");
	this.shape_7.setTransform(133.2,-16.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AgyB5QgXgJgQgRQgPgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQARgSAWgKQAXgLAYAAQAYAAAUAKQAWAJAPAQQAQAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAKAJAMAFQAOAEANAAQANAAAMgEQALgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgMAQQgLARgQAMQgPAMgSAGQgSAHgVAAQgcAAgXgJgAgShXQgMAEgKAHQgLAIgIAOQgJANgDATICHgQIgBgEQgJgXgPgMQgPgNgXAAQgIAAgLADg");
	this.shape_8.setTransform(90.4,-15.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AhlA3IgCg3IgBgrIgCghIgBgwIAzgBIABAcIAAAaIAAAcIAQgXQAJgMAMgLQAMgLANgJQAOgHAQgDQASAAAPAIQAGAEAHAGQAGAFAFAKQAFAJAEANQAEANABARIgsARIgCgTIgFgOIgFgJIgHgHQgIgFgKAAQgGABgHAEQgHAFgHAGQgHAHgIAJIgOASIgOASIgLAPIABAhIABAfIABAaIABASIgxAFIgChGg");
	this.shape_9.setTransform(65.3,-15.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgYB9QgUAAgOgHQgPgHgKgMQgKgLgGgPQgGgQgDgRQgDgQgBgSIgBghQAAgYACgYQACgYAEgZIAuACQgEAbgBAXIgBAnIAAATIACAYIAEAaQADANAGAKQAGAKAJAFQAIAGANgCQARgDATgVQASgVAVgsIgBg5IgBggIgBgTIAxgFIACBGIACA3IABArIABAhIABAwIgzAAIgBhAQgHANgKAMQgJAMgLAJQgLAJgNAFQgKAEgMAAIgDAAg");
	this.shape_10.setTransform(36.8,-16);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_11.setTransform(11.8,-20.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AgoB5QgZgKgSgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAEgQAJgPQAJgOAMgNQAMgMAOgJQAOgJARgFQASgFARAAQAUAAASAFQARAEAPAJQAPAJAMANQANANAHAQIABABIgmAXIAAgCQgGgLgIgJQgJgJgJgHQgLgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQAMAAAMgDQALgDAKgGQAKgFAIgJQAIgIAHgKIAAgBIAnAWIAAABQgJAPgMAMQgNALgPAJQgQAIgRAFQgRAEgSAAQgYAAgXgKg");
	this.shape_12.setTransform(-13,-15.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AgcCpIABg6IABhGIAChgIAugCIgBA7IgCAxIgBAnIAAAeIgBAxgAgLhqQgHgDgDgEQgFgEgDgGQgCgGAAgHQAAgGACgGQADgGAFgEQADgFAHgCQAFgDAGAAQAGAAAHADQAFACAFAFQAEAEADAGQACAGAAAGQAAAHgCAGQgDAGgEAEQgFAEgFADQgHACgGAAQgGAAgFgCg");
	this.shape_13.setTransform(-32.6,-21.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("Ah7i2IAxgCIAAAhQANgNAOgGQAOgHALgEQAOgEAMgBQAPAAAPAEQAQAFANAIQAOAIALALQAMALAIAOQAIAOAEAQQAEAQAAARQgBAUgFARQgFARgJANQgIAPgLALQgMALgMAIQgNAIgOAFQgNAEgOAAQgMgBgPgDQgMgEgPgGQgPgHgOgMIgBCYIgqABgAAAiMQgOAAgMAFQgMAFgKAIQgKAHgGALQgHALgEALIAAArQADAPAHALQAIALAJAHQAKAHAMAEQALAEAMAAQAPABANgGQAOgGALgKQAKgLAGgOQAGgPABgQQABgRgFgPQgFgPgKgMQgKgLgOgHQgNgGgPAAIgCAAg");
	this.shape_14.setTransform(-52.8,-10.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_15.setTransform(-93.9,-15.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#663300").s().p("AA8CsQAHgXADgUIAEglIAAgkQgBgYgHgQQgHgRgLgKQgKgLgNgEQgMgGgMAAQgMABgNAHQgMAGgMALQgNAKgLAVIgCCVIgsABIgElaIA0gCIgBCJQAMgOAOgHQAOgIAMgEQAOgEANgCQAZAAAVAJQAVAJAOAQQAPARAJAZQAJAXABAfIgBAgQAAARgBAPIgDAfQgDAOgDAMg");
	this.shape_16.setTransform(-122.7,-21.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_17.setTransform(-149,-20.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#663300").s().p("AgbgZIhOABIABgpIBOgCIAChjIAsgDIgCBlIBYgDIgEAqIhUACIgCDDIgvACg");
	this.shape_18.setTransform(-182.9,-20.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#663300").s().p("AgoB5QgYgKgTgRQgTgSgKgYQgMgXAAgdQAAgPAFgRQAFgQAIgPQAIgOANgNQALgMAPgJQAPgJAQgFQASgFARAAQAUAAASAFQASAEAPAJQAPAJAMANQALANAIAQIABABIglAXIgBgCQgGgLgIgJQgJgJgKgHQgKgGgMgEQgMgDgNAAQgRAAgQAHQgQAHgMAMQgMAMgHARQgHAQAAARQAAATAHAQQAHAQAMAMQAMAMAQAHQAQAHARAAQANAAALgDQALgDAKgGQAKgFAIgJQAJgIAGgKIABgBIAmAWIAAABQgIAPgNAMQgNALgPAJQgQAIgQAFQgSAEgSAAQgYAAgXgKg");
	this.shape_19.setTransform(-207.7,-15.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#663300").s().p("AgyB5QgXgJgPgRQgRgQgIgXQgIgXAAgdQAAgbAKgYQAJgZARgSQAQgSAXgKQAXgLAZAAQAXAAAVAKQAUAJARAQQAPAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAJAJAOAFQANAEAOAAQAMAAALgEQAMgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgLAQQgMARgPAMQgPAMgTAGQgTAHgTAAQgdAAgXgJgAgShXQgMAEgKAHQgKAIgJAOQgIANgDATICGgQIgBgEQgJgXgPgMQgPgNgWAAQgJAAgLADg");
	this.shape_20.setTransform(-235.2,-15.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#663300").s().p("AAODEQgNgFgHgIQgKgJgHgKQgGgLgEgLQgMgZgCghIAGkcIAuAAIgCBJIgCA9IgBAxIAAAlIgCA9QABAVAEAQIAHAOQADAHAGAFQAFAGAHADQAIADAJAAIgGAtQgPAAgNgFg");
	this.shape_21.setTransform(-254.1,-24.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#663300").s().p("AgyB5QgXgJgQgRQgPgQgJgXQgIgXAAgdQAAgbAKgYQAJgZARgSQARgSAWgKQAWgLAZAAQAYAAAUAKQAWAJAPAQQAQAQALAWQAKAVADAaIjCAcQABAQAHANQAGAMAKAJQAKAJAMAFQAOAEANAAQANAAAMgEQALgDAKgHQAKgIAHgKQAIgLADgOIArAJQgGAVgMAQQgLARgQAMQgPAMgSAGQgSAHgVAAQgcAAgXgJgAgShXQgMAEgKAHQgLAIgIAOQgJANgDATICHgQIgBgEQgJgXgPgMQgPgNgXAAQgIAAgLADg");
	this.shape_22.setTransform(-275.4,-15.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#663300").s().p("AgdC1QgRgEgQgHQgRgGgPgKQgQgIgQgMIAhgmQASAPARAJQARAJAOAFQAQAFANACQARAAAOgDQANgDALgHQAKgGAHgJQAGgJABgJQABgJgEgGQgDgIgHgGQgHgFgKgFIgUgIIgWgFIgVgFQgNgCgOgEQgOgDgOgGQgOgGgMgJQgMgJgJgNQgJgMgEgRQgFgRACgWQABgSAGgOQAGgPAKgMQAKgLANgIQAMgIAPgFQAOgGAQgCQAPgCAOAAQAWAAAVAGIATAGIAUAIIAUALQAKAHAJAJIgZAnQgHgHgJgGIgQgLIgQgIIgPgFQgSgEgQgCQgUAAgQAHQgQAGgLAKQgLAJgFAMQgGAMAAAMQAAAMAHAKQAHALAMAJQAMAJAQAGQARAHASACQAQACAQAEQARADAPAGQAOAGANAKQAMAIAJAMQAIALAFAPQADAPgCARQgCAPgHANQgHAMgKAJQgJAJgNAHQgMAFgNAFQgOADgOACIgaACQgQAAgRgEg");
	this.shape_23.setTransform(-304.2,-20.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#CC6600").ss(5,1,1).p("EgzBgGPMBmDAAAQCQAABmBsQBnBrAACaIAAA9QAACahnBrQhmBsiQAAMhmDAAAQiPAAhmhsQhohrAAiaIAAg9QAAiaBohrQBmhsCPAAg");
	this.shape_24.setTransform(0,-22);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFCC00").s().p("EgzAAGQQiQAAhmhsQhohsABiYIAAg+QgBiaBohrQBmhsCQAAMBmBAAAQCQAABnBsQBmBrAACaIAAA+QAACYhmBsQhnBsiQAAg");
	this.shape_25.setTransform(0,-22);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-363.9,-64.5,727.9,85);


(lib.fxTween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("Ag9BfQgDgCAAgDIAAgDIAMg8IgtgpQgDgEgBgDIABgCQACgDAFgBIA+gIIAag3QABgDABgBQABgBAAAAQABAAAAAAQABAAAAgBQAAAAAAAAIAEACIADAEIAaA3IA9AIQAGABABADIABACQgBADgDAEIgtApIAMA8IAAADQAAADgDACQgDADgFgDIg2geIg1AeIgFACIgDgCgAA3BdQAEACACgCQACgBgBgFIgMg9IAugqQAEgDgCgDQAAgCgEgBIg/gHIgbg5QgCgEgCAAQgBAAgDAEIgaA5Ig+AHQgFABAAACQgCADAEADIAuAqIgMA9QAAAFACABQABACAEgCIA2gfg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEE03A").s().p("AgpBSQgCgCABgEIAKg1IgogkQgDgDABgDQABgDAFAAIA1gHIAWgxQACgEADAAQADAAACAEIAXAxIAjAEQgwAOgcAaQgeAbAAAiIAAAEIgEABIgEACIgCgBg");
	this.shape_1.setTransform(-1.2,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AA3BdIg3gfIg2AfQgEACgBgCQgCgBAAgFIAMg9IgugqQgEgDACgDQAAgCAFgBIA+gHIAag5QADgEABAAQACAAACAEIAbA5IA/AHQAEABAAACQACADgEADIguAqIAMA9QABAFgCABIgCABIgEgBgAgEhNIgXAxIg1AHQgEAAgBADQgBADACADIAoAkIgKA1QgBAEADACQACABADgCIAEgBIAAgEQAAgiAegbQAcgaAxgOIgkgEIgXgxQgCgEgDAAQgBAAgDAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.1,-9.6,20.3,19.3);


(lib.fxSymbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFC00","#FFFFAD"],[0,1],-37.8,-8.3,22.7,-8.3).s().p("ABjDjIihhaQgGgDgHAAQgGAAgGADIijBaQgOk7EaiNIAIARQADAGAFAEQAFADAHABIC3AXQAKABAHAIQAGAHgBAKQAAAKgIAHIiHB/IAAAAQgEAEgCAGIAAAAQgCAGABAGIAiC3QACAKgFAIQgGAIgJADIgHABQgGAAgFgDg");
	this.shape.setTransform(7.6,9.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("Aj5F+QgJgIAAgPIABgLIAujxIi0inQgOgOAAgMIACgHQAGgPAYgDIDzgfIBojeQAEgLAJgFQAGgFAGgBIACAAQAGAAAIAGQAIAFAFALIBoDeIDxAfQAbADADAPQACAEABADQAAAMgPAOIizCnIAvDxIABALQAAAPgKAIQgNAKgUgNIjYh2IjXB4QgMAGgJAAQgIAAgGgFgADcFzQAPAIAJgFQAIgHgEgSIguj2IC2irQANgMgDgJQgDgJgSgDIj4gfIhrjjQgIgQgJAAIgCABQgJABgGAOIhrDjIj5AfQgSADgDAJQgEAJANAMIC4CrIgvD2QgDASAIAHQAHAFAPgIIDdh6g");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FFCC00","#FFFF00"],[0,1],-18.6,0,41.9,0).s().p("AhME4QgJgCgGgJQgFgIACgKIAki3IAAAAQABgGgCgGQgCgGgFgFIiIh+QgHgHgBgJQgBgKAHgIQAGgIAKgBIC5gXQAFgBAFgDQAGgEACgGIAAAAIBPioQAEgJAKgEQAJgEAJAEQAJADAEAJIBICYQkaCNAOE7IgBAAQgFADgGAAIgHgBg");
	this.shape_2.setTransform(-11.7,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FFCC00"],[0,1],-39.5,0,39.5,0).s().p("ADdFzIjch6IjdB6QgPAIgHgFQgIgHADgSIAwj2Ii5irQgMgMADgJQAEgJARgDID5gfIBrjjQAGgOAKgCIABAAQAJAAAIAQIBrDjID5AfQASADADAJQACAJgMAMIi3CrIAuD2QAEASgIAHQgDACgFAAQgGAAgJgFgAhshwQgFAEgHAAIi5AXQgKACgGAHQgGAIAAAKQABAKAHAGICJB+QAEAFACAGQACAGgBAGIAAAAIgkC3QgCAKAGAIQAFAJAKACQAJADAJgFIABAAICjhaQAFgDAGAAQAGAAAGADICiBaQAJAFAJgDQAKgCAFgJQAFgIgCgKIgii3QgBgGACgGIAAAAQACgGAFgFIgBAAICIh+QAHgGABgKQAAgKgGgIQgHgHgJgCIi4gXQgGAAgFgEQgGgEgDgGIgHgQIhIiYQgEgJgJgEQgKgEgIAEQgJAEgEAJIhPCoIAAAAQgDAGgFAEg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol3, new cjs.Rectangle(-40.5,-38.6,81.1,77.4), null);


(lib.fxSymbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("Ah3B3QgwgxAAhGQAAhFAwgyQAygwBFAAQBGAAAxAwQAyAygBBFQABBGgyAxQgxAyhGgBQhFABgygygAhqhqQgtAsAAA+QAAA/AtArQAsAuA+gBQA/ABAsguQAtgrAAg/QAAg+gtgsQgsgtg/AAQg+AAgsAtg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.fxSymbol2copy, new cjs.Rectangle(-16.8,-16.8,33.7,33.7), null);


(lib.Tween1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#663300").ss(3,1,1).p("Ai8huIDiAEIB/ACIBRADICkACImnK9IhDh5IlJpTIDdAEIBlnrIBFABIBIABIBuHs");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Aj0HhIFGpGICjACImmK8gAh+hqIg5nuIBJABIBuHsIAAADg");
	this.shape_1.setTransform(16.5,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AlHg2IDcAEIDiAFIB/ACIBSADIlHJFgAB3gtgAhrgyIBmnqIBEAAIA4HvgAhrgyg");
	this.shape_2.setTransform(-8.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-61.6,85,123.3);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol2copy12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0099FF").s().p("AnnCCQgcgTgXgTQgfgXgagYQhUhTgdhZICDg5QATgIAVgLQAKAiAUAhIARAYQAVAZAbAaIApAiQAQAIARAMQAvAeA1AWQAWAKAZAHQCHAuCjAAQDCAACZg/QAzgWAxgeQAXgNAWgSQAvgjAkgpIAfgnIAZgnIABAAQAABOgtBMQg9BthuA2QhdAxiDAPQhBAIiuAGIl6ABQhLgehAgsg");
	this.shape.setTransform(212.3,47);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663366").s().p("AkyBuQgZgHgWgKQg1gWgvgfQgRgMgQgIIgpghQgbgagVgZIgRgYQgUghgKgiQAFAEAfAEQALAAAVADQgHAWgEAYQASAUAWAgQAFAFACAFQAPgeAUgZIAhAbQBEA4BMAjQCJA+CoAAQEEAAC4iZQAIgEAFgHQAJAXAIAZIAYgTQApgeAWgcIgJgoIAvAAIAZAAIgZAnIgfAnQgkApgvAiQgWASgXANQgxAfgzAWQiZA/jBAAQikAAiHgug");
	this.shape_1.setTransform(220.8,42.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0066CC").s().p("Ai1DwQjsgBi+hSIF7gBQCugGBAgIQCDgPBegxQBug2A8htQAthMAAhOICdBTQgXA+g7BAIgeAdQgtAmg7ApQjvCilDAAIgKAAg");
	this.shape_2.setTransform(238.2,51.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AhBCHQgegEgbgIQgcgKgHgSQgHgNADgmQAAgVAFgcQADgeAGgcQAIgoALgmQAGABAFAEQAZAJAeAGIAcADQAZAFAZAAQA1AAA7gHIATBIQAIAqAFAqQAJBRgRAGIhEALQgoAIgnAAQgjAAgjgHg");
	this.shape_3.setTransform(221.4,-11);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EAF0EF").s().p("Ah+CNQgkgIgRgYQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgMgUAGgfQAGhnAkhsIAAgCQAFAEAJADIAIADQgKAmgJAoQgFAcgDAeQgFAcAAAVQgEAmAHANQAHASAdAKQAbAIAdAEQBIANBOgOIBEgLQARgGgJhRQgFgqgJgqIgThIIAQgBQAFgDAHAAQADAfAIAkQAHA6ALBCQAAA1gHAOQgUAZgxAAQg3AFgvAAQhRAAg5gRg");
	this.shape_4.setTransform(220.9,-10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6699FF").s().p("AgECyQhUgEgjgHQghgJgpgWQgFgBgFgEQgKgDADg4IAOh/QAHhDANg4QALAKANAIQAEAAADACIAAACQgkBsgGBoQgGAfAMATQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABQARAXAkAJQBaAaCWgPQAxAAAUgZQAHgNAAg1QgLhCgHg6QgIglgDgeIAZgBQAKAXAJAeQAHAoAFAyIAAAQQAFBHgFAcQgEAJgIAJQgUAPgmAJQgxAKg8AAIgmgBg");
	this.shape_5.setTransform(221.3,-10.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#413030").s().p("AgLAKQgCgDAAgHQAAgGACgFQAGgCAFAAQAIAAABACQAFAFAAAGQAAAHgFADQgBAEgIAAQgFAAgGgEg");
	this.shape_6.setTransform(155.9,-46);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FCD518").s().p("AJGGsQgbg/gzgqQgYgWgdgRQhYguhvgRQhPgOhXAEQhVAEhKASQhDAQg8AgQgbANgbAVIgWAPIgGAHIgWARQgfAbgWAfQgUAZgPAeQgCgFgFgFQgWgggSgUQAEgYAHgWIAxjGIAmiYIALg1QAGgjAAgGQADgWgJgRQgFgLgGgGQgTgXgigFIgWgBQgIAAgFABQgRAAgWAFIgHABIgFgbQgFgagIgYQAjgJAdgIQAagHAVgIQATgKAWgJIBHgnQAmgYAegZQApAlAiAzQApA+AlAeQgNA4gHBDIgOB/QgDA4AKACQAFAFAFAAQApAWAgAJQAjAIBUAEQBUAEBAgNQAmgJAUgPQAIgKAEgIQAFgcgFhIIAAgOQgFgzgHgoQgJgfgKgXQAWgEAnACIAxAGQAjACAeAPQAHACAGAGQAdAPAWAeIANAUQAlA2AQBAIAJAkQAMA3AKA5QAPBLANBNIAJAoQgWAcgpAeIgYATQgIgZgJgXgAp5iuQgngdgJglQgLgnASgWQALgOAegWQAqAwAeA2QAWAnARApQgSACgQAAQgwAAgdgVgApEl2QAWgTgHgdQA5g0BPgBQAvAAApAMQAxAOAsAdIAhAYQgsAjhSAwQgwAYgoAMQgnALgiACQgchHgygngAoVmEQgDAFAAAGQAAAIADACQAFAFAGAAQAIAAABgFQAFgCAAgIQAAgGgFgFQgBgDgIAAQgGAAgFADg");
	this.shape_7.setTransform(208.2,-8.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EC6108").s().p("AI4EcQhUhAgsggQhLg2g9gRQgbgJgbAAQgGgGgHgCQA7gwA2gMQAdgJAbAEQAkABAqAaQAaAMAvAgQB4BBBggIQA3gEA5gaIgJAfQgPAtgRAbQgUAkggAaQgiAbgmAGIgQABQg9AAhLgwgAsFBNQgRgpgWgmQgeg2gqgwQAUgPAagRIAHgFQAyAnAcBHQAigCAngLQAogMAwgYQBSgwAsgjIghgYQgsgdgxgOQgpgMgvAAQhPABg5A0QgEgUgNgWQgMgRgcghQA1g4CIAHQCXARBDArQBkA4A3B9QAgBFAKBJQgDgDgEAAQgNgIgLgJQglgegpg9QgigzgpglQgeAZgmAYIhHAnQgWAJgTAKQgVAIgaAHQgdAIgjAJQAIAXAFAaIAFAbIgsAIg");
	this.shape_8.setTransform(233.3,-31.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EE8427").s().p("AIpF8QgcgQghgkIg6g7QgygshDgTIgNgUQgWgegdgPQAbAAAbAJQA9ARBLA2QAsAgBUBAQBWA3BCgIQAmgGAigbQAggaAUgkQARgbAPgtIAJgfIARgIQgYB1hKBHQg0AwhIALQgSADgRAAQg2AAgpgcgAkjh3Qg8hdhQhOQhThQiiAFQiFAAhfBLQAAgQAGgPQAJgYAWgPQAagTBzgUQB2gWBaAoQBeAoBQBpQBQBmAgA/QAdA8ACAoQgthMgthIg");
	this.shape_9.setTransform(233,-34.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EE6B18").s().p("AE1DlIgIgDQgJgDgFgFQgLhJgfhGQg3h8hkg3QhEgsiVgRQiIgHg2A4IgYgbIgGgMIgEgCQBfhMCGAAQChgEBTBQQBPBOA9BbQAsBIAuBOQAAAkggAgIAAAEQgFgEgGgBg");
	this.shape_10.setTransform(177.6,-48.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#413030").s().p("AgLAKQgCgDAAgHQAAgGACgFQAGgCAFAAQAIAAABACQAFAFAAAGQAAAHgFADQgBAEgIAAQgFAAgGgEg");
	this.shape_11.setTransform(-75.6,-46);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EE6B18").s().p("AE1DlIgIgDQgJgDgFgFQgLhJgfhGQg3h8hkg3QhEgsiVgRQiIgHg2A4IgYgbIgGgMIgEgCQBfhMCGAAQChgEBTBQQBPBOA9BbQAsBIAuBOQAAAkggAgIAAAEQgFgEgGgBg");
	this.shape_12.setTransform(-53.9,-48.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0066CC").s().p("Ai1DwQjsgBi9hSIF6gBQCugGBAgIQCDgPBdgxQBvg2A8htQAthMAAhOICeBTQgYA+g7BAIgeAdQgtAmg6ApQjwCilDAAIgKAAg");
	this.shape_13.setTransform(6.7,51.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0099FF").s().p("AnnCCQgcgTgXgTQgfgXgagYQhUhTgdhZICDg5QATgIAVgLQAKAiAUAhIARAYQAVAZAbAaIApAiQAQAIARAMQAvAeA1AWQAWAKAZAHQCHAuCjAAQDCAACZg/QAzgWAxgeQAXgNAWgSQAvgjAkgpIAfgnIAZgnIABAAQAABOgtBMQg9BthuA2QhdAxiDAPQhBAIiuAGIl6ABQhLgehAgsg");
	this.shape_14.setTransform(-19.2,47);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663366").s().p("AkyBuQgZgHgWgKQg1gWgvgfQgRgMgQgIIgpghQgbgagVgZIgRgYQgUghgKgiQAFAEAfAEQALAAAVADQgHAWgEAYQASAUAWAgQAFAFACAFQAPgeAUgZIAhAbQBEA4BMAjQCJA+CoAAQEEAAC4iZQAIgEAFgHQAJAXAIAZIAYgTQApgeAWgcIgJgoIAvAAIAZAAIgZAnIgfAnQgkApgvAiQgWASgXANQgxAfgzAWQiZA/jBAAQikAAiHgug");
	this.shape_15.setTransform(-10.7,42.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#EC6108").s().p("AI4EcQhUhAgsggQhLg2g9gRQgbgJgbAAQgGgGgHgCQA7gwA2gMQAdgJAbAEQAkABAqAaQAaAMAvAgQB4BBBggIQA3gEA5gaIgJAfQgPAtgRAbQgUAkggAaQgiAbgmAGIgQABQg9AAhLgwgAsFBNQgRgpgWgmQgeg2gqgwQAUgPAagRIAHgFQAyAnAcBHQAigCAngLQAogMAwgYQBSgwAsgjIghgYQgsgdgxgOQgpgMgvAAQhPABg5A0QgEgUgNgWQgMgRgcghQA1g4CIAHQCXARBDArQBkA4A3B9QAgBFAKBJQgDgDgEAAQgNgIgLgJQglgegpg9QgigzgpglQgeAZgmAYIhHAnQgWAJgTAKQgVAIgaAHQgdAIgjAJQAIAXAFAaIAFAbIgsAIg");
	this.shape_16.setTransform(1.8,-31.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#EE8427").s().p("AIpF8QgcgQghgkIg6g7QgygshDgTIgNgUQgWgegdgPQAbAAAbAJQA9ARBLA2QAsAgBUBAQBWA3BCgIQAmgGAigbQAggaAUgkQARgbAPgtIAJgfIARgIQgYB1hKBHQg0AwhIALQgSADgRAAQg2AAgpgcgAkjh3Qg8hdhQhOQhThQiiAFQiFAAhfBLQAAgQAGgPQAJgYAWgPQAagTBzgUQB2gWBaAoQBeAoBQBpQBQBmAgA/QAdA8ACAoQgthMgthIg");
	this.shape_17.setTransform(1.5,-34.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF3333").s().p("AhBCHQgegEgbgIQgcgKgHgSQgHgNADgmQAAgVAFgcQADgeAGgcQAIgoALgmQAGABAFAEQAZAJAeAGIAcADQAZAFAZAAQA1AAA7gHIATBIQAIAqAFAqQAJBRgRAGIhEALQgoAIgnAAQgjAAgjgHg");
	this.shape_18.setTransform(-10.1,-11);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#EAF0EF").s().p("Ah+CNQgkgIgRgYQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgMgUAGgfQAGhnAkhsIAAgCQAFAEAJADIAIADQgKAmgJAoQgFAcgDAeQgFAcAAAVQgEAmAHANQAHASAdAKQAbAIAdAEQBIANBOgOIBEgLQARgGgJhRQgFgqgJgqIgThIIAQgBQAFgDAHAAQADAfAIAkQAHA6ALBCQAAA1gHAOQgUAZgxAAQg3AFgvAAQhRAAg5gRg");
	this.shape_19.setTransform(-10.6,-10.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#6699FF").s().p("AgECyQhUgEgjgHQghgJgpgWQgFgBgFgEQgKgDADg4IAOh/QAHhDANg4QALAKANAIQAEAAADACIAAACQgkBsgGBoQgGAfAMATQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABQARAXAkAJQBaAaCWgPQAxAAAUgZQAHgNAAg1QgLhCgHg6QgIglgDgeIAZgBQAKAXAJAeQAHAoAFAyIAAAQQAFBHgFAcQgEAJgIAJQgUAPgmAJQgxAKg8AAIgmgBg");
	this.shape_20.setTransform(-10.2,-10.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCD518").s().p("AJGGsQgbg/gzgqQgYgWgdgRQhYguhvgRQhPgOhXAEQhVAEhKASQhDAQg8AgQgbANgbAVIgWAPIgGAHIgWARQgfAbgWAfQgUAZgPAeQgCgFgFgFQgWgggSgUQAEgYAHgWIAxjGIAmiYIALg1QAGgjAAgGQADgWgJgRQgFgLgGgGQgTgXgigFIgWgBQgIAAgFABQgRAAgWAFIgHABIgFgbQgFgagIgYQAjgJAdgIQAagHAVgIQATgKAWgJIBHgnQAmgYAegZQApAlAiAzQApA+AlAeQgNA4gHBDIgOB/QgDA4AKACQAFAFAFAAQApAWAgAJQAjAIBUAEQBUAEBAgNQAmgJAUgPQAIgKAEgIQAFgcgFhIIAAgOQgFgzgHgoQgJgfgKgXQAWgEAnACIAxAGQAjACAeAPQAHACAGAGQAdAPAWAeIANAUQAlA2AQBAIAJAkQAMA3AKA5QAPBLANBNIAJAoQgWAcgpAeIgYATQgIgZgJgXgAp5iuQgngdgJglQgLgnASgWQALgOAegWQAqAwAeA2QAWAnARApQgSACgQAAQgwAAgdgVgApEl2QAWgTgHgdQA5g0BPgBQAvAAApAMQAxAOAsAdIAhAYQgsAjhSAwQgwAYgoAMQgnALgiACQgchHgygngAoVmEQgDAFAAAGQAAAIADACQAFAFAGAAQAIAAABgFQAFgCAAgIQAAgGgFgFQgBgDgIAAQgGAAgFADg");
	this.shape_21.setTransform(-23.3,-8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000033").ss(3,1,1).p("EAlJAL2QAACzh+B+Qh/B+iyAAMg8zAAAQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzg");
	this.shape_22.setTransform(109.6,-5.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("A+ZSlQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzIAAXrQAACzh+B+Qh/B+iyAAg");
	this.shape_23.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22}]}).wait(1));

	// Layer_3
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(255,255,255,0.02)").s().p("AeaTXMg8zAAAQjHAAiNiMQiNiNAAjIIAA3rQAAjICNiNQCNiMDHAAMA8zAAAQDHAACNCMQCNCNAADIIAAXrQAADIiNCNQiNCMjHAAIAAAAg");
	this.shape_24.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_24).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy12, new cjs.Rectangle(-133,-129,485.4,247.9), null);


(lib.Symbol2copy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#330404").s().p("AAbhYIAGAAQAjAAAPAQQAGAIACAKQAEAGACAOQAEAbgDAOQgBAWgNAMQgIAJgWAHIgBAAIgfALIgMADIg9AKQgYADgXAFQA5hOBEhjg");
	this.shape.setTransform(294.9,-5.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D71B2B").s().p("AhIAzQgwAAgVgFIADg8IAKAEQBJAVA/AAIAZgBQAogEAYgPQAWgNAKgWIABAAQAJgDADgEQgPAagZAUQgbAWgjASIgHADQghAOhDAAIgFgBg");
	this.shape_1.setTransform(265.4,-62.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E63544").s().p("Ah7AvIgLgEQABgsACgsIAAgNQAQgIAJgCQALAAALAEQANADAbAQQAoAXAqAPQAiALAiAIQAQADAKgCIADAAIgCAAQgKAWgVANQgZAQgnAEIgaABQg/AAhIgWg");
	this.shape_2.setTransform(264.9,-68.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D1D7D7").s().p("AgVChIgWgBQARiCAQhNIAIglQAUhWASAHQAEAJACAOIACAIIAAANQgCAsAAAtIgDA8IgGCGIg2gDg");
	this.shape_3.setTransform(247.3,-61.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9C5050").s().p("AHEA/Ig1gHQi7gbi/gFIgegBQjDgGhgADQhmADg6ATQAAgFACgEQACgWANgUIAPgPQASgSAXgKQAagMAigEQAEAAADgCIAjAAQAQgCATACIESAJIDUAJIAWABIA3ADIAkADIAcADQANABAJAEQAKAFAJAIIAIAKQASAXAOAxIADAOQgEgGgFgDg");
	this.shape_4.setTransform(219.3,-39.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#56ACEC").s().p("AEUA3QATgXARgaIAFgJQAQgXADgSQAFADADAEQASAQAAAXQAAAYgSAQQgOANgSAEIgKAAQgOAAgMgEgAhFAqQAygpAZgvQAGAEAFAFQASAQAAAXQAAAYgSAQQgSARgZAAQgZAAgSgRgAlpAnQAKgLAKgPQAVgdAIgTIAMgXIAGAEQATARgBAXQABAYgTARQgSAQgbAAQgLAAgLgEg");
	this.shape_5.setTransform(217.1,-22.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#66B4EE").s().p("AEiAyQgUgQAAgYQAAgXAUgQQARgRAbAAIAKAAQANACALAIQgDASgQAWIgFAKQgRAagTAXQgKgEgIgJgAg4AKQAAgXATgQQASgRAYAAQATAAAOAIQgaAvgxApQgTgQAAgYgAldAjQgSgRgBgYQABgXASgRQASgQAYAAQAWAAARAMIgMAXQgIATgVAdQgKAPgKALQgLgEgJgIg");
	this.shape_6.setTransform(213.9,-23.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#6D0A0A").s().p("AiXJRQCph7CljGQA0gIA0gKIBBgMIA+gMIArgIQgKBQghBLQgoBdhJBWIgOAEQg/APhBANIhxATIgxAHQgiAEghADQhIAJhHAFIA+gqgAnIoPQA3gzAzg4QAjAHAXAJIAPAFIgBAEQgBAQgCANIgCANIAAAEIgHApIgFAaQhPgShSgNg");
	this.shape_7.setTransform(254.5,-8.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#850F0F").s().p("AgtJyQg8gHg5gKIghgFQhxgXhsgeIg+gTQhdgehcgoQg7hWglhYQgbhCgPhDQAVAJAVAKQCEA4CVAlQACAAADACIA5ANQBpAWB0ANIAhADQA5AGA8ADQCyALDHgMQBpgFBvgMIBDgHQAZgFAZgCIBxgRIAYgDQimDGipB7Ig/AqIhIACIhWACQiVAAiOgSgAH3g4QgNgFgLgKIgCgCQgZgZAAgiQAAgkAbgaQAbgaApAAQADAAAHADQAfABAYAWQAbAaAAAkQAAAkgbAZQgbAagmAAQgaAAgSgLgAH3irQgVAPABAYQgBAYAVAQQAHAJALAEQAMAFAOAAIAKgBQASgEANgNQATgQgBgYQABgYgTgPQgDgFgEgCQgLgIgNgCIgKAAQgbAAgRARgACYhHIgCgCIAAAAQgZgZAAgiQgBgkAcgaQAbgaAoAAQAYAAAUALIAVAPQAcAaAAAkQAAAigZAZIgDACQgaAagnAAQgoAAgbgagACvirQgTAPgBAYQABAYATAQQARASAZAAQAaAAASgSQASgQAAgYQAAgYgSgPQgFgGgFgEQgPgHgTAAQgZAAgRARgAiKhIQgBAAAAAAQAAAAAAAAQAAAAAAAAQgBgBAAAAQgKgGgJgHQgbgagBglQABgjAbgaQAbgZAmAAQAoAAAbAZQAbAaAAAjQAAAlgbAaQgJAHgJAGQgWAMgbAAQgZAAgTgLgAiIi8QgTARAAAXQAAAYATARQAIAIALAEQAMAEALAAQAaAAASgQQATgRAAgYQAAgXgTgRIgFgFQgRgLgWAAQgZAAgRAQgAgeooIABgGIADgSIADgUQADgEAAgEIAFgaQAUgHAdgBQBugNB1AVIAHABQgzA4g3AzIgfgEQhCgJgsAEQgeACgbAIQACgPAEgQg");
	this.shape_8.setTransform(192.7,-9.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D4C5C5").s().p("AoDMJQgdgEgbgFIgngKQBLgSBKgXQCvg5CVhTQAkgVAkgXQBHgGBIgIQAhgDAigFIAygGIBxgUQBBgMA/gPIAOgEIgOAQQgiAngqAmQgYAWgcAWIgzAkIg+AkQgzAdg6AXIgIACQgwARgxAOQgsALgtAJIgdAEQhVAOhcADIgmABQhVAAhOgNgAFbCYQAug5A9hPQAXgFAYgDIA+gJIAMgDIAfgLIABAAQAEAaAAAfQAAAngFAnIgrAIIg+AMIhBAMQg0AKg0AIIAPgSgAnGnkQgSgCgRACIAyglQBGgzA+g5QBSAMBPATIgFAUIgBAFIgHAkIgEAKQgGAYgJAXIgBAFgAiQrQQgWgJgjgHIAvg1IAOAGQAGAEAFAGIABAEQADALgCAhQgCAFABAFIgQgFg");
	this.shape_9.setTransform(239.7,1.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E2E2E2").s().p("AjVMPIgOgEIgUgHQgigMghgOQgygXgugdIg3gkIgDgEQgfgXgfgbQgdgagbgeQhJhQg4hSQBcAoBdAeIA+ATQBtAeBwAXIAiAFQA4AKA8AHQC3AXDCgHIBJgCQgkAXglAVQiVBTiuA5QhJAXhMASIgVgGgAhhD9Qg8gDg4gGIgigDQh0gNhpgWIg5gNQgDgCgCAAQiUgliFg4QgUgKgWgJIgGgYIgDgRQgFgjgCgjQgDgpADgmQADAAAEADQABAAAAAAQABAAAAAAQAAABABAAQAAAAAAAAIAwAQQCMAuCNAlIAFAAIA5ALQBuAaBvASQBLALBLAJQEjAhEugPIBDgFQAZgBAZgCQApgDAogFQAQgCAQgBQBBgIA/gKIASgBQg9BPguA5IgPASIgYADIhxARQgZACgZAFIhDAHQhvAMhpAFQhoAHhiAAQhZAAhWgGgAhhhcQhKgJhMgOQhTgOhTgTIACgBQAbhNAdgkQAcgfAegVQAYgQAagKIACAAQA5gTBmgDQBggDDDAGIAeABQDAAFC6AbIA1AHQAGADADAGQASAZAABNIABB9IhRAJIgyADIhDAFQhPAEhPAAQjYAAjbgegAGskOQgbAaAAAkQAAAiAYAZIADACQAKAKANAFQASALAaAAQAmAAAbgaQAbgZAAgkQAAgkgbgaQgYgWgfgBQgHgDgDAAQgpAAgaAagABkkOQgbAaAAAkQAAAiAZAZIAAAAIACACQAbAaAoAAQAnAAAagaIADgCQAZgZAAgiQAAgkgcgaIgUgPQgVgLgYAAQgoAAgbAagAjTkeQgbAaAAAjQAAAlAbAaQAJAHALAGQAAAAAAABQAAAAAAAAQAAAAAAAAQABAAAAAAQAUALAYAAQAbAAAWgMQAJgGAJgHQAcgaAAglQAAgjgcgaQgagZgpAAQgmAAgbAZgAhsncIAFgeQACgKAAgLIAEgUIAGggIADgSQAbgIAfgCQAsgEBBAJIAfAEQg+A5hFAzIgyAlIgiAAQgDACgFAAIAFgZgADRrCQh0gVhuANQgdABgUAHIADgLQADgNAEgMIADgFQAEgLAHgIQASgUAagCQAYgBAYABQAOAAAOACIAcABQAOADAOABIA3AIIAzAJIAYAGIgwA1IgHgBg");
	this.shape_10.setTransform(197.8,-1.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#4F0909").s().p("AiMCoQhLgIhKgLQhwgThtgZIg5gNIgGAAQiMgkiNguIgvgPQgBgBAAAAQAAAAgBAAQAAgBAAAAQgBAAAAAAQgEgCgEgBQgFgBgDgCQgHgEgFgGQgFgHgDgQIgKg0QgFgbABgZQAAgXAQgLQAPgKATAGIADABIABAAIAnARIAHABIBkAqQBbAcBaAYIAGABIA5AOQAHADAGAAIApAJIABACQBTASBTAPQBLANBKAIQEsApElgPIBEgEIAygEIBQgJIAhgEQBAgIBAgKIAagFIAngIQAjgHAbgDIABAAIAQgBQhFBkg6BOIgRABQhAAKhAAHQgRABgPADQgoAEgpADQgZADgZABIhEAFQhdAEhdAAQjOAAjJgXg");
	this.shape_11.setTransform(202.1,-11.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#282727").s().p("AHQE8QgjAAgbgaQgYgaAAgmIAAioIDVAAIAEAAIAACoQAAAmgbAaQgXAagkAAgAn8E8QglAAgagaQgXgaAAgmIAAioIDYAAIAACoQAAAmgaAaQgYAagkAAgAjRgSQgMABgKgLQgJgIAAgNQAAgMAJgLQAKgIAMAAIGfAAQANAAAJAIQAKALAAAMQAAANgKAIQgJALgNgBgAjRiIQgMAAgKgKQgJgIAAgNQAAgMAJgKQAKgKAMAAIGfAAQANAAAJAKQAKAKAAAMQAAANgKAIQgJAKgNAAgAjRj+QgMABgKgKQgJgJAAgMQAAgNAJgKQAKgJAMAAIGfAAQANAAAJAJQAKAKAAANQAAAMgKAJQgJAKgNgBg");
	this.shape_12.setTransform(0.2,50.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#3A3737").s().p("AjfC0QhDAAAAhDIAAh7IAAg2IAAgwIABgSQAHgxA7AAIG+AAQA7AAAIAxQABAIAAAKIAAAwIAAA2IAAB7QAABDhEAAgAjiBeQgIALAAALQAAANAIAJQAKAKANAAIGfAAQANAAAIgKQALgJAAgNQAAgLgLgLQgIgJgNAAImfAAQgNAAgKAJgAjigWQgIAJAAAMQAAAMAIAJQAKAJANAAIGfAAQANAAAIgJQALgJAAgMQAAgMgLgJQgIgKgNAAImfAAQgNAAgKAKgAjiiMQgIAJAAANQAAAMAIAJQAKAKANAAIGfAAQANAAAIgKQALgJAAgMQAAgNgLgJQgIgJgNAAImfAAQgNAAgKAJg");
	this.shape_13.setTransform(-0.4,34);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AISBqQgigEgZgaQgLgMgHgMQgOgXAAgcQAAgqAggfQAfghAsAAQAsAAAfAhQAeAfAAAqQAAAcgLAXQgIAMgLAMQgaAaghAEgApZBbQgKgHgIgIQgMgMgFgMQgOgXAAgcQAAgqAfgfQAfghAsAAQArAAAhAhQAfAfAAAqQAAAcgOAXQgFAMgMAMQgSASgSAHQgRAEgRAAQgiAAgdgOg");
	this.shape_14.setTransform(-0.3,22.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#DEB50A").s().p("AJ/B0QALgXAAgcQAAgrgegeQgfghgsAAQgsAAgfAhQggAeAAArQAAAcAOAXIgWAAQggglAAg2QAAgxAfgnIAKgMQAqgoA7AAQA8AAApAoQAHAGACAGQAhAnAAAxQAAA2ghAlgAnEB0QAOgXAAgcQAAgrgfgeQghghgrAAQgsAAgfAhQgfAeAAArQAAAcAOAXIgNAAQggglAAg2QAAgxAfgnIAKgMQApgoA9AAQA7AAApAoIAKAMQAfAnAAAxQAAA2gfAlg");
	this.shape_15.setTransform(-0.2,15.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FECF09").s().p("AJUDrIjVAAIr0AAIjYAAIgHAAQg2gBgggWQgwgfAAhOIAAhnICsAAIAOABIASgBIDrAAIAAB8QAABDBDAAIG/AAQBEAAAAhDIAAh8IDvAAIARABIAQgBICoAAIAABnQAABOgvAfQggAVg0ACIgEAAgAKMg1QAgglAAg2QAAgzgggmIBHAAIACAyIAGCCgAEjg1IAAgxQAAgKgCgHQgHgyg7ABIm/AAQg6gBgIAyIgBARIAAAxIiJAAQAfglAAg2QAAgzgfgmINbAAQgfAmAAAzQAAA2AgAlgArag1IAAi0IBQAAQgeAmAAAzQAAA2AgAlg");
	this.shape_16.setTransform(-0.3,33);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#212B28").s().p("AIzCXQAhgEAZgaQAMgMAHgMIAMAAIBPAAIAAA2gAEjCXIAAg2ICNAAIAVAAQAHAMAMAMQAYAaAiAEgAoOCXQALgBAMgEQASgHASgSQALgMAGgMIAWAAICJAAIAAA2gAraCXIAAg2IBSAAIAMAAQAGAMALAMQAIAIAKAHQASAMAZADgAKMhSQgDgGgGgGQgpgog8AAQg7AAgrAoIgJAMItbAAIgKgMQgpgog7AAQg8AAgpAoIgLAMIhQAAQAAgmAOgeIWcAAIADBEg");
	this.shape_17.setTransform(-0.3,17.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#F7FFC2").s().p("Ak8A5QgZAAgOgPQgRgSgBgYQABgWARgRQAOgRAZAAIJ5AAQAXAAAQARQARARAAAWQAAAYgRASQgQAPgXAAg");
	this.shape_18.setTransform(1.8,-73.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#98C8F7").s().p("AmxD8QAkhQA8hIQCCiLDkhfQC4hNDdgnQAIAOAAAWIAABbIgCAAQknBSjSCRQhkBFhMBPg");
	this.shape_19.setTransform(11.6,-31.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#84C0FC").s().p("AHmELIpoAAQBNhPBjhFQDTiREnhSIABAAIAAE0QAAA7g1AIIgOAAgAnlELIgHAAQg8gFAAg+IAAmPQAAgoAYgQQARgLAaAAIPLAAQAcAAAPALQALAHAFANQjdAoi4BMQjjBgiDCKQg7BJgkBPg");
	this.shape_20.setTransform(-0.3,-33.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E0AE10").s().p("AJRGLIymAAIAAgfIADAAQABAAAAAAQAAAAABAAQAAgBAAAAQAAAAAAgBQABgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAgCgCgDIgDAAIAAn2IADAAQAAAAABgBQAAAAABAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBIgDAAIAAhjQAAiGCFAAIOiAAQCEAAAACGIAABjIgDAAIgCAEQgBABAAABQAAAAAAABQAAAAAAABQABAAAAABIAAACQAAAAAAAAQABABAAAAQAAAAABAAQABAAAAAAIACAAIAAH2IgCAAQgDADAAACQAAAAgBAAQAAABAAAAQAAABAAABQAAAAABABQAAABAAAAQAAAAABABQAAAAAAAAQABAAAAAAIADAAIAAAfIgFAAgAoTihQgYAQAAAoIAAGPQAAA+A8AFIAHAAICsAAIC4AAIJnAAIAPAAQA0gIAAg7IAAk0IAAhbQAAgWgHgOQgGgNgLgHQgPgLgcABIvLAAQgagBgRALgAlRlWQgRARAAAWQAAAZARASQAPAPAZAAIJ5AAQAXAAAQgPQARgSAAgZQAAgWgRgRQgQgRgXAAIp5AAQgZAAgPARg");
	this.shape_21.setTransform(-0.1,-42.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#EEBE25").s().p("ArQE1QAbg9BeAAIABAAISmAAIAFAAQBZAAAeA9gAJVDZIgCAAQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAgBgBAAQAAgBAAgBQAAAAABgBQAAAAAAAAQAAgCAEgDIABAAQAegIAWgXQAlgmgFhMIAAgBQgVgCgOgQQgUgRAAgYIAAh6QAAgXAUgSQAPgRAZAAIAAAAIAAgPQgHgogegdQgWgZgegIIgBAAQgBAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAAAIAAgCQAAgBAAAAQgBgBAAAAQAAgBAAAAQABgBAAgBIADgEIACAAIACAAIARAGQAWAKAUAUQAfAgAHAsQABAJABAIIAAABQARADANANQARASAAAXIAAB6QAAAYgRARQgQAQgUACQAGBRgoArQgaAZghAIIgCAAgApWDZIgBAAQgggIgagZQgogrAEhRQgTgCgPgQQgRgRAAgYIAAh6QAAgXARgSQAMgNASgDIAAgBQACg1AmgoQAagbAggJIABAAIADAAQABABAAAAQABABAAAAQAAABAAAAQABABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAgBABQAAAAAAABQAAAAgBAAQAAABgBAAIgDAAQgcAIgXAZQgiAigDAyIABAAQAYAAAQARQATASAAAXIAAB6QAAAYgTARQgPAQgVACIAAABQgFBMAlAmQAXAXAcAIIADAAQADADAAACQAAAAAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAABgBAAQAAAAAAABQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_22.setTransform(0,-28.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer_1
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000033").ss(3,1,1).p("A+ZykMA8zAAAQCyAAB/B+QB+B+AACzIAAXrQAACzh+B+Qh/B+iyAAMg8zAAAQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAg");
	this.shape_23.setTransform(109.6,-5.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("A+ZSlQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzIAAXrQAACzh+B+Qh/B+iyAAg");
	this.shape_24.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23}]}).wait(1));

	// Layer_3
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("rgba(255,255,255,0.02)").s().p("AeaTXMg8zAAAQjHAAiNiMQiNiNAAjIIAA3rQAAjICNiNQCNiMDHAAMA8zAAAQDHAACNCMQCNCNAADIIAAXrQAADIiNCNQiNCMjHAAIAAAAg");
	this.shape_25.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_25).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy9, new cjs.Rectangle(-133,-129,485.4,247.9), null);


(lib.Symbol2copy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0099FF").s().p("AnnCCQgcgTgXgTQgfgXgagYQhUhTgdhZICDg5QATgIAVgLQAKAiAUAhIARAYQAVAZAbAaIApAiQAQAIARAMQAvAeA1AWQAWAKAZAHQCHAuCjAAQDCAACZg/QAzgWAxgeQAXgNAWgSQAvgjAkgpIAfgnIAZgnIABAAQAABOgtBMQg9BthuA2QhdAxiDAPQhBAIiuAGIl6ABQhLgehAgsg");
	this.shape.setTransform(212.3,47);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663366").s().p("AkyBuQgZgHgWgKQg1gWgvgfQgRgMgQgIIgpghQgbgagVgZIgRgYQgUghgKgiQAFAEAfAEQALAAAVADQgHAWgEAYQASAUAWAgQAFAFACAFQAPgeAUgZIAhAbQBEA4BMAjQCJA+CoAAQEEAAC4iZQAIgEAFgHQAJAXAIAZIAYgTQApgeAWgcIgJgoIAvAAIAZAAIgZAnIgfAnQgkApgvAiQgWASgXANQgxAfgzAWQiZA/jBAAQikAAiHgug");
	this.shape_1.setTransform(220.8,42.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0066CC").s().p("Ai1DwQjsgBi+hSIF7gBQCugGBAgIQCDgPBegxQBug2A8htQAthMAAhOICdBTQgXA+g7BAIgeAdQgtAmg7ApQjvCilDAAIgKAAg");
	this.shape_2.setTransform(238.2,51.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AhBCHQgegEgbgIQgcgKgHgSQgHgNADgmQAAgVAFgcQADgeAGgcQAIgoALgmQAGABAFAEQAZAJAeAGIAcADQAZAFAZAAQA1AAA7gHIATBIQAIAqAFAqQAJBRgRAGIhEALQgoAIgnAAQgjAAgjgHg");
	this.shape_3.setTransform(221.4,-11);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EAF0EF").s().p("Ah+CNQgkgIgRgYQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgMgUAGgfQAGhnAkhsIAAgCQAFAEAJADIAIADQgKAmgJAoQgFAcgDAeQgFAcAAAVQgEAmAHANQAHASAdAKQAbAIAdAEQBIANBOgOIBEgLQARgGgJhRQgFgqgJgqIgThIIAQgBQAFgDAHAAQADAfAIAkQAHA6ALBCQAAA1gHAOQgUAZgxAAQg3AFgvAAQhRAAg5gRg");
	this.shape_4.setTransform(220.9,-10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6699FF").s().p("AgECyQhUgEgjgHQghgJgpgWQgFgBgFgEQgKgDADg4IAOh/QAHhDANg4QALAKANAIQAEAAADACIAAACQgkBsgGBoQgGAfAMATQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABQARAXAkAJQBaAaCWgPQAxAAAUgZQAHgNAAg1QgLhCgHg6QgIglgDgeIAZgBQAKAXAJAeQAHAoAFAyIAAAQQAFBHgFAcQgEAJgIAJQgUAPgmAJQgxAKg8AAIgmgBg");
	this.shape_5.setTransform(221.3,-10.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#413030").s().p("AgLAKQgCgDAAgHQAAgGACgFQAGgCAFAAQAIAAABACQAFAFAAAGQAAAHgFADQgBAEgIAAQgFAAgGgEg");
	this.shape_6.setTransform(155.9,-46);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FCD518").s().p("AJGGsQgbg/gzgqQgYgWgdgRQhYguhvgRQhPgOhXAEQhVAEhKASQhDAQg8AgQgbANgbAVIgWAPIgGAHIgWARQgfAbgWAfQgUAZgPAeQgCgFgFgFQgWgggSgUQAEgYAHgWIAxjGIAmiYIALg1QAGgjAAgGQADgWgJgRQgFgLgGgGQgTgXgigFIgWgBQgIAAgFABQgRAAgWAFIgHABIgFgbQgFgagIgYQAjgJAdgIQAagHAVgIQATgKAWgJIBHgnQAmgYAegZQApAlAiAzQApA+AlAeQgNA4gHBDIgOB/QgDA4AKACQAFAFAFAAQApAWAgAJQAjAIBUAEQBUAEBAgNQAmgJAUgPQAIgKAEgIQAFgcgFhIIAAgOQgFgzgHgoQgJgfgKgXQAWgEAnACIAxAGQAjACAeAPQAHACAGAGQAdAPAWAeIANAUQAlA2AQBAIAJAkQAMA3AKA5QAPBLANBNIAJAoQgWAcgpAeIgYATQgIgZgJgXgAp5iuQgngdgJglQgLgnASgWQALgOAegWQAqAwAeA2QAWAnARApQgSACgQAAQgwAAgdgVgApEl2QAWgTgHgdQA5g0BPgBQAvAAApAMQAxAOAsAdIAhAYQgsAjhSAwQgwAYgoAMQgnALgiACQgchHgygngAoVmEQgDAFAAAGQAAAIADACQAFAFAGAAQAIAAABgFQAFgCAAgIQAAgGgFgFQgBgDgIAAQgGAAgFADg");
	this.shape_7.setTransform(208.2,-8.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EC6108").s().p("AI4EcQhUhAgsggQhLg2g9gRQgbgJgbAAQgGgGgHgCQA7gwA2gMQAdgJAbAEQAkABAqAaQAaAMAvAgQB4BBBggIQA3gEA5gaIgJAfQgPAtgRAbQgUAkggAaQgiAbgmAGIgQABQg9AAhLgwgAsFBNQgRgpgWgmQgeg2gqgwQAUgPAagRIAHgFQAyAnAcBHQAigCAngLQAogMAwgYQBSgwAsgjIghgYQgsgdgxgOQgpgMgvAAQhPABg5A0QgEgUgNgWQgMgRgcghQA1g4CIAHQCXARBDArQBkA4A3B9QAgBFAKBJQgDgDgEAAQgNgIgLgJQglgegpg9QgigzgpglQgeAZgmAYIhHAnQgWAJgTAKQgVAIgaAHQgdAIgjAJQAIAXAFAaIAFAbIgsAIg");
	this.shape_8.setTransform(233.3,-31.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EE8427").s().p("AIpF8QgcgQghgkIg6g7QgygshDgTIgNgUQgWgegdgPQAbAAAbAJQA9ARBLA2QAsAgBUBAQBWA3BCgIQAmgGAigbQAggaAUgkQARgbAPgtIAJgfIARgIQgYB1hKBHQg0AwhIALQgSADgRAAQg2AAgpgcgAkjh3Qg8hdhQhOQhThQiiAFQiFAAhfBLQAAgQAGgPQAJgYAWgPQAagTBzgUQB2gWBaAoQBeAoBQBpQBQBmAgA/QAdA8ACAoQgthMgthIg");
	this.shape_9.setTransform(233,-34.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EE6B18").s().p("AE1DlIgIgDQgJgDgFgFQgLhJgfhGQg3h8hkg3QhEgsiVgRQiIgHg2A4IgYgbIgGgMIgEgCQBfhMCGAAQChgEBTBQQBPBOA9BbQAsBIAuBOQAAAkggAgIAAAEQgFgEgGgBg");
	this.shape_10.setTransform(177.6,-48.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#413030").s().p("AgLAKQgCgDAAgHQAAgGACgFQAGgCAFAAQAIAAABACQAFAFAAAGQAAAHgFADQgBAEgIAAQgFAAgGgEg");
	this.shape_11.setTransform(-75.6,-46);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EE6B18").s().p("AE1DlIgIgDQgJgDgFgFQgLhJgfhGQg3h8hkg3QhEgsiVgRQiIgHg2A4IgYgbIgGgMIgEgCQBfhMCGAAQChgEBTBQQBPBOA9BbQAsBIAuBOQAAAkggAgIAAAEQgFgEgGgBg");
	this.shape_12.setTransform(-53.9,-48.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0066CC").s().p("Ai1DwQjsgBi9hSIF6gBQCugGBAgIQCDgPBdgxQBvg2A8htQAthMAAhOICeBTQgYA+g7BAIgeAdQgtAmg6ApQjwCilDAAIgKAAg");
	this.shape_13.setTransform(6.7,51.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0099FF").s().p("AnnCCQgcgTgXgTQgfgXgagYQhUhTgdhZICDg5QATgIAVgLQAKAiAUAhIARAYQAVAZAbAaIApAiQAQAIARAMQAvAeA1AWQAWAKAZAHQCHAuCjAAQDCAACZg/QAzgWAxgeQAXgNAWgSQAvgjAkgpIAfgnIAZgnIABAAQAABOgtBMQg9BthuA2QhdAxiDAPQhBAIiuAGIl6ABQhLgehAgsg");
	this.shape_14.setTransform(-19.2,47);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663366").s().p("AkyBuQgZgHgWgKQg1gWgvgfQgRgMgQgIIgpghQgbgagVgZIgRgYQgUghgKgiQAFAEAfAEQALAAAVADQgHAWgEAYQASAUAWAgQAFAFACAFQAPgeAUgZIAhAbQBEA4BMAjQCJA+CoAAQEEAAC4iZQAIgEAFgHQAJAXAIAZIAYgTQApgeAWgcIgJgoIAvAAIAZAAIgZAnIgfAnQgkApgvAiQgWASgXANQgxAfgzAWQiZA/jBAAQikAAiHgug");
	this.shape_15.setTransform(-10.7,42.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#EC6108").s().p("AI4EcQhUhAgsggQhLg2g9gRQgbgJgbAAQgGgGgHgCQA7gwA2gMQAdgJAbAEQAkABAqAaQAaAMAvAgQB4BBBggIQA3gEA5gaIgJAfQgPAtgRAbQgUAkggAaQgiAbgmAGIgQABQg9AAhLgwgAsFBNQgRgpgWgmQgeg2gqgwQAUgPAagRIAHgFQAyAnAcBHQAigCAngLQAogMAwgYQBSgwAsgjIghgYQgsgdgxgOQgpgMgvAAQhPABg5A0QgEgUgNgWQgMgRgcghQA1g4CIAHQCXARBDArQBkA4A3B9QAgBFAKBJQgDgDgEAAQgNgIgLgJQglgegpg9QgigzgpglQgeAZgmAYIhHAnQgWAJgTAKQgVAIgaAHQgdAIgjAJQAIAXAFAaIAFAbIgsAIg");
	this.shape_16.setTransform(1.8,-31.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#EE8427").s().p("AIpF8QgcgQghgkIg6g7QgygshDgTIgNgUQgWgegdgPQAbAAAbAJQA9ARBLA2QAsAgBUBAQBWA3BCgIQAmgGAigbQAggaAUgkQARgbAPgtIAJgfIARgIQgYB1hKBHQg0AwhIALQgSADgRAAQg2AAgpgcgAkjh3Qg8hdhQhOQhThQiiAFQiFAAhfBLQAAgQAGgPQAJgYAWgPQAagTBzgUQB2gWBaAoQBeAoBQBpQBQBmAgA/QAdA8ACAoQgthMgthIg");
	this.shape_17.setTransform(1.5,-34.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF3333").s().p("AhBCHQgegEgbgIQgcgKgHgSQgHgNADgmQAAgVAFgcQADgeAGgcQAIgoALgmQAGABAFAEQAZAJAeAGIAcADQAZAFAZAAQA1AAA7gHIATBIQAIAqAFAqQAJBRgRAGIhEALQgoAIgnAAQgjAAgjgHg");
	this.shape_18.setTransform(-10.1,-11);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#EAF0EF").s().p("Ah+CNQgkgIgRgYQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgMgUAGgfQAGhnAkhsIAAgCQAFAEAJADIAIADQgKAmgJAoQgFAcgDAeQgFAcAAAVQgEAmAHANQAHASAdAKQAbAIAdAEQBIANBOgOIBEgLQARgGgJhRQgFgqgJgqIgThIIAQgBQAFgDAHAAQADAfAIAkQAHA6ALBCQAAA1gHAOQgUAZgxAAQg3AFgvAAQhRAAg5gRg");
	this.shape_19.setTransform(-10.6,-10.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#6699FF").s().p("AgECyQhUgEgjgHQghgJgpgWQgFgBgFgEQgKgDADg4IAOh/QAHhDANg4QALAKANAIQAEAAADACIAAACQgkBsgGBoQgGAfAMATQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABQARAXAkAJQBaAaCWgPQAxAAAUgZQAHgNAAg1QgLhCgHg6QgIglgDgeIAZgBQAKAXAJAeQAHAoAFAyIAAAQQAFBHgFAcQgEAJgIAJQgUAPgmAJQgxAKg8AAIgmgBg");
	this.shape_20.setTransform(-10.2,-10.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FCD518").s().p("AJGGsQgbg/gzgqQgYgWgdgRQhYguhvgRQhPgOhXAEQhVAEhKASQhDAQg8AgQgbANgbAVIgWAPIgGAHIgWARQgfAbgWAfQgUAZgPAeQgCgFgFgFQgWgggSgUQAEgYAHgWIAxjGIAmiYIALg1QAGgjAAgGQADgWgJgRQgFgLgGgGQgTgXgigFIgWgBQgIAAgFABQgRAAgWAFIgHABIgFgbQgFgagIgYQAjgJAdgIQAagHAVgIQATgKAWgJIBHgnQAmgYAegZQApAlAiAzQApA+AlAeQgNA4gHBDIgOB/QgDA4AKACQAFAFAFAAQApAWAgAJQAjAIBUAEQBUAEBAgNQAmgJAUgPQAIgKAEgIQAFgcgFhIIAAgOQgFgzgHgoQgJgfgKgXQAWgEAnACIAxAGQAjACAeAPQAHACAGAGQAdAPAWAeIANAUQAlA2AQBAIAJAkQAMA3AKA5QAPBLANBNIAJAoQgWAcgpAeIgYATQgIgZgJgXgAp5iuQgngdgJglQgLgnASgWQALgOAegWQAqAwAeA2QAWAnARApQgSACgQAAQgwAAgdgVgApEl2QAWgTgHgdQA5g0BPgBQAvAAApAMQAxAOAsAdIAhAYQgsAjhSAwQgwAYgoAMQgnALgiACQgchHgygngAoVmEQgDAFAAAGQAAAIADACQAFAFAGAAQAIAAABgFQAFgCAAgIQAAgGgFgFQgBgDgIAAQgGAAgFADg");
	this.shape_21.setTransform(-23.3,-8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000033").ss(3,1,1).p("EAlJAL2QAACzh+B+Qh/B+iyAAMg8zAAAQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzg");
	this.shape_22.setTransform(109.6,-5.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("A+ZSlQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzIAAXrQAACzh+B+Qh/B+iyAAg");
	this.shape_23.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22}]}).wait(1));

	// Layer_3
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(255,255,255,0.02)").s().p("AeaTXMg8zAAAQjHAAiNiMQiNiNAAjIIAA3rQAAjICNiNQCNiMDHAAMA8zAAAQDHAACNCMQCNCNAADIIAAXrQAADIiNCNQiNCMjHAAIAAAAg");
	this.shape_24.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_24).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy8, new cjs.Rectangle(-133,-129,485.4,247.9), null);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy9();
	this.instance.parent = this;
	this.instance.setTransform(212.5,5.8,1.072,1.072,0,0,0,3.1,-0.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,0,1.213,0.668);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-616.3,-182.4,1232.7,365);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween17("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(4.1,2,1.02,1.02);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.05,scaleY:1.05},9).to({scaleX:1.02,scaleY:1.02},10).to({scaleX:1.05,scaleY:1.05},10).to({scaleX:1.02,scaleY:1.02},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2copy9();
	this.instance.parent = this;
	this.instance.setTransform(212.5,-23.7,1.072,1.072,0,0,0,3.1,-0.9);

	this.instance_1 = new lib.Symbol2copy8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-441.1,-22.9,1.072,1.072,0,0,0,3.1,-0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(3,42,74,0.498)").s().p("EhHkAqrQjPAAiTiTQiSiSAAjPMAAAhFtQAAjPCSiSQCTiTDPAAMCPJAAAQDPAACTCTQCSCSAADPMAAABFtQAADPiSCSQiTCTjPAAg");
	this.shape.setTransform(0,-29.5,1.213,0.668);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-616.3,-211.9,1232.7,365);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2copy12();
	this.instance.parent = this;
	this.instance.setTransform(-114.2,4.7,1.072,1.072,0,0,0,3.1,-0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1,x:-114.1,y:4.6},9).to({scaleX:1.07,scaleY:1.07,x:-114.2,y:4.7},10).to({scaleX:1.1,scaleY:1.1,x:-114.1,y:4.6},10).to({scaleX:1.07,scaleY:1.07,x:-114.2,y:4.7},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-260.1,-132.8,520.3,265.7);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween22("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(120.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.02,scaleY:1.02,x:120},9).to({scaleX:1,scaleY:1,x:120.1},10).to({scaleX:1.02,scaleY:1.02,x:120},10).to({scaleX:1,scaleY:1,x:120.1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-82.7,-36.3,405.5,72.8);


(lib.fxTween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol2copy();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FF6600",0,0,18);
	this.instance.filters = [new cjs.BlurFilter(4, 4, 1)];
	this.instance.cache(-19,-19,38,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37.8,-37.8,78,78);


(lib.fxTween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxSymbol3();
	this.instance.parent = this;
	this.instance.shadow = new cjs.Shadow("#FFFFFF",0,0,10);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.5,-49.6,106,102);


(lib.fxSymbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.fxTween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,0.205,0.205,0,0,0,0.3,0);
	this.instance.alpha = 0.109;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,scaleX:0.5,scaleY:0.5,rotation:128.6,y:0.1,alpha:1},6).to({regX:0,scaleX:0.51,scaleY:0.51,rotation:180,y:0},7).to({scaleX:0.32,scaleY:0.32,alpha:0},4).wait(3));

	// Layer_2
	this.instance_1 = new lib.fxTween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.1,0.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:-0.1,regY:0.1,scaleX:1.18,scaleY:1.5,rotation:-23,x:-0.3,y:0.4},2).to({regX:0,regY:0,scaleX:1.54,scaleY:2.5,rotation:0,x:-0.1,y:0.1},4).to({regX:-0.1,regY:0.1,scaleX:2.33,scaleY:2.97,x:-0.4,y:0.4,alpha:0.672},3).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,x:0,y:0,alpha:0},6).wait(1));

	// Layer_2
	this.instance_2 = new lib.fxTween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,0.2,0.64,0.64);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:-0.1,regY:0.2,scaleX:3.05,scaleY:1.06,rotation:22.7,x:-0.5,y:0.5,alpha:1},6).to({regX:0,regY:0,scaleX:3.91,scaleY:3.91,rotation:0,x:0,y:0,alpha:0.109},6).wait(8));

	// Layer_3
	this.instance_3 = new lib.fxTween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.3,-0.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({rotation:90,x:28.3,y:-14.5},7).to({rotation:180,x:55.6,y:-25,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_4 = new lib.fxTween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.3,-0.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({rotation:90,x:-29.7,y:-12.9},7).to({rotation:180,x:-56.6,y:-28.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_5 = new lib.fxTween4("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(0.3,-0.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({scaleX:0.5,scaleY:0.5,rotation:90,x:0.6,y:-25},7).to({scaleX:1,scaleY:1,rotation:180,x:-4.2,y:-66.2,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_6 = new lib.fxTween4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(0.3,-0.3);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({rotation:90,x:30.4,y:36},7).to({rotation:180,x:55.6,y:35.7,alpha:0.109},6).wait(1));

	// Layer_3
	this.instance_7 = new lib.fxTween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0.3,-0.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({rotation:90,x:-20.8,y:33.3},7).to({rotation:180,x:-45.5,y:41.7,alpha:0.109},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.9,-31.6,66,66);


(lib.Tween2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AiqD/QgWgRgTgWQhMhYAGh2QAIh0BYhOQBYhNBzAIQAUABARADQA/AMAyAlQAYASAUAXQA+BGAJBX");
	this.shape.setTransform(-12,-29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("Ah4CnQgLgJgJgKQgzg8AEhNQAFhOA7gyQA6g1BNAFQBOAEA1A8QAlAoAIA1");
	this.shape_1.setTransform(-12,-28.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AhSiXQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQg0AXgMgUQgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFQATACAUABQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdQACAEACAEQAQAkASAdQAGANAKAMQACADACAFQAYAgAbAaQA/A7ANAIAA/jPQBUANBBB1");
	this.shape_2.setTransform(22.2,15.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AmEH0QgmhAAihJQAdhAA/gnQBAgoA5AFQASABAlAFIAnADQhNiNhfhiQgYgagTgdQgNgSgMgVQgXgogTgwQgOgkgbhmQgZhoA8g7QA9g5A/BpQBABqAnBdIAEAIQAQAkASAdQAGANAKAMIAEAIQAYAgAbAaQA/A7ANAIQgNgIg/g7QgbgagYggIgEgIIAKgIQAQgLATgKQAggRAigIQAVgHAXgDIAAAAQAOgEAMgBQAPgCAPAAQBpADBoA/QBpBCgUGzQisCRhkgQQh9gUg3gUQgVgIgfABQgdAAhMASQhLAUg1AYQgcAMgQAAQgPAAgFgJgADUhNQhBh1hUgNQBUANBBB1g");
	this.shape_3.setTransform(22.2,15.8);

	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(-5.1,-17.5,0.68,0.68,23.5,0,0,0.3,-0.1);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.BlurFilter(33, 33, 3)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.1,-107.3,183,183);


(lib.Symbol2copy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Tween23("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(119.4,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:0},20).to({startPosition:0},50).wait(1));

	// Layer_2
	this.instance_1 = new lib.Tween24("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(119.4,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({x:231.5},20).to({startPosition:0},50).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000033").ss(3,1,1).p("EAlJAL2QAACzh+B+Qh/B+iyAAMg8zAAAQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzg");
	this.shape.setTransform(109.6,-5.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("A+ZSlQiyAAh/h+Qh+h+AAizIAA3rQAAizB+h+QB/h+CyAAMA8zAAAQCyAAB/B+QB+B+AACzIAAXrQAACzh+B+Qh/B+iyAAg");
	this.shape_1.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(71));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,255,0.02)").s().p("AeaTXMg8zAAAQjHAAiNiMQiNiNAAjIIAA3rQAAjICNiNQCNiMDHAAMA8zAAAQDHAACNCMQCNCNAADIIAAXrQAADIiNCNQiNCMjHAAIAAAAg");
	this.shape_2.setTransform(109.6,-5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(71));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-133,-129,485.4,247.9);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1copy("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(41,60.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:83.2},8).to({y:60.2},9).to({y:83.2},9).to({y:60.2},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,85,123.3);


(lib.Symbol1copy_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.Tween2copy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(184.3,62.4,0.9,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:1,scaleY:1,x:171.5,y:46.8},8).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},9).to({scaleX:1,scaleY:1,x:171.5,y:46.8},9).to({scaleX:0.9,scaleY:0.9,x:184.3,y:62.4},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103.8,-29.2,156,156);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// StarAni
	this.instance = new lib.fxSymbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(318.8,431.6,2.5,2.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(243).to({_off:false},0).wait(42).to({startPosition:2},0).to({alpha:0,startPosition:8},6).wait(5));

	// Layer_8
	this.instance_1 = new lib.Symbol2copy8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(197.8,429.8,1.072,1.072,0,0,0,3.1,-0.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(233).to({_off:false},0).to({scaleX:1.18,scaleY:1.18},10).wait(30).to({alpha:0},6).wait(17));

	// Layer_3
	this.instance_2 = new lib.Symbol1copy_1("synched",10);
	this.instance_2.parent = this;
	this.instance_2.setTransform(527.5,554.9,1,1,-12.5,0,0,190.3,64.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(199).to({_off:false},0).to({_off:true},34).wait(63));

	// Layer_2
	this.instance_3 = new lib.Symbol1copy("synched",10);
	this.instance_3.parent = this;
	this.instance_3.setTransform(332.9,234.6,1,1,-1.2,0,0,41.1,60.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(164).to({_off:false},0).to({_off:true},35).wait(97));

	// Layer_6
	this.instance_4 = new lib.Symbol2copy13("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(199.1,429,1.072,1.072,0,0,0,3.1,-0.8);
	this.instance_4.alpha = 0.66;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(85).to({_off:false},0).to({_off:true},59).wait(152));

	// Layer_10
	this.instance_5 = new lib.Symbol5("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(312,424.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(147).to({_off:false},0).to({_off:true},47).wait(102));

	// highlight
	this.instance_6 = new lib.Symbol4("synched",10);
	this.instance_6.parent = this;
	this.instance_6.setTransform(641.6,114.7,1,1,0,0,0,0.5,0.5);

	this.instance_7 = new lib.Tween16("synched",5);
	this.instance_7.parent = this;
	this.instance_7.setTransform(767.7,111.3);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},80).to({state:[]},64).to({state:[{t:this.instance_7}]},123).to({state:[{t:this.instance_7}]},6).to({state:[]},1).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(267).to({_off:false},0).to({alpha:0,startPosition:9},6).to({_off:true},1).wait(22));

	// Layer_7
	this.instance_8 = new lib.Tween4("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(640,451.1);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween20("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(640,421.6);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(49).to({_off:false},0).to({alpha:1},7).wait(88).to({startPosition:0},0).to({_off:true},123).wait(29));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(267).to({_off:false},0).to({alpha:0},6).to({_off:true},1).wait(22));

	// Layer_5
	this.instance_10 = new lib.Tween2("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(640,136.1);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(20).to({_off:false},0).to({alpha:1},7).to({startPosition:0},53).to({startPosition:0},187).to({alpha:0},6).to({_off:true},1).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(593.8,320.8,1370,782);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;