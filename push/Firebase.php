<?php 

class Firebase {
    public function send() {
		$tokens = array(); 
		array_push($tokens, 'fLoWjWYwig8:APA91bHl736oFoJrklskahkQE5rMY0Zb5FsqExflhbfcsezrGUkFz_qXZ6_fkvHSwMKjQIp7Rgs58hik_h9qiUxbzPNgdj-6fHAMYGV0ToLS6fSct38Vx1EsVOzNlegNVmDxAGEQ9I8P');
		$res = array();
        $res['data']['title'] ="Skillangels";
        $res['data']['message'] ="EDSIX";
        $res['data']['image'] = '';
		$data=$res;
		array_push($data,"Skillangels");
        $fields = array(
            'registration_ids' =>$tokens,
            'data' =>$data,
        );
        return $this->sendPushNotification($fields);
    }
    
    /*
    * This function will make the actuall curl request to firebase server
    * and then the message is sent 
    */
    private function sendPushNotification($fields) {
         
        //importing the constant files
       // require_once 'Config.php';
        
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        //building headers for the request
        $headers = array(
            'Authorization: key=AAAAfHgZB0w:APA91bGurz1RPnSd2E_d1sSrSVS163U5tVmWylkQ9wfOERZjm2F0v3zb8ezWTZmdY46q6ZpL_Ju0LoC8eFQk8Awyw8OqS1seEXaryBjnYXV8nV5RUfcAjyl98LdndrYHmzNYbHs6p7tb',
            'Content-Type: application/json'
        );

        //Initializing curl to open a connection
        $ch = curl_init();
 
        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);

        //adding headers 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        //adding the fields in json format 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        //finally executing the curl request 
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        //Now close the connection
        curl_close($ch);
 
        //and return the result 
        echo $result;
    }
}

$testObject = new Firebase();

$testObject->send();